# Les labs opensatck

La documenation générée est disponible sur le site [https://ebraux.gitlab.io/openstack-labs/](https://ebraux.gitlab.io/openstack-labs/)

## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_openstack-labs .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_openstack-labs rm -rf /work/site
docker image rm mkdocs_openstack-labs
```
Rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_openstack-labs mkdocs new openstack-labs
sudo chown -R $(id -u -n):$(id -g -n)  *
```
