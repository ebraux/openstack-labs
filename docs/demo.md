# Démo déploiement d'une infrastructure


Création d'un réseau privé, d'un sous-réseau et de groupes de sécurité et d'une instance dans un projet.

```bash
# ----------------------------------
#  Configuration de l'infrastructure
#     ___        __               _                   _
#    |_ _|_ __  / _|_ __ __ _ ___| |_ _ __ _   _  ___| |_ _   _ _ __ ___
#     | || '_ \| |_| '__/ _` / __| __| '__| | | |/ __| __| | | | '__/ _ \
#     | || | | |  _| | | (_| \__ \ |_| |  | |_| | (__| |_| |_| | | |  __/
#    |___|_| |_|_| |_|  \__,_|___/\__|_|   \__,_|\___|\__|\__,_|_|  \___|
#
# ----------------------------------


# Initialisation du nom du projet
PROJECT_NAME=$OS_PROJECT_NAME
echo $PROJECT_NAME

EXTERNAL_NETWORK_NAME=external
echo $EXTERNAL_NETWORK_NAME
# ->  defini dans la config de neutron

# configuration
NETWORK_NAME=${PROJECT_NAME}selfservice
SUBNET_NAME=${PROJECT_NAME}selfservice
ROUTER_NAME=${PROJECT_NAME}Routeur
echo $NETWORK_NAME
echo $SUBNET_NAME
echo $ROUTER_NAME
```

# Configuration d'une infrastructure réseau 
```bash

# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 8.8.4.4 --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
#neutron router-interface-add router selfservice
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'une interface vers le réseau externe
#neutron router-gateway-set router provider
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

# configuration DNS
#openstack subnet set --dns-nameserver 192.44.75.10 ${SUBNET_NAME}
#openstack subnet set --dns-nameserver 192.108.115.2 ${SUBNET_NAME}

# creation des groupes de sécurité
openstack security group create --description 'Admin Basiques rules' ${PROJECT_NAME}BasicAdmin
openstack security group rule create --proto icmp --dst-port 0 ${PROJECT_NAME}BasicAdmin
openstack security group rule create --proto tcp --dst-port 22 ${PROJECT_NAME}BasicAdmin
```

# creation d'une instance

```bash

# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list | grep '| cirros ' | awk '{ print $2 }')
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list | grep ' cirros ' | awk '{ print $2 }')
echo ${IMAGE_CIRROS_ID}

# creation d'un instance de test
TEST_INSTANCE_NAME=${PROJECT_NAME}Test
openstack server create \
  --flavor ${FLAVOR_CIRROS_ID} \
  --image ${IMAGE_CIRROS_ID} \
  --nic net-id=${PROJECT_NAME}selfservice \
  --security-group ${PROJECT_NAME}BasicAdmin \
  --wait \
  ${TEST_INSTANCE_NAME}


# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=`openstack floating ip list | grep '| None             | None' | head -1 | awk '{ print $4 }'`
openstack server add floating ip ${TEST_INSTANCE_NAME} ${FREE_FLOATING_IP}


ping ${FREE_FLOATING_IP}

ssh cirros@${FREE_FLOATING_IP}

```