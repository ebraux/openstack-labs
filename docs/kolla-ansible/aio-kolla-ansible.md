
# source
# https://docs.openstack.org/kolla-ansible/latest/user/quickstart.html


---
## Préparation

### Mise à jour du système

``` bash
sudo apt update 
sudo apt -y upgrade 
sudo apt autoremove -y --purge && sudo apt clean
[ -f /var/run/reboot-required ] && sudo systemctl reboot
```

### Configuration du réseau

Création d'une Dummy Interface pour le réseau external (ip flottantes)
``` bash
sudo ip tuntap add mode tap br_ex_port
sudo ip link set dev br_ex_port up
```

Vérification
``` bash
ip a show br_ex_port 
# 4: br_ex_port: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
#     link/ether 42:37:2e:e4:ee:66 brd ff:ff:ff:ff:ff:ff
```
- [https://gist.github.com/gilangvperdana/e74b3536c0c8786c68cb3ed51e4acbd2](https://gist.github.com/gilangvperdana/e74b3536c0c8786c68cb3ed51e4acbd2)


---
## Prérequis


### Packages
``` bash
sudo apt install -y  software-properties-common
sudo apt install -y   git python3-dev libffi-dev gcc libssl-dev
sudo apt install -y python3-venv
```

### Environnement virtuel

Mise en place
``` bash
cd ~
python3 -m venv $HOME/kolla-ansible
```

Initialisation
``` bash
cd $HOME/kolla-ansible
source bin/activate
```

Installation de pip 
``` bash
pip install -U pip
``` 

---
## Installation de Ansible

Installation 
``` bash
pip install 'ansible-core>=2.15,<2.16.99'
```

Configuration
``` bash
tee  $HOME/ansible.cfg > /dev/null <<EOF 
[defaults]
host_key_checking=False
pipelining=True
forks=100
EOF
```

---
## Préparation du déploiement de Kolla Ansible

Récupération du dépôt :
``` bash
pip install git+https://opendev.org/openstack/kolla-ansible@master
```
> Pour installer une version spécifique, remplacer `@master`, par la version souhaitée.

Mise en place fichiers de configuration
``` bash
sudo mkdir /etc/kolla
sudo chown $USER:$USER /etc/kolla
cp $HOME/kolla-ansible/share/kolla-ansible/etc_examples/kolla/* /etc/kolla/
```

Création du fichier d'inventory pour le mode "All In One" :
``` bash
cp $HOME/kolla-ansible/share/kolla-ansible/ansible/inventory/all-in-one .
```


Installation des  "Galaxy requirements" pour Ansible
``` bash
kolla-ansible install-deps
```

``` bash
# fix bug 2066255, with "request" version
# https://bugs.launchpad.net/kolla-ansible/+bug/2066255
# pip uninstall requests
# pip install requests==2.31.0
# pip uninstall docker
# pip install "docker>=3.0.0,<7.0.0"
```

Génération de mots de passes aléatoires (pour ceux qui n'ont pas été spécifiés)
``` bash
cp -p /etc/kolla/passwords.yml /etc/kolla/passwords.yml.DIST
kolla-genpwd
```

Préparation de la personnalisation du fichier de configuration
- Observer la configuration réseau de la machine.
    - Le réseau "Publique" (Interface et adresse IP), pour les accès à Horizon et aux API publiques. Il sera également utilisé pour les échanges internes.
    - L'interface pour le réseau "PROVIDER"
``` bash
ip a
# ...
# 2: ens2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     inet 51.159.182.163/32 metric 100 scope global dynamic ens2
#     ...
# 3: ens6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     inet 172.16.10.2/24 metric 50 brd 172.16.10.255 scope global dynamic ens6
#     ...
# 4: br_ex_port: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
#     link/ether 4a:e4:dd:9c:79:a3 brd ff:ff:ff:ff:ff:ff

```
- Déclarer les interfaces et adresses IP à utiliser
``` bash
export PUBLIC_NETWORK_ADDRESS="51.159.182.163"
export PUBLIC_NETWORK_INTERFACE="ens2"
export PROVIDER_NETWORK_INTERFACE="br_ex_port"
```

Génération du fichier de configuration
``` bash
cp -p /etc/kolla/globals.yml /etc/kolla/globals.yml.DIST
sudo tee /etc/kolla/globals.yml <<EOF
---
workaround_ansible_issue_8743: yes
kolla_base_distro: "ubuntu"
kolla_internal_vip_address: "${PUBLIC_NETWORK_ADDRESS}"
network_interface: "${PUBLIC_NETWORK_INTERFACE}"
neutron_external_interface: "${PROVIDER_NETWORK_INTERFACE}"
neutron_plugin_agent: "ovn"
enable_openstack_core: "yes"
enable_hacluster: "no"
enable_haproxy: "no"
nova_compute_virt_type: "qemu"
EOF
```
Vérification
``` bash
cat /etc/kolla/globals.yml
```
---
## Déploiement de Kolla Ansible

- Initialisation de l’environnement
``` bash
cd $HOME/kolla-ansible
source bin/activate
```
- Génération de l'ensemble des configurations
``` bash
kolla-ansible -i all-in-one bootstrap-servers
```
- Vérifications avant déploiement
``` bash
kolla-ansible -i all-in-one prechecks
```
- Déploiment :
``` bash
kolla-ansible -i all-in-one deploy
```
- Actions post déploiement (génération des fichier openrc, ...)
``` bash
kolla-ansible -i all-in-one post-deploy
```

---
## Installation des clients Openstack

``` bash
cd $HOME/kolla-ansible
source bin/activate

pip install python-openstackclient -c https://releases.openstack.org/constraints/upper/master
pip install python-neutronclient   -c https://releases.openstack.org/constraints/upper/master
pip install python-glanceclient    -c https://releases.openstack.org/constraints/upper/master
pip install python-heatclient      -c https://releases.openstack.org/constraints/upper/master

cp -p ~/.bashrc ~/.bashrc.ORI
openstack complete >> ~/.bashrc
source ~/.bashrc
```

Mise en place du ficher d'authentification
``` bash
cp -p /etc/kolla/admin-openrc.sh  .
```

---
## Initialisation

Kolla-Ansible propose un script qui initialise l'environnement en créant :

- Une image "cirros"
- Un réseau provider "public1"
    - De type external,
    - Associé à "physnet1"
    - En mode "flat"
- Un subnet associé : "public1-subnet", pour les accès sortants (routeurs et IP Flottantes)
    - CIDR : 203.0.113.0/24
    - Gateway : 203.0.113.1
    - DHCP activé : 203.0.113.150-203.0.113.199
    - DNS Serveur : - 
- Un routeur "demo-router"
- Un réseau "demo-net", et un subnet "demo-subnet", associé avec
    - Gateway : 10.0.0.1
    - DHCP activé : 10.0.0.2-10.0.0.254
    - DNS Serveur : 8.8.8.8
- Un groupe de sécurité autorisant en entrée, depuis n'importe où  : ping, SSH, TCP-8000, TCP-8080 depuis 
- Une clé SSH : mykey
    - clé privée : $HOME/.ssh/id_ecdsa
    - clé publique $HOME/.ssh/id_ecdsa.pub
- Des gabarits : m1.tiny, m1.small, m1.medium ...
 
``` bash
# Initialisation de l'environnement
cd $HOME/kolla-ansible
source bin/activate

source admin-openrc.sh

# Parametrage du réseau "external"
export ENABLE_EXT_NET=1
export EXT_NET_CIDR='203.0.113.0/24'
export EXT_NET_RANGE='start=203.0.113.150,end=203.0.113.199'
export EXT_NET_GATEWAY='203.0.113.1'

# Pas de paramétrage possible du réseau privé "demo"
# Le script force le réseau demo sur 10.0.0.0/24
#export DEMO_NET_CIDR='192.168.10.0/24'
#export DEMO_NET_GATEWAY='192.168.10.1'
#export DEMO_NET_DNS='8.8.8.8'

# Lancement de l'initialisation
~/kolla-ansible/share/kolla-ansible/init-runonce
```

Pour créer un instance :
``` bash
openstack --os-cloud=kolla-admin server create \
    --image cirros \
    --flavor m1.tiny \
    --key-name mykey \
    --network demo-net \
    demo1
```

---
## Ajouter des fonctionnalités : cinder

Sauvegarder le fichier de configuration
``` bash
cp -p /etc/kolla/globals.yml /etc/kolla/globals.yml.v1
```

Et y ajouter les lignes correspondant à cinder
``` bash
sudo tee -a /etc/kolla/globals.yml <<EOF
enable_cinder: "yes"
enable_cinder_backend_lvm: "yes"
EOF
```

Puis reconfigurer Kolla-ansible
``` bash
kolla-ansible -i all-in-one reconfigure
```
