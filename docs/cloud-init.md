# Cloud Init

---
## Introduction

Cloud-Init :

- Mécanisme qui permet de configurer les instances, au premier démarrage
- Intégré aux images Cloud, et supporté par les Cloud providers
- Intégré à la plupart des distributions : Alpine, Arch, Debian, Fedora, Gentoo, openSUSE, Red Hat ... 
 

Principe : 

  -  Un fichier de configuration (user-data)
  -  Peut être accompagné de métadonnées d'instance.
  -  Peut interagir avec des outils d'orchestration Ansible, Chef, Puppet, ...
  
Execution de script "user-data:
 - Lancement de scripts shell, python, ... (doit être supporté par la VM)
 - Le format le plus populaires : cloud-config.
 

---
## Utilisation avec Openstack

### Avec Horizon 

Menu "configuration" lors d ela création de l'instance:

![](img/horizon-cloud-init.png){ width="600"}

### En ligne de commande 

Option `--userdata <FICHIER_A_EXECUTER>` de la commande `openstack server create ...`


---
## Ressources pour tests


### Création du dossier pour les tests
``` bash
mkdir cloud-init
cd cloud-init
```

### Configuration de l'environnement en tant qu'utilisateur démo

``` bash 
source ~/demo-openrc
env | grep OS_
```

### Connexion réseau

``` bash 
openstack network list -f value -c Name
# external
# net-demo
``` 
``` bash 
NETWORK_NAME=net-demo
```

### Groupe de sécurité

``` bash 
openstack security group list -f value -c Name
# sg-demo-admin
# default
``` 
``` bash 
SECGROUP_ADMIN=sg-demo-admin
```

### Exemple de création d'un fichier user-data

``` bash
USER_DATA_FILE=cirros-passwd.sh
INSTANCE_NAME=cirros-passwd

tee ${USER_DATA_FILE} > /dev/null <<EOF
#!/bin/sh
echo "cirros:cirros" | chpasswd
EOF

```


### Parametres complémentaires pour une instance cirros

``` bash
IMAGE_NAME=cirros
FLAVOR_NAME=cirros
```

### Parametres complémentaires pour une instance Ubuntu
``` bash
IMAGE_NAME=ubuntu22.04
FLAVOR_NAME=m1.large	
```

### Lancement d'une instance cirros
``` bash
echo " - FLAVOR_NAME: ${FLAVOR_NAME}"
echo " - IMAGE_NAME: ${IMAGE_NAME}"
echo " - NETWORK_NAME: ${NETWORK_NAME}"
echo " - SECGROUP_ADMIN: ${SECGROUP_ADMIN}"
echo " - USER_DATA_FILE: ${USER_DATA_FILE}"
echo " - INSTANCE_NAME: ${INSTANCE_NAME}"

cat ${USER_DATA_FILE}

openstack server create \
   --flavor ${FLAVOR_NAME} \
   --image ${IMAGE_NAME} \
   --nic net-id=${NETWORK_NAME} \
   --security-group ${SECGROUP_ADMIN} \
   --user-data ${USER_DATA_FILE} \
   ${INSTANCE_NAME}
```


### Affectetion d'une IP flottante

Lister les IP Flottantes
``` bash
openstack floating ip list -c  'Floating IP Address' -c 'Fixed IP Address' 
# +---------------------+------------------+
# | Floating IP Address | Fixed IP Address |
# +---------------------+------------------+
# | 203.0.113.183       | None             |
# | 203.0.113.121       | None             |
# | 203.0.113.222       | None             |
# +---------------------+------------------+
```

``` bash
FREE_FLOATING_IP=203.0.113.183
```

Si besoin associer une IP flottante au projet
``` bash
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
```

Associer une IP flottante à une instance
``` bash
openstack server add floating ip ${INSTANCE_NAME} ${FREE_FLOATING_IP}
```

### Suppression d'une instance
``` bash
openstack server delete ${INSTANCE_NAME}
```

---
## Exemple de script "user-data" au format Shell


- ***CIRROS - shell*** Changement de mot de passe :  [cirros-passwd.sh](files/cloud-init/cirros-passwd.sh)
``` bash
USER_DATA_FILE=cirros-passwd.sh
INSTANCE_NAME=cirros-passwd
```

- ***UBUNTU - shell*** Authentification par mot de passe : [ubuntu-passwd.sh](files/cloud-init/ubuntu-passwd.sh)
``` bash
USER_DATA_FILE=ubuntu-passwd.sh
INSTANCE_NAME=ubuntu-passwd
```

- ***UBUNTU - shell*** Installation Docker : [ubuntu-nginx.sh](files/cloud-init/ubuntu-docker.sh)
``` bash
USER_DATA_FILE=ubuntu-docker.sh
INSTANCE_NAME=ubuntu-docker
```

- ***CIRROS - cloud-config*** Changement de mot de passe :  [cirros-passwd.yaml](files/cloud-init/cirros-passwd.yaml)
``` bash
USER_DATA_FILE=cirros-passwd.yaml
INSTANCE_NAME=cirros-passwd-cc
```

- ***UBUNTU - cloud-config*** Changement de mot de passe :  [ubuntu-passwd.yaml](files/cloud-init/ubuntu-passwd.yaml)
``` bash
USER_DATA_FILE=ubuntu-passwd.yaml
INSTANCE_NAME=ubuntu-passwd-cc
```

- ***UBUNTU - cloud-config*** Installation Docker : [ubuntu-docker.yaml](files/cloud-init/ubuntu-docker.yaml)
``` bash
USER_DATA_FILE=ubuntu-docker.yaml
INSTANCE_NAME=ubuntu-docker-cc
```



---
Références 

- [https://cloudinit.readthedocs.io/en/latest/](https://cloudinit.readthedocs.io/en/latest/)
- [https://cloudinit.readthedocs.io/en/latest/topics/format.html](https://cloudinit.readthedocs.io/en/latest/topics/format.html)
- [https://cloudinit.readthedocs.io/en/latest/topics/instancedata.html](https://cloudinit.readthedocs.io/en/latest/topics/instancedata.html)




