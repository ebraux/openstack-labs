
# Utilisation des volumes

---
## Objectif

- Initialiser un volume monté sur un serveur Linux 



Exepmle à partir d'un volume, monté sur un serveur cirros.

Rappel, 2 types de volumes : 

- Volume créés lors de la création d'un serveur :
    - "system disque" : disque dans lequel est stocké le système d'exploitation du serveur
    - "data disque" : disque additionnel
        - directment attaché au serveur
        - doit être initialisé avnt de pouvoir être utilisé.
- Volume créés indépendament d'un serveur : 
    - uniquement de type "data disque"
    - doit être attaché à un serveur
    - Une fois le volume attaché à un serveur, il doit être initialisé

**Dans les 2 cas, le volume, une fois attaché à un serveur doit êtr initailisé**

> L'initilisation d'un volume n'est à faire qu'une seule fois.


---
## Affichage des périphériques disponibles sur le serveur Hôte

``` bash
sudo fdisk -l
# Disk /dev/vda: 1 GiB, 1073741824 bytes, 2097152 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
# Disklabel type: gpt
# Disk identifier: F05EDE64-4EFD-477A-B01E-B37CCD5D3EB4
# 
# Device     Start     End Sectors  Size Type
# /dev/vda1  18432 2097118 2078687 1015M Linux filesystem
# /dev/vda15  2048   18431   16384    8M EFI System
# 
# Partition table entries are not in disk order.
# 
# Disk /dev/vdb: 1 GiB, 1073741824 bytes, 2097152 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
```

- Le device `/dev/sda` correspond au disque "système". Il contient 2 partitions `/dev/sda1` et `/dev/sda15`.
- Le device `/dev/sdb` correspond au disque "data". Il ne contient aucune partition.
  

## Création d'une partition sur le Volume

Utilisation de la commande fdisk, en mode édition, sur le device `/dev/sdb`: 

- pour créer nouvelle une partition.
- de type primaire
- utilisant l'essemble de l'espace disponible.
  
``` bash
sudo fdisk /dev/vdb
# 
# Welcome to fdisk (util-linux 2.33).
# Changes will remain in memory only, until you decide to write them.
# Be careful before using the write command.
# 
# Device does not contain a recognized partition table.
# Created a new DOS disklabel with disk identifier 0x4bf4ef4d.
# 
# Command (m for help): 
```
- La commande `n` permet de créer une nouvelle partition.
``` bash
Command (m for help): n
# Partition type:
# p primary (0 primary, 0 extended, 4 free)
# e extended
```
- La commande `p` permet de créer une partition primaire
``` bash
Select (default p): p
```
- Les options par défaut permettent de créer une partition "1", utilisant l'intégralité de l'espace disponible
``` bash
Partition number (1-4, default 1): 
# Using default value 1
First sector (2048-2097151, default 2048): 
# Using default value 2048
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-2097151, default 2097151): 
# Using default value 2097151
# 
# Created a new partition 1 of type 'Linux' and of size 1023 MiB.
# 
Command (m for help): 
```
- Afficher la nouvelle partition créée, avec la commande `p` :
``` bash
Command (m for help): p
# Disk /dev/vdb: 1 GiB, 1073741824 bytes, 2097152 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
# Disklabel type: dos
# Disk identifier: 0x4bf4ef4d
# 
# Device     Boot Start     End Sectors  Size Id Type
# /dev/vdb1        2048 2097151 2095104 1023M 83 Linux
# 
Command (m for help):
```
- Sauvegarder la configuration, avec la commande `w` :
``` bash
Command (m for help): w
# The partition table has been altered.
# Calling ioctl() to re-read partition table.
# Syncing disks.
```

## Initialisation de la partition créée

### Création du système de fichiers

- Synchronisation de la configuration avec le système
``` bash
partprobe
```
- Création d'un système de fichier sur la partition
``` bash
sudo mkfs -t ext4 /dev/vdb1
# mke2fs 1.44.5 (15-Dec-2018)
# Discarding device blocks: done                            
# Creating filesystem with 261888 4k blocks and 65536 inodes
# Filesystem UUID: fa080457-7850-4737-80f3-0cca98476110
# Superblock backups stored on blocks: 
# 	32768, 98304, 163840, 229376
# 
# Allocating group tables: done                            
# Writing inode tables: done                            
# Creating journal (4096 blocks): done
# Writing superblocks and filesystem accounting information: done
```

### Rendre le système de fichier accessible au système

- Création d'un dossier pour le point de montage
``` bash
sudo mkdir /mnt/data1
```
- "Montage" du volume
``` bash
sudo mount /dev/vdb1 /mnt/data1
```
- Vérification du montage
``` bash
ls -l /mnt/data1/
# total 16
# drwx------    2 root     root         16384 Sep 24 13:14 lost+found
```
- Affichage des volume au niveau du système
``` bash
sudo df -Th
# Filesystem           Type            Size      Used Available Use% Mounted on
# /dev                 devtmpfs      105.9M         0    105.9M   0% /dev
# /dev/vda1            ext3          980.0M     32.9M    905.8M   4% /
# tmpfs                tmpfs         113.0M         0    113.0M   0% /dev/shm
# tmpfs                tmpfs         113.0M     80.0K    112.9M   0% /run
# /dev/vdb1            ext4          990.9M      2.5M    921.2M   0% /mnt/data1
```
- Commande pour "démonter" le volume
``` bash
sudo umount /mnt/data1
```

---
## Rendre le volume accessible au redémarrage

Avec, la commande mount, le montage de la partition n'est pas permanent. Pour le rendre permanent, il faut modifier la configuration système, le fichier `/etc/fstab`, pour ajouter le point de montage.

Pour plus d'informations sur la syntaxe du fichier fstab : [https://www.linuxtricks.fr/wiki/fstab-explications-sur-le-fichier-et-sa-structure](https://www.linuxtricks.fr/wiki/fstab-explications-sur-le-fichier-et-sa-structure)

- Affichage du le contenu du fichier `/etc/fstab` , et en faire une sauvegarde
``` bash
sudo cat /etc/fstab 
# # /etc/fstab: static file system information.
# #
# # <file system> <mount pt>     <type> <options>         <dump> <pass>
# /dev/root  /         auto     rw,noauto                 0 1
# proc       /proc     proc     defaults                  0 0
# devpts     /dev/pts  devpts   defaults,gid=5,mode=620   0 0
# tmpfs      /dev/shm  tmpfs    mode=0777                 0 0
# sysfs      /sys      sysfs    defaults                  0 0
# sysfs      /sys      sysfs    defaults                  0 0
# tmpfs      /run      tmpfs    rw,nosuid,relatime,mode=755 0 0
```
- Sauvegarde du fichier /etc/fstab
``` bash
sudo cp -p /etc/fstab /etc/fstab.DIST
```
- Ajout d'une ligne dans le fichier `/etc/fstab`
``` bash
# Ajouter l'entrée à fstab
echo "/dev/vdb1 /mnt/data1  ext4 defaults 0  2"  | sudo tee -a  /etc/fstab

# Vérififier le contenu du fichier /etc/fstab
sudo cat /etc/fstab 
# # /etc/fstab: static file system information.
# #
# # <file system> <mount pt>     <type> <options>         <dump> <pass>
# /dev/root  /         auto     rw,noauto                 0 1
# proc       /proc     proc     defaults                  0 0
# devpts     /dev/pts  devpts   defaults,gid=5,mode=620   0 0
# tmpfs      /dev/shm  tmpfs    mode=0777                 0 0
# sysfs      /sys      sysfs    defaults                  0 0
# sysfs      /sys      sysfs    defaults                  0 0
# tmpfs      /run      tmpfs    rw,nosuid,relatime,mode=755 0 0
# /dev/vdb1  /mnt/data1  ext4 defaults 0  2
```
- Mise à jour de la configuration au niveau système
``` bash
mount -a
```
- Vérifier le résultat
``` bash
sudo mount | grep /mnt/data1
# /dev/vdb1 on /mnt/data1 type ext4 (rw,relatime)
```
