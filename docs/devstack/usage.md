# Utiliser Devstack

---
## Horizon

- `http://HOST_IP/dashboard/`

--
## Initialiser l'environnement

``` bash
# source openrc PROJECT USER
source openrc admin admin
```


---
## Accès aux Endpoint

Devstack déploie un reverse-proxy Nginx, qui permet un accès aux différentes API en utilisant le même port, le routage se faisant sur l'URI demandée

Exmple pour keystone, il faut utiliser `http://HOST_IP/identity/` au lieu de  `http://HOST_IP:5000`