# Déploiement de devstack

---
## Préparation du système

``` bash
sudo apt update && sudo apt upgrade -y
[ -f /var/run/reboot-required ] && sudo systemctl reboot
```

``` bash
sudo apt install -y git vim
```

---
## Création de l'utilisateur stack

Vérifier l'utilisateur stack :
``` bash
cat /etc/passwd | grep stack
# stack:x:1001:1001::/opt/stack:/bin/bash
```

Si l'utilisateur n'existe pas :
``` bash
sudo adduser --shell /bin/bash --home /opt/stack  stack
# Adding user `stack' ...
# Adding new group `stack' (1000) ...
# Adding new user `stack' (1000) with group `stack' ...
# Creating home directory `/opt/stack' ...
# Copying files from `/etc/skel' ...

sudo chmod +x /opt/stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
```

Attention le Homedir doit être "/opt/stack". Devstack indique dans la documentation qu'il est possible de modifier la destination de l'installation, avec la variable "DEV", mais ça ne fonctionne pas correctement.

La commande ci dessous permet de modifier le homedir de l'utilisateur stack, mais elle nécessite d'être exécuté sans que l'utilisateur stack soit connecté :
``` bash
sudo usermod -d /opt/stack -m stack 
```


---
## Préparation de Devstack

### Si besoin, se connecter en tant que stack

- Si non connecté, se connecter en ssh
```
ssh -l stack -h xxxx
```
- Si déjà connecté mais pas en utilisateur stack, utiliser `su`
``` bash
sudo su - stack
```

 
### Récupérer le dépôt
``` bash
cd ~
git clone https://git.openstack.org/openstack-dev/devstack
```

---
## Configurer Devstack


Préparer la génération de la configuration 
``` bash
export HOST_IP=51.159.182.163
```

Créer le fichier de configuration
- Minimum :
``` bash
tee ~/devstack/local.conf <<EOF
[[local|localrc]]
# Password for KeyStone, Database, RabbitMQ and Service
ADMIN_PASSWORD=stack
DATABASE_PASSWORD=\$ADMIN_PASSWORD
RABBIT_PASSWORD=\$ADMIN_PASSWORD
SERVICE_PASSWORD=\$ADMIN_PASSWORD
HOST_IP=${HOST_IP}
EOF
```
- Ou plus complet :
``` bash
tee ~/devstack/local.conf <<EOF
[[local|localrc]]
# Password for KeyStone, Database, RabbitMQ and Service
ADMIN_PASSWORD=stack
DATABASE_PASSWORD=\$ADMIN_PASSWORD
RABBIT_PASSWORD=\$ADMIN_PASSWORD
SERVICE_PASSWORD=\$ADMIN_PASSWORD

HOST_IP=${HOST_IP}

FLOATING_RANGE="203.0.113.0/24"
Q_FLOATING_ALLOCATION_POOL=start=203.0.113.101,end=203.0.113.250
PUBLIC_NETWORK_GATEWAY="203.0.113.1"

#Enable heat services
enable_service h-eng h-api h-api-cfn h-api-cw
#Enable heat plugin
enable_plugin heat https://opendev.org/openstack/heat
# Enable heat Dashbord
enable_plugin heat-dashboard https://opendev.org/openstack/heat-dashboard
EOF
```
> Pour d'autres options voir [Options complémentaires du fichier local.conf](./configs.md)


Vérifier
``` bash
cat ~/devstack/local.conf 
```
---
## Lancement

Correction BUG à l'installation
``` bash
chmod 755 /opt/stack
```

Lancement de l'installation
``` bash
cd ~/devstack
./stack.sh
```

---
## Désinstallation

``` bash
cd ~/devstack
./unstack.sh
```

