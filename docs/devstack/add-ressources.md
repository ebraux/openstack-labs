# Ajout de ressources

Image Ubuntu 22.04 LTS
``` bash
cd devstack/
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img

openstack image create ubuntu22 \
    --file jammy-server-cloudimg-amd64.img \
    --disk-format qcow2 \
    --container-format bare \
    --public
```
