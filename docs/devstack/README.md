# Déploiement avec  Devstack

- Mode opératoire pour Déploiement [manuel](manual.md)
- Fichier de déploiement avec [cloud-init](./user-data-devstack.yaml)
- [Configurations complémentaire](./configs.md)