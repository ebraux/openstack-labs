# Options complémentaires du fichier local.conf


Configurer les mots de passe
``` ini
# Password for KeyStone, Database, RabbitMQ and Service
ADMIN_PASSWORD=stack
DATABASE_PASSWORD=\$ADMIN_PASSWORD
RABBIT_PASSWORD=\$ADMIN_PASSWORD
SERVICE_PASSWORD=\$ADMIN_PASSWORD
```

Définir la configuration du réseau externe
``` bash
FLOATING_RANGE="203.0.113.0/24"
Q_FLOATING_ALLOCATION_POOL=start=203.0.113.101,end=203.0.113.250
PUBLIC_NETWORK_GATEWAY="203.0.113.1"
```

Ajouter heat
``` ini
[[local|localrc]]
#Enable heat services
enable_service h-eng h-api h-api-cfn h-api-cw
#Enable heat plugin
enable_plugin heat https://opendev.org/openstack/heat
# Enable heat Dashbord
enable_plugin heat-dashboard https://opendev.org/openstack/heat-dashboard

```


Ecrire les logs dans un fichier
``` ini
[[local|localrc]]
# DEST is /opt/stack
LOGFILE=$DEST/logs/stack.sh.log
```

Utilisation de OVS au lieu d'OVN
``` ini
[[local|localrc]]
# Open vSwitch provider networking configuration
Q_USE_PROVIDERNET_FOR_PUBLIC=True
OVS_PHYSICAL_BRIDGE=br-ex
PUBLIC_BRIDGE=br-ex
OVS_BRIDGE_MAPPINGS=public:br-ex
```

Neutron options
``` ini
[[local|localrc]]
Q_USE_SECGROUP=True
```

Modifier l'adressage IP pour le réseau publique
``` ini
[[local|localrc]]
FLOATING_RANGE="10.20.11.0/24"
# IPV4_ADDRS_SAFE_TO_USE="10.0.0.0/22"
Q_FLOATING_ALLOCATION_POOL=start=10.20.11.200,end=10.20.11.250
PUBLIC_NETWORK_GATEWAY="10.20.11.1"
PUBLIC_INTERFACE=eth2
```

Modifier le chemin de déploiement de devstack (/opt/stack par défaut). **Mais ne fonctionne pas lors des tests réalisés** :
``` ini
[[local|localrc]]
# !!! Non fonctionnel lors des tests !!
# Homedir of stack user
DEST=/home/stack
DATA_DIR=\$DEST/data
LOG_DIR=\$DEST/logs
```

---
## Récupérer des exemples dans les dépôt Openstack

Exemple avec une config dans le dépôt neutron, pour remplacer OVN par OVS 
``` bash
git clone https://opendev.org/openstack/neutron.git
cp neutron/devstack/ml2-ovs-local.conf.sample .
cat ml2-ovs-local.conf.sample
cp ml2-ovs-local.conf.sample devstack/local.conf
```