# Labs Openstack

---
## Labs de prise en main d'Openstack :

- Avec Horizon : [https://ebraux.gitlab.io/openstack-labs-101-horizon/](https://ebraux.gitlab.io/openstack-labs-101-horizon/)
- En ligne de commande : [https://ebraux.gitlab.io/openstack-labs-101-cli/](https://ebraux.gitlab.io/openstack-labs-101-cli/)
- Avec l'outil d'orchestation heat [https://ebraux.gitlab.io/openstack-labs-101-heat/](https://ebraux.gitlab.io/openstack-labs-101-heat/)

---
## Labs Thématiques

- Déploiement d'une infra DEMO : [demo](./demo.md)
- Initialisation d'un volume : [volumes](./volumes.md)
- Utilisation de Cloud-Init : [cloud-init](./cloud-init.md)
- Interagir avec placement : [placement](./placement.md)
- Labs d'approfondissement de l'outil d'orchestation heat [https://ebraux.gitlab.io/openstack-labs-heat/](https://ebraux.gitlab.io/openstack-labs-heat/)

---
## Labs de déploiement

- Lab d'installation d'Openstack [https://ebraux.gitlab.io/openstack-labs-installation/](https://ebraux.gitlab.io/openstack-labs-installation/)
- Déploiement avec [Devstack](./devstack/README.md)
- Déploiement avec [Kolla-Ansible](./kolla-ansible/README.md)
  
---
## Ressources heat

- "Machine Bastion" : [os-admin.yaml](files/heat/os-admin.yaml)
- "Devstack" : [devstack.yaml](files/heat/devstack.yaml)
- "Réseau dédié Jupyter + jupyter base notebook" : [jupyter-base-env.yaml](files/heat/jupyter-base-env.yaml)
- "Jupyter Lab Spark additionnel" : [jupyter-spark-node.yaml](files/heat/jupyter-spark-node.yaml)

---
    

![image alt <>](assets/openstack.png)

