


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index)


Ouvrir un shell ceph
``` bash
ssh root@10.129.172.61
cephadm shell
```

Verify that your external Ceph Storage cluster has an active Metadata Server (MDS):
``` bash
ceph -s
#   cluster:
#     id:     ae733d34-f325-11ed-9da0-e4434b831188
#     health: HEALTH_OK
 
#   services:
#     mon: 3 daemons, quorum rhcs-ceph-br-11.imta.fr,rhcs-ceph-br-12,rhcs-ceph-br-13 (age 4w)
#     mgr: rhcs-ceph-br-11.imta.fr.toqnbt(active, since 4w), standbys: rhcs-ceph-br-12.wyfnrw
#     osd: 12 osds: 12 up (since 4w), 12 in (since 4w)
 
#   data:
#     pools:   5 pools, 513 pgs
#     objects: 12 objects, 0 B
#     usage:   365 MiB used, 17 TiB / 17 TiB avail
#     pgs:     513 active+clean
```


Verify the pools in the CephFS file system (uniquement pour Manilla)
``` bash
ceph fs ls
# No filesystems enabled
```

Vérification de l'accès en mode client "cephx client name and key"
``` bash
ceph auth get client.admin
[client.admin]
	key = AQC1NWJkjOeoHRAAuAcmCIw4yvDl244QRVIDew==
	caps mds = "allow *"
	caps mgr = "allow *"
	caps mon = "allow *"
	caps osd = "allow *"
exported keyring for client.admin
```


Vérification / création des pools :

- storage for OpenStack Block Storage (cinder): `ceph osd pool create volumes <pgnum>`
- Storage for OpenStack Image Storage (glance): `ceph osd pool create images <pgnum>`
- Storage for instances: `ceph osd pool create vms <pgnum>`
- Storage for OpenStack Block Storage Backup (cinder-backup): `ceph osd pool create backups <pgnum>`
- Storage for OpenStack Telemetry Metrics (gnocchi): `ceph osd pool create metrics <pgnum>`

Les noms des pools sont définis par défaut. Ca reste possible de les modifier dans le fichier d'environnement.

### Pool pour openstack :
- volumes, classe slow
- images,  classe slow
- backups, classe slow
- vms, classe standard

``` bash
ceph osd lspools
# 1 device_health_metrics
# 4 backups
# 6 volumes
# 7 images
# 8 vms
```


Création d'un client Openstack pour
``` bash
ceph auth add client.openstack mgr 'allow *' mon 'profile rbd' osd 'profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups'
```
Et vérification
``` bash
ceph auth list
# client.openstack
# 	key: AQBl0pBkPbkyFhAAVkpbdBpIsozbi0Qc+C48AQ==
# 	caps: [mgr] allow *
# 	caps: [mon] profile rbd
# 	caps: [osd] profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_ceph_storage/4/html/administration_guide/ceph-user-management](https://access.redhat.com/documentation/fr-fr/red_hat_ceph_storage/4/html/administration_guide/ceph-user-management)

Récupérer la clé du client.openstack
``` bash

ceph auth get-key client.openstack
# AQBl0pBkPbkyFhAAVkpbdBpIsozbi0Qc+C48AQ==
```

Récupérer le fsid du cluster
``` bash
cat /etc/ceph/ceph.conf 
# # minimal ceph.conf for ae733d34-f325-11ed-9da0-e4434b831188
# [global]
# 	fsid = ae733d34-f325-11ed-9da0-e4434b831188
# 	mon_host = [v2:192.168.177.61:3300/0,v1:192.168.177.61:6789/0] [v2:192.168.177.62:3300/0,v1:192.168.177.62:6789/0] [v2:192.168.177.63:3300/0,v1:192.168.177.63:6789/0]
```


Installation de ceph-ansible (normalment déjà installé)
``` bash
sudo dnf install -y ceph-ansible
```

Intégration de ceph au déploiment

le fichier de temlate d eheat correspondnat à l'intégration est : `/usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml`

Crér un fichier de customisation de l'accès à CEPH
``` bash
touch /home/stack/templates/ceph-config.yaml
```

Ajouter la configuration pour accéder au cluster
``` yaml
parameter_defaults:
  CephClientKey: AQBl0pBkPbkyFhAAVkpbdBpIsozbi0Qc+C48AQ==
  CephClusterFSID: ae733d34-f325-11ed-9da0-e4434b831188
  CephExternalMonHost: 192.168.176.61, 192.168.176.62, 192.168.176.63
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index#proc-creating-a-custom-environment-file_integrate-with-existing-cs-cluster](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index#proc-creating-a-custom-environment-file_integrate-with-existing-cs-cluster)


---
## Intégration avec le déploiement de l'overcloud

``` yaml
openstack overcloud deploy --templates \
-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml \
-e /home/stack/templates/ceph-config.yaml \
-e --ntp-server pool.ntp.org \
...
```

--templates - Creates the overcloud from the default heat template collection, /usr/share/openstack-tripleo-heat-templates/.

-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml - Sets the director to integrate an existing Ceph cluster to the overcloud.

-e /home/stack/templates/ceph-config.yaml - Adds a custom environment file to override the defaults set by -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml. In this case, it is the custom environment file you created in Installing the ceph-ansible package.

--ntp-server pool.ntp.org - Sets the NTP server.

---
## procédure de vérification

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index#assembly-verify-external-ceph-storage-cluster-integration_preparing-overcloud-nodes](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index#assembly-verify-external-ceph-storage-cluster-integration_preparing-overcloud-nodes)