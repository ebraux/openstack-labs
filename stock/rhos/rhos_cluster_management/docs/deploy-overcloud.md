# Déploiement de l'overcloud

---
## Déploiement par défaut.

La configuration déployé par défaut est :
- 1 controller et 1 compute
- quel réseau ??
- 1 seule interface réseau, (vlan ?


Pour faire un déploiement en production, il faut donc personnaliser:

- Le nombre de noeuds déployés (au moins 3 controler, et plusieurs compute)
- Le type de noeuds déployés: des compute DVR, GPU, ...
- Les réseaux, pour qu'ils correspondent aux subnet et Vlans configurés sur le matériel physique (switch, routeurs, ...)
- La configuration des interface réseau : nombre de cartes, répartition des réseaux sur ces cartes, mise en place de redondance, ... 
- L'enregistrement des noeuds auprès des souscription RedHat, pour l'accès aux ressources (packages et images)
- L'affectation des adresses IP
- La gestion du nom d'accès au service et du certificats https


Le coeur de la configuration est composé des 3 éléments :

1. La liste des types de noeud à déployer
2. La liste des réseaux ((Vlan/subnets) à utiliser
3. La configuration de la gestion des interfaces. 
   - Cette configuration est généré en fonction des 2 listes précédentes
   - Elle doit être re-générée si des modfications leur sont apportées

Les autres ajustements viennent en complément et sont relativement indépendants


---
## Configuration du type de noeuds à déployer

---
## Configuration des réseaux (Subnets et Vlans)


---
## Configuration des interfaces réseau

---
## Nombre et type de noeuds à déployer

configuration_network-interfaces

---

---
## Configuration des profil et des rôles 

### principe

| Type	  | Description |
| --------|-------------|
| Role    | The role defines how director configures nodes. |
| Flavor  | The flavor defines the hardware profile for nodes. You assign this flavor to a role so that director can decide which nodes to use. |
| Profile | The profile is a tag you apply to the flavor. This defines the nodes that belong to the flavor. |
| Node    | You also apply the profile tag to individual nodes, which groups them to the flavor and, as a result, director configures them using the role. |


### profile flavors
Default profile flavors are created during undercloud installation :

- compute
- control
- swift-storage
- ceph-storage
- block-storage 



---
## Gestion des rôles



---
## Création des fichier d'environnement

### principe

The undercloud includes a set of heat templates that form the plan for your overcloud creation. You can customize aspects of the overcloud with environment files, which are YAML-formatted files that override parameters and resources in the core heat template collection. You can include as many environment files as necessary. However, the order of the environment files is important because the parameters and resources that you define in subsequent environment files take precedence. 

Red Hat recommends that you organize your custom environment files in a separate directory, such as the templates directory.

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_defining-the-root-disk-for-multi-disk-clusters_basic](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_defining-the-root-disk-for-multi-disk-clusters_basic)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/)


### Dimensionnement du cluster

Fichier `node-info.yaml`
``` bash
touch /home/stack/templates/node-info.yaml
```

contenu
``` yaml
parameter_defaults:
  OvercloudControllerFlavor: control
  OvercloudComputeFlavor: compute
  ControllerCount: 3
  ComputeCount: 3
```

###  Disabling TSX on new deployments

Fichier `disable-TSX.yaml`
``` bash
touch /home/stack/templates/disable-TSX.yaml
```

Contenu
``` yaml
parameter_defaults:
    ComputeParameters:
       KernelArgs: "tsx=off"
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_disabling-tsx-on-new-deployments_basic](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_disabling-tsx-on-new-deployments_basic)



### Mot de pass root pour les instances

- [https://access.redhat.com/solutions/4364591](https://access.redhat.com/solutions/4364591)


### configuration du réseau (OVN + DVR)

Open Virtual Networking (OVN) is the default networking mechanism driver in Red Hat OpenStack Platform 16.2. If you want to use OVN with distributed virtual routing (DVR), you must include the environments/services/neutron-ovn-dvr-ha.yaml file in the openstack overcloud deploy command. If you want to use OVN without DVR, you must include the environments/services/neutron-ovn-ha.yaml file in the openstack overcloud deploy command.

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#environment-files](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#environment-files)


---
## Affecter un nom à un noeud




---
## Gestion de l'affectation des adresse IP internes

``` bash
cp /usr/share/openstack-tripleo-heat-templates/environments/ips-from-pool-ctlplane.yaml ~/templates/.
```

- Créer des catégories au format `<role_name>IPs`
- Et dans cahque catégorie définir les IP à utiliser dans les différetnts plages réseau

Donc dans notre cas :

- `ControllerIPs` : external, internal_api, storage, storage_mgmt, tenant
- `ComputeDVRIPs` : internal_api, storage, tenant


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_controlling-node-placement#proc_assigning-predictable-ips_controlling-node-placement](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_controlling-node-placement#proc_assigning-predictable-ips_controlling-node-placement)


Il y a un truc pas net avec les 
``` bash
resource_registry:
  OS::TripleO::Controller::Ports::ExternalPort: ../network/ports/external_from_pool.yaml
  OS::TripleO::Controller::Ports::InternalApiPort: ../network/ports/internal_api_from_pool.yaml
  OS::TripleO::Controller::Ports::StoragePort: ../network/ports/storage_from_pool.yaml
  OS::TripleO::Controller::Ports::StorageMgmtPort: ../network/ports/storage_mgmt_from_pool.yaml
  OS::TripleO::Controller::Ports::TenantPort: ../network/ports/tenant_from_pool.yaml
  # Management network is optional and disabled by default
  #OS::TripleO::Controller::Ports::ManagementPort: ../network/ports/management_from_pool.yaml

  OS::TripleO::Compute::Ports::ExternalPort: ../network/ports/noop.yaml
  OS::TripleO::Compute::Ports::InternalApiPort: ../network/ports/internal_api_from_pool.yaml
  OS::TripleO::Compute::Ports::StoragePort: ../network/ports/storage_from_pool.yaml
  OS::TripleO::Compute::Ports::StorageMgmtPort: ../network/ports/noop.yaml
  OS::TripleO::Compute::Ports::TenantPort: ../network/ports/tenant_from_pool.yaml
  #OS::TripleO::Compute::Ports::ManagementPort: ../network/ports/management_from_pool.yaml
```

---
## Getsion de l'affectation des adresse IP des service clusterisés

```
parameter_defaults:
  ...
  # Predictable VIPs
  ControlFixedIPs: [{'ip_address':'192.168.201.101'}]
  InternalApiVirtualFixedIPs: [{'ip_address':'172.16.0.9'}]
  PublicVirtualFixedIPs: [{'ip_address':'10.1.1.9'}]
  StorageVirtualFixedIPs: [{'ip_address':'172.18.0.9'}]
  StorageMgmtVirtualFixedIPs: [{'ip_address':'172.19.0.9'}]
  RedisVirtualFixedIPs: [{'ip_address':'172.16.0.8'}]
  OVNDBsVirtualFixedIPs: [{'ip_address':'172.16.0.7'}]
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_controlling-node-placement#proc_assigning-predictable-virtual-ips_controlling-node-placement](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_controlling-node-placement#proc_assigning-predictable-virtual-ips_controlling-node-placement)




---



---
### activer DVR

c'ets un role spécifique :
- [cp /usr/share/openstack-tripleo-heat-templates/environments/ips-from-pool-ctlplane.yaml ~/templates/.
](cp /usr/share/openstack-tripleo-heat-templates/environments/ips-from-pool-ctlplane.yaml ~/templates/.
)



 Se fait au moment de la définition des rôles, et de l'affecttion des profiles.
 Nécessite la regénration des templates réseau, si ça a déjà été fait.


---
## Gestion de la configuration réseau

La configuration définie est :

- Pour les controller : 
  - Control plane attached to nic1.
  - Bonded NIC1 : NIC2 et NIC3 : Tenant, Internal API, External, FIP
  - Bonded NIC2 (MTU à 9000) : NIC4 et NIC5 : Storage, Storage Management
- Pour les compute (RJ45 et SFP+): 
  - Control plane attached to nic1.
  - Bonded NIC1 : NIC2 et NIC3 : Internal API, Tenant, External, FIP
  - Bonded NIC2 (MTU à 9000 ) : NIC4 et NIC5 : Storage

Pour les Composable Network, pas besoin de configuration complémentaire, les "composable Network définis par défaut conviennent. Il suffit juste d'adapter les données de configuration à nos données

Pour les La gestion des intefaces par contre il ets nécéccaise d'adapter la configuration.


ControlPlane

InternalApi
Tenant
External

Storage
StorageMgmt


---
## Paramétrage des "composable Network" (configuration des vlan et subnets)


### Activation de la personnalisation du réseau

Pour configurer le réseau, il faut :

- An environment file that you can use to enable network isolation (/usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml).
  - Note : Before you deploy RHOSP with director, the files network-isolation.yaml and network-environment.yaml are only in Jinja2 format and have a .j2.yaml extension. Director renders these files to .yaml versions during deployment.
- An environment file that you can use to configure network defaults (/usr/share/openstack-tripleo-heat-templates/environments/network-environment.yaml).
- A network_data file that you can use to define network settings such as IP ranges, subnets, and virtual IPs. This example shows you how to create a copy of the default and edit it to suit your own network.
- Templates that you can use to define your NIC layout for each node. The overcloud core template collection contains a set of defaults for different use cases.
- An environment file that you can use to enable NICs. This example uses a default file located in the environments directory.

Les réseaux utilisé par l'overcloud et leur configuration sont décrit dans des fichiers de template de heat. Les apels à ces templates sont regroupés dans le fichier  !
- fichier principal : `/usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml`
- dossier contennant toutes les tempalates : `/usr/share/openstack-tripleo-heat-templates/network`


Il faut donc inclure le fichier principal dans la liste des fichiers d'environnement à charger : 
``` bash
openstack overcloud deploy --templates \
    ...
    -e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
    ...
``` 

Before you deploy RHOSP with director, the files network-isolation.yaml and network-environment.yaml are only in Jinja2 format and have a .j2.yaml extension.
Director renders these files to .yaml versions during deployment.


### Personnalisation de la configuration du réseau (Vlan et subnets)

Copier le fichier par défaut, et l'adapter par rapport aux réseaux définis
``` bash
cp /usr/share/openstack-tripleo-heat-templates/network_data.yaml /home/stack/.
```

Puis modier le fichier `/home/stack/network_data.yaml` :

Et enfin l'inclure dans la commande de déployement "Include the custom network_data.yaml file with your deployment using the -n option. Without the -n option, the deployment command uses the default network details."
``` bash
openstack overcloud deploy --templates \
    ...
    -n  /home/stack/network_data.yaml \
    ...
``` 

---
### Gestion des interfaces

Architectures par défaut :

- single-nic-vlans :  Single NIC (nic1) with control plane and VLANs attached to default Open vSwitch bridge.
- single-nic-linux-bridge-vlans : Single NIC (nic1) with control plane and VLANs attached to default Linux bridge.
- bond-with-vlans :
  - Control plane attached to nic1.
  - Default Open vSwitch bridge with bonded NIC configuration (nic2 and nic3) and VLANs attached.
- multiple-nics : 
  - Control plane attached to nic1.
  - Assigns each sequential NIC to each network defined in the network_data.yaml file.
  - By default, this is Storage to nic2, Storage Management to nic3, Internal API to nic4, Tenant to nic5 on the br-tenant bridge, and External to nic6 on the default Open vSwitch bridge.

Par défaut Tripleo génére les fichiers de template Heat pour la gestion du réseau pendnat la phase de déploiment, à partir de fichier de template écrits en Jinja2.

Pour pouvoir gérer une configuration personnalisée, il faut générer ces fichiers, et le modifier.Puis ensuite indiquer le chemin vers cete configuration lors du déploiement



Attention : bine définir les rôle avant de génerer les fichier de template heat. En cas de modification des rôle, il faudra les régénérer.

Génération des fichiers de template Heat utilisés pour la configuration du réseau. Ils sont générés, dans le dossier `~/openstack-tripleo-heat-templates-rendered`
``` bash
cd /usr/share/openstack-tripleo-heat-templates
#./tools/process-templates.py -o ~/openstack-tripleo-heat-templates-rendered
#./tools/process-templates.py -o ~/openstack-tripleo-heat-templates-rendered -n /home/stack/network_data.yaml -r /home/stack/roles_data.yaml
./tools/process-templates.py -o /home/stack/openstack-tripleo-heat-templates-rendered -n /home/stack/network_data.yaml -r /home/stack/roles_data.yaml
```

Copie des template Heat de configuration du réseau dans le dossier des templates personnalisées pour le déploiement (ici "multiple-NIC servirait de base). Mais dans notre cas, c'est un modèle qui dépend de plusieurs template, donc pas de template de base, on prend notre template : 
``` bash
# ll /home/stack/openstack-tripleo-heat-templates-rendered/network/config
cp -r /home/stack/openstack-tripleo-heat-templates-rendered/network/config/bond-with-vlans /home/stack/templates/nics-config/
#mkdir /home/stack/templates/custom-nics/
```

Modification pour répartir les Vlan sur 2 bonds pourle controller.
  Pas de Vlan External pour le DVR ???

Modififier le chemin de run-os-net-config.sh. remplacer `../../scripts/run-os-net-config.sh` par `/usr/share/openstack-tripleo-heat-templates/network/scripts/run-os-net-config.sh`

Principe des fichiers de template Heat:

- 2 parties : 
  - `parameters` : 
    - Reprend la définition des données qui vont être utilisées pour le déploiment.
    - Ce sont ces données qui sont définies dans le fichier `/home/stack/network_data.yaml`  (ControlPlaneIp, InternalApiIpSubnet, TenantNetworkVlanID, ...)
    - La plupart ont des valeurs définies par défaut (ex  ControlPlaneMtu = 1500)
  - `resources` :
    - Définition desressources qui seront réélement déploiyées par l'orchestarteur Heat
      - définition des interface réseau utilisées
      - 
  - `outputs` : 
    - Données publiées par la stack une fois déploiyée
    - Peuvent être utilisée par d'autres template

Dans le dossier de template par défaut , un modèle '2-linux-bonds-vlans' est disponible, qui pourrait correspondre à notre besoin (nic1 : ControlPlane / bond_api : nic2 et nic3 / bond-data : nic4 et nic5)
, mais : 

- non documenté sur le site
- ne semble pas fonctienelle (par exemple, mélange entre données du `bond_api`  et du `StorageNetworkVlanID`)
 

Définition donc d'un nouveau modèle "bond-with-vlans-imta", définissant pour les rêmes "controller" et "compute"

- Une interface pour le `ControlPlane`
  - utilisant l'interface NIC1
- Un bridge `Linux bridge` en mode active/backup
  - nom: bond1
  - Utilisant les interfaces NIC2 et NIC3
  - Hébergeant les Vlans `InternalApi`, `Tenant` et `External`
  - configuré ave le MTU calculé en fonction des MTU de ces Vlan : `MinViableMtuBond1`
- Un bridge `Linux bridge` en mode active/backup
  - nom: bond2
  - Utilisant les interfaces NIC4 et NIC5
  - Hébergeant les Vlans `Storage` et `StorageMgmt`
  - configuré ave le MTU calculé en fonction des MTU de ces Vlan : `MinViableMtuBond2`

Sur le modèle :

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#con_creating-linux-bonds_network-interface-bonding](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#con_creating-linux-bonds_network-interface-bonding)

Autres exemples de configuration :

- [https://github.com/openstack/os-net-config/tree/master/etc/os-net-config/samples](https://github.com/openstack/os-net-config/tree/master/etc/os-net-config/samples)
  


Fichier d'environnement associé : /home/stack/templates/custom-network-configuration.yaml

``` yaml
resource_registry:
  #OS::TripleO::BlockStorage::Net::SoftwareConfig:
  #  /home/stack/templates/nic-configs/cinder-storage.yaml
  OS::TripleO::Compute::Net::SoftwareConfig:
    /home/stack/templates/nic-configs/compute.yaml
  OS::TripleO::Controller::Net::SoftwareConfig:
    /home/stack/templates/nic-configs/controller.yaml
  #OS::TripleO::ObjectStorage::Net::SoftwareConfig:
  #  /home/stack/templates/nic-configs/swift-storage.yaml
  #OS::TripleO::CephStorage::Net::SoftwareConfig:
  #  /home/stack/templates/nic-configs/ceph-storage.yaml

parameter_defaults:
  # Gateway router for the provisioning network (or Undercloud IP)
  #ControlPlaneDefaultRoute: 192.0.2.254
  # Define the DNS servers (maximum 2) for the overcloud nodes
  DnsServers: ["192.44.75.10","192.108.115.2"]
  #NeutronExternalNetworkBridge: "''"
  BondInterfaceOvsOptions: "bond_mode=active-backup"
  NeutronEnableDVR: true
```  

 


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#network-interface-templates](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#network-interface-templates)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#assembly_custom-network-interface-templates](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/advanced_overcloud_customization/index#assembly_custom-network-interface-templates)
- [https://infohub.delltechnologies.com/l/red-hat-openstack-platform-16-1-with-dell-emc-powerflex-family-1/custom-yamls-for-openstack-overcloud-deployment-1](https://infohub.delltechnologies.com/l/red-hat-openstack-platform-16-1-with-dell-emc-powerflex-family-1/custom-yamls-for-openstack-overcloud-deployment-1)
- [https://atl.kr/dokuwiki/doku.php/redhat_openstack_16.2_installation](https://atl.kr/dokuwiki/doku.php/redhat_openstack_16.2_installation)

---
## Mapping d'interfaces

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_custom-network-interface-templates#ref_network-interface-architecture_custom-network-interface-templates](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_custom-network-interface-templates#ref_network-interface-architecture_custom-network-interface-templates)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces][https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces]


Docs également, voirla description dan sla template heat : `/usr/share/openstack-tripleo-heat-templates/firstboot/os-net-config-mappings.yaml`
``` yaml
description: >
  Configure os-net-config mappings for specific nodes
  Your environment file needs to look like:
    parameter_defaults:
      NetConfigDataLookup:
        node1:
          nic1: "00:c8:7c:e6:f0:2e"
        node2:
          nic1: "00:18:7d:99:0c:b6"
        node3:
          dmiString: 'system-uuid'
          id: 'A8C85861-1B16-4803-8689-AFC62984F8F6'
          nic1: em3
        # Dell PowerEdge
        nodegroup1:
          dmiString: "system-product-name"
          id: "PowerEdge R630"
          nic1: em3
          nic2: em1
          nic3: em2
        # Cisco UCS B200-M4"
        nodegroup2:
          dmiString: "system-product-name"
          id: "UCSB-B200-M4"
          nic1: enp7s0
          nic2: enp6s0

  This will result in the first node* entry where either:
       a) a mac matches a local device
    or b) a DMI String matches the specified id
  being written as a mapping file for os-net-config in
  /etc/os-net-config/mapping.yaml
```


Pour obtenir les informations : utiliser `sudo dmidecode`. Par exemple :
``` bash
sudo dmidecode
# System Information
# 	Manufacturer: Dell Inc.
# 	Product Name: PowerEdge R620
# 	Version: Not Specified
# 	Serial Number: GZCFS12
# 	UUID: 4c4c4544-005a-4310-8046-c7c04f533132
# 	Wake-up Type: Power Switch
# 	SKU Number: SKU=NotProvided;ModelName=PowerEdge R620
# 	Family: Not Specified
```

```
No longer hard coding to a specifc network interface name.

    Instead of using a specific network interface name, thi fix
    fetch all ethernet mac addresses. Then uses this list of
    mac addresses to do a check if any entries in the list
    match any of the values in NetConfigDataLookup for a node.
    If there is a match, the /etc/os-net-config/mapping.yaml
    file for the node will be written.

    This fix removes the hard coded interface name 'eth0' used
    to get a mac address as identifyer for the specific node
    before. Using a hard coded interface name such as 'eth0'
    would have failed on most hardware because of "consistent
    network device names".
```    

os-net-config



Config réseau

``` bash
 openstack baremetal introspection interface list ${NODE}
# +-----------+-------------------+----------------------+-------------------+----------------+
# | Interface | MAC Address       | Switch Port VLAN IDs | Switch Chassis ID | Switch Port ID |
# +-----------+-------------------+----------------------+-------------------+----------------+
# | eno4      | 18:66:da:ec:3a:b7 | []                   | None              | None           |
# | enp5s0f3  | 00:0a:f7:53:a8:6f | []                   | None              | None           |
# | eno2      | 18:66:da:ec:3a:b5 | []                   | f0:78:16:37:7c:00 | Gi2/0/20       |
# | enp5s0f1  | 00:0a:f7:53:a8:6d | []                   | None              | None           |
# | eno3      | 18:66:da:ec:3a:b6 | []                   | None              | None           |
# | enp5s0f2  | 00:0a:f7:53:a8:6e | []                   | None              | None           |
# | eno1      | 18:66:da:ec:3a:b4 | []                   | f0:78:16:37:7c:00 | Gi2/0/8        |
# | enp5s0f0  | 00:0a:f7:53:a8:6c | []                   | None              | None           |
# +-----------+-------------------+----------------------+-------------------+----------------+
```

Et pour obtenir des infos détaillées sur une interface
``` bash
openstack baremetal introspection interface show ${NODE} eno1
# +--------------------------------------+----------------------------------------------------------------------------------------+
# | Field                                | Value                                                                                  |
# +--------------------------------------+----------------------------------------------------------------------------------------+
# | interface                            | eno1                                                                                   |
# | mac                                  | 18:66:da:ec:3a:b4                                                                      |
# | node_ident                           | rhos-comp-br-08                                                                        |
# | switch_capabilities_enabled          | ['Bridge']                                                                             |
# | switch_capabilities_support          | ['Bridge', 'Router']                                                                   |
# | switch_chassis_id                    | f0:78:16:37:7c:00                                                                      |
# | switch_port_autonegotiation_enabled  | True                                                                                   |
# | switch_port_autonegotiation_support  | True                                                                                   |
# | switch_port_description              | Openstack DFVS-RHOS vlan 50-56  et 172-179, 50 native                                  |
# | switch_port_id                       | Gi2/0/8                                                                                |
# | switch_port_link_aggregation_enabled | None                                                                                   |
# | switch_port_link_aggregation_id      | None                                                                                   |
# | switch_port_link_aggregation_support | None                                                                                   |
# | switch_port_management_vlan_id       | None                                                                                   |
# | switch_port_mau_type                 | 1000BASE-T full duplex                                                                 |
# | switch_port_mtu                      | None                                                                                   |
# | switch_port_physical_capabilities    | ['1000BASE-T fdx', '100BASE-TX fdx', '100BASE-TX hdx', '10BASE-T fdx', '10BASE-T hdx'] |
# | switch_port_protocol_vlan_enabled    | None                                                                                   |
# | switch_port_protocol_vlan_ids        | None                                                                                   |
# | switch_port_protocol_vlan_support    | None                                                                                   |
# | switch_port_untagged_vlan_id         | 173                                                                                    |
# | switch_port_vlan_ids                 | []                                                                                     |
# | switch_port_vlans                    | None                                                                                   |
# | switch_protocol_identities           | None                                                                                   |
# | switch_system_name                   | stack-d01-02.enst-bretagne.fr                                                          |
# +--------------------------------------+----------------------------------------------------------------------------------------+
``` 
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_additional-introspection-operations](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_additional-introspection-operations)



Autres informations
``` bash
openstack baremetal introspection data save ${NODE} |  jq ".interfaces"

openstack baremetal introspection data save ${NODE} |  jq ".numa_topology.nics"
   
openstack baremetal introspection data save ${NODE} |  jq ".extra.network"
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_additional-introspection-operations](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_additional-introspection-operations)

Refaire la même procédure avec les différents neouds: 
``` bash
NODE=rhos-comp-br-08
PROFILE=compute
```
Profils R630
eno1 : "18:66:da:ec:3a:b4",
eno2 : 18:66:da:ec:3a:b5
eno3 : 18:66:da:ec:3a:b6
eno4 : 18:66:da:ec:3a:b7"
enp5s0f0 : 00:0a:f7:53:a8:6c
enp5s0f1 : 00:0a:f7:53:a8:6d
enp5s0f2 : 00:0a:f7:53:a8:6e
enp5s0f3 : 00:0a:f7:53:a8:6f

Profil HPE
ens10f0np0 : 00:62:0b:ac:48:b4 
ens10f0np1 : 00:62:0b:ac:48:b5
ens1f0 : "30:3e:a7:02:ba:3a"
ens1f1 : 30:3e:a7:02:ba:3b",
ens2f0 : "30:3e:a7:02:ba:2a",
ens2f1 : 30:3e:a7:02:ba:2b",



Pour faire eun config en fonction du modèle : "DMI keyword" 

If you want to use the NetConfigDataLookup configuration, you must also include the os-net-config-mappings.yaml file in the NodeUserData resource registry.

Normally, os-net-config registers only the interfaces that are already connected in an UP state. However, if you hardcode interfaces with a custom mapping file, the interface is registered even if it is in a DOWN state.


``` yaml
resource_registry:
  OS::TripleO::NodeUserData: /usr/share/openstack-tripleo-heat-templates/firstboot/os-net-config-mappings.yaml
parameter_defaults:
  NetConfigDataLookup:
    node1:
      nic1: "em1"
      nic2: "em2"
    node2:
      nic1: "00:50:56:2F:9F:2E"
      nic2: "em2"
```      



Scroll to the network configuration section. This section looks like the following
``` yaml
   config:
        str_replace:
          template:
            get_file: ../../scripts/run-os-net-config.sh
```            
``` yaml
   config:
        str_replace:
          template:
            get_file: /usr/share/openstack-tripleo-heat-templates/network/scripts/run-os-net-config.sh
```            

---
### Appel de la configuration réseau

``` bash
$ openstack overcloud deploy --templates \
    ...
    -r /home/stack/templates/roles_data.yaml \
    -n /home/stack/network_data.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
    -e /usr/share/openstack-tripleo-heat-templates/environments/network-environment.yaml \
    -e /home/stack/templates/custom-network-configuration.yaml \
    ...
```

rem: l'apple à "roles_data.yaml" n'est necessaire que si un #composable réseau a été défini", et est utilisé dans un rôle


---
## intégration avec CEPH

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_storage_cluster/index)


---
## RHSM registration

- avec Ansible [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_ansible-based-overcloud-registration](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_ansible-based-overcloud-registration)
  
Le fichier de configuration par défaur : `/usr/share/openstack-tripleo-heat-templates/environments/rhsm.yaml`

Vérifier que chacun des roles définis dans le fichier `/home/stack/roles_data.yaml` contient bien le service composable Rhsm
``` bash
 ServicesDefault:
    ...
   - OS::TripleO::Services::Rhsm
```

Inclure le fichier `/usr/share/openstack-tripleo-heat-templates/environments/rhsm.yaml` dans les fichiers d'environnement

Créer un fichier pour adapter les parametres: `/home/stack/templates/custom-rhsm.yaml` `

``` yaml
parameter_defaults:
  ControllerParameters:
    RhsmVars:
      rhsm_repos:
        - rhel-8-for-x86_64-baseos-eus-rpms
        - rhel-8-for-x86_64-appstream-eus-rpms
        - rhel-8-for-x86_64-highavailability-eus-rpms
        - ansible-2.9-for-rhel-8-x86_64-rpms
        - openstack-16.2-for-rhel-8-x86_64-rpms
        - fast-datapath-for-rhel-8-x86_64-rpms
      rhsm_username: "myusername"
      rhsm_password: "p@55w0rd!"
      rhsm_org_id: "1234567"
      rhsm_pool_ids: "55d251f1490556f3e75aa37e89e10ce5"
      rhsm_method: "portal"
      rhsm_release: 8.4
  ComputeParameters:
    RhsmVars:
      rhsm_repos:
        - rhel-8-for-x86_64-baseos-eus-rpms
        - rhel-8-for-x86_64-appstream-eus-rpms
        - rhel-8-for-x86_64-highavailability-eus-rpms
        - ansible-2.9-for-rhel-8-x86_64-rpms
        - openstack-16.2-for-rhel-8-x86_64-rpms
        - fast-datapath-for-rhel-8-x86_64-rpms
      rhsm_username: "myusername"
      rhsm_password: "p@55w0rd!"
      rhsm_org_id: "1234567"
      rhsm_pool_ids: "55d251f1490556f3e75aa37e89e10ce5"
      rhsm_method: "portal"
      rhsm_release: 8.4
```
-[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/framework_for_upgrades_13_to_16.2/updating-overcloud-registration-to-the-red-hat-customer-portal-preparing-overcloud#proc_applying-the-rhsm-composable-service-to-different-roles_portal](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/framework_for_upgrades_13_to_16.2/updating-overcloud-registration-to-the-red-hat-customer-portal-preparing-overcloud#proc_applying-the-rhsm-composable-service-to-different-roles_portal)
- [https://github.com/openstack/ansible-role-redhat-subscription](https://github.com/openstack/ansible-role-redhat-subscription)


-
---
## Déploiement

Vérifier les nodes disponible et leur profils
``` bash
openstack overcloud profiles list
```

Options de déploiement
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#deployment-command](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#deployment-command)
- []

détail : [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#ref_including-environment-files-in-an-overcloud-deployment_basic](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#ref_including-environment-files-in-an-overcloud-deployment_basic)
``` bash
(undercloud) $ openstack overcloud deploy --templates \
  -e /home/stack/templates/node-info.yaml\
  -e /home/stack/containers-prepare-parameter.yaml \
  -e /home/stack/inject-trust-anchor-hiera.yaml \
  -r /home/stack/templates/roles_data.yaml \
```



Valider le plan d edéploiement :

``` bash
openstack overcloud deploy --templates \
-e environment-file1.yaml \
-e environment-file2.yaml \
...
```
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#ref_including-environment-files-in-an-overcloud-deployment_basic](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#ref_including-environment-files-in-an-overcloud-deployment_basic)


--limit NODE1,NODE2
Use this option with a comma-separated list of nodes to limit the config-download playbook execution to a specific node or set of nodes. For example, the --limit option can be useful for scale-up operations, when you want to run config-download only on new nodes. This argument might cause live migration of instances between hosts to fail, see Running config-download with the [ansible-playbook-command.sh script](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-the-overcloud-with-ansible#running-config-download-with-the-ansible-playbook-command-sh-script-configuring-the-overcloud-with-ansible)




- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_provisioning-bare-metal-nodes-before-deploying-the-overcloud#proc_provisioning-bare-metal-nodes_preprovisioned](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_provisioning-bare-metal-nodes-before-deploying-the-overcloud#proc_provisioning-bare-metal-nodes_preprovisioned)


Résultat :

``` bash
 Stack overcloud/3d139d77-5786-4097-93df-e1d477030c15 UPDATE_COMPLETE 

Waiting for messages on queue 'tripleo' with no timeout.
Deploying overcloud configuration
The output file /home/stack/overcloud-deploy/overcloud/overcloud-deployment_status.yaml will be overriden

Enabling ssh admin (tripleo-admin) for hosts:
10.129.173.195 10.129.173.188 10.129.173.133
Using ssh user heat-admin for initial connection.
Using ssh key at /home/stack/.ssh/id_rsa for initial connection.
ssh-keygen has been run successfully
Removing short term keys locally

The output file /home/stack/overcloud-deploy/overcloud/overcloud-deployment_status.yaml will be overriden
Overcloud Endpoint: http://10.129.176.9:5000
Overcloud Horizon Dashboard URL: http://10.129.176.9:80/dashboard
Overcloud rc file: /home/stack/scripts/overcloudrc
Overcloud Deployed with error

Timed out waiting for port 22 from 10.129.173.195
END return value: 1
```


openstack stack failures list overcloud --long

### Options de déploiement

``` bash
  --run-validations     Run external validations from the tripleo-validations
                        project.
  --inflight-validations
                        Activate in-flight validations during the deploy. In-
                        flight validations provide a robust way to ensure
                        deployed services are running right after their
                        activation. Defaults to False.
 --no-proxy NO_PROXY   A comma separated list of hosts that should not be
                        proxied.
 --environment-directory <HEAT ENVIRONMENT DIRECTORY>
                        Environment file directories that are automatically
                        added to the heat stack-create or heat stack-update
                        commands. Can be specified more than once. Files in
                        directories are loaded in ascending sort order.
  --roles-file ROLES_FILE, -r ROLES_FILE
                        Roles file, overrides the default roles_data.yaml in
                        the --templates directory. May be an absolute path or
                        the path relative to --templates
  --networks-file NETWORKS_FILE, -n NETWORKS_FILE
                        Networks file, overrides the default network_data.yaml
                        in the --templates directory
 --dry-run             Only run validations, but do not apply any changes.

``` 

---
## Annexes

### Pb d'accès aux API / proxy

``` bash
curl -v --interface enp10s0 http://10.129.176.9:5000
* Rebuilt URL to: http://10.129.176.9:5000/
* Uses proxy env variable no_proxy == '127.0.0.1,localhost,.localdomain,rhos-director-br-01.ctlplane.localdomain,,rhos-director-br-01,10.129.173.16,10.129.173.17,.imta.fr,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr,10.129.172.0/24,10.129.173.0/24,192.168.176.0/24,192.168.177.0/24'
* Uses proxy env variable http_proxy == 'http://proxy.enst-bretagne.fr:8080'
*   Trying 192.108.115.2...
* TCP_NODELAY set
* Local Interface enp10s0 is ip 10.129.176.16 using address family 2
* Local port: 0
```

``` bash
no_proxy=127.0.0.1,localhost,.localdomain,rhos-director-br-01.ctlplane.localdomain,rhos-director-br-01,10.129.176.9,192.168.74.9,10.129.173.16,10.129.173.17,.imta.fr,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr,10.129.172.0/24,10.129.173.0/24,192.168.176.0/24,192.168.177.0/24,10.129.176.0/22,192.168.174.0/24,192.168.175.0/24'
```

