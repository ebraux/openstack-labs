# Déploiement de DVR


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/networking_with_open_virtual_network/deploying-dvr-ovn](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/networking_with_open_virtual_network/deploying-dvr-ovn)
- [https://linkedin.com/pulse/taste-distributed-virtual-routing-derek-li/](https://linkedin.com/pulse/taste-distributed-virtual-routing-derek-li/)

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#dvr-known-issues_config-dvr](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#dvr-known-issues_config-dvr)

La config par défaut, c'est OVN + DVR

avec OVS : stock/openstack-tripleo-heat-templates-rendered_DVR/environments/neutron-ovs-dvr.yaml




---
## Principe :

Déployer DVR est prévu, mail assez mal documenté.
Il n'est pas possible de faire cohabiter des compute avec et sans DVR. L'activation de DVR ne se fait donc pas via un profil spécifique, mais via la modification du profil "Compute"


--
## mise en eouvre


### Modifier la customisation du réseau


Fichier : `/home/stack/templates/custom-network-configuration.yaml`

Vérifier les spécification des fichiers de configuration à prendre en compte pour les controller et les computes (entrée de `OS::TripleO::xxxx::Net::SoftwareConfig` :

- `OS::TripleO::Compute::Net::SoftwareConfig: /home/stack/templates/nic-configs/compute.yaml`
- `OS::TripleO::Controller::Net::SoftwareConfig: /home/stack/templates/nic-configs/controller.yaml`

Ca doit être les mêmes???

Les principales différences, c'est  :
- le réseau "StorageMgmtInterfaceRoutes"
- le réseau "ExternalIpSubnet"
- la répartition des interfaces

Il faudrait donc juste ajouter le réseau "external" normalement ???

On copie quand même le fichier, à tester plus tard

``` bash
cd /home/stack/templates/nic-configs
mv compute.yaml compute-sans-dvr.yaml
cp -p controller.yaml compute.yaml
```

### Modifier le rôle Compute


Vérifier la Différence entre le rôle Compute et le rôle ComputeDVR :
``` bash
cd /usr/share/openstack-tripleo-heat-templates/roles/
diff Compute.yaml  ComputeDVR.yaml 
# 2c2
# < # Role: Compute                                                               #
# ---
# > # Role: ComputeDVR                                                            #
# 4c4
# < - name: Compute
# ---
# > - name: ComputeDVR
# 6c6
# <     Basic Compute Node role
# ---
# >     DVR enabled Compute Node role
# 8d7
# <   # Create external Neutron bridge (unset if using ML2/OVS without DVR)
# 18c17
# <   HostnameFormatDefault: '%stackname%-novacompute-%index%'
# ---
# >   HostnameFormatDefault: '%stackname%-novacompute-dvr-%index%'
# 22,33d20
# <   # Deprecated & backward-compatible values (FIXME: Make parameters consistent)
# <   # Set uses_deprecated_params to True if any deprecated params are used.
# <   # These deprecated_params only need to be used for existing roles and not for
# <   # composable roles.
# <   uses_deprecated_params: True
# <   deprecated_param_image: 'NovaImage'
# <   deprecated_param_extraconfig: 'NovaComputeExtraConfig'
# <   deprecated_param_metadata: 'NovaComputeServerMetadata'
# <   deprecated_param_scheduler_hints: 'NovaComputeSchedulerHints'
# <   deprecated_param_ips: 'NovaComputeIPs'
# <   deprecated_server_resource_name: 'NovaCompute'
# <   deprecated_nic_config_name: 'compute.yaml'
# 68d54
# <     - OS::TripleO::Services::Rear
```

Re générer le fichier de rôle :
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller ComputeDVR
```

Dans le fichier de rôle généré `/home/stack/roles_data.yaml` :

- Modifier le nom du rôle, de ComputeDVR en Compute
- Ajouter le réseau "External"

``` bash
diff /home/stack/roles_data.yaml.DIST /home/stack/roles_data.yaml
# 202c202
# < # Role: ComputeDVR                                                            #
# ---
# > # Role: Compute                                                               #
# 204c204
# < - name: ComputeDVR
# ---
# > - name: Compute
# 210a211,212
# >     External:
# >       subnet: external_subnet
```




### Re-générer la configuration de template pour vérifier

``` bash
cd /usr/share/openstack-tripleo-heat-templates
./tools/process-templates.py -o /home/stack/openstack-tripleo-heat-templates-rendered_DVR -n /home/stack/network_data.yaml -r /home/stack/roles_data.yaml
cd -
```

Vérifier la partie `Port assignments by role`, et vérifier que pour le `Compute`, il y a bien un `ExternalPort` : `OS::TripleO::Compute::Ports::ExternalPort: ../network/ports/external.yaml`

``` yaml
# Port assignments by role, edit role definition to assign networks to roles.
  # Port assignments for the Controller
  OS::TripleO::Controller::Ports::StoragePort: ../network/ports/storage.yaml
  OS::TripleO::Controller::Ports::StorageMgmtPort: ../network/ports/storage_mgmt.yaml
  OS::TripleO::Controller::Ports::InternalApiPort: ../network/ports/internal_api.yaml
  OS::TripleO::Controller::Ports::TenantPort: ../network/ports/tenant.yaml
  OS::TripleO::Controller::Ports::ExternalPort: ../network/ports/external.yaml
  # Port assignments for the Compute
  OS::TripleO::Compute::Ports::StoragePort: ../network/ports/storage.yaml
  OS::TripleO::Compute::Ports::InternalApiPort: ../network/ports/internal_api.yaml
  OS::TripleO::Compute::Ports::TenantPort: ../network/ports/tenant.yaml
  OS::TripleO::Compute::Ports::ExternalPort: ../network/ports/external.yaml
```



---
##




Pour pouvoir utiliser "DVR", il faut considérer qu'on crée un "custom role", en s'aidant de l'implémentation qui est partiellement réalisée.

Le nom du rôle est `ComputeDVR`.

Par rapport à un noeud de compute standard


- Gestion du rôle ComputeDVR
  - Il est bien intégré à la liste des rôles par défaut
  - Il comprend

[configurer un rôle](./configuration_role.md)

Dans notre cas, nous avons besoin des roles : Controller et ComputeDVR. pour générer la configuration correspondante :
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller ComputeDVR
```


Lister les noeuds et leur état :
``` bash
openstack baremetal node list
```
Il faut des noeuds dont le `Provisioning State` est **available**


Lister les profils
``` bash
openstack overcloud profiles list
``` 
Parmis les noeuds qui sont dispnibles, il faut 

---
## tests
resources.ComputeDVR: Went to status ERROR due to "Message: No valid host was found.
https://access.redhat.com/discussions/2779731
--> gérer le profile

openstack compute service list --service nova-compute
+----+--------------+---------------------------------+------+---------+-------+----------------------------+
| ID | Binary       | Host                            | Zone | Status  | State | Updated At                 |
+----+--------------+---------------------------------+------+---------+-------+----------------------------+
|  6 | nova-compute | rhos-director-br-01.localdomain | nova | enabled | up    | 2023-06-20T15:17:23.000000 |
+----+--------------+---------------------------------+------+---------+-------+----------------------------+


openstack baremetal node show e4f88d1f-d262-4988-9fc4-23a0eb087183  -c properties
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field      | Value                                                                                                                                                                                                                                                                 |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| properties | {'vendor': 'unknown (0xb85c)', 'local_gb': '893', 'cpus': '64', 'cpu_arch': 'x86_64', 'memory_mb': '131072',
                'capabilities': 'node:rhos-ctrl-br-01,boot_option:local,profile:control,cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true'} |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


openstack baremetal node show 7c981c9b-da70-4d61-b1cc-3e0bbe10fcfa -c properties
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field      | Value                                                                                                                                                                                                                                      |
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| properties | {'local_gb': '445', 'cpus': '80', 'cpu_arch': 'x86_64', 'memory_mb': '196608', 
                'capabilities': 'node:rhos-comp-br-01,profile:computedvr,boot_option:local,cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true'} |
+------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


network-environment.yaml point sur /usr fichier, avec relation to ../network au lieux de /home/stack/tempaltes/nic..
