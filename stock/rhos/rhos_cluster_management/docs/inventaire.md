
---
## ocs
``` bash
sudo apt install -y ocsinventory-agent read-edid

sudo tee << EOF /etc/ocsinventory/ocsinventory-agent.cfg > /dev/null
server=https://ocs.imta.fr/ocsinventory
wait=3600
nosoftware=1
EOF

ocsinventory-agent -f -i --wait=5 --server https://ocs.imta.fr/ocsinventory --nosoftware --ssl=0 --debug
```

---
## eqi

ctr01 : 10399
ctr02 : 10400
ctr03 : 10401
CEPH21: 10402
CEPH22: 10403
CEPH23: 10404
CEPH24: 10405
CEPH25: 10406

---
## etiquettes

COMP 14
CTRL 2
CEPH 7

24 13
10 11 12 13 14 15

# crtl-01
NIC1 : 
00:62:0b:ac:48:b4
NIC2 : 


#02
NIC1 : 
00:62:0b:ad:e4:62
NIC2 : 


#ctrl03
NIC1 : 
00:62:0b:ad:14:30
173           00:62:0b:ad:14:30   dynamic     ethernet1/1/1           
NIC2 : 


#1
NIC1: 
e4:43:4b:ea:b2:b0
173           e4:43:4b:ea:73:b0   dynamic     ethernet1/1/4  

#2
e4:43:4b:ea:67:a0

#3 
e4:43:4b:ea:67:50



173           30:3e:a7:02:a6:9a   dynamic     ethernet1/1/17          
173           52:54:00:57:70:99   dynamic     ethernet1/1/1           

173           68:4f:64:e9:a3:e6   dynamic     ethernet1/1/1           




#4
NIC1 : 
E4:43:4B:EA:67:70
173           e4:43:4b:ea:67:70   dynamic     ethernet1/1/3           
NIC2 :

#5 
NIC1 : 
173           e4:43:4b:ea:73:b0   dynamic     ethernet1/1/4  
NIC2 :




#7
NIC1 : stack D1-02/2, port 4
NIC2 : stack D1-02/1, port 4
PXE : 18:66:DA:EC:3A:B4


#8
NIC1: stack D1-02/2, port 8
NIC2: stack D1-02/2, port 20
PXE : 18:66:DA:EC:3A:B4

#9
NIC1: stack D1-02/2, port 12
NIC2: stack D1-02/2, port 24
18:66:DA:EC:5E:B0

#10 (ex comp04)
NIC1: stack D1-02/2, port 3
NIC2: stack D1-02/2, port 15
18:66:DA:EC:5E:B0

#11 (ex comp05)
NIC1: stack D1-02/2, port 7
NIC2: stack D1-02/2, port 19


#12 (ex comp06)
NIC1: stack D1-02/2, port 11
NIC2: stack D1-02/2, port 23


#13 (ex comp01)
NIC1: stack D1-02/2, port 2
NIC2: stack D1-02/2, port 14
c8:1f:66:bd:eb:49

#14 (ex comp02)
NIC1: stack D1-02/2, port 6
NIC2: stack D1-02/2, port 18
c8:1f:66:bd:ea:ad

#15 (ex comp03)
NIC1: stack D1-02/2, port 10
NIC2: stack D1-02/2, port 22
c8:1f:66:bd:ec:0d
