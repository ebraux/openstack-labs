# Préparation du déploiment de l'Overcloud

---
## Récupération des images (Openstack) pour l'Overcloud

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#assembly_obtaining-images-for-overcloud-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#assembly_obtaining-images-for-overcloud-nodes)


voir le doc : [https://bugzilla.redhat.com/show_bug.cgi?id=1913527](https://bugzilla.redhat.com/show_bug.cgi?id=1913527)


## Gestion des images Docker

Il y a une registry déployée avec Apache ???

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#undercloud-container-registry](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#undercloud-container-registry)

## ceph :

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#con_using-ceph-storage-in-a-multi-architecture-overcloud_cpu-architecture](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#con_using-ceph-storage-in-a-multi-architecture-overcloud_cpu-architecture)



