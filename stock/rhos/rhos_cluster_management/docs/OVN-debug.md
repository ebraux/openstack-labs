# Debug et gestion de la config OVN

---
## config

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#troubleshoot-vlan-networks_neutron-troubleshoot](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#troubleshoot-vlan-networks_neutron-troubleshoot)
-[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#config-flat-prov-networks_connect-instance](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#config-flat-prov-networks_connect-instance)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/configuring-bridge-mappings_rhosp-network#config-bridge-mappings_config-bridge-mappings](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/configuring-bridge-mappings_rhosp-network#config-bridge-mappings_config-bridge-mappings)
]
-[https://www.youtube.com/watch?v=dfRZv5LzWhg](https://www.youtube.com/watch?v=dfRZv5LzWhg)

---
## debug
``` bash
[root@overcloud-controller-0 ~]# podman ps | grep ovn
# a537e59b466e  cluster.common.tag/openstack-ovn-northd:pcmklatest                                           /bin/bash /usr/lo...  4 hours ago  Up 4 hours ago          ovn-dbs-bundle-podman-0
# 4331df18e9b5  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-neutron-server-ovn:16.2  kolla_start           4 hours ago  Up 4 hours ago          neutron_api
# a41bbf5585ef  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-nova-novncproxy:16.2     kolla_start           4 hours ago  Up 4 hours ago          nova_vnc_proxy
# d38080c625c0  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-ovn-controller:16.2      kolla_start           4 hours ago  Up 4 hours ago          ovn_controller
```

``` bash
podman exec -it ovn_controller sh
# sh-4.4# 
```


Liste l'infar réseau

- br-ex, avec les Vlan, et le patch vers br-int
- br-int, avec les chassis (autre neouds), et le patch vers br-ex
``` bash
ovs-vsctl show
```


lister les chassis : renvoie les différent Nodes (Controller et compute) et leur interface dans le vlan vxlan (175)
``` bash
ovn-sbctl show
```

Lister les réseaux (switch) et routeur (router) créés dans Openstack
``` bash
ovn-nbctl show
```

interface physique.

sur cette interface, le bridge "br-ex"
  - avec une interface pour le bridge phy-br-ex

Sur l'interface phy-br-ex, le bridge "br-int"

Sur le bridge "br-int", on va une interface pour chaque routeurs créé pour les vlan virtuels.

Et pour chaxcun de ces routeurs,
- une interface vers br-int,
- et une interface vers la VM

- [https://www.openvswitch.org/support/dist-docs-2.5/tutorial/OVN-Tutorial.md.html](https://www.openvswitch.org/support/dist-docs-2.5/tutorial/OVN-Tutorial.md.html)
- [https://docs.openstack.org/developer/networking-ovn/testing.html](https://docs.openstack.org/developer/networking-ovn/testing.html)
- [https://docs.openstack.org/neutron/latest/contributor/testing/ml2_ovn_devstack.html](https://docs.openstack.org/neutron/latest/contributor/testing/ml2_ovn_devstack.html)
- [https://github.com/ovn-org/ovn-scale-test](https://github.com/ovn-org/ovn-scale-test)
- [https://docs.openstack.org/networking-ovn/queens/admin/refarch/provider-networks.html](https://docs.openstack.org/networking-ovn/queens/admin/refarch/provider-networks.html)


---
## Config Overcloud

services sur un controller :
``` bash
podman ps | grep ovn
  openstack-ovn-northd:pcmklatest    ovn-dbs-bundle-podman-0
  openstack-neutron-server-ovn:16.2  neutron_api
  openstack-ovn-controller:16.2      ovn_controller
```

Sur un compute :
``` bash
sudo podman ps  | grep ovn
openstack-ovn-controller:16.2   ovn_controller
openstack-neutron-metadata-agent-ovn:16.2  ovn_metadata_agent
```

``` bash
openstack network list
# +--------------------------------------+------------+--------------------------------------+
# | ID                                   | Name       | Subnets                              |
# +--------------------------------------+------------+--------------------------------------+
# | 79065691-deb9-4c72-aefe-af3ae81baa6a | network-eb | 6191e810-9cef-4791-b911-af95b58a6d2c |
```

Rechercher une machine sur laquelle le réseau est présent :
``` bash
ip netns list
ovnmeta-79065691-deb9-4c72-aefe-af3ae81baa6a (id: 0)
```

Tester la connexion
``` bash
ip netns exec ovnmeta-79065691-deb9-4c72-aefe-af3ae81baa6a route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 tap79065691-d1
```

``` bash
ip netns exec ovnmeta-79065691-deb9-4c72-aefe-af3ae81baa6a ping 10.129.176.1
```

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/networking_guide/neutron-troubleshoot_rhosp-network](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/networking_guide/neutron-troubleshoot_rhosp-network)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/neutron-troubleshoot_rhosp-network](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/neutron-troubleshoot_rhosp-network)





``` bash
[heat-admin@overcloud-controller-0 ~]$ sudo podman ps | grep ovn
4edef434526a  cluster.common.tag/openstack-ovn-northd:pcmklatest                                           /bin/bash /usr/lo...  2 hours ago        Up 2 hours ago                ovn-dbs-bundle-podman-0
53416f4ddd0a  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-neutron-server-ovn:16.2  kolla_start           About an hour ago  Up About an hour ago          neutron_api
1b518f32a8fb  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-nova-novncproxy:16.2     kolla_start           About an hour ago  Up About an hour ago          nova_vnc_proxy
e0a01268a995  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-ovn-controller:16.2      kolla_start           About an hour ago  Up About an hour ago          ovn_controller
```


``` bash
 NeutronBridgeMappings: 'datacentre:br-ex,tenant:br-tenant'
  #
  # DVR est activé par défaut avec RHOSP 16.2
  NeutronEnableDVR: True
```

``` bash
  network/scripts/run-os-net-config.sh:    sed -i "s/: \"bridge_name/: \"${bridge_name:-''}/g" /etc/os-net-config/config.json
```

``` bash
# The following environment variables may be set to substitute in a
# custom bridge or interface name.  Normally these are provided by the calling
# SoftwareConfig resource, but they may also be set manually for testing.
# $bridge_name : The bridge device name to apply
# $interface_name : The interface name to apply
```

voir les fichier d'exemple :
- /home/stack/template-stock/openstack-tripleo-heat-templates-rendered/network/config/multiple-nics/compute-dvr.yaml
- multiple-nics-vlans/role.role.j2.yaml 




/etc/neutron/plugin.ini
``` bash
[ml2]
type_drivers=geneve,vxlan,vlan,flat
tenant_network_types=geneve
mechanism_drivers=ovn
path_mtu=0
extension_drivers=qos,port_security,dns
overlay_ip_version=4

[securitygroup]
firewall_driver=iptables_hybrid

[ml2_type_geneve]
max_header_size=38
vni_ranges=1:65536

[ml2_type_vxlan]
vxlan_group=224.0.0.1
vni_ranges=1:65536

[ml2_type_vlan]
network_vlan_ranges=datacentre:1:1000

[ml2_type_flat]
flat_networks=datacentre

[ovn]
ovn_nb_connection=tcp:10.129.173.155:6641
ovn_sb_connection=tcp:10.129.173.155:6642
ovsdb_connection_timeout=180
ovsdb_probe_interval=60000
neutron_sync_mode=log
ovn_l3_mode=True
vif_type=ovs
ovn_metadata_enabled=True
enable_distributed_floating_ip=True
dns_servers=

[network_log]
rate_limit=100
burst_limit=25
```

plugin ml2
``` bash
[ml2]
type_drivers=geneve,vxlan,vlan,flat
tenant_network_types=geneve
mechanism_drivers=ovn
path_mtu=0
extension_drivers=qos,port_security,dns
overlay_ip_version=4

[securitygroup]
firewall_driver=iptables_hybrid

[ml2_type_geneve]
max_header_size=38
vni_ranges=1:65536

[ml2_type_vxlan]
vxlan_group=224.0.0.1
vni_ranges=1:65536

[ml2_type_vlan]
network_vlan_ranges=datacentre:1:1000

[ml2_type_flat]
flat_networks=datacentre

[ovn]
ovn_nb_connection=tcp:10.129.173.155:6641
ovn_sb_connection=tcp:10.129.173.155:6642
ovsdb_connection_timeout=180
ovsdb_probe_interval=60000
neutron_sync_mode=log
ovn_l3_mode=True
vif_type=ovs
ovn_metadata_enabled=True
enable_distributed_floating_ip=True
dns_servers=

[network_log]
rate_limit=100
burst_limit=25
```

