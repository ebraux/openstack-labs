
---
## Méthodes de provisionning

Tripleo permet de provisionner :

- Soit des machines en les gérant de bout en bout :
  - Installation du système via IMPI,  boot PXE+DHCP
  - Configuration
  - Et enfin déploiement d'Openstack
- Soit utilisant  des machines pre-installées.
  - Configuration des machines pour correspondre aux egigeance de Tripleo
  - Et déployant Openstack sur

L'utilisation de machine prè-installé peut paraître plus simple et plus souple. Mais elle reste complexe, et surtout est en "technology Preview".


La méthode de provisionning utilisée est donc le déploiement complet des machines via Tripleo/Ironic

Sources : 

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_planning-your-overcloud#Provisioning-methods](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_planning-your-overcloud#Provisioning-methods)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_provisioning-bare-metal-nodes-before-deploying-the-overcloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_provisioning-bare-metal-nodes-before-deploying-the-overcloud)


---
## Choix des rôles

2 types de machine identifiées

- Controller
- Compute, avec l'option DVR activée

Un gros travail d'uniformisation du parc de compute a été réalisé afin de permettre de n'utiliser qu'un seul rôle/profil par 

- Les noeuds "controller" sont identiques
- Les noeuds "compute" disposent de la même configuration réseau, permettant de mettre en place
  - une interface de provisionning
  - 2 bonds, constitué de 2 interfaces réparties sur 2 cartes (NIC2/NIC3, et NIC4/NIC5)

En fonction des configuration la répartition des NICS sur les cartes peut être différente :

- Serveurs HPE 
  - Carte Interne RJ45 : NIC1
  - Carte additionnelle #1 SFP+ : NIC2 et NIC4
  - Carte additionnelle #2 SFP+ : NIC3 et NIC5
- Serveurs DELL SFP+
  - Carte Interne SFP+ : NIC1, NIC2 et NIC4
  - Carte additionnelle #1 SFP+ : NIC3 et NIC5
- Serveurs DELL RJ45
  - Carte Interne RJ45 : NIC1
  - Carte additionnelle #1 SFP+ : NIC2 et NIC4
  - Carte additionnelle #2 SFP+ : NIC3 et NIC5

- Serveur DELL RJ45


---
## configuration réseau




---
## Utilisation ce CEPH

Ceph externe