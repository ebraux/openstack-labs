



Vérifier le déploiement
``` bash
openstack stack list 
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
| ID                                   | Stack Name | Project                          | Stack Status    | Creation Time        | Updated Time         |
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
| 9d28ac6c-79a1-45d8-8829-9e83f17b78f0 | overcloud  | b3fd53a52b8b46139212c7bae5015593 | UPDATE_COMPLETE | 2023-06-26T09:36:03Z | 2023-06-26T13:20:58Z |
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
``` 

Vérifier l'état de toutes les stacks
``` bash
openstack stack list --nested
```


lister les controllers
``` bash
openstack stack resource show overcloud ControllerServers
```