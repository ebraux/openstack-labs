
# Relevé des NICs et MACs des serveurs

---
## Controller #1: 10.29.20.51
Carte fille : Adapter 3 - 10Gb 2-port Base-T BCM57416 OCP3 Adapter
1	00:62:0b:ac:48:b4 | UP | |
2	00:62:0b:ac:48:b5 | UP | |

PCI-Slot1 : Adapter 1 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:ba:3a | DOWN | |
2	30:3e:a7:02:ba:3b | DOWN | |

PCI-Slot2 : Adapter 2 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:ba:2a | UP | |
2	30:3e:a7:02:ba:2b | DOWN | |

---
## Controller #2: 10.29.20.52

Carte fille : Adapter 3 - 10Gb 2-port Base-T BCM57416 OCP3 Adapter
1	00:62:0b:ad:e4:62 | UP | |
2	00:62:0b:ad:e4:63 | DOWN | |

PCI-Slot1 : Adapter 1 - Intel(R) Eth E810-XXVDA2
| 1	| 30:3e:a7:02:a6:58 | UP | |
2	30:3e:a7:02:a6:59 | UP | |

PCI-Slot2 : Adapter 2 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:b7:42 | DOWN | |
2	30:3e:a7:02:b7:43 | DOWN | |

---
## Controller #3: 10.29.20.53

Carte fille : Adapter 3 - 10Gb 2-port Base-T BCM57416 OCP3 Adapter
1	00:62:0b:ad:14:30 | UP | ens10f0np0 |
2	00:62:0b:ad:14:31 | DOWN | ens10f1np1 |

PCI-Slot1 : Adapter 1 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:a5:8a | UP | ens1f0 |
2	30:3e:a7:02:a5:8b | UP | ens1f1 |

PCI-Slot2 : Adapter 2 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:b3:3a | DOWN | ens2f0 |
2	30:3e:a7:02:b3:3b | DOWN | ens2f1 |
