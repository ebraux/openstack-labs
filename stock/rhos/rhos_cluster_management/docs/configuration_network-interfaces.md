# Gestion de la configuration des interfaces réseau

## configuration Réseau

Director va générer la configuration réseau, à partir d'un fichier de template, et de fichier de configuration.

Le fichier de template pa défaut est `/usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml`

le fichier de configuration des `network_data`

Par défaut Director propose des rôles prédéfinis. Les fichiers de rôles sont stockés dans `/usr/share/openstack-tripleo-heat-templates/roles`.


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_basic-network-isolation](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_basic-network-isolation)


### Pour définir une nouvelle template réseau


A faire en tant que 'root'.

Générer les template réseau. : Render the Jinja2 templates with the tools/process-templates.py script, your custom network_data file, and custom roles_data file:


``` bash
cd /usr/share/openstack-tripleo-heat-templates
tools/process-templates.py \
    -n /home/stack/network_data_spine_leaf.yaml \
    -r /home/stack/roles_data_spine_leaf.yaml \
    -o /home/stack/openstack-tripleo-heat-templates-spine-leaf
```
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_custom-network-interface-templates](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_custom-network-interface-templates)

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-custom-nic-configuration](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-custom-nic-configuration)



---
## Forcer la prise en compte des modifications


Forcer le mode de Deploiement à èCREATE` uniquement dans le fichier de templating du réseau
``` bash
parameter_defaults:
  ComputeNetworkDeploymentActions: ['CREATE']
  #ComputeNetworkDeploymentActions: ['CREATE','UPDATE']
```

- [https://access.redhat.com/solutions/2213711?band=se&seSessionId=bc644044-f98f-46d0-ba77-911808a070c5&seSource=Recommendation%20Aside&seResourceOriginID=be0e41d4-2692-4edc-b9e1-9139e8b77f6b](https://access.redhat.com/solutions/2213711?band=se&seSessionId=bc644044-f98f-46d0-ba77-911808a070c5&seSource=Recommendation%20Aside&seResourceOriginID=be0e41d4-2692-4edc-b9e1-9139e8b77f6b)