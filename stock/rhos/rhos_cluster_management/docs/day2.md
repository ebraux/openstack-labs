

Liste les noeuds disponible et leur état :
``` bash
openstack server list
# +--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+
# | UUID                                 | Name            | Instance UUID                        | Power State | Provisioning State | Maintenance |
# +--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+
# | e4f88d1f-d262-4988-9fc4-23a0eb087183 | rhos-ctrl-br-01 | None                                 | power off   | available          | False       |
# | f4c78751-c63e-421a-8eaf-29f11df90753 | rhos-ctrl-br-02 | None                                 | power off   | available          | False       |
# | aee72e41-c6b3-4868-b6ac-69693e371477 | rhos-ctrl-br-03 | ae8cc442-28bb-48ae-a0fa-b6adf2cd2201 | power on    | active             | False       |
# | 7c981c9b-da70-4d61-b1cc-3e0bbe10fcfa | rhos-comp-br-01 | None                                 | power off   | available          | False       |
# | 53fd5c53-a4e2-47bb-8209-0b8633c2998b | rhos-comp-br-02 | None                                 | power off   | available          | False       |
# | ec0d50b3-98a9-4801-a672-b81e03efc7a3 | rhos-comp-br-03 | ec0064e6-bb59-4d3a-abeb-7c31253f1d0a | power on    | active             | False       |
# | 7bf267c6-e996-4c86-b389-0a5aee6d125c | rhos-comp-br-07 | None                                 | power off   | available          | False       |
# | bb62b740-76f3-45c1-a0fc-421663b661cb | rhos-comp-br-08 | None                                 | power off   | available          | False       |
# | 3162ba65-192d-4d74-96d7-792dfafd7ae6 | rhos-comp-br-09 | None                                 | power off   | available          | False       |
# | 72504bcb-11f7-49c3-887e-47fb3612cb93 | rhos-comp-br-10 | None                                 | power off   | available          | False       |
# | ca75c116-ffa9-4475-b2d9-343f77afe84c | rhos-comp-br-12 | None                                 | power on    | clean failed       | True        |
# | 71a03e8c-1fd7-4d91-93f1-37f6952ddcc9 | rhos-comp-br-11 | None                                 | power on    | clean failed       | True        |
# +--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+
```


Lister les instances :
``` bash
openstack server list
# +--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+
# | ID                                   | Name                    | Status | Networks                | Image          | Flavor  |
# +--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+
# | 9ffb1fbf-3fa1-4be6-811f-23b7a0d50a8a | overcloud-controller-0  | ACTIVE | ctlplane=10.129.173.197 | overcloud-full | control |
# | 30b40e9d-1da5-4cdf-b861-dd8ffc5d1db0 | overcloud-novacompute-0 | ACTIVE | ctlplane=10.129.173.165 | overcloud-full | compute |
# +--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+
```

Se connecter aux instances : utiliser l'utilisateur `heat-admin`
``` bash
ssh heat-admin@10.129.173.197
```


---
## Fichiers de config

Ils sont dans `/var/lib/kolla/config_files`

POur savoir ceux qui sont utilisés, faire un `podman inspect` sur un conteneur


---
## ha proxy 

Ecoute sur les IP :

- interne : 192.168.174
- externe :  10.129.176.9

Et redirige ver l'IP du noeud, sur le réseau internal API 192.168.174.51

Ports d'écoute :

- cinder: 8776
- glance_api: 9292
- heat_api : 8004
- heat_cfn : 8000
- horizon : 80
- keystone_public : 5000
- neutron 9696
- nova_novncproxy : 6080
- nova_osapi : 8774
- placement : 8778
- swift_proxy_server : 8080
- uniquement sur internal  
  - mysql : 3306
  - nova_metadata : 8775
  - redis : 6379
- uniquement sur external
  - keystone_admin : 35357 (redirige vers le port 5000)
Et en plus : 

- haproxy.stats : 1993


test :
``` bash
curl http://10.129.176.9:5000 
{"versions": {"values": [{"id": "v3.13", "status": "stable", "updated": "2019-07-19T00:00:00Z", "links": [{"rel": "self", "href": "http://10.129.176.9:5000/v3/"}], "media-types": [{"base": "application/json", "type": "application/vnd.openstack.identity-v3+json"}]}]}}
```

``` bash
curl http://10.129.176.9:80
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://10.129.176.9/dashboard">here</a>.</p>
</body></html>
```

depuis un compute, fonctionne avec les IP internalAPI
``` bash
curl http://192.168.174.9:5000
curl http://192.168.174.9:80
```



---
## vérification du réseau

Afficher les interfaces d'un noeud : 
``` bash
openstack baremetal introspection interface list ${NODE}
```


---
## debug déploiement


- [https://slagle.fedorapeople.org/tripleo-docs/install/troubleshooting/troubleshooting-overcloud.html](https://slagle.fedorapeople.org/tripleo-docs/install/troubleshooting/troubleshooting-overcloud.html)