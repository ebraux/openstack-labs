# Préparation pour déploiement du cluster Openstack

---
## Mise en oeuvre

### Préparation d'Ansible et de l' "automation"

- Initialisation d'Ansible
``` bash
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
ansible-galaxy install -r requirements.yml
```
- Préparation au déploiement
Initialisation d'une paire de clé ssh utilisée dans le cluster
``` bash
mkdir keys
ssh-keygen -t ed25519 -C "RH-clusters-key" -f ./secret/cluster-key
```
- Création de l'utilisateur "d'automation" sur les noeuds, et et déploiement de la clé
``` bash
ansible-playbook cluster-enable-automation.yml --user root --ask-pass
```

### Préparation des machines pour le déploiement du cluster

- Post-installation  standard (enregistrement, configuration, ...)
``` bash
ansible-playbook cluster-post-install.yml
```
- Reconfiguration des disques (!! à lancer une fois car non idempotent !!)
``` bash
# ansible-playbook oneshot-storage-config.yml --user root --askpass
``` 
- Configuration du réseau : fait la configuration, mais ne corrige pas une configuration pré-existante
``` bash
ansible-playbook cluster-network-config.yml
```
- Préparation du cluster pour le déploiement
``` bash
ansible-playbook cluster-prepare.yml
```



---
## Description des opréations

### Création de l'utilisateur "d'automation"

### Installation des prérequis

Mise à jour du systeme, et installation des paquages
- tar : necessaire pour le module "unarchive", utilisé pour la reconfiguartion des disques, 
- lvm2
- podman


### Mise en place de clés SSH communes

Pour la gestion des noeuds, la machine sur laquelle est déployé "cephadmin" doit pouvoir se connecter en SSH à tous les noeuds du cluster, pour les initialiser, les gérers, ...
La doc standard prévoit de faire le bootstarp du cluster, de récupéer les clés, et de les déployer manuellement sur chaque noeud ensuite, au moment de l'intégeration au cluster.
Il y a une option du "bootstrap" qui permet de fournir ces clés.

Le choix a donc été fait de permettre au compte root de se connecter à toutes les machines du cluster, depuis toutes les machines du cluster :
- Créer les clés en amont, ainsi que le fichier de ssh_config, et les placer dans le dossier 'files/ceph-ssh-config
    - cephadm_private_key
    - cephadm_public_key
    - cephadm_ssh_config
- Les déployer sur tous les noeuds du cluster
    - en tant que clé par défaut pour l'utilisateur root
    - en tant que clé autorisée pour l'utilisateur root
- Les déployer sur le neoud utilisé pour le bootstrp, dans le dossier '/root/ceph-ssh-config'  (déployé sur tous les noeuds pour simplifier)    
- Les utiliser lors du lancement de la commande `bootstrap`

### Authentification auprès de la registry pour podman

Ajout des crédentials dans le dossier '/run/user/0/containers/auth.json'



