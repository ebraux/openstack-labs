#!/bin/bash
#set -xe

source ~/stackrc

openstack overcloud deploy  \
  --timeout 360 \
  --verbose \
  -r /home/stack/roles_data.yaml \
  -n /home/stack/network_data.yaml \
  --answers-file /home/stack/templates/answers.yaml \
  --log-file /home/stack/overcloudDeploy.log\
  --ntp-server 10.129.173.14

