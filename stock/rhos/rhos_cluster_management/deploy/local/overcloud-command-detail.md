
``` bash
sudo --preserve-env openstack tripleo deploy \
  --standalone \
  --standalone-role 'Undercloud' \
  --stack 'undercloud' \
  --local-domain=localdomain' \
  --local-ip=10.129.173.16/24' \
  , '--templates=/usr/share/openstack-tripleo-heat-templates/', '--networks-file=network_data_undercloud.yaml', '--upgrade', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/lifecycle/undercloud-upgrade-prepare.yaml', '--heat-native', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/undercloud.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/use-dns-for-vips.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/podman.yaml', '-e', '/home/stack/containers-prepare-parameter.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/masquerade-networks.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/ironic.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/ironic-inspector.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/mistral.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/zaqar-swift-backend.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/disable-telemetry.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/tempest.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/public-tls-undercloud.yaml', '--public-virtual-ip', '10.129.173.16', '--control-virtual-ip', '10.129.173.17', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/ssl/tls-endpoints-public-ip.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/undercloud-haproxy.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/undercloud-keepalived.yaml', '--deployment-user', 'stack', '--output-dir=/home/stack/output', '-e', '/home/stack/output/tripleo-config-generated-env-files/undercloud_parameters.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/tripleo-validations.yaml', '-e', '/home/stack/templates/custom-undercloud-params.yaml', '--log-file=install-undercloud.log', '-e', '/usr/share/openstack-tripleo-heat-templates/undercloud-stack-vstate-dropin.yaml']' returned non-zero exit status 1.
Command '['sudo', '--preserve-env', 'openstack', 'tripleo', 'deploy', '--standalone', '--standalone-role', 'Undercloud', '--stack', 'undercloud', '--local-domain=localdomain', '--local-ip=10.129.173.16/24', '--templates=/usr/share/openstack-tripleo-heat-templates/', '--networks-file=network_data_undercloud.yaml', '--upgrade', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/lifecycle/undercloud-upgrade-prepare.yaml', '--heat-native', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/undercloud.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/use-dns-for-vips.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/podman.yaml', '-e', '/home/stack/containers-prepare-parameter.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/masquerade-networks.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/ironic.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/ironic-inspector.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/mistral.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/zaqar-swift-backend.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/disable-telemetry.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/tempest.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/public-tls-undercloud.yaml', '--public-virtual-ip', '10.129.173.16', '--control-virtual-ip', '10.129.173.17', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/ssl/tls-endpoints-public-ip.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/undercloud-haproxy.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/services/undercloud-keepalived.yaml', '--deployment-user', 'stack', '--output-dir=/home/stack/output', '-e', '/home/stack/output/tripleo-config-generated-env-files/undercloud_parameters.yaml', '-e', '/usr/share/openstack-tripleo-heat-templates/environments/tripleo-validations.yaml', '-e', '/home/stack/templates/custom-undercloud-params.yaml', '--log-file=install-undercloud.log', '-e', '/usr/share/openstack-tripleo-heat-templates/undercloud-stack-vstate-dropin.yaml']' returned non-zero exit status 1.
```

``` bash
690c02f4583e  rhos-director-br-01.ctlplane.localdomain:8787/rhosp-rhel8/openstack-heat-engine:16.2  /usr/bin/bootstra...  18 minutes ago  Up 18 minutes ago          heat_engine_db_sync


2023-06-10 13:15:40.594 12 WARNING oslo_db.sqlalchemy.engines [-] SQL connection failed. -84 attempts left.: oslo_db.exception.DBConnectionError: (pymysql.err.OperationalError) (2003, "Can't connect to MySQL server on '10.129.173.17' ([Errno 113] No route to host)")
2023-06-10 13:15:53.713 12 WARNING oslo_db.sqlalchemy.engines [-] SQL connection failed. -85 attempts left.: oslo_db.exception.DBConnectionError: (pymysql.err.OperationalError) (2003, "Can't connect to MySQL server on '10.129.173.17' ([Errno 113] No route to host)")
2023-06-10 13:16:06.833 12 WARNING oslo_db.sqlalchemy.engines [-] SQL connection failed. -86 attempts left.: oslo_db.exception.DBConnectionError: (pymysql.err.OperationalError) (2003, "Can't connect to MySQL server on '10.129.173.17' ([Errno 113] No route to host)")
2023-06-10 13:16:19.953 12 WARNING oslo_db.sqlalchemy.engines [-] SQL connection failed. -87 attempts left.: oslo_db.exception.DBConnectionError: (pymysql.err.OperationalError) (2003, "Can't connect to MySQL server on '10.129.173.17' ([Errno 113] No route to host)")

```


``` bash
# 2023-06-09 15:17:28.354991 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Summary Information ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2023-06-09 15:17:28.355209 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Total Tasks: 308        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2023-06-09 15:17:28.355361 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Elapsed Time: 0:07:35.673232 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2023-06-09 15:17:28.355489 |                                 UUID |       Info |       Host |   Task Name |   Run Time
# 2023-06-09 15:17:28.355629 | 52540032-1894-29f4-5aa9-000000000e9e |    SUMMARY | rhos-director-br-01 | chrony : Ensure system is NTP time synced | 133.27s
# 2023-06-09 15:17:28.355768 | 52540032-1894-29f4-5aa9-000000001135 |    SUMMARY | rhos-director-br-01 | Run tripleo-container-image-prepare logged to: /var/log/tripleo-container-image-prepare.log | 26.79s
# 2023-06-09 15:17:28.355892 | 52540032-1894-29f4-5aa9-00000000069b |    SUMMARY | rhos-director-br-01 | tripleo-image-serve : ensure apache is installed | 14.32s
# 2023-06-09 15:17:28.356013 | 52540032-1894-29f4-5aa9-0000000010a4 |    SUMMARY | rhos-director-br-01 | Install tuned | 11.55s
# 2023-06-09 15:17:28.356152 | 52540032-1894-29f4-5aa9-000000000050 |    SUMMARY | rhos-director-br-01 | tripleo-network-config : Run NetworkConfig script | 10.85s
# 2023-06-09 15:17:28.356286 | 52540032-1894-29f4-5aa9-000000000575 |    SUMMARY | rhos-director-br-01 | authorize httpd to listen on registry ports | 9.48s
# 2023-06-09 15:17:28.356409 | 52540032-1894-29f4-5aa9-0000000001b3 |    SUMMARY | rhos-director-br-01 | tripleo-bootstrap : Deploy required packages to bootstrap TripleO | 8.74s
# 2023-06-09 15:17:28.356535 | 52540032-1894-29f4-5aa9-000000000814 |    SUMMARY | rhos-director-br-01 | tripleo-kernel : Remove dracut-config-generic | 8.66s
# 2023-06-09 15:17:28.356665 | 52540032-1894-29f4-5aa9-00000000064d |    SUMMARY | rhos-director-br-01 | install tmpwatch on the host | 8.62s
# 2023-06-09 15:17:28.356787 | 52540032-1894-29f4-5aa9-0000000001b7 |    SUMMARY | rhos-director-br-01 | tripleo-bootstrap : Deploy network-scripts required for deprecated network service | 8.59s
# 2023-06-09 15:17:28.356905 | 52540032-1894-29f4-5aa9-000000000c20 |    SUMMARY | rhos-director-br-01 | Install chronyd package | 8.58s
# 2023-06-09 15:17:28.357036 | 52540032-1894-29f4-5aa9-0000000009b0 |    SUMMARY | rhos-director-br-01 | tripleo-podman : ensure podman and deps are installed | 8.57s
# 2023-06-09 15:17:28.357165 | 52540032-1894-29f4-5aa9-000000000639 |    SUMMARY | rhos-director-br-01 | Ensure rsyslog is installed | 8.57s
# 2023-06-09 15:17:28.357288 | 52540032-1894-29f4-5aa9-000000000326 |    SUMMARY | rhos-director-br-01 | tripleo-validations-package : Install tripleo-validations package | 8.53s
# 2023-06-09 15:17:28.357406 | 52540032-1894-29f4-5aa9-0000000004e9 |    SUMMARY | rhos-director-br-01 | tripleo-hieradata : Render hieradata from template | 7.12s
# 2023-06-09 15:17:28.357526 | 52540032-1894-29f4-5aa9-000000000398 |    SUMMARY | rhos-director-br-01 | tripleo-hieradata : Render hieradata from template | 7.11s
# 2023-06-09 15:17:28.357649 | 52540032-1894-29f4-5aa9-000000000819 |    SUMMARY | rhos-director-br-01 | tripleo-kernel : Set extra sysctl options | 5.19s
# 2023-06-09 15:17:28.357778 | 52540032-1894-29f4-5aa9-00000000046e |    SUMMARY | rhos-director-br-01 | tripleo_nodes_validation : Check IP responsiveness | 4.50s
# 2023-06-09 15:17:28.357896 | 52540032-1894-29f4-5aa9-000000000446 |    SUMMARY | rhos-director-br-01 | tripleo_nodes_validation : Check Default IPv4 Gateway availability | 4.45s
# 2023-06-09 15:17:28.358016 | 52540032-1894-29f4-5aa9-000000000574 |    SUMMARY | rhos-director-br-01 | allow logrotate to read inside containers | 3.88s
# 2023-06-09 15:17:28.358152 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ End Summary Information ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

```


``` bash
2023-06-09 15:17:28.358306 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ State Information ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
2023-06-09 15:17:28.358435 | ~~~~~~~~~~~~~~~~~~ Number of nodes which did not deploy successfully: 1 ~~~~~~~~~~~~~~~~~
2023-06-09 15:17:28.358555 |  The following node(s) had failures: rhos-director-br-01
2023-06-09 15:17:28.358680 | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Exception: Deployment failed
Traceback (most recent call last):
  File "/usr/lib/python3.6/site-packages/tripleoclient/v1/tripleo_deploy.py", line 1360, in _standalone_deploy
    raise exceptions.DeploymentError('Deployment failed')
tripleoclient.exceptions.DeploymentError: Deployment failed
None
Not cleaning working directory /home/stack/output/tripleo-heat-installer-templates
Not cleaning ansible directory /home/stack/output/undercloud-ansible-ic21igcd
Install artifact is located at /home/stack/output/undercloud-install-20230609131728.tar.bzip2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Deployment Failed!

ERROR: Heat log files: /var/log/heat-launcher/undercloud_deploy-_jmy3bd8
``` 



``` bash
2023-06-09 16:06:13.217152 | 52540032-1894-e0af-c753-000000000052 |       TASK | NetworkConfig stdout
2023-06-09 16:06:13.325368 | 52540032-1894-e0af-c753-000000000052 |         OK | NetworkConfig stdout | rhos-director-br-01 | result={
    "NetworkConfig_result.stderr_lines": [
        "+ '[' -n '{\"network_config\": [{\"addresses\": [{\"ip_netmask\": \"10.129.173.16/24\"}], \"dns_servers\": [\"192.44.75.10\", \"192.108.115.2\"], \"domain\": [], \"members\": [{\"mtu\": 1500, \"name\": \"interface_name\", \"primary\": true, \"type\": \"interface\"}], \"name\": \"br-ctlplane\", \"ovs_extra\": [\"br-set-external-id br-ctlplane bridge-id br-ctlplane\"], \"routes\": [], \"type\": \"ovs_bridge\", \"use_dhcp\": false}]}' ']'",
        "+ '[' -z '' ']'",
        "+ trap configure_safe_defaults EXIT",
        "++ date +%Y-%m-%dT%H:%M:%S",
        "+ DATETIME=2023-06-09T16:06:09",
        "+ '[' -f /etc/os-net-config/config.json ']'",
        "+ mv /etc/os-net-config/config.json /etc/os-net-config/config.json.2023-06-09T16:06:09",
        "+ mkdir -p /etc/os-net-config",
        "+ echo '{\"network_config\": [{\"addresses\": [{\"ip_netmask\": \"10.129.173.16/24\"}], \"dns_servers\": [\"192.44.75.10\", \"192.108.115.2\"], \"domain\": [], \"members\": [{\"mtu\": 1500, \"name\": \"interface_name\", \"primary\": true, \"type\": \"interface\"}], \"name\": \"br-ctlplane\", \"ovs_extra\": [\"br-set-external-id br-ctlplane bridge-id br-ctlplane\"], \"routes\": [], \"type\": \"ovs_bridge\", \"use_dhcp\": false}]}'",
        "++ type -t network_config_hook",
        "+ '[' '' = function ']'",
        "+ sed -i 's/: \"bridge_name/: \"br-ex/g' /etc/os-net-config/config.json",
        "+ sed -i s/interface_name/enp8s0/g /etc/os-net-config/config.json",
        "+ set +e",
        "+ os-net-config -c /etc/os-net-config/config.json -v --detailed-exit-codes",
        "[2023/06/09 04:06:10 PM] [INFO] Using config file at: /etc/os-net-config/config.json",
        "[2023/06/09 04:06:10 PM] [INFO] Ifcfg net config provider created.",
        "[2023/06/09 04:06:10 PM] [INFO] Not using any mapping file.",
        "[2023/06/09 04:06:11 PM] [INFO] Finding active nics",
        "[2023/06/09 04:06:11 PM] [INFO] enp8s0 is an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] br-ctlplane is not an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] lo is not an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] enp1s0 is an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] virbr0 is not an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] enp7s0 is an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] ovs-system is not an active nic",
        "[2023/06/09 04:06:11 PM] [INFO] No DPDK mapping available in path (/var/lib/os-net-config/dpdk_mapping.yaml)",
        "[2023/06/09 04:06:11 PM] [INFO] Active nics are ['enp1s0', 'enp7s0', 'enp8s0']",
        "[2023/06/09 04:06:11 PM] [INFO] nic2 mapped to: enp7s0",
        "[2023/06/09 04:06:11 PM] [INFO] nic3 mapped to: enp8s0",
        "[2023/06/09 04:06:11 PM] [INFO] nic1 mapped to: enp1s0",
        "[2023/06/09 04:06:11 PM] [INFO] adding bridge: br-ctlplane",
        "[2023/06/09 04:06:11 PM] [INFO] adding interface: enp8s0",
        "[2023/06/09 04:06:11 PM] [INFO] applying network configs...",
        "[2023/06/09 04:06:11 PM] [INFO] No changes required for interface: enp8s0",
        "[2023/06/09 04:06:11 PM] [INFO] No changes required for bridge: br-ctlplane",
        "+ RETVAL=0",
        "+ set -e",
        "+ [[ 0 == 2 ]]",
        "+ [[ 0 != 0 ]]",
        "+ '[' -f /usr/libexec/os-apply-config/templates/etc/os-net-config/config.json ']'",
        "+ '[' -f /usr/libexec/os-apply-config/templates/etc/os-net-config/element_config.json ']'",
        "+ configure_safe_defaults",
        "+ [[ 0 == 0 ]]",
        "+ return 0"
    ],
    "changed": false,
    "failed_when_result": false
}
2023-06-09 16:06:13.327203 | 52540032-1894-e0af-c753-000000000052 |     T
```

