# Operation sur openstack

---
## Perimetre

Configuration fonctionelle d el'instance Openstack-dfvs :

- Getsion des images, gabarits, ....
- Création de utilisateurs, projets, ...


---
## Initialisation de l'environnement

`git clone git@gitlab-disi.imt-atlantique.fr:openstack/os-operation.git`

Installation de outils nécessaires (Client Opensatck, Ansible, ...) : [Environnement](./docs/environment.md)


---
## Operations de getsion des comptes et projets

Création d'un projet
``` bash
ansible-playbook playbooks/project_config.yml -e '@vars/projects/ebraux_perso.yml'
```

Création d'un lab
``` bash
ansible-playbook playbooks/lab_config.yml -e '@vars/labs/202111_orange-dtsi.yml' 
```


```

