
# Initialisation de l'infra

---
## création des ressources

Gestion des Gabarits
``` bash
ansible-playbook playbooks/config_flavors.yml
```

Gestion des Images
``` bash
ansible-playbook playbooks/config_images.yml
```


---
## réseau 'external'

---
Le réseau "provider" par défaut c'est `datacentre`

/etc/neutron/plugin.ini
[ml2_type_flat]
flat_networks=datacentre

/etc/neutron/plugins/ml2/ml2_conf.ini
[ml2_type_flat]
flat_networks=datacentre :flat_networks=datacentre



``` bash
#!/bin/bash
set -ex

# creation de external si il n'existe pas
#  --provider-network-type flat \

openstack network show external || openstack network create \
  --provider-network-type vlan \
  --provider-segment 178 \
  --provider-physical-network datacentre \
  --external \
  --share \
  external



# set default gateway via any non-master/worker node to get into ingress controller
openstack subnet show external-subnet || openstack subnet create  \
  --subnet-range  10.129.176.0/22 \
  --allocation-pool start=10.129.176.99,end=10.129.179.249 \
  --gateway 10.129.176.1 \
  --network external \
  external-subnet
```


