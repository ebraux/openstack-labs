

``` bash
export PROJECT_TO_BE_DELETED=fila2-devops-03

echo " Suppression de ${PROJECT_TO_BE_DELETED}" \
 && openstack role add --user admin --project ${PROJECT_TO_BE_DELETED} admin \
 && openstack project cleanup --project  ${PROJECT_TO_BE_DELETED} \
 && openstack project purge --project ${PROJECT_TO_BE_DELETED}
```


