
# Validation de l'infra



- [https://ebraux.gitlab.io/openstack-labs-installation/04_demo/01-demo/](https://ebraux.gitlab.io/openstack-labs-installation/04_demo/01-demo/)

---
## Le réseau

### Ping du routeur
``` bash
# configuration
PROJECT_NAME=admin
NETWORK_NAME=net-${PROJECT_NAME}
SUBNET_NAME=subnet-${PROJECT_NAME}
ROUTER_NAME=router-${PROJECT_NAME}

EXTERNAL_NETWORK_NAME=external
# ->  defini dans la config de neutron

# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 8.8.4.4 --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

openstack router show  ${ROUTER_NAME}

# recupérer l'IP externe du routeur
openstack router show  ${ROUTER_NAME} -c "external_gateway_info"
openstack router show  ${ROUTER_NAME} -f json  -c "external_gateway_info" | jq '.external_gateway_info.external_fixed_ips[].ip_address' 

# faire un ping sur l'IP
```



### Lancement d'une instance

``` bash
# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list -f value -c Name | grep  cirros)
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list -f value -c Name | grep 'cirros')
echo ${IMAGE_CIRROS_ID}


# lancement d'une instance
# creation d'un instance de test
TEST_INSTANCE_NAME=${PROJECT_NAME}Test
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --nic net-id=${NETWORK_NAME} \
   --security-group ${SECGROUP_ADMIN} \
   ${TEST_INSTANCE_NAME}


# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=$(openstack floating ip list -f value -c 'Floating IP Address' -c 'Fixed IP Address' | grep None | head -1 | awk '{ print $1 }')
echo ${FREE_FLOATING_IP}
openstack server add floating ip ${TEST_INSTANCE_NAME} ${FREE_FLOATING_IP}
```

```bash
ping ${FREE_FLOATING_IP}

ssh cirros@${FREE_FLOATING_IP}
# gocubsgo
```



heat-admin@overcloud-controller-0 ~]$ sudo podman ps -a --format="{{.Names}}"|grep ovn
ovn-dbs-bundle-podman-0
ovn_dbs_init_bundle
ovn_controller


sudo  grep -r ERR /var/log/containers/openvswitch/ /var/log/containers/neutron/
/var/log/containers/neutron/server.log:2023-06-30 08:41:17.868 7 DEBUG neutron.common.config [-] logging_exception_prefix       = %(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s log_opt_values /usr/lib/python3.6/site-packages/oslo_config/cfg.py:2581
/var/log/containers/neutron/server.log:2023-06-30 08:41:18.650 7 DEBUG oslo_db.sqlalchemy.engines [req-a2cd88c3-2dea-40e0-8b26-c041c7ab8a8c - - - - -] MySQL server mode set to STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION _check_effective_sql_mode /usr/lib/python3.6/site-packages/oslo_db/sqlalchemy/engines.py:307
/var/log/containers/neutron/server.log:2023-06-30 08:41:24.234 7 DEBUG oslo_service.service [-] logging_exception_prefix       = %(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s log_opt_values /usr/lib/python3.6/site-packages/oslo_config/cfg.py:2581
/var/log/containers/neutron/server.log:2023-06-30 08:41:24.270 7 DEBUG oslo_service.service [-] logging_exception_prefix       = %(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s log_opt_values /usr/lib/python3.6/site-packages/oslo_config/cfg.py:2581


sudo ip netns list
--> rien



[heat-admin@overcloud-novacompute-0 ~] sudo podman ps -a --format="{{.Names}}"|grep ovn
ovn_controller
ovn_metadata_agent

[heat-admin@overcloud-novacompute-0 ~] sudo  grep -r ERR /var/log/containers/openvswitch/ /var/log/containers/neutron/
/var/log/containers/neutron/ovn-metadata-agent.log:2023-06-30 08:41:14.820 32617 DEBUG networking_ovn.agent.metadata_agent [-] logging_exception_prefix       = %(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s log_opt_values /usr/lib/python3.6/site-packages/oslo_config/cfg.py:2581
/var/log/containers/neutron/ovn-metadata-agent.log:2023-06-30 08:41:15.359 32716 INFO oslo.privsep.daemon [-] privsep process running with capabilities (eff/prm/inh): CAP_DAC_OVERRIDE|CAP_DAC_READ_SEARCH|CAP_NET_ADMIN|CAP_SYS_ADMIN|CAP_SYS_PTRACE/CAP_DAC_OVERRIDE|CAP_DAC_READ_SEARCH|CAP_NET_ADMIN|CAP_SYS_ADMIN|CAP_SYS_PTRACE/none
/var/log/containers/neutron/ovn-metadata-agent.log:2023-06-30 08:41:15.734 32617 DEBUG oslo_service.service [-] logging_exception_prefix       = %(asctime)s.%(msecs)03d %(process)d ERROR %(name)s %(instance)s log_opt_values /usr/lib/python3.6/site-packages/oslo_config/cfg.py:2581




openstack router show router-external || openstack router create router-external
openstack router set --external-gateway external router-external
openstack network set external --external  --default 
```


---
sur controller :

interfaces :
2: ens10f0np0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:62:0b:ad:e4:62 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.51/24 brd 10.129.173.255 scope global dynamic noprefixroute ens10f0np0

3: ens1f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether 30:3e:a7:02:a6:58 brd ff:ff:ff:ff:ff:ff

4: ens10f1np1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 00:62:0b:ad:e4:63 brd ff:ff:ff:ff:ff:ff
5: ens1f1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 30:3e:a7:02:a6:59 brd ff:ff:ff:ff:ff:ff
6: ens2f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 30:3e:a7:02:b7:42 brd ff:ff:ff:ff:ff:ff
7: ens2f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 30:3e:a7:02:b7:43 brd ff:ff:ff:ff:ff:ff


8: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0e:8f:ab:91:36:62 brd ff:ff:ff:ff:ff:ff

9: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 30:3e:a7:02:a6:58 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.51/24 brd 10.129.173.255 scope global br-ex
15: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 52:fb:7a:fb:58:82 brd ff:ff:ff:ff:ff:ff
16: genev_sys_6081: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN group default qlen 1000
    link/ether b6:cc:50:77:91:da brd ff:ff:ff:ff:ff:ff



10: vlan175: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6e:42:18:89:01:16 brd ff:ff:ff:ff:ff:ff
    inet 192.168.175.51/24 brd 192.168.175.255 scope global vlan175
11: vlan177: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6a:f4:4f:26:5b:15 brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.51/24 brd 192.168.177.255 scope global vlan177
12: vlan176: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 1e:10:8c:9a:8f:53 brd ff:ff:ff:ff:ff:ff
    inet 192.168.176.51/24 brd 192.168.176.255 scope global vlan176
13: vlan174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 46:56:5c:9c:62:2a brd ff:ff:ff:ff:ff:ff
    inet 192.168.174.51/24 brd 192.168.174.255 scope global vlan174
    inet 192.168.174.9/32 brd 192.168.174.255 scope global vlan174
    inet 192.168.174.7/32 brd 192.168.174.255 scope global vlan174
14: vlan178: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 46:2f:25:4c:1a:fb brd ff:ff:ff:ff:ff:ff
    inet 10.129.176.51/22 brd 10.129.179.255 scope global vlan178


```
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens10f0np0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:62:0b:ad:e4:62 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.51/24 brd 10.129.173.255 scope global dynamic noprefixroute ens10f0np0
       valid_lft 82256sec preferred_lft 82256sec
    inet 10.129.173.9/32 brd 10.129.173.255 scope global ens10f0np0
       valid_lft forever preferred_lft forever
    inet6 fe80::262:bff:fead:e462/64 scope link 
       valid_lft forever preferred_lft forever
3: ens1f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether 30:3e:a7:02:a6:58 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::323e:a7ff:fe02:a658/64 scope link 
       valid_lft forever preferred_lft forever
4: ens10f1np1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 00:62:0b:ad:e4:63 brd ff:ff:ff:ff:ff:ff
5: ens1f1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 30:3e:a7:02:a6:59 brd ff:ff:ff:ff:ff:ff
6: ens2f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 30:3e:a7:02:b7:42 brd ff:ff:ff:ff:ff:ff
7: ens2f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 30:3e:a7:02:b7:43 brd ff:ff:ff:ff:ff:ff
8: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0e:8f:ab:91:36:62 brd ff:ff:ff:ff:ff:ff
9: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 30:3e:a7:02:a6:58 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.51/24 brd 10.129.173.255 scope global br-ex
       valid_lft forever preferred_lft forever
    inet6 fe80::323e:a7ff:fe02:a658/64 scope link 
       valid_lft forever preferred_lft forever
10: vlan175: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6e:42:18:89:01:16 brd ff:ff:ff:ff:ff:ff
    inet 192.168.175.51/24 brd 192.168.175.255 scope global vlan175
       valid_lft forever preferred_lft forever
    inet6 fe80::6c42:18ff:fe89:116/64 scope link 
       valid_lft forever preferred_lft forever
11: vlan177: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6a:f4:4f:26:5b:15 brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.51/24 brd 192.168.177.255 scope global vlan177
       valid_lft forever preferred_lft forever
    inet6 fe80::68f4:4fff:fe26:5b15/64 scope link 
       valid_lft forever preferred_lft forever
12: vlan176: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 1e:10:8c:9a:8f:53 brd ff:ff:ff:ff:ff:ff
    inet 192.168.176.51/24 brd 192.168.176.255 scope global vlan176
       valid_lft forever preferred_lft forever
    inet6 fe80::1c10:8cff:fe9a:8f53/64 scope link 
       valid_lft forever preferred_lft forever
13: vlan174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 46:56:5c:9c:62:2a brd ff:ff:ff:ff:ff:ff
    inet 192.168.174.51/24 brd 192.168.174.255 scope global vlan174
       valid_lft forever preferred_lft forever
    inet 192.168.174.9/32 brd 192.168.174.255 scope global vlan174
       valid_lft forever preferred_lft forever
    inet 192.168.174.7/32 brd 192.168.174.255 scope global vlan174
       valid_lft forever preferred_lft forever
    inet6 fe80::4456:5cff:fe9c:622a/64 scope link 
       valid_lft forever preferred_lft forever
14: vlan178: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 46:2f:25:4c:1a:fb brd ff:ff:ff:ff:ff:ff
    inet 10.129.176.51/22 brd 10.129.179.255 scope global vlan178
       valid_lft forever preferred_lft forever
    inet6 fe80::442f:25ff:fe4c:1afb/64 scope link 
       valid_lft forever preferred_lft forever
15: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 52:fb:7a:fb:58:82 brd ff:ff:ff:ff:ff:ff
16: genev_sys_6081: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN group default qlen 1000
    link/ether b6:cc:50:77:91:da brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b4cc:50ff:fe77:91da/64 scope link 
       valid_lft forever preferred_lft forever
```

--
## sur compute


2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:50 brd ff:ff:ff:ff:ff:ff
3: eno2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:51 brd ff:ff:ff:ff:ff:ff
4: eno3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:52 brd ff:ff:ff:ff:ff:ff
5: eno4: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether e4:43:4b:ea:67:53 brd ff:ff:ff:ff:ff:ff
6: ens1f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether f8:f2:1e:aa:be:80 brd ff:ff:ff:ff:ff:ff
7: ens1f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether f8:f2:1e:aa:be:81 brd ff:ff:ff:ff:ff:ff

8: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ae:4c:08:1e:dc:8d brd ff:ff:ff:ff:ff:ff
9: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether e4:43:4b:ea:67:50 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.101/24 brd 10.129.173.255 scope global br-ex
15: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ae:dd:e0:cc:e7:d6 brd ff:ff:ff:ff:ff:ff
16: genev_sys_6081: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN group default qlen 1000
    link/ether b2:0c:66:ab:73:8d brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b00c:66ff:feab:738d/64 scope link 


10: vlan174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 76:d6:c8:93:83:b7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.174.101/24 brd 192.168.174.255 scope global vlan174
11: vlan178: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether ca:49:07:80:b8:39 brd ff:ff:ff:ff:ff:ff
    inet 10.129.179.38/22 brd 10.129.179.255 scope global vlan178
12: vlan175: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 0a:77:22:87:ac:e8 brd ff:ff:ff:ff:ff:ff
    inet 192.168.175.101/24 brd 192.168.175.255 scope global vlan175
13: vlan177: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether ee:ed:2d:3c:9a:fb brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.101/24 brd 192.168.177.255 scope global vlan177
14: vlan176: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6e:24:cf:94:92:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.176.101/24 brd 192.168.176.255 scope global vlan176



```
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:50 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::e643:4bff:feea:6750/64 scope link 
       valid_lft forever preferred_lft forever
3: eno2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:51 brd ff:ff:ff:ff:ff:ff
4: eno3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether e4:43:4b:ea:67:52 brd ff:ff:ff:ff:ff:ff
5: eno4: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether e4:43:4b:ea:67:53 brd ff:ff:ff:ff:ff:ff
6: ens1f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether f8:f2:1e:aa:be:80 brd ff:ff:ff:ff:ff:ff
7: ens1f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether f8:f2:1e:aa:be:81 brd ff:ff:ff:ff:ff:ff
8: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ae:4c:08:1e:dc:8d brd ff:ff:ff:ff:ff:ff
9: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether e4:43:4b:ea:67:50 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.101/24 brd 10.129.173.255 scope global br-ex
       valid_lft forever preferred_lft forever
    inet6 fe80::e643:4bff:feea:6750/64 scope link 
       valid_lft forever preferred_lft forever
10: vlan174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 76:d6:c8:93:83:b7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.174.101/24 brd 192.168.174.255 scope global vlan174
       valid_lft forever preferred_lft forever
    inet6 fe80::74d6:c8ff:fe93:83b7/64 scope link 
       valid_lft forever preferred_lft forever
11: vlan178: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether ca:49:07:80:b8:39 brd ff:ff:ff:ff:ff:ff
    inet 10.129.179.38/22 brd 10.129.179.255 scope global vlan178
       valid_lft forever preferred_lft forever
    inet6 fe80::c849:7ff:fe80:b839/64 scope link 
       valid_lft forever preferred_lft forever
12: vlan175: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 0a:77:22:87:ac:e8 brd ff:ff:ff:ff:ff:ff
    inet 192.168.175.101/24 brd 192.168.175.255 scope global vlan175
       valid_lft forever preferred_lft forever
    inet6 fe80::877:22ff:fe87:ace8/64 scope link 
       valid_lft forever preferred_lft forever
13: vlan177: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether ee:ed:2d:3c:9a:fb brd ff:ff:ff:ff:ff:ff
    inet 192.168.177.101/24 brd 192.168.177.255 scope global vlan177
       valid_lft forever preferred_lft forever
    inet6 fe80::eced:2dff:fe3c:9afb/64 scope link 
       valid_lft forever preferred_lft forever
14: vlan176: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 6e:24:cf:94:92:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.176.101/24 brd 192.168.176.255 scope global vlan176
       valid_lft forever preferred_lft forever
    inet6 fe80::6c24:cfff:fe94:927f/64 scope link 
       valid_lft forever preferred_lft forever
15: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ae:dd:e0:cc:e7:d6 brd ff:ff:ff:ff:ff:ff
16: genev_sys_6081: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN group default qlen 1000
    link/ether b2:0c:66:ab:73:8d brd ff:ff:ff:ff:ff:ff
    inet6 fe80::b00c:66ff:feab:738d/64 scope link 
       valid_lft forever preferred_lft forever

```
