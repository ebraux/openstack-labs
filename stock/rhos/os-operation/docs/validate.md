
# Validation du déploiment.

Après la phase d'initilisation :

- du réseau external
- des gabarits
- des images

Et la création de l'utilisateur/projet `ebraux` (à faire évoluer avec un projet/user de test)


---
## Changement de l'environnement

``` bash
source openrc/admin-openrc.sh
```

Création du projet et de l'utilisateur de validation
``` bash 
openstack project create \
  --domain default \
  --description "Validation  Project" validation

openstack user create \
  --domain default \
  --password stack \
  validation

openstack role add --project validation --user validation member  

openstack role add --project validation --user validation swiftoperator
``` 



``` bash
export OS_PROJECT_NAME=validation
export OS_USERNAME=validation
export OS_PASSWORD=stack
```


``` bash
PROJECT_NAME=validation
NETWORK_NAME=net-validation
SUBNET_NAME=subnet-validation
ROUTER_NAME=router-validation
SECGROUP_NAME=sg-validation
KEYPAIR_NAME=keypair-validation
VOLUME_NAME=vol-validation
INSTANCE_NAME=server-validation
LB_NAME=lb-validation

EXTERNAL_NETWORK_NAME=external


# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 \
  --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

openstack router show  ${ROUTER_NAME}

# test du router
# openstack router show  ${ROUTER_NAME} -c external_gateway_info -f value |
#ping -c 3 $ROUTER_EXTERNEL_IP
```


``` bash
# creation des groupes de sécurité
echo ${SECGROUP_NAME}
openstack security group create --description 'Security Group' ${SECGROUP_NAME}
openstack security group rule create --proto icmp --dst-port 0 ${SECGROUP_NAME}
openstack security group rule create --proto tcp --dst-port 22 ${SECGROUP_NAME}
```

``` bash
# creation d'une keypair
openstack keypair create ${KEYPAIR_NAME} > tmp/${KEYPAIR_NAME}.pem
cat tmp/${KEYPAIR_NAME}.pem
chmod 400  tmp/${KEYPAIR_NAME}.pem 

```


### Lancement d'une instance

``` bash
# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list -f value -c Name | grep  cirros)
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list -f value -c Name | grep 'cirros')
echo ${IMAGE_CIRROS_ID}


# lancement d'une instance
# creation d'un instance de test
echo ${INSTANCE_NAME}
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --key-name ${KEYPAIR_NAME} \
   --nic net-id=${NETWORK_NAME} \
   --security-group ${SECGROUP_NAME} \
   --wait \
   ${INSTANCE_NAME}

# verification que l'instance est active
openstack server show ${INSTANCE_NAME} -c status -f value



# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=$(openstack floating ip list -f value -c 'Floating IP Address' -c 'Fixed IP Address' | grep None | head -1 | awk '{ print $1 }')
echo ${FREE_FLOATING_IP}
openstack server add floating ip ${INSTANCE_NAME} ${FREE_FLOATING_IP}
```

```bash
ping -c 3 ${FREE_FLOATING_IP}

ssh cirros@${FREE_FLOATING_IP}
# gocubsgo


ssh -i tmp/${KEYPAIR_NAME}.pem cirros@${FREE_FLOATING_IP}

```

```bash
openstack volume create --size 1 ${VOLUME_NAME}
```


```bash
openstack server add volume ${INSTANCE_NAME} ${VOLUME_NAME}
```

``` bash
 openstack volume show ${VOLUME_NAME} -f json -c attachments

openstack server  show ${INSTANCE_NAME} -f json -c volumes_attached

```
---
## LB
``` bash
LB_NAME=lb-validation
LB_LISTENER_NAME=lb-listener-validation
LB_POOL_NAME=lb-pool-validation
LB_MONITOR_NAME=lb-monitor-validation
LB_MEMBER_PREFIX=lb-member-validation
LB_MEMBER_FLAVOR=m1.small
LB_MEMBER_IMAGE=imta-ubuntu20
LB_MEMBER_SG=sg-validation-web

```

LB
``` bash
openstack loadbalancer create --wait --name ${LB_NAME}  --vip-subnet-id ${SUBNET_NAME}
openstack loadbalancer show ${LB_NAME} 
```

Listener
``` bash
openstack loadbalancer listener create --wait --name ${LB_LISTENER_NAME} --protocol HTTP --protocol-port 80 ${LB_NAME}
openstack loadbalancer listener list --loadbalancer ${LB_NAME}
openstack loadbalancer listener show ${LB_LISTENER_NAME}
```

Pool
``` bash
openstack loadbalancer pool create --wait --name ${LB_POOL_NAME} --lb-algorithm ROUND_ROBIN --listener ${LB_LISTENER_NAME} --protocol HTTP

openstack loadbalancer pool show ${LB_POOL_NAME}

openstack loadbalancer listener list --loadbalancer ${LB_NAME} -c name -c default_pool_id
```

monitor
``` bash

openstack loadbalancer healthmonitor create --name ${LB_MONITOR_NAME} --delay 15 --max-retries 4 --timeout 10 --type HTTP --url-path / ${LB_POOL_NAME}

openstack loadbalancer healthmonitor show ${LB_MONITOR_NAME} 
```


Members

- Lancement de 2 instances de type serveur WEB
- INstaller APACHE avec cloud-init
- Modifier le nom du html par défaut avec hostname

Fichier  tmp/webserver.yaml :

``` bash
#cloud-config
packages:
  - apache2
``` 


Member Security group 
``` bash
openstack security group create --description 'Security Group' ${LB_MEMBER_SG}
openstack security group rule create --ingress --proto tcp  --remote-ip '172.16.1.0/24' --dst-port 80 ${LB_MEMBER_SG}
```

``` bash
openstack server create \
   --flavor ${LB_MEMBER_FLAVOR} \
   --image ${LB_MEMBER_IMAGE} \
   --key-name ${KEYPAIR_NAME} \
   --network ${NETWORK_NAME} \
   --security-group ${SECGROUP_NAME} \
   --security-group ${LB_MEMBER_SG} \
   --config-drive True \
   --user-data tmp/webserver.yaml \
   --min 2 --max 2 \
   --wait \
   ${LB_MEMBER_PREFIX}
```

``` bash

openstack server list  -c Name -c Networks
```


``` bash

LB_MEMBER_1_IP=$(openstack server show ${LB_MEMBER_PREFIX}-1 -f value  -c addresses | awk -F'=' '{print $2}')
echo ${LB_MEMBER_1_IP}

LB_MEMBER_2_IP=$(openstack server show ${LB_MEMBER_PREFIX}-2 -f value  -c addresses | awk -F'=' '{print $2}')
echo ${LB_MEMBER_2_IP}

``` bash
openstack loadbalancer member create \
  --subnet-id ${SUBNET_NAME}  \
  --address ${LB_MEMBER_1_IP} \
  --protocol-port 80 \
  ${LB_POOL_NAME} 

openstack loadbalancer member create \
  --subnet-id ${SUBNET_NAME}  \
  --address ${LB_MEMBER_2_IP}\
  --protocol-port 80 \
  ${LB_POOL_NAME} 

openstack loadbalancer member list  ${LB_POOL_NAME} 
```


Floating IP
``` bash
openstack floating ip create external
LB_FLOATING_IP=$(openstack floating ip list -f value -c 'Floating IP Address' -c 'Fixed IP Address' | grep None | head -1 | awk '{ print $1 }')
echo ${LB_FLOATING_IP}

LB_PORT_ID=$(openstack loadbalancer show ${LB_NAME}  -f value -c vip_port_id)
echo ${LB_PORT_ID}

openstack floating ip set --port ${LB_PORT_ID}  ${LB_FLOATING_IP}
```

Test
``` bash
echo "http://${LB_FLOATING_IP}"

curl http://${LB_FLOATING_IP}
```

---
## Stockage Objet

``` bash
BUCKET_NAME=bucket-validation
OBJECT_NAME=object-validation

echo "validation object-storage" > tmp/object-validation.txt 

openstack container create ${BUCKET_NAME}
openstack container  list

openstack object create ${BUCKET_NAME} --name object-validation.txt tmp/object-validation.txt
openstack object list ${BUCKET_NAME}


curl -k https://openstack.imt-atlantique.fr:13808/v1/AUTH_9f666f6e984a40bdb4eda5edfe8780f4/bucket-validation/object/object-validation.txt 
```


Ménage 
``` bash
openstack object delete  ${BUCKET_NAME} object-validation.txt
```
