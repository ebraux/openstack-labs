# Configuration de l'environnement



## Cloner le dépot

``` bash
git clone
```

**Attention** : il ne faut pas oublier de faire un `git pull` avant modifications et un `git push` après pour que le dépôt central soit toujours à jour.


## Initialisation d'un environnement de travail avec direnv


`direnv` permet d'automatiser la création d'un environnement `venv` ainsi que les phases d'activation et désactivation de l'environnement qui sont effectuées automatiquement en changeant de répertoire.

Avant la première utilisation, `direnv` doit être initialisé pour le shell de travail. Pour cela, se référer à la documentation de l'outil : https://github.com/direnv/direnv/blob/master/docs/hook.md

Exemple en bash, créer un fichier de coniguartion
``` bash
echo 'eval "$(direnv hook bash)"' > ~/.bashrc.d/direnv.bashrc
```


Activation (autorisation d'exécution) de l'environnement et installation des dépendances
```sh
cd  operation
direnv allow .
```

---
## Installer les packages python nécéssaires

- ansible
- Opensatck client
- ...


``` bash
pip install -r requirements.txt
```

---
## Installer les rôles Ansible

``` bash
ansible-galaxy install -r requirements.yml
```


