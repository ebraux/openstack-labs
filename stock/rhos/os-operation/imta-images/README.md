#  Getsion des images imta

---
## Principe

1. Utilisation de packer pour générer les image en mode privé pour "admin"
2. download, compression et chargement des images dans openstack en mode "public"


---
## Création d'une image


``` bash
PROJECT_NAME=admin
NETWORK_NAME=net-packer
SUBNET_NAME=subnet-packer
ROUTER_NAME=router-packer

EXTERNAL_NETWORK_NAME=external


# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 \
  --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

openstack router show  ${ROUTER_NAME}

```


``` bash
# creation des groupes de sécurité
SECGROUP_ADMIN=sg-packer
echo ${SECGROUP_ADMIN}
openstack security group create --description 'Packer Security Group' ${SECGROUP_ADMIN}
openstack security group rule create --proto icmp --dst-port 0 ${SECGROUP_ADMIN}
openstack security group rule create --proto tcp --dst-port 22 ${SECGROUP_ADMIN}
```

``` bash
openstack network show  -f value -c id net-packer 
d56632cf-91bc-4e9b-b27b-2108f5bafd12
```

---
## Création d'une image

### Configuration de l'environnement

- Image imta-ubuntu20
```
export IMAGE_NAME=imta-ubuntu20
```


- Image imta-ubuntu22
```
export IMAGE_NAME=imta-ubuntu22
```

- Image imta-docker
```
export IMAGE_NAME=imta-docker
```

- Image imta-centos9
```
export IMAGE_NAME=imta-centos9
```

- Image imta-centos8
```
export IMAGE_NAME=imta-centos8
```


###  Création de l'image  

``` bash
echo  ${IMAGE_NAME}

packer validate  ${IMAGE_NAME}.json
packer build  ${IMAGE_NAME}.json
```

---
## Post traitement et cration de l'image définitive

### POST Traitement de l'image
``` bash
export WORKING_DIR='/datas/tmp'
mkdir ${WORKING_DIR}
```

- Récupération de l'ID de l'image
```
export IMAGE_OS_ID=`openstack  image show -f value -c id ${IMAGE_NAME}_model`
echo $IMAGE_OS_ID
```
- Téléchargement de l'image en local
```
openstack image save --file ${WORKING_DIR}/${IMAGE_NAME}.raw ${IMAGE_OS_ID}
```
- Optimisation de l'image (compression, ...)
```
qemu-img convert -O qcow2 -c ${WORKING_DIR}/${IMAGE_NAME}.raw ${WORKING_DIR}/${IMAGE_NAME}.qcow2
```

### Chargement de l'image définitive dans Openstack

- Import dans openstack
``` bash
echo ${IMAGE_NAME}
echo ${WORKING_DIR}
#echo ${IMAGE_ID}

# --id ${IMAGE_ID} \
openstack image create \
    --public \
    --disk-format qcow2 \
    --container-format bare \
    --file ${WORKING_DIR}/${IMAGE_NAME}.qcow2 ${IMAGE_NAME}
```

---
##  Ménage

- Image "Model" dans Openstack
```
openstack image delete $IMAGE_OS_ID
```
- fichiers en local
```
rm ${WORKING_DIR}/$IMAGE_NAME.*
```



---
## Tests optimisation

```
qemu-img convert -O qcow2 ${WORKING_DIR}/${IMAGE_NAME}.raw ${WORKING_DIR}/${IMAGE_NAME}.qcow2

qemu-img convert -O qcow2 -c ${WORKING_DIR}/${IMAGE_NAME}.raw ${WORKING_DIR}/${IMAGE_NAME}-compress.qcow2

virt-sparsify ${WORKING_DIR}/$IMAGE_NAME.qcow2 --compress ${WORKING_DIR}/${IMAGE_NAME}-sparsify.qcow2
```

``` bash
-rw-r--r--. 1 cloud-user cloud-user  853409792 Oct  9 10:31 imta-ubuntu22-compress.qcow2
-rw-r--r--. 1 cloud-user cloud-user 2081751040 Oct  9 10:28 imta-ubuntu22.qcow2
-rw-r--r--. 1 cloud-user cloud-user 2081751040 Oct  9 10:25 imta-ubuntu22.raw
-rw-r--r--. 1 cloud-user cloud-user  714342400 Oct  9 11:14 imta-ubuntu22-sparsify.qcow2
```
