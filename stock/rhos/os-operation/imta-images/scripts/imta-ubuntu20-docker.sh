#!/bin/bash


# proxy configuration
export http_proxy='http://proxy.enst-bretagne.fr:8080'
export https_proxy='http://proxy.enst-bretagne.fr:8080'
export no_proxy='127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr'

# silent install
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"


#
# ADD Docker
#
apt update
apt install -y apt-transport-https ca-certificates curl software-properties-common

# Add the Docker repository to APT sources:
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /tmp/download.docker.co.gpg
#apt-key add /tmp/download.docker.co.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt-cache policy docker-ce

# Install Docker
# apt-cache policy docker-ce
apt install -y docker-ce
#systemctl status docker
# ubuntu user can Executing the Docker Command Without Sudo
usermod -a -G docker ubuntu

# config Docker :
#  - DNS
cat >> /etc/docker/daemon.json <<FIN
{
    "dns": ["192.44.75.10", "192.108.115.2"]'
}
FIN


# Add Docker-compose
curl -L "https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#
# Config proxy 
#

# Config proxy for docker Daemon
mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" > /etc/systemd/system/docker.service.d/proxy.conf
echo "Environment='HTTP_PROXY=$http_proxy'"   >> /etc/systemd/system/docker.service.d/proxy.conf
echo "Environment='HTTPS_PROXY=$http_proxy'"  >> /etc/systemd/system/docker.service.d/proxy.conf
echo "Environment='NO_PROXY=$no_proxy'"       >> /etc/systemd/system/docker.service.d/proxy.conf
systemctl daemon-reload
systemctl restart docker

# Config proxy for user root
mkdir /root/.docker
tee /root/.docker/config.json << 'EOF'
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://proxy.enst-bretagne.fr:8080",
     "httpsProxy": "http://proxy.enst-bretagne.fr:8080",
     "noProxy": "127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"
   }
 }
}
EOF

# Duplicate for Ubuntu user
cp -rp /root/.docker /home/ubuntu/.docker
chown -R ubuntu.ubuntu /home/ubuntu/.docker
