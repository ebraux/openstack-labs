# Clear any old environment that may conflict.
for key in $( set | awk '{FS="="}  /^OS_/ {print $1}' ); do unset $key ; done

export PYTHONWARNINGS='ignore:Certificate has no, ignore:A true SSLContext object is not available'

export OS_IDENTITY_API_VERSION=3
export OS_COMPUTE_API_VERSION=2.latest
export OS_IMAGE_API_VERSION=2
export OS_VOLUME_API_VERSION=3

export no_proxy=.ctlplane,.enst-bretagne.fr,.external,.imt-atlantique.fr,.imta.fr,.localdomain,.overcloud-prod.com,.telecom-bretagne.eu,10.129.172.0/24,10.129.173.0/24,10.129.173.16,10.129.173.17,10.129.173.9,10.129.176.0/22,10.129.176.9,127.0.0.1,192.168.174.0/24,192.168.175.0/24,192.168.176.0/24,192.168.177.0/24,192.168.74.9,localhost,openstack.imt-atlantique.fr,rhos-director-br-01


export OS_NO_CACHE=True
export OS_AUTH_TYPE=password

# export OS_CLOUD=overcloud

export OS_AUTH_URL=https://openstack.imt-atlantique.fr:13000
export OS_REGION_NAME=Brest1
export OS_USERNAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_NAME=admin
export OS_TENANT_NAME=admin
export OS_PROJECT_DOMAIN_NAME=Default
export OS_PASSWORD=xxxxxx

# Add OS_CLOUD to PS1
if [ -z "${CLOUDPROMPT_ENABLED:-}" ]; then
    export PS1=${PS1:-""}
    export PS1=\${OS_CLOUD:+"(\$OS_CLOUD)"}\ $PS1
    export CLOUDPROMPT_ENABLED=1
fi



