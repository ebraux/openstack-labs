
# --- dependances ---
munch==4.0.*


# --- Openstavk Python Client ---
# - Pour Rocky : version = 3.16.*
# - pour RH0S16.2: version 4.0.*
# - Pour Victoria = 5.4.*
# - Pour RHOS 17.1 = 5.5.2.dev16 (pas dispo via pip, c'ets une dev)
# - Pour Wallaby = 5.5.1.*
# - Pour Yoga ==5.8.*
python-openstackclient==5.5.1.*


oslo.utils==4.8.2

# --- Openstack SDK ---
#openstacksdk==0.36.*  # RHOS 16.2
#openstacksdk==0.55.1  # RHOS 17.1
#openstacksdk==1.0.*   # ansible collection openstack
openstacksdk==1.5.*

# --- nova client
# Pour RHOS 17.1 = python-novaclient==17.4.1
python-novaclient==17.4.1


python-keystoneclient==4.3.0
python-glanceclient==3.3.0
python-neutronclient==7.3.1
python-novaclient==17.4.1


# --- Heat client ---
# python-heatclient==2.3.1 # RHOS 17.1
python-heatclient==2.3.1


# --- Octavia Python Client---
# python-octaviaclient==1.10.*  # RHOS 16.2
# python-octaviaclient==2.3.1 # RHOS 17.1
# python-octaviaclient<3.5.0    # bug version 3.5.0
python-octaviaclient==2.3.1


python-swiftclient==3.11.1

# ---
#aodhclient==2.2.0
#gnocchiclient==7.0.7
#ipaclient==4.10.1
#os-client-config==2.1.0
#python-barbicanclient==5.3.0
python-cinderclient==7.4.1
#python-designateclient==4.2.1
#python-ironic-inspector-client==4.5.0
#python-ironicclient==4.6.4
#python-magnumclient==3.4.1
#python-manilaclient==2.6.4
#python-mistralclient==4.2.0
#python-saharaclient==3.3.0
#python-tripleoclient==16.5.1.dev199
#python-troveclient==7.0.0
#python-zaqarclient==2.3.0


