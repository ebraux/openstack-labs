#!/bin/bash
#set -xe

source ~/stackrc

cp  /home/stack/templates/deployed/overcloud-baremetal-deployed.yaml \
    /home/stack/templates-deployed-backup/overcloud-baremetal-deployed.yaml.`date +%Y%m%d%H%M`

openstack overcloud node provision -y \
    --templates /usr/share/openstack-tripleo-heat-templates  \
    --stack  overcloud \
    --network-config  \
    --output /home/stack/templates/deployed/overcloud-baremetal-deployed.yaml \
    /home/stack/templates/deployment-datas.yaml

