#!/bin/bash
#set -xe

source ~/stackrc

openstack overcloud deploy  \
  --timeout 360 \
  --verbose \
  --roles-file /home/stack/templates/roles_data.yaml \
  --networks-file /home/stack/templates/network_data.yaml \
  --answers-file /home/stack/templates/overcloud-answers.yaml \
  --log-file /home/stack/overcloudDeploy.log\
  --ntp-server 10.129.173.14

