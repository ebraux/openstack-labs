#!/bin/bash
#set -xe

source ~/stackrc


cp  /home/stack/templates/deployed/overcloud-networks-deployed.yaml \
    /home/stack/templates-deployed-backup/overcloud-networks-deployed.yaml.`date +%Y%m%d%H%M`

openstack overcloud network provision -y \
    --templates /usr/share/openstack-tripleo-heat-templates  \
    --output  /home/stack/templates/deployed/overcloud-networks-deployed.yaml \
    /home/stack/templates/network_data.yaml

