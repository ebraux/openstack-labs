#!/bin/bash
#set -xe

source ~/stackrc

cp  /home/stack/templates/deployed/overcloud-vip-deployed.yaml \
    /home/stack/templates-deployed-backup/overcloud-vip-deployed.yaml.`date +%Y%m%d%H%M`

openstack overcloud network vip provision -y \
 --templates /usr/share/openstack-tripleo-heat-templates  \
 --stack  overcloud \
 --output /home/stack/templates/deployed/overcloud-vip-deployed.yaml \
 /home/stack/templates/vip_data.yaml


