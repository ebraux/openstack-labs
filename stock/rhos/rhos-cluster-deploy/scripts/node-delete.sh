#!/bin/bash
#set -xe

source ~/stackrc

openstack overcloud node delete -y \
    --stack  overcloud \
    --baremetal-deployment /home/stack/templates/deployment-datas.yaml

