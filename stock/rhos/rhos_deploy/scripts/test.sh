
# ----------------------------------
#  Configuration de l'infrastructure
#     ___        __               _                   _
#    |_ _|_ __  / _|_ __ __ _ ___| |_ _ __ _   _  ___| |_ _   _ _ __ ___
#     | || '_ \| |_| '__/ _` / __| __| '__| | | |/ __| __| | | | '__/ _ \
#     | || | | |  _| | | (_| \__ \ |_| |  | |_| | (__| |_| |_| | | |  __/
#    |___|_| |_|_| |_|  \__,_|___/\__|_|   \__,_|\___|\__|\__,_|_|  \___|
#
# ----------------------------------


Pour le projet "demo", en tant que "demo"
#source clients_demo-openrc
 
#
# configuration
USER_NAME=demo
PROJECT_NAME=${PROJECT_NAME}
NETWORK_NAME=${PROJECT_NAME}Network
SUBNET_NAME=${PROJECT_NAME}Subnet
ROUTER_NAME=${PROJECT_NAME}Routeur
EXTERNAL_NETWORK_NAME=external

#---
#Ajout d'une image
wget http://download.cirros-cloud.net/0.5.1/cirros-0.5.1-x86_64-disk.img
openstack image create "cirros" \
  --file cirros-0.5.1-x86_64-disk.img \
  --disk-format qcow2 \
  --container-format bare \
  --public

#---
openstack image list 
IMAGE_CIRROS_ID=$(openstack image show cirros -f value -c id)

# creation d'un flavor
openstack flavor create --ram 256 --disk 0 --vcpus 1 cirros
openstack flavor list
FLAVOR_CIRROS_ID=$(openstack flavor show cirros -f value -c id)

# ---
# reseau externe

#openstack network create  --share --external \
#  --provider-physical-network provider \
#  --provider-network-type flat \
#  ${EXTERNAL_NETWORK_NAME}

openstack network create  --share --external \
  --provider-network-type vlan \
  --provider-physical-network datacentre \
  --provider-segment 178
  ${EXTERNAL_NETWORK_NAME}



# creation d'un sous réseau
openstack subnet create --network ${EXTERNAL_NETWORK_NAME} \
  --allocation-pool start=10.129.176.20,end=10.129.179.250 \
  --dns-nameserver 192.108.115.2 --gateway 10.129.176.1 \
  --subnet-range 10.129.176.0/22 \
  ${EXTERNAL_NETWORK_NAME}
openstack subnet set --dns-nameserver 192.44.75.10 ${EXTERNAL_NETWORK_NAME}

# -- demo project --
openstack project create \
  --domain default \
  --description "Demo Project" ${PROJECT_NAME}

openstack project list
openstack project show ${PROJECT_NAME}
PROJECT_ID_DEMO=$(openstack project show ${PROJECT_NAME}  -f value -c id)

#EXTERNAL_NETWORK_NAME=external
# ->  defini dans la config de neutron


# -- demo user --
openstack user create \
  --domain default \
  --password password \
  ${USER_NAME}

openstack user list
openstack user show ${USER_NAME}
USER_ID_DEMO=$(openstack user show ${USER_NAME} -f value -c ID)


# -- affecte role "member" à l'utilisateur demo, sur le projet demo --
openstack role add --project ${PROJECT_NAME} --user ${USER_NAME}  member
openstack role  assignment list --names
openstack role  assignment list --names --project ${PROJECT_NAME}


# --------------------------------
#  Connexion en tant de demo su le projet demo
# --------------------------------
OS_PROJECT_NAME=${PROJECT_NAME}
OS_USERNAME=${USER_NAME}
OS_PASSWORD=password



# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 8.8.4.4 --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME}

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

# configuration DNS
openstack subnet set --dns-nameserver 192.44.75.10 ${SUBNET_NAME}
openstack subnet set --dns-nameserver 192.44.75.10 ${SUBNET_NAME}
openstack subnet unset --dns-nameserver 8.8.4.4 ${SUBNET_NAME}

# creation des groupes de sécurité
openstack security group create --description 'Admin rules' ${PROJECT_NAME}Admin
openstack security group rule create --proto icmp --dst-port 0 ${PROJECT_NAME}Admin
openstack security group rule create --proto tcp --dst-port 22 ${PROJECT_NAME}Admin



# creation d'une instance
# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list | grep '| cirros ' | awk '{ print $2 }')
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list | grep ' cirros ' | awk '{ print $2 }')
echo ${IMAGE_CIRROS_ID}


# lancement d'une instance
# creation d'un instance de test
TEST_INSTANCE_NAME=${PROJECT_NAME}Test
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --nic net-id=${PROJECT_NAME}selfservice \
   --security-group ${PROJECT_NAME}BasicAdmin \
   ${TEST_INSTANCE_NAME}


# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=`openstack floating ip list | grep '| None             | None' | head -1 | awk '{ print $4 }'`
openstack server add floating ip ${TEST_INSTANCE_NAME} ${FREE_FLOATING_IP}


ping ${FREE_FLOATING_IP}



# ------------------------

# Menage

openstack security group delete ${PROJECT_NAME}Admin

openstack router delete subnet ${ROUTER_NAME} ${SUBNET_NAME}

openstack router delete  ${ROUTER_NAME}

openstack subnet delete ${SUBNET_NAME}
