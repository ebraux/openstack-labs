#!/bin/bash
#set -xe

source ~/stackrc

openstack overcloud deploy  \
  --timeout 240 \
  --verbose \
  -r /home/stack/templates/roles_data.yaml \
  -n /home/stack/templates/network_data.yaml \
  --answers-file /home/stack/templates/answers.yaml \
  --log-file /home/stack/overcloudDeploy.log \
  --ntp-server ntp1.svc.enst-bretagne.fr



