

---
## Installation de cockpit

``` bash
dnf install -y cockpit cockpit-machines
systemctl enable --now cockpit.socket
systemctl start --now cockpit.socket
```

``` bash
firewall-cmd --add-service=cockpit --permanent
firewall-cmd --reload
```

Connection :
- [https://10.29.241.11:9090](https://10.29.241.11:9090)
- cloud-user [rhos]
- devrait fonctionner avec https://10.129.172.11:9090




