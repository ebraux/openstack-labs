


NTP server : 172.25.254.254

external (accés extenes) : 172.25.250.0/24

Provider 103 : 10.0.103.0/24
Provider 104 : 10.0.104.0/24

PXE provisionning 172.25.249.0/24
 diretor à 2 IP 5x et 50
 inspection range : 172.25.249.150 à 180
 gateway : 172.25.249.200
 local_IP : 172.25.249.200
 DCHP :  172.25.249.51 à 59
 undercoud_admin_host : 202
 undercoud_public_host : 201

Internal API, Vlan 10, 172.24.1.0/24
Tenant, Vlan 20, 172.24.2.0/24
storage, Vlan 30, 172.24.3.0/24
Storage Mgmt, Vlan 40, 172.24.4.0/24
Management, Vlan 50, 172.24.5.0/24


Controller0 a toutes les passerelle en IP 1



             network_config:
Interface pour Provisioning
              - type: interface
                name: nic1
                addresses:
                - ip_netmask:
                    list_join:
                    - /
                    - - get_param: ControlPlaneIp
                      - get_param: ControlPlaneSubnetCidr
                routes:
                - ip_netmask: 169.254.169.254/32
                  next_hop:
                    get_param: EC2MetadataIp

Bridge "Trunk" pour Tous les vlan sauf external
              - type: ovs_bridge
                name: br-trunk
                members:
                - type: interface
                  name: nic2
                  - StorageNetworkVlanID
                  - StorageMgmtNetworkVlanID
                  - InternalApiNetworkVlanID
                  - TenantNetworkVlanID
                  - ManagementNetworkVlanID
                - 
Bridge br-ex pour external. il y a le mapping "bridge_name" avec br-ex qui est fait                  
              - type: ovs_bridge
                name: bridge_name
                dns_servers:
                  get_param: DnsServers
                use_dhcp: false
                addresses:
                - ip_netmask:
                    get_param: ExternalIpSubnet
                routes:
                  list_concat_unique:
                    - get_param: ExternalInterfaceRoutes
                    - - default: true
                        next_hop: '172.25.250.254'
                members:
                - pour: interface
                  name: nic3
                  primary: true

les 2 provisionnig                  
              - type: ovs_bridge
                name: br-prov1
                - type: interface
                  name: nic4

             - type: ovs_bridge
                name: br-prov2
                - type: interface
                  name: nic5


diffrence dans net-config

pour interface nic1
 - ajout du DNS
 - dans calssromm
  ``` yaml
                 routes:
                - ip_netmask: 169.254.169.254/32
                  next_hop:
                    get_param: EC2MetadataIp
  ```
