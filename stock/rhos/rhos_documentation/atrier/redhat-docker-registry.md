# getsion de la registry Docker de Redhat


doc :
- [https://access.redhat.com/documentation/fr-fr/red_hat_software_collections/3/html-single/using_red_hat_software_collections_container_images/index](https://access.redhat.com/documentation/fr-fr/red_hat_software_collections/3/html-single/using_red_hat_software_collections_container_images/index)
- [https://access.redhat.com/RegistryAuthentication](https://access.redhat.com/RegistryAuthentication)

connexion çà la registry
``` bash
export https_proxy=http://proxy.enst-bretagne.fr:8080
podman login registry.redhat.io
Username: DISI1
Password: xxxxxx
# Login Succeeded!
```


curl -s --user $CREDS https://$THE_REGISTRY/v2/_catalog | \
    jq -r '.["repositories"][]' | \
    xargs -I @REPO@ curl -s --user $CREDS https://$THE_REGISTRY/v2/@REPO@/tags/list | \
    jq -M '.["name"] + ":" + .["tags"][]'

THE_REGISTRY=registry.redhat.io

# Get username:password from docker configuration. You could
# inject these some other way instead if you wanted.
CREDS=$(jq -r ".[\"auths\"][\"$THE_REGISTRY\"][\"auth\"]" .docker/config.json | base64 -d)
#DISI1:DiSi2003

curl -s --user $CREDS https://$THE_REGISTRY/v2/_catalog | \
    jq -r '.["repositories"][]' | \
    xargs -I @REPO@ curl -s --user $CREDS https://$THE_REGISTRY/v2/@REPO@/tags/list | \
    jq -M '.["name"] + ":" + .["tags"][]'