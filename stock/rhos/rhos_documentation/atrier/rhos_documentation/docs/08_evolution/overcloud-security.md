

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/planning-your-overcloud#overcloud-security

6.4. Overcloud security
Your OpenStack Platform implementation is only as secure as your environment. Follow good security principles in your networking environment to ensure that you control network access properly:

Use network segmentation to mitigate network movement and isolate sensitive data. A flat network is much less secure.
Restrict services access and ports to a minimum.
Enforce proper firewall rules and password usage.
Ensure that SELinux is enabled.
For more information about securing your system, see the following Red Hat guides:

Security Hardening for Red Hat Enterprise Linux 8
Using SELinux for Red Hat Enterprise Linux 8

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_selinux/index


### configure cert between undercloud and overcloud

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA


créer le fichier "/home/stack/inject-trust-anchor-hiera.yaml" avec la clé  de la CA "cat /home/stack/undercloud-cert/ca.crt.pem"

```bash
parameter_defaults:
  CAMap:
    overcloud-ca:
      content: |
        -----BEGIN CERTIFICATE-----
        MIIDl ... 
        -----END CERTIFICATE-----
    undercloud-ca:
      content: |
        -----BEGIN CERTIFICATE-----
        MIIDl ... 
        -----END CERTIFICATE-----
```