

## roles
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/creating-a-basic-overcloud-with-cli-tools#creating-architecture-specific-roles


## gérer les tempslates : 

* ADVANCED OVERCLOUD CUSTOMIZATION
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html-single/advanced_overcloud_customization/index

* BASIC NETWORK ISOLATION
  *  https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/basic-network-isolation

* CUSTOM COMPOSABLE NETWORKS
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/custom-composable-networks


* CUSTOM NETWORK INTERFACE TEMPLATES
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/custom-network-interface-templates


* EXTERNAL LOAD BALANCING FOR THE OVERCLOUD
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/external_load_balancing_for_the_overcloud/index

* ca-trust
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/creating-a-basic-overcloud-with-cli-tools#creating-an-environment-file-for-undercloud-ca-trust

* MONITORING TOOLS CONFIGURATION GUIDE
  * https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/monitoring_tools_configuration_guide/index

* skydive
  * https://access.redhat.com/solutions/3956281