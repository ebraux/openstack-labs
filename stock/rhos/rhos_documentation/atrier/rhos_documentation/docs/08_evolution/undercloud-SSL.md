Les valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway" sont utilisée ensuite dans la génération des certificats et la configuration du proxy.

## Gestion des certificats


* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-SSLTLS_Certificate_Configuration](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-SSLTLS_Certificate_Configuration)

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA)


Création de la CA

```bash
sudo su - stack

sudo touch /etc/pki/CA/index.txt
echo '1005' | sudo tee /etc/pki/CA/serial

mkdir undercloud-cert
cd undercloud-cert

sudo openssl genrsa -out ca.key.pem 4096
sudo openssl req  -key ca.key.pem -new -x509 -days 7300 -extensions v3_ca -out ca.crt.pem -subj '/C=FR/ST=BRETAGNE/L=BREST/O=IMT-ATLANTIQUE/OU=STAGE/CN=os-director-01.imta.fr/emailAddress=emmanuel.braux@imt-atlantique.fr'
 
sudo cp ca.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract
```

```bash
openssl genrsa -out server.key.pem 2048

cp /etc/pki/tls/openssl.cnf openssl.cnf

#vim openssl.cnf

diff  /etc/pki/tls/openssl.cnf openssl.cnf
126c126
< # req_extensions = v3_req # The extensions to add to a certificate request
---
> req_extensions = v3_req # The extensions to add to a certificate request
130c130
< countryName_default		= XX
---
> countryName_default		= FR
135c135
< #stateOrProvinceName_default	= Default Province
---
> stateOrProvinceName_default	= BRETAGNE
138c138
< localityName_default		= Default City
---
> localityName_default		= BREST
141c141
< 0.organizationName_default	= Default Company Ltd
---
> 0.organizationName_default	= IMT-ATLANTIQUE
148c148
< #organizationalUnitName_default	=
---
> organizationalUnitName_default	= DISI
151a152
> commonName_default              = 10.129.172.2
154a156
> emailAddress_default		= emmanuel.braux@imt-atlantique.fr
224a227
> subjectAltName = @alt_names
352a356,366
> 				#
> [alt_names]
> IP.1 = 10.129.173.2
> IP.2 = 10.129.173.3
> IP.3 = 10.129.173.5
> DNS.1 = instack.localdomain
> DNS.2 = vip.localdomain
> DNS.3 = os-director-01.imta.fr
> DNS.4 = 10.129.173.2
> DNS.5 = 10.129.173.3
> DNS.6 = 10.129.173.5
```

"commonName_default" et les valeur de "[alt_names]" doivnet être adaptées à aux valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway".

```bash
sudo openssl req -config openssl.cnf -key server.key.pem -new -out server.csr.pem -subj '/C=FR/ST=BRETAGNE/L=BREST/O=IMT-ATLANTIQUE/OU=STAGE/CN=os-director-01.imta.fr/emailAddress=emmanuel.braux@imt-atlantique.fr'


sudo openssl ca -config openssl.cnf -extensions v3_req -days 3650 -in server.csr.pem -out server.crt.pem -cert ca.crt.pem -keyfile ca.key.pem

cat server.crt.pem server.key.pem > undercloud.pem

openssl x509 -in undercloud.pem -text | grep -A 1 'Alternative Name'
```

```bash
sudo mkdir /etc/pki/instack-certs
sudo cp undercloud.pem /etc/pki/instack-certs/

openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text | grep -A 1 'Alternative Name'
sudo semanage fcontext -a -t etc_t "/etc/pki/instack-certs(/.*)?"
sudo restorecon -R /etc/pki/instack-certs

sudo cp ca.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract
```

``` bash
openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text
openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text | grep -A 1 'Alternative Name'

```


### Ménage si besoin 

```bash
rm -rf /etc/pki/tls/certs/undercloud*
rm -rf /etc/pki/tls/private/undercloud*
rm -rf /etc/pki/ca-trust/source/anchors/*local*
```

si besoin d modifier, revocation du certificat

```bash
cat /etc/pki/CA/index.txt

sudo openssl ca -config openssl.cnf -keyfile ca.key.pem -cert ca.crt.pem  -revoke /etc/pki/CA/newcerts/1000.pem
```
