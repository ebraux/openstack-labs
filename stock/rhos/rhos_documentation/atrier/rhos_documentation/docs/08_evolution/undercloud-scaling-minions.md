



An undercloud minion provides additional heat-engine and ironic-conductor services on a separate host. These additional services support the undercloud with orchestration and provisioning operations. The distribution of undercloud operations across multiple hosts provides more resources to run an overcloud deployment, which can result in potentially faster and larger deployments.


IMPORTANT
This feature is available in 16.1 release as a Technology Preview, and therefore is not fully supported by Red Hat. It should only be used for testing, and should not be deployed in a production environment

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/installing-undercloud-minions
```bash
openstack orchestration service list
+---------------------+-------------+--------------------------------------+---------------------+--------+----------------------------+--------+
| Hostname            | Binary      | Engine ID                            | Host                | Topic  | Updated At                 | Status |
+---------------------+-------------+--------------------------------------+---------------------+--------+----------------------------+--------+
| rh-director.imta.fr | heat-engine | 478fb1b3-d728-47ea-8eb2-77cb4e6f3149 | rh-director.imta.fr | engine | 2020-09-22T18:21:06.000000 | up     |
| rh-director.imta.fr | heat-engine | 308c75fb-bde7-4a8b-bd43-96559b0a7b8c | rh-director.imta.fr | engine | 2020-09-22T18:21:06.000000 | up     |
| rh-director.imta.fr | heat-engine | 86f6d4ac-0eff-4d99-a212-23fde12aebc9 | rh-director.imta.fr | engine | 2020-09-22T18:21:06.000000 | up     |
| rh-director.imta.fr | heat-engine | d2145d92-7fb0-407d-b967-ac599f27221d | rh-director.imta.fr | engine | 2020-09-22T18:21:06.000000 | up     |
+---------------------+-------------+--------------------------------------+---------------------+--------+----------------------------+--------+

```