

si on veut pouvoir utiliser une registry avec authentification, il faut ajouter les infos dans "containers-prepare-parameter.yaml" :

```yaml
  ContainerImageRegistryLogin: true
  ContainerImageRegistryCredentials:
    registry.redhat.io:
	  DISI1: DiSi2003
	'10.129.172.22:5000':
      login: password
`
```