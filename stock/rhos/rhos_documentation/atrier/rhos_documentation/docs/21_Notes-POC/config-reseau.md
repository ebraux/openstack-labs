
## description des réseaux à mettre en place :


Provisioning / Control Plane  10.129.173.0/24 doit avoir accés aux DNS : 192.108.115.2, 192.44.75.10

### IPMI

concerne : All nodes

Reseau iPMI (iDRAC DELL, Cartes ILO HP....)

Network used for power management of nodes.
This network is predefined before the installation of the undercloud.

### Provisioning / Control Plane

concerne : All nodes

VLAN de provisioning/admin - “Admin Network” Entre tous les server controllers, computes et Director, sur
une interface physique dédiée

The director uses this network traffic type to deploy new nodes over PXE boot and orchestrate the installation of OpenStack Platform on the overcloud bare metal servers.  This network is predefined before the installation of the undercloud.

### Internal API


##  Résumé config réseau



* VLAN interne pour les réseaux de projet (Vxlan) - “Vxlan Network” - Communication entre les VMs déployées
* VLAN pour les API Publiques - “External Network” Connexion à un routeur externe au cluster pour
l'accès aux API OpenStack, aux passerelles RADOS, et l’interface Horizon.
* VLAN interne pour les API - “Internal Network API” - Pour la communication intra cluster, entre les
contrôleurs, Director et les noeuds de compute
* VLAN pour le stockage - “Storage Network” - Utilisé par tous les noeuds et les VMs consommant les ressources de l'infra Ceph.
* VLAN pour la gestion du stockage - “Storage Management” - Utilisé pour les opérations de réplication
et d’opération liées au cluster CEPH. Interface dédié, lien 10G entr eles noeuds CEPH.
* VLAN externe : pour les IP flottantes et accès externes pour les VMs. Connexion à travers un routeur externe au cluster

En résumé : 8 VLANs sont nécessaires au déploiement d’OpenStack.

Quels accès pour Director  ? aux API Externe, Interne ?

Mutualisation possible : 

* sur les compute : Vxlan et Internal API
* sur les controller :
  * Vxlan, external network  et Internal API (mauvaise idée !!!)
  * “Storage Network” et “Storage Management”  (besoin du storage management sur les controlplane ??)


pour les classe d'IP necessaires : 
[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/planning-your-undercloud#preparing-your-network](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/planning-your-undercloud#preparing-your-network)

# -----------------



sw-d01-04#show interfaces status

Port      Name               Status       Vlan       Duplex  Speed Type
Gi1/0/1                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/2                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/3                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/4                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/5                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/6                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/7                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/8                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/9                      connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/12                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/13                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/14                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/15                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/16                     connected    trunk      a-full  a-100 10/100/1000BaseTX
Gi1/0/17                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/18                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/19                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/21                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/24                     connected    trunk      a-full a-1000 10/100/1000BaseTX

Gi1/0/10                     notconnect   420          auto   auto 10/100/1000BaseTX
Gi1/0/11                     disabled     148          auto   auto 10/100/1000BaseTX
Gi1/0/20                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/22                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/23                     notconnect   1            auto   auto 10/100/1000BaseTX


Te1/0/1                      notconnect   1            full    10G Not Present
Te1/0/2                      notconnect   1            full    10G Not Present
Fa0                          disabled     routed       auto   auto 10/100BaseTX


show mac address-table interface Gi1/0/1
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
 420    0024.e83b.9f24    DYNAMIC     Gi1/0/2
 420    d094.6622.6197    DYNAMIC     Gi1/0/9
 172    1866.daed.2c3c    DYNAMIC     Gi1/0/14
 172    001e.c9ab.8292    DYNAMIC     Gi1/0/15




sw-d01-03#


sw-d01-03>show mac address-table

 172    0024.e83b.9f1e    DYNAMIC     Gi1/0/1
 176    0024.e83b.9f20    DYNAMIC     Gi1/0/2
 172    1866.daec.4b3c    DYNAMIC     Gi1/0/3
 172    1866.daec.4b3d    DYNAMIC     Gi1/0/4
 172    f04d.a202.7236    DYNAMIC     Gi1/0/5
 172    0024.e83b.c202    DYNAMIC     Gi1/0/7
 176    0024.e83b.c204    DYNAMIC     Gi1/0/8
 172    0024.e83b.9fa1    DYNAMIC     Gi1/0/9
 176    0024.e83b.9fa3    DYNAMIC     Gi1/0/10
 172    1866.daed.2c3d    DYNAMIC     Gi1/0/11
 172    fa16.3e9d.c446    DYNAMIC     Gi1/0/11
 172    1866.daed.2c3e    DYNAMIC     Gi1/0/12
 172    000a.f7b8.fef4    DYNAMIC     Gi1/0/13
 172    000a.f7b8.fef6    DYNAMIC     Gi1/0/14
 172    b82a.72d2.e59c    DYNAMIC     Gi1/0/15
 172    b82a.72d2.e59d    DYNAMIC     Gi1/0/16
 172    b82a.72d2.f0cd    DYNAMIC     Gi1/0/17
 172    b82a.72d2.f0ce    DYNAMIC     Gi1/0/18
 172    c81f.66ce.5b98    DYNAMIC     Gi1/0/19
 172    c81f.66ce.5b99    DYNAMIC     Gi1/0/20
 172    001e.c9ab.8294    DYNAMIC     Gi1/0/21

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 172    b82a.72d2.e59c    DYNAMIC     Gi1/0/3
 172    b82a.72d2.e59d    DYNAMIC     Gi1/0/4
 172    b82a.72d2.f0cd    DYNAMIC     Gi1/0/5
 172    b82a.72d2.f0ce    DYNAMIC     Gi1/0/6
 172    000a.f7b8.fef4    DYNAMIC     Gi1/0/15
  50    000a.f7b8.fef6    DYNAMIC     Gi1/0/16
  54    000a.f7b8.fef6    DYNAMIC     Gi1/0/16
 172    c81f.66ce.5b98    DYNAMIC     Gi1/0/21
  50    c81f.66ce.5b99    DYNAMIC     Gi1/0/22
  54    c81f.66ce.5b99    DYNAMIC     Gi1/0/22


Port      Name               Status       Vlan       Duplex  Speed Type
Gi1/0/2                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/3                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/4                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/5                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/6                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/8                      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/12                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/15                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/16  os-compute-13 C2   connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/18                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/20                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/21                     connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/22                     connected    trunk      a-full a-1000 10/100/1000BaseTX

Gi1/0/1                      notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/7                      notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/9                      notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/10                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/11                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/13                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/14                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/17                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/19                     notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/23                     notconnect   1            auto   auto 10/100/1000BaseTX

Gi1/0/24  vers COEUR         connected    trunk      a-full a-1000 10/100/1000BaseTX
Te1/0/1                      notconnect   1            full    10G Not Present
Te1/0/2                      notconnect   1            full    10G Not Present
Fa0                          disabled     routed       auto   auto 10/100BaseTX
