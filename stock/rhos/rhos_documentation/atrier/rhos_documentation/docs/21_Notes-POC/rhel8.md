


## Getsion des souscription 

Register

```bash
subscription-manager register
```

* [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_openstack_platform/7/html-single/director_installation_and_usage/index#sect-Registering_your_System](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_openstack_platform/7/html-single/director_installation_and_usage/index#sect-Registering_your_System)
* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation]


## Installation drivers aditionels (perc)

Doc pas a pas :

* [https://gainanov.pro/eng-blog/linux/rhel8-install-to-dell-raid/](https://gainanov.pro/eng-blog/linux/rhel8-install-to-dell-raid/)

Définir son type de driver :

* [http://elrepo.org/tiki/DeviceIDs](http://elrepo.org/tiki/DeviceIDs)



* https://serverfault.com/questions/1018399/centos-8-1-installer-cannot-see-sas-disks-attached-to-perc-6-i?rq=1
* https://www.youtube.com/watch?v=4fOAuXiynYM


* [https://access.redhat.com/discussions/3722151](https://access.redhat.com/discussions/3722151)
* [https://elrepo.org/linux/dud/el8/x86_64/](https://elrepo.org/linux/dud/el8/x86_64/)
* [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/updating-drivers-during-installation_installing-rhel-as-an-experienced-user#performing-an-assisted-driver-update_updating-drivers-during-installation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/updating-drivers-during-installation_installing-rhel-as-an-experienced-user#performing-an-assisted-driver-update_updating-drivers-during-installation)


* https://elrepo.org/bugs/view.php?id=927
* https://access.redhat.com/discussions/3722151
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/considerations_in_adopting_rhel_8/index#removed-adapters_hardware-enablement
* https://fatmin.com/2019/11/23/installing-rhel-8-1-on-dell-r710-r610-with-h700-raid-controller/
* http://elrepoproject.blogspot.com/2019/08/rhel-80-and-support-for-removed-adapters.html

## connexion avec clé SSH.

Pardéfaut la connexion par clé n'est pas activée. Et SE Linux bloque l'accès à  Could not open authorized keys '/root/.
ssh/authorized_keys': Permission denied


Dans le fichier /var/log/secure :

``` bash
 ... Could not open authorized keys '/root/.ssh/authorized_keys': Permission denied
```

Il faut donc modifier la config de sshd.

```bash
vi /etc/ssh/sshd_config

LogLevel DEBUG
PubkeyAuthentication yes

systemctl restart sshd
```

Et régler le pb au niveau de SElinux

```bash
restorecon -R -v /root/.ssh
#chcon -R -v system_u:object_r:usr_t:s0 ~/.ssh/
```


* [https://unix.stackexchange.com/questions/136877/selinux-preventing-ssh-via-public-key](https://unix.stackexchange.com/questions/136877/selinux-preventing-ssh-via-public-key)
* [https://massimoronca.it/2017/03/14/fixing-selinux-and-passwordless-ssh-authentication.html](https://massimoronca.it/2017/03/14/fixing-selinux-and-passwordless-ssh-authentication.html)
* [https://blog.tinned-software.net/ssh-key-authentication-is-not-working-selinux/0](https://blog.tinned-software.net/ssh-key-authentication-is-not-working-selinux/)

Rem : pour tester si c'est bien un pb avec SElinux :

```bash
sudo setenforce 0
``` 

* [https://linuxize.com/post/how-to-disable-selinux-on-centos-8/][https://linuxize.com/post/how-to-disable-selinux-on-centos-8/]
* [https://stackoverflow.com/questions/20688844/sshd-gives-error-could-not-open-authorized-keys-although-permissions-seem-corre](https://stackoverflow.com/questions/20688844/sshd-gives-error-could-not-open-authorized-keys-although-permissions-seem-corre)

