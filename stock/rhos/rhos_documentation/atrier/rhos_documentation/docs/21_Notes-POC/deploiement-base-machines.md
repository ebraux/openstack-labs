


## machines


os-director-01 -> OK
os-director-02 ->

os-bastion-01 -> OK

os-ceph-03 -> HS - erreur UCS
os-ceph-02 -> HS erreur PXE
os-ceph-03 -> OK



## Accès aux machines


```bash
J'ai basculé sur https:// 10.29.20.20/  le mode Console en HTML5 via le bouton  Paramètres   de la partie " Aperçu de la console virtuelle "
```

```bash
Pour les KVM IP :  Mdp = Di07Si      et il faut un Java qui fonctionne (java7 ca marche)

depuis RDP1 :
 j'ai une erreur comme quoi, il n'arrive pas à lancer java webstart avec Firefox. Mais avec IE ça passe.
```

## Config du Kickstart


### Prérequis : Accès au serveur Kickstart 

* au niveau réseau, il faut que le serveur sur lequel l'installation va être faite ait accès au serveur kickstart en HTTP, et en NFS. I
* Sur le serveur kickstart, il faut autoriser les accès depuis la plage IP du serveur à installer (fichier /etc/exports)

### Configuration DNS : 

* il faut que chaque serveur ait une entrée dans le DNS
* il faut aussi une entrée dans le Reverse-DNS, sinon le système Kickstart ne peut pas déterminer le nom du serveur qui fait la requete d'un fichier d'installation, et donc donne le fichier d'installation par défaut, qui ne marche pas.

### Configuration DHCP :

Install RHEL7.7. via PXE Kickstart

Modif du serveur DHCP, pour inclure sur chaque ligne des serveurs concernés :

```bash
next-server 10.29.89.3 ; filename "rh7-x86_64-install/pxelinux.0"
```

Puis lancer le script sur univers :

```bash
cd /usr/local/DHCPD/etc;
./RESTART
```

### Configuration Kickstart

Déclarer un fichier de configuration kickstart pour chaque serveur dans :

```bash
[root@srv-disi-b2-75 serveur]# pwd
/export/distrib/kickstart/RH/serveur
```


## Dépot RHEL8

version 8.2

``` bash
Faire un dépôt 8.1, je devrais pouvoir le faire (mais pas pour demain en tout cas).
Après, il faudra prévoir et tester le PXE.

Pour les DUD, je n'ai jamais eu besoin ; normalement un système récent comprends tout les drivers y compris pour les anciens modèles ... sauf que je viens de trouver avec R620+RHEL8.1 :


The whole situation regarding depracated SAS-2 controller/RAID drivers (including PERC/LSI/Megaraid) is discussed in a forum post here.https://access.redhat.com/discussions/3722151

The originator of the discussion thread (Art of Server) has generated a great youtube video on the workaround (sideloading old drivers in a Driver Update Disk available from ELRepo). The youtube video is here https://www.youtube.com/watch?v=4fOAuXiynYM&feature=youtu.be

Affects CentOS 8/8.1 as well as RHEL8.


Vraiment dommage. Du coup, je ne sais pas si un DUD peut être ajouté dans un boot PXE ....  va falloir chercher.
```

## Compatibilité machine

### machines récentes avec iDrac

Pour les m-a-j Firmware, maintenant on essaie de pointer sur le site de Dell directement. Dans l'iDRAC (qui doit être assez récent)  :

```bash
HTTP Address : downloads.dell.com
Catalog Location : /
Catalog Filename : Catalog.xml.gz
Avec PROXY :
Serveur : proxy.enst-bretagne.fr
Port : 8080
user : toto (ou n'importe quelle chaîne)
password : toto (ou n'importe quelle chaîne)
type : HTTP
```

### Machine sans iDrac, avec DeLL Build Utility <F10>

Si pas d'iDRAC, alors la, compliqué ....
Dans le Boot PXE, il devrait toujours y avoir une entrée "Outils DELL --> DELL Build and Update Utility". En fonction des serveurs, qquefois, ca boote, qquesfois, non.

Si oui, tu devrais avoir un menu style iDRAC et pointer sur le dépot local des firmware accessible en NFS (si je me rappelle bien, cela ne marche pas avec le site Dell en direct )

```bash
Serveur NFS : 10.29.88.16
Chemin :  /export/distrib/public/dell/latest
Répertoire du repository :  repository
```

### Machine sans iDrac, sans DeLL Build Utility

Il faut un OS widows ou linux.
Telechanger les msies à jour de firmWare, et le slancer depuis l'OS

Par exemple, sous linux

```bash
cd
wget http://..../fichier.BIN   
chmod -X fichier.BIN
./fichier.BIN
```
