
    - import_role:
        name: ceph-defaults
      tags: [with_pkg, fetch_container_image]
    - import_role:
        name: ceph-facts
    - import_role:
        name: ceph-validate
    - import_role:
        name: ceph-infra
    - import_role:
        name: ceph-handler
    - import_role:
        name: ceph-container-engine
      tags: with_pkg
    - import_role:
        name: ceph-container-common
      tags: fetch_container_image
      when: (group_names != ['clients']) or (inventory_hostname == groups.get('clients', [''])|first)



ceph_origin: dummy
valid_ceph_origins:
  - repository
  - distro
  - local


ceph_repository: dummy
valid_ceph_repository:
  - community
  - rhcs
  - dev
  - uca
  - custom
  - obs




---
/usr/share/ceph-ansible/roles/ceph-defaults/vars/main.yml
ceph_osd_pool_default_size: 3
ceph_osd_pool_default_pg_num: 8
ceph_osd_pool_default_crush_rule: -1



/usr/share/ceph-ansible/roles/ceph-defaults/defaults/main.yml

ceph_mirror: http://download.ceph.com
ceph_stable_key: https://download.ceph.com/keys/release.asc
ceph_stable_release: dummy
ceph_stable_repo: "{{ ceph_mirror }}/debian-{{ ceph_stable_release }}"


ceph_rhcs_version: "{{ ceph_stable_rh_storage_version | default(3) }}"
valid_ceph_repository_type:
  - cdn
  - iso
ceph_rhcs_iso_path: "{{ ceph_stable_rh_storage_iso_path | default('') }}"
ceph_rhcs_mount_path: "{{ ceph_stable_rh_storage_mount_path | default('/tmp/rh-storage-mount') }}"
ceph_rhcs_repository_path: "{{ ceph_stable_rh_storage_repository_path | default('/tmp/rh-storage-repo') }}" # where to copy iso's content

# RHCS installation in Debian systems
ceph_rhcs_cdn_debian_repo: https://customername:customerpasswd@rhcs.download.redhat.com
ceph_rhcs_cdn_debian_repo_version: "/3-release/" # for GA, later for updates use /3-updates/





##########
# DOCKER #
##########
container_binary: docker
docker_exec_cmd:
docker: false
ceph_docker_image: "ceph/daemon"
ceph_docker_image_tag: latest-mimic
ceph_docker_registry: docker.io
## Client only docker image - defaults to {{ ceph_docker_image }}
ceph_client_docker_image: "{{ ceph_docker_image }}"
ceph_client_docker_image_tag: "{{ ceph_docker_image_tag }}"
ceph_client_docker_registry: "{{ ceph_docker_registry }}"
ceph_docker_enable_centos_extra_repo: false
ceph_docker_on_openstack: false
containerized_deployment: False





#############
# OPENSTACK #
#############
openstack_config: false
openstack_glance_pool:
  name: "images"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_cinder_pool:
  name: "volumes"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_nova_pool:
  name: "vms"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_cinder_backup_pool:
  name: "backups"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_gnocchi_pool:
  name: "metrics"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_cephfs_data_pool:
  name: "manila_data"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_cephfs_metadata_pool:
  name: "manila_metadata"
  pg_num: "{{ osd_pool_default_pg_num }}"
  pgp_num: "{{ osd_pool_default_pg_num }}"
  rule_name: "replicated_rule"
  type: 1
  erasure_profile: ""
  expected_num_objects: ""
  application: "rbd"
  size: "{{ osd_pool_default_size }}"
openstack_pools:
  - "{{ openstack_glance_pool }}"
  - "{{ openstack_cinder_pool }}"
  - "{{ openstack_nova_pool }}"
  - "{{ openstack_cinder_backup_pool }}"
  - "{{ openstack_gnocchi_pool }}"
  - "{{ openstack_cephfs_data_pool }}"
  - "{{ openstack_cephfs_metadata_pool }}"

# The value for 'key' can be a pre-generated key,
# e.g key: "AQDC2UxZH4yeLhAAgTaZb+4wDUlYOsr1OfZSpQ=="
# By default, keys will be auto-generated.
#
openstack_keys:
  - { name: client.glance, caps: { mon: "profile rbd", osd: "profile rbd pool=volumes, profile rbd pool={{ openstack_glance_pool.name }}"}, mode: "0600" }
  - { name: client.cinder, caps: { mon: "profile rbd", osd: "profile rbd pool={{ openstack_cinder_pool.name }}, profile rbd pool={{ openstack_nova_pool.name }}, profile rbd pool={{ openstack_glance_pool.name }}"}, mode: "0600" }
  - { name: client.cinder-backup, caps: { mon: "profile rbd", osd: "profile rbd pool={{ openstack_cinder_backup_pool.name }}"}, mode: "0600" }
  - { name: client.gnocchi, caps: { mon: "profile rbd", osd: "profile rbd pool={{ openstack_gnocchi_pool.name }}"}, mode: "0600", }
  - { name: client.openstack, caps: { mon: "profile rbd", osd: "profile rbd pool={{ openstack_glance_pool.name }}, profile rbd pool={{ openstack_nova_pool.name }}, profile rbd pool={{ openstack_cinder_pool.name }}, profile rbd pool={{ openstack_cinder_backup_pool.name }}"}, mode: "0600" }


