

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_getting_started_guide/sec-virtualization_getting_started-quickstart_virt-manager-create_vm




## Prérequis

Une machine avec RHEL 7 installé, et enregistré chez redhat.

Souscription : 

Config Machine

Config réseau
* un accès à la machine
* un accés au réseau "Provisioning or Control Plane network,"  (ssh + ansible vers les serveurs)
* un accés au réseau "external" ( enables access to OpenStack Platform repositories, container image sources, and other servers such as DNS servers or NTP servers. Use this network for standard access the undercloud from your workstation. You must manually configure an interface on the undercloud to access the external network.)

zattention, Director utilise la première IP de la zone ctrl-plane pour le sservice sinternes comme rabbitMq
http://10.129.172.128:15672


* Do not use the same Provisioning or Control Plane NIC as the one that you use to access the director machine from your workstation. The director installation creates a bridge by using the Provisioning NIC, which drops any remote connections. Use the External NIC for remote connections to the director system.

* The Provisioning network requires an IP range that fits your environment size. Use the following guidelines to determine the total number of IP addresses to include in this range:
    * Include at least one temporary IP address for each node that connects to the Provisioning network during introspection.
    * Include at least one permanent IP address for each node that connects to the Provisioning network during deployment.
    * Include an extra IP address for the virtual IP of the overcloud high availability cluster on the Provisioning network.
    * Include additional IP addresses within this range for scaling the environment.


## Procédure

### Principe

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/)







Pour info, le fichier de config du subscription manager est  '/etc/rhsm/rhsm.conf'. on peut configurer
Installation des packages pour Director

```bash
yum install -y python-tripleoclient
```


## configuration de l'installation


```bash
su - stack
cp /usr/share/instack-undercloud/undercloud.conf.sample ~/undercloud.conf-ref
cp /usr/share/instack-undercloud/undercloud.conf.sample ~/undercloud.conf
```

undercloud.conf

```bash
[DEFAULT]

undercloud_hostname = os-director-01.imta.fr

local_interface = eth1
local_subnet = ctlplane-subnet
local_ip = 10.129.172.5/24
local_mtu = 1500

undercloud_public_host = 10.129.172.2
undercloud_admin_host =  10.129.172.3
undercloud_service_certificate = /etc/pki/instack-certs/undercloud.pem

undercloud_nameservers = 192.44.75.10,192.108.115.2
undercloud_ntp_servers = ntp1.svc.enst-bretagne.fr,ntp3.svc.enst-bretagne.fr

overcloud_domain_name = localdomain

subnets = ctlplane-subnet

inspection_interface = br-ctlplane
undercloud_update_packages = true

enable_ui = true

clean_nodes = true

[auth]

[ctlplane-subnet]
cidr = 10.129.172.0/24
gateway = 10.129.172.1
dhcp_start = 10.129.172.100
dhcp_end = 10.129.172.199
inspection_iprange = 10.129.172.90,10.129.172.99

```


## Intégration de CEPH

Avec RHOS 13 :  "regardless of external cluster being rhcs3 or rhcs4, on the osp13 undercloud we need to install ceph-ansible from rhcs3 ... which supports and is tested with the version of ansible and python shipping with osp13"

```bash

yum list \*ceph-ansible\*
yum list \*ansible\*

sudo subscription-manager repos --enable=rhel-7-server-rhceph-3-tools-rpms
sudo yum --showduplicates list ceph-ansible | expand
sudo yum --showduplicates list ansible | expand

sudo yum install -y ceph-ansible-3.2.49-1.el7cp.noarch 
sudo yum downgrade ansible-2.6.20-1.el7ae.noarch
```

```bash

 ...
ceph-ansible.noarch   3.1.5-1.el7cp           rhel-7-server-openstack-13-rpms   
 ...
ceph-ansible.noarch   4.0.25.2-1.el7cp        rhel-7-server-rhceph-4-tools-rpms 

yum list \*ceph-ansible\*
Paquets installés
ceph-ansible.noarch                                                                              4.0.25.2-1.el7cp                                                                               rhel-7-server-rhceph-4-tools-rpms

yum downgrade ceph-ansible-3.1.5-1.el7cp.noarch


Paquets installés
ceph-ansible.noarch                                                                              3.1.5-1.el7cp                                                                                  @rhel-7-server-openstack-13-rpms 
Paquets disponibles
ceph-ansible.noarch                                                                              4.0.25.2-1.el7cp                                                                               rhel-7-server-rhceph-4-tools-rpms
```


sudo yum --showduplicates list ansible | expand




## lancement de l'installation

Vérifier la config du proxy dans les fichier "/etc/profile.d/setproxy.sh" et "/etc/environment", principalment "no_proxy" pour les valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway".


Ouvrir un nouveau shell

```bash
sudo su - stack

export | grep proxy

export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,10.129.41.130,10.129.173.2,10.129.173.3,10.129.173.5
openstack undercloud install
```

```bash
#############################################################################
Undercloud install complete.
The file containing this installation's passwords is at /home/stack/undercloud-passwords.conf.
There is also a stackrc file at /home/stack/stackrc.

```

```bash
2020-07-29 08:12:51,429 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 99-refresh-completed completed
2020-07-29 08:12:51,430 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 ----------------------- PROFILING -----------------------
2020-07-29 08:12:51,431 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,434 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 Target: post-configure.d
2020-07-29 08:12:51,435 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,436 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 Script                                     Seconds
2020-07-29 08:12:51,437 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 ---------------------------------------  ----------
2020-07-29 08:12:51,438 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,446 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 10-iptables                                   0.005
2020-07-29 08:12:51,450 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 80-seedstack-masquerade                       0.036
2020-07-29 08:12:51,455 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 98-undercloud-setup                           5.889
2020-07-29 08:12:51,460 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 99-refresh-completed                          0.405
2020-07-29 08:12:51,462 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,463 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 --------------------- END PROFILING ---------------------

/usr/share/instack-undercloud/puppet-stack-config/os-refresh-config/post-configure.d/10-iptables
/usr/libexec/os-refresh-config/post-configure.d/10-iptables


```

```bash
cat undercloud-passwords.conf
cat stackrc

```


### vérification

```bash

tail -f /home/stack/.instack/install-undercloud.log 
sudo systemctl list-units openstack-*

```


## ménage si besoin


### Infos config réseau

```
 ip addr

2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.41.130/24 brd 10.129.41.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever

3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
    inet6 fe80::1a66:daff:feed:2c3d/64 scope link 
       valid_lft forever preferred_lft forever

6: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether a6:75:4a:df:5d:ad brd ff:ff:ff:ff:ff:ff

7: tap7e3fb6c6-44: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 76:03:67:17:7d:ab brd ff:ff:ff:ff:ff:ff

8: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 8e:45:b6:36:5b:4c brd ff:ff:ff:ff:ff:ff

9: tap6876f5c8-44: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether d6:52:7e:56:2e:e3 brd ff:ff:ff:ff:ff:ff

10: br-ctlplane: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.5/24 brd 10.129.172.255 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.172.3/32 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.172.2/32 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3d/64 scope link 
       valid_lft forever preferred_lft forever

11: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:e8:26:66:40 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
```

```bash
 ip route
default via 10.129.41.1 dev eth0 
10.129.41.0/24 dev eth0 proto kernel scope link src 10.129.41.130 
10.129.172.0/24 dev br-ctlplane proto kernel scope link src 10.129.172.5 
169.254.0.0/16 dev eth0 scope link metric 1002 
169.254.0.0/16 dev eth1 scope link metric 1003 
169.254.0.0/16 dev br-ctlplane scope link metric 1010 
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 
```



## CONFIGURING A CONTAINER IMAGE SOURCE

Using the undercloud as a local registry

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#Configuring-Registry_Details-Local

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#container-images-additional-services](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#container-images-additional-services)

```bash
cd
 
# vérifier que la reistry locale fonctionne sur "undercloud_admin_host"
sudo netstat -ltun | grep 8787
curl http://10.129.172.3:8787/v2/_catalog

openstack overcloud container image prepare \
  --namespace=registry.access.redhat.com/rhosp13 \
  --push-destination=10.129.172.3:8787 \
  --prefix=openstack- \
  -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/octavia.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/ironic.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml \
  --tag-from-label {version}-{release} \
  --output-env-file=/home/stack/templates/overcloud_images.yaml \
  --output-images-file /home/stack/local_registry_images.yaml
# attention, la commande est peu longye, sans barre de progression


rem :On ajoute "-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml" pour intégrer une instance CEPH externe
#-e /usr/share/openstack-tripleo-heat-templates/environments/manila-cephfsnative-config.yaml \

#openstack overcloud container image prepare \
#  --namespace=registry.access.redhat.com/rhosp13 \
#  --push-destination=10.129.173.3:8787 \
#  --prefix=openstack- \
#  -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
#  --set ceph_image=rhceph-4-rhel7 \
#  --output-env-file=/home/stack/templates/ceph_images.yaml \
#  --output-images-file /home/stack/ceph_local_registry_images.yaml

  
 

#curl http://10.129.172.129:8787/v1/_ping
#curl http://10.129.173.3/:8787/v2/_catalog | jq .repositories[]

cat local_registry_images.yaml
cat templates/overcloud_images.yaml
```

edit local_registry_images.yaml
```bash
ContainerImageRegistryLogin: true
ContainerImageRegistryCredentials:
  registry.access.redhat.com:
    DISI1: DiSi2003
```

```bash
sudo docker login registry.access.redhat.com
sudo openstack overcloud container image upload \
  --config-file  /home/stack/local_registry_images.yaml \
  --verbose
#fin avec un message un peu bizarre "END return value: None", mais c'ets bon
```
Vérification

```bash
curl http://10.129.173.3:8787/v2/_catalog | jq .repositories[]
...
"rhosp13/openstack-keystone"
...

curl -s http://10.129.173.3:8787/v2/rhosp13/openstack-keystone/tags/list | jq .tags
[
  "13.0-116"
]

skopeo inspect --tls-verify=false docker://10.129.173.3:8787/rhosp13/openstack-keystone:13.0-116
{
    "Name": "10.129.173.1:8787/rhosp13/openstack-keystone",
...
```

## mise à jour de la config

```bash
openstack undercloud upgrade
```

## Intégration de CEPH

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index

installation de "ceph-ansible" et intégration de la config globale "/usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml" déjà réalisée.
```

Il faut faire un fichier de config "/home/stack/templates/ceph-config.yaml", la config globale est dans ".

```bash
parameter_defaults:
  CephClientKey: AQA86R5fdxTeGhAAUvLToVVr+Y3suSrtzecDxw==
  CephClusterFSID: 36eea4f3-07cb-41a0-8ead-c550fec0b0e4
  CephExternalMonHost: 172.16.176.10, 172.16.176.11, 172.16.176.12
  CephClientUserName: openstack
  NovaRbdPoolName: vms
  CinderRbdPoolName: volumes
  GlanceRbdPoolName: images
  CinderBackupRbdPoolName: backups
  GnocchiRbdPoolName: metrics
```

## CHAPTER 6. CONFIGURING A BASIC OVERCLOUD WITH THE CLI TOOLS
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-configuring_basic_overcloud_requirements_with_the_cli_tools


Tester que IPMI est bien actvé sur un noeud distant
``` bash
ipmitool -I lanplus -H 10.29.20.32 -U root -P ospoc2020 sdr elist all
```
* https://techexpert.tips/fr/dell-idrac-fr/ipmi-sur-linterface-idrac/
* https://osric.com/chris/accidental-developer/2017/10/using-ipmitool-to-configure-dell-idrac/


``` bash
sudo su - stack
source ~/stackrc
```

création du fichier listant les hotes

``` bash
touch ~/instackenv.json
```

Exemple de fichier minimum
``` bash
{
    "nodes":[
        {
            "mac":[
                "C8:1F:66:CE:5B:98"
            ],
            "name":"os-poc-ctrl-01",
            "pm_addr":"10.29.20.31",
            "pm_type":"ipmi",
            "arch":"x86_64"
        }
    ]
}
```

Validation du fichier
``` bash
openstack overcloud node import --validate-only ~/instackenv.json
```


```bash
# ---

openstack baremetal node list 
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| e9095481-6f94-4a2e-bafd-9402f2a5c629 | os-poc-ctrl-01 | None          | power off   | manageable         | False       |
| 131f0f0e-4e57-4930-8ecf-53d97b4f22db | os-poc-ctrl-02 | None          | power on    | manageable         | False       |
| b14fd7f0-3459-4f3f-b2b9-a444bcfa0627 | os-poc-ctrl-03 | None          | power on    | manageable         | False       |
| 77dde3a1-9e11-4083-b32c-fd8d18c5e994 | os-poc-comp-01 | None          | power off   | manageable         | False       |
| ac1e3482-94f0-4b1a-87bd-c6f7574fb34e | os-poc-comp-02 | None          | power off   | manageable         | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+

# ---
Name: os-poc-ctrl-01
cycle de boot: message bbattery PERC HS, mais non bloquant
UUID: e9095481-6f94-4a2e-bafd-9402f2a5c629
root_device.serial: 6c81f660d1d33400216f34ae06a7bbe5

# ---
Name: os-poc-ctrl-02
cycle de boot: OK. (Lyfecycle controller a désactiver)
UUID: 131f0f0e-4e57-4930-8ecf-53d97b4f22db
root_device.serial: 6b083fe0d31ab400241515dd06792e14

# ---
Name: os-poc-ctrl-03
UUID: b14fd7f0-3459-4f3f-b2b9-a444bcfa0627
root_device.serial: 6b083fe0d32fd5002415247d05a01c68

# ---
Name: os-poc-comp-01
UUID: 77dde3a1-9e11-4083-b32c-fd8d18c5e994
root_device.serial: 6847beb0d517f70024114fdb0545859e

# ---
Name: os-poc-comp-02
UUID: ac1e3482-94f0-4b1a-87bd-c6f7574fb34e
root_device.serial:

# ---



### suivre les opérations, debug inspector :

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-troubleshooting_director_issues

```

```bash
(undercloud) [stack@os-director-01 ~]$ openstack workflow execution output show 536e0e7e-d226-48f6-bc04-e6d2ceaa5b73
{
    "result": "Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state \"manageable\", the state is \"enroll\", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status."
}
```

openstack baremetal node list
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| 1dcd9a53-a299-4476-b751-630b2612e196 | os-poc-ctrl-01 | None          | None        | enroll             | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+


```bash
Exception registering nodes: {u'status': u'FAILED', u'message': [{u'result': u'Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state "manageable", the state is "enroll", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status.'}], u'result': None}
clean_up ImportNode: Exception registering nodes: {u'status': u'FAILED', u'message': [{u'result': u'Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state "manageable", the state is "enroll", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status.'}], u'result': None}
```
Node 1dcd9a53-a299-4476-b751-630b2612e196

```bash

sudo journalctl -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq

sudo systemctl  openstack-ironic-inspector
/usr/bin/python2 /usr/bin/ironic-inspector --config-file /etc/ironic-inspector/inspector-dist.conf --config-file /etc/ironic-inspector/inspector.conf

sudo systemctl  status openstack-ironic-inspector-dnsmasq
/sbin/dnsmasq --conf-file=/etc/ironic-inspector/dnsmasq.conf


log_dir=/var/log/ironic-inspector

```

Apparement, on peut tagger les node, et lancer des inspection ciblées : 
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-Automatic_Profile_Tagging
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Tagging_Nodes_into_Profiles



## Configuration de l'undercloud



## se conneter à 'interface de gestion

```bash
netstat -ltunp | grep 443
tcp        0      0 10.129.172.2:443        0.0.0.0:*               LISTEN      10058/haproxy  
```

dans config Haproxy :

```bash
listen ui
  bind 10.129.172.2:443 transparent ssl crt /etc/pki/instack-certs/undercloud.pem
  bind 10.129.172.3:3000 transparent
  mode http
  option forwardfor
  redirect scheme https code 301 if { hdr(host) -i 10.129.172.2 } !{ ssl_fc }
  rsprep ^Location:\ http://(.*) Location:\ https://\1
  timeout tunnel 3600s
  server 10.129.172.5 10.129.172.5:3000 check fall 5 inter 2000 rise 2
```


Récupération du mot de passe

```bash
sudo su - stack
Dernière connexion : jeudi 30 juillet 2020 à 00:06:02 CEST sur pts/0
[stack@os-director-01 ~]$ sudo hiera admin_password
fbbf3ef5ccd25da0be38cf979623a4c273017e01


```



---
Configuration idrac

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-Power_Management_Drivers

```bash
B.2. Dell Remote Access Controller (DRAC)
DRAC is an interface that provides out-of-band remote management features including power management and server monitoring.

pm_type
Set this option to idrac.
pm_user; pm_password
The DRAC username and password.
pm_addr
The IP address of the DRAC host.

```
# ---

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/planning-your-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/planning-your-undercloud)






# Infos complémentaires

## Vérification de la config de CEPH

https://access.redhat.com/solutions/3509271
https://access.redhat.com/solutions/3676921
https://bugzilla.redhat.com/show_bug.cgi?id=1645134


Vérifier lq config de CEPH

sur le "director"

``` bash
sudo su - stack
source ~/stackrc

swift list
swift list overcloud_ceph_ansible_fetch_dir
  temporary_dir-20200803-152514.tar.gz
```


## Personnalisation d'un compute

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/10/html-single/advanced_overcloud_customization/index#sect-Customizing_Hieradata_for_Individual_Nodes

## Introspaction des noeuds


Suivre l'introspection

```bash
sudo journalctl -l -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq -u openstack-ironic-conductor -f
```

Import des nodes (Add nodes to Ironic)
```bash
openstack overcloud node import ~/instackenv.json
```

List des nodes
```bash
openstack baremetal node list

```

Inspection des nodes

```bash
# pour tous
openstack overcloud node introspect --all-manageable --provide

# le "Provisioning State" passe de "manageable" à "available"
# pour un noeud spécifique

openstack overcloud node introspect 265f71e3-7d58-45ef-999d-c5abee932d51 --provide
openstack overcloud node introspect --run-validations 265f71e3-7d58-45ef-999d-c5abee932d51
# 15 miniute par noeud
```

```bash
Waiting for messages on queue 'tripleo' with no timeout.
Introspection of node 2c7e3680-5251-4d40-bb18-b103480cf4ef completed. Status:SUCCESS. Errors:None
Successfully introspected 1 node(s).
...

Introspection completed.
Started Mistral Workflow tripleo.baremetal.v1.provide_manageable_nodes. Execution ID: 6bd4bd4c-9047-4bd3-8a1a-f591a6d695dc
Waiting for messages on queue 'tripleo' with no timeout.

1 node(s) successfully moved to the "available" state.

```


Voir le résultat

```bash
openstack baremetal node list 
openstack baremetal introspection data save <UUID> | jq .

# lister les interfaces, avec infos de config switch (LLDP)
openstack baremetal introspection interface list [NODE UUID]
openstack baremetal introspection interface show [NODE UUID] [INTERFACE]

```
