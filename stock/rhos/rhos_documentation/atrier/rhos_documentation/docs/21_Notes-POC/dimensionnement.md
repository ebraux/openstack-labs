# Dimensionnement 

## Director
* 64 bits x86 à 8 cœurs  [2 CPU - 4 cores / CPU min]
* 24 Go de RAM au minimum  [64 Go RAM Minimum. Selon les services déployés sur le serveur en plus de Director, 128 Go est recommandé.]
* 100 Go d'espace disque disponible au minimum (10 Go requis pour une mise à jour ou un
déploiement de type overcloud) [2 SSD 120 Go en RAID 1 pour l’OS, Disques SATA RAID 500 Go utiles min pour les services déployés (notamment dépôts locaux)]
* 2 cartes d'interface réseau 1 Gbit/s au minimum (10 Gbit/s recommandés pour l'approvisionnement du trafic réseau, en particulier si vous approvisionnez un grand nombre de nœuds dans un environnement de type overcloud) [ 4 cartes  1Gb/s min: 1 pour l’accès au réseau iPMI, 1 pour le provisionning,  2 en bounding pour les accès externes ]

## Control-plane et moniteur CEPH
* [2 CPU / (10 cores / CPU)]
* 32 Go de RAM au minimum (64 Go recommandés pour des performances optimales) [128 Go RAM]
* 40 Go d'espace disque disponible au minimum  [2 SSD 120 Go en RAID 1 pour l’OS]
* 2 cartes d'interface réseau 1 Gbit/s [7 cartes réseaux :  2 pour le storage management 10Gb/s ??, 2 pour l'accès stockage, 1 pour le provisionning, 2 mutualisées (interfaces virtuelles sur VLAN taggués)]

## Compute
* [2 CPU - 14 core / CPU]
* 6 Go de RAM au minimum (RAM supplémentaire à prévoir selon la quantité de mémoire que
l'utilisateur souhaite allouer aux instances de machines virtuelles) [RAM : 128 Go (ou moins selon les charges à déployer)]
* 40 Go d'espace disque disponible au minimum (1 To recommandé) [2 SSD 120 Go pour l’OS en RAID 1, Le stockage des charges de travail et inhérent aux fonctionnalités de OpenStack sera fourni
par Ceph]
* 2 cartes d'interface réseau 1 Gbit/s (au moins 2 cartes d'interface réseau recommandées pour
les environnements de production) [5 cartes réseaux: 2 pour l'accès stockage, 1 pour le provisionning, 2 mutualisées (interfaces virtuelles sur VLAN taggués)]
* Chaque nœud de calcul nécessite une interface IPMI (Intelligent Platform Management
Interface) sur la carte mère du serveur

## Serveurs de stockage (OSD Ceph)
* [CPU: 2 sockets / 1 core par OSD (disque de stockage)]
* [ 3 cartes 1Gb/s + 2 cartes 10Gb/s min : 2 pour l'accès stockage, 2 cartes 10Gb/s (Storage Management), 1 pour le provisionning ]
* RAM: 64 Go min, recommandé 128 Go
* Disques : 2 SSD 120Go RAID 1 pour l’OS, N Disques de stockage avec des groupes de disques de références et capacités homogènes (ou un seul groupe), pour 4 à 6 disques de stockage: 1 SSD de 10% de la capacité de chaque OSD pour les journaux CEPH (toujours d'actualité les journanux sur un SSD ??)

