## configuration des disques avec percli


si besoin RAID

* Create a single RAID 0 volume with write-back for each Ceph OSD data drive with write-back cache enabled.
*  Enabling pass-through mode helps avoid caching logic, and generally results in much lower latency for fast media.

```bash

cd 
mkdir perccli
cd perccli
wget https://dl.dell.com/FOLDER04470715M/1/perccli_7.1-007.0127_linux.tar.gz .
tar -xzvf perccli_7.1-007.0127_linux.tar.gz
rpm -ivh Linux/perccli-007.0127.0000.0000-1.noarch.rpm

ls /opt/MegaRAID/
```

### type de carte
```bash

/opt/MegaRAID/perccli/perccli64 show

ceph-poc-01
-------------------------------------------------------------------------------
Ctl Model              Ports PDs DGs DNOpt VDs VNOpt BBU sPR DS EHS ASOs Hlth  
-------------------------------------------------------------------------------
  0 PERCH700Integrated     8   3   1     0   1     0 Fld On  -  N      0 NdAtn 
-------------------------------------------------------------------------------

ceph-poc-02
-----------------------------------------------------------------------------
Ctl Model             Ports PDs DGs DNOpt VDs VNOpt BBU sPR DS EHS ASOs Hlth 
-----------------------------------------------------------------------------
  0 PERC6/iIntegrated     8   3   2     0   2     0 Opt On  2  N      0 Opt  
-----------------------------------------------------------------------------

ceph-poc-03
------------------------------------------------------------------------------
Ctl Model              Ports PDs DGs DNOpt VDs VNOpt BBU sPR DS EHS ASOs Hlth 
------------------------------------------------------------------------------
  0 PERCH700Integrated     8   3   1     0   1     0 Opt On  -  N      0 Opt  
------------------------------------------------------------------------------

Ctl=Controller Index|DGs=Drive groups|VDs=Virtual drives|Fld=Failed
PDs=Physical drives|DNOpt=DG NotOptimal|VNOpt=VD NotOptimal|Opt=Optimal
Msng=Missing|Dgd=Degraded|NdAtn=Need Attention|Unkwn=Unknown
sPR=Scheduled Patrol Read|DS=DimmerSwitch|EHS=Emergency Hot Spare
Y=Yes|N=No|ASOs=Advanced Software Options|BBU=Battery backup unit
Hlth=Health|Safe=Safe-mode boot

```

### Info sur les disque et les disque virtuels

```bash

/opt/MegaRAID/perccli/perccli64 /c0/e32/sall show
-------------------------------------------------------------------------
EID:Slt DID State DG       Size Intf Med SED PI SeSz Model            Sp 
-------------------------------------------------------------------------
32:0      0 Onln   0 136.125 GB SAS  HDD N   N  512B ST3146356SS      U  
32:1      1 Onln   0 136.125 GB SAS  HDD N   N  512B ST3146855SS      U  
32:2      2 Onln   1 136.125 GB SAS  HDD N   N  512B ST3146356SS      U  
-------------------------------------------------------------------------

# Virtual Drives :
/opt/MegaRAID/perccli/perccli64 /c0/vall show
---------------------------------------------------------------
DG/VD TYPE  State Access Consist Cache Cac sCC       Size Name 
---------------------------------------------------------------
0/0   RAID1 Optl  RW     Yes     NRWBD -   OFF 136.125 GB sys  
1/1   RAID0 Optl  RW     Yes     NRWBD -   OFF 136.125 GB data 
```

### Suppression du rAId

```bash

/opt/MegaRAID/perccli/perccli64 /c0/v1 del


/opt/MegaRAID/perccli/perccli64 /c0/e32/sall show
...
32:2      2 UGood  - 136.125 GB SAS  HDD N   N  512B ST3146356SS      U  

/opt/MegaRAID/perccli/perccli64 /c0/e32/s2 set jbod

Description = Set Drive JBOD Failed.
------------------------------------------------
Drive      Status  ErrCd ErrMsg                 
------------------------------------------------
/c0/e32/s2 Failure   255 Operation not allowed. 
------------------------------------------------
```

shttp://l4u-00.jinr.ru/pub/misc/h-w/LSI/dell-sas-hba-12gbps_reference-guide_en-us.pdf

autres

``` bash
/opt/MegaRAID/perccli/perccli64  /c0 /eall /sall show all 

Drive /c0/e32/s0 :
-------------------------------------------------------------------------
EID:Slt DID State DG       Size Intf Med SED PI SeSz Model            Sp 
-------------------------------------------------------------------------
32:0      0 Onln   0 136.125 GB SAS  HDD N   N  512B ST3146356SS      U  
-------------------------------------------------------------------------

Drive /c0/e32/s1 :
-------------------------------------------------------------------------
EID:Slt DID State DG       Size Intf Med SED PI SeSz Model            Sp 
-------------------------------------------------------------------------
32:1      1 Onln   0 136.125 GB SAS  HDD N   N  512B ST3146855SS      U  
-------------------------------------------------------------------------

Drive /c0/e32/s2 :
-------------------------------------------------------------------------
EID:Slt DID State DG       Size Intf Med SED PI SeSz Model            Sp 
-------------------------------------------------------------------------
32:2      2 Onln   1 136.125 GB SAS  HDD N   N  512B ST3146356SS      U  
-------------------------------------------------------------------------


EID-Enclosure Device ID|Slt-Slot No.|DID-Device ID|DG-DriveGroup
DHS-Dedicated Hot Spare|UGood-Unconfigured Good|GHS-Global Hotspare
UBad-Unconfigured Bad|Onln-Online|Offln-Offline|Intf-Interface
Med-Media Type|SED-Self Encryptive Drive|PI-Protection Info
SeSz-Sector Size|Sp-Spun|U-Up|D-Down/PowerSave|T-Transition|F-Foreign
UGUnsp-Unsupported|UGShld-UnConfigured shielded|HSPShld-Hotspare shielded
CFShld-Configured shielded|Cpybck-CopyBack|CBShld-Copyback Shielded




/opt/MegaRAID/perccli/perccli64 -LDInfo -Lall -aALL


Adapter 0 -- Virtual Drive Information:
Virtual Drive: 0 (Target Id: 0)
Name                :sys
RAID Level          : Primary-1, Secondary-0, RAID Level Qualifier-0
Size                : 136.125 GB
 ...
Virtual Drive: 1 (Target Id: 1)
Name                :data
RAID Level          : Primary-0, Secondary-0, RAID Level Qualifier-0
Size                : 136.125 GB
...


```
