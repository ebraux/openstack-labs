


Erreur :

```bash
2020-08-31 12:07:53.079 8 ERROR nova.compute.manager [req-a161d93a-9a22-44bb-a32e-a2d5b42b02d3 - - - - -] No compute node record for host overcloud-compute-0.localdomain: ComputeHostNotFound_Remote: Compute host overcloud-compute-0.localdomain could not be found.
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager [req-a161d93a-9a22-44bb-a32e-a2d5b42b02d3 - - - - -] Error updating resources for node overcloud-compute-0.localdomain.: ProcessExecutionError: Unexpected error while running command.
Command: ceph df --format=json --id openstack --conf /etc/ceph/ceph.conf
Exit code: 1
Stdout: u''
Stderr: u'2020-08-31 12:07:53.221411 7f995883d700 -1 auth: unable to find a keyring on /etc/ceph/ceph.client.openstack.keyring,/etc/ceph/ceph.keyring,/etc/ceph/keyring,/etc/ceph/keyring.bin,: (2) No such file or directory\n2020-08-31 12:07:53.223610 7f995883d700 -1 monclient: authenticate NOTE: no keyring found; disabled cephx authentication\n2020-08-31 12:07:53.223617 7f995883d700  0 librados: client.openstack authentication error (95) Operation not supported\n[errno 95] error connecting to the cluster\n'
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager Traceback (most recent call last):
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/compute/manager.py", line 7783, in update_available_resource_for_node
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     rt.update_available_resource(context, nodename)
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/compute/resource_tracker.py", line 704, in update_available_resource
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     resources = self.driver.get_available_resource(nodename)
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/virt/libvirt/driver.py", line 6532, in get_available_resource
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     disk_info_dict = self._get_local_gb_info()
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/virt/libvirt/driver.py", line 5799, in _get_local_gb_info
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     info = LibvirtDriver._get_rbd_driver().get_pool_info()
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/virt/libvirt/storage/rbd_utils.py", line 377, in get_pool_info
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     out, _ = utils.execute(*args)
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/nova/utils.py", line 231, in execute
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     return processutils.execute(*cmd, **kwargs)
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager   File "/usr/lib/python2.7/site-packages/oslo_concurrency/processutils.py", line 424, in execute
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager     cmd=sanitized_cmd)
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager ProcessExecutionError: Unexpected error while running command.
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager Command: ceph df --format=json --id openstack --conf /etc/ceph/ceph.conf
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager Exit code: 1
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager Stdout: u''
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager Stderr: u'2020-08-31 12:07:53.221411 7f995883d700 -1 auth: unable to find a keyring on /etc/ceph/ceph.client.openstack.keyring,/etc/ceph/ceph.keyring,/etc/ceph/keyring,/etc/ceph/keyring.bin,: (2) No such file or directory\n2020-08-31 12:07:53.223610 7f995883d700 -1 monclient: authenticate NOTE: no keyring found; disabled cephx authentication\n2020-08-31 12:07:53.223617 7f995883d700  0 librados: client.openstack authentication error (95) Operation not supported\n[errno 95] error connecting to the cluster\n'
2020-08-31 12:07:53.242 8 ERROR nova.compute.manager
```

Infos de config ceph

``` bash
When you access Ceph via a Ceph client, the Ceph client will look for a local keyring. Ceph presets the keyring setting with the following four keyring names by default so you don’t have to set them in your Ceph configuration file unless you want to override the defaults (not recommended):
/etc/ceph/$cluster.$name.keyring
/etc/ceph/$cluster.keyring
/etc/ceph/keyring
/etc/ceph/keyring.bin
```

```bash
/var/lib/config-data/nova_libvirt/etc/ceph
```

```bash
/etc/ceph/ceph.client.openstack.keyring
[client.openstack]
    key = AQDSCSZfB0LOEhAA+5Pi3dOUpIZrQV1vgljkXQ==
    caps mgr = "allow *"
    caps mon = "profile rbd"
    caps osd =  "profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups, profile rbd pool=metrics"
```

```bash
ceph.client.admin.keyring
[client.admin]
	key = AQBWBiZfF+2yCxAA+AJcA32WUosva2SCApFYzA==
	caps mds = "allow *"
	caps mgr = "allow *"
	caps mon = "allow *"
	caps osd = "allow *"
```



# Solution.

Avec docker inspect, on trouve que le container nova_copute monte le volume /etc/ceph local dans "/var/lib/kolla/config_files/src-ceph"
```bash
docker inspect keystone | jq .[0].Mounts
...
{
    "Propagation": "rprivate",
    "RW": false,
    "Mode": "ro",
    "Destination": "/var/lib/kolla/config_files/src-ceph",
    "Source": "/etc/ceph",
    "Type": "bind"
  },
...

docker exec -it nova_compute ls /var/lib/kolla/config_files/src-ceph
```

En redémarrant le container  les fichiers de config apparaissent dans "/etc/ceph" dans le container.
```bash
docker restart  nova_compute

docker exec -it nova_compute ls /etc/ceph/
```

Vérification des logs de nova
```bash
tail -f /var/log/containers/nova/nova-compute.log
```


infos sur le keyring :
 https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/1.2.3/html/red_hat_ceph_administration_guide/keyring_management





## Solution

* https://access.redhat.com/solutions/4125811?band=se&seSessionId=7f3d4172-4338-46b4-b000-833d7ec3ab67&seSource=Recommendation%20Aside&seResourceOriginID=8c87ce16-19f7-4b2d-9e95-df1a9968cc07

* https://access.redhat.com/solutions/2686941


# test :

* https://access.redhat.com/solutions/3480001?band=se&seSessionId=7f3d4172-4338-46b4-b000-833d7ec3ab67&seSource=Recommendation%20Aside&seResourceOriginID=8c87ce16-19f7-4b2d-9e95-df1a9968cc07
* https://access.redhat.com/solutions/2625991?band=se&seSessionId=7f3d4172-4338-46b4-b000-833d7ec3ab67&seSource=Recommendation%20Aside&seResourceOriginID=8c87ce16-19f7-4b2d-9e95-df1a9968cc07

# Infos

* https://opendev.org/openstack/tripleo-heat-templates/commit/bf906d75a920103011feb01fe953c43aca52d40c
* https://access.redhat.com/security/cve/CVE-2017-12155
* https://access.redhat.com/solutions/4939291