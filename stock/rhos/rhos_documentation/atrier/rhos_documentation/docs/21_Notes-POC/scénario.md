

# Déploiement

Thibault (RH) : pb avec la version Rocky actuelle (pas considéré GA) qui rend plus difficile le passage en douceur de l'ancienne plate-forme à la nouvelle => sortir plusieurs compute et en basculer certains en control ce qui prendra plus de temps et aller directement sur la toute dernière version 16 (a minima 2 jours avec interruption ?)

## Doc de déploiement

## Version OS de RedHat
Build la plateforme en paralléle de l'existant 
Rocky, version 14, pas GA
Opensatck 13
futur = version 16
15 et 15, sont intermédiaire
gros changement entre REHEL7 et 8
 - Openstack 16 -> RHEL8
-> 16.1 est sortie en mars.


### Version 16

* Quick start guide: [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/quick_start_guide/index](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/quick_start_guide/index)
* Product Documentation for Red Hat OpenStack Platform 16.0 : [https://access.redhat.com/documentation/en-US/Red_Hat_OpenStack_Platform/16.0/](https://access.redhat.com/documentation/en-US/Red_Hat_OpenStack_Platform/16.0/)


### Version 15

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/index](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/index)
* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/partner_integration/architecture](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/partner_integration/architecture)


Scénario envisagé :
1. libérer 5 noeuds de l'infa actuelle (1 director, 3 controlleurs en HA, 1 compute)
2. monter la nouvelle infra
3. migrer les ressoures de l'ancienn infra vers la nouvelle, et transférer les compute au fure et à mesure
4. éteindre l'ancienne infra
5. pour chacun des 3 controlleur actuels : le provisionner en tant que controlleur dans la nouvelle infra, et déprovisionner un des controlleurs d'origine.
--> OK

Si pas possibilité de mettre Director dans une VM, pb actuel d'accès distant iDrac (1 seul accés à un compute). Idem comment le reansférer dans un 2eme temps ? Dans tous le scas, si réseau iPMI necesaire, on est marron.

Comment faire avec CEPH ? utilisation de gluster pour stockage synchro glance, et déploiement dans un second temps ?


Transfert des noeuds.
* possibilité de tansferer un controller ? [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/replacing-controller-nodes)
* possibilité de tansferer director ?


Minima 5 jours.
1 semaine pour le déploien et ammener le MCO (Maintien en conditions opérationelles)
évolution, configuration, automatisation, architeure -> 10jours
Thibaut (RH) : un idéal d'accompagnement serait de 7 jours (voire de 10 jours)



Gestion des phases de transitions.

64To Bruts.
RH fourni de façon limitée pour Glance, les Nova, mais pas Cinder. donc on est en 22k€ + 10k€.
64To Bruts. 
si transition 2020/2021.

Cinder : plusieurs backend ? restrictions.
  -> netapp, gluster, CEPH.

Scénario limité avec Gluster à la place de CEPH.
