

Scénario 2 jours :

* 1/2 journée architecture et stratégie de migration
  * Présentation de l'existant travaille sur la stratégie de migration
* 1/2 journée de validation post-installation des premiers noeuds et réajustement de la stratégie
* 1 journée de transfert de connaissance pour votre MCO
  * Assistance validation du bon fonctionnement et transfert de connaissance
  
Jours suplémentaires :
* 1/2 jour : Validation technique de la migration CEPH et OpenStack
* 1 jour : Assistance de mise en oeuvre de la nouvelle infrastructure OpenStack
* 1 jour : Assistance de mise en oeuvre de la mise à jour CEPH communautaire (ou passage vers GlusterFS)


