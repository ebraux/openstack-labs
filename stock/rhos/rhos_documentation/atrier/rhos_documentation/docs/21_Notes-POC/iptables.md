#lister les rgéles de pre-routing
sudo iptables -t nat -v -L PREROUTING -n --line-number

# créer 
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j DNAT --to-destination 10.129.173.2:443
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 13000 -j DNAT --to-destination 10.129.173.2:13000
# sudo iptables -A FORWARD -i eth0 -p tcp --dport 443 -j ACCEPT
# si besoin supprimer :
sudo iptables -t nat -D PREROUTING {rule-number-here}

/sbin/iptables -L -v -n

iptables -t nat -L


-A INPUT -p tcp -m multiport --dports 443 -m state --state NEW -m comment --comment "100 ui_haproxy_ssl ipv4" -j ACCEPT
-A INPUT -m state --state NEW -m comment --comment "999 drop all ipv4" -j DROP

iptables -A FORWARD -i eth0 -p tcp --dport 80 -d 10.129.173.31.0.23 -j ACCEPT

sudo iptables -A FORWARD -i eth0 -o eth1 -p tcp --syn --dport 443 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o eth1 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -i eth1 -o eth0 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT




sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j DNAT --to-destination 10.129.173.3:443


iptables -L -n -t nat
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         
REDIRECT   tcp  --  0.0.0.0/0            169.254.169.254      tcp dpt:80 redir ports 8775
DOCKER     all  --  0.0.0.0/0            0.0.0.0/0            ADDRTYPE match dst-type LOCAL
DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:443 to:10.129.173.3:443
...


sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80  -j DNAT --to-destination 192.0.2.2

sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80  -j DNAT --to 5.6.7.8:8080
