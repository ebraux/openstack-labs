

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-requirements#virtualization_support
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index

* http://alesnosek.com/blog/2015/09/07/bridging-vlan-trunk-to-the-guest/
* http://blog.davidvassallo.me/2012/05/05/kvm-brctl-in-linux-bringing-vlans-to-the-guests/

* https://www.linux-kvm.org/page/Networking
* https://www.itzgeek.com/how-tos/mini-howtos/create-a-network-bridge-on-centos-7-rhel-7.html
* https://developers.redhat.com/blog/2017/09/14/vlan-filter-support-on-bridge/

* https://linux.goffinet.org/administration/virtualisation-kvm/#31-cr%C3%A9er-une-machine-virtuelle-avec-virt-manager
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-vlan_on_bond_and_bridge_using_the_networkmanager_command_line_tool_nmcli
* https://www.thegeekdiary.com/centos-rhel-7-how-to-configure-vlan-tagging-using-nmcli/
* https://www.redhat.com/sysadmin/vlans-configuration
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-managing_guest_virtual_machines_with_virsh-managing_virtual_networks

* https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf
* https://images.rdoproject.org/docs/baremetal/customizing-external-network-vlan.html



On installe Director en tant que VM sur un noeud hote. Là on sinstall directeor avec KVM

The KVM host uses two Linux bridges:

* br-ex (eth0) :  
  * Provides outside access to the undercloud
  * DHCP server on outside network assigns network configuration to undercloud using the virtual NIC (eth0)
  * Provides access for the undercloud to access the power management interfaces for the bare metal servers

* br-ctlplane (eth1)
  * Connects to the same network as the bare metal overcloud nodes
  * Undercloud fulfills DHCP and PXE boot requests through virtual NIC (eth1)
  * Bare metal servers for the overcloud boot through PXE over this network

### config réseau intiliale de l'HOST

```bash
ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.13/24 brd 10.129.172.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
4: eth2: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3e brd ff:ff:ff:ff:ff:ff
5: eth3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3f brd ff:ff:ff:ff:ff:ff
```

/etc/sysconfig/network
```bash
# Created by anaconda
GATEWAY=10.129.172.1
```

/etc/sysconfig/network-scripts/ifcfg-eth0
```bash
DEVICE=eth0
BOOTPROTO=static
ONBOOT=yes
TYPE=Ethernet
HWADDR=18:66:da:ed:2c:3c
IPADDR=10.129.172.13
BROADCAST=10.129.172.255
NETMASK=255.255.255.0
NETWORK=10.129.172.0
NM_CONTROLLED=no
```

### création des Linux Bridges

```bash
brctl addbr br-ex
brctl addbr br-ctlplane

ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.13/24 brd 10.129.172.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
4: eth2: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3e brd ff:ff:ff:ff:ff:ff
5: eth3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3f brd ff:ff:ff:ff:ff:ff
6: br-ex: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether de:29:51:8b:27:c8 brd ff:ff:ff:ff:ff:ff
7: br-ctlplane: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ea:e5:4c:85:a7:e3 brd ff:ff:ff:ff:ff:ff
```




### Instalation de la VM pour Director

The KVM host requires the following packages :

```bash
yum install libvirt-client libvirt-daemon qemu-kvm libvirt-daemon-driver-qemu libvirt-daemon-kvm virt-install bridge-utils rsync virt-viewer
```

récupérer : https://access.redhat.com/downloads/content/69/ver=/rhel---7/7.8/x86_64/product-software

Lancemement :

```bash
virt-install \
  --name undercloud \
  --memory 16384 \
  --vcpus 4 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --disk size=100 \
  --network bridge=br-ex \
  --network bridge=br-ctlplane \
  --hvm \
  --graphics=none \
  --os-variant=rhel7 \
  --console pty,target_type=serial 
 ```
 
 Autres options :
 
``` bash


  
  --extra-args 'console=ttyS0,115200n8'

  --disk /path/to/imported/disk.qcow \ 
  --import \ 

  --graphics=vnc
```


Config reseau pour "undercloud" \
 :

``` bash
 1) IPv4 address or "dhcp" for DHCP
    10.129.172.20
 2) IPv4 netmask
    255.255.255.0
 3) IPv4 gateway
    10.29.172.1
 4) IPv6 address[/prefix] or "auto" for automatic, "dhcp" for DHCP, "ignore" to
    turn off
    auto
 5) IPv6 default gateway
 6) Nameservers (comma separated)
    192.108.115.2,192.44.75.10
 7) [x] Connect automatically after reboot
 8) [x] Apply configuration in installer
Configuring device eth0.
```
``` bash
 1) IPv4 address or "dhcp" for DHCP
    10.129.41.130
 2) IPv4 netmask
    255.255.255.0
 3) IPv4 gateway
    10.129.41.1
 4) IPv6 address[/prefix] or "auto" for automatic, "dhcp" for DHCP, "ignore" to
    turn off
    auto
 5) IPv6 default gateway
 6) Nameservers (comma separated)
    192.108.115.2, 192.44.75.10
 7) [x] Connect automatically after reboot
 8) [x] Apply configuration in installer
 Configuring device eth1.
``` 


https://linux.goffinet.org/administration/virtualisation-kvm/#31-cr%C3%A9er-une-machine-virtuelle-avec-virt-manager
  


Une fois créé :

``` bash
[root@undercloud ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:e0:01:e4 brd ff:ff:ff:ff:ff:ff
    inet 10.29.172.20/24 brd 10.29.172.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::d25b:da52:88a2:5b90/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:2a:2e:ef brd ff:ff:ff:ff:ff:ff
    inet 10.129.41.130/24 brd 10.129.41.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::6e8e:6eee:5803:4af6/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
``` 
et sur la machine hote

```  bash
19: vnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br-ex state UNKNOWN group default qlen 1000
    link/ether fe:54:00:e0:01:e4 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc54:ff:fee0:1e4/64 scope link 
       valid_lft forever preferred_lft forever
20: vnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br-ctlplane state UNKNOWN group default qlen 1000
    link/ether fe:54:00:2a:2e:ef brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc54:ff:fe2a:2eef/64 scope link 
       valid_lft forever preferred_lft forever

brctl show
bridge name	bridge id		STP enabled	interfaces
br-ctlplane		8000.fe54002a2eef	no		vnet1
br-ex		8000.fe5400e001e4	no		vnet0


[root@os-director-01 ~]# brctl showmacs br-ctlplane
port no	mac addr		is local?	ageing timer
  1	fe:54:00:2a:2e:ef	yes		   0.00
  1	fe:54:00:2a:2e:ef	yes		   0.00
[root@os-director-01 ~]# brctl showmacs br-ex
port no	mac addr		is local?	ageing timer
  1	fe:54:00:e0:01:e4	yes		   0.00
  1	fe:54:00:e0:01:e4	yes		   0.00

``` 




### Gestion de la VM pour Director

#### Backup

Create a live snapshot of the running VM 

``` bash
virsh snapshot-create-as --domain undercloud --disk-only --atomic --quiesce
```

Take a copy of the (now read-only) QCOW backing file
```bash
rsync --sparse -avh --progress /var/lib/libvirt/images/undercloud.qcow2 1.qcow2
```

Merge the QCOW overlay file into the backing file and switch the undercloud VM back to using the original file:
``` bash
virsh blockcommit undercloud vda --active --verbose --pivot

```





