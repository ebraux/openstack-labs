




undercloud_hostname = os-director-01.imta.fr
local_ip = 10.129.172.1/24
undercloud_public_host = 10.129.172.2
undercloud_admin_host =  10.129.172.3
undercloud_nameservers = 192.44.75.10,192.108.115.2
undercloud_ntp_servers = ntp1.svc.enst-bretagne.fr,ntp3.svc.enst-bretagne.fr
overcloud_domain_name = os-overcloud.imta.fr
subnets = ctlplane-subnet
local_subnet = ctlplane-subnet

local_interface = eth1
local_mtu = 1500


undercloud_service_certificate = /etc/pki/instack-certs/undercloud.pem





```bash
Name:	os-director-01.imta.fr
Address: 10.129.172.13
```

local_ip : The IP address defined for the director’s Provisioning NIC. 
undercloud_public_host : The IP address or hostname defined for director Public API endpoints
undercloud_admin_host : The IP address or hostname defined for director Admin API endpoints over SSL/TLS.


## mécanisme de getsion des machines :

```bash
# Whether to enable extra hardware collection during the inspection
# process. Requires python-hardware or python-hardware-detect package
# on the introspection image. (boolean value)
#inspection_extras = true

# Whether to run benchmarks when inspecting nodes. Requires
# inspection_extras set to True. (boolean value)
# Deprecated group/name - [DEFAULT]/discovery_runbench
#inspection_runbench = false

# Whether to support introspection of nodes that have UEFI-only
# firmware. (boolean value)
#inspection_enable_uefi = true

# Makes ironic-inspector enroll any unknown node that PXE-boots
# introspection ramdisk in Ironic. By default, the "fake" driver is
# used for new nodes (it is automatically enabled when this option is
# set to True). Set discovery_default_driver to override.
# Introspection rules can also be used to specify driver information
# for newly enrolled nodes. (boolean value)
#enable_node_discovery = false
```

```bash
ipxe_enabled
Defines whether to use iPXE or standard PXE. The default is true, which enables iPXE. Set to false to set to standard PXE. For more information, see Appendix D, Alternative Boot Modes.
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-Alternative_Boot_Modes
```



## interface graphique

```bash
enable_ui
Defines Whether to install the director’s web UI. This allows you to perform overcloud planning and deployments through a graphical web interface. For more information, see Chapter 7, Configuring a Basic Overcloud with the Web UI. Note that the UI is only available with SSL/TLS enabled using either the undercloud_service_certificate or generate_service_certificate.
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_UI_Tools
```


## Personnalisation de la configguration

```bash
hieradata_override
Path to hieradata override file that configures Puppet hieradata on the director, providing custom configuration to services beyond the undercloud.conf parameters. If set, the undercloud installation copies this file to the /etc/puppet/hieradata directory and sets it as the first file in the hierarchy. See Section 4.10, “Configuring hieradata on the undercloud” for details on using this feature.
```

```bash
# Whether to enable the debug log level for Undercloud OpenStack
# services. (boolean value)
#undercloud_debug = true


```
## Personnalisation de la config réseau (bond, ...)

```bash
net_config_override
Path to network configuration override template. If set, the undercloud uses a JSON format template to configure the networking with os-net-config. This ignores the network parameters set in undercloud.conf. Use this parameter when you want to configure bonding or add an option to the interface. See /usr/share/instack-undercloud/templates/net-config.json.template for an example.
```
