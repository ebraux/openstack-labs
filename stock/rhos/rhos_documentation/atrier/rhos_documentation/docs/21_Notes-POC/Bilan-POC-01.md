
# Objectif 

* Déploiement Controlplane en HA, 3 noeuds
* Utilisation d'une instance de CEPH, externe, a déployer.
* utilisation de CEPH pour glance, cinder. Nova, uniquement pour 3 compute



# Contraintes pour la migration

Une nouvelle infrastructure réseau, dédiée à été mise en place pour RHOS.
Sur le principe d'uné resau en étoile, avec 2 réseaux physiques, et le doublage des interfaces sur les serveurs.
Des vlans ont été créés, et des subnet ont été réservés.

Avec cette infrastruture réseau dédiée, il est donc possible de créer une infrastructure Opensatck RHOS en parallèle de l'infrastructure existante.

Par contre nous ne disposons pas des machines necessaire spour monter entièrement une nouvelle infrastructure RHOS/CEPH de production

Rem : l'infrastructure réseau mise en place est une infra 1G. Une évolution vers une infra 10G pour le controlpane, le cluster CEPH et 3 computes est en cours, et devrait être finalisée mi septembre.

# Scénario de déploement envisagé

1 / déploiement d'un POC RHOS : validation de l'environnement réseau et des fichiers de configuration
2 / backup de la production actuelle : arrêt des instances et création de snapshots puis export des images.
3 / arrêt de la production, reconfiguration des serveurs actuels, et redéploiement de la nouvelle infra 




# Tests réalisés

## description d ece qui a été mis en place

Déploiement d'un cluster CEPH en standalone :
* le cluster est actif, et opérationnel.
* chaque noeud a les rôles : OSD, MON, MDS, MGR, RGW
* un noeud est dédié aux services Grafana
* l'espace de stockage assez limité : 3 OSD, 146Go
* le lien réseau pour le "mgmt" est un lien 1Go
* pas d'accès sortant direct, mais par un proxy
* instalation en mode Container, avec une registry locale (utilisation de celle de Director)

Déploiement d'une infra RHOS 13 :
Undercloud :
* endpoint public en SSL, certificat signé par un "certificate authority" local
* registry Docker : utilisation d'une registry locale
* images complémentaires : "octavia", "ironic", "" "manila-cephfsnative-config"
* provisionning de 5 noeuds

Overcloud :
* 1 controller, 1 compute
* scénario réseau : network-isolation, single-nic-with-vlans
* scénario CEPH : ceph-ansible-external
* état :
  * déploiement des 2 noeuds "overcloud-controller-0" et "overcloud-compute-0".
  * les machines sont accessibles
   * 

## Liste machines et rôles

* os-bastion-01 : bastion
* os-director-01 : noeud Director
* os-director-02 : prévue en backup pour director, utilisée finalement pour metrics CEPH
* ceph-poc-01, ceph-poc-02, ceph-poc-03 : cluster CEPH
* os-poc-ctrl-01, os-poc-ctrl-02, os-poc-ctrl-03 : control-plane RHOS
* os-poc-comp-01, os-poc-comp-02 : compute RHOS

## Config réseau

Accés réseau "admin" depuis infra IMT Atlantique : 10.129.41.130/24 [vlan 148]
* 10.129.41.130 : os-director-01
* 10.129.41.131 : os-director-02
* 10.129.41.132 : os-bastion-01

Réseau de management  10.129.172.0/24 [Vlan 172]
Le réseau est routé sur le réseau interne IMT atlantique
Il sert à la fois :
* de réseau de provisionning pour le cluster CEPH, et le serveur director
* pour le crtplane pour l'undercloud

Gateway sur réseau interne : 10.129.172.1

IP statiques pour serveurs hors undercloud:
10.129.172.10 : ceph-poc-01 (noeud CEPH)
10.129.172.11 : ceph-poc-02 (noeud CEPH)
10.129.172.12 : ceph-poc-03 (noeud CEPH)
10.129.172.13 : os-director-01  (noeud Director)
10.129.172.14 : os-director-02 (pour metrics CEPH)
10.129.172.15 : os-bastion-01 (bastion)

IP Utilisée par director :
10.129.172.2 : undercloud_public_host
10.129.172.3 : undercloud_admin_host
10.129.172.5 : local_ip

Config du ctlplane-subnet de l'undercloud :
  name_lower: 'management'
  vlan: 172
  ip_subnet: '10.129.172.0/24'
  allocation_pools: [{'start': '10.129.172.100', 'end': '10.129.172.199'}]
  gateway_ip: '10.129.172.1'



## Openstack overcloud

### Network

Configurer l'affectation des bridges à des interfaces spécifiques.
Dans la doc, c'est indiqué que c'est par défaut, dans l'ordre de déclaration du fichier network_datas
Mais pas d'info sur comment personnaliser cette affectation.

### proxy

Comment configurer le proxy les noeuds déployés ?


### souscription

Comment valider automatiquement la souscription pour les noeuds déployés

###  fonctionnalités
Ajout du support de Manilla dans l'undercloud

# fichiers de config

## Config de CEPH

/usr/share/ceph-ansible/group_vars/all.yml
```bash
---
fetch_directory: /usr/share/ceph-ansible/ceph-ansible-keys

redhat_package_dependencies: []
upgrade_ceph_packages: False
ceph_test: false

ip_version: ipv4
public_network: 192.168.176.0/24
cluster_network: 192.168.177.0/24
monitor_interface: br-storage
monitor_address_block: 192.168.176.0/24
mon_use_fqdn: false
radosgw_interface: br-storage
radosgw_address_block: 192.168.176.0/24
configure_firewall: false

ceph_rhcs_version: 4
ceph_origin: repository
ceph_repository: rhcs
ceph_repository_type: cdn
containerized_deployment: true
ceph_docker_registry: 10.129.172.3:8787
ceph_docker_registry_auth: false
ceph_docker_image: rhceph/rhceph-4-rhel8
ceph_docker_image_tag: latest
docker_pull_timeout: 600s

dashboard_enabled: true
dashboard_admin_user: admin
dashboard_admin_password: CEPHstack20!

prometheus_container_image: 10.129.172.3:8787/openshift4/ose-prometheus:4.1
node_exporter_container_image: 10.129.172.3:8787/openshift4/ose-prometheus-node-exporter:v4.1
alertmanager_container_image: 10.129.172.3:8787/openshift4/ose-prometheus-alertmanager:4.1

grafana_admin_user: admin
grafana_admin_password: CEPHstack20!
grafana_container_image: 10.129.172.3:8787/rhceph/rhceph-4-dashboard-rhel8
grafana_server_addr: 10.129.172.14
```

## config Undercloud

```bash
[DEFAULT]
undercloud_hostname = os-director-01.imta.fr

local_interface = eth1
local_subnet = ctlplane-subnet
local_ip = 10.129.172.5/24
local_mtu = 1500

undercloud_public_host = 10.129.172.2
undercloud_admin_host =  10.129.172.3
generate_service_certificate = false
undercloud_service_certificate = /etc/pki/instack-certs/undercloud.pem

undercloud_nameservers = 192.44.75.10,192.108.115.2
undercloud_ntp_servers = ntp1.svc.enst-bretagne.fr,ntp3.svc.enst-bretagne.fr

overcloud_domain_name = localdomain

subnets = ctlplane-subnet

inspection_interface = br-ctlplane
undercloud_update_packages = true

enable_ui = true
clean_nodes = false

[auth]

[ctlplane-subnet]
cidr = 10.129.172.0/24
gateway = 10.129.172.1
dhcp_start = 10.129.172.100
dhcp_end = 10.129.172.199
inspection_iprange = 10.129.172.90,10.129.172.99
```

## config Overcloud

node-info.yaml

```bash
parameter_defaults:
  OvercloudControllerFlavor: 'control'
  OvercloudComputeFlavor: 'compute'
  ControllerCount: 1
  ComputeCount: 1
  ControlPlaneSubnetCidr: '24'
  ControlPlaneDefaultRoute: '10.129.172.1'
  ManagementInterfaceDefaultRoute: '10.129.172.1'
  EC2MetadataIp: '10.129.172.5'
```

network_data.yaml

```bash
- name: 'Storage'
  vip: true
  vlan: 176
  name_lower: 'storage'
  ip_subnet: '192.168.176.0/24'
  defroute: false
  allocation_pools: [{'start': '192.168.176.4', 'end': '192.168.176.250'}]
- name: 'StorageMgmt'
  name_lower: 'storage_mgmt'
  vip: true
  vlan: 177
  ip_subnet: '192.168.177.0/24'
  defroute: false
  allocation_pools: [{'start': '192.168.177.4', 'end': '192.168.177.250'}]
- name: 'InternalApi'
  name_lower: 'internal_api'
  vip: true
  vlan: 175
  defroute: false
  ip_subnet: '192.168.175.0/24'
  allocation_pools: [{'start': '192.168.175.4', 'end': '192.168.175.250'}]
- name: 'Tenant'
  vip: false  # Tenant network does not use VIPs
  name_lower: 'tenant'
  vlan: 173
  defroute: false
  ip_subnet: '192.168.173.0/24'
  allocation_pools: [{'start': '192.168.173.4', 'end': '192.168.173.250'}]
- name: 'External'
  vip: true
  name_lower: 'external'
  vlan: 178
  ip_subnet: '10.129.176.0/22'
  allocation_pools: [{'start': '10.129.176.10', 'end': '10.129.179.250'}]
  defroute: false
  gateway_ip: '10.129.176.1'
- name: 'Management'
  enabled: true
  vip: false  # Management network does not use VIPs
  name_lower: 'management'
  vlan: 172
  ip_subnet: '10.129.172.0/24'
  allocation_pools: [{'start': '10.129.172.100', 'end': '10.129.172.199'}]
  gateway_ip: '10.129.172.1'
  defroute: true
```
