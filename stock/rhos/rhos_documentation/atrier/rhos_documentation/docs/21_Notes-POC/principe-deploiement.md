


## Architecture

* [https://www.redhat.com/en/blog/introduction-red-hat-openstack-platform-director](https://www.redhat.com/en/blog/introduction-red-hat-openstack-platform-director)
* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/partner_integration/architecture](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/partner_integration/architecture)

# A voir :
16 steps to a fully high available Red Hat OpenStack Platform environment with ceph build by OSP Director 7.1
* [http://blog.domb.net/?p=1199](http://blog.domb.net/?p=1199)


Pour le déploiement des machine, Director s'appuie sur IRONIC.
C'est IRONIC qui gère l'initialisation des machines physiques avec le DHCP, PXE, ...
IRONIC est hébergé sur la machine du Director.
Il faut un réseau dédié au provisionning avec la config pour que les machine ajoutée dans ce réseau contactent Ironic au boot.
Interface dédié sur chaque machine, à relier à l'infra, ou possibilité d'utiliser un vlan par défaut -> interface dédiée
Ce réseau peut servir de réseau d'admin -> a valider

Director s'appuie aussi sur les iPMI : DELL DRAC OK ? Indispensable (pc ELEC)?
Il faut un réseau dédié pour les iPMI, ou on peut leur réserver une partie du réseau "provsionning/admin" ? -> A valider
Il faut la version "Enterprise" de la DRAC DEll ? -> a valider
Pourra-t-on utiliser Director pour nos autres déploiements iPXE ? Réponse à venir

L'infra de déploiement (undercloud), c'est TripleO, un mini openstack avec glance, Heat Ironic, Neutron (et Nova ?).
http://tripleo.org/
https://docs.openstack.org/tripleo-docs/latest/
https://www.worteks.com/fr/2019/09/09/openstack-on-openstack/

HEAT est utilisé pour déployer tous les composants (comme les réseau, les servers....) qui seront déployées sur l'undercloud, pour pouvoir monter le cloud Openstack dessus ensuite ?
On fait de la virtualisation de réseau dans cette couche, et on en refait ensuite avec Openstack ?
remarque : puisqu'il y a Nova, mais on pourrait aussi déployer les serveurs de base sur des VM, et pas dur du baremetal.

Ca gére les bonding, les bridges, ... ? 
  --> oui, c'est géré. c'est une des partie à customisé. Voir les lilitations.
  --> faire une cartographie, et valider.
  Thibault (RH) : via Ansible tout ça devrait être gérable 

Il faut renseigner toutes les infos sur les machine (adreesse mac, type, ...) ou TripleO/Director sait les récupérer avec IPMI ? 
[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/automatically-discover-bare-metal-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/15/html/director_installation_and_usage/automatically-discover-bare-metal-nodes)

Pour les serveurs, il y a une image de base, et la configuration est complétée par Puppet ou par Ansible (ca dépend des docs) ?  -> complétement sur Ansible en v16. 
Les images sont sur la machine de Director ?
Est-ce quon peut/doit personnaliser les images ? -> c'ets assez standard.


Accés à une registry contenant les images docker à déploiyer
3 solutions :
* la regisrtyr docker de redhat : tous les noeuds doivet avoir accès in internet pour accéder à la registry. traffic réseau
* une registry installée en local sur Director
* le service redHat "Satellite"



La configuration de Director est elle aussi gérée par Puppet ?

Une fois le réseau de base, et les serveur déployés, TripleO déploie les composants Opensatck en s'appuyant des containers du projet Kolla [https://docs.openstack.org/kolla/latest/index.html] avec des customisations spécifques pour TripleO : [https://docs.openstack.org/kolla/latest/admin/image-building.html#dockerfile-customisation] et [https://github.com/openstack/tripleo-common/blob/master/container-images/tripleo_kolla_template_overrides.j2]
Est-ce quon peut/doit personnaliser les images ?

Les containers tournent avec "podman", et pas Docker.
https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/architecture.html

Pour la préparation des déploiement, et la gestion des services on n'utilise plus puppet, mais Ansible ?
Ansible sert aussi pour toutes les taches d'administration ?

On peut aussi choisir de déployer un composant en mode "baremetal", pas sous forme de dontainer. Dans ce cas, tout le déploiement + configuration est fait avec Puppet ?

 Exploitation
Tous les scripts d'exploitation sont prévus dans Director.
Dans director, ansible Engine -> projet communautaire

Ansible automation, c'est ansible Engine + ansible automation.
Ansible Tower -> necessaire si on est en multi-régions, ...
---> à confirmer ce qui est dans Openstack.

