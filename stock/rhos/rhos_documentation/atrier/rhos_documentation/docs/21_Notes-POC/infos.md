Réponses aux questions levés en séance :
Utilisation d'IRONIC pour déployer de l'infrastructure et pour le déploiement openstack ?
Director utilise Ironic pour gérer le cycle de vie du matériel baremetal dans l'Overcloud (https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/partner_integration/architecture)
Pour le déploiement des noeuds controller et compute, IRONIC est donc piloté par Director pour configurer les machines et déployer les modules openstack.

Matériel Dell, réseau dédié carte iDRAC ? Est-ce que pour le management de l'infrastructure il faut déployer un réseau dédié ?
Notre recommandation est de mettre en place 2 réseaux (ségrégation):
Réseau interne (management ) pour les APIs interne OSP(Undercloud) et les réseaux iDRAC/iPMI
Réseau production (externe) ou les services (VMs) sont exposés
Vérifier que la version de l’iDRAC fait de l’IPMI2 permet de garantir la compatibilité

Nombre d’interface par machine ?
2 interfaces physiques par machine sont recommandés, et mettre en place  un bound sur chaque carte pour éviter le SPOF.

Stockage avec plusieurs backend de stockage ?
Non possible en standard mais il est possible de le mettre en place en modifiant le container cinder livrés dans la distribution openstack. ce n'est pas forcément recommandé car il faut ensuite maintenir cette modification pendant le cycle de vie.

Quelques infos sur Director:
Packagé depuis le projet communautaire TripleO (http://tripleo.org/)
Permet de déployer, maintenir et opérer l'infrastructure undercloud et overcloud
S'appuie sur ansible pour réaliser l'ensemble de actions (https://www.redhat.com/en/blog/introduction-red-hat-openstack-platform-director)
Ci-dessous un diagramme de séquence pour comprendre :

Director vous permettra effectivement de déployé et configuré l'ensemble des composants des noeuds de l'overcloud (interface réseau, configurations OS, etc)

Concernant votre stratégie de migration et vos contraintes budgétaires
Sur l'aspect donné 2 options possible qui n'auront pas d'impact majeures pour les futurs évolutions de votre infrastructure OpenStack:
Option 1:
Garder le ceph existant et l'intégrer à la nouvelle infrastructure OpenStack.
Option 2:
Utiliser l'infrastructure netapp et donc copié les data de CEPH vers NetApp
Une migration à froid est à envisager pour chacune des options.

Pour déployer l'infrastructure openstack il faudra opter par la souscription site Infrastructure. Le director pouvant être déployé dans votre infrastructure RHV sur une VM RHEL.

Sur les aspects accompagnement, sur une base de 2 jours, nous pourrions les découper de la manière suivante :
1/2 journée architecture et stratégie de migration
1/2 journée de validation post-installation des premiers noeuds et réajustement de la stratégie
1 journée de transfert de connaissance pour votre MCO