


# Réseau  permettant de se connecter à toute les instances, avec getsion manuelle : 10.129.172.0

## déclaration
Le réseau 10.129.172.0/24 a une gateway déclarée sur le réseau : 10.129.172.1

Exemple de fichier "/etc/sysconfig/network"

```bash
# Created by anaconda
GATEWAY=10.129.172.1
```

## configurer l'interace principale

```bash
DEVICE=eth0
BOOTPROTO=static
ONBOOT=yes
TYPE=Ethernet
HWADDR=00:24:e8:3b:c2:02
IPADDR=10.129.172.11
BROADCAST=10.129.172.255
NETMASK=255.255.255.0
NETWORK=10.129.172.0
DEFROUTE=yes
DNS1=192.44.75.10
DNS2=192.108.115.2
NM_CONTROLLED=no
```

Attention, la configuration DNS, et la getsion de la route par défaut se font désoemais par interface. Il faut donc préciser "DEFROUTE=yes" pour l'interafce par défaut et "DEFROUTE=no" pour les autres

## activer la gestion des vlan

Vérifier la prise en compte d ela getsion des Vlan (802.1q) par le systéme

```bash
modinfo 8021q
```

Si besoin activer 
```bash
modprobe --first-time 8021q
```

## tester la connectionque réseau : configuration basique

### exemple de configuration du vlan de Stockage 176

Configurer l'interface : /etc/sysconfig/network-scripts/ifcfg-eth1

```bash
DEVICE=eth1
TYPE=Ethernet
BOOTPROTO=none
ONBOOT=yes
```

Configurer le support des vlan: /etc/sysconfig/network-scripts/eth1.176

```bash
DEVICE=eth1.176
BOOTPROTO=none
ONBOOT=yes
IPADDR=10.129.176.10
PREFIX=24
NETWORK=10.129.176.0
VLAN=yes
```

Activer

```bash
systemctl restart network
```

Tester

```bash
ip addr

ping -c 3 10.129.176.10
ping -c 3 -I eth1.176 10.129.176.11
ping -c 3 -I eth1.176 10.129.176.12

ping -c 3 10.129.177.10
ping -c 3 -I eth2.177 10.129.177.11
ping -c 3 -I eth2.177 10.129.177.12
```

## configuration Manuelle des Bridge

Les diféfrents outils necessitent la mise en place de bridges, pour faciliter les configurations

* CEPH
  * br-storage, Vlan 176
  * br-mgmt, Vlan 177

### Fichiers de config pour br-storage, Vlan 176

ifcfg-eth1.176
```bash
DEVICE=eth1.176
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-storage
```

ifcfg-br-storage
```bash
DEVICE=br-storage
TYPE=Bridge
BOOTPROTO=none
IPADDR=10.129.176.12
NETMASK=255.255.255.0
BROADCAST=10.129.176.255
NETWORK=10.129.176.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-storage 10.129.176.10
ping -c 3 -I br-storage 10.129.176.11
ping -c 3 -I br-storage 10.129.176.12

```


### Fichiers de config pour br-mgmt, Vlan 177

ifcfg-eth2.177
```bash
DEVICE=eth2.177
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-mgmt
```

ifcfg-br-mgmt
```bash
DEVICE=br-mgmt
TYPE=Bridge
BOOTPROTO=none
IPADDR=10.129.177.12
NETMASK=255.255.255.0
BROADCAST=10.129.177.255
NETWORK=10.129.177.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-mgmt 10.129.177.10
ping -c 3 -I br-mgmt 10.129.177.11
ping -c 3 -I br-mgmt 10.129.177.12
```







## tets ajout d'in bridge

```bash
nmcli connection add type bridge \
  con-name br-mgmt \
  ifname br-mgmt \
  bridge.stp no \
  ipv6.method ignore \
  ipv4.method manual \
  ipv4.addresses 10.129.177.12/24 ipv4.gateway 10.129.177.1 
  

```

crée n un fichier /etc/sysconfig/network-scripts/ifcfg-BrMgmt

```bash
STP=yes
BRIDGING_OPTS=priority=32768
TYPE=Bridge
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=BrMgmt
UUID=ed13fff5-0a4b-49cc-a5fd-b8052d948259
DEVICE=br-mgmt
ONBOOT=yes
```

```bash
nmcli conn add type ethernet slave-type bridge con-name br-mgmt-eth2 ifname eth2 master br-mgmt

nmcli conn mod br-mgmt ip4 10.129.176.12/24
nmcli conn mod  br-mgmt  ipv4.method manual
nmcli conn mod  br-mgmt ipv6.method ignore
nmcli conn add  type bridge-slave ifname eth2 master br-mgmt

nmcli conn add type ethernet slave-type bridge ifname eth2 master br-mgmt
nmcli conn up br-mgmt
```

https://www.tecmint.com/create-network-bridge-in-rhel-centos-8/#nmcli
https://www.lisenet.com/2016/software-bridge-on-top-of-a-teamed-device-on-rhel-7/
https://www.itzgeek.com/how-tos/mini-howtos/create-a-network-bridge-on-centos-7-rhel-7.html

#sudo nmcli con mod eth0 ipv4.dns "192.168.2.254"
# nmcli conn modify br0 ipv4.addresses '192.168.1.1/24'
# nmcli conn modify br0 ipv4.gateway '192.168.1.1'
# nmcli conn modify br0 ipv4.dns '192.168.1.1'
# nmcli conn modify br0 ipv4.method manual
# nmcli conn add type ethernet slave-type bridge con-name bridge-br0 ifname enp2s0 master br0



```bash
nmcli con show
NAME           UUID                                  TYPE      DEVICE   
BrMgmt         ed13fff5-0a4b-49cc-a5fd-b8052d948259  bridge    br-mgmt  
```




## solution complète



avec nmcli : https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configure_802_1q_vlan_tagging_using_the_command_line_tool_nmcli

```bash
nmcli con show
nmcli con add type vlan con-name VLAN176 ifname eth1.176 dev eth1 id 176
nmcli con add type vlan con-name VLAN176 dev eth1 id 176 ip4 10.129.176.10/24

nmcli con show
nmcli -p con show VLAN176
```

```bash
systemctl restart network
```


## config réseau

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-vlan_on_bond_and_bridge_using_the_networkmanager_command_line_tool_nmcli


```bash
lsmod | grep bridge
bridge                151336  0 
stp                    12976  2 garp,bridge
llc                    14552  3 stp,garp,bridge

modinfo bridge
filename:       /lib/modules/3.10.0-1127.13.1.el7.x86_64/kernel/net/bridge/bridge.ko.xz
alias:          rtnl-link-bridge
version:        2.3
license:        GPL
retpoline:      Y
rhelversion:    7.8
srcversion:     01E9193CE9DE2C3DF95808C
depends:        stp,llc
intree:         Y
vermagic:       3.10.0-1127.13.1.el7.x86_64 SMP mod_unload modversions 
signer:         Red Hat Enterprise Linux kernel signing key
sig_key:        0F:4C:1F:61:68:2D:DE:1D:5A:46:48:0B:4F:5D:34:A7:CB:C3:40:17
sig_hashalgo:   sha256


```

```bash

systemctl start NetworkManager
systemctl enable NetworkManager
systemctl status NetworkManager

nmcli connection add type bond con-name BondStorage ifname bond0 bond.options "mode=active-backup,miimon=100" ipv4.method disabled ipv6.method ignore

nmcli connection add type ethernet con-name BondStorageCon1 ifname eth1 master bond0 slave-type bond
# adapter l'IP
nmcli connection add type bridge con-name BridgeStorage ifname br-storage ip4 10.129.176.10/24
nmcli connection add type vlan con-name VlanStorage-176 ifname bond0.176 dev bond0 id 176 master br-storage slave-type bridge

nmcli connection show

ping -c 3 -I  br-storage 10.129.176.10
ping -c 3 -I  br-storage 10.129.176.11
ping -c 3 -I  br-storage 10.129.176.12


```

```bash
nmcli connection add type bond \
 con-name BondMgmt \
 ifname bond1 \
 bond.options "mode=active-backup,miimon=100" 
 ipv4.method disabled \
 ipv6.method ignore

nmcli connection add type ethernet \
 con-name BondMgmtCon1 \
 ifname eth2 \
 master bond1 \
 slave-type bond


# adapter l'IP
nmcli connection add type bridge \
 con-name BridgeMgmt \
 ifname br-mgmt \
 ip4 10.129.177.10/24

nmcli connection add type vlan \
 con-name VlanStorage-177 \
 ifname bond1.177 \
 dev bond1 \
 id 177 \
 master br-mgmt \
 slave-type bridge

nmcli connection show

ping -c 3 -I  br-mgmt 10.129.177.10
ping -c 3 -I  br-mgmt 10.129.177.11
ping -c 3 -I  br-mgmt 10.129.177.12


nmcli connection add type ethernet \
 con-name BondMgmtCon2 \
 ifname eth0 \
 master bond1 \
 slave-type bond
```

Ménage

```bash
nmcli connection del VlanStorage-176
nmcli connection del BridgeStorage
nmcli connection del BondStorageCon1
nmcli connection del BondStorage


nmcli connection del VlanStorage-177
nmcli connection del BridgeMgmt
nmcli connection del BondMgmtCon1
nmcli connection del BondMgmtCon2
nmcli connection del BondMgmt

```

Dans la conf, il y a un pb de route
```bash
cd /etc/sysconfig/network-scripts/
modifier "DEFROUTE=yes" et "ifcfg-BridgeStorage"
DEFROUTE=yes
en
DEFROUTE=no

```


piste pour ajuster la config 
```bash
nmcli con modify bridge-app-br0 ipv4.method manual ipv4.address "192.151.12.6/26" ipv4.gateway "192.151.12.62"  ipv4.dns 8.8.8.8 ipv4.dns-search example.com
```

* https://www.golinuxcloud.com/configure-network-bridge-nmcli-static-dhcp/
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configuring_ip_networking_with_nmcli



## Divers 

Iptable masquerade par director : /var/opt/undercloud-stack/masquerade
```bash
# In case this script crashed or was interrupted earlier, flush, unlink and
# delete the temp chain.
IPTCOMMAND=iptables
if [[ 10.129.172.128 =~ : ]] ; then
    IPTCOMMAND=ip6tables
fi
$IPTCOMMAND -w -t nat -F BOOTSTACK_MASQ_NEW || true
$IPTCOMMAND -w -t nat -D POSTROUTING -j BOOTSTACK_MASQ_NEW || true
$IPTCOMMAND -w -t nat -X BOOTSTACK_MASQ_NEW || true
$IPTCOMMAND -w -t nat -N BOOTSTACK_MASQ_NEW
# Build the chain we want.
NETWORK=10.129.172.128/26
NETWORKS=10.129.172.128/26,192.168.24.0/24,
# Shell substitution to remove the traling comma
NETWORKS=${NETWORKS%?}
$IPTCOMMAND -w -t nat -A BOOTSTACK_MASQ_NEW -s $NETWORK -d $NETWORKS -j RETURN
$IPTCOMMAND -w -t nat -A BOOTSTACK_MASQ_NEW -s $NETWORK -j MASQUERADE
NETWORK=192.168.24.0/24
NETWORKS=10.129.172.128/26,192.168.24.0/24,
# Shell substitution to remove the traling comma
NETWORKS=${NETWORKS%?}
$IPTCOMMAND -w -t nat -A BOOTSTACK_MASQ_NEW -s $NETWORK -d $NETWORKS -j RETURN
$IPTCOMMAND -w -t nat -A BOOTSTACK_MASQ_NEW -s $NETWORK -j MASQUERADE
# Link it in.
$IPTCOMMAND -w -t nat -I POSTROUTING -j BOOTSTACK_MASQ_NEW
# Delete the old chain if present.
$IPTCOMMAND -w -t nat -F BOOTSTACK_MASQ || true
$IPTCOMMAND -w -t nat -D POSTROUTING -j BOOTSTACK_MASQ || true
$IPTCOMMAND -w -t nat -X BOOTSTACK_MASQ || true
# Rename the new chain into permanence.
$IPTCOMMAND -w -t nat -E BOOTSTACK_MASQ_NEW BOOTSTACK_MASQ
# remove forwarding rule (fixes bug 1183099)
$IPTCOMMAND -w -D FORWARD -j REJECT --reject-with icmp-host-prohibited || true

```

``` json
"network_config": [{"name": "br-ctlplane", "mtu": 1500, "ovs_extra": ["br-set-external-id br-ctlplane bridge-id br-ctlplane"], "members": [{"dns_servers": ["192.44.75.10", "192.108.115.2"], "type": "interface", "name": "eth1", "primary": "true", "mtu": 1500}], "routes": [], "type": "ovs_bridge", "addresses": [{"ip_netmask": "10.129.172.128/26"}]}]}
```

