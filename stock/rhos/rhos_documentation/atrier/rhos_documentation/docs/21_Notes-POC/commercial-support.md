
## Contacts
tonny botté depuis 11 ans
depuis 6 ans accord cadre groupe logiciel. Remis en concurrence cette année...
licences, formation, unité d'oeuvre.

Thibault Jurado-Leduc : account "system architect" pour secteur MESRI.


Sana LARAB : travaille avec 

## Licence provisoire pour pouvoir tester
1 mois renouvelable 1ou 2 fois pour RHV ?

Si nous lançons une évaluation de votre solution, est-ce qu'il est possible de l'installer sans souscription, ou est-ce qu'il est possible d'avoir une licence temporaire ?

30 et 90 jour, obtention immédiate. support redhat.

# Commercial et support

## validité du support

Support si on déploie nous meme ?
Conditions ? (validation par RedHat, ...)
Pas de besoin de "prodeploy DELL" ?

## confirmation licence site

Offres campus.
La notion de site est bien valable pour les 3 sites physique d'IMT Atlantique (Brest Nnates, Rennes) ?


## Cadre Du CPER

Comme faire avec nos partenaires ?
Extension de la notion de site ?
déploiement avec de simages "minimal", fonctions limités et pas de support ?
autres ?

-> pb juridique  d'un point de vue juridique. Modèle en cour de discussion pour le futur marché.

## Infos prestation RedHat
Pour info, si c'est RedHat qui déploie l'infra et 4 noueud de compute, c'est :

* 30 jours de Consultant Technique. juste le paramtrage et le déploiement, il faut que tout soit en place et calé correctment au niveau réseau avant.
* 5 jours de chef de projet



Gagnant/gagant RedHat IMT.
colonne vertébrale, ...
RHV questionné par rapport à VmWare ...
Pb du nombre de jours de prestation.
Doublon sur le déploiement EB + FG (et un peu MLT).



Emmanuel,
Comme vu ensemble par téléphone.
Les souscriptions d'évaluation ont été activées de ton côté, mais tu vérifies dans la journée que côté ceph tu vois bien les produits.

Je vais ajouter l'équipe Consulting dans la boucle pour planifier les journées.
Comme partagé, tu as commandé 2 jours pour le moment mais tu as la possibilité d'aller jusqu'à 10 jours en mutualisant avec les équipes RHV IMT.

Pour ton projet OpenStack on reste sur une base de 5 jours pour mener à bien le périmètre initial de ta demande:
1/2 jour : Présentation de l'existant et travailler sur la stratégie de déploiement (Architecture)
1 jour : Travailler sur la stratégie de migration et validation des prérequis (Architecture)
1 jour : Assistance de mise en oeuvre de la nouvelle infrastructure CEPH
1 jour : Assistance de mise en oeuvre de la nouvelle infrastructure OpenStack
1/2 jour : Assistance mise en oeuvre de la migration vers la nouvelle infrastructure CEPH et OpenStack
1 jour: Assistance validation du bon fonctionnement et transfert de connaissance


Enfin, je t'ai envoyé précédemment les deux fichiers de prérequis que tu dois stp me renvoyer complétés.

En attendant un retour de part,

Bien cordialement, 





## souscriptions

```bash
Your recently purchased subscription(s) listed below are now active and immediately accessible. *Please save this information* for future reference or for additional purchases on this account.

Account Number       : 474687
Contract Number      : 12388005

....


--> Get started with your subscription now: http://www.redhat.com/start
--> See your subscription details and web support options at the customer center: http://access.redhat.com/subscriptions
--> Do you need to speak with someone from Red Hat? Contact us: http://www.redhat.com/contact

Note: You received this notice because you are listed as the technical administrator for this account. Please forward this notice to others in your organization who may need the message. If you need assistance, please call your regional customer service representative listed here: access.redhat.com/customerservice

Customers with active technical support subscriptions may obtain support for a technical issue by contacting a regional Red Hat Technical Support Engineer at https://access.redhat.com/support

This message was automatically generated.


```


### Red Hat Enterprise Linux Server

3/07/2020 : 60 Day Red Hat Enterprise Linux Server Supported Evaluation with Smart Management, Monitoring and all Add-Ons - 60 Days.

```bash
Item Description     : 60 Day Red Hat Enterprise Linux Server Supported Evaluation with Smart Management, Monitoring and all Add-Ons
Quantity             : 1
Service Dates        : 18-JUN-20 through 16-AUG-20
```

### Red Hat OpenStack Platform

* 06/07/2020 : 60 Day Supported Red Hat OpenStack Platform Evaluation - 60 Days.

```bash
Item Description     : 60 Day Supported Red Hat OpenStack Platform Evaluation
Quantity             : 5
Service Dates        : 18-JUN-20 through 16-AUG-20
```

### Red Hat Ceph Storage

* 06/07/2020 : 60 Day Supported Red Hat Ceph Storage (8 Physical Nodes) - 60 Days.

```bash
Item Description     : 60 Day Supported Red Hat Ceph Storage (8 Physical Nodes)
Quantity             : 3
Service Dates        : 18-JUN-20 through 16-AUG-20
```
