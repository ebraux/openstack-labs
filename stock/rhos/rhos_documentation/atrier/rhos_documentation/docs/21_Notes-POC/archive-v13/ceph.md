
* [https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html/installation_guide/index](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html/installation_guide/index)

https://access.redhat.com/products/red-hat-ceph-storage


https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html/installation_guide/what-is-red-hat-ceph-storage_install
https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html-single/architecture_guide/index



Concernant la formation, l'idéal serait de pouvoir focaliser la journée sur du transfert opérationnel en connaissant les fondamentaux CEPH et OpenStack, il faudrait donc la planifier à ce moment là.
En amont tu pourras prendre connaissances de CEPH au travers de la documentation en ligne et du blog Red Hat:

https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/
https://www.redhat.com/en/blog/whats-new-red-hat-ceph-storage-4?source=blogchannel&channel=blog/channel/red-hat-storage&f%5B0%5D=taxonomy_product%3ARed+Hat+Ceph+Storage

https://www.redhat.com/en/blog/command-and-control-red-hat-ceph-storage-4-dashboard-changes-game?source=blogchannel&channel=blog/channel/red-hat-storage&f%5B0%5D=taxonomy_product%3ARed+Hat+Ceph+Storage

https://www.redhat.com/en/blog/deploying-containerized-red-hat-ceph-storage-4-cluster-using-ceph-ansible

https://www.redhat.com/en/blog/building-ceph-powered-cloud-deploying-containerized-red-hat-ceph-storage-4-cluster-red-hat-open-stack-platform-16?source=blogchannel&channel=blog/channel/red-hat-storage&f%5B0%5D=taxonomy_product%3ARed+Hat+Ceph+Storage


## configuration mise en place

Infrastructure avec 3 noeud.
Chaque noeud a les différents rôles : Monitor, OSD, MDS, 
Le mode retenu est " container deployment only"

pour le stockage des images, on utilise la registry mise en place sur la machine director, et accessible :

* 10.129.172.3:5000
* 10.129.172.3:8787


## Operating system requirements for Red Hat Ceph Storage

Red Hat Enterprise Linux 7.7 or Higher or Red Hat Enterprise Linux 8.1  or Higher

-> Dans tous les cas, c'est des images "Red Hat Enterprise Linux 8 container" qui sont déployées.

Attention : 
The ceph-volume tool checks for empty devices, so devices which are not empty will not be used.


## Récupération des images Docker

Si pas d'accès Internet, il faut une registry Docker. On utilise celle de director.

Si besoin la doc standard est là : https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html/installation_guide/requirements-for-installing-rhcs


Sur la machine Director

Vérifier la config de la registry Docker

```bash
cat  /etc/containers/registries.conf | grep registry.redhat.io
```

Créer le fichier de configuration : /home/stack/ceph_local_registry_images.yaml

```bash
ContainerImageRegistryLogin: true
ContainerImageRegistryCredentials:
  registry.redhat.io:
    DISI1: DiSi2003
container_images:
- imagename: registry.redhat.io/rhceph/rhceph-4-rhel8
  push_destination: 10.129.172.5:8787
- imagename: registry.redhat.io/rhceph/rhceph-4-dashboard-rhel8
  push_destination: 10.129.172.5:8787
- imagename: registry.redhat.io/openshift4/ose-prometheus:4.1
  push_destination: 10.129.172.5:8787
- imagename: registry.redhat.io/openshift4/ose-prometheus-alertmanager:4.1
  push_destination: 10.129.172.5:8787

```

```bash
sudo docker login registry.redhat.io
   DISI1: DiSi2003

sudo openstack overcloud container image upload \
  --config-file  /home/stack/ceph_local_registry_images.yaml \
  --verbose
```

Tester

```bash
curl -X GET http://10.129.172.5:8787/v2/_catalog | jq .

docker pull  10.129.172.5:8787/rhceph/rhceph-4-rhel8

```


### enregistrement du serveur de Métrics

Gestion des souscription

```bash
sudo su -
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
subscription-manager register
  DISI1
  DiSi2003
The system has been registered with ID: 413c5fa7-225b-4282-bd6a-37ada69faa48
The registered system name is: os-director-02.imta.fr

subscription-manager attach --pool=8a85f9997313c7b0017314a11821246b
```

```bash
subscription-manager repos --enable=rhel-7-server-extras-rpms
```


### enregistrement. a faire sur les 3 neouds

Gestion du proxy

```bash
echo "http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/environment
echo "https_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/environment
echo "no_proxy=127.0.0.1,localhost,10.129.172.5,192.168.176.10,192.168.176.11, 192.168.176.12" >> /etc/environment

echo "export http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/profile.d/setproxy.sh
echo "export http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/profile.d/setproxy.sh
echo "export no_proxy=\"127.0.0.1,localhost,10.129.172.5,192.168.176.10,192.168.176.11, 192.168.176.12\"" >> /etc/profile.d/setproxy.sh
```

```bash
sudo su -
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080

subscription-manager register
  DISI1
  DiSi2003

Le système a été enregistré avec l'ID : 399ce024-9c40-4cde-8366-8d10c278cfb4
Le nom du système enregistré est : ceph-poc-01.imta.fr

Le système a été enregistré avec l'ID : e88a8703-644e-4618-b26a-7b9b9d6162f3
Le nom du système enregistré est : ceph-poc-02.imta.fr

Le système a été enregistré avec l'ID : 2dfd45d3-a15f-46c4-bb5d-700079a7e070
Le nom du système enregistré est : ceph-poc-03.imta.fr


subscription-manager refresh
subscription-manager list --available --all --matches="*Ceph*"


Nom de l'abonnement : 60 Day Supported Red Hat Ceph Storage (8 Physical Nodes)
Fournit :             Red Hat OpenStack Director Deployment Tools Beta
                      Red Hat Software Collections (for RHEL Server)
                      Red Hat CodeReady Linux Builder for x86_64
                      Red Hat Ansible Engine
                      Red Hat Ceph Storage
                      Red Hat Enterprise Linux Scalable File System (for RHEL Server)
                      Red Hat OpenStack Director Deployment Tools for IBM Power LE
                      Red Hat OpenStack Director Deployment Tools Beta for IBM Power LE
                      Red Hat Storage Console Node
                      Red Hat Storage Console
                      Red Hat Enterprise Linux Server
                      Red Hat Ceph Storage OSD
                      Red Hat Enterprise Linux for x86_64
                      Red Hat Ceph Storage MON
                      Red Hat Ceph Storage Calamari
                      Red Hat OpenStack Director Deployment Tools
SKU :                 RS00033
Contrat :             12388696
ID du pool :          8a85f99a7322bd4d017322edb2e70b2b
Fournit la gestion :  Non
Disponible :          24
Suggestion(s) :       1
Type de service :     L1-L3
Roles:                
Niveau de service :   Standard
Usage:                
Add-ons:              
Type d'abonnement :   Standard
Démarre :             18/06/2020
Termine :             17/08/2020
Entitlement Type:     Physique
```

```bash
subscription-manager attach --pool=8a85f99a7322bd4d017322edb2e70b2b
```

```bash
subscription-manager repos --disable=*
subscription-manager repos --enable=rhel-7-server-rpms
subscription-manager repos --enable=rhel-7-server-extras-rpms

yum update

yum install yum-utils vim -y
yum-config-manager --disable epel

subscription-manager repos --enable=rhel-7-server-rhceph-4-tools-rpms 
--enable=rhel-7-server-ansible-2.8-rpms
```

Monitor Node :

```bash
subscription-manager repos --enable=rhel-7-server-rhceph-4-mon-rpms
```

OSD node

```bash
subscription-manager repos --enable=rhel-7-server-rhceph-4-osd-rpms
```

RBD mirroring, Ceph clients, Ceph Object Gateways, Metadata Servers, NFS, iSCSI gateways, and Dashboard servers.

```bash
subscription-manager repos --enable=rhel-7-server-rhceph-4-tools-rpms
```

```bash
yum update
```

* The Monitor daemons use port 6789 for communication within the Ceph storage clus*ter.
* On each Ceph OSD node, the OSD daemons use several ports in the range 6800-7300:
* The Ceph Manager (ceph-mgr) daemons use ports in range 6800-7300. Consider colocating the ceph-mgr daemons with Ceph Monitors on same nodes.
* The Ceph Metadata Server nodes (ceph-mds) use port range 6800-7300.

## Configuration des disques


* on utilise l'utilitaire "percli"
* Disque principal : deux disque en mirroir
*  un disque de donénes pour CEPH


Installation de l'utilitaire "perccli"
```bash
cd 
mkdir perccli
cd perccli
wget https://dl.dell.com/FOLDER04470715M/1/perccli_7.1-007.0127_linux.tar.gz .
tar -xzvf perccli_7.1-007.0127_linux.tar.gz
rpm -ivh Linux/perccli-007.0127.0000.0000-1.noarch.rpm

ls /opt/MegaRAID/
```

Infos sur les disques :

```bash
/opt/MegaRAID/perccli/perccli64 /c0/e32/sall show
```

Infos sur les disques virtuels
```bash
/opt/MegaRAID/perccli/perccli64 /c0/vall show
```

Suppression d'un disque virtuel
```bash
/opt/MegaRAID/perccli/perccli64 /c0/v1 del
```

Création du RAID 0
```bash
/opt/MegaRAID/perccli/perccli64 /c0/vall show
```

Configuration du disque pour CEPH
```bash
# si la crte support le mode jbod
/opt/MegaRAID/perccli/perccli64 /c0/e32/s2 set jbod

# sinon, on fait du RAID0, sans cache
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off

```

/opt/MegaRAID/perccli/perccli64 /c0/v1 del
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off


si besoin RAID

* Create a single RAID 0 volume with write-back for each Ceph OSD data drive with write-back cache enabled.
*  Enabling pass-through mode helps avoid caching logic, and generally results in much lower latency for fast media.

## configuration du réseau

### Interace principale, sur NIC1

```bash
DEVICE=eth0
BOOTPROTO=static
ONBOOT=yes
TYPE=Ethernet
HWADDR=00:24:e8:3b:c2:02
IPADDR=10.129.172.11
BROADCAST=10.129.172.255
NETMASK=255.255.255.0
NETWORK=10.129.172.0
DEFROUTE=yes
DNS1=192.44.75.10
DNS2=192.108.115.2
NM_CONTROLLED=no
```

### br-storage, Vlan 176, sur NIC2

ifcfg-eth1.176

```bash
DEVICE=eth1.176
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-storage
```

ifcfg-br-storage

```bash
DEVICE=br-storage
TYPE=Bridge
BOOTPROTO=none
IPADDR=192.168.176.12
NETMASK=255.255.255.0
BROADCAST=192.168.176.255
NETWORK=192.168.176.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-storage 10.129.176.10
ping -c 3 -I br-storage 10.129.176.11
ping -c 3 -I br-storage 10.129.176.12
```

### br-mgmt, Vlan 177, sur NIC3

ifcfg-eth2.177

```bash
DEVICE=eth2.177
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-mgmt
```

ifcfg-br-mgmt

```bash
DEVICE=br-mgmt
TYPE=Bridge
BOOTPROTO=none
IPADDR=192.168.177.12
NETMASK=255.255.255.0
BROADCAST=192.168.177.255
NETWORK=192.168.177.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-mgmt 10.129.177.10
ping -c 3 -I br-mgmt 10.129.177.11
ping -c 3 -I br-mgmt 10.129.177.12
```


Also, edit the Ceph configuration file, /etc/ceph/ceph.conf, to instruct Ceph to


## Config ansible 

### sur tous les noeuds CEPH

```bash
adduser ansible-user
passwd ansible-user
 ansible2020

cat << EOF >/etc/sudoers.d/ansible-user
ansible-user ALL = (root) NOPASSWD:ALL
EOF

chmod 0440 /etc/sudoers.d/ansible-user
```

### sur le noeud "ansible"  (director)

```bash
subscription-manager repos --enable=rhel-7-server-ansible-2.8-rpms

yum update
```

```bash
sudo su - ansible-user
ssh-keygen
  /home/ansible-user/.ssh/id_rsa
  /home/ansible-user/.ssh/id_rsa.pub
  
ssh-copy-id ansible-user@ceph-poc-01.imta.fr
ssh ansible-user@ceph-poc-01.imta.fr

ssh-copy-id ansible-user@ceph-poc-02.imta.fr
ssh ansible-user@ceph-poc-02.imta.fr

ssh-copy-id ansible-user@ceph-poc-03.imta.fr  
ssh ansible-user@ceph-poc-03.imta.fr

ssh-copy-id ansible-user@os-director-02.imta.fr  
ssh ansible-user@os-director-02.imta.fr  


ssh-copy-id ansible-user@localhost
ssh ansible-user@localhost
touch ~/.ssh/config

Host node1
   Hostname ceph-poc-01.imta.fr
   User ansible-user
Host node2
   Hostname ceph-poc-02.imta.fr
   User ansible-user
Host node2
   Hostname ceph-poc-03.imta.fr
   User ansible-user
Host node-metrics
   Hostname os-director-02.imta.fr
   User ansible-user

chmod 600 ~/.ssh/config
```


```bash
yum install ceph-ansible

cd  /usr/share/ceph-ansible

mkdir -p inventory
touch inventory/hosts
```

```bash
[mons]
ceph-poc-01.imta.fr
ceph-poc-02.imta.fr
ceph-poc-03.imta.fr
```


Modif fichier de conf

```bash
[defaults]
+ inventory = ./inventory/hosts # Assign a default inventory directory
```


## configurer l'accées à la regitry docker locale

A faire sur les neouds CEPH, et sur le neoud de metrics

```bash
echo 'INSECURE_REGISTRY="--insecure-registry 10.129.172.5:8787 --insecure-registry 10.129.172.3:8787 --insecure-registry 10.129.172.2:13787"' >> /etc/sysconfig/docker
# on peut aussi ajouter le CA de la machine "Director-01"
systemctl restart docker

curl -X GET http://10.129.172.3:8787/v2/_catalog

docker pull  10.129.172.2:13787/rhceph/rhceph-4-rhel8
docker images
docker rmi 85e371cd1f53
```

```bash
yum install -y docker

#cp -p  /etc/containers/registries.conf /etc/containers/registries.conf.DIST

sudo tee /etc/containers/registries.conf > /dev/null <<EOT
[registries.search]
registries = ['registry.access.redhat.com', 'registry.redhat.io', 'docker.io','10.129.172.5:8787', '10.129.172.3:8787','10.129.172.2:13787']

[registries.insecure]
registries = ['10.129.172.3:8787' ]

[registries.block]
registries = ['*']

EOT

cat /etc/containers/registries.conf

systemctl restart docker
```

```bash

docker pull 10.129.172.3:8787/rhceph/rhceph-4-dashboard-rhel8
docker pull 10.129.172.3:8787/openshift4/ose-prometheus:4.1
docker pull 10.129.172.3:8787/openshift4/ose-prometheus-alertmanager:4.1
docker pull 10.129.172.3:8787/openshift4/ose-prometheus-node-exporter:v4.1
```

### Configuration 

``` bash
cd /usr/share/ceph-ansible/group_vars
cp -p all.yml all.yml.DIST
```

``` bash
---
fetch_directory: /usr/share/ceph-ansible/ceph-ansible-keys

redhat_package_dependencies: []
upgrade_ceph_packages: False
ceph_test: false

ip_version: ipv4
public_network: 192.168.176.0/24
cluster_network: 192.168.177.0/24
monitor_interface: br-storage
monitor_address_block: 192.168.176.0/24
mon_use_fqdn: false
radosgw_interface: br-storage
radosgw_address_block: 192.168.176.0/24
configure_firewall: false

ceph_rhcs_version: 4
ceph_origin: repository
ceph_repository: rhcs
ceph_repository_type: cdn
containerized_deployment: true
ceph_docker_registry: 10.129.172.3:8787
ceph_docker_registry_auth: false
#ceph_docker_registry_username: <service-account-user-name>
##ceph_docker_registry_password: <token>
ceph_docker_image: rhceph/rhceph-4-rhel8
ceph_docker_image_tag: latest
docker_pull_timeout: 600s

dashboard_enabled: true
dashboard_admin_user: admin
dashboard_admin_password: CEPHstack20!

prometheus_container_image: 10.129.172.3:8787/openshift4/ose-prometheus:4.1
node_exporter_container_image: 10.129.172.3:8787/openshift4/ose-prometheus-node-exporter:v4.1
alertmanager_container_image: 10.129.172.3:8787/openshift4/ose-prometheus-alertmanager:4.1

grafana_admin_user: admin
grafana_admin_password: CEPHstack20!
grafana_container_image: 10.129.172.3:8787/rhceph/rhceph-4-dashboard-rhel8
grafana_server_addr: 10.29.41.131
```

### Déploiement

``` bash
ansible-playbook site-container.yml -i inventory/hosts
```

## modif de la conf pour accéder au dashbord grafana

```bash
/etc/grafana/grafana.ini
#http_addr = 10.129.176.13
http_addr = 10.129.41.131

docker container restart grafana-server

http://10.129.41.131:3000
```


## vérifiation


Status du cluster : From a Ceph Monitor node
```bash
docker ps

docker exec ceph-mon-ceph-poc-01 ceph health
  HEALTH_WARN 3 pools have too few placement groups

docker exec ceph-mon-ceph-poc-01 ceph -v
ceph version 14.2.8-81.el8cp (0336e23b7404496341b988c8057538b8185ca5ec) nautilus (stable)

docker exec ceph-mon-ceph-poc-01 ceph status
  cluster:
    id:     36eea4f3-07cb-41a0-8ead-c550fec0b0e4
    health: HEALTH_WARN
            4 pools have too few placement groups
 
  services:
    mon: 3 daemons, quorum ceph-poc-01,ceph-poc-02,ceph-poc-03 (age 36h)
    mgr: ceph-poc-02(active, since 6d), standbys: ceph-poc-01, ceph-poc-03
    mds: cephfs:1 {0=ceph-poc-01=up:active} 2 up:standby
    osd: 3 osds: 3 up (since 36h), 3 in (since 36h)
    rgw: 3 daemons active (ceph-poc-01.rgw0, ceph-poc-02.rgw0, ceph-poc-03.rgw0)
 
  task status:
    scrub status:
        mds.ceph-poc-01: idle
 
  data:
    pools:   9 pools, 168 pgs
    objects: 246 objects, 13 KiB
    usage:   3.1 GiB used, 405 GiB / 408 GiB avail
    pgs:     168 active+clean

docker exec ceph-mon-ceph-poc-01 ceph df
RAW STORAGE:
    CLASS     SIZE        AVAIL       USED       RAW USED     %RAW USED 
    hdd       408 GiB     405 GiB     57 MiB      3.1 GiB          0.75 
    TOTAL     408 GiB     405 GiB     57 MiB      3.1 GiB          0.75 
 
POOLS:
    POOL                           ID     STORED      OBJECTS     USED        %USED     MAX AVAIL 
    cephfs_data                     1         0 B           0         0 B         0       128 GiB 
    cephfs_metadata                 2      10 KiB          22     1.5 MiB         0       128 GiB 
    defaults.rgw.buckets.data       3         0 B           0         0 B         0       128 GiB 
    defaults.rgw.buckets.index      4         0 B           0         0 B         0       128 GiB 
    .rgw.root                       5     3.6 KiB           8     1.5 MiB         0       128 GiB 
    default.rgw.control             6         0 B           8         0 B         0       128 GiB 
    default.rgw.meta                7       393 B           2     384 KiB         0       128 GiB 
    default.rgw.log                 8     3.4 KiB         206       6 MiB         0       128 GiB 
    test                            9         0 B           0         0 B         0       128 GiB 

docker exec ceph-mon-ceph-poc-01 ceph  osd ls
0
1
2


docker exec ceph-mon-ceph-poc-01 ceph osd pool ls
cephfs_data
cephfs_metadata
defaults.rgw.buckets.data
defaults.rgw.buckets.index
.rgw.root
default.rgw.control
default.rgw.meta
default.rgw.log


 docker exec ceph-mon-ceph-poc-01 ceph osd dump | grep repli
pool 1 'cephfs_data' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode warn last_change 22 flags hashpspool stripe_width 0 application cephfs
pool 2 'cephfs_metadata' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode warn last_change 22 flags hashpspool stripe_width 0 pg_autoscale_bias 4 pg_num_min 16 recovery_priority 5 application cephfs
pool 3 'defaults.rgw.buckets.data' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode warn last_change 56 flags hashpspool stripe_width 0 application rgw
pool 4 'defaults.rgw.buckets.index' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode warn last_change 57 flags hashpspool stripe_width 0 application rgw
pool 5 '.rgw.root' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode warn last_change 60 flags hashpspool stripe_width 0 application rgw
pool 6 'default.rgw.control' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode warn last_change 62 flags hashpspool stripe_width 0 application rgw
pool 7 'default.rgw.meta' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode warn last_change 65 flags hashpspool stripe_width 0 application rgw
pool 8 'default.rgw.log' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode warn last_change 66 flags hashpspool stripe_width 0 application rgw

```


Test du stockage : From a Ceph Monitor node
```bash
docker exec ceph-mon-ceph-poc-01 ceph osd pool create test 8

docker exec ceph-mon-ceph-poc-01 touch /tmp/hello-world.txt
docker exec ceph-mon-ceph-poc-01 rados --pool test put hello-world /tmp/hello-world.txt

docker exec ceph-mon-ceph-poc-01 rados --pool test get hello-world /tmp/fetch.txt
docker exec ceph-mon-ceph-poc-01 cat /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rm -f /tmp/hello-world.txt
docker exec ceph-mon-ceph-poc-01 rm -f /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rados --pool test rm  hello-world

docker exec ceph-mon-ceph-poc-01 ceph osd pool rm  test test --yes-i-really-really-mean-it
  Error EPERM: pool deletion is disabled; you must first set the mon_allow_pool_delete config option to true before you can destroy a pool



```

## Modification

```bash
ansible-playbook site-container.yml --limit mdss -i hosts
ansible-playbook site-container.yml --limit clients -i hosts
ansible-playbook site-docker.yml --limit rgws -i hosts
```
In addition to verifying the storage cluster status, you can use the ceph-medic utility to overall diagnose the Ceph Storage cluster. See the Installing and Using ceph-medic to Diagnose a Ceph Storage Cluster chapter in the Red Hat Ceph Storage 4 Troubleshooting Guide.
https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html-single/troubleshooting_guide/#installing-and-using-ceph-medic-to-diagnose-a-ceph-storage-cluster




* https://docs.ceph.com/ceph-ansible/master/day-2/purge.html


## configuration CEPH pour Openstack

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/12/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index


Create the pool 
```bash
# By default, Ceph block devices use the rbd pool.
docker exec ceph-mon-ceph-poc-01 ceph osd pool create volumes 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create images 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create vms 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create backups 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create metrics 16
```


Create a client.openstack
```bash
docker exec -it ceph-mon-ceph-poc-01 bash

ceph auth add client.openstack mgr 'allow *' mon 'profile rbd' osd 'profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups, profile rbd pool=metrics'

ceph osd pool ls

ceph auth list
  client.openstack
    key: AQDSCSZfB0LOEhAA+5Pi3dOUpIZrQV1vgljkXQ==
    caps: [mgr] allow *
    caps: [mon] profile rbd
    caps: [osd] profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups, profile rbd pool=metrics

cat /etc/ceph/ceph.conf | grep fsid
fsid = 45853161-a453-492a-87bb-eeb7322bbdc7

```

* Ceph client key  : AQDSCSZfB0LOEhAA+5Pi3dOUpIZrQV1vgljkXQ==
* file system ID  (fsid) : 45853161-a453-492a-87bb-eeb7322bbdc7


### Créaton des pools

On teste avec pg_num=16. 



## Opérations complémentaires

### Détail sur les pool pou openstack

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/deploying_an_overcloud_with_containerized_red_hat_ceph/index#ceph-rgw


https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/deploying_an_overcloud_with_containerized_red_hat_ceph/index#cinder-backup-ceph


https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/deploying_an_overcloud_with_containerized_red_hat_ceph/index#proc_ceph-configuring-osp-pools-assembly_ceph-second-tier-storage


```bash
parameter_defaults:
  CephPools:
    - name: volumes
      pg_num: 64
      rule_name: standard
      application: rbd

    - name: vms
      pg_num: 64
      rule_name: standard
      application: rbd

    - name: backups
      pg_num: 64
      rule_name: standard
      application: rbd

    - name: images
      pg_num: 64
      rule_name: standard
      application: rbd

    - name: metrics
      pg_num: 64
      rule_name: standard
      application: openstack_gnocchi

si pg_num = 64 pour les 5 pool openstack :
Error ERANGE:  pg_num 64 size 3 would mean 888 total pgs, which exceeds max 750 (mon_max_pg_per_osd 250 * num_in_osds 3)


Total PGs = (Total_number_of_OSD * 100) / max_replication_count
          = (3 * 100) / 3
          = 100
    -> puissance de 2 la plus proche : 128

Total PGs per pool Calculation:
Total PGs = ((Total_number_of_OSD * 100) / max_replication_count) / pool count
          = (( 3 * 100 ) / 3 ) / ( 8 + 5) 
          = 100 / 13
          = 7.69
    -> puissance de 2 la plus proche : 8

vérifi des pool existants :

ceph osd pool get cephfs_data  pg_num -> 8
ceph osd pool get cephfs_metadata pg_num -> 8
ceph osd pool get defaults.rgw.buckets.data  pg_num  -> 8
ceph osd pool get defaults.rgw.buckets.index  pg_num -> 8
ceph osd pool get .rgw.root  pg_num -> 32
ceph osd pool get default.rgw.control  pg_num -> 32
ceph osd pool get default.rgw.meta  pg_num -> 32
ceph osd pool get default.rgw.log  pg_num -> 32


docker exec ceph-mon-ceph-poc-01 ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config show | grep mon_max_pg_per_osd
   "mon_max_pg_per_osd": "250",

docker exec ceph-mon-ceph-poc-01 ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config show | grep osd_max_pg_per_osd_hard_ratio
   "osd_max_pg_per_osd_hard_ratio": "3.000000",


160 pg actif.
888-160 /64 
pg_num 64 size 3 would mean 888 total pgs ???

docker exec ceph-mon-ceph-poc-01 ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config show | grep mon_allow_pool_delete
```

### getsion de pool

https://stackoverflow.com/questions/40771273/ceph-too-many-pgs-per-osd

```bash
ceph osd pool create <pool-name> <pg-number> <pgp-number> - To create a new pool

ceph osd pool get <pool-name> pg_num - To get number of PG in a pool

ceph osd pool get <pool-name> pgp_num - To get number of PGP in a pool

ceph osd pool set <pool-name> pg_num <number> - To increase number of PG in a pool

ceph osd pool set <pool-name> pgp_num <number> - To increase number of PGP in a pool
```

### pour supprimer un pool

* https://access.redhat.com/solutions/3300281

Activer la suppression

```bash
docker exec ceph-mon-ceph-poc-01 ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config show | grep mon_allow_pool_delete
docker exec ceph-mon-ceph-poc-01  ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config set mon_allow_pool_delete true
```


Supprimer le pool

```bash
docker exec ceph-mon-ceph-poc-01 ceph osd pool delete  volumes volumes --yes-i-really-really-mean-it
```

Error ERANGE:  pg_num 64 size 3 would mean 888 total pgs, which exceeds max 750 (mon_max_pg_per_osd 250 * num_in_osds 3)
docker exec ceph-mon-ceph-poc-01 ceph osd pool rm  test test --yes-i-really-really-mean-it

750 (mon_max_pg_per_osd 250 * num_in_osds 3)
750 (mon_max_pg_per_osd 250 * num_in_osds 3)





## tests

### acceder la registry sans la déclarer unsecure


Test de copie de la clé de CA pour accéder à laregistry sans quelle soit "unsecure"

```bash
scp /etc/pki/ca-trust/source/anchors/ca.crt.pem root@ceph-poc-01:/etc/pki/ca-trust/source/anchors/
ssh root@ceph-poc-01 update-ca-trust extract

800  scp /etc/pki/ca-trust/source/anchors/ca.crt.pem root@ceph-poc-01:/etc/pki/ca-trust/source/anchors/
  801  ssh root@ceph-poc-01 update-ca-trust extract
  802  scp /etc/pki/ca-trust/source/anchors/ca.crt.pem root@ceph-poc-02:/etc/pki/ca-trust/source/anchors/
  803  ssh root@ceph-poc-02 update-ca-trust extract
  804  scp /etc/pki/ca-trust/source/anchors/ca.crt.pem root@ceph-poc-03:/etc/pki/ca-trust/source/anchors/
  805  ssh root@ceph-poc-03 update-ca-trust extract
  806  scp /etc/pki/ca-trust/source/anchors/ca.crt.pem root@os-director-02:/etc/pki/ca-trust/source/anchors/
  807  ssh root@os-director-02 update-ca-trust extract
```

Pour info, l'authentification à la registry est géré dans le fichier "/usr/share/ceph-ansible/roles/ceph-container-common/tasks/registry.yml"


### améliorer l'accès à Grafana

``` bash
   "ansible_all_ipv4_addresses": [
        "10.129.41.131", 
        "10.129.172.14", 
        "172.17.0.1"
    ], 


- name: set grafana_server_addr fact - ipv4
  set_fact:
    grafana_server_addr: "{{ hostvars[inventory_hostname]['ansible_all_ipv4_addresses'] | ips_in_ranges(public_network.split(',')) | first }}"
  when:
    - groups.get(grafana_server_group_name, []) | length > 0
    - ip_version == 'ipv4'
    - dashboard_enabled | bool
    - inventory_hostname in groups[grafana_server_group_name]
```





### INSTALLING RED HAT CEPH STORAGE USING THE COCKPIT WEB INTERFACE

https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/4/html/installation_guide/installing-red-hat-ceph-storage-using-the-cockpit-web-interface

```bash
rpm -q cockpit
yum install cockpit

systemctl status cockpit.socket
systemctl enable --now cockpit.socket
systemctl status cockpit.socket

 - Logs begin at jeu. 2020-07-09 22:16:07 CEST, end at jeu. 2020-07-09 22:23:43 CEST. --
juil. 09 22:21:02 ceph-poc-01.imta.fr systemd[1]: Starting Cockpit Web Service Socket.
juil. 09 22:21:02 ceph-poc-01.imta.fr update-motd[2302]: /usr/share/cockpit/motd/update-motd: line 24: /run/coc
juil. 09 22:21:02 ceph-poc-01.imta.fr ln[2313]: /bin/ln: failed to create symbolic link ‘/run/cockpit/motd’: No
juil. 09 22:21:02 ceph-poc-01.imta.fr systemd[1]: Listening on Cockpit Web Service Socket.
```

```bash
subscription-manager repos --enable=rhel-7-server-rhceph-4-tools-rpms

yum install cockpit-ceph-installer

rpm -q cockpit-ceph-installer
su - ansible-user

sudo su -
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080


sudo docker login -u DISI1 https://registry.redhat.io
 DiSi2003

cat /etc/containers/registries.conf 

cat /etc/containers/registries.conf  | grep registry.redhat.io

sudo ansible-runner-service.sh -s

```

``` bash
ssh-copy-id -f -i /usr/share/ansible-runner-service/env/ssh_key.pub root@ceph-poc-01
ssh-copy-id -f -i /usr/share/ansible-runner-service/env/ssh_key.pub root@ceph-poc-02
ssh-copy-id -f -i /usr/share/ansible-runner-service/env/ssh_key.pub root@ceph-poc-03
ssh-copy-id -f -i /usr/share/ansible-runner-service/env/ssh_key.pub root@localhost
```


Token pour la récupération des images

``` bash
Username is 2662793|ceph and password is the token below:
eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI1NWE1YzMxNjc1MDE0MTRjYThjNmMyMTQ5OTFiNjRiNCJ9.mGdDip4gsmL6nTqLXL7XiXUcmHjAQeMcOFJg8IBg4ZLkvnHEV95DhkJBBiCOJFdasxLcCFWuux52cSODaENutsXBuwzVY3xS2uzKthvBKPMud1uzqiVWcVoTPSDQQsRD4fNGD14dqF0UBcd9mzf-dDuJxgxKzbDdfQfOL864dHZh1JJKu-fm6XuG3H_M2uDVk_DPl651yRJSrKRQ011804_X6EYSE-n3qBi8R8O5A2Ts6Pc1PD4P9HDz_Atuwue5frJny4EoechH8SlnZPD85NJOA2kQv56bjNHiTKeDHQ4xYxpJHbqHreCbps0GTgB8WBT6Z_P8nSUsstBBBf2HAAGJFZ8K7z9DxS7tW827Tdyl6dx2S3e33C7VKmjUtisb3a9WVB3uw7iAnxznfS127IL9gvv-duR5HtPWmUSipOf8lPLHL5NZj5QiifvWSJxKrqPjScEAdku7cc_lLngtUc7Ai9jVd_7BaKEgnQRCBteriMzhckI6ad4ItuMDtMykO3_AM0QafsDZg1vDX7oq-6ZMesXDq1Yqpqz_cSBsPbSyWv9E2aZDjHiIFaebtXPSEkfJ2EGywU3SD1JEV9xz-TYWRNCuYLdUE1j_43kG4bjhTXB6Yhk-Vgc_z6bPyEF-3vaD9wLeJgEHSqN2azbLt0X8JXTFy8B5GhFtFcfM-A4
```

```bash
/usr/libexec/platform-python /usr/bin/ansible-playbook -i /usr/share/ansible-runner-service/inventory site-container.yml


3b25794c067d
b8600409a22e

```


```bash
ansible-playbook ice/inventory site-container.yml
```



```bash
# nmap 10.129.41.130

Starting Nmap 7.60 ( https://nmap.org ) at 2020-07-21 13:51 CEST
Nmap scan report for br-os-director-01.imta.fr (10.129.41.130)
Host is up (0.00035s latency).
Not shown: 959 filtered ports, 39 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
8088/tcp open  radan-http

Nmap done: 1 IP address (1 host up) scanned in 49.20 seconds
```

```bash
netstat -ltunp | grep 9090
tcp6       0      0 :::9090                 :::*                    LISTEN      1/systemd

lsof -i -P |grep 9090
systemd       1             root  151u  IPv6 1385319      0t0  TCP *:9090 (LISTEN)

sudo ss -tulpn | grep :9090
tcp    LISTEN     0      128    [::]:9090               [::]:*                   users:(("systemd",pid=1,fd=43))

iptables-save 
iptables-save | grep 9090

sudo iptables -t filter -I INPUT 6 -p tcp -m multiport --dports 9090 -m state --state NEW -m comment --comment "144 cockpit ipv4" -j ACCEPT
```
