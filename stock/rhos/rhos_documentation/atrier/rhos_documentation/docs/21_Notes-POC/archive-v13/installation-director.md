

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_getting_started_guide/sec-virtualization_getting_started-quickstart_virt-manager-create_vm




## Prérequis

Une machine avec RHEL 7 installé, et enregistré chez redhat.

Souscription : 

Config Machine

Config réseau
* un accès à la machine
* un accés au réseau "Provisioning or Control Plane network,"  (ssh + ansible vers les serveurs)
* un accés au réseau "external" ( enables access to OpenStack Platform repositories, container image sources, and other servers such as DNS servers or NTP servers. Use this network for standard access the undercloud from your workstation. You must manually configure an interface on the undercloud to access the external network.)

zattention, Director utilise la première IP de la zone ctrl-plane pour le sservice sinternes comme rabbitMq
http://10.129.172.128:15672


* Do not use the same Provisioning or Control Plane NIC as the one that you use to access the director machine from your workstation. The director installation creates a bridge by using the Provisioning NIC, which drops any remote connections. Use the External NIC for remote connections to the director system.

* The Provisioning network requires an IP range that fits your environment size. Use the following guidelines to determine the total number of IP addresses to include in this range:
    * Include at least one temporary IP address for each node that connects to the Provisioning network during introspection.
    * Include at least one permanent IP address for each node that connects to the Provisioning network during deployment.
    * Include an extra IP address for the virtual IP of the overcloud high availability cluster on the Provisioning network.
    * Include additional IP addresses within this range for scaling the environment.


## Procédure

### Principe

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/)
* [https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf](https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf)
 * https://www.linuxtechi.com/install-tripleo-undercloud-centos-7/


* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation)


Si besoin configurer le proxy pour la machine :

L'installeur lance des daemon. configurer le proxy dans la fenre de shels n'est pas suffisant :

* https://www.thegeekdiary.com/how-to-configure-proxy-server-in-centos-rhel-fedora/
* https://urclouds.com/2019/01/12/how-to-configure-proxy-in-centos-7-and-rhel-7/

```bash
echo "http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/environment
echo "https_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/environment
echo "no_proxy=127.0.0.1,localhost,10.129.172.2,10.129.172.3,10.129.172.5" >> /etc/environment

#export no_proxy=${no_proxy},$(echo 192.168.100.{1..255} | sed 's/ /,/g')

```


* https://access.redhat.com/solutions/3120561

Create a new file /etc/profile.d/setproxy.sh
```bash
echo "export http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/profile.d/setproxy.sh
echo "export http_proxy=http://proxy.enst-bretagne.fr:8080/" >> /etc/profile.d/setproxy.sh
echo "export no_proxy=\"127.0.0.1,localhost,10.129.172.2,10.129.172.3,10.129.172.128\"" >> /etc/profile.d/setproxy.sh
```


Create the stack user, avec droits root

```bash
useradd stack
passwd stack
echo "stack ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/stack
chmod 0440 /etc/sudoers.d/stack
```

Créer l'arbo de référence

```bash
su - stack
mkdir ~/images
mkdir ~/templates
exit
```

Gestion du hostname

```bash
hostname
hostname -f

#sudo hostnamectl set-hostname manager.example.com
#sudo hostnamectl set-hostname --transient manager.example.com

```

Config du "/etc/hosts" : The IP address in /etc/hosts must match the address that you plan to use for your undercloud public API.

```bash
echo '127.0.0.1   os-director-01.imta.fr os-director-01' >> /etc/hosts
```


Gestion des souscription

```bash
sudo su -
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
subscription-manager register
  DISI1
  DiSi2003
The system has been registered with ID: 58ffc6d7-3b30-4c5b-8b36-85a0542acc66
The registered system name is: os-director-01.imta.fr
```

localectl
   System Locale: LANG=en_US.UTF-8
       VC Keymap: us
      X11 Layout: n/a


``` bash

subscription-manager list --available --all --matches="Red Hat OpenStack"
  Subscription Name:   60 Day Supported Red Hat OpenStack Platform Evaluation
  Provides:
     ...
     Red Hat OpenStack
     ...
  ...
  Pool ID:             8a85f9997322c047017322ec1623060a
```

```bash
subscription-manager attach --pool=8a85f9997322c047017322ec1623060a
```


```bash
subscription-manager repos --disable=*
subscription-manager repos \
 --enable=rhel-7-server-rpms \
 --enable=rhel-7-server-extras-rpms \
 --enable=rhel-7-server-rh-common-rpms \
 --enable=rhel-ha-for-rhel-7-server-rpms \
 --enable=rhel-7-server-openstack-13-rpms
```

Ajout des packages pour se connecter à CEPH

Avec RHOS 13 :  "regardless of external cluster being rhcs3 or rhcs4, on the osp13 undercloud we need to install ceph-ansible from rhcs3 ... which supports and is tested with the version of ansible and python shipping with osp13"

```bash
sudo subscription-manager repos --enable=rhel-7-server-rhceph-3-tools-rpms
```

vérification des package
```bash
subscription-manager repos  --list
subscription-manager repos  --list-enabled
subscription-manager repos  --list-disabled
```


```bash
yum update -y
reboot
```



Pour info, le fichier de config du subscription manager est  '/etc/rhsm/rhsm.conf'. on peut configurer
Installation des packages pour Director

```bash
yum install -y python-tripleoclient
```


## configuration de l'installation


```bash
su - stack
cp /usr/share/instack-undercloud/undercloud.conf.sample ~/undercloud.conf-ref
cp /usr/share/instack-undercloud/undercloud.conf.sample ~/undercloud.conf
```

undercloud.conf

```bash
[DEFAULT]

undercloud_hostname = os-director-01.imta.fr

local_interface = eth1
local_subnet = ctlplane-subnet
local_ip = 10.129.172.5/24
local_mtu = 1500

undercloud_public_host = 10.129.172.2
undercloud_admin_host =  10.129.172.3
undercloud_service_certificate = /etc/pki/instack-certs/undercloud.pem

undercloud_nameservers = 192.44.75.10,192.108.115.2
undercloud_ntp_servers = ntp1.svc.enst-bretagne.fr,ntp3.svc.enst-bretagne.fr

overcloud_domain_name = localdomain

subnets = ctlplane-subnet

inspection_interface = br-ctlplane
undercloud_update_packages = true

enable_ui = true

clean_nodes = true

[auth]

[ctlplane-subnet]
cidr = 10.129.172.0/24
gateway = 10.129.172.1
dhcp_start = 10.129.172.100
dhcp_end = 10.129.172.199
inspection_iprange = 10.129.172.90,10.129.172.99

```

Les valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway" sont utilisée ensuite dans la génération des certificats et la configuration du proxy.

## Gestion des certificats


* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-SSLTLS_Certificate_Configuration](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-SSLTLS_Certificate_Configuration)

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA)


Création de la CA

```bash
sudo su - stack

sudo touch /etc/pki/CA/index.txt
echo '1005' | sudo tee /etc/pki/CA/serial

mkdir undercloud-cert
cd undercloud-cert

sudo openssl genrsa -out ca.key.pem 4096
sudo openssl req  -key ca.key.pem -new -x509 -days 7300 -extensions v3_ca -out ca.crt.pem -subj '/C=FR/ST=BRETAGNE/L=BREST/O=IMT-ATLANTIQUE/OU=STAGE/CN=os-director-01.imta.fr/emailAddress=emmanuel.braux@imt-atlantique.fr'
 
sudo cp ca.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract
```

```bash
openssl genrsa -out server.key.pem 2048

cp /etc/pki/tls/openssl.cnf openssl.cnf

#vim openssl.cnf

diff  /etc/pki/tls/openssl.cnf openssl.cnf
126c126
< # req_extensions = v3_req # The extensions to add to a certificate request
---
> req_extensions = v3_req # The extensions to add to a certificate request
130c130
< countryName_default		= XX
---
> countryName_default		= FR
135c135
< #stateOrProvinceName_default	= Default Province
---
> stateOrProvinceName_default	= BRETAGNE
138c138
< localityName_default		= Default City
---
> localityName_default		= BREST
141c141
< 0.organizationName_default	= Default Company Ltd
---
> 0.organizationName_default	= IMT-ATLANTIQUE
148c148
< #organizationalUnitName_default	=
---
> organizationalUnitName_default	= DISI
151a152
> commonName_default              = 10.129.172.2
154a156
> emailAddress_default		= emmanuel.braux@imt-atlantique.fr
224a227
> subjectAltName = @alt_names
352a356,366
> 				#
> [alt_names]
> IP.1 = 10.129.173.2
> IP.2 = 10.129.173.3
> IP.3 = 10.129.173.5
> DNS.1 = instack.localdomain
> DNS.2 = vip.localdomain
> DNS.3 = os-director-01.imta.fr
> DNS.4 = 10.129.173.2
> DNS.5 = 10.129.173.3
> DNS.6 = 10.129.173.5
```

"commonName_default" et les valeur de "[alt_names]" doivnet être adaptées à aux valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway".

```bash
sudo openssl req -config openssl.cnf -key server.key.pem -new -out server.csr.pem -subj '/C=FR/ST=BRETAGNE/L=BREST/O=IMT-ATLANTIQUE/OU=STAGE/CN=os-director-01.imta.fr/emailAddress=emmanuel.braux@imt-atlantique.fr'


sudo openssl ca -config openssl.cnf -extensions v3_req -days 3650 -in server.csr.pem -out server.crt.pem -cert ca.crt.pem -keyfile ca.key.pem

cat server.crt.pem server.key.pem > undercloud.pem

openssl x509 -in undercloud.pem -text | grep -A 1 'Alternative Name'
```

```bash
sudo mkdir /etc/pki/instack-certs
sudo cp undercloud.pem /etc/pki/instack-certs/

openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text | grep -A 1 'Alternative Name'
sudo semanage fcontext -a -t etc_t "/etc/pki/instack-certs(/.*)?"
sudo restorecon -R /etc/pki/instack-certs

sudo cp ca.crt.pem /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract
```

``` bash
openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text
openssl x509 -in /etc/pki/instack-certs/undercloud.pem -text | grep -A 1 'Alternative Name'

```


### Ménage si besoin 

```bash
rm -rf /etc/pki/tls/certs/undercloud*
rm -rf /etc/pki/tls/private/undercloud*
rm -rf /etc/pki/ca-trust/source/anchors/*local*
```

si besoin d modifier, revocation du certificat

```bash
cat /etc/pki/CA/index.txt

sudo openssl ca -config openssl.cnf -keyfile ca.key.pem -cert ca.crt.pem  -revoke /etc/pki/CA/newcerts/1000.pem
```

## Intégration de CEPH

Avec RHOS 13 :  "regardless of external cluster being rhcs3 or rhcs4, on the osp13 undercloud we need to install ceph-ansible from rhcs3 ... which supports and is tested with the version of ansible and python shipping with osp13"

```bash

yum list \*ceph-ansible\*
yum list \*ansible\*

sudo subscription-manager repos --enable=rhel-7-server-rhceph-3-tools-rpms
sudo yum --showduplicates list ceph-ansible | expand
sudo yum --showduplicates list ansible | expand

sudo yum install -y ceph-ansible-3.2.49-1.el7cp.noarch 
sudo yum downgrade ansible-2.6.20-1.el7ae.noarch
```

```bash

 ...
ceph-ansible.noarch   3.1.5-1.el7cp           rhel-7-server-openstack-13-rpms   
 ...
ceph-ansible.noarch   4.0.25.2-1.el7cp        rhel-7-server-rhceph-4-tools-rpms 

yum list \*ceph-ansible\*
Paquets installés
ceph-ansible.noarch                                                                              4.0.25.2-1.el7cp                                                                               rhel-7-server-rhceph-4-tools-rpms

yum downgrade ceph-ansible-3.1.5-1.el7cp.noarch


Paquets installés
ceph-ansible.noarch                                                                              3.1.5-1.el7cp                                                                                  @rhel-7-server-openstack-13-rpms 
Paquets disponibles
ceph-ansible.noarch                                                                              4.0.25.2-1.el7cp                                                                               rhel-7-server-rhceph-4-tools-rpms
```


sudo yum --showduplicates list ansible | expand




## lancement de l'installation

Vérifier la config du proxy dans les fichier "/etc/profile.d/setproxy.sh" et "/etc/environment", principalment "no_proxy" pour les valeurs de "undercloud_hostname", "undercloud_public_host", "undercloud_admin_host" et "[ctlplane-subnet]gateway".


Ouvrir un nouveau shell

```bash
sudo su - stack

export | grep proxy

export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,10.129.41.130,10.129.173.2,10.129.173.3,10.129.173.5
openstack undercloud install
```

```bash
#############################################################################
Undercloud install complete.
The file containing this installation's passwords is at /home/stack/undercloud-passwords.conf.
There is also a stackrc file at /home/stack/stackrc.

```

```bash
2020-07-29 08:12:51,429 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 99-refresh-completed completed
2020-07-29 08:12:51,430 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 ----------------------- PROFILING -----------------------
2020-07-29 08:12:51,431 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,434 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 Target: post-configure.d
2020-07-29 08:12:51,435 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,436 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 Script                                     Seconds
2020-07-29 08:12:51,437 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 ---------------------------------------  ----------
2020-07-29 08:12:51,438 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,446 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 10-iptables                                   0.005
2020-07-29 08:12:51,450 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 80-seedstack-masquerade                       0.036
2020-07-29 08:12:51,455 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 98-undercloud-setup                           5.889
2020-07-29 08:12:51,460 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 99-refresh-completed                          0.405
2020-07-29 08:12:51,462 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020
2020-07-29 08:12:51,463 INFO: dib-run-parts Wed Jul 29 08:12:51 CEST 2020 --------------------- END PROFILING ---------------------

/usr/share/instack-undercloud/puppet-stack-config/os-refresh-config/post-configure.d/10-iptables
/usr/libexec/os-refresh-config/post-configure.d/10-iptables


```

```bash
cat undercloud-passwords.conf
cat stackrc

```


### vérification

```bash

tail -f /home/stack/.instack/install-undercloud.log 
sudo systemctl list-units openstack-*

```


## ménage si besoin


### Infos config réseau

```
 ip addr

2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.41.130/24 brd 10.129.41.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever

3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
    inet6 fe80::1a66:daff:feed:2c3d/64 scope link 
       valid_lft forever preferred_lft forever

6: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether a6:75:4a:df:5d:ad brd ff:ff:ff:ff:ff:ff

7: tap7e3fb6c6-44: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 76:03:67:17:7d:ab brd ff:ff:ff:ff:ff:ff

8: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 8e:45:b6:36:5b:4c brd ff:ff:ff:ff:ff:ff

9: tap6876f5c8-44: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether d6:52:7e:56:2e:e3 brd ff:ff:ff:ff:ff:ff

10: br-ctlplane: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.5/24 brd 10.129.172.255 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.172.3/32 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.172.2/32 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3d/64 scope link 
       valid_lft forever preferred_lft forever

11: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:e8:26:66:40 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
```

```bash
 ip route
default via 10.129.41.1 dev eth0 
10.129.41.0/24 dev eth0 proto kernel scope link src 10.129.41.130 
10.129.172.0/24 dev br-ctlplane proto kernel scope link src 10.129.172.5 
169.254.0.0/16 dev eth0 scope link metric 1002 
169.254.0.0/16 dev eth1 scope link metric 1003 
169.254.0.0/16 dev br-ctlplane scope link metric 1010 
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 
```

## Obtaining images for overcloud nodes


### Minimal overcloud image

```bash
su - stack
source ~/stackrc
mkdir ~/images
cd ~/images


# Minimal overcloud image
sudo yum  install rhosp-director-images-minimal
tar xf /usr/share/rhosp-director-images/overcloud-minimal-latest-13.0.tar
#openstack overcloud image upload --image-path /home/stack/images/ --os-image-name overcloud-minimal.qcow2

# Single CPU architecture overclouds
sudo yum install rhosp-director-images rhosp-director-images-ipa
for i in /usr/share/rhosp-director-images/overcloud-full-latest-13.0.tar /usr/share/rhosp-director-images/ironic-python-agent-latest-13.0.tar; do tar -xvf $i; done


# Multiple CPU architecture overclouds : x86_64

sudo yum install rhosp-director-images-all

cd /tftpboot/
for arch in x86_64 ; do sudo  mkdir $arch ; done

cd ~/images
for arch in x86_64; do mkdir $arch ; done

for arch in x86_64; do for i in /usr/share/rhosp-director-images/overcloud-full-latest-13.0-${arch}.tar /usr/share/rhosp-director-images/ironic-python-agent-latest-13.0-${arch}.tar ; do tar -C $arch -xf $i ; done ; done

ls -l ~/images
ls -l ~/images/x86_64 
ls -l /tftpboot 
ls -l /tftpboot/x86_64 


#
cd ~/images
openstack overcloud image upload --image-path /home/stack/images/
openstack overcloud image upload --image-path /home/stack/images/x86_64/ --http-boot /tftpboot
#openstack overcloud image upload --image-path ~/images/ppc64le --architecture ppc64le --whole-disk --http-boot /tftpboot/ppc64le
```


### Vérification

```bash
openstack image list
+--------------------------------------+---------------------------+--------+
| ID                                   | Name                      | Status |
+--------------------------------------+---------------------------+--------+
| 44091594-de87-44fb-b528-1913d2491e08 | bm-deploy-kernel          | active |
| c8c2491e-53af-4356-95e0-a6fcdf7e36d1 | bm-deploy-ramdisk         | active |
| a18c1a1e-8563-4472-943c-cb53416e2920 | overcloud-full            | active |
| 693c5e60-b94e-4539-af45-a0b38f045aee | overcloud-full-initrd     | active |
| b85fcdf6-09bc-4388-81fb-f79e2c914cd7 | overcloud-full-vmlinuz    | active |
| 4d356304-c67d-436f-bd2e-9ba2160dcc90 | overcloud-minimal         | active |
| 7f0895dd-1912-4d0c-95f7-5f8d6bfd1287 | overcloud-minimal-initrd  | active |
| 93cb1b4e-aa67-4d66-ac2a-621bb7c65cf9 | overcloud-minimal-vmlinuz | active |

```


## Setting a nameserver for the control plane

```bash
openstack subnet set --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 ctlplane-subnet

openstack subnet show ctlplane-subnet
+-------------------+----------------------------------------------------------+
| Field             | Value                                                    |
+-------------------+----------------------------------------------------------+
| allocation_pools  | 10.129.173.100-10.129.173.199                            |
| cidr              | 10.129.173.0/24                                          |
| created_at        | 2020-07-27T10:49:38Z                                     |
| description       |                                                          |
| dns_nameservers   |                                                          |
| enable_dhcp       | True                                                     |
| gateway_ip        | 10.129.173.1                                             |
| host_routes       | destination='169.254.169.254/32', gateway='10.129.173.1' |
| id                | 467acaf0-b259-46e2-9e1e-037f17797db9                     |
| ip_version        | 4                                                        |
| ipv6_address_mode | None                                                     |
| ipv6_ra_mode      | None                                                     |
| name              | ctlplane-subnet                                          |
| network_id        | 2973baff-542f-4bdc-904b-bd671660507c                     |
| prefix_length     | None                                                     |
| project_id        | 76d8f2bf92b0401882324188cf0e3f2d                         |
| revision_number   | 0                                                        |
| segment_id        | None                                                     |
| service_types     |                                                          |
| subnetpool_id     | None                                                     |
| tags              |                                                          |
| updated_at        | 2020-07-27T10:49:38Z                                     |
+-------------------+----------------------------------------------------------+
```

## CONFIGURING A CONTAINER IMAGE SOURCE

Using the undercloud as a local registry

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#Configuring-Registry_Details-Local

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#container-images-additional-services](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/configuring-a-container-image-source#container-images-additional-services)

```bash
cd
 
# vérifier que la reistry locale fonctionne sur "undercloud_admin_host"
sudo netstat -ltun | grep 8787
curl http://10.129.172.3:8787/v2/_catalog

openstack overcloud container image prepare \
  --namespace=registry.access.redhat.com/rhosp13 \
  --push-destination=10.129.172.3:8787 \
  --prefix=openstack- \
  -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/octavia.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/services-docker/ironic.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml \
  --tag-from-label {version}-{release} \
  --output-env-file=/home/stack/templates/overcloud_images.yaml \
  --output-images-file /home/stack/local_registry_images.yaml
# attention, la commande est peu longye, sans barre de progression

rem :On ajoute "-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml" pour intégrer une instance CEPH externe
#-e /usr/share/openstack-tripleo-heat-templates/environments/manila-cephfsnative-config.yaml \

#openstack overcloud container image prepare \
#  --namespace=registry.access.redhat.com/rhosp13 \
#  --push-destination=10.129.173.3:8787 \
#  --prefix=openstack- \
#  -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
#  --set ceph_image=rhceph-4-rhel7 \
#  --output-env-file=/home/stack/templates/ceph_images.yaml \
#  --output-images-file /home/stack/ceph_local_registry_images.yaml

  
 

#curl http://10.129.172.129:8787/v1/_ping
#curl http://10.129.173.3/:8787/v2/_catalog | jq .repositories[]

cat local_registry_images.yaml
cat templates/overcloud_images.yaml
```

edit local_registry_images.yaml
```bash
ContainerImageRegistryLogin: true
ContainerImageRegistryCredentials:
  registry.access.redhat.com:
    DISI1: DiSi2003
```

```bash
sudo docker login registry.access.redhat.com
sudo openstack overcloud container image upload \
  --config-file  /home/stack/local_registry_images.yaml \
  --verbose
#fin avec un message un peu bizarre "END return value: None", mais c'ets bon
```
Vérification

```bash
curl http://10.129.173.3:8787/v2/_catalog | jq .repositories[]
...
"rhosp13/openstack-keystone"
...

curl -s http://10.129.173.3:8787/v2/rhosp13/openstack-keystone/tags/list | jq .tags
[
  "13.0-116"
]

skopeo inspect --tls-verify=false docker://10.129.173.3:8787/rhosp13/openstack-keystone:13.0-116
{
    "Name": "10.129.173.1:8787/rhosp13/openstack-keystone",
...
```

## mise à jour de la config

```bash
openstack undercloud upgrade
```

## Intégration de CEPH

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index

installation de "ceph-ansible" et intégration de la config globale "/usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml" déjà réalisée.
```

Il faut faire un fichier de config "/home/stack/templates/ceph-config.yaml", la config globale est dans ".

```bash
parameter_defaults:
  CephClientKey: AQA86R5fdxTeGhAAUvLToVVr+Y3suSrtzecDxw==
  CephClusterFSID: 36eea4f3-07cb-41a0-8ead-c550fec0b0e4
  CephExternalMonHost: 172.16.176.10, 172.16.176.11, 172.16.176.12
  CephClientUserName: openstack
  NovaRbdPoolName: vms
  CinderRbdPoolName: volumes
  GlanceRbdPoolName: images
  CinderBackupRbdPoolName: backups
  GnocchiRbdPoolName: metrics
```

## CHAPTER 6. CONFIGURING A BASIC OVERCLOUD WITH THE CLI TOOLS
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-configuring_basic_overcloud_requirements_with_the_cli_tools


Tester que IPMI est bien actvé sur un noeud distant
``` bash
ipmitool -I lanplus -H 10.29.20.32 -U root -P ospoc2020 sdr elist all
```
* https://techexpert.tips/fr/dell-idrac-fr/ipmi-sur-linterface-idrac/
* https://osric.com/chris/accidental-developer/2017/10/using-ipmitool-to-configure-dell-idrac/


``` bash
sudo su - stack
source ~/stackrc
```

création du fichier listant les hotes

``` bash
touch ~/instackenv.json
```

Exemple de fichier minimum
``` bash
{
    "nodes":[
        {
            "mac":[
                "C8:1F:66:CE:5B:98"
            ],
            "name":"os-poc-ctrl-01",
            "pm_addr":"10.29.20.31",
            "pm_type":"ipmi",
            "arch":"x86_64"
        }
    ]
}
```

Validation du fichier
``` bash
openstack overcloud node import --validate-only ~/instackenv.json
```


```bash
# ---

openstack baremetal node list 
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| e9095481-6f94-4a2e-bafd-9402f2a5c629 | os-poc-ctrl-01 | None          | power off   | manageable         | False       |
| 131f0f0e-4e57-4930-8ecf-53d97b4f22db | os-poc-ctrl-02 | None          | power on    | manageable         | False       |
| b14fd7f0-3459-4f3f-b2b9-a444bcfa0627 | os-poc-ctrl-03 | None          | power on    | manageable         | False       |
| 77dde3a1-9e11-4083-b32c-fd8d18c5e994 | os-poc-comp-01 | None          | power off   | manageable         | False       |
| ac1e3482-94f0-4b1a-87bd-c6f7574fb34e | os-poc-comp-02 | None          | power off   | manageable         | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+

# ---
Name: os-poc-ctrl-01
cycle de boot: message bbattery PERC HS, mais non bloquant
UUID: e9095481-6f94-4a2e-bafd-9402f2a5c629
root_device.serial: 6c81f660d1d33400216f34ae06a7bbe5

# ---
Name: os-poc-ctrl-02
cycle de boot: OK. (Lyfecycle controller a désactiver)
UUID: 131f0f0e-4e57-4930-8ecf-53d97b4f22db
root_device.serial: 6b083fe0d31ab400241515dd06792e14

# ---
Name: os-poc-ctrl-03
UUID: b14fd7f0-3459-4f3f-b2b9-a444bcfa0627
root_device.serial: 6b083fe0d32fd5002415247d05a01c68

# ---
Name: os-poc-comp-01
UUID: 77dde3a1-9e11-4083-b32c-fd8d18c5e994
root_device.serial: 6847beb0d517f70024114fdb0545859e

# ---
Name: os-poc-comp-02
UUID: ac1e3482-94f0-4b1a-87bd-c6f7574fb34e
root_device.serial:

# ---



export nodeUUID=ac1e3482-94f0-4b1a-87bd-c6f7574fb34e

openstack overcloud node introspect ${nodeUUID} --provide

openstack overcloud node introspect --run-validations ${nodeUUID}

openstack baremetal introspection data save ${nodeUUID}| jq .

openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"
[
  {
    "size": 146163105792,
    "serial": "6b083fe0d31ab400241515dd06792e14",
    "wwn": "0x6b083fe0d31ab400",
    "rotational": true,
    "vendor": "DELL",
    "name": "/dev/sda",
    "wwn_vendor_extension": "0x241515dd06792e14",
    "hctl": "0:2:0:0",
    "wwn_with_extension": "0x6b083fe0d31ab400241515dd06792e14",
    "by_path": "/dev/disk/by-path/pci-0000:03:00.0-scsi-0:2:0:0",
    "model": "PERC H710"
  }
]



openstack baremetal node set --property root_device='{"serial": "6b083fe0d32fd5002415247d05a01c68"}' ${nodeUUID}
```


```bash
openstack baremetal node manage [NODE UUID]
```

QUESTION : processus de l'introspection ?




### suivre les opérations, debug inspector :

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-troubleshooting_director_issues

```bash
$ source ~/stackrc
openstack workflow execution list | grep "ERROR"

openstack workflow execution list | grep "ERROR"
```

```bash
(undercloud) [stack@os-director-01 ~]$ openstack workflow execution output show 536e0e7e-d226-48f6-bc04-e6d2ceaa5b73
{
    "result": "Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state \"manageable\", the state is \"enroll\", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status."
}
```

openstack baremetal node list
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| 1dcd9a53-a299-4476-b751-630b2612e196 | os-poc-ctrl-01 | None          | None        | enroll             | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+


```bash
Exception registering nodes: {u'status': u'FAILED', u'message': [{u'result': u'Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state "manageable", the state is "enroll", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status.'}], u'result': None}
clean_up ImportNode: Exception registering nodes: {u'status': u'FAILED', u'message': [{u'result': u'Node 1dcd9a53-a299-4476-b751-630b2612e196 did not reach state "manageable", the state is "enroll", error: Failed to get power state for node 1dcd9a53-a299-4476-b751-630b2612e196. Error: IPMI call failed: power status.'}], u'result': None}
```
Node 1dcd9a53-a299-4476-b751-630b2612e196

```bash

sudo journalctl -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq

sudo systemctl  openstack-ironic-inspector
/usr/bin/python2 /usr/bin/ironic-inspector --config-file /etc/ironic-inspector/inspector-dist.conf --config-file /etc/ironic-inspector/inspector.conf

sudo systemctl  status openstack-ironic-inspector-dnsmasq
/sbin/dnsmasq --conf-file=/etc/ironic-inspector/dnsmasq.conf


log_dir=/var/log/ironic-inspector

```

Apparement, on peut tagger les node, et lancer des inspection ciblées : 
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-Automatic_Profile_Tagging
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Tagging_Nodes_into_Profiles



## Configuration de l'undercloud


### affectation d'un role à chaque noeud

Les rôles correspondent à des gabarits :

```bash
openstack flavor  list
+--------------------------------------+---------------+------+------+-----------+-------+-----------+
| ID                                   | Name          |  RAM | Disk | Ephemeral | VCPUs | Is Public |
+--------------------------------------+---------------+------+------+-----------+-------+-----------+
| 1db4c812-5498-4675-8fc0-5f03173c8246 | ceph-storage  | 4096 |   40 |         0 |     1 | True      |
| 2c747c62-a5e5-4845-853d-f7e0209523ce | control       | 4096 |   40 |         0 |     1 | True      |
| 469cb78c-e4f1-489c-97b6-0e3dc177e092 | swift-storage | 4096 |   40 |         0 |     1 | True      |
| 4c7b129c-a681-4d76-a9a4-73df02011729 | baremetal     | 4096 |   40 |         0 |     1 | True      |
| 96bde8e4-5f30-493a-a87e-6755d8d7873d | block-storage | 4096 |   40 |         0 |     1 | True      |
| fcf53097-469e-47c2-bd04-786a37de901d | compute       | 4096 |   40 |         0 |     1 | True      |
+--------------------------------------+---------------+------+------+-----------+-------+-----------+

```

Affecteation d'un role à chaque noeud

```bash
openstack overcloud profiles list
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| Node UUID                            | Node Name      | Provision State | Current Profile | Possible Profiles |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| 2c7e3680-5251-4d40-bb18-b103480cf4ef | os-poc-ctrl-02 | available       | None            |                   |
| fa4e6b87-6add-4aa3-b7fe-163ac2f65c13 | os-poc-comp-01 | available       | None            |                   |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+

(undercloud) [stack@os-director-01 ~]$ openstack baremetal node set --property capabilities='profile:control,boot_option:local' 2c7e3680-5251-4d40-bb18-b103480cf4ef
(undercloud) [stack@os-director-01 ~]$ openstack baremetal node set --property capabilities='profile:compute,boot_option:local' fa4e6b87-6add-4aa3-b7fe-163ac2f65c13

openstack baremetal node unset --property capabilities fa4e6b87-6add-4aa3-b7fe-163ac2f65c13


(undercloud) [stack@os-director-01 ~]$ openstack overcloud profiles list
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| Node UUID                            | Node Name      | Provision State | Current Profile | Possible Profiles |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| 2c7e3680-5251-4d40-bb18-b103480cf4ef | os-poc-ctrl-02 | available       | control         |                   |
| fa4e6b87-6add-4aa3-b7fe-163ac2f65c13 | os-poc-comp-01 | available       | compute         |                   |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
```

Gestion des disques de boot

```bash
export nodeUUID=2c7e3680-5251-4d40-bb18-b103480cf4ef
openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"
[
  {
    "size": 146163105792,
    "serial": "6b083fe0d31ab400241515dd06792e14",
    "wwn": "0x6b083fe0d31ab400",
    "rotational": true,
    "vendor": "DELL",
    "name": "/dev/sda",
    "wwn_vendor_extension": "0x241515dd06792e14",
    "hctl": "0:2:0:0",
    "wwn_with_extension": "0x6b083fe0d31ab400241515dd06792e14",
    "by_path": "/dev/disk/by-path/pci-0000:03:00.0-scsi-0:2:0:0",
    "model": "PERC H710"
  }
]

openstack baremetal node set --property root_device='{"serial": "6b083fe0d31ab400241515dd06792e14"}' ${nodeUUID}


export nodeUUID=2c7e3680-5251-4d40-bb18-b103480cf4ef
openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"
openstack baremetal node set --property root_device='{"serial": "6b083fe0d31ab400241515dd06792e14"}' ${nodeUUID}

export nodeUUID=fa4e6b87-6add-4aa3-b7fe-163ac2f65c13
openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"
openstack baremetal node set --property root_device='{"serial": "6847beb0d517f70024114fdb0545859e"}' ${nodeUUID}
```


```bash
openstack overcloud profiles list
```


## se conneter à 'interface de gestion

```bash
netstat -ltunp | grep 443
tcp        0      0 10.129.172.2:443        0.0.0.0:*               LISTEN      10058/haproxy  
```

dans config Haproxy :

```bash
listen ui
  bind 10.129.172.2:443 transparent ssl crt /etc/pki/instack-certs/undercloud.pem
  bind 10.129.172.3:3000 transparent
  mode http
  option forwardfor
  redirect scheme https code 301 if { hdr(host) -i 10.129.172.2 } !{ ssl_fc }
  rsprep ^Location:\ http://(.*) Location:\ https://\1
  timeout tunnel 3600s
  server 10.129.172.5 10.129.172.5:3000 check fall 5 inter 2000 rise 2
```

On accède à la machine depui son adresse dens le vlan 148.
Il faudrait lui configurer une IP dans le vLAN 172, ou gérer les histoires de route par défaut

En attendant, on lance firfox directement sur la machine

```bash
yum install firefox.
```

```bash
ssh -X
firefox  &

https://10.129.172.2
```

Récupération du mot de passe

```bash
sudo su - stack
Dernière connexion : jeudi 30 juillet 2020 à 00:06:02 CEST sur pts/0
[stack@os-director-01 ~]$ sudo hiera admin_password
fbbf3ef5ccd25da0be38cf979623a4c273017e01
```

### proxy et subscription

* https://bugs.launchpad.net/tripleo/+bug/1818590

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/advanced_overcloud_customization/sect-registering_the_overcloud

```bash
cp -r /usr/share/openstack-tripleo-heat-templates/extraconfig/pre_deploy/rhel-registration ~/templates/.
```

fichier "~/templates/rhel-registration/environment-rhel-registration.yaml"
```bash
sudo subscription-manager orgs
Username: DISI1
Password:
+-------------------------------------------+
          DISI1 Organizations
+-------------------------------------------+

Name: 2662793
Key:  2662793
```

```bash
parameter_defaults:
  rhel_reg_auto_attach: ""
  rhel_reg_activation_key: "60daysrhos"
  rhel_reg_org: "2662793"
  rhel_reg_user: "DISI1"
  rhel_reg_password:  "DiSi2003"
  rhel_reg_pool_id: "8a85f9997322c047017322ec1623060a"
  rhel_reg_repos: "rhel-7-server-rpms,rhel-7-server-extras-rpms,rhel-7-server-rh-common-rpms,rhel-ha-for-rhel-7-server-rpms,rhel-7-server-openstack-13-rpms,rhel-7-server-rhceph-4-tools-rpms"
  rhel_reg_method: "portal"
  rhel_reg_sat_repo: ""
  rhel_reg_base_url: ""
  rhel_reg_environment: ""
  rhel_reg_force: "true"
  rhel_reg_machine_name: ""
  rhel_reg_release: ""
  rhel_reg_sat_url: ""
  rhel_reg_server_url: ""
  rhel_reg_service_level: ""
  rhel_reg_type: ""
  rhel_reg_http_proxy_host: "proxy.enst-bretagne.fr"
  rhel_reg_http_proxy_port: "8080"
  rhel_reg_http_proxy_username: ""
  rhel_reg_http_proxy_password: ""

```


### configure cert between undercloud and overcloud

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-Configuring_Basic_Overcloud_Requirements_with_the_CLI_Tools#sect-Configure_overcloud_nodes_to_trust_the_undercloud_CA


créer le fichier "/home/stack/inject-trust-anchor-hiera.yaml" avec la clé  de la CA "cat /home/stack/undercloud-cert/ca.crt.pem"

```bash
parameter_defaults:
  CAMap:
    overcloud-ca:
      content: |
        -----BEGIN CERTIFICATE-----
        MIIDl ... 
        -----END CERTIFICATE-----
    undercloud-ca:
      content: |
        -----BEGIN CERTIFICATE-----
        MIIDl ... 
        -----END CERTIFICATE-----
```

## création d'un overcloud

C'est normalement assez simple. si on a exactement l'environnement de référence. Mais il faut en fait modifier pas mal de choses.

La config ets générée à partir de fichier template, dans le dossier "/usr/share/openstack-tripleo-heat-templates". Tout ce qui ne correspond pas à la config de réfernce doit être modifié : la config réseau, ...

### génération de la config par défaut

```bash
cd /usr/share/openstack-tripleo-heat-templates
$ ./tools/process-templates.py -o ~/openstack-tripleo-heat-templates-rendered_DEFAULT

 /usr/share/openstack-tripleo-heat-templates/tools/process-templates.py -o ~/tripleo-heat-templates
```

Les fichiers interressants sont les fichiers "data" :

```bash
 find . -name "*_data*"
./roles_data_undercloud.yaml
./roles_data.yaml
./network_data_ganesha.yaml
./network_data.yaml
./network/endpoints/endpoint_data.yaml
```

* network_data.yaml  :la configuartion réseau
* roles_data.yaml :  les composants à déployer
* roles_data_undercloud.yaml : les composants à déployer sur l'undercloud


* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/advanced_overcloud_customization/custom-network-interface-templates#rendering-default-network-interface-templates-for-customization

### création du fichsier de configuration du réseau

https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/features/custom_networks.html

The overcloud assigns services to the provisioning network by default

Test :

```bash
openstack overcloud netenv validate -f ~/templates/network-environment.yaml
  --> ne fonctionne pas
```

Attention, pour le controlleur, la gatewy par défaut est-celle du réseau "external". Dans la phase de déploiement, un test de ping est fait vers l'IP de cette passerelle. et si le ping échoue, ça plante. il faut donc que la passerelle soit activée, ou en tout cas, que quelque chqose réponde au ping.


Include the custom network_data file with your deployment using the -n option. Without the -n option, the deployment command uses the default network details.

Valider l'environnement réseau

```bash
openstack overcloud netenv validate -f ~/templates/network-environment.yaml
```

### création de la config CEPH

https://bugs.launchpad.net/tripleo/+bug/1838460

/home/stack/basic-cloud-templates/ceph-config.yaml

```bash
parameter_defaults:
  CephClientKey: 'AQDSCSZfB0LOEhAA+5Pi3dOUpIZrQV1vgljkXQ=='
  CephClusterFSID: '36eea4f3-07cb-41a0-8ead-c550fec0b0e4'
  CephExternalMonHost: '192.168.176.10, 192.168.176.11, 192.168.176.12'
  CephClientUserName: 'openstack'
  NovaRbdPoolName: 'vms'
  CinderRbdPoolName: 'volumes'
  GlanceRbdPoolName: 'images'
  CinderBackupRbdPoolName: 'backups'
  GnocchiRbdPoolName: 'metrics'
  # bug dashboard instale apr defaut dans CEPH4. a désactiver
  CephAnsibleExtraConfig:
    dashboard_enabled: False


```



```bash
ceph_rhcs_version: 4
ceph_origin: repository
ceph_repository: rhcs
ceph_repository_type: cdn
containerized_deployment: true
ceph_docker_registry: 10.129.172.3:8787
ceph_docker_registry_auth: false
#ceph_docker_registry_username: <service-account-user-name>
##ceph_docker_registry_password: <token>
ceph_docker_image: rhceph/rhceph-4-rhel8
ceph_docker_image_tag: latest
docker_pull_timeout: 600s
```

### creation de la config pour la registration

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-configuring_basic_overcloud_requirements_with_the_cli_tools
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/8/html/director_installation_and_usage/sect-registering_the_overcloud


/home/stack/basic-cloud-templates/redh-hat-registration.yaml

``` bash
parameter_defaults:
  rhel_reg_method : portal
  rhel_reg-activation-key : 60daysrhos
  rhel_reg_http_proxy_host: 'proxy.enst-bretagne.fr'
  rhel_reg_http_proxy_port: '8080'
  rhel_reg_http_proxy_username: ''
  rhel_reg_http_proxy_password:''
  
```

### creation de la config pour les images

/home/stack/basic-cloud-templates/overcloud_images.yaml 


si besoin de proxy pour Docker

https://access.redhat.com/solutions/3357821


### configure overcloud configuration

/home/stack/templates/node-info.yaml

```bash
parameter_defaults:
  ControllerCount: 1
  ComputeCount: 1
  OvercloudControlFlavor: control
  OvercloudComputeFlavor: compute
```


## création d'un uncercloud simple

 6.8. Creating an Environment File that Defines Node Counts and Flavors

L'option  '-e' permet d'icorpore des fichier d'environnement (environement file)

L'option '-n' permet une config réseau (network file)


```bash
cd
source ~/stackrc 

openstack overcloud deploy --templates \
  --timeout 240 \
  -e /home/stack/basic-cloud-templates/overcloud_images.yaml  \
  -e /usr/share/openstack-tripleo-heat-templates/environments/network-environment.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
  -n /home/stack/basic-cloud-templates/network_data.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/net-single-nic-with-vlans.yaml \
  -e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml \
  -e /home/stack/basic-cloud-templates/ceph-config.yaml \
  -e /home/stack/basic-cloud-templates/node-info.yaml \
  --reg-method portal \
  --reg-activation-key 60daysrhos \
  -e /home/stack/basic-cloud-templates/red-hat-registration.yaml \
  -e /home/stack/templates/rhel-registration/rhel-registration-resource-registry.yaml \
  -e /home/stack/inject-trust-anchor-hiera.yaml \
  --ntp-server ntp1.svc.enst-bretagne.fr 

#-e /usr/share/openstack-tripleo-heat-templates/extraconfig/pre_deploy/rhel-registration/rhel-registration.yaml
#-e /usr/share/openstack-tripleo-heat-templates/extraconfig/pre_deploy/rhel-registration/rhel-registration-resource-registry.yaml \
```

On peut retrouver des infos sur les erreurs :

```bash
openstack stack failures list overcloud --long
```
Sinon, on retrouve des log dans /var/log/mistral.

On peut activer des logs en ajoutant

```bash
  --log-file overcloudDeploy.log \
  --debug \
```

Autre option :

```bash
# modifier les roles
-r /home/stack/templates/roles_data.yaml \


# modifier le dossier de template
--templates /usr/share/openstack-tripleo-heat-templates \

# parametre de l'overcloud
--stack overcloud \
--libvirt-type kvm \
--overcloud-ssh-user heat-admin \

--config-download \
-e /usr/share/openstack-tripleo-heat-templates/environments/config-download-environment.yaml \

-e /home/stack/virt/enable-tls.yaml \
-e /home/stack/virt/public_vip.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ssl/tls-endpoints-public-ip.yaml \

-e /home/stack/virt/debug.yaml \

```


On peut mettre les fichier de personnalisation dans un dossier spécifique

```bash
  --environment-directory /home/stack/mytemplates/env \
```



---
Configuration idrac

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/appe-Power_Management_Drivers

```bash
B.2. Dell Remote Access Controller (DRAC)
DRAC is an interface that provides out-of-band remote management features including power management and server monitoring.

pm_type
Set this option to idrac.
pm_user; pm_password
The DRAC username and password.
pm_addr
The IP address of the DRAC host.

```
# ---

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/planning-your-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/planning-your-undercloud)






# Infos complémentaires

## Vérification de la config de CEPH

https://access.redhat.com/solutions/3509271
https://access.redhat.com/solutions/3676921
https://bugzilla.redhat.com/show_bug.cgi?id=1645134


Vérifier lq config de CEPH

sur le "director"

``` bash
sudo su - stack
source ~/stackrc

swift list
swift list overcloud_ceph_ansible_fetch_dir
  temporary_dir-20200803-152514.tar.gz
```


## Personnalisation d'un compute

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/10/html-single/advanced_overcloud_customization/index#sect-Customizing_Hieradata_for_Individual_Nodes

## Introspaction des noeuds


Suivre l'introspection

```bash
sudo journalctl -l -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq -u openstack-ironic-conductor -f
```

Import des nodes (Add nodes to Ironic)
```bash
openstack overcloud node import ~/instackenv.json
```

List des nodes
```bash
openstack baremetal node list

```

Inspection des nodes

```bash
# pour tous
openstack overcloud node introspect --all-manageable --provide

# le "Provisioning State" passe de "manageable" à "available"
# pour un noeud spécifique

openstack overcloud node introspect 265f71e3-7d58-45ef-999d-c5abee932d51 --provide
openstack overcloud node introspect --run-validations 265f71e3-7d58-45ef-999d-c5abee932d51
# 15 miniute par noeud
```

```bash
Waiting for messages on queue 'tripleo' with no timeout.
Introspection of node 2c7e3680-5251-4d40-bb18-b103480cf4ef completed. Status:SUCCESS. Errors:None
Successfully introspected 1 node(s).
...

Introspection completed.
Started Mistral Workflow tripleo.baremetal.v1.provide_manageable_nodes. Execution ID: 6bd4bd4c-9047-4bd3-8a1a-f591a6d695dc
Waiting for messages on queue 'tripleo' with no timeout.

1 node(s) successfully moved to the "available" state.

```


Voir le résultat

```bash
openstack baremetal node list 
openstack baremetal introspection data save <UUID> | jq .

# lister les interfaces, avec infos de config switch (LLDP)
openstack baremetal introspection interface list [NODE UUID]
openstack baremetal introspection interface show [NODE UUID] [INTERFACE]

```
