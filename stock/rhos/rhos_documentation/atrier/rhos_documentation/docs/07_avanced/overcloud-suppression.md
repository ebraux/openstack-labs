title: Suppression de l'overcloud

Attention, l'overcloud permet le déploiement et la getsion de l'infra Openstack.
si il est supprimé, l'infra ne peut plus être gérée, et il faudra la redéployer.
A utiliser uniquement dans les période d etest, pour une remise à zéro de la machione, si réinstallation impossible.

Source :

* [https://access.redhat.com/solutions/2210421](https://access.redhat.com/solutions/2210421)


``` bash
systemctl list-unit-files | grep tripleo | awk '{print $1}' | xargs -I {} sudo systemctl stop {}
systemctl list-unit-files | grep openstack | awk '{print $1}' | xargs -I {} sudo systemctl stop {}
systemctl list-unit-files | grep neutron | awk '{print $1}' | xargs -I {} sudo systemctl stop {}

sudo systemctl stop docker
sudo systemctl stop keepalived
sudo systemctl stop httpd



sudo rm -rf \
  /home/stack/undercloud-passwords.conf \
  /root/tripleo-undercloud-passwords \
  /var/lib/mysql  \
  /root/.my.cnf \
  /var/lib/rabbitmq \
  /root/stackrc \
  /home/stack/stackrc  \
  /var/opt/undercloud-stack \
  /var/lib/ironic-discoverd/discoverd.sqlite \
  /var/lib/ironic-inspector/inspector.sqlite  \
  /home/stack/.instack \
  /var/lib/registry \
  /httpboot/ \
  /tftpboot/ \
  /srv/* \
  /var/lib/docker

#  /opt/stack/.undercloud-setup \

dirs="ceilometer heat glance horizon ironic ironic-discoverd keystone neutron nova swift haproxy"; for dir in $dirs; do sudo rm -rf /etc/$dir ; sudo rm -rf /var/log/$dir ;  sudo rm -rf /var/lib/$dir; done

yum remove -y httpd rabbitmq-server mariadb haproxy openvswitch keepalived $(rpm -qa | grep openstack) python-tripleoclient docker os-collect-config os-net-config

rm -rf /etc/httpd/conf.d/0*
rm -rf /etc/httpd/conf.d/10*
rm -rf /etc/httpd/conf.d/15*
rm -rf /etc/httpd/conf.d/25*
rm -rf /etc/httpd/conf.d/openstack-tripleo-ui.conf*
sed -i '/:3000/d ; /:35357/d ; /:5000/d ; /:8042/d ; /:8774/d ; /:8777/d' /etc/httpd/conf/ports.conf

ifdown ifcfg-br-ctlplane
rm -rf /etc/sysconfig/network-scripts/ifcfg-br-ctlplane
mv  /etc/os-net-config/config.json  /etc/os-net-config/config.json.orig
sed -i '/ovs/d ; /OVS/d'  /etc/sysconfig/network-scripts/ifcfg-eth1

rm -rf /root/.my.cnf

reboot


yum clean all
yum repolist

```
