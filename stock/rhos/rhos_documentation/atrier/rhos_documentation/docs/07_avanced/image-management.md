

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/advanced_overcloud_customization/sect-containerized_services


Vérifier le simlages disponibles :

```bash
openstack tripleo container image list
```


openstack tripleo container image prepare default \
  --local-push-destination \
  --output-env-file containers-prepare-parameter.yaml



When you push images to the undercloud, use push_destination: true instead of push_destination: UNDERCLOUD_IP:PORT. The push_destination: true method provides a level of consistency across both IPv4 and IPv6 addresses.

--local-push-destination sets the registry on the undercloud as the location for container images. With this option, director pulls the necessary images from the Red Hat Container Catalog and pushes the images to the registry on the undercloud. Director uses the undercloud registry as the container image source. To pull container images directly from the Red Hat Container Catalog, omit this option.
--output-env-file specifies an environment file that includes include the parameters for preparing your container images. In this example, the name of the file is containers-prepare-parameter.yaml.

You can use the same containers-prepare-parameter.yaml file to define a container image source for both the undercloud and the overcloud.

The container images use multi-stream tags based on Red Hat OpenStack Platform version. This means there is no longer a latest tag.



In the example, replace my_username and my_password with your authentication credentials. Instead of using your individual user credentials, Red Hat recommends creating a registry service account and using those credentials to access registry.redhat.io content. For more information, see "Red Hat Container Registry Authentication".
https://access.redhat.com/RegistryAuthentication


The default ContainerImagePrepare parameter pulls container images from registry.redhat.io, which requires authentication.


You must set ContainerImageRegistryLogin to true if push_destination is not configured for a given strategy. If push_destination is configured in a ContainerImagePrepare strategy and the ContainerImageRegistryCredentials parameter is configured, the system logs in to fetch the containers and pushes them to the remote system. If the overcloud nodes do not have network connectivity to the registry hosts defined in the ContainerImageRegistryCredentials, set push_destination to true and ContainerImageRegistryLogin to false


## getsion des images

```bash
(undercloud) [stack@rh-director ~]$ openstack tripleo container image prepare -h 
usage: openstack tripleo container image prepare [-h]
                                                 [--environment-file <file path>]
                                                 [--environment-directory <HEAT ENVIRONMENT DIRECTORY>]
                                                 [--roles-file ROLES_FILE]
                                                 [--output-env-file <file path>]
                                                 [--dry-run]
                                                 [--cleanup <full, partial, none>]

Prepare and upload containers from a single command.

optional arguments:
  -h, --help            show this help message and exit
  --environment-file <file path>, -e <file path>
                        Environment file containing the ContainerImagePrepare
                        parameter which specifies all prepare actions. Also,
                        environment files specifying which services are
                        containerized. Entries will be filtered to only
                        contain images used by containerized services. (Can be
                        specified more than once.)
  --environment-directory <HEAT ENVIRONMENT DIRECTORY>
                        Environment file directories that are automatically
                        added to the environment. Can be specified more than
                        once. Files in directories are loaded in ascending
                        sort order.
  --roles-file ROLES_FILE, -r ROLES_FILE
                        Roles file, overrides the default roles_data.yaml in
                        the t-h-t templates directory used for deployment. May
                        be an absolute path or the path relative to the
                        templates dir.
  --output-env-file <file path>
                        File to write heat environment file which specifies
                        all image parameters. Any existing file will be
                        overwritten.
  --dry-run             Do not perform any pull, modify, or push operations.
                        The environment file will still be populated as if
                        these operations were performed.
  --cleanup <full, partial, none>
                        Cleanup behavior for local images left after upload.
                        The default 'full' will attempt to delete all local
                        images. 'partial' will leave images required for
                        deployment on this host. 'none' will do no cleanup.

```

```bash

```

Lancer la commande de gestion des images individuellement
```bash
https_proxy=10.129.173.5:3128 openstack tripleo container image prepare  -e /home/stack/containers-prepare-parameter_director.yaml --dry-run

sudo https_proxy=10.129.173.5:3128 no_proxy=rh-director.ctlplane.localdomain openstack tripleo container image prepare  -e /home/stack/containers-prepare-parameter_director.yaml
```

Les images sont ajoutés à la registry locales de l'undercloud
```bash
openstack tripleo container image list

curl -s http://rh-director.ctlplane.localdomain:8787/v2/_catalog | jq .repositories[]

curl -s http://rh-director.ctlplane.localdomain:8787/v2/rhosp-rhel8/openstack-keystone/tags/list | jq .tags
[
  "16.1-50",
  "16.1"
]

curl -s http://rh-director.ctlplane.localdomain:8787/v2/_catalog | jq .repositories[] | grep neutron
"rhosp-rhel8/openstack-neutron-server-ovn"
"rhosp-rhel8/openstack-neutron-metadata-agent-ovn"
"rhosp-rhel8/openstack-neutron-dhcp-agent"
"rhosp-rhel8/openstack-ironic-neutron-agent"
"rhosp-rhel8/openstack-neutron-metadata-agent"
"rhosp-rhel8/openstack-neutron-l3-agent"
"rhosp-rhel8/openstack-neutron-openvswitch-agent"

sudo podman search --limit 1000 "rh-director.ctlplane.localdomain:8787/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | sed "s/rh-director.ctlplane.localdomain:8787\///g" | tail -n+2

```

Dans tous les cas, 83 images, et 7 neutre "neutron-server-ovn"

Elle ne sont pas disponible en local via podman :
```bash
sudo podman images
podman images
```

Pour les supprimer :
```bash
openstack tripleo container image list
sudo openstack tripleo container image delete -y rh-director.ctlplane.localdomain:8787/rhosp-rhel8/openstack-neutron-server-ovn:16.1
```


OpenStack services are hitting the proxy for internal calls : https://access.redhat.com/solutions/4895301
Connection refused error while deploying RHOSP14 undercloud : https://access.redhat.com/solutions/3971171

```bash
sudo https_proxy=10.129.173.5:3128   podman search --limit 1000 "registry.redhat.io/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | grep -v beta | sed "s/registry.redhat.io\///g" | wc -1
117

sudo https_proxy=10.129.173.5:3128   podman search --limit 1000 "registry.redhat.io/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | grep -v beta | sed "s/registry.redhat.io\///g" | tail -n+2 > registry.redhat.io_rhosp.list

sudo podman search --limit 1000 "rh-director.ctlplane.localdomain:8787/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | sed "s/rh-director.ctlplane.localdomain:8787\///g" | tail -n+2 | wc -l
83


sudo podman search --limit 1000 "rh-director.ctlplane.localdomain:8787/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | sed "s/rh-director.ctlplane.localdomain:8787\///g" | tail -n+2> registry.rh-director.ctlplane.localdomain.list
```


Infos RedHat :

```bash
 openstack tripleo container image prepare default --local-push-destination --output-env-file /home/stack/containers-prepare-parameter-test.yaml
# Generated with the following on 2020-09-29T11:38:33.347224
#
#   openstack tripleo container image prepare default --local-push-destination --output-env-file /home/stack/containers-prepare-parameter-test.yaml
#

parameter_defaults:
  ContainerImagePrepare:
  - push_destination: true
    set:
      ceph_alertmanager_image: ose-prometheus-alertmanager
      ceph_alertmanager_namespace: registry.redhat.io/openshift4
      ceph_alertmanager_tag: 4.1
      ceph_grafana_image: rhceph-4-dashboard-rhel8
      ceph_grafana_namespace: registry.redhat.io/rhceph
      ceph_grafana_tag: 4
      ceph_image: rhceph-4-rhel8
      ceph_namespace: registry.redhat.io/rhceph
      ceph_node_exporter_image: ose-prometheus-node-exporter
      ceph_node_exporter_namespace: registry.redhat.io/openshift4
      ceph_node_exporter_tag: v4.1
      ceph_prometheus_image: ose-prometheus
      ceph_prometheus_namespace: registry.redhat.io/openshift4
      ceph_prometheus_tag: 4.1
      ceph_tag: latest
      name_prefix: openstack-
      name_suffix: ''
      namespace: registry.redhat.io/rhosp-rhel8
      neutron_driver: ovn
      rhel_containers: false
      tag: '16.1'
    tag_from_label: '{version}-{release}'
```

## Exemples de config

### Depuis la registry RedHat, vers la registry locale de l'Overcloud

Les images sont créé en tant que !
```
rh-director.ctlplane.localdomain:8787/rhosp-rhel8/openstack-aodh-api:16.1-51
```

```yaml
parameter_defaults:
  ContainerImageRegistryCredentials:
    registry.redhat.io:
      DISI1: DiSi2003

  ContainerImagePrepare:
  #--- Openstack ---
  - tag_from_label: '{version}-{release}'
    push_destination: true
    set:
      name_prefix: openstack-
      name_suffix: ''
      namespace: registry.redhat.io/rhosp-rhel8
      neutron_driver: ovn
      rhel_containers: false
      tag: '16.1'
  #--- CEPH ---
  - tag_from_label: '{version}-{release}'
    push_destination: true
    set:
      ceph_alertmanager_image: ose-prometheus-alertmanager
      ceph_alertmanager_namespace: registry.redhat.io/openshift4
      ceph_alertmanager_tag: 4.1
      ceph_grafana_image: rhceph-4-dashboard-rhel8
      ceph_grafana_namespace: registry.redhat.io/rhceph
      ceph_grafana_tag: 4
      ceph_image: rhceph-4-rhel8
      ceph_namespace: registry.redhat.io/rhceph
      ceph_node_exporter_image: ose-prometheus-node-exporter
      ceph_node_exporter_namespace: registry.redhat.io/openshift4
      ceph_node_exporter_tag: v4.1
      ceph_prometheus_image: ose-prometheus
      ceph_prometheus_namespace: registry.redhat.io/openshift4
      ceph_prometheus_tag: 4.1
      ceph_tag: latest
```




## commandes utiles

Les images sont copié dans l'environnement de root, et non dans celui de "stack".
Toutes les images dans l'environnement de "stack" peuvent être supprimées.

Lister les images
```bash
sudo podman image list
``` 

Supprimer une image
```bash
sudo podman image rm -f <ID>
```

Supprimer toutes les images locales
```bash
sudo podman image rm -f $(podman image list -q)
```

```bash
images=`curl -s https://registry.access.redhat.com/v1/search?q="*" | python -mjson.tool | grep ".name.:" | cut -d: -f2 | sed -e "s/ "//g"" -e "s/,"//g""`
for i in $images; do echo $i; done
```

curl -s https://10.129.172.22:5000/v2/search?q="*"


## Exemples en python

* https://opendev.org/openstack/tripleo-common/src/branch/master/tripleo_common/image/image_uploader.py



