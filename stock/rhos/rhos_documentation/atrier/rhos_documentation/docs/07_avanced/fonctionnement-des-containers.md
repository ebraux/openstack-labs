

## fonctionnement des containers

ils sont lancé via des serveices de systemd, avec podman.
Il y a des service qui lancent des container comme "tripleo_ironic_api.service"
et d'autre qu lancnet de scommandes dans les containers (healchec) et qui sont dons en status wait. par exmeple : tripleo_ironic_api_healthcheck.service

Accèder aux containers :

```bash
podman ps
podman exec -it ironic_api /bin/bash
```

Supprimer un container
```bash
podman rm -f ironic_api
```

Lister le simages disponibles sur la machine
```bash
podman images --format {{.Repository}}:{{.Tag}} {{.Repository}}:{{.Tag}}
```

podman create --help

Infos sur comment les container sont lancé : /var/lib/tripleo-config/
 
/usr/bin/podman exec --user root ironic_api env

cat  container-startup-config-readme.txt
cd /var/lib/tripleo-config/container-startup-config

POur trouver comment est lancé un container
```bash
find . -name ironic_api\*
```

vim ./step_4/ironic_api.json
Dans chaque dossier, on retrouve un fichier json pas container (avant c'était un gos fichier par niveau)
Dans la commande, on doit préciser le nom du container, mailgré le faait que aitenant c'ets de fichiser individuels. c'ets un peu con, mais c'ets comme ça.
```bash
paunch debug --file ./step_4/ironic_api.json  --container ironic_api --action print-cmd
```

Supprimer tous les containers
```bash
podman rm -f $(podman ps -a -q)
```

