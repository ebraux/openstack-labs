

## Voir le sinterface sur une machine

```bash
openstack baremetal introspection interface list aebce0ac-be39-4355-aafc-4fd3bb462ccd
+-----------+-------------------+----------------------+-------------------+----------------+
| Interface | MAC Address       | Switch Port VLAN IDs | Switch Chassis ID | Switch Port ID |
+-----------+-------------------+----------------------+-------------------+----------------+
| eno3      | c8:1f:66:ce:5b:9a | []                   | None              | None           |
| eno1      | c8:1f:66:ce:5b:98 | []                   | None              | None           |
| eno4      | c8:1f:66:ce:5b:9b | []                   | None              | None           |
| eno2      | c8:1f:66:ce:5b:99 | []                   | None              | None           |
+-----------+-------------------+----------------------+-------------------+----------------+
si LLDP acivé --qs infos de port

openstack baremetal introspection interface list  9ebf828e-1b7a-482b-a00c-abeed9fb2d64
+-----------+-------------------+----------------------+-------------------+----------------+
| Interface | MAC Address       | Switch Port VLAN IDs | Switch Chassis ID | Switch Port ID |
+-----------+-------------------+----------------------+-------------------+----------------+
| eno4      | 00:0a:f7:b8:fe:f5 | []                   | None              | None           |
| eno1np0   | 00:0a:f7:b8:fe:f6 | []                   | 0c:11:67:94:48:00 | Gi1/0/14       |
| eno3      | 00:0a:f7:b8:fe:f4 | []                   | 0c:11:67:94:48:00 | Gi1/0/13       |
| eno2np1   | 00:0a:f7:b8:fe:f7 | []                   | None              | None           |
+-----------+-------------------+----------------------+-------------------+----------------+


(undercloud) [stack@rh-director ~]$ openstack baremetal introspection interface show  aebce0ac-be39-4355-aafc-4fd3bb462ccd eno1
+--------------------------------------+--------------------------------------+
| Field                                | Value                                |
+--------------------------------------+--------------------------------------+
| interface                            | eno1                                 |
| mac                                  | c8:1f:66:ce:5b:98                    |
| node_ident                           | aebce0ac-be39-4355-aafc-4fd3bb462ccd |
| switch_capabilities_enabled          | None                                 |
| switch_capabilities_support          | None                                 |
| switch_chassis_id                    | None                                 |
| switch_port_autonegotiation_enabled  | None                                 |
| switch_port_autonegotiation_support  | None                                 |
| switch_port_description              | None                                 |
| switch_port_id                       | None                                 |
| switch_port_link_aggregation_enabled | None                                 |
| switch_port_link_aggregation_id      | None                                 |
| switch_port_link_aggregation_support | None                                 |
| switch_port_management_vlan_id       | None                                 |
| switch_port_mau_type                 | None                                 |
| switch_port_mtu                      | None                                 |
| switch_port_physical_capabilities    | None                                 |
| switch_port_protocol_vlan_enabled    | None                                 |
| switch_port_protocol_vlan_ids        | None                                 |
| switch_port_protocol_vlan_support    | None                                 |
| switch_port_untagged_vlan_id         | None                                 |
| switch_port_vlan_ids                 | []                                   |
| switch_port_vlans                    | None                                 |
| switch_protocol_identities           | None                                 |
| switch_system_name                   | None                                 |
+--------------------------------------+--------------------------------------+


```


## Vérifier les érseaux déployés

```bash
openstack network list
+--------------------------------------+--------------+--------------------------------------+
| ID                                   | Name         | Subnets                              |
+--------------------------------------+--------------+--------------------------------------+
| 48f1b1cb-7be3-4968-b73e-63ebc102c106 | ctlplane     | ad5ee3e2-cabc-41ab-81e7-39c07808df6b |
| 4e8a1173-413f-4ed0-a512-e788695a3217 | external     | bcc709d9-fe9c-44c5-9629-735d7eadb5ba |
| 60457db1-37e9-4590-8964-8739ebc4b884 | internal_api | f8a81628-cdc7-4bc1-bf58-cfeb576551d4 |
| 62c924f8-089d-431a-b9ee-2aa87050f1dc | storage_mgmt | 1f757dbc-09da-421d-8366-0dbbcbfb2d8d |
| 7332a9d1-4916-4ef1-8fd2-91d6dd1c4d38 | storage      | 6bed7e90-e38d-480f-a90f-a08282156357 |
| f019e2b2-42fe-4930-b0a8-6f2edd8cb4bd | tenant       | d0eeb15d-f5e0-4dfa-9f61-bad4ebed8355 |
+--------------------------------------+--------------+--------------------------------------+
(undercloud) [stack@rh-director ~]$ openstack subnet list
+--------------------------------------+---------------------+--------------------------------------+------------------+
| ID                                   | Name                | Network                              | Subnet           |
+--------------------------------------+---------------------+--------------------------------------+------------------+
| 1f757dbc-09da-421d-8366-0dbbcbfb2d8d | storage_mgmt_subnet | 62c924f8-089d-431a-b9ee-2aa87050f1dc | 192.168.177.0/24 |
| 6bed7e90-e38d-480f-a90f-a08282156357 | storage_subnet      | 7332a9d1-4916-4ef1-8fd2-91d6dd1c4d38 | 192.168.176.0/24 |
| ad5ee3e2-cabc-41ab-81e7-39c07808df6b | ctlplane-subnet     | 48f1b1cb-7be3-4968-b73e-63ebc102c106 | 10.129.173.0/24  |
| bcc709d9-fe9c-44c5-9629-735d7eadb5ba | external_subnet     | 4e8a1173-413f-4ed0-a512-e788695a3217 | 10.129.172.0/24  |
| d0eeb15d-f5e0-4dfa-9f61-bad4ebed8355 | tenant_subnet       | f019e2b2-42fe-4930-b0a8-6f2edd8cb4bd | 192.168.174.0/24 |
| f8a81628-cdc7-4bc1-bf58-cfeb576551d4 | internal_api_subnet | 60457db1-37e9-4590-8964-8739ebc4b884 | 192.168.175.0/24 |
+--------------------------------------+---------------------+--------------------------------------+------------------+


```