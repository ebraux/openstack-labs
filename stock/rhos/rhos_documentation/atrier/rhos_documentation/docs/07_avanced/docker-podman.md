


podman login  https://registry.redhat.io
podman pull registry.redhat.io/rhosp-rhel8/openstack-cron:16.1


podman info


https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index


docker pull registry.redhat.io/openshift4/ose-prometheus-alertmanager:4.1
docker tag registry.redhat.io/openshift4/ose-prometheus-alertmanager:4.1 10.129.172.22:5000/openshift4/ose-prometheus-alertmanager:4.1
docker push 10.129.172.22:5000/openshift4/ose-prometheus-alertmanager:4.1
```

Vérification
``` bash
curl http://10.129.172.22:5000/v2/_catalog | jq .repositories[]
```

curl -s http://10.129.172.22:5000/v2/rhceph/rhceph-4-rhel8/tags/list | jq .tags
[
  "4-32",
  "latest"
]


skopeo inspect --tls-verify=false docker://10.129.172.22:5000/rhceph/rhceph-4-rhel8:latest
{latest
    "Name": "10.129.173.1:8787/rhosp13/openstack-keystone",
...
```


https://github.com/david-hill/cloud/blob/master/ops/list-docker-local-registry.sh
```bash
dockerurl=192.0.2.1:8787

function gettags {
   tags=$( curl -s ${dockerurl}/v2/$p/tags/list )
   echo $tags
}


catalog=$( curl -s $dockerurl/v2/_catalog )

for p in $(echo $catalog | sed -e 's/,/\n/g' | sed -e 's/repositories//' -e 's/"//g' -e 's/{:\[//g' -e 's/\]}//g' ); do                                                                    
  gettags
done


```