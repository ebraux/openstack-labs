

```bash
$ swift list
__cache__
overcloud
overcloud-config
overcloud-messages
overcloud-swift-rings
```

Créer un fichier
cd tmp
  591  vim ci/environments/multinode-3nodes-registry.yaml
  592  swift upload overcloud ci/environments/multinode-3nodes-registry.yaml




Swift stores object in containers, this is part of swift man page:
```bash
       list [command-options] [container]
           Lists the containers for the account or the objects for a container.  The -p <prefix> or --prefix <prefix> is an  option
           that  will  only  list  items beginning with that prefix. The -d <delim> or --delimiter <delim> is option (for container
           listings only) that will roll up items with the given delimiter (see OpenStack Swift general documentation for what this
           means).
```

Inside each container there are one or more files:
```bash
$ swift list overcloud
all-nodes-validation.yaml
bootstrap-config.yaml
capabilities-map.yaml
ci/README.rst
ci/common/all-nodes-validation-disabled.yaml
ci/common/net-config-multinode-os-net-config.yaml
...
...
```

You just need to delete the containers with swift delete, for example:
```bash
$ swift delete overcloud
$ swift delete overcloud-config
...
...
```

