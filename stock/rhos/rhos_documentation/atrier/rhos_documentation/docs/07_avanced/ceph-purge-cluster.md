Title: Purge du cluster CEPH

#### Avec le script ansible

```bash
cd /usr/share/ceph-ansible
ansible-playbook -vv \
  -i inventory/hosts \
  infrastructure-playbooks/purge-container-cluster.yml
```

#### A la main
 Sur cahaque nnoeud CEP

```bash
rm -f /etc/systemd/system/ceph* /etc/systemd/system/node_exporter*
rm -f /etc/systemd/system/multi-user.target.wants/ceph* /etc/systemd/system/multi-user.target.wants/node_exporter*
reboot

rm -rf /var/lib/ceph/
rm -rf /etc/ceph
rm -rf /var/run/ceph
rm -rf /var/log/ceph


/opt/MegaRAID/perccli/perccli64 /c0/vall show
/opt/MegaRAID/perccli/perccli64 /c0/v1 del
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off

docker images
# --> supprimer les images
 docker image rm 
docker system prune -f


vgdisplay
  ceph-xxxxx

vgremove ceph-xxxxx


```

Sur le noeud d emetric

```bash
systemctl stop grafana-server.service node_exporter.service prometheus.service alertmanager.service node_exporter.service
systemctl disable  grafana-server.service node_exporter.service prometheus.service alertmanager.service
reboot

rm -rf /etc/alertmanager
rm -rf /var/lib/alertmanager
rm -rf /etc/prometheus
rm -rf /var/lib/prometheus


```