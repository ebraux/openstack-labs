

openstack baremetal list

os-ctrl-01
openstack baremetal introspection interface list aebce0ac-be39-4355-aafc-4fd3bb462ccd
+-----------+-------------------+----------------------+-------------------+----------------+
| Interface | MAC Address       | Switch Port VLAN IDs | Switch Chassis ID | Switch Port ID |
+-----------+-------------------+----------------------+-------------------+----------------+
| eno3      | c8:1f:66:ce:5b:9a | []                   | None              | None           |
| eno1      | c8:1f:66:ce:5b:98 | []                   | None              | None           |
| eno4      | c8:1f:66:ce:5b:9b | []                   | None              | None           |
| eno2      | c8:1f:66:ce:5b:99 | []                   | None              | None           |
+-----------+-------------------+----------------------+-------------------+----------------+
