



déploiement depuis la machine ansible.


## A faire sur chacun des noeuds

### Gestion des souscriptions sur les machines CEPH

Vérifier le "statut : Abonné" pour le souscriptions :

```bash
subscription-manager list
...
Nom du produit :    Red Hat Ceph Storage
Version :           4.0
...
Nom du produit :    Red Hat Ceph Storage MON
Version :           4.0
```

Sinon gére les souscription pour le système
```bash
subscription-manager attach --pool=8a85f99b72fa63f90173069fc5ef1665
```



### Getsion de la registry 

Déclaration de la registry 10.129.172.22:5000 comme registrie insecure /etc/containers/registries.conf

```bash

mkdir /etc/containers

sudo tee /etc/containers/registries.conf > /dev/null <<EOT
[registries.search]
registries = ['registry.access.redhat.com', 'registry.redhat.io', 'docker.io','10.129.172.5:8787', '10.129.172.3:8787','10.129.172.2:13787']

[registries.insecure]
registries = ['10.129.172.3:8787' ]

[registries.block]
registries = ['*']

EOT

cat /etc/containers/registries.conf
```

### configuration du proxy pour Docker

```bash
mkdir -p /etc/systemd/system/docker.service.d

cat > /etc/systemd/system/docker.service.d/http-proxy.conf << EOF
[Service]
Environment="HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="NO_PROXY=127.0.0.1,localhost,10.129.172.21,10.129.172.22,10.129.173.5,10.129.172.31,10.129.172.32,10.129.172.33"
EOF

systemctl daemon-reload
systemctl restart docker
```

### Chargement des images

Depuis une machine avec Docker installé :
```bash
docker login -u DISI1 -p DiSi2003 registry.redhat.io

docker pull registry.redhat.io/rhceph/rhceph-4-rhel8:latest
docker tag registry.redhat.io/rhceph/rhceph-4-rhel8:latest 10.129.172.22:5000/rhceph/rhceph-4-rhel8:latest
docker push 10.129.172.22:5000/rhceph/rhceph-4-rhel8:latest

docker pull registry.redhat.io/openshift4/ose-prometheus-node-exporter:v4.1
docker tag registry.redhat.io/openshift4/ose-prometheus-node-exporter:v4.1 10.129.172.22:5000/openshift4/ose-prometheus-node-exporter:v4.1
docker push 10.129.172.22:5000/openshift4/ose-prometheus-node-exporter:v4.1

docker pull registry.redhat.io/openshift4/ose-prometheus:4.1
docker tag registry.redhat.io/openshift4/ose-prometheus:4.1 10.129.172.22:5000/openshift4/ose-prometheus:4.1
docker push 10.129.172.22:5000/openshift4/ose-prometheus:4.1

docker pull registry.redhat.io/openshift4/ose-prometheus-alertmanager:4.1
docker tag registry.redhat.io/openshift4/ose-prometheus-alertmanager:4.1 10.129.172.22:5000/openshift4/ose-prometheus-alertmanager:4.1
docker push 10.129.172.22:5000/openshift4/ose-prometheus-alertmanager:4.1

docker pull registry.redhat.io/rhceph/rhceph-4-dashboard-rhel8:latest
docker tag registry.redhat.io/rhceph/rhceph-4-dashboard-rhel8:latest 10.129.172.22:5000/rhceph/rhceph-4-dashboard-rhel8:latest
docker push 10.129.172.22:5000/rhceph/rhceph-4-dashboard-rhel8:latest

```

Vérification
``` bash
curl http://10.129.172.22:5000/v2/_catalog | jq .repositories[]
```

curl -s http://10.129.172.22:5000/v2/rhceph/rhceph-4-rhel8/tags/list | jq .tags
[
  "4-32",
  "latest"
]


skopeo inspect --tls-verify=false docker://10.129.172.22:5000/rhceph/rhceph-4-rhel8:latest
{latest
    "Name": "10.129.173.1:8787/rhosp13/openstack-keystone",
...
```
## Installation de ceph-ansible

```bash

sudo subscription-manager repos \
  --enable=rhceph-4-tools-for-rhel-8-x86_64-rpms

sudo dnf upgrade -y && sudo dnf update -y

sudo dnf install -y ceph-ansible
```

## Configuration

``` bash
cd /usr/share/ceph-ansible/group_vars
cp -p all.yml all.yml.DIST
```

Créer le fichier /usr/share/ceph-ansible/group_vars/all.yml
``` yaml
---
fetch_directory: /usr/share/ceph-ansible/ceph-ansible-keys

redhat_package_dependencies: []
upgrade_ceph_packages: False
ceph_test: false

ip_version: ipv4
public_network: 192.168.176.0/24
cluster_network: 192.168.177.0/24
monitor_interface: br-storage
monitor_address_block: 192.168.176.0/24
mon_use_fqdn: false
radosgw_interface: br-storage
radosgw_address_block: 192.168.176.0/24
configure_firewall: false

ceph_rhcs_version: 4
ceph_origin: repository
ceph_repository: rhcs
ceph_repository_type: cdn
containerized_deployment: true
ceph_docker_registry: 10.129.172.22:5000
ceph_docker_registry_auth: false
#ceph_docker_registry_username: <service-account-user-name>
##ceph_docker_registry_password: <token>
ceph_docker_image: rhceph/rhceph-4-rhel8
ceph_docker_image_tag: latest
docker_pull_timeout: 600s

dashboard_enabled: true
dashboard_admin_user: admin
dashboard_admin_password: CEPHstack20!

prometheus_container_image: 10.129.172.22:5000/openshift4/ose-prometheus:4.1
node_exporter_container_image: 10.129.172.22:5000/openshift4/ose-prometheus-node-exporter:v4.1
alertmanager_container_image: 10.129.172.22:5000/openshift4/ose-prometheus-alertmanager:4.1

grafana_admin_user: admin
grafana_admin_password: CEPHstack20!
grafana_container_image: 10.129.172.22:5000/rhceph/rhceph-4-dashboard-rhel8
grafana_server_addr: 10.129.172.30

#ceph_keyring_permissions: '0600'
#cephx: true
```
## Préparation du déploiement

### Création du fichier de hosts

Création du fichier
```bash
cd  /usr/share/ceph-ansible

mkdir -p inventory
touch inventory/hosts
```

```yaml
all:
  children:
    grafana-server:
      hosts:
        10.129.172.30: null
    mdss:
      hosts:
        10.129.172.31: null
        10.129.172.32: null
        10.129.172.33: null
    mgrs:
      hosts:
        10.129.172.31: null
        10.129.172.32: null
        10.129.172.33: null
    mons:
      hosts:
        10.129.172.31: null
        10.129.172.32: null
        10.129.172.33: null
    osds:
      hosts:
        10.129.172.31: null
        10.129.172.32: null
        10.129.172.33: null
    rgws:
      hosts:
        10.129.172.31: null
        10.129.172.32: null
        10.129.172.33: null
```

### Modif fichier de conf : /usr/share/ceph-ansible/ansible.cfg

```ini
[defaults]
+ inventory = ./inventory/hosts # Assign a default inventory directory
```

### 
```bash
cp -p site-container.yml.sample site-container.yml
```

## Déploiement

``` bash
su - ansible-user
mkdir /home/ansible-user/ansible/

cd /usr/share/ceph-ansible/
ansible-playbook site-container.yml -i inventory/hosts

```

## Post-config

### Activer la suppression

* https://access.redhat.com/solutions/3300281



```bash
docker exec ceph-mon-ceph-poc-01 ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config show | grep mon_allow_pool_delete
docker exec ceph-mon-ceph-poc-01  ceph --admin-daemon /var/run/ceph/ceph-mon.`hostname -s`.asok config set mon_allow_pool_delete true
```

### Test

Test du stockage : From a Ceph Monitor node
```bash
docker exec ceph-mon-ceph-poc-01 ceph osd pool create test 8

docker exec ceph-mon-ceph-poc-01 touch /tmp/hello-world.txt
docker exec ceph-mon-ceph-poc-01 rados --pool test put hello-world /tmp/hello-world.txt

docker exec ceph-mon-ceph-poc-01 rados --pool test get hello-world /tmp/fetch.txt
docker exec ceph-mon-ceph-poc-01 cat /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rm -f /tmp/hello-world.txt
docker exec ceph-mon-ceph-poc-01 rm -f /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rados --pool test rm  hello-world

docker exec ceph-mon-ceph-poc-01 ceph osd pool rm  test test --yes-i-really-really-mean-it
```

## Configuration pour Openstack


* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index
* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/12/html-single/integrating_an_overcloud_with_an_existing_red_hat_ceph_cluster/index


Create the pools 
```bash
# By default, Ceph block devices use the rbd pool.
docker exec ceph-mon-ceph-poc-01 ceph osd pool create volumes 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create images 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create vms 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create backups 16
docker exec ceph-mon-ceph-poc-01 ceph osd pool create metrics 16
```


Create a client.openstack
```bash
docker exec -it ceph-mon-ceph-poc-01 bash

ceph auth add client.openstack mgr 'allow *' mon 'profile rbd' osd 'profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups, profile rbd pool=metrics'

ceph osd pool ls

ceph auth list
  client.openstack
	  key: AQCGn2pfxq3rEBAAGqDOvhx53kdsD+pMNyBp8A==
	  caps: [mgr] allow *
	  caps: [mon] profile rbd
	  caps: [osd] profile rbd pool=volumes, profile rbd pool=vms, profile rbd pool=images, profile rbd pool=backups, profile rbd pool=metrics
  
cat /etc/ceph/ceph.conf | grep fsid
fsid = 32566426-c8c1-4845-9381-79d74ee658a7

```

* Ceph client key  : AQDSCSZfB0LOEhAA+5Pi3dOUpIZrQV1vgljkXQ==
* file system ID  (fsid) : 45853161-a453-492a-87bb-eeb7322bbdc7






