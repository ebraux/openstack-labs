

### Definition des rôles 

```bash
cp /usr/share/openstack-tripleo-heat-templates/roles_data.yaml /home/stack/templates/roles_data.yaml
```
Tripleo génère ce fichier avec les roles : Controller, Compute, BlockStorage, ObjectStorage et CephStorage.

Supprimer / ajouter des rôles si besoin. Avec CEPH en externe, on a besoin uniquement de "Controller" et "Compute". Mais on risque d'avoir besoin de défini plusiuers type de compute.
