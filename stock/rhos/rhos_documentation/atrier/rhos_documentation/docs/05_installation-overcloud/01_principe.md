fichier d'environnement : https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/creating-a-basic-overcloud-with-cli-tools#environment-files


### Principe

On va générer des fichier de configuration avec Heat (et un peu de puppet). Le déploiement est ensuite géré avec Ansible.

On créer quelques fichiers de base, décrivant l'architecture : le nombre d enoeuds, le type de noeud, la config réseau.

Ensuite, on ajuste la configuration, ou on ajoute de sfonctionnalités en intégrant des fichier de ressource pour heat. Dans ce fichier, on ajoute des variable, qui vont impacter le déploiement.

La bonne pratique consite à ne pas modifier les fichiers de ressource complémentaires fournis avec la distribution, mais à ajouter des fichiers de customisation, qui vont venir surcharger les variables. Par exemple, pour l'intégration avec CEPH, on va intégrer le fichier général  "/usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml", et on va surcharger des infos spécifiques, comme les données d'authentification sur le cluster, dans un fichier à part "/home/stack/templates/ceph-config.yaml".

On regroupe ces fichiers dans le dossier /home/stack/templates.

Certains fichiers 'environnement sont générée au lancement du déployement à partir de fichier jinja2.  Pour pouvoir voir le rendu final de ces fichiers, et savoir quelles variables doivent être ajustée, on fait une génération des fichiers à partir des templates.

Pour inclure les fichiers d'environnement, on peut utiliser l'option sur la ligne de commande, qui permet d'inclure un fichier d'environnement. mais le plus simpl consiste à créer un fichier yaml (answers.yaml), et de configurer les fichier d'environnementdans ce fichier. 3 avantges : lea ligne de commande est plus courte, c'ets gérable en version, et on peut mettre des commentaires.


[TODO] Les fichiers interressants sont les fichiers "data" :

```bash
 find . -name "*_data*"
./roles_data_undercloud.yaml
./roles_data.yaml
./network_data_ganesha.yaml
./network_data.yaml
./network/endpoints/endpoint_data.yaml
```

* network_data.yaml  :la configuartion réseau
* roles_data.yaml :  les composants à déployer
* roles_data_undercloud.yaml : les composants à déployer sur l'undercloud

