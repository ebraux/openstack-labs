

## Validation de la configuration des serveurs

Tester que IPMI est bien actvé sur un noeud distant

```bash
sudo dnf install -y ipmitool
```

``` bash
ipmitool -I lanplus -H 10.29.20.32 -U root -P ospoc2020 sdr elist all
```
* https://techexpert.tips/fr/dell-idrac-fr/ipmi-sur-linterface-idrac/
* https://osric.com/chris/accidental-developer/2017/10/using-ipmitool-to-configure-dell-idrac/



## création du fichier d'inventory 

``` bash
sudo su - stack
source ~/stackrc
```

Création du fichier listant les hotes :  ~/nodes.json

Exemple  :
```json
{
    "nodes":[
        {
            "mac":[
                "c8:1f:66:ce:5b:98"
            ],
            "name":"os-poc-ctrl-01",
            "pm_type":"ipmi",
            "pm_addr":"10.29.20.31",
            "pm_user":"root",
            "pm_password":"ospoc2020",
            "arch":"x86_64"
        },
		{
            "mac":[
                "18:66:da:ec:4b:3c"
            ],
            "name":"os-poc-comp-01",
            "pm_type":"ipmi",
            "pm_addr":"10.29.20.41",
            "pm_user":"root",
            "pm_password":"ospoc2020",
            "arch":"x86_64"
        }
	]
}
```


Validation du fichier
``` bash
openstack overcloud node import --validate-only ~/nodes.json
```

## Enregistrement des noeuds

```bash
openstack overcloud node import ~/nodes.json

openstack baremetal node list 
```

## Introspection des noeuds

### processus

L'introspection dure assez longtemps, et peut êter lancée en parallèle sur plusieurs noeuds. Mais en cas d'erreur, sur un noeud, on peut lancer l'introspection sur un noeud spécifique

POur suivre le déroulement d el'introspection, ouvrir un 2eme terminal, pour afficher les logs :
```bash
sudo tail -f /var/log/containers/ironic-inspector/ironic-inspector.log
```

Avant de lancer l'introspection sur un neoud, on peut tester la comptibilité (mais bon, c'est purement informatif)
```bash
openstack tripleo validator run --group pre-introspection
```

### Lancement de l'introspection

Pour lancer l'introspection de tous les noeuds
```bash
openstack overcloud node introspect --all-manageable --provide
```

Pour lancer l'introspection sur un noeud spécifique
```bash
openstack overcloud node introspect ${nodeUUID} --provide
```

Affichage de données récoltées
```bash
openstack baremetal introspection data save ${nodeUUID}| jq .
```

### Gestion des disques de boot

Pour chque noeud, il faut préciser quel est l'ID du disque de boot (même si il n'y en a qu'un)

Récupération du numéro de série
```bash
openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"
[
  {
    "size": 146163105792,
    "serial": "6b083fe0d31ab400241515dd06792e14",
    "wwn": "0x6b083fe0d31ab400",
    "rotational": true,
    "vendor": "DELL",
    "name": "/dev/sda",
    "wwn_vendor_extension": "0x241515dd06792e14",
    "hctl": "0:2:0:0",
    "wwn_with_extension": "0x6b083fe0d31ab400241515dd06792e14",
    "by_path": "/dev/disk/by-path/pci-0000:03:00.0-scsi-0:2:0:0",
    "model": "PERC H710"
  }
]
```

Configuration
```bash
openstack baremetal node set --property root_device='{"serial": "6b083fe0d31ab400241515dd06792e14"}' ${nodeUUID}
```

## Exemple de Séquence pour intégrer un noeud spécifique


```bash

openstack baremetal node list
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| aebce0ac-be39-4355-aafc-4fd3bb462ccd | os-poc-ctrl-01 | None          | power off   | manageable         | False       |
| 9a558884-cff0-4784-a328-939f600bc72e | os-poc-ctrl-02 | None          | power off   | manageable         | False       |
| 2974b203-9f00-4b20-9602-23b906aec304 | os-poc-ctrl-03 | None          | power off   | manageable         | False       |
| fef24917-0be8-4d54-8e59-c5ba84026b19 | os-poc-comp-01 | None          | power off   | manageable         | False       |
| 9ebf828e-1b7a-482b-a00c-abeed9fb2d64 | os-poc-comp-02 | None          | power off   | manageable         | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+


| NOM | UUID | boot disk serial |
| ---- | ---- | ---- |
| os-poc-ctrl-01 | aebce0ac-be39-4355-aafc-4fd3bb462ccd | 6c81f660d1d33400216f34ae06a7bbe5 |
| os-poc-ctrl-02 | 9a558884-cff0-4784-a328-939f600bc72e | 6b083fe0d31ab400241515dd06792e14 |
| os-poc-ctrl-03 | 2974b203-9f00-4b20-9602-23b906aec304 | 6b083fe0d32fd5002415247d05a01c68 |
| os-poc-comp-01 | fef24917-0be8-4d54-8e59-c5ba84026b19 | 6847beb0d517f70024114fdb0545859e |
| os-poc-comp-02 | 9ebf828e-1b7a-482b-a00c-abeed9fb2d64 | 6d0946601a1ce7002247c31c1fe3d822 |

export nodeUUID=fef24917-0be8-4d54-8e59-c5ba84026b19

openstack overcloud node introspect ${nodeUUID} --provide

openstack baremetal introspection data save ${nodeUUID} | jq ".inventory.disks"

openstack baremetal node set --property root_device='{"serial": "6847beb0d517f70024114fdb0545859e"}' ${nodeUUID}

openstack baremetal node show -f json -c properties ${nodeUUID}
```

``` bash
openstack baremetal node list

openstack overcloud profiles list
```


## autres

```bash
#openstack overcloud node introspect --run-validations ${nodeUUID}
#openstack baremetal introspection data save ${nodeUUID}| jq .
```

# Préparation

Vérifier le "statut : Abonné" pour la souscriptions :

```bash
subscription-manager list

sudo subscription-manager list
  ...
 Product Name:   Red Hat OpenStack
 Version:        16.1
 ...
 Product Name:   Red Hat Ceph Storage
 Version:        4.0
 ...
```

Vérifier que l'undercloud est configuré pour déployer les noeuds
```bash
openstack flavor  list
+--------------------------------------+---------------+------+------+-----------+-------+-----------+
| ID                                   | Name          |  RAM | Disk | Ephemeral | VCPUs | Is Public |
+--------------------------------------+---------------+------+------+-----------+-------+-----------+
| 141da1a8-4f0d-46e6-8cfa-72bf7d3efa31 | baremetal     | 4096 |   40 |         0 |     1 | True      |
| 367a00c8-9a5c-4acf-bec5-48c4897779d1 | ceph-storage  | 4096 |   40 |         0 |     1 | True      |
| 701f7ac5-83a1-42df-a0fc-653a9f8fd699 | swift-storage | 4096 |   40 |         0 |     1 | True      |
| ab7a0fcd-8207-42c5-a296-74a8ce5df7a9 | block-storage | 4096 |   40 |         0 |     1 | True      |
| ccfb7e5a-981d-4f1e-91e9-a19b0926d467 | compute       | 4096 |   40 |         0 |     1 | True      |
| db8b8372-cb95-4378-bb1f-ecd1b058f806 | control       | 4096 |   40 |         0 |     1 | True      |
+--------------------------------------+---------------+------+------+-----------+-------+-----------+

# Il faut des Flavor, avec les "name" "control" et "compute".
```


## affectation des roles

Lister les serveurs 
```bash
openstack baremetal node list
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
| aebce0ac-be39-4355-aafc-4fd3bb462ccd | os-poc-ctrl-01 | None          | power off   | available          | False       |
| 9a558884-cff0-4784-a328-939f600bc72e | os-poc-ctrl-02 | None          | power off   | manageable         | False       |
| 2974b203-9f00-4b20-9602-23b906aec304 | os-poc-ctrl-03 | None          | power off   | manageable         | False       |
| fef24917-0be8-4d54-8e59-c5ba84026b19 | os-poc-comp-01 | None          | power off   | available          | False       |
| 9ebf828e-1b7a-482b-a00c-abeed9fb2d64 | os-poc-comp-02 | None          | power off   | manageable         | False       |
+--------------------------------------+----------------+---------------+-------------+--------------------+-------------+
# seuls les baremetal node dont "Provisioning State" est "available" pourront être utilisés.
# pour passer "manageable" à "available", il faut fair el'introspection du node (voir précédement).
```

Vérifier les serveurs disponibles pour le déploiement
```bash
openstack overcloud profiles list
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| Node UUID                            | Node Name      | Provision State | Current Profile | Possible Profiles |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| aebce0ac-be39-4355-aafc-4fd3bb462ccd | os-poc-ctrl-01 | available       | None            |                   |
| fef24917-0be8-4d54-8e59-c5ba84026b19 | os-poc-comp-01 | available       | None            |                   |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
# Il faut des "Node", avec les bon "Profile" et qui soient "available"
```

Leur affecter des rôle, par exemple
```bash
openstack baremetal node set --property capabilities='profile:control,boot_option:local' aebce0ac-be39-4355-aafc-4fd3bb462ccd
openstack baremetal node set --property capabilities='profile:compute,boot_option:local' fef24917-0be8-4d54-8e59-c5ba84026b19

openstack overcloud profiles list
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| Node UUID                            | Node Name      | Provision State | Current Profile | Possible Profiles |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
| aebce0ac-be39-4355-aafc-4fd3bb462ccd | os-poc-ctrl-01 | available       | control         |                   |
| fef24917-0be8-4d54-8e59-c5ba84026b19 | os-poc-comp-01 | available       | compute         |                   |
+--------------------------------------+----------------+-----------------+-----------------+-------------------+
```
