
### Integration de ceph
Reprendre les données de la rubrique "parameter_defaults" du fichier 
/home/stack/templates/ceph-config.yaml
```bash
parameter_defaults:
  CephClusterFSID: '32566426-c8c1-4845-9381-79d74ee658a7'
  CephClientKey: 'AQCGn2pfxq3rEBAAGqDOvhx53kdsD+pMNyBp8A=='
  CephExternalMonHost: '192.168.176.31, 192.168.176.32, 192.168.176.33'

  # the following parameters enable Ceph backends for Cinder, Glance, Gnocchi and Nova
  NovaEnableRbdBackend: true
  CinderEnableRbdBackend: true
  CinderBackupBackend: ceph
  GlanceBackend: rbd
  GnocchiBackend: rbd
  NovaRbdPoolName: vms
  CinderRbdPoolName: volumes
  CinderBackupRbdPoolName: backups
  GlanceRbdPoolName: images
  GnocchiRbdPoolName: metrics
  CephClientUserName: openstack

  # finally we disable the Cinder LVM backend
  CinderEnableIscsiBackend: false

```

