








### Fichier de customisation answers.yaml


* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/10/html/director_installation_and_usage/chap-configuring_basic_overcloud_requirements_with_the_cli_tools


/home/stack/templates/answers.yaml
```yaml
templates: /usr/share/openstack-tripleo-heat-templates/
environments:
  - /home/stack/containers-prepare-parameter.yaml
  # rhsm-registration
  #  - 
  - /home/stack/templates/rhsm.yaml
  # network configuration
  - /usr/share/openstack-tripleo-heat-templates/environments/network-environment.yaml
  - /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml
  - /usr/share/openstack-tripleo-heat-templates/environments/net-single-nic-with-vlans.yaml
  # CEPH integration
  - /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible-external.yaml
  - /home/stack/templates/ceph-config.yaml
  # 
  - /home/stack/templates/node-info.yaml

```



### Commande à lancer 

```bash
su - stack
source ~/stachrc

openstack overcloud deploy  \
  --timeout 240 \
  --verbose \
  -r /home/stack/templates/roles_data.yaml \
  -n /home/stack/templates/network_data.yaml \
  --answers-file /home/stack/templates/answers.yaml \
  --log-file overcloudDeploy.log \
  --ntp-server ntp1.svc.enst-bretagne.fr

#--debug \

```

L'option '-r' permet une config de role
L'option '-n' permet une config réseau (network file)
L'option  '-e' permet d'incorporer des fichier d'environnement (environement file)


On peut retrouver des infos sur les erreurs :

```bash
openstack stack failures list overcloud --long

openstack workflow execution list | grep "ERROR"

```
Sinon, on retrouve des log dans /var/log/mistral. ???

On peut activer des logs en ajoutant

```bash
  --log-file overcloudDeploy.log \
  --debug \
```


Autres options 


```bash
--verbose 
--force-postconfig
--stack-only

--stack overcloud \
--libvirt-type kvm \
--overcloud-ssh-user heat-admin \

--config-download \
-e /usr/share/openstack-tripleo-heat-templates/environments/config-download-environment.yaml \

-e /home/stack/virt/enable-tls.yaml \
-e /home/stack/virt/public_vip.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ssl/tls-endpoints-public-ip.yaml \

-e /home/stack/virt/debug.yaml \
```




