## Initialistion de l'environnement



### Definition de l'architecture, et des gabarits (node counts and flavors)

/home/stack/templates/node-info.yaml
```bash
parameter_defaults:
  OvercloudControllerFlavor: control
  OvercloudComputeFlavor: compute
  ControllerCount: 1
  ComputeCount: 1
```

Celui en v13
```bash
parameter_defaults:
  OvercloudControllerFlavor: 'control'
  OvercloudComputeFlavor: 'compute'
  ControllerCount: 1
  ComputeCount: 1
  ControlPlaneSubnetCidr: '24'
  ControlPlaneDefaultRoute: '10.129.172.1'
  ManagementInterfaceDefaultRoute: '10.129.172.1'
  EC2MetadataIp: '10.129.172.5'
```

### Définition des images à utiliser

The container image preparation environment file. You generated this file during the undercloud installation and can use the same file for your overcloud creation.


### Definion des sources des images 

"containers-prepare-parameter.yaml"
The container image preparation environment file. You generated this file during the undercloud installation and can use the same file for your overcloud creation.

### Génération des fichiers de templates de réference pour la customisation

```bash
rm -rf /home/stack/tripleo-heat-templates/
cd /usr/share/openstack-tripleo-heat-templates
./tools/process-templates.py -o ~/openstack-tripleo-heat-templates-rendered -n /home/stack/templates/network_data.yaml -r /home/stack/templates/roles_data.yaml
```
