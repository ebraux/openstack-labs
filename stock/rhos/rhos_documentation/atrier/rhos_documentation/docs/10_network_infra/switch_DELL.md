

se connecter

au démarrage login .
login admin, mot de passe admin.


docs :

https://topics-cdn.dell.com/pdf/force10-s4048-on_reference-guide7_en-us.pdf
doc vxrail : https://infohub.delltechnologies.com/section-assets/vxrail-networking-guide-with-dell-emc-s4148-on-switches
doc profile : https://infohub.delltechnologies.com/l/dell-emc-networking-virtualization-overlay-with-bgp-evpn-10/configure-switch-port-profile-s4148u-on-only-6


## Connexion

Connect to the serial console port. The serial settings are 115200 baud, 8 data bits, and no parity

## configuration


Le systéme de gestion des switch DELL est Onie ()
Les switch DELL ont déjà un OS10 préinstallé.

"OS10 Enterprise Edition may come factory-loaded and is available for download from the Dell Digital Locker (DDL). A factory-loaded OS10
image has a perpetual license installed. An OS10 image that you download has a 120-day trial license and requires a perpetual license to run
beyond the trial period. See the Quick Start Guide shipped with your device and My Account FAQs for more information."

Tant que le systéme n'est pas configuré, on a un shell avec le prompt "ONIE".

Une fois configuré, on a un shell avec le prompt "OS10#"


On peut défnir des switch port profile (voir doc vxrail, page 16)

Pour se connecter la première fois :
```bash
OS10 login: admin
Password: admin
```


### getsion de a conf :


* candidate-configuration
* running-configuration

```bash
show diff candidate-configuration running-configuration
``` 


## Vérification, infos sur le switch

```bash
OS10# show license status
  
System Information
---------------------------------------------------------
Vendor Name          :   Dell EMC
Product Name         :   S4148F-ON
Hardware Version     :   A05
Platform Name        :   x86_64-dellemc_s4148f_c2338-r0
PPID                 :   CN0R2RKC282980550002
Service Tag          :   9FMFPK2
Product Base         :   
Product Serial Number:   
Product Part Number  :   
License Details
----------------
Software        :        OS10-Enterprise
Version         :        10.4.3.4
License Type    :        PERPETUAL
License Duration:        Unlimited
License Status  :        Active
License location:        /mnt/license/9FMFPK2.lic
---------------------------------------------------------
```

``` bash
OS10# show system

Node Id              : 1
MAC                  : 68:4f:64:e9:a3:55
Number of MACs       : 256
Up Time              : 00:29:04

-- Unit 1 --
Status                     : up
System Identifier          : 1
Down Reason                : user-triggered
Digital Optical Monitoring : disable
System Location LED        : off
Required Type              : S4148F
Current Type               : S4148F
Hardware Revision          : A05
Software Version           : 10.4.3.4
Physical Ports             : 48x10GbE, 2x40GbE, 4x100GbE
BIOS                          : 3.33.0.1-7    
System CPLD                   : 1.1           
Master CPLD                   : 1.0           
Slave CPLD                    : 0.7           

-- Power Supplies --
        PSU-ID  Status      Type    AirFlow   Fan  Speed(rpm)  Status
----------------------------------------------------------------
1       up          AC      REVERSE   1    11664       up         
2       fail        

-- Fan Status --
FanTray  Status      AirFlow   Fan  Speed(rpm)  Status
----------------------------------------------------------------
1        up          REVERSE   1    10983       up         
2        up          REVERSE   1    10983       up         
3        up          REVERSE   1    11014       up         
``` 


```bash
show candidate-configuration compressed
!
ip vrf default
!
interface breakout 1/1/25 map 100g-1x
interface breakout 1/1/26 map 100g-1x
interface breakout 1/1/29 map 100g-1x
interface breakout 1/1/30 map 100g-1x
system-user linuxadmin password $6$5DdOHYg5$JCE1vMSmkQOrbh31U74PIPv7lyOgRmba1Ixhk1
iscsi enable
iscsi target port 860
iscsi target port 3260
username admin password $6$q9QBeYjZ$jfxzVqGhkxX3smxJSH9DDz7/3OJc6m5wjF8nnLD7/VKx85
username imta-admin password $6$rounds=656000$JVjU.Cf8oxTTJy9Y$B2w2pqhIbGbGfMKf/55
aaa authentication login default local
aaa authentication login console local
!
class-map type application class-iscsi
!
policy-map type application policy-iscsi
!
interface vlan 1
 no shutdown
!
interface mgmt1/1/1
 no shutdown
 no ip address dhcp
 ip address 10.129.172.201/24
 ipv6 address autoconfig
!
interface range ethernet 1/1/1-1/1/26,1/1/29-1/1/54
 no shutdown
 switchport access vlan 1
 flowcontrol receive on
!
support-assist
snmp-server contact "Contact Support"
```


## configuration

### Configiuration de l'interface de management

```bash
OS10#
OS10# config
OS10(config)# interface mgmt 1/1/1
OS10(conf-if-ma-1/1/1)# no ip address dhcp
OS10(conf-if-ma-1/1/1)# ip address 10.129.172.201/24
OS10(conf-if-ma-1/1/1)# no shutdown
OS10(conf-if-ma-1/1/1)# management route 0.0.0.0/0 10.129.172.1
OS10(conf-if-ma-1/1/1)# exit
OS10(config)# exit
OS10# write memory
OS10# reload

Proceed to reboot the system? [confirm yes/no]:y
```

```bash
OS10# config
OS10(config)# show interface mgmt 1/1/1
Management 1/1/1 is up, line protocol is down
Hardware is Eth, address is 68:4f:64:e9:a3:55
    Current address is 68:4f:64:e9:a3:55
Interface index is 10
Internet address is 10.129.172.201/24
Mode of IPv4 Address Assignment: MANUAL
Interface IPv6 oper status: Enabled
Virtual-IP is not set
Virtual-IP IPv6 address is not set
MTU 1532 bytes, IP MTU 1500 bytes
LineSpeed 0
Flowcontrol rx off tx off
ARP type: ARPA, ARP Timeout: 60
Last clearing of "show interface" counters: 00:02:20
Queuing strategy: fifo
Input statistics:
     Input 0 packets, 0 bytes, 0 multicast
     Received 0 errors, 0 discarded
Output statistics:
     Output 0 packets, 0 bytes, 0 multicast
     Output 0 errors, Output  invalid protocol
Time since last interface status change: 00:01:50

```


### divers :

Nom du switch
```bash
OS10(conifg)# hostname stack10g-01
stack10g-01(config)# 
```

Créatin d'utilisateurs
```bash
OS10(conf-if-ma-1/1/1)# username imta-admin password alpha404! role sysadmin
```

## Zero-touch deployment

Zero-touch deployment (ZTD) allows OS10 users to automate switch deployment:
28 Getting Started
• Upgrade an existing OS10 image.
• Execute a CLI batch file to configure the switch.
• Execute a post-ZTD script to perform additional functions.

```bash
OS10# show ztd-status
  
-----------------------------------
ZTD Status     : disabled
ZTD State      : init
Protocol State : idle
Reason         : 
-----------------------------------
```

Vérification de la licance
```bash
OS10#                                                                           
OS10#                                                                           
OS10# enable                                                                    
OS10# configure terminal  
```