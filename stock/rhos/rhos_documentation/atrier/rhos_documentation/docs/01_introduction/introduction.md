title: Introduction

## Présentation 

RedHat propose une distribution d'Openstack : Red Hat OpenStack Plateform (RHOS).

Cette ditribution comprend le coeur d'Opensatck, et la plupart des composants additionnels stable. Ainsi qu'un ensemble d'outils permettant de gérer l'installation et le cycle de vie de l'infrastructure Openstack.

* [https://www.redhat.com/fr/technologies/linux-platforms/openstack-platform](https://www.redhat.com/fr/technologies/linux-platforms/openstack-platform)


## Getsion des versions de RHOS

Il y a des version Long Time Support, et des version Intermédiaires.

La version actuelle de RHOS est la version 16, qui correspond à la version xxx d'Openstack.

La version déployée est la version 13, qui correspond à la version xxx d'Openstack. 




## Principe

RedHat propose dans sa distribution un ensemble 


Red Hat intégre dans sa distribution RedHat Openstack Plateform un outil permettant de gérer l'installation, .

Il faut tout d'abord installer sur une console d'adminitrastation, appelée director.

A partir de cette console, il faut ensuite configurer un environnement de déploiement.

Cet environnement permet d'effectuer une initialisation minimum des serveurs (bootstrap) qui seront ensuite utilisés pour l'infrastructure Openstack. Il met en place une couche réseau minimum entre ces serveurs. Il permet également de définir pour chaque serveur à quel type d'usage il est destiné (controlleur, compute,...).
C'est la couche base de l'infrastructure déployée, le socle.
Cet environemment est appelé "undercloud".

Enfin une fois que les serveurs ont été initialisés,  il reste à définir l'environemment Openstack à déployer : quelles briques, quelle architecture, ... Une fois le modéle défini, "Director" va le déployer en s'appuyant sur les serveurs qui sont disponibles dans l'undercloud.
Cet environemment est l'infrastructure Openstack fnale, et est appelé "overcloud".

## Director

"Director" est un esemble d'outils permattant d'installer et de gérer un environnement Openstack.

Le déploiement des premières version de RHOS était basé sur foreman/puppet.
Depuis la version 7, le déploiement est principalement basé sur sur le projet Opensource "Tripleo".
Petit à petit puppet est remplacé par Ansible.

## Undercloud

L'undercloud est le composant principal de "Director".

 L'initialisation consiste à  :

 * analyser la machine pour définir ses caractéristique
 * lui affecter un role (controleur, compute, ...)

The undercloud is the main director node. It is a single-system OpenStack installation that includes components for provisioning and managing the OpenStack nodes that form your OpenStack environment (the overcloud).

The primary objectives of undercloud are as below:

* Discover the bare-metal servers on which the deployment of Openstack Platform has been deployed
* Serve as the deployment manager for the software to be deployed on these nodes
* Define complex network topology and configuration for the deployment
* Rollout of software updates and configurations to the deployed nodes
* Reconfigure an existing undercloud deployed environment
* Enable high availability support for the openstack nodes



## Overcloud

The overcloud is the resulting Red Hat OpenStack Platform environment created using the undercloud.
This includes different nodes roles which you define based on the OpenStack Platform environment you aim to create.

## Tripleo

Tripleo est une abréviation de "OpenStack-On-OpenStack".

 Every distribution has their own OpenStack deployment tool. Clearly deployments differ as they are based on support decisions each distribution makes. However many distributions have created their own proprietary installers. Shouldn’t the OpenStack community unite around a common installer? What would be better than using OpenStack to deploy OpenStack? Why should OpenStack administrators have to learn separate proprietary tooling? Why should we be creating unnecessary vendor lock-in for OpenStack’s deployment tooling? Installing OpenStack is one thing but what about upgrade and life-cycle management?

 * TripleO uses OpenStack to deploy OpenStack. It mainly utilizes Ironic for provisioning and Heat for orchestration.
 * Under the hood puppet is used for configuration management. 
 * The networking requirement is that all systems share a non-routed provisioning network.
 * TripleO also uses PXE to boot and install initial OS image (bootstrap). There are different types of nodes or roles a node can have.

 ## pour résumer 
 TripleO first deploys an OpenStack cloud used to deploy other OpenStack clouds. This is referred to as the undercloud. The OpenStack cloud environment deployed from undercloud is known as overcloud. 