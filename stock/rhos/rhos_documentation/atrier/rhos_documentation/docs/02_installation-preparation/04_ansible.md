
## VM rh-ansible

Une seule interface :
 * * ens2, reliée à br-ex, Ip 10.129.127.72.21


```bash
virt-install \
  --name rh-ansible \
  --memory 2048 \
  --vcpus 1 \
  --disk size=20 \
  --location /var/lib/libvirt/images/rhel-8.2-x86_64-dvd.iso \
  --os-variant=rhel8.0  \
  --graphics=none \
  --noautoconsole \
  --console pty,target_type=serial \
  --autostart \
  --network bridge=br-ex \
  --initrd-inject /root/kickstart/rh-ansible.cfg \
  --extra-args="ks=file:/rh-ansible.cfg console=ttyS0 serial"
```

## Enregistrement et installation des packages

Repositories
```bash
sudo subscription-manager repos \
  --enable=rhel-8-for-x86_64-baseos-eus-rpms \
  --enable=rhel-8-for-x86_64-appstream-eus-rpms \
  --enable=ansible-2.9-for-rhel-8-x86_64-rpms 
```

Update et reboot
```bash
dnf clean all && rm -rf /var/cache/dnf  && dnf upgrade -y && dnf update -y
sudo reboot
```

```bash
dnf install -y ansible
```

## création d'un utilisateur dédié

```bash
groupadd -g 2004 ansible-user
useradd -u 2004 -g ansible-user -d /home/ansible-user -c 'dedicated user for ansible' -s /bin/bash ansible-user
#echo "ansible-user ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/ansible-user
#chmod 0440 /etc/sudoers.d/ansible-user
```


```bash
sudo su - ansible-user
ssh-keygen
  /home/ansible-user/.ssh/id_rsa
  /home/ansible-user/.ssh/id_rsa.pub

```




