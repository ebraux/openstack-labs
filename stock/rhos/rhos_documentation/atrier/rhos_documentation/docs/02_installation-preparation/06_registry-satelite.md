title: Les repositories
description: docker Registry, RH Satellite, ... 

## VM rh-registry

Une seule interface :
 * ens2, reliée à br-ex, Ip 10.129.127.72.22

On reste en RHEL7, car en 8, toute la gestion de Docker à changé : "Docker is not included in RHEL 8.0. For working with containers, use the podman, buildah, skopeo, and runc tools."

## Déploiement de la VM

```bash
virt-install \
  --name rh-registry \
  --memory=2048 \
  --vcpus=2 \
  --disk size=40 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --os-variant=rhel7  \
  --graphics=none \
  --noautoconsole \
  --wait=-1 \
  --console pty,target_type=serial \
  --autostart \
  --network bridge=br-ex \
  --initrd-inject /root/kickstart/rh-registry.cfg \
  --extra-args="ks=file:/rh-registry.cfg console=ttyS0 serial"
```

## Enregistrement et installation des packages

Pas de repository complémentaire

## installation de Docker 

```bash
yum install -y docker
systemctl start docker.service
systemctl enable docker.service

yum install -y docker-distribution
systemctl start docker-distribution.service
systemctl enable docker-distribution.service
```

```bash
mkdir -p /etc/systemd/system/docker.service.d

cat > /etc/systemd/system/docker.service.d/http-proxy.conf << EOF
[Service]
Environment="HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="NO_PROXY=127.0.0.1,localhost
EOF

systemctl daemon-reload
systemctl restart docker
```

```bash
systemctl stop dbus-org.fedoraproject.FirewallD1.service
systemctl disable  dbus-org.fedoraproject.FirewallD1.service
```

Installation : 
* https://access.redhat.com/solutions/705273

Securisation :
* https://access.redhat.com/solutions/3175391


## Pour RHEL 8
* https://linuxconfig.org/how-to-install-docker-in-rhel-8
* https://computingforgeeks.com/install-docker-and-docker-compose-on-rhel-8-centos-8/
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index


https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/installing-the-undercloud#undercloud-container-registry