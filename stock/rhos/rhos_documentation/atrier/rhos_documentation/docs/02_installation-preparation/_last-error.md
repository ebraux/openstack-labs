

TASK [AllNodesValidationConfig] ************************************************
task path: /var/lib/mistral/overcloud/deploy_steps_playbook.yaml:190
Wednesday 23 September 2020  20:51:01 +0200 (0:00:00.877)       0:02:42.211 *** 
changed: [overcloud-controller-0] => {"changed": true, "rc": 0, "stderr": "Shared connection to 10.129.173.128 closed.\r\n", "stderr_lines": ["Shared connection to 10.129.173.128 closed."], "stdout": "Trying to ping default gateway 10.129.172.1...Ping to 10.129.172.1 succeeded.\r\nSUCCESS\r\nTrying to ping 10.129.173.128 for local network 10.129.173.0/24.\r\nPing to 10.129.173.128 succeeded.\r\nSUCCESS\r\n", "stdout_lines": ["Trying to ping default gateway 10.129.172.1...Ping to 10.129.172.1 succeeded.", "SUCCESS", "Trying to ping 10.129.173.128 for local network 10.129.173.0/24.", "Ping to 10.129.173.128 succeeded.", "SUCCESS"]}
fatal: [overcloud-novacompute-0]: FAILED! => {"changed": true, "msg": "non-zero return code", "rc": 1, "stderr": "Shared connection to 10.129.173.181 closed.\r\n", "stderr_lines": ["Shared connection to 10.129.173.181 closed."], "stdout": "Trying to ping default gateway 10.129.173.1...Ping to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nPing to 10.129.173.1 failed. Retrying...\r\nFAILURE\r\n10.129.173.1 is not pingable.\r\n", "stdout_lines": ["Trying to ping default gateway 10.129.173.1...Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "Ping to 10.129.173.1 failed. Retrying...", "FAILURE", "10.129.173.1 is not pingable."]}


```bash
[heat-admin@overcloud-controller-0 ~]$ ip route
default via 10.129.172.1 dev vlan172 
default via 10.129.173.1 dev eno2 proto dhcp metric 101 
default via 10.129.173.1 dev eno3 proto dhcp metric 102 
default via 10.129.173.1 dev eno4 proto dhcp metric 103 
10.129.172.0/24 dev vlan172 proto kernel scope link src 10.129.172.150 
10.129.173.0/24 dev br-ex proto kernel scope link src 10.129.173.128 
10.129.173.0/24 dev eno2 proto kernel scope link src 10.129.173.93 metric 101 
10.129.173.0/24 dev eno3 proto kernel scope link src 10.129.173.91 metric 102 
10.129.173.0/24 dev eno4 proto kernel scope link src 10.129.173.92 metric 103 
192.168.174.0/24 dev vlan174 proto kernel scope link src 192.168.174.250 
192.168.175.0/24 dev vlan175 proto kernel scope link src 192.168.175.183 
192.168.176.0/24 dev vlan176 proto kernel scope link src 192.168.176.245 
192.168.177.0/24 dev vlan177 proto kernel scope link src 192.168.177.210 
```
[ctlplane-subnet]
cidr = 10.129.173.0/24
dhcp_start = 10.129.173.100
dhcp_end = 10.129.173.199
inspection_iprange = 10.129.173.90,10.129.173.99
gateway = 10.129.173.1



```bash
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master ovs-system state UP group default qlen 1000
    link/ether c8:1f:66:ce:5b:98 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::ca1f:66ff:fece:5b98/64 scope link 
       valid_lft forever preferred_lft forever
3: eno2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether c8:1f:66:ce:5b:99 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.93/24 brd 10.129.173.255 scope global dynamic noprefixroute eno2
       valid_lft 504sec preferred_lft 504sec
    inet6 fe80::ca1f:66ff:fece:5b99/64 scope link 
       valid_lft forever preferred_lft forever
4: eno3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether c8:1f:66:ce:5b:9a brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.91/24 brd 10.129.173.255 scope global dynamic noprefixroute eno3
       valid_lft 505sec preferred_lft 505sec
    inet6 fe80::ca1f:66ff:fece:5b9a/64 scope link 
       valid_lft forever preferred_lft forever
5: eno4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether c8:1f:66:ce:5b:9b brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.92/24 brd 10.129.173.255 scope global dynamic noprefixroute eno4
       valid_lft 503sec preferred_lft 503sec
    inet6 fe80::ca1f:66ff:fece:5b9b/64 scope link 
       valid_lft forever preferred_lft forever
```