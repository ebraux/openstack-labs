


## configuration mise en place

Infrastructure avec 3 noeud.
Chaque noeud a les différents rôles : Monitor, OSD, MDS, 
Le mode retenu est "container deployment only"

Il faut un noeud complémentaire pour tout ce qui concerne le monitoring

Pour le stockage des images, on utilise une registry locale (sur rh_registry", qui est considérée comme inscure par Docker.

Ceph-ansible ne sais pas gérér les registry insecure, il faut donc installe ret configurer Docker en amont.

Tout est déployé en RedHat 7.8, pour pouvoit intégrer des serveurs de génération 11, en tant qu'OSD ou MON.

Les image ssont stockée sur la registry commune : "10.129.172.22:5000"


## Installation des systémes

Installation de l'OS

### Enregistrement chez RH

```bash
subscription-manager attach --pool=8a85f99a7322bd4d017322edb2e70b2b
```


## VM rh-ceph-grafana

1 interfaces 
 * ens2, reliée à br-ex, Ip 10.129.127.72.30


```bash
virt-install \
  --name rh-ceph-grafana \
  --memory=8096 \
  --vcpus=4 \
  --disk size=40 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --os-variant=rhel7  \
  --graphics=none \
  --noautoconsole \
  --wait=-1 \
  --console pty,target_type=serial \
  --autostart \
  --network bridge=br-ex \
  --initrd-inject /root/kickstart/rh-ceph-grafana.cfg \
  --extra-args="ks=file:/rh-ceph-grafana.cfg console=ttyS0 serial"
```


# configuration de docker

Configuration de la registry : ceph ansible install Docker. Si le fichier d econfig d ela registry existe, il est conservé.

```bash
sudo tee /etc/containers/registries.conf > /dev/null <<EOT
[registries.search]
registries = ['registry.access.redhat.com', 'registry.redhat.io', 'docker.io','10.129.172.22:5000']
[registries.insecure]
registries = ['10.129.172.22:5000' ]
[registries.block]
registries = ['*']
EOT
```

## Configuration des disques

* on utilise l'utilitaire "percli"
* Disque principal : deux disque en mirroir
*  un disque de donénes pour CEPH


Installation de l'utilitaire "perccli"
```bash
cd 
mkdir perccli
cd perccli
wget https://dl.dell.com/FOLDER04470715M/1/perccli_7.1-007.0127_linux.tar.gz .
tar -xzvf perccli_7.1-007.0127_linux.tar.gz
rpm -ivh Linux/perccli-007.0127.0000.0000-1.noarch.rpm

ls /opt/MegaRAID/
```

### initialisation pour 1 disque
```bash
/opt/MegaRAID/perccli/perccli64 /c0/vall show
/opt/MegaRAID/perccli/perccli64 /c0/v1 del
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off

```


### infos complémentaires

Infos sur les disques :

```bash
/opt/MegaRAID/perccli/perccli64 /c0/e32/sall show
```

Infos sur les disques virtuels
```bash
/opt/MegaRAID/perccli/perccli64 /c0/vall show
```

Suppression d'un disque virtuel
```bash
/opt/MegaRAID/perccli/perccli64 /c0/v1 del
```

Création du RAID 0
```bash
/opt/MegaRAID/perccli/perccli64 /c0/vall show
```

Configuration du disque pour CEPH
```bash
# si la crte support le mode jbod
/opt/MegaRAID/perccli/perccli64 /c0/e32/s2 set jbod

# sinon, on fait du RAID0, sans cache
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off

```

/opt/MegaRAID/perccli/perccli64 /c0/v1 del
/opt/MegaRAID/perccli/perccli64 /c0 add vd type=raid0 name=osd1 drives=32:2 wt nora pdcache=off


si besoin RAID

* Create a single RAID 0 volume with write-back for each Ceph OSD data drive with write-back cache enabled.
*  Enabling pass-through mode helps avoid caching logic, and generally results in much lower latency for fast media.

## configuration du réseau

### Interace principale, sur NIC1

```bash
DEVICE=eth0
BOOTPROTO=static
ONBOOT=yes
TYPE=Ethernet
HWADDR=00:24:e8:3b:c2:02
IPADDR=10.129.172.11
BROADCAST=10.129.172.255
NETMASK=255.255.255.0
NETWORK=10.129.172.0
DEFROUTE=yes
DNS1=192.44.75.10
DNS2=192.108.115.2
NM_CONTROLLED=no
```

### br-storage, Vlan 176, sur NIC2

ifcfg-eth1.176

```bash
DEVICE=eth1.176
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-storage
```

ifcfg-br-storage

```bash
DEVICE=br-storage
TYPE=Bridge
BOOTPROTO=none
IPADDR=192.168.176.12
NETMASK=255.255.255.0
BROADCAST=192.168.176.255
NETWORK=192.168.176.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-storage 10.129.176.10
ping -c 3 -I br-storage 10.129.176.11
ping -c 3 -I br-storage 10.129.176.12
```

### br-mgmt, Vlan 177, sur NIC3

ifcfg-eth2.177

```bash
DEVICE=eth2.177
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-mgmt
```

ifcfg-br-mgmt

```bash
DEVICE=br-mgmt
TYPE=Bridge
BOOTPROTO=none
IPADDR=192.168.177.12
NETMASK=255.255.255.0
BROADCAST=192.168.177.255
NETWORK=192.168.177.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

Tester

```bash
ip addr

ping -c 3 -I br-mgmt 10.129.177.10
ping -c 3 -I br-mgmt 10.129.177.11
ping -c 3 -I br-mgmt 10.129.177.12
```


## Ajout de l'utilisateur Ansible

```bash
adduser ansible-user
passwd ansible-user
 ansible2020

echo "ansible-user ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/ansible-user

chmod 0440 /etc/sudoers.d/ansible-user
```

Authorisation d'accès. à lancer dpuis machine rh-ansible

```bash
ssh-copy-id ansible-user@10.129.172.30
ssh-copy-id ansible-user@10.129.172.31
ssh-copy-id ansible-user@10.129.172.32
ssh-copy-id ansible-user@10.129.172.33


```
