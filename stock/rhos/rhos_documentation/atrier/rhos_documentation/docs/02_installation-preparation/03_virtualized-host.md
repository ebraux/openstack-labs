

* https://access.redhat.com/articles/1344173



# installation de libvirt

## RHEL7 :

* [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-installing_the_virtualization_packages-installing_virtualization_packages_on_an_existing_red_hat_enterprise_linux_system](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-installing_the_virtualization_packages-installing_virtualization_packages_on_an_existing_red_hat_enterprise_linux_system)
```bash
yum install qemu-kvm libvirt
yum install virt-install libvirt-python libvirt-client
yum install virt-manager
yum install bridge-utils
#libvirt-daemon libvirt-daemon-driver-qemu libvirt-daemon-kvm 
# bridge-utils rsync virt-viewer
```


## RHEL8 

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization
```bash

yum install libvirt-client libvirt-daemon qemu-kvm libvirt-daemon-driver-qemu libvirt-daemon-kvm virt-install 
bridge-utils rsync virt-viewer

Erreur : qemu-img conflicts with 10:qemu-img-rhev-2.12.0-44.el7_8.1.x86_64
Erreur : qemu-img-rhev conflicts with 10:qemu-img-1.5.3-173.el7.x86_64
Erreur : qemu-kvm-common conflicts with 10:qemu-kvm-common-rhev-2.12.0-44.el7_8.1.x86_64
Erreur : qemu-kvm-rhev conflicts with 10:qemu-kvm-1.5.3-173.el7.x86_64
Erreur : qemu-kvm conflicts with 10:qemu-kvm-rhev-2.12.0-44.el7_8.1.x86_64
Erreur : qemu-kvm-common-rhev conflicts with 10:qemu-kvm-common-1.5.3-173.el7.x86_64

systemctl start libvirtd
systemctl enable  libvirtd

virt-host-validate

```


### Correction pour IOMMU

`WARN (IOMMU appears to be disabled in kernel. Add intel_iommu=on to kernel cmdline arguments)`

Edit the file /etc/default/grub and add intel_iommu=on to the existing GRUB_CMDLINE_LINUX line.

```ini
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="crashkernel=auto rd.lvm.lv=VolGroup00/lvroot rd.lvm.lv=VolGroup00/lvswap biosdevname=0 net.ifnames=0 rhgb quiet kvm-intel.nested=1 intel_iommu=on"
GRUB_DISABLE_RECOVERY="true"
```

Mise à jour
```bash
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
```

Redémarrage
```bash
sudo reboot
```

Source : 
 
 * [https://scottlinux.com/2017/05/10/how-to-enable-iommu-support-in-fedora-linux/](https://scottlinux.com/2017/05/10/how-to-enable-iommu-support-in-fedora-linux/)


### Correction pour FUSE

`FAIL (Load the 'fuse' module to enable /proc/ overrides)`

```bash
echo 'fuse' | sudo tee -a /etc/modules-load.d/fuse.conf
sudo systemctl enable systemd-modules-load
sudo systemctl restart systemd-modules-load
```

Redémarrage

```bash
sudo reboot
```

Source : 
* [https://access.redhat.com/solutions/1609883](https://access.redhat.com/solutions/1609883)


Test des outils 


# Configuration réseau d ela machine Hote
https://jamielinux.com/docs/libvirt-networking-handbook/bridged-network.html

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-networkscripts-interfaces_network-bridge


Véfiri module bridge
```bash
modinfo bridge
```

Sinon activer
```bash
modprobe --first-time bridge
modprobe --first-time 8021q
```
 

## Cible pour la machien Hote director

eth0: interface d'accèas principale sur le Vlan Externe
eth1: interface bridge pour les VM qui seront dans le Vlan externe
eth2 : interface dédié à Director
eth2 : interface dédié à Ceph Monitoring

eth0: br-external : bridge avec une IP principale pour la VM, et des interface vityuelle pour les VM
eth1: rien
eth2 : br-ctlplane : pas d econfiguartion, pas d'IP. dédié à Director
eth3 : eth3.176 / br-storage  : interface dédié à Ceph Monitoring


# Lancement des VM








## VM rh-director version 13

2 interfaces 
 * ens2, reliée à br-ex, Ip 10.129.127.72.24
 * ens3, reliée à br-ctlplane, sans IP

```bash
virt-install \
  --name rh-director13 \
  --memory=8096 \
  --vcpus=4 \
  --disk size=40 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --os-variant=rhel7  \
  --graphics=none \
  --noautoconsole \
  --wait=-1 \
  --console pty,target_type=serial \
  --autostart \
  --network bridge=br-ex \
  --initrd-inject /root/kickstart/rh-director13.cfg \
  --extra-args="ks=file:/rh-director13.cfg console=ttyS0 serial"
```


## VM rh-director

2 interfaces 
 * ens2, reliée à br-ex, Ip 10.129.127.72.23
 * ens3, reliée à br-ctlplane, sans IP

```bash
virt-install \
  --name rh-director \
  --memory=16384 \
  --vcpus=4 \
  --disk size=100 \
  --location /var/lib/libvirt/images/rhel-8.2-x86_64-dvd.iso \
  --os-variant=rhel8.0  \
  --graphics=none \
  --noautoconsole \
  --wait=-1 \
  --console pty,target_type=serial \
  --autostart \
  --network bridge=br-ex \
  --network bridge=br-ctlplane \
  --initrd-inject /root/kickstart/rh-director.cfg \
  --extra-args="ks=file:/rh-director.cfg console=ttyS0 serial"
```



# Annexes

## Options complémentaires de virt-install


--extra-args 'console=ttyS0,115200n8 serial'

# console=tty0 console=ttyS0,115200n8"

virt-install --name undercloud --memory=16384 --vcpus=4 --location /var/lib/libvirt/images/rhel-server-7.5-x86_64-dvd.iso --disk size=100 --network bridge=br-ex --network bridge=br-ctlplane --graphics=vnc --hvm --os-variant=rhel7



```bash
virt-install \
  --name test4 \
  --memory 2048 \
  --vcpus 1 \
  --disk size=20 \
  --location /var/lib/libvirt/images/rhel-8.2-x86_64-dvd.iso \
  --os-variant=rhel8.0  \
  --graphics=none \
  --extra-args console=ttyS0
```



WARNING  "console=ttyS0" introuvables dans --extra-args (nécessaires pour voir la sortie de l’installateur en mode texte de l’invité).



Autres options :
* --noautoconsole : ne pas retser connecté à la console. ensuite, il fauta faire `virsh console <...>`
* --autostart : start the VM when the host server comes up after reboot
* --hvm \
* --console=tty0 console=ttyS0,115200n8
* --network network=default  --network bridge=nm-bridge --console pty,target_type=serial --graphics=vnc -v
* --initrd-inject /home/username/ks.cfg : OS is automatically configured using the /home/username/ks.cfg kickstart file
* --extra-args='console=ttyS0' 
* --console=tty0 console=ttyS0,115200n8"
* --console pty,target_type=serial 

On peut déployer à disatnce :
* --connect qemu+ssh://root@10.0.0.1/system 

* https://www.golinuxcloud.com/virt-install-examples-kvm-virt-commands-linux/





### os-variant infos

Pour RHEL8, actuellement seul '--os-variant=rhel8.0' est disponible.

Pour avoir la liste des valeurs possibles :

```bash
osinfo-query os
```

## fichier de config réseau

### br-ex sur la machine hote

ifcfg-eth0
```ini
DEVICE=eth0
NAME=eth0
HWADDR=18:66:da:ed:2c:3c
TYPE=Ethernet
NM_CONTROLLED=no
ONBOOT=yes
BRIDGE=br-ex
IPV6INIT=no
```

ifcfg-br-ex
```ini
DEVICE=br-ex
NAME=br-ex
NM_CONTROLLED=no
ONBOOT=yes
TYPE=Bridge
# If you want to turn on Spanning Tree Protocol, ask your hosting
# provider first as it may conflict with their network.
STP=off
# If STP is off, set to 0. If STP is on, set to 2 (or greater).
DELAY=0

IPADDR=10.129.172.13
NETMASK=255.255.255.0
GATEWAY=10.129.172.1
NETWORK=10.129.172.0
BROADCAST=10.129.172.255

IPV6INIT=no
```

### br-ctlplane sur la machine hote



### sur les VM

``` ini
# Generated by dracut initrd
NAME="ens2"
DEVICE="ens2"
ONBOOT="yes"
NETBOOT="yes"
UUID="1e5824f8-0f72-4a54-8347-51c4e4cd32e6"
BOOTPROTO="none"
IPADDR="10.129.172.21"
NETMASK="255.255.255.0"
GATEWAY="10.129.172.1"
TYPE="Ethernet"
DNS1="192.44.75.10"
DNS2="192.108.115.2"
PROXY_METHOD="none"
BROWSER_ONLY="no"
PREFIX="24"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="no"


```

## verification

```bash
virsh list
virsh net-list --all


virsh destroy test
virsh shutdown test

virsh undefine test

virsh start test
```


## Config du réseau

Info sur déclaratin de bridge et Bond manuellement :
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-network_bridging_using_the_command_line_interface


Choix du modéle :
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_administration_guide/sect-bridged-mode

On met en place sur la machine des interface virtuelles sur les interfaces physique, avec filtrage des Vlan.
Puis on monte des bridge.
Enfin, on amméne les bridge sur les VM




Déclarer un acces via un Vlan, sur une des interfaces


Création de l'interface de Trunk

* no IP address configuration (neither static nor via DHCP). the interface est uniquement un bridge
* The `BRIDGE` configuration variable connects the physical interface to the br-enp3s0f0 bridge. 

``` ini
TYPE=Ethernet
BOOTPROTO=none
DEVICE=enp3s0f0
ONBOOT=yes
BRIDGE=br-enp3s0f0
```

Avec filtrage de vlan
```bash
DEVICE=eth1.148
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-imta
```


Création du bridge

```bash
TYPE=Bridge
BOOTPROTO=none
DEVICE=br-enp3s0f0
ONBOOT=yes
DELAY=0
```

Bridge avec une IP

```bash
DEVICE=br-imta
TYPE=Bridge
BOOTPROTO=none
IPADDR=10.129.41.131
BROADCAST=10.129.41.255
NETMASK=255.255.255.0
NETWORK=10.129.41.0
GATEWAY=10.129.41.1
DEFROUTE=YES
DELAY=0
ONBOOT=yes
NM_CONTROLLED=no
```

Redémarrage du network

```bash
sudo systemctl restart network
```

ensuite 2 approches :

* soit créer un bridge dans libvirt, attacher les instances à ce bridge, et faire la config IP au niveau système dans les instances
  * http://alesnosek.com/blog/2015/09/07/bridging-vlan-trunk-to-the-guest/
* soit créerr des tap au niveau du bridge sur le système et les configurer dans la config de la VM


exenple de config d'un VM avec Openstack
```xml
    <interface type='bridge'>
      <mac address='fa:16:3e:23:58:0e'/>
      <source bridge='br-int'/>
      <virtualport type='openvswitch'>
        <parameters interfaceid='7f31b290-5eb7-4215-abc1-46a6723a68a7'/>
      </virtualport>
      <target dev='tap7f31b290-5e'/>
      <model type='virtio'/>
      <mtu size='1450'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
    </interface>

```


Test de config, sur director2

```bash
brctl show
brctl addbr br0

brctl addif br0 eth0

brctl addif br0 vNIC1

brctl addif br0 vNIC2

```


Options d econfiguration de L'IP pour la VM 
 * https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-guest_virtual_machine_installation_overview-creating_guests_with_virt_install#sect-Guest_virtual_machine_installation_overview-virt_install-network_installation

Bridged network with DHCP
```bash
--network br0
```

Désactivation du network manager :

```bash
systemctl disable NetworkManager.service
systemctl enable network.service

systemctl stop NetworkManager.service
systemctl start network.service
```

Création d'un interface pour accéder au Vlan

```bash
DEVICE=eth2.172
BOOTPROTO=none
ONBOOT=yes
VLAN=yes
BRIDGE=br-external16
```

Création du bridge
```bash
DEVICE=br-external16
TYPE=Bridge
BOOTPROTO=none
#IPADDR=10.129.176.7
#NETMASK=255.255.252.0
#BROADCAST=10.129.179.255
#NETWORK=10.129.176.0
DEFROUTE=NO
DELAY=0
ONBOOT=yes
```

on a donc un bridge, qui donne accès au résau external (VLAN 172) :  on fait la même chose pour ctrlplane (

* br-external16, interface eth2.172, VLAN 172)
* br-prov16, interface eth2.173, VLAN 173)


```bash
virt-install \
  --name director16 \
  --memory 2048 \
  --vcpus 2 \
  --disk size=10 \
  --location /var/lib/libvirt/images/rhel-8.2-x86_64-dvd.iso \
  --os-variant=rhel8.0  \
  --graphics=none \
  --network bridge=br-external16 \
  --network bridge=br-external16 \
  --initrd-inject /root/kickstart/director16.cfg \
  --extra-args="ks=file:/director16.cfg console=tty0 console=ttyS0,115200n8"
```

  --extra-args console=ttyS0 \
  --network network=default
* --network network=default  --network bridge=nm-bridge
* 

--> reboot automatique ?
--> Activate the web console with: systemctl enable --now cockpit.socket

```bash
ip addr show br-external16
22: br-external16: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether f0:4d:a2:02:72:38 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::f24d:a2ff:fe02:7238/64 scope link 
       valid_lft forever preferred_lft forever
```



Bridged network with a static IP address
```bash
--network br0 \
--extra-args "ip=192.168.1.2::192.168.1.1:255.255.255.0:test.example.com:eth0:none"
```

* https://serverfault.com/questions/627238/kvm-libvirt-how-to-configure-static-guest-ip-addresses-on-the-virtualisation-ho



* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-network_configuration-bridged_networking#sect-Network_configuration-Bridged_networking_in_RHEL

libvirt is now able to take advantage of new kernel tunable parameters to manage host bridge forwarding database (FDB) entries, thus potentially improving system network performance when bridging multiple virtual machines. Set the macTableManager attribute of a network's <bridge> element to 'libvirt' in the host's XML configuration file:
<bridge name='br0' macTableManager='libvirt'/>

#---

# tests :

Lancement d'une VM

The KVM host requires the following packages :

```bash
yum install libvirt-client libvirt-daemon qemu-kvm libvirt-daemon-driver-qemu libvirt-daemon-kvm virt-install bridge-utils rsync virt-viewer
```

Récupérer l'image de boot : https://access.redhat.com/downloads/content/69/ver=/rhel---7/7.8/x86_64/product-software

Lancemement :

```bash
virt-install \
  --name undercloud \
  --memory 16384 \
  --vcpus 4 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --disk size=100 \
  --network bridge=br-ex \
  --network bridge=br-ctlplane \
  --hvm \
  --graphics=none \
  --os-variant=rhel7 \
  --console pty,target_type=serial 
 ```




Récupérer son adresse IP

```bash
virsh  dumpxml  $VM_NAME | grep 'mac address'
virsh domiflist test4
```

Attacher une interface

```bash
virsh attach-interface --domain vm1 --type network \
        --source openstackvms --model virtio \
        --mac 52:54:00:4b:73:5f --config --live
```

Détacher l'interface

```bash
virsh detach-interface --domain vm1 --type network \
        --mac 52:53:00:4b:75:6f --config
```

#---

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/13/html/director_installation_and_usage/chap-requirements#virtualization_support
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index

* http://alesnosek.com/blog/2015/09/07/bridging-vlan-trunk-to-the-guest/
* http://blog.davidvassallo.me/2012/05/05/kvm-brctl-in-linux-bringing-vlans-to-the-guests/
* http://nickapedia.com/2011/11/28/now-for-something-completely-different-ubuntu-11-10-kvm-vlan-trunking/

* https://www.linux-kvm.org/page/Networking
* https://www.itzgeek.com/how-tos/mini-howtos/create-a-network-bridge-on-centos-7-rhel-7.html
* https://developers.redhat.com/blog/2017/09/14/vlan-filter-support-on-bridge/

* https://linux.goffinet.org/administration/virtualisation-kvm/#31-cr%C3%A9er-une-machine-virtuelle-avec-virt-manager
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-vlan_on_bond_and_bridge_using_the_networkmanager_command_line_tool_nmcli
* https://www.thegeekdiary.com/centos-rhel-7-how-to-configure-vlan-tagging-using-nmcli/
* https://www.redhat.com/sysadmin/vlans-configuration
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-managing_guest_virtual_machines_with_virsh-managing_virtual_networks

* https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf
* https://images.rdoproject.org/docs/baremetal/customizing-external-network-vlan.html

* https://www.dedoimedo.com/computers/kvm-bridged.html


On installe Director en tant que VM sur un noeud hote. Là on sinstall directeor avec KVM

The KVM host uses two Linux bridges:

* br-ex (eth0) :  
  * Provides outside access to the undercloud
  * DHCP server on outside network assigns network configuration to undercloud using the virtual NIC (eth0)
  * Provides access for the undercloud to access the power management interfaces for the bare metal servers

* br-ctlplane (eth1)
  * Connects to the same network as the bare metal overcloud nodes
  * Undercloud fulfills DHCP and PXE boot requests through virtual NIC (eth1)
  * Bare metal servers for the overcloud boot through PXE over this network

### config réseau intiliale de l'HOST

```bash
ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.13/24 brd 10.129.172.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
4: eth2: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3e brd ff:ff:ff:ff:ff:ff
5: eth3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3f brd ff:ff:ff:ff:ff:ff
```

/etc/sysconfig/network
```bash
# Created by anaconda
GATEWAY=10.129.172.1
```

/etc/sysconfig/network-scripts/ifcfg-eth0
```bash
DEVICE=eth0
BOOTPROTO=static
ONBOOT=yes
TYPE=Ethernet
HWADDR=18:66:da:ed:2c:3c
IPADDR=10.129.172.13
BROADCAST=10.129.172.255
NETMASK=255.255.255.0
NETWORK=10.129.172.0
NM_CONTROLLED=no
```

### création des Linux Bridges

```bash
brctl addbr br-ex
brctl addbr br-ctlplane

ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 18:66:da:ed:2c:3c brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.13/24 brd 10.129.172.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1a66:daff:feed:2c3c/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3d brd ff:ff:ff:ff:ff:ff
4: eth2: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3e brd ff:ff:ff:ff:ff:ff
5: eth3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 18:66:da:ed:2c:3f brd ff:ff:ff:ff:ff:ff
6: br-ex: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether de:29:51:8b:27:c8 brd ff:ff:ff:ff:ff:ff
7: br-ctlplane: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether ea:e5:4c:85:a7:e3 brd ff:ff:ff:ff:ff:ff
```




### Instalation de la VM pour Director

The KVM host requires the following packages :

```bash
yum install libvirt-client libvirt-daemon qemu-kvm libvirt-daemon-driver-qemu libvirt-daemon-kvm virt-install bridge-utils rsync virt-viewer
```

récupérer : https://access.redhat.com/downloads/content/69/ver=/rhel---7/7.8/x86_64/product-software

Lancemement :

```bash
virt-install \
  --name undercloud \
  --memory 16384 \
  --vcpus 4 \
  --location /var/lib/libvirt/images/rhel-server-7.8-x86_64-dvd.iso \
  --disk size=100 \
  --network bridge=br-ex \
  --network bridge=br-ctlplane \
  --hvm \
  --graphics=none \
  --os-variant=rhel7 \
  --console pty,target_type=serial 
 ```
 
 Autres options :
 
``` bash


  
  --extra-args 'console=ttyS0,115200n8'

  --disk /path/to/imported/disk.qcow \ 
  --import \ 

  --graphics=vnc
```


Config reseau pour "undercloud" \
 :

``` bash
 1) IPv4 address or "dhcp" for DHCP
    10.129.172.20
 2) IPv4 netmask
    255.255.255.0
 3) IPv4 gateway
    10.29.172.1
 4) IPv6 address[/prefix] or "auto" for automatic, "dhcp" for DHCP, "ignore" to
    turn off
    auto
 5) IPv6 default gateway
 6) Nameservers (comma separated)
    192.108.115.2,192.44.75.10
 7) [x] Connect automatically after reboot
 8) [x] Apply configuration in installer
Configuring device eth0.
```
``` bash
 1) IPv4 address or "dhcp" for DHCP
    10.129.41.130
 2) IPv4 netmask
    255.255.255.0
 3) IPv4 gateway
    10.129.41.1
 4) IPv6 address[/prefix] or "auto" for automatic, "dhcp" for DHCP, "ignore" to
    turn off
    auto
 5) IPv6 default gateway
 6) Nameservers (comma separated)
    192.108.115.2, 192.44.75.10
 7) [x] Connect automatically after reboot
 8) [x] Apply configuration in installer
 Configuring device eth1.
``` 


https://linux.goffinet.org/administration/virtualisation-kvm/#31-cr%C3%A9er-une-machine-virtuelle-avec-virt-manager
  


Une fois créé :

``` bash
[root@undercloud ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:e0:01:e4 brd ff:ff:ff:ff:ff:ff
    inet 10.29.172.20/24 brd 10.29.172.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::d25b:da52:88a2:5b90/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 52:54:00:2a:2e:ef brd ff:ff:ff:ff:ff:ff
    inet 10.129.41.130/24 brd 10.129.41.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::6e8e:6eee:5803:4af6/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
``` 
et sur la machine hote

```  bash
19: vnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br-ex state UNKNOWN group default qlen 1000
    link/ether fe:54:00:e0:01:e4 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc54:ff:fee0:1e4/64 scope link 
       valid_lft forever preferred_lft forever
20: vnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br-ctlplane state UNKNOWN group default qlen 1000
    link/ether fe:54:00:2a:2e:ef brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc54:ff:fe2a:2eef/64 scope link 
       valid_lft forever preferred_lft forever

brctl show
bridge name	bridge id		STP enabled	interfaces
br-ctlplane		8000.fe54002a2eef	no		vnet1
br-ex		8000.fe5400e001e4	no		vnet0


[root@os-director-01 ~]# brctl showmacs br-ctlplane
port no	mac addr		is local?	ageing timer
  1	fe:54:00:2a:2e:ef	yes		   0.00
  1	fe:54:00:2a:2e:ef	yes		   0.00
[root@os-director-01 ~]# brctl showmacs br-ex
port no	mac addr		is local?	ageing timer
  1	fe:54:00:e0:01:e4	yes		   0.00
  1	fe:54:00:e0:01:e4	yes		   0.00

``` 




### Gestion de la VM pour Director

#### Backup

Create a live snapshot of the running VM 

``` bash
virsh snapshot-create-as --domain undercloud --disk-only --atomic --quiesce
```

Take a copy of the (now read-only) QCOW backing file
```bash
rsync --sparse -avh --progress /var/lib/libvirt/images/undercloud.qcow2 1.qcow2
```

Merge the QCOW overlay file into the backing file and switch the undercloud VM back to using the original file:
``` bash
virsh blockcommit undercloud vda --active --verbose --pivot

```






* https://forums.centos.org/viewtopic.php?t=72261 : dns=192.168.39.2 ip=192.168.39.200::192.168.39.2:255.255.255.0:test.example.com:ens2:none






# anaconda

```bash
 cat anaconda-ks.cfg 
#version=RHEL8
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=thinp
# Partition clearing information
clearpart --linux --initlabel --drives=vda
# Use text mode install
text
# Use CDROM installation media
cdrom
# Keyboard layouts
keyboard --vckeymap=us --xlayouts=''
# System language
lang fr_FR.UTF-8

# Network information
network  --bootproto=static --device=ens3 --gateway=192.168.1.1 --ip=192.168.1.45 --nameserver=108.108.15.2 --netmask=255.255.255.0 --ipv6=auto --activate
network  --hostname=test4
repo --name="AppStream" --baseurl=file:///run/install/repo/AppStream
# Root password
rootpw --iscrypted $6$uv6E8khsXK2atiQA$4UrxnEOCTM/F3mc9cn7BRX.SCx3AIh7Th/snbITLDm6e3UXHtcFMGqVBBzX5HJ22.0SCtnk55TGzDvwPfq5.K/
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"
# System timezone
timezone America/New_York --isUtc
user --groups=cloud --name=cloud --password=$6$q0GVpLdpOWpVdQtC$n0j5lEDAr83Kfv7.jfD6WCa06hl/nIQnloi5z9s6eZ.MVbUokCDAe84f/fpS6y7OgCKmL83Dc9P2cgyIQbcBD1 --iscrypted --gecos="cloud"

%packages
@^minimal-environment
@standard
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
```

---


/etc/sysconfig/network-scripts/ifcfg-ens3
```bash
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens3"
UUID="db0f5a8d-23b4-485b-b456-67d3fe9f469a"
DEVICE="ens3"
ONBOOT="yes"
IPADDR="192.168.1.45"
PREFIX="24"
GATEWAY="192.168.1.1"
DNS1="108.108.15.2"
```


```bash
 cat anaconda-ks.cfg 
#version=RHEL8
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=thinp
# Partition clearing information
clearpart --linux --initlabel --drives=vda
# Use text mode install
text
# Use CDROM installation media
cdrom
# Keyboard layouts
keyboard --vckeymap=us --xlayouts=''
# System language
lang fr_FR.UTF-8

# Network information
network  --bootproto=static --device=ens3 --gateway=10.129.172.1 --ip=10.129.172.20 --nameserver=192.44.75.10,192.108.115.2 --netmask=255.255.255.0 --noipv6 --activate
network  --hostname=director16
repo --name="AppStream" --baseurl=file:///run/install/repo/AppStream
# Root password
rootpw --iscrypted $6$F3qXctBG$vOGgzEnaiCkfRuFstU3jJTeLpmo1k4dIYNET.tcQbok5KackkynJ4e31fjM4frYhYiIELAMbTZbtwCuvFPZtc.
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"
# System timezone
timezone Europe/Paris --isUtc --ntpservers=univers.enst-bretagne.fr
user --groups=cloud --name=cloud --password=$6$q0GVpLdpOWpVdQtC$n0j5lEDAr83Kfv7.jfD6WCa06hl/nIQnloi5z9s6eZ.MVbUokCDAe84f/fpS6y7OgCKmL83Dc9P2cgyIQbcBD1 --iscrypted --gecos="cloud"
hostnamectl set-hostname `hostname -f`

%packages
@^minimal-environment
@standard
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
#pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
#pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
#pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
```
