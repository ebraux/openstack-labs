title: Péparation générales des noeuds
dscription: config du prxy, souscription




Gestion de local

```bash
localectl

localectl list-locales
dnf install glibc-langpack-en
dnf install glibc-langpack-fr
localectl set-locale LANG=en_US.utf8
#localectl set-locale LANG=fr_FR.utf8

localectl
```

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/ch-keyboard_configuration
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/using-langpacks_configuring-basic-system-settings



Gestion du hostname


```bash
hostname
hostname -f
sudo hostnamectl set-hostname rh-director.imta.fr
sudo hostnamectl set-hostname --transient rh-director.imta.fr
```


Config du "/etc/hosts" : The IP address in /etc/hosts must match the address that you plan to use for your undercloud public API.

On défini ces infos manuellement lors du déploirment de l'undercloud.

 * Le subnet utilisé pour le ctrlplane sera : 10.129.173.0/24
 * L'IP utilisée pour le "undercloud public API." ser 10.129.173.5


```bash
echo '10.129.173.5 rh-director.ctlplane.localdomain rh-director.ctlplane'  >> /etc/hosts
#echo '10.129.173.5 rh-director.imta.fr rh-director' >> /etc/hosts
```

### configuration du proxy

!!! attention avec le sproxy !!
podman recopie la config de proxy de l'hote, au moment du lancment.
donc si c'est incomplet, c'est la merde ....
Il vaut mieux faire une config spécique pas appli, plutôt qu'une config globale par variables d'environnement.

Qui a beoisn du proxy ?
enregistrement de smachines : rhsm

Getsion des package, si pas de dépot local  : yum dnf.

Docker / podman pour les accès registry red.hat, si pas d edépot local.

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation#configuring-an-undercloud-proxy

```bash
http_proxy=http://10.129.173.5:8080
https_proxy=http://10.129.173.5:8080
no_proxy=127.0.0.1,localhost,rh-director.ctlplane.localdomain,rh-director.ctlplane,10.129.172.0/24,10.129.173.0/24,192.168.173.0/24,192.168.174.0/24,192.168.175.0/24,192.168.176.0/24,192.168.177.0/24,10.29.20.0/24,10.129.176.0/22
```
# ---

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/preparing-for-director-installation)

L'installeur lance des daemon. configurer le proxy dans la fenre de shels n'est pas suffisant :

* https://www.thegeekdiary.com/how-to-configure-proxy-server-in-centos-rhel-fedora/
* https://urclouds.com/2019/01/12/how-to-configure-proxy-in-centos-7-and-rhel-7/

* https://access.redhat.com/solutions/3120561

Create a new file /etc/profile.d/setproxy.sh

exlusion :
* IP local : 10.129.172.xxx
* registry Docker : 10.129.172.22
* Ip publique de l'undercloud : 10.129.173.5
* les MON Ceph : 10.129.172.31, 10.129.172.32 et 10.129.172.33


```bash
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,10.129.172.21,10.129.172.22,10.129.173.5,10.129.172.31,10.129.172.32,10.129.172.33

echo "http_proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/environment
echo "https_proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/environment
echo "no_proxy=127.0.0.1,localhost,10.129.172.21,10.129.172.22,10.129.173.5,10.129.172.31,10.129.172.32,10.129.172.33" >> /etc/environment

echo "export http_proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/profile.d/setproxy.sh
echo "export http_proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/profile.d/setproxy.sh
echo "export no_proxy=\"127.0.0.1,localhost,10.129.172.21,10.129.172.22,10.129.173.5,10.129.172.31,10.129.172.32,10.129.172.33\"" >> /etc/profile.d/setproxy.sh
```

pour yum (RHEL7)
```bash
echo "proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/yum.conf
```

Pour RHSM
```bash
subscription-manager config --server.proxy_hostname=proxy.enst-bretagne.fr --server.proxy_port=8080 --server.proxy_user='' --server.proxy_password=''
cat /etc/rhsm/rhsm.conf
```

### Gestion des souscription et des repositories

```bash
subscription-manager clean

/sbin/subscription-manager register --force --baseurl https://cdn.redhat.com --org 2662793 --proxy proxy.enst-bretagne.fr:8080 --auto-attach --username DISI1 --password DiSi2003 --release 8.2
```

```bash
subscription-manager attach  --pool=8a85f99b72fa63f90173068663fd1181
```

Vérifier la souscription
```bash
subscription-manager identity
subscription-manager list
```

```bash
subscription-manager repos --disable=*
```

Ensuite, il faut "Fixer" la release du systéme, car maintetant, les version de RHOS et RHEL sont liées :  RHOS 16.1 va avec RHEL8.2, RHOS 16.2 ira avec RHEL 8.3


## configuration de base

### pour RHEL7

Lock sur la version
```bash
subscription-manager release --list
subscription-manager release --set=7.8
```

Repositoies de base pour RHEL7
```bash
subscription-manager repos \
 --enable=rhel-7-server-rpms \
 --enable=rhel-7-server-extras-rpms \
 --enable=rhel-7-server-rh-common-rpms
```

```bash
yum install -y dnf
```

### Pour RHEL8

Configurer manuellement la release. Très important, sinon, on a res erreurs 404 :
```bash
subscription-manager release --list
subscription-manager release --set=8.2
```
Repositoies de base pour RHEL8
```bash
sudo subscription-manager repos \
  --enable=rhel-8-for-x86_64-baseos-eus-rpms \
  --enable=rhel-8-for-x86_64-appstream-eus-rpms
```

## finalisation

vérification des package
```bash
subscription-manager repos  --list
subscription-manager repos  --list-enabled
subscription-manager repos  --list-disabled
```

Proxy pour dnf
```bash
echo "proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/dnf/dnf.conf
```

Update et reboot
```bash
dnf clean all && rm -rf /var/cache/dnf  && dnf upgrade -y && dnf update -y
sudo reboot
```

## Désactivation de firewalld

```bash
sudo systemctl stop  firewalld
sudo systemctl disable   firewalld
```

## packages complémentaires

```bash
dnf install -y net-tools
dnf install -y vim
```


## Améliorations

### Proxy

Apparement, en RH8, on peut utiliser des subnet dans "no_proxy"
```bash
no_proxy
A comma-separated list of IP addresses and domains excluded from proxy communications. Include all IP addresses and domains relevant to the undercloud.

For example, use the following syntax to define the http_proxy, https_proxy, and no_proxy parameters:

http_proxy=https://10.0.0.1:8080
https_proxy=https://10.0.0.1:8080
no_proxy=127.0.0.1,172.16.0.0/16,172.17.0.0/16,172.18.0.0/16,192.168.0.0/16,HOSTNAME.ctlplane.localdomain

#export no_proxy=${no_proxy},$(echo 192.168.100.{1..255} | sed 's/ /,/g')
```
