


subscription-manager register
  DISI1
  DiSi2003

## souscriptions :

CEPH :
```bash
subscription-manager attach --pool=8a85f99b72fa63f90173068663fd1181
```

Opensatck :
```bash
subscription-manager attach --pool=8a85f99b72fa63f90173068663fd1181
```

## Openstack


subscription : "Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)"


```bash
Nom de l'abonnement : Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
Fournit :             dotNET on RHEL (for RHEL Workstation)
                      Oracle Java (for RHEL Workstation)
                      Red Hat Ansible Engine
                      Red Hat CodeReady Linux Builder for Power, little endian
                      Red Hat Enterprise Linux Fast Datapath
                      Red Hat CloudForms
                      JBoss Enterprise Application Platform
                      Red Hat Software Collections Beta (for RHEL Server for IBM Power LE)
                      Red Hat Enterprise Linux for Power, big endian - Extended Update Support
                      Red Hat Virtualization
                      Red Hat Beta
                      Red Hat Enterprise Linux Resilient Storage for Power, little endian
                      Red Hat OpenShift Container Platform
                      Red Hat Gluster Storage Management Console (for RHEL Server)
                      dotNET on RHEL (for RHEL Server)
                      Red Hat CodeReady Linux Builder for x86_64 - Extended Update Support
                      Red Hat Ceph Storage Calamari
                      Red Hat Developer Tools (for RHEL Workstation)
                      Red Hat Software Collections Beta (for RHEL Server for IBM Power)
                      Red Hat Developer Tools (for RHEL Server for IBM Power LE)
                      Red Hat Enterprise Linux High Availability (for IBM Power LE) - Extended Update Support
                      Oracle Java (for RHEL Server)
                      Red Hat Enterprise Linux Resilient Storage for x86_64
                      Red Hat Software Collections (for RHEL Server)
                      Red Hat Enterprise Linux for Power, little endian - Extended Update Support
                      Red Hat Enterprise Linux for Power, little endian
                      Red Hat Enterprise Linux for Power, big endian
                      Red Hat Gluster Storage Server for On-premise
                      Oracle Java (for RHEL Compute Node)
                      Red Hat Enterprise Linux for Scientific Computing
                      Red Hat Enterprise Linux for Power 9
                      Red Hat Enterprise Linux Fast Datapath Beta for x86_64
                      Red Hat Developer Tools (for RHEL Server)
                      Red Hat Enterprise MRG Messaging
                      Red Hat Software Collections Beta (for RHEL Workstation)
                      Red Hat Developer Tools Beta (for RHEL Server)
                      Red Hat Enterprise Linux for x86_64
                      Red Hat Ceph Storage MON
                      Red Hat Software Collections (for RHEL Server for IBM Power)
                      Red Hat S-JIS Support (for RHEL Server) - Extended Update Support
                      RHEL for SAP Applications for Power BE EUS
                      Red Hat Gluster Storage Nagios Server
                      Red Hat Developer Tools (for RHEL Server for IBM Power)
                      dotNET on RHEL Beta (for RHEL Server)
                      Red Hat CodeReady Linux Builder for x86_64
                      Red Hat Software Collections (for RHEL Workstation)
                      Red Hat Ceph Storage
                      Red Hat Enterprise Linux Resilient Storage for IBM Power LE - Extended Update Support
                      Red Hat Enterprise Linux Scalable File System (for RHEL Server) - Extended Update Support
                      Red Hat Container Images Beta
                      Red Hat Software Collections (for RHEL Server for IBM Power LE)
                      Red Hat Enterprise Linux Atomic Host Beta
                      Red Hat JBoss Core Services
                      Red Hat OpenStack Beta
                      Red Hat Container Images
                      Red Hat Enterprise Linux Load Balancer (for RHEL Server)
                      Red Hat Virtualization Manager
                      Red Hat EUCJP Support (for RHEL Server) - Extended Update Support
                      Red Hat Enterprise Linux High Availability for x86_64
                      Red Hat Single Sign-On
                      Red Hat Enterprise Linux Load Balancer (for RHEL Server) - Extended Update Support
                      Red Hat Enterprise Linux for Power, little endian Beta
                      Red Hat Gluster Storage Web Administration (for RHEL Server)
                      Red Hat Enterprise Linux Desktop
                      Red Hat Enterprise Linux Workstation
                      Red Hat Developer Tools Beta (for RHEL Server for IBM Power LE)
                      Red Hat CloudForms Beta
                      Red Hat Enterprise Linux Resilient Storage for x86_64 - Extended Update Support
                      Red Hat Enterprise Linux High Availability for x86_64 - Extended Update Support
                      Red Hat Developer Tools Beta (for RHEL Workstation)
                      RHEL for SAP Applications for Power LE EUS
                      Red Hat Enterprise Linux for SAP Applications for Power, little endian
                      Oracle Java (for RHEL Client)
                      Red Hat Developer Toolset (for RHEL Workstation)
                      Red Hat OpenStack
                      Red Hat Enterprise Linux Scalable File System (for RHEL Server)
                      Red Hat Enterprise Linux High Performance Networking (for RHEL Server) - Extended Update Support
                      Red Hat Virtualization Host
                      Oracle Java (for RHEL Server) - Extended Update Support
                      Red Hat Enterprise Linux Atomic Host
                      Red Hat Software Collections Beta (for RHEL Server)
                      Red Hat Enterprise Linux Server
                      RHEL for SAP Applications for Power BE
                      Red Hat Enterprise Linux for SAP Applications for x86_64
                      Red Hat Enterprise Linux for x86_64 - Extended Update Support
                      dotNET on RHEL Beta (for RHEL Workstation)
                      dotNET on RHEL (for RHEL Compute Node)
                      RHEL for SAP - Extended Update Support
                      Red Hat Developer Toolset (for RHEL Server)
                      dotNET on RHEL Beta (for RHEL Compute Node)
                      Red Hat CodeReady Linux Builder for Power, little endian - Extended Update Support
SKU :                 RH00191
Contrat :             12385470
ID du pool :          8a85f99b72fa63f90173068663fd1181
Fournit la gestion :  Oui
Disponible :          249996
Suggestion(s) :       1
Type de service :     L1-L3
Roles:                
Niveau de service :   Standard
Usage:                Production
Add-ons:              
Type d'abonnement :   Standard
Démarre :             22/08/2020
Termine :             22/08/2021
Entitlement Type:     Physique
```


## CEPH

Prix du support c'est 10k€/an pour 256To ?
Possibilité de se connecter à une infra CEPH sans support RedHat ?

Tony et Thibault (RH) : attention ! le CEPH fournit dans la licence site est limité au "stockage interne" d'OpenStack (Nb : le Téra Ceph est 7 fois inférieur au Téra NetApp (~ 50 € pour Ceph)
EB : on a besoin de Ceph pour (un peu) de stockage Cinder donc le budget ne passe plus ; une solution en utilisant du Gluster par exemple afin de ne pas avoir de surcoût Ceph ; Gluster en backend Cinder
Si calcul de prix à la carte c'est impossible de tenir dans le budget alloué

Red Hat Ceph Storage Pre-Production, Standard (Up to 100 Physical Nodes)
expiration : 21/08/2021


``` bash
subscription-manager list --available --all --matches="Red Hat Ceph Storage Pre-Production, Standard (Up to 100 Physical Nodes)"
+-------------------------------------------+
    Abonnements disponibles
+-------------------------------------------+
Nom de l'abonnement : Red Hat Ceph Storage Pre-Production, Standard (Up to 100 Physical Nodes)
Fournit :             Red Hat OpenStack Director Deployment Tools Beta
                      Red Hat Software Collections (for RHEL Server)
                      Red Hat CodeReady Linux Builder for x86_64
                      Red Hat Ansible Engine
                      Red Hat Ceph Storage
                      Red Hat Enterprise Linux Scalable File System (for RHEL Server)
                      Red Hat OpenStack Director Deployment Tools for IBM Power LE
                      Red Hat OpenStack Director Deployment Tools Beta for IBM Power LE
                      Red Hat Storage Console Node
                      Red Hat Storage Console
                      Red Hat Enterprise Linux Server
                      Red Hat Ceph Storage OSD
                      Red Hat Enterprise Linux for x86_64
                      Red Hat Ceph Storage MON
                      Red Hat Ceph Storage Calamari
                      Red Hat OpenStack Director Deployment Tools
SKU :                 RS00017
Contrat :             12385572
ID du pool :          8a85f99b72fa63f90173069fc5ef1665
Fournit la gestion :  Non
Disponible :          100
Suggestion(s) :       1
Type de service :     L1-L3
Roles:                
Niveau de service :   Standard
Usage:                
Add-ons:              
Type d'abonnement :   Standard
Démarre :             22/08/2020
Termine :             22/08/2021
Entitlement Type:     Physique
```