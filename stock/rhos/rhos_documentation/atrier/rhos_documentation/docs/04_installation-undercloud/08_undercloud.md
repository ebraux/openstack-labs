title: Installation de Director et de l'Undercloud


# Description

Configuration et installation de l'undercloud
Récupération des images pour Opensatck pour le déploiement de l'undercloud

création d'un Registry docker locale
Récupération des images Docker depuis "registry.access.redhat.com" pour le déploiement de l'overcloud

Installation de ceph-ansible

Intégration des serveurs dans l'undercloud
Introspection des serveurs

Parametrage des serveur pour préparer le déploiement de l'overcloud

Création des différentes configuration
* intérgration ceph externe
* configuration résau
* RedHat-registation

Source :

* [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/)


# recommandtationq

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/planning-your-undercloud

The NIC used for the provisioning (ctlplane) network requires the ability to broadcast and serve DHCP requests to the NICs of the overcloud’s bare metal nodes. As a recommendation, create a bridge that connects the VM’s NIC to the same network as the bare metal NICs.

Use UTF-8 encoding on all nodes. Ensure the LANG environment variable is set to en_US.UTF-8 on all nodes.


Red Hat OpenStack Platform 16.1 runs on Red Hat Enterprise Linux 8.2. As a result, you must lock the content from these repositories to the respective Red Hat Enterprise Linux version.



## Gestion des repository

Mise à jour des repository par rapport à la base défini dans "host_generic".

```bash
subscription-manager repos --disable=*
```

Repositoies pour RHOS16.1 et CEPH 4
```bash
sudo subscription-manager repos \
  --enable=rhel-8-for-x86_64-baseos-eus-rpms \
  --enable=rhel-8-for-x86_64-appstream-eus-rpms \
  --enable=rhel-8-for-x86_64-highavailability-eus-rpms \
  --enable=ansible-2.9-for-rhel-8-x86_64-rpms \
  --enable=openstack-16.1-for-rhel-8-x86_64-rpms \
  --enable=fast-datapath-for-rhel-8-x86_64-rpms \
  --enable=satellite-tools-6.5-for-rhel-8-x86_64-rpms \
  --enable=rhceph-4-tools-for-rhel-8-x86_64-rpms
```

```bash
dnf clean all && rm -rf /var/cache/dnf  && dnf upgrade -y && dnf update -y
```

Set the container-tools repository module to version 2.0
```bash
sudo dnf module disable -y container-tools:rhel8
sudo dnf module enable -y container-tools:2.0
```

Update et reboot
```bash
sudo dnf update -y
sudo reboot
```

## Hostname et accès

La gestion des hostname à été définie dans "host_generic".

Edit the /etc/hosts and include an entry for the system hostname. The IP address in /etc/hosts must match the address that you plan to use for your undercloud public API. For example, if the system is named manager.example.com and uses 10.0.0.1 for its IP address, add the following line to the /etc/hosts file:

## Création de l'environnement "stack"

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/preparing-for-director-installation

Create the stack user, avec droits root

```bash
useradd stack
passwd stack
echo "stack ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/stack
chmod 0440 /etc/sudoers.d/stack
```

Créer l'arbo de référence

```bash
su - stack
mkdir ~/images
mkdir ~/templates
exit
```

## Création d'un proxy pour l'overcloud

Le déploiement de l'undercloud se fera sur le réreau "ctrlplane", qui n'a pas d'accès sortant. L'accès sortant se fait par le réseau "external". Il faut donc prévoir un proxy sur une machine paserlle entre ces 2 réseau. La machine director est parfaite pour ça.

On utilise "squid, comme relais vers le proxy de l'école 

Installation
```bash
dnf install -y squid

systemctl start squid
systemctl enable squid
```

Modif de la configuration
```bash
tee /etc/squid/squid.conf
cache_peer proxy.enst-bretagne.fr parent 8080 0 no-query default
acl all src 0.0.0.0/0.0.0.0
http_access allow all
never_direct allow all
```

Prise en compte
```bash
systemctl restart squid
```

Docs :

* [https://access.redhat.com/solutions/259903](https://access.redhat.com/solutions/259903)


## Préparation des images

### préparation de l'accès à la registry

Configurer la registry "10.129.172.22:500", comme  dans "/etc/containers/registries.conf"
```ini
...
[registries.search]
registries = ['registry.access.redhat.com', 'registry.redhat.io', 'docker.io','10.129.172.22']
...
[registries.insecure]
registries = ['rh-director.ctlplane.localdomain', '10.129.173.5', '10.129.173.3', '10.129.172.22:5000']
...
```

Vérifier l'accès à la registry Docker
```bash
curl http://10.129.172.22:5000/v2/_catalog
```


### Generation du fichier de configuration des images

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/preparing-for-director-installation#preparing-container-images

```bash
su - stack
```

Generate the default container image preparation file
```bash
openstack tripleo container image prepare default \
  --local-push-destination \
  --output-env-file local-reg-containers-prepare-parameter.yaml
```

```yaml
parameter_defaults:
  ContainerImagePrepare:
  - tag_from_label: '{version}-{release}'
    push_destination: true
    set:
      name_prefix: openstack-
      name_suffix: ''
      namespace: rh-director.ctlplane.localdomain:8787/rhosp-rhel8
      neutron_driver: ovn
      rhel_containers: false
      tag: '16.1'
```

Modifier la registry cible 
```yaml
  ContainerImagePrepare:
  - push_destination: '10.129.172.22:5000'
    ...
```




## Installation

## Configuration de Director

* https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/installing-the-undercloud

```bash
sudo su - stack
cp /usr/share/python-tripleoclient/undercloud.conf.sample ~/undercloud.conf
```
undercloud.conf
```ini
[DEFAULT]
undercloud_hostname = rh-director.imta.fr
local_interface = ens3
local_subnet = ctlplane-subnet
local_ip = 10.129.173.5/24
local_mtu = 1500

undercloud_public_host = 10.129.173.2
undercloud_admin_host =  10.129.173.3
undercloud_service_certificate =
generate_service_certificate = false

undercloud_nameservers = 192.44.75.10,192.108.115.2
undercloud_ntp_servers = ntp1.svc.enst-bretagne.fr,ntp3.svc.enst-bretagne.fr

overcloud_domain_name = localdomain

subnets = ctlplane-subnet

#inspection_interface = br-ctlplane
undercloud_update_packages = true

clean_nodes = false
container_images_file = /home/stack/containers-prepare-parameter.yaml
container_insecure_registries = '10.129.172.22:5000'
custom_env_files = /home/stack/templates/custom-undercloud-params.yaml

[auth]

[ctlplane-subnet]
cidr = 10.129.173.0/24
dhcp_start = 10.129.173.100
dhcp_end = 10.129.173.199
inspection_iprange = 10.129.173.90,10.129.173.99
gateway = 10.129.173.1
```



création des fihisers de template
```bash
touch /home/stack/templates/custom-undercloud-params.yaml
```

custom-undercloud-params.yaml
```yaml
parameter_defaults:
  Debug: True
```

## Lancement du déploiement

```bash
openstack undercloud install
```

```bash
Deployment successful!
########################################################
Writing the stack virtual update mark file /var/lib/tripleo-heat-installer/update_mark_undercloud
##########################################################
The Undercloud has been successfully installed.
Useful files:
  Password file is at /home/stack/undercloud-passwords.conf
  The stackrc file is at ~/stackrc
Use these files to interact with OpenStack services, and
ensure they are secured.
```


```bash
cat undercloud-passwords.conf
cat stackrc

```


Vérification

```bash
sudo podman ps
 -> doit afficher uen quarantaine de comtainers (keystone, neutron, nova, swift, mistral, heat ...)
```

```bash
openstack service list
+----------------------------------+------------------+-------------------------+
| ID                               | Name             | Type                    |
+----------------------------------+------------------+-------------------------+
| 0445ced9c49546d6ad73be95305f45b8 | placement        | placement               |
| 055b0ae77bf34a738506c070eb8c76e9 | glance           | image                   |
| 1b4ea1b6d1b0457e8b499fb56db2b66b | heat             | orchestration           |
| 341074b459f24640afc898454797abde | zaqar-websocket  | messaging-websocket     |
| 62018fe2ac2140ec85f2fb94feb55a84 | ironic-inspector | baremetal-introspection |
| 730ca5f3b6ad4d8ead05f5ae0b0a98b1 | keystone         | identity                |
| 8a09cf4720c74c0483680c80b9f6ec93 | ironic           | baremetal               |
| a2c28accec4044b7a621445cd5f8b0b3 | zaqar            | messaging               |
| b44a3d75ce544eab932867588be78866 | nova             | compute                 |
| c8264d7d17014342a5265f9c5e53265a | mistral          | workflowv2              |
| cc8f21a85b0743c0a687f2e44bf9841f | neutron          | network                 |
| e48cdf6d4ebb4684a65cc4136c32edcb | swift            | object-store            |
+----------------------------------+------------------+-------------------------+

cat  /home/stack/install-undercloud.log 
```

Vérification des images pour les containers à déployer
``` bash
curl http://10.129.172.22:5000/v2/_catalog | jq .repositories[]
```


## Gestion des images de l'Overcloud

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.1/html/director_installation_and_usage/installing-the-undercloud#sect-Obtaining_Images_for_Overcloud_Nodes


```bash
sudo su - stack

source ~/stackrc
```

### Single CPU architecture overclouds

```bash
sudo dnf install -y rhosp-director-images rhosp-director-images-ipa

cd ~/images
for i in /usr/share/rhosp-director-images/overcloud-full-latest-16.1.tar /usr/share/rhosp-director-images/ironic-python-agent-latest-16.1.tar; do tar -xvf $i; done

openstack overcloud image upload --image-path /home/stack/images/

openstack image list
  overcloud-full
  overcloud-full-initrd
  overcloud-full-vmlinuz

ll /var/lib/ironic/httpboot
  agent.kernel
  agent.ramdisk
  boot.ipxe
  inspector.ipxe

```

```bash
sudo dnf install -y rhosp-director-images-minimal
cd ~/images
tar xf /usr/share/rhosp-director-images/overcloud-minimal-latest-16.1.tar

openstack overcloud image upload --image-path /home/stack/images/ --os-image-name overcloud-minimal.qcow2

openstack image list
  overcloud-minimal
  overcloud-minimal-initrd
  overcloud-minimal-vmlinuz
```


## Setting a nameserver for the control plane


```bash
sudo su - stack
source ~/stackrc

openstack subnet show ctlplane-subnet -f value -c dns_nameservers
# Et si besoin : 
openstack subnet set --dns-nameserver 192.44.75.10 ctlplane-subnet
openstack subnet set --dns-nameserver 192.108.115.2 ctlplane-subnet
```


## Exemple de modification

ex : Changement de la liste des drivers supportés par Ironic

avant
```bash
$ openstack baremetal driver list
+---------------------+-----------------------+
| Supported driver(s) | Active host(s)        |
+---------------------+-----------------------+
| idrac               | localhost.localdomain |
| ilo                 | localhost.localdomain |
| ipmi                | localhost.localdomain |
| redfish             | localhost.localdomain |
+---------------------+-----------------------+
```

Modicication de undercloud.conf, ajout d'une ligne de config des drivers :
```bash
enabled_hardware_types = ipmi,redfish,idrac
```

Prise en compte
```bash
openstack undercloud install
```

Vérification
```bash
openstack baremetal driver list
+---------------------+-----------------------+
| Supported driver(s) | Active host(s)        |
+---------------------+-----------------------+
| idrac               | localhost.localdomain |
| ipmi                | localhost.localdomain |
| redfish             | localhost.localdomain |
+---------------------+-----------------------+
```

# Autres liens

* [https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf](https://www.smartembedded.com/ec/assets/rhosp_12_director_installation1521636566.pdf)
 * https://www.linuxtechi.com/install-tripleo-undercloud-centos-7/


# Debug


## Gestion des images

Lancer la commande de gestion des images individuellement
```bash
openstack tripleo container image prepare  -e containers-prepare-parameter.yaml
```

Test de recupération manuelle d'une image:
```bash
podman login -u DISI1 -p DiSi2003 https://registry.redhat.io
sudo podman login -u DISI1 -p DiSi2003 https://registry.redhat.io
```

Lister les images dispos chez REDHAT (extrait  Config avec Satellite)
```bash
sudo podman search --limit 1000 "registry.redhat.io/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | grep -v beta | sed "s/registry.redhat.io\///g" | tail -n+2 > satellite_images
```

```bash
podman  pull 10.129.172.22:5000/rhosp-rhel8/openstack-memcached:16.1-57
```

## commande lancée pour déploiement Director

```bash
Running: sudo --preserve-env 
openstack tripleo deploy \
--standalone \
--standalone-role Undercloud \
--stack undercloud \
--local-domain=localdomain \
--local-ip=10.129.173.5/24 \
--templates=/usr/share/openstack-tripleo-heat-templates/ \
--networks-file=network_data_undercloud.yaml \
--heat-native \
-e /usr/share/openstack-tripleo-heat-templates/environments/undercloud.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/use-dns-for-vips.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/podman.yaml \
-e /home/stack/containers-prepare-parameter.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/services/ironic.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/services/ironic-inspector.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/services/mistral.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/services/zaqar-swift-backend.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/disable-telemetry.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/services/tempest.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ssl/no-tls-endpoints-public-ip.yaml \
--deployment-user stack \
--output-dir=/home/stack \
--cleanup 
-e /home/stack/tripleo-config-generated-env-files/undercloud_parameters.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/tripleo-validations.yaml \
-e /home/stack/templates/custom-undercloud-params.yaml \
--log-file=install-undercloud.log \
-e /usr/share/openstack-tripleo-heat-templates/undercloud-stack-vstate-dropin.yaml
```


