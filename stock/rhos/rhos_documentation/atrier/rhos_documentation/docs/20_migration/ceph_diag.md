

depuis une machine avec CEPH monitor (os-ctrl-04)

``` bash

ceph df
GLOBAL:
    SIZE        AVAIL       RAW USED     %RAW USED 
    133 TiB     132 TiB      1.6 TiB          1.19 
POOLS:
    NAME        ID     USED        %USED     MAX AVAIL     OBJECTS 
    images      1      142 GiB      0.35        39 TiB       18304 
    volumes     2      184 GiB      0.45        39 TiB       24085 
    backups     3          0 B         0        39 TiB           0 
    metrics     4         14 B         0        39 TiB           1 
``` 

##

root@os-ctrl-04:~# ceph


ceph> health
HEALTH_WARN mon os-ctrl-04 is low on available space

ceph>  status
  cluster:
    id:     b98b9a3a-c3a1-4e9f-8ed4-377fd4ff5792
    health: HEALTH_WARN
            mon os-ctrl-04 is low on available space
 
  services:
    mon:        3 daemons, quorum os-ctrl-04,os-ctrl-05,os-ctrl-06
    mgr:        os-ctrl-04(active), standbys: os-ctrl-05, os-ctrl-06
    osd:        21 osds: 21 up, 20 in
    rbd-mirror: 3 daemons active
 
  data:
    pools:   4 pools, 832 pgs
    objects: 42.39 k objects, 326 GiB
    usage:   1.6 TiB used, 132 TiB / 133 TiB avail
    pgs:     832 active+clean
 
  io:
    client:   9.9 KiB/s wr, 0 op/s rd, 1 op/s wr
 

ceph> quorum_status
{"election_epoch":464,"quorum":[0,1,2],"quorum_names":["os-ctrl-04","os-ctrl-05","os-ctrl-06"],"quorum_leader_name":"os-ctrl-04","monmap":{"epoch":1,"fsid":"b98b9a3a-c3a1-4e9f-8ed4-377fd4ff5792","modified":"2019-01-26 19:07:23.023919","created":"2019-01-26 19:07:23.023919","features":{"persistent":["kraken","luminous","mimic","osdmap-prune"],"optional":[]},"mons":[{"rank":0,"name":"os-ctrl-04","addr":"192.168.74.24:6789/0","public_addr":"192.168.74.24:6789/0"},{"rank":1,"name":"os-ctrl-05","addr":"192.168.74.25:6789/0","public_addr":"192.168.74.25:6789/0"},{"rank":2,"name":"os-ctrl-06","addr":"192.168.74.26:6789/0","public_addr":"192.168.74.26:6789/0"}]}}


ceph>  mon_status
{"name":"os-ctrl-04","rank":0,"state":"leader","election_epoch":464,"quorum":[0,1,2],"features":{"required_con":"144115738102218752","required_mon":["kraken","luminous","mimic","osdmap-prune"],"quorum_con":"4611087854031667195","quorum_mon":["kraken","luminous","mimic","osdmap-prune"]},"outside_quorum":[],"extra_probe_peers":[],"sync_provider":[],"monmap":{"epoch":1,"fsid":"b98b9a3a-c3a1-4e9f-8ed4-377fd4ff5792","modified":"2019-01-26 19:07:23.023919","created":"2019-01-26 19:07:23.023919","features":{"persistent":["kraken","luminous","mimic","osdmap-prune"],"optional":[]},"mons":[{"rank":0,"name":"os-ctrl-04","addr":"192.168.74.24:6789/0","public_addr":"192.168.74.24:6789/0"},{"rank":1,"name":"os-ctrl-05","addr":"192.168.74.25:6789/0","public_addr":"192.168.74.25:6789/0"},{"rank":2,"name":"os-ctrl-06","addr":"192.168.74.26:6789/0","public_addr":"192.168.74.26:6789/0"}]},"feature_map":{"mon":[{"features":"0x3ffddff8ffacfffb","release":"luminous","num":1}],"osd":[{"features":"0x3ffddff8ffa4fffb","release":"luminous","num":5}],"client":[{"features":"0x3ffddff8ffa4fffb","release":"luminous","num":2},{"features":"0x3ffddff8ffacfffb","release":"luminous","num":27}],"mgr":[{"features":"0x3ffddff8ffacfffb","release":"luminous","num":1}]}}


## Ménage

Lister les volumes dans CEPH
```bash
rbd -p volumes  ls -l 
```



 rbd rm volumes/volume-b73f8e85-c60f-4ffa-bb41-65708478f578
 2044  rbd rm volumes/volume-19b42e3e-62c8-42b0-abb5-395439476079
 2045  rbd rm volumes/volume-490986c9-0006-4291-a6f7-1eec3a8439e5
 2046  rbd snap ls volumes/volume-490986c9-0006-4291-a6f7-1eec3a8439e5
 2047  rbd snap unprotect volumes/volume-490986c9-0006-4291-a6f7-1eec3a8439e5@snapshot-0200b453-fb89-4d3a-b9cb-7e1bf62d7d03
 2048  rbd snap rm volumes/volume-490986c9-0006-4291-a6f7-1eec3a8439e5@snapshot-0200b453-fb89-4d3a-b9cb-7e1bf62d7d03
 2049  rbd rm volumes/volume-490986c9-0006-4291-a6f7-1eec3a8439e5
 2050  history
 2051  rbd rm volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69
 2052  rbd snap ls volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69
 2053  rbd snap unprotect volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69@snapshot-0200b453-fb89-4d3a-b9cb-7e1bf62d7d03
 2054  rbd snap unprotect volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69@snapshot-f1abe391-634b-4e8d-966b-0832fbbbda16
 2055  rbd snap rm volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69@snapshot-f1abe391-634b-4e8d-966b-0832fbbbda16
 2056  rbd rm volumes/volume-d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69


# ---
Name Docker-recette
ID 19b42e3e-62c8-42b0-abb5-395439476079

# ---
Name datas-01
ID b73f8e85-c60f-4ffa-bb41-65708478f578
@snapshot-67b9cc99-5b9f-4351-a8e6-3bd9782e033b

# ---
Name mounir_volume
ID 490986c9-0006-4291-a6f7-1eec3a8439e5
@snapshot-0200b453-fb89-4d3a-b9cb-7e1bf62d7d03

# ---
Name d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69
ID d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69
@snapshot-f1abe391-634b-4e8d-966b-0832fbbbda16