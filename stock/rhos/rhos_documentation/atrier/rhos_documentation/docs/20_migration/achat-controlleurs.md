

	BKDQYR2  Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz  Date d’expiration 19 SEPT. 2023
        BKJLYR2  Intel(R) Xeon(R) Silver 4116 CPU @ 2.10GHz  Date d’expiration 19 SEPT. 2023


Processeur additionnel Intel Xeon Silver 4214Y 2.2GHz, 16.5M Cache,9.60GT/s,  2UPI, Turbo, HT,12C/24T (85W) - DDR4-2400  + 825.00 €
Processeur additionnel Intel Xeon Silver 4216  2.1GHz, 22M   Cache,9.60GT/s,  2UPI, Turbo, HT,16C/32T (100W) - DDR4-2400   + 1008.00 €
Processeur additionnel Intel Xeon Gold 5218R   2.1GHz, 27,5M Cache,10.40GT/s, 2UPI, Turbo, HT,20C/40T (125W) - DDR4-2666  + 1300.00 €

Ajout Transceiver 10 Gb SR SFP+ pour les cartes 10 Gb Intel
+ 90.00 €

1
Dell Networking Câble Direct Attach Twinaxial SFP+ DELL 3 mètres
+ 25.00 €

1
Dell Networking Câble Direct Attach Twinaxial SFP28 (25G) DELL 3 mètres
+ 105.00 €



4 512,00 €
Total éco-contribution :	0,00 €
Total HT éco-contribution incluse :	4 512,00 €
Montant HT soumis à la TVA :	4 512,00 €
Montant HT non soumis à la TVA :	0,00 €
Total TVA :	902,40 €
Montant total TTC :	5 414,40 €