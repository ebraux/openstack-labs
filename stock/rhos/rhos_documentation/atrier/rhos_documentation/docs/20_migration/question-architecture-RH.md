


# Question

## architecture- virtualisation

Exemple, info de config réseau pour utiliser Director dans une VM.
Pb idem idem pour ceph management et Satellite.

cohabitation Director  /ceph mahagemennt / Satellite possible ?

## getsion des licences 

Comment se passe le renouvellement (changement de no de pool ?)
que peut-on faire sans licence : installtion, mise à jour ?




## Director

### déploiement

* Intégration en tant que VM dans RHV ?

### utilisation
* On peut aussi déployer des noeuds "Baremetal" indépendament d'Openstack ? (ex cluster Ceph, cluster RHV, cluster k8s)
  * objectif remplacer nos outils de dépoiement tftp/pxe actuels, gérer tous les serveurs avec le même
  * si oui, possibilité d'avoir plusieurs réseaux de provisionning ?

* comment on le sécurise ?

* quelle consomation en ressources RAM/CPU/Réseau ? assez faible en dehors de la phase de déploiement ? (VM dans Vsphere ?)
Thibault (RH) : encouragement fort à passer par Ansible (pour le build de la plate-forme) et Director embarque les scripts nécessaires.
Ansible Engine est intégré dans Director (mais pas Ansible Tower, à vérifier)

### dans fichier de config, activation de l'UI ?
 c'est quoi l'UI


## CEPH :

### acceder à une regitry "insecure"

Comment activer l'utilisation d'une registry local dans la config :

 actuellement : 
  * déclaration de la regitry dans le fichier de config
  * mais le playbook installe docker, sans activer l'accès à cette regitry locale.
  * obligation de modifier manuellement la config de Docker, pour autoriser cette regitry "insecure"
 Piste : 
   * ajouter la déclaration de la regitry quelque part avec ceph-ansible 
   * rendre la registry secur ean ajoutant la CA.

### confiogurer l'accès

machine avec une IP publique, et une IP dans le réseaue de provisionning.
Dans la config, grafana accès limité à 1 IP. provisionnog par défaut.
cahger vers IP publique ?


### info de version

version de CEPH :  
docker exec ceph-mon-ceph-poc-01 ceph -v
ceph version 14.2.8-81.el8cp (0336e23b7404496341b988c8057538b8185ca5ec) nautilus (stable)
--> octopus ?
c'est RED HAT CEPH STORAGE v5 ?

## Openstack
Configurer l'affectation des bridges à des interfaces spécifiques.
Dans la doc, c'est indiqué que c'est par défaut, dans l'ordre de déclaration du fichier network_datas
Mais pas d'info sur comment personnaliser cette affectation.

Ajout du support de Manilla dans l'undercloud


## scénario

Comment gérer CEPH ?
possibilité de le faire évoluer ?
* config basique avec des R710 dans un premier temps, puis remplacement par les R740
* passer l'infra actuelle NFS sur un des neouds, + en GlusterFS pour le backup des données. 
  * glance en local avec Gluster : OK
  * cinder ?

possible de faire cohabiter plusiseurs cluster sur la même architecture (possible avec l'utilisation de containers) ? 
