

## prépartion de l'instance de prod actuelle

principe : on désactives tous les services des noeuds "os-crtl-05" et "os-ctrl-06", pour ne laisser que "os-ctrl-04" actif.
On reconfigure le stockage de glance, pour que ce soit en local.


Quand memcache n'a plus qu'une seul instance, keystone rame. On doit donc modifier la config de keystone pour ne se connecter qu'au seul serveur memcache local. on en profite pour faire pareil pour RabittMq
* os-ctrl-04_memcached_container-fa800f3e : 192.168.73.91
* os-ctrl-04_rabbit_mq_container-891b027d : 192.168.73.232

Sur :
* keystone
* cinder_api
* cinder_volumes
* glance
* horizon
* neutron_agents
* neutron-server
* nova-api


Désactivation du redémarrage auto des containers LXC

```bash
cd /var/lib/lxc
for TOTO in `ls -1` ; do \
  echo $TOTO;
  sed -i -e "s/^lxc.start.auto = 1/lxc.start.auto = 0/g" $TOTO/config; \
  sed -i -e "s/^lxc.group = onboot/#lxc.group = onboot/g" $TOTO/config; \
  cat $TOTO/config; \
done
```

# Sur les computes

Modif de la config pour pointer sur un seul memcache/rabbit

```bash
/etc/nova/nova.conf
systemctl restart nova-*
tail -f /var/log/nova/nova-compute.log

/etc/neutron/neutron.conf
systemctl restart neutron-*
tail -f /var/log/neutron/neutron-openvswitch-agent.log
```

Désactivation de la télémétrie

```bash
systemctl stop telegraf ceilometer-*
systemctl disable telegraf ceilometer-*
```


## Desactivation de CEPH / Transferts des images

### Principe

On utilise le drivers "Filsystem de glance".

Pour info, si on faisait du NFS, on faudrait monter le volume NFS au niveau du système, et l'utiliser comme un volume local

On peu directement exporter les images depuis CEPH, avec un 'rbd export'. Mais on ne peut pas se contenter de les copier dans le nouvel espace disque, car 

Elles sont directement en format qcow. Donc on les exporte, on les supprime d'opensatck, et on les ré-importe avec le même nom. Elle seront stockée dans le nouveau filesystem. leur ID chnage, mais en général les utilisateurs se basent sur le nom.


### config du container pour accèder à un dossier local
 
```bash
 /var/lib/lxd ...
 redémarrage
```

### Configuration de glance pour le drivers Filesystem

```bash
latt glance
```

Config glance dans le container : /etc/glance/glance-api.conf

```bash
[glance_store]
default_store = file

stores = file,rbd

filesystem_store_datadir = /local-storage/images

rbd_store_pool = images
rbd_store_user = glance
rbd_store_ceph_conf = /etc/ceph/ceph.conf
rbd_store_chunk_size = 8
```

Redémarrage du container

```bash
tail -f /var/log/glance/glance-api.log

```

### suivi des manips


```bash
openstack image list  --long -c ID -c Name -c Size -c Visibility -c Project
+--------------------------------------+----------------------------+-------------+------------+----------------------------------+
| ID                                   | Name                       |        Size | Visibility | Project                          |
+--------------------------------------+----------------------------+-------------+------------+----------------------------------+
OK| 2ad23fc5-fc03-4b51-95e1-24e74943b005 | Arwa_image                 |  4345495552 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
| c82b0da1-a33f-4004-867c-6aadf3370681 | MYSERVER-shelved           |  3198681088 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| e119a456-9410-4d2a-b888-ce0e2c733a11 | Maryam-ILS-1               |  4998758400 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
| ee0a8173-bf24-4d17-8466-b8eb58d4e491 | Maryam-ILS-2               |  5127929856 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
| 0e160f2a-fc00-4eb2-a232-e4f940229625 | Maryam-ILS-3               |  5035327488 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
| 045ea1b0-1e50-4868-b847-6b43b467bb93 | Maryam-ILS-4               |  5066129408 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
OK| ccb175f2-4394-488a-8d99-51b729368fe4 | arthur-m                   |  5003345920 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
OK| 9da07ef3-a37b-4f8f-9213-3323e5648c2e | cirros                     |    12716032 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| ce39ed06-7921-4ae5-8fd6-af383871863d | debian-8-current           |   687223296 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| 723b41d0-fb5e-4667-8546-4c20d876e805 | debian-9-current           |   580867584 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| b8e615b5-302e-4e11-9cb4-0b15e2fae5a7 | easyobs-01-shelved         |  5007409152 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| 5b86dd39-6264-400a-ada7-d007784e5289 | imta-ubuntu-basic          |   611057664 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 223e51ba-156d-44a9-b7a8-5cfa9715e69a | imta-ubuntu-docker         |   885166080 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 6d7884be-d910-402b-8a28-a1ef99da3761 | imta-ubuntu-osmanager      |  1024898560 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 10f79544-aafa-49ce-b947-3a46c001db8d | imta_ubuntu_docker_model   |  2373910528 | private    | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 232f77a1-6463-4eb3-a15a-35792e4131f9 | k8s-master-shelved         | 10025959424 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| daf5253d-c3a1-436c-b407-156883e20c6b | k8s-worker-2-shelved       |  9986310144 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 5a157325-80e6-49ea-9196-9778d5778a43 | lb-apache-shelved          |  2998796288 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| 174ddc43-8c49-48d9-aef3-1a58f97c517c | lbrisson-dascci-2020-redis | 20836712448 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
OK| eed2df61-909d-493d-935e-451d57f4b555 | lbrisson-nosql-elastic     | 10170662912 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
OK| 08db4a61-6e62-457f-bfb2-c3122b0a6c9d | lbrisson-nosql-redis       | 10078257152 | private    | fb680bdbc69a4966a02569bd6e2676b3 | decide
| 7ab8ea5a-6501-427e-815a-89edf7a0aaa4 | migration-test-image       | 10087890944 | private    | 1cfd80e89d5f493894e9a15840bc3bb0 | ebraux_perso
| f567bc15-f303-4bf8-af97-ec0e4c3a3fdf | nosql-redis-shelved        | 10078257152 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| e25d7acc-5b5a-4a64-87d0-ab8127186218 | overleaf-test-2019_CLI     | 13836091392 | shared     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
| e890a811-410d-4694-925c-cd5343a810c9 | redis_backup_2020          | 20836712448 | private    | b89f7765ee4a409f890de7b93b41e5d1 |
| c0598b57-cc9c-4d5b-90aa-e7ef25dd606f | snapshot-sbigaret-test-2   |  1423441920 | private    | d999ea47a245432b9f09542e808099f0 |
| 5256905a-ceca-47de-952b-30e877a9cde0 | ubuntu_14.04_current       |   263717376 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| 9a9a6f09-7e88-4fa5-b56f-650a109821d7 | ubuntu_16.0.4_current      |   296747008 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| d601281b-d1ff-420b-8d32-aeebb280a703 | ubuntu_18.0.4_current      |   344457216 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
OK| 90064820-3ca2-4170-a9c6-c95ba2cc563d | ubuntu_18_current          |   339542016 | public     | dbdddc0e1d1f4fa4a54eae49b2e577c9 | admin
+--------------------------------------+----------------------------+-------------+------------+----------------------------------+
```

```bash
openstack image create \
  --disk-format qcow2 \
  --container-format bare \
  --public \
  --file 9da07ef3-a37b-4f8f-9213-3323e5648c2e \
  cirros
```

``` bash
openstack image create \
  --disk-format qcow2 \
  --container-format bare \
  --private \
  --project decide \
  --file 2ad23fc5-fc03-4b51-95e1-24e74943b005 \
  Arwa_image
```

``` bash
scp root@os-ctrl-04.priv:/local-storage/migration/glance/rbd_export/eed2df61-909d-493d-935e-451d57f4b555 .
openstack image delete lbrisson-nosql-elastic
openstack image create   --disk-format qcow2   --container-format bare   --private --project decide --file eed2df61-909d-493d-935e-451d57f4b555 lbrisson-nosql-elastic
rm eed2df61-909d-493d-935e-451d57f4b555
```

## Desactivation de CEPH / Transfert des snapshots

### dans Mysql


Suppression d'un snapshot
```console


Ex de correction du status d'une instance : `task_state` à passer de `rebooting`  à `NULL`
```language
SELECT *  FROM volumes  WHERE uuid = <VOLUME_ID>;

UPDATE instances SET task_state = NULL WHERE uuid = '<INSTANCE_ID>';

SELECT display_name,vm_state,task_state FROM instances WHERE uuid = <INSTANCE_ID>;

delete from volumes where id="aaaaaaaaaaaa"; ERROR 1451 (23000): cannot delete or update a parent row: a foreign key constraint fails ('cinder'.'iscsi_targets', CONSTRAINT 'iscsi_targets_ibfk_1' FOREIGN KEY('volume_id') REFERENCES 'volumes' ('id'))

> mysql -u root cinder

19b42e3e-62c8-42b0-abb5-395439476079

mysql> delete from snapshots where id='xxxxxxx-xxx-xxx-xxx-xxxxxxxxxx'

mysql> delete from volumes where id='xxxxxxx-xxx-xxx-xxx-xxxxxxxxxx'
```

* https://ask.openstack.org/en/question/1378/cannot-remove-volume/
* https://mcwhirter.com.au/craige/blog/2015/Cinder_Cannot_Delete_Volume_-_volume_is_busy_With_Ceph_Block_Storage/

### Dans CEPH


### suivi des manips de ménage des snapshots


``` bash
openstack volume snapshot list --all
+--------------------------------------+--------------------------------------+-------------+----------+------+
| ID                                   | Name                                 | Description | Status   | Size |
+--------------------------------------+--------------------------------------+-------------+----------+------+
| 4cc3495c-5a18-4e95-a8f8-300df9ec12cf | mounir_temporalClustering_snapshot_2 |             | deleting |   10 |
| 581bb2e3-3df0-4231-8ad9-17c49bf5b029 | mounir_temporalClustering_snapshot_3 |             | deleting |   10 |
| ced5d85e-0dbe-4010-ad24-e33de9a03ddd | mounir_temporalClustering_snapshot_1 |             | deleting |   10 |
| d7bd6ed6-be56-4298-857e-1226b864506d | mounir_temporalClustering_snapshot_5 |             | deleting |   10 |
| 67b9cc99-5b9f-4351-a8e6-3bd9782e033b | mounir_temporalClustering_snapshot_4 |             | deleting |   10 |
| f1abe391-634b-4e8d-966b-0832fbbbda16 | mounir_temporalClustering_snapshot_6 |             | deleting |   10 |
+--------------------------------------+--------------------------------------+-------------+----------+------+

a6797ee6-51e1-4b84-b67c-d3761506e272   snapshot for mounir_instantanne_test1


 openstack volume snapshot list --all --long
+--------------------------------------+--------------------------------------+-------------+----------+------+----------------------------+--------------------------------------+------------+
| ID                                   | Name                                 | Description | Status   | Size | Created At                 | Volume                               | Properties |
+--------------------------------------+--------------------------------------+-------------+----------+------+----------------------------+--------------------------------------+------------+
| 4cc3495c-5a18-4e95-a8f8-300df9ec12cf | mounir_temporalClustering_snapshot_2 |             | deleting |   10 | 2019-08-07T10:35:07.000000 | 4222a516-f6ae-4e7d-9004-e6992a690572 |            |
| 581bb2e3-3df0-4231-8ad9-17c49bf5b029 | mounir_temporalClustering_snapshot_3 |             | deleting |   10 | 2019-08-07T10:34:39.000000 | 5cf9a571-4c32-41c9-9c5c-e1132793f927 |            |
| ced5d85e-0dbe-4010-ad24-e33de9a03ddd | mounir_temporalClustering_snapshot_1 |             | deleting |   10 | 2019-08-07T10:34:03.000000 | b3206f73-5fcc-450d-8ebe-9ddec6e06dca |            |
| d7bd6ed6-be56-4298-857e-1226b864506d | mounir_temporalClustering_snapshot_5 |             | deleting |   10 | 2019-08-07T10:33:37.000000 | 4a1cba99-b119-4ed6-861f-fa62f0560802 |            |
| 67b9cc99-5b9f-4351-a8e6-3bd9782e033b | mounir_temporalClustering_snapshot_4 |             | deleting |   10 | 2019-08-07T10:33:01.000000 | f787c33e-97da-4ae8-9f02-487d4cdd84f2 |            |
| f1abe391-634b-4e8d-966b-0832fbbbda16 | mounir_temporalClustering_snapshot_6 |             | deleting |   10 | 2019-08-07T10:32:28.000000 | d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69 |            |
+--------------------------------------+--------------------------------------+-------------+----------+------+----------------------------+--------------------------------------+------------+


 select id,display_name,volume_id from snapshots;
+--------------------------------------+---------------------------------------+--------------------------------------+
| id                                   | display_name                          | volume_id                            |
+--------------------------------------+---------------------------------------+--------------------------------------+
| 0200b453-fb89-4d3a-b9cb-7e1bf62d7d03 | mounir_volume_snapshotBckp            | 490986c9-0006-4291-a6f7-1eec3a8439e5 |
| 4cc3495c-5a18-4e95-a8f8-300df9ec12cf | mounir_temporalClustering_snapshot_2  | 4222a516-f6ae-4e7d-9004-e6992a690572 |
| 581bb2e3-3df0-4231-8ad9-17c49bf5b029 | mounir_temporalClustering_snapshot_3  | 5cf9a571-4c32-41c9-9c5c-e1132793f927 |
| 67b9cc99-5b9f-4351-a8e6-3bd9782e033b | mounir_temporalClustering_snapshot_4  | f787c33e-97da-4ae8-9f02-487d4cdd84f2 |
| a6797ee6-51e1-4b84-b67c-d3761506e272 | snapshot for mounir_instantanne_test1 | a7d5c552-8b50-4028-921b-f5cd19bff8b0 |
| ced5d85e-0dbe-4010-ad24-e33de9a03ddd | mounir_temporalClustering_snapshot_1  | b3206f73-5fcc-450d-8ebe-9ddec6e06dca |
| d7bd6ed6-be56-4298-857e-1226b864506d | mounir_temporalClustering_snapshot_5  | 4a1cba99-b119-4ed6-861f-fa62f0560802 |
| f1abe391-634b-4e8d-966b-0832fbbbda16 | mounir_temporalClustering_snapshot_6  | d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69 |
+--------------------------------------+---------------------------------------+--------------------------------------+
```


## Desactivation de CEPH / Transferts des volumes


### Principe 

Cinder ne sait pas fonctionner avec un espace disque local. Il lui faut :

 * soit un accès à un volume LVM
 * soit un accès NFS.

 C'est compliqué la getsion d'un accès direct à un volume LVM pour un container LXC, alors on passe par NFS.
 export d'un volume de os-ctrl-04 via NFS

On ne peut pas utiliser la fonctinnalité de backup, qui permet de récupérer une "image" du volume dans un system de fichier à part, car cinder-backup n'a pas été activé lors du déploiement.

On va exporter les volumes directement depuis dans le pool CEPH avec un "rbd export", et les réimporter ensuite.


### Installation du serveur NFS

* https://www.tecmint.com/install-nfs-server-on-ubuntu/

```bash
sudo apt update
sudo apt install nfs-kernel-server
```

Création des dossiers

```bash
mkdir -p /local-storage/nfs_share/cinder/volumes
mkdir -p /local-storage/nfs_share/cinder/backup
chown -R nobody:nogroup /local-storage/nfs_share
chmod 777  /local-storage/nfs_share/cinder/volumes
chmod 777  /local-storage/nfs_share/cinder/backup
```

Fichier de config /etc/exports

```bash
/local-storage/nfs_share/cinder/volumes   192.168.73.0/24(rw,sync,no_subtree_check)
/local-storage/nfs_share/cinder/volumes   10.29.241.0/24(rw,sync,no_subtree_check)
/local-storage/nfs_share/cinder/backup    192.168.73.0/24(rw,sync,no_subtree_check)
/local-storage/nfs_share/cinder/backup    10.29.241.0/24(rw,sync,no_subtree_check)
```

Activation

```bash
systemctl restart nfs-kernel-server
sudo exportfs -a
```

Test en local

```bash
apt install nfs-common

mount 192.168.73.24:/local-storage/nfs_share/cinder/volumes /mnt
touch /mnt/local-volumes.txt
ls /mnt
umount /mnt
ll /local-storage/nfs_share/cinder/volumes

mount 192.168.73.24:/local-storage/nfs_share/cinder/backup /mnt
touch /mnt/local-backup.txt
ls /mnt
umount /mnt
ll /local-storage/nfs_share/cinder/backup
```

### Configuration de cinder pour le drivers NFS pour cinder

Test d'acces NFS puis le container

```bash
latt cinder_v

apt install nfs-common

mount 192.168.73.24:/local-storage/nfs_share/cinder/volumes /mnt
touch /mnt/cincer_container-volumes.txt
ls /mnt
umount /mnt


mount 192.168.73.24:/local-storage/nfs_share/cinder/backup /mnt
touch /mnt/cincer_container-backup.txt
ls /mnt
umount /mnt

```


Config nfs pour cinder, dans le container : /etc/cinder/nfs_shares

```bash
192.168.73.24:/local-storage/nfs_share/cinder/volumes
```

Config nfs pour cinder, dans le container : /etc/cinder/cinder.conf

```bash
[DEFAULT]
...
enabled_backends=LOCAL,RDB
default_volume_type=LOCAL

backup_driver=cinder.backup.drivers.nfs
backup_share=192.168.73.24:/local-storage/nfs_share/cinder/backup
...

[LOCAL]
nfs_shares_config = /etc/cinder/nfs_shares
volume_backend_name = localbackend
volume_driver = cinder.volume.drivers.nfs.NfsDriver
nfs_mount_options =
```

Redémmarer le container
Config nfs pour cinder, dans le container : /etc/cinder/nfs_share

```bash
lxc-stop ...
lxc-start ...
```


### Configuration du type de volume dans Openstack

Depuis machine os-admin

```bash
openstack volume type create LOCAL
openstack volume type set LOCAL  --property volume_backend_name=LOCAL

openstack volume type list
+--------------------------------------+-------+-----------+
| ID                                   | Name  | Is Public |
+--------------------------------------+-------+-----------+
| 597eb151-e5b0-48c0-a1ee-3472fe4a52de | LOCAL | True      |
| 080d5989-3f83-43f9-a1e0-6e86753b6256 | RBD   | True      |
+--------------------------------------+-------+-----------+

openstack volume type show LOCAL
+--------------------+--------------------------------------+
| Field              | Value                                |
+--------------------+--------------------------------------+
| access_project_ids | None                                 |
| description        | None                                 |
| id                 | 597eb151-e5b0-48c0-a1ee-3472fe4a52de |
| is_public          | True                                 |
| name               | LOCAL                                |
| properties         | volume_backend_name='localbackend'   |
| qos_specs_id       | None                                 |
+--------------------+--------------------------------------+
```




### Lister les infos sur les volumes

#### Dans mysql

```console
# se connecter sur le container mairaiDB d'un un des controller
ssh os-ctrl-04.priv
latt galera
mysql
> use cinder;

> describe volume_glance_metadata;
> select id,volume_id,snapshot_id from volume_glance_metadata where not volume_id like 'NULL' and not snapshot_id like 'NULL';

> describe volume_attachment;
> select id,volume_id,attached_host,instance_uuid,mountpoint,attach_status from volume_attachment where attach_status NOT like 'detached';

> DESCRIBE snapshots;
> select id,display_name,volume_id from snapshots;

> DESCRIBE volumes;

> SELECT id,display_name,status FROM volumes where status not like 'deleted';
> SELECT id,display_name,status,attach_status,snapshot_id FROM volumes where status not like 'deleted';

```

#### Dans ceph

```console
rbd -p volumes ls -l
```

### Suppression d'un volume "490986c9-0006-4291-a6f7-1eec3a8439e5"

#### Dans Myqsl

```bash
> select id,volume_id,attached_host,instance_uuid,mountpoint,attach_status from volume_attachment where volume_id like '490986c9-0006-4291-a6f7-1eec3a8439e5';
> delete from volume_attachment where volume_id like '490986c9-0006-4291-a6f7-1eec3a8439e5';

> select id,display_name,volume_id from snapshots where volume_id like '490986c9-0006-4291-a6f7-1eec3a8439e5';
+--------------------------------------+----------------------------+--------------------------------------+
| id                                   | display_name               | volume_id                            |
+--------------------------------------+----------------------------+--------------------------------------+
| 0200b453-fb89-4d3a-b9cb-7e1bf62d7d03 | mounir_volume_snapshotBckp | 490986c9-0006-4291-a6f7-1eec3a8439e5 |
+--------------------------------------+----------------------------+--------------------------------------+

> select id,volume_id,snapshot_id from volume_glance_metadata where like snapshot_id like '4cc3495c-5a18-4e95-a8f8-300df9ec12cf';
> delete  from volume_glance_metadata where snapshot_id like '4cc3495c-5a18-4e95-a8f8-300df9ec12cf';

> delete from snapshots where id='0200b453-fb89-4d3a-b9cb-7e1bf62d7d03';

> delete from volumes where id='490986c9-0006-4291-a6f7-1eec3a8439e5';
```

#### Dans CEPH
```bash

rdb -p volumes ls -l

rbd snap purge volumes/volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81

# [opt]  - si il y a des snapshot bloquants, les déproteger et refaire le snap purge
rbd snap unprotect volumes/volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81@snapshot-d7bd6ed6-be56-4298-857e-1226b864506d
# [opt]  - si il y a des locks, les supprimer  et refaire le snap purge
rbd lock ls --pool volumes volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81
rbd lock remove --pool  volumes volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81 "auto 140680580042688" client.33257248 

rbd rm volumes/volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81

```

### Suivi des manips de ménage dans les volumes


```bash
# dans mysl
delete from volume_attachment where volume_id like 'b2a1c257-610d-41b7-8091-30ad0a96b254';
delete from snapshots where volume_id='b2a1c257-610d-41b7-8091-30ad0a96b254';
delete from volume_glance_metadata  where volume_id like 'b2a1c257-610d-41b7-8091-30ad0a96b254';
delete from volume_admin_metadata where volume_id like 'b2a1c257-610d-41b7-8091-30ad0a96b254';
delete from volumes where id='b2a1c257-610d-41b7-8091-30ad0a96b254';

# dans ceph

rbd --pool volumes info volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a

## snapshot
rbd snap purge volumes/volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a

# [opt]  - si il y a des snapshot bloquants, les déproteger et refaire le snap purge
rbd snap unprotect volumes/volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81@snapshot-d7bd6ed6-be56-4298-857e-1226b864506d
# [opt]  - si il y a des locks, les supprimer  et refaire le snap purge

## lock 
rbd lock ls --pool volumes volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a
rbd lock remove --pool  volumes volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a "auto 140680580042688" client.33257248 

## watcher
rbd status volumes/volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a
# [] affichie la liste deswatchers
ceph osd blacklist ls
ceph osd blacklist add 192.168.74.119:0/1804234782
rbd rm volumes/volume-a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a
ceph osd blacklist ls
ceph osd blacklist rm 192.168.74.119:0/1804234782

# Ménage dans les volumes

openstack volume list --all 
+--------------------------------------+------------------------------------------+-----------+------+---------------------------------------------------------------+
| ID                                   | Display Name                             | Status    | Size | Attached to                                                   |
+--------------------------------------+------------------------------------------+-----------+------+---------------------------------------------------------------+
| 97a1da43-b556-454d-b338-89bcf2d252a2 | backup_gitlab_local                      | in-use    |   40 | Attached to d988cdca-919b-4254-a690-4060df048356 on /dev/vdc  |
| 4394b089-e5e7-4f8c-8dfb-6e92a05dcc30 | test2                                    | available |    1 |                                                               |
| 371a7f3c-6c30-4ab9-9f78-aaadbeaabf75 | test-backup                              | available |    1 |                                                               |
| ec735f63-aabe-44e4-bd41-2db0501c6324 | pvc-dfc7d6fb-61ca-4b21-afe6-68ad4cee1d83 | in-use    |    1 | Attached to 86c6c646-5a7f-45b2-a683-cd4701c682d6 on /dev/vdc  |
| 1487c31b-03ba-4425-8b3b-04c37ef015c6 | pvc-014d3261-1777-4772-8759-b146f0b1a982 | in-use    |    1 | Attached to 86c6c646-5a7f-45b2-a683-cd4701c682d6 on /dev/vdb  |
| 3321935d-7682-4be0-a79b-94f05835ebe6 | pvc-46181013-8623-4547-a4ce-231de079f193 | in-use    |    1 | Attached to 8da9f9e7-649a-46fe-ba45-508b94333e14 on /dev/vdb  |
| dfd5e624-9ad1-43cb-939d-36c988ee8b81 |                                          | in-use    |   10 | Attached to d7baa5d1-522b-4770-98c1-675f76f08104 on /dev/vda  |
| 7c7a70d5-a6cd-443d-b840-0f0bbf5beef0 | anr-pil-data                             | in-use    |   20 | Attached to 1e7eafe3-9bfb-4ff7-a71f-9ea98eb0c2a6 on /dev/vdb  |
| b2a1c257-610d-41b7-8091-30ad0a96b254 |                                          | in-use    |   20 | Attached to 1e7eafe3-9bfb-4ff7-a71f-9ea98eb0c2a6 on /dev/vda  |
OK| 9dd5fc2c-92c4-45a0-a023-f8fb8310f75d |                                          | in-use    |   15 | Attached to 9d585d5c-ceaa-4bef-a43b-1b5a7423a41b on /dev/vda  |
OK| 19b42e3e-62c8-42b0-abb5-395439476079 | Docker-recette                           | deleting  |    5 |                                                               |
OK| a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a | backup_gitlab                            | in-use    |   40 | Attached to d988cdca-919b-4254-a690-4060df048356 on /dev/vdb  |
OK| b73f8e85-c60f-4ffa-bb41-65708478f578 | datas-01                                 | deleting  |  300 |                                                               |
OK| d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69 |                                          | available |   10 |                                                               |
OK| f787c33e-97da-4ae8-9f02-487d4cdd84f2 |                                          | available |   10 |                                                               |
OK| 4a1cba99-b119-4ed6-861f-fa62f0560802 |                                          | available |   10 |                                                               |
OK| b3206f73-5fcc-450d-8ebe-9ddec6e06dca |                                          | available |   10 |                                                               |
OK| 5cf9a571-4c32-41c9-9c5c-e1132793f927 |                                          | available |   10 |                                                               |
OK| 4222a516-f6ae-4e7d-9004-e6992a690572 |                                          | available |   10 |                                                               |
| 09355ed7-e127-4fa0-b81e-83a9d766ff91 | ressources                               | in-use    |   30 | Attached to d626e698-aef1-4d05-bafd-00a82edafe90 on /dev/vdb  |
OK| 22f0227e-e38e-4fe3-b487-2f9bba88bffe | backup-config                            | detaching |    5 | Attached to f851ae1c-9abf-4128-a94f-9f2e22629edc on /dev/vdb  |
| f9cf8d9d-500a-4422-b16e-5117bcd6741a | backup-docker-volumes                    | detaching |   50 | Attached to f851ae1c-9abf-4128-a94f-9f2e22629edc on /dev/vdc  |
| f835f294-80b0-4c70-8444-c18dd4066285 | code-source                              | in-use    |    5 | Attached to b720fc2e-119e-4f95-a343-f275677498ef on /dev/vdb  |
| 8329bf61-61d2-4f39-bb7a-7995e96c4b04 | taf-datas                                | in-use    |   30 | Attached to adead1df-041b-4e0a-823a-48a2ab664f33 on /dev/vdb  |
+--------------------------------------+------------------------------------------+-----------+------+---------------------------------------------------------------+


volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81

openstack volume list --all --long
+--------------------------------------+------------------------------------------+-----------+------+-------+----------+---------------------------------------------+-----------------------------------------------+
| ID                                   | Display Name                             | Status    | Size | Type  | Bootable | Attached to                                 | Properties                                    |
+--------------------------------------+------------------------------------------+-----------+------+-------+----------+---------------------------------------------+-----------------------------------------------+
| 97a1da43-b556-454d-b338-89bcf2d252a2 | backup_gitlab_local                      | in-use    |   40 | LOCAL | false    | Attached to d988cdca-                       |                                               |
|                                      |                                          |           |      |       |          | 919b-4254-a690-4060df048356 on /dev/vdc     |                                               |
| 4394b089-e5e7-4f8c-8dfb-6e92a05dcc30 | test2                                    | available |    1 | None  | false    |                                             |                                               |
| 371a7f3c-6c30-4ab9-9f78-aaadbeaabf75 | test-backup                              | available |    1 | LOCAL | false    |                                             |                                               |
| ec735f63-aabe-44e4-bd41-2db0501c6324 | pvc-dfc7d6fb-61ca-4b21-afe6-68ad4cee1d83 | in-use    |    1 | None  | false    | Attached to 86c6c646-5a7f-                  | cinder.csi.openstack.org/cluster='kubernetes' |
|                                      |                                          |           |      |       |          | 45b2-a683-cd4701c682d6 on /dev/vdc          |                                               |
| 1487c31b-03ba-4425-8b3b-04c37ef015c6 | pvc-014d3261-1777-4772-8759-b146f0b1a982 | in-use    |    1 | None  | false    | Attached to 86c6c646-5a7f-                  | cinder.csi.openstack.org/cluster='kubernetes' |
|                                      |                                          |           |      |       |          | 45b2-a683-cd4701c682d6 on /dev/vdb          |                                               |
| 3321935d-7682-4be0-a79b-94f05835ebe6 | pvc-46181013-8623-4547-a4ce-231de079f193 | in-use    |    1 | None  | false    | Attached to 8da9f9e7-649a-46fe-             | cinder.csi.openstack.org/cluster='kubernetes' |
|                                      |                                          |           |      |       |          | ba45-508b94333e14 on /dev/vdb               |                                               |
| dfd5e624-9ad1-43cb-939d-36c988ee8b81 |                                          | in-use    |   10 | None  | true     | Attached to                                 | attached_mode='rw', readonly='False'          |
|                                      |                                          |           |      |       |          | d7baa5d1-522b-4770-98c1-675f76f08104 on     |                                               |
|                                      |                                          |           |      |       |          | /dev/vda                                    |                                               |
| 7c7a70d5-a6cd-443d-b840-0f0bbf5beef0 | anr-pil-data                             | in-use    |   20 | None  | false    | Attached to 1e7eafe3-9bfb-4ff7-a71f-        |                                               |
|                                      |                                          |           |      |       |          | 9ea98eb0c2a6 on /dev/vdb                    |                                               |
| b2a1c257-610d-41b7-8091-30ad0a96b254 |                                          | in-use    |   20 | None  | true     | Attached to 1e7eafe3-9bfb-4ff7-a71f-        | attached_mode='rw', readonly='False'          |
|                                      |                                          |           |      |       |          | 9ea98eb0c2a6 on /dev/vda                    |                                               |
| 9dd5fc2c-92c4-45a0-a023-f8fb8310f75d |                                          | in-use    |   15 | None  | true     | Attached to 9d585d5c-ceaa-4bef-a43b-        | attached_mode='rw', readonly='False'          |
|                                      |                                          |           |      |       |          | 1b5a7423a41b on /dev/vda                    |                                               |
| 19b42e3e-62c8-42b0-abb5-395439476079 | Docker-recette                           | deleting  |    5 | None  | false    |                                             |                                               |
| a948f8eb-b7ef-42a7-a02a-c2ed86dcfc4a | backup_gitlab                            | in-use    |   40 | None  | false    | Attached to d988cdca-                       |                                               |
|                                      |                                          |           |      |       |          | 919b-4254-a690-4060df048356 on /dev/vdb     |                                               |
| b73f8e85-c60f-4ffa-bb41-65708478f578 | datas-01                                 | deleting  |  300 | None  | false    |                                             |                                               |
| d3acf70c-7d90-4f9f-a5c9-6e9ae470ef69 |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| f787c33e-97da-4ae8-9f02-487d4cdd84f2 |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| 4a1cba99-b119-4ed6-861f-fa62f0560802 |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| b3206f73-5fcc-450d-8ebe-9ddec6e06dca |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| 5cf9a571-4c32-41c9-9c5c-e1132793f927 |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| 4222a516-f6ae-4e7d-9004-e6992a690572 |                                          | available |   10 | None  | true     |                                             | readonly='False'                              |
| 09355ed7-e127-4fa0-b81e-83a9d766ff91 | ressources                               | in-use    |   30 | None  | false    | Attached to d626e698-aef1-4d05-bafd-        |                                               |
|                                      |                                          |           |      |       |          | 00a82edafe90 on /dev/vdb                    |                                               |
| 22f0227e-e38e-4fe3-b487-2f9bba88bffe | backup-config                            | detaching |    5 | None  | false    | Attached to f851ae1c-9abf-4128-a94f-        |                                               |
|                                      |                                          |           |      |       |          | 9f2e22629edc on /dev/vdb                    |                                               |
| f9cf8d9d-500a-4422-b16e-5117bcd6741a | backup-docker-volumes                    | detaching |   50 | None  | false    | Attached to f851ae1c-9abf-4128-a94f-        |                                               |
|                                      |                                          |           |      |       |          | 9f2e22629edc on /dev/vdc                    |                                               |
| f835f294-80b0-4c70-8444-c18dd4066285 | code-source                              | in-use    |    5 | None  | false    | Attached to b720fc2e-119e-                  |                                               |
|                                      |                                          |           |      |       |          | 4f95-a343-f275677498ef on /dev/vdb          |                                               |
| 8329bf61-61d2-4f39-bb7a-7995e96c4b04 | taf-datas                                | in-use    |   30 | None  | false    | Attached to adead1df-041b-4e0a-823a-        |                                               |
|                                      |                                          |           |      |       |          | 48a2ab664f33 on /dev/vdb                    |                                               |
+--------------------------------------+------------------------------------------+-----------+------+-------+----------+---------------------------------------------+-----------------------------------------------+
```




## Traitement des Instance avec Boot sur volume

export depuis CEPH
https://serverfault.com/questions/466702/copy-kvm-image-from-ceph-to-other-storage


```bash

cd /local-storage/migration/cinder
rbd export volumes/volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81 volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81

qemu-img info volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81
qemu-img convert -f raw -O qcow2 volume-dfd5e624-9ad1-43cb-939d-36c988ee8b81 test-image.qcow2

openstack image create \
  --disk-format qcow2 \
  --container-format bare \
  --private \
  --project ebraux_perso \
  --file test-image.qcow2 \
  migration-test-image 
```

https://docs.openstack.org/cinder/ussuri/admin/blockstorage-volume-backups.html
https://blog.oddbit.com/post/2015-09-29-migrating-cinder-volumes-between-openstack-environments/
https://cloud.garr.it/support/kb/general/cinderVolumeMigration/




## network


template pour l'IP
```bash
heat_template_version: rocky
parameters:
  key:
    description: Key Name
    type: string

resources:

  test-net-heat_port:
    type: OS::Neutron::Port
    properties:
      fixed_ips:
      - ip_address: 172.16.0.6
        subnet_id: private
      network_id: private
      security_groups:
      - admin-access

  test-net-heat:
    type: OS::Nova::Server
    properties:
      flavor: m1.medium
      image: imta-ubuntu-basic
      key_name:
        get_param: key
      name: test-net-heat1
      networks:
      - port:
          get_resource: test-net-heat_port
```





openstack volume backup create [--incremental] [--force] VOLUME

openstack volume backup restore BACKUP_ID VOLUME_ID


## pb volumes HS



```bash
ceph osd map volumes Docker-recette
osdmap e949 pool 'volumes' (2) object 'Docker-recette' -> pg 2.2147a6ff (2.ff) -> up ([20,9,13], p20) acting ([20,9,13], p20)

root@os-ctrl-04:~# ceph osd map volumes datas-01
osdmap e949 pool 'volumes' (2) object 'datas-01' -> pg 2.c73dba1f (2.1f) -> up ([20,7,8], p20) acting ([20,7,8], p20)

ceph osd map volumes  mounir_volume
osdmap e949 pool 'volumes' (2) object 'mounir_volume' -> pg 2.cab4bec1 (2.c1) -> up ([0,5,9], p0) acting ([0,5,9], p0)

ceph osd map volumes  490986c9-0006-4291-a6f7-1eec3a8439e5
osdmap e949 pool 'volumes' (2) object '490986c9-0006-4291-a6f7-1eec3a8439e5' -> pg 2.4238d788 (2.188) -> up ([11,15,8], p11) acting ([11,15,8], p11)

ceph osd map volumes  490986c9-0006-4291-a6f7-1eec3a8439e5
osdmap e949 pool 'volumes' (2) object '490986c9-0006-4291-a6f7-1eec3a8439e5' -> pg 2.4238d788 (2.188) -> up ([11,15,8], p11) acting ([11,15,8], p11)



docker exec ceph-mon-ceph-poc-01 rados --pool test get hello-world /tmp/fetch.txt
docker exec ceph-mon-ceph-poc-01 cat /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rm -f /tmp/hello-world.txt
docker exec ceph-mon-ceph-poc-01 rm -f /tmp/fetch.txt

docker exec ceph-mon-ceph-poc-01 rados --pool test rm  hello-world
```
