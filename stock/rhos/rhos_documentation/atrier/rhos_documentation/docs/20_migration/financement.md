


Bonjour,

Suite au CA de la DG nous disposons du budget global ( équipement école + collectif), voici le résumé des dotations

Moyens vidéo conf (Classes virtuelles ) BBB , Zoom (Collectif) --> 24K€
Outillage pédagogique Salle TP virtuelles (Ecole + Collectif)  --> 93+56 = 149K€

Ce qui est entendu par collectif : favoriser les investissements de cohérence d'ensemble (portage des orientations collectives et / ou de moyens mutualisés). En clair faire des choix en cohérence avec les autres écoles.

A dépenser avant la fin de l'année..

Christian.

# ---
Avant d'envoyer à tous les participants, je propose une première version du relevé de conclusion après avoir visionner le petit enregistrement effectué pendant qu'on causait du sujet. Il commence après les -------
N'hésitez pas à corriger ou complété ou dire si c'est OK.
En tout cas, il faudra qu'on rediscute ensemble de la ventilation de budget entre RHV et OpenStack. Il y a des parties qui peuvent être communes (switchs) et on va probablement avoir de la marge côté RHV puisqu'au final, on cumule budget DG et renouvellement salle de TP (et peut-être qu'on aura déjà ce qu'il faut côté Netapp sans ré-investir).

--------------------------------------------------------------------------------------------------------------
La DG a validé une partie du budget demandé
Le budget de renouvellement des salles de TP va être réorienté mais pas celui d'équipement de nouvelle salle (Cencyble à Rennes)
JFB doit présenter une synthèse des possibilités de budget (investissement/fonctionnement) pour le mardi 23/06
Chaque chef de projet devra ensuite décider de l'utilisation du budget (limite de mi-septembre pour le choix)

Côté RHV :

* première phase pour muscler la ferme de Rennes pour la rentrée (décision à prendre rapidement) :
  * achat de 4 serveurs (a priori des R440)
  * éventuellement des switchs
* deuxième phase (décision à prendre fin août/début septembre) pour monter une nouvelle ferme complémentaire à Brest avec pour objectif la fin d'année civile :
  * beaucoup de points à éclaircir et à arbitrer suivant le budget disponible
  * achats de serveurs permettant de supporter des GPU (a priori des R740)
    * nombre de serveurs ?
    * avec GPU tout de suite ou non ?
  * quels besoins de switchs 10Gb et quelle marque ? => réflexion à mener côté NOC en prenant aussi en compte les problématiques OpenStack
  * au niveau stockage, on s'oriente vers du NFS fourni par le nouveau Netapp mais avec les interrogation suivantes :
    * quel espace pourrait-on dégager une fois tout transféré depuis le Netapp actuel ? CLB doit faire le point.
    * quelles possibilités de priorisation des flux pour éviter des effets de bords néfastes sur les usages critiques (ferme VMware, données utilisateurs, ...) en cas d'emballement de RHV ? Questions à poser lors du workshop de début août.

Côté OpenStack :

* première phase :
  * un serveur à racheter
* deuxième phase
  * évolution du cœur de réseau en collaboration avec NOC (problématiques techniques et budgétaires)
