# Commandes pour créer un bond avec 1 Vlan

- Un bond
- 2 connexions
- 1 Vlan

## Creation du Bond
- l'IP sera portée par le VLAN, le bon est uniquement le support de bas niveau
- pas d'IPV6
``` bash
nmcli connection add \
  type bond \
  con-name Bond0 \
  ifname bond0 \
  bond.options "mode=active-backup,miimon=100" \
  ipv4.method disabled \
  ipv6.method ignore
```

## Association des 2 interfaces
``` bash
nmcli connection add \
  type ethernet \
  con-name bond0-if1-ens2f1 \
  ifname ens2f1 \
  master bond0 \
  slave-type bond

nmcli connection add \
  type ethernet \
  con-name bond0-if2-ens3f1 \
  ifname ens3f1 \
  master bond0 \
  slave-type bond
```

## Déclaration du VLAN associé au Bond
``` bash
nmcli con add \
  type vlan \
  con-name vlan177 \
  dev bond0 \
  id 177 \
  ip4 192.168.177.72/24 \
  gw4 192.168.177.1
```

## Vérification
``` bash
nmcli con show
# NAME               UUID                                  TYPE      DEVICE     
# ens10f0np0         6be172fe-325b-3dcb-986d-36c4d9cb1f7d  ethernet  ens10f0np0 
# vlan177            2bfd88b8-7b0c-41dc-b449-b90128c014b1  vlan      bond0.177  
# Bond0              0a0dccee-c73e-4b4e-97ab-db8ed2841edd  bond      bond0      
# bond0-con2-ens3f1  d1d60fc9-3a79-439d-bd66-b42938a20687  ethernet  ens3f1     
# bond0-ens2f1       5686c8e5-9d36-40a2-9699-13533ee37f9e  ethernet  ens2f1     
# ens10f0np0         82fe350b-1a21-4793-adec-6f80af96a231  ethernet  --         
# ens10f1np1         4bc7e97e-92cc-4621-ba31-6ee14a8a84cb  ethernet  --         
# ens2f0             20e1421a-7c39-3989-9d99-de5f9a834465  ethernet  --         
# ens2f1             a19e5e45-ab32-37db-a317-b89f8a806730  ethernet  --         
# ens3f0             d923b5dc-5a28-4054-ad77-03fcc1005241  ethernet  --         
# ens3f1             ba70f5b6-29be-4934-9f03-31490672dcf2  ethernet  --         
# vlan176            92c50851-86aa-49c9-bd3b-e5e79b65b5c0  vlan      --         
```

``` bash
nmcli -p con show  vlan177
```

---
## Sources

- [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configure_802_1q_vlan_tagging_using_the_command_line_tool_nmcli](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configure_802_1q_vlan_tagging_using_the_command_line_tool_nmcli)


---
## a trier

``` bash
BOND_NAME="${BRIDGE_NAME}-b"
VLAN_NAME="${BRIDGE_NAME}-v"
BRIDGE_INTERFACE_NAME="${BRIDGE_NAME}-c1"

nmcli connection add type bond con-name ${BOND_NAME} ifname ${BOND_NAME} bond.options "mode=active-backup,miimon=100" ipv4.method disabled ipv6.method ignore
nmcli connection add type ethernet con-name ${BRIDGE_INTERFACE_NAME} ifname ${BRIDGE_INTERFACE} master ${BOND_NAME} slave-type bond
nmcli up ${BRIDGE_INTERFACE_NAME}
nmcli connection add type bridge con-name ${BRIDGE_NAME} ifname ${BRIDGE_NAME} ip4 ${BRIDGE_IP4}
nmcli connection add type vlan con-name ${VLAN_NAME} ifname ${BOND_NAME}.${BRIDGE_VLAN} dev ${BOND_NAME} id ${BRIDGE_VLAN} master ${BRIDGE_NAME} slave-type bridge
```
``` bash
 1003  nmcli con show

nmcli con add   type bond       con-name Bond0   ifname bond0   bond.options "mode=active-backup,miimon=100"   ipv4.method disabled   ipv6.method ignore
nmcli con add   type ethernet   con-name bond0-if1-eno2   ifname eno2   master bond0   slave-type bond
nmcli con add   type vlan       con-name vlan176   dev bond0   id 176   ip4 192.168.176.11/24   gw4 192.168.176.1

nmcli con add   type bond       con-name Bond2   ifname bond2   bond.options "mode=active-backup,miimon=100"   ipv4.method disabled   ipv6.method ignore
nmcli con add   type ethernet   con-name bond2-if1-eno2   ifname eno2   master bond2   slave-type bond
nmcli con up bond2-if1-eno2
nmcli con add   type vlan       con-name vlan177   dev bond2   id 177   ip4 192.168.177.11/24   gw4 192.168.177.1

nmcli con add   type bond       con-name Bond1   ifname bond1   bond.options "mode=active-backup,miimon=100"   ipv4.method disabled   ipv6.method ignore
nmcli con add   type ethernet   con-name bond1-if1-eno2   ifname eno2   master bond1   slave-type bond
nmcli con up bond1-if1-eno2
nmcli con add   type vlan       con-name vlan173   dev bond1   id 173   ip4 10.129.173.11/24   gw4 10.129.173.1



nmcli con add   type vlan       con-name vlan176   dev bond0   id 176   ip4 192.168.176.11/24   gw4 192.168.176.1

OK : 
nmcli con add type vlan con-name eno2.173 dev eno2 id 173  ipv4.method manual ip4 10.129.173.11/24    gw4 10.129.173.1     ipv6.method ignore
nmcli con add type vlan con-name eno2.177 dev eno2 id 177  ipv4.method manual ip4 192.168.177.11/24   gw4 192.168.177.1     ipv6.method ignore



nmcli con add type vlan con-name eno2.173 dev eno2 id 173
nmcli con add type vlan con-name eno2.177 dev eno2 id 177

Il faut donc mettre en place un bridge intermédiaire, connecté à eno2, sur lequel on va venir créer les sutres bridges
```

---
## Annexes

### Tests d'attachechement d'une interface à plusieurs bond/bridge

``` bash
nmcli con add   type ethernet   con-name bondtest1-eno2   ifname eno2   master bondtest1   slave-type bond
```

``` bash
nmcli con add   type ethernet   con-name bondtest2-eno2   ifname eno2   master bondtest2   slave-type bond
```