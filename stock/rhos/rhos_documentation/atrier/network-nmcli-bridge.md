``` bash
nmcli con add \
  type vlan \
  con-name vlan176 \
  dev eno2 \
  id 176 \
  ip4 192.168.176.11/24 \
  gw4 192.168.176.1

nmcli con add \
  type vlan \
  con-name vlan177 \
  dev eno2 \
  id 177 \
  ip4 192.168.177.11/24 \
  gw4 192.168.177.1
```

Définir les variables :

- BRIDGE_NAME=brexternal
- BRIDGE_INTERFACE=eno2
- BRIDGE_VLAN=178
- BRIDGE_IP4='10.129.176.11/22'

Et lancer la commande
``` bash
BRIDGE_INTERFACE_NAME="${BRIDGE_INTERFACE}-${BRIDGE_NAME}"
nmcli con add type bridge ifname ${BRIDGE_NAME} con-name ${BRIDGE_NAME} ip4 ${BRIDGE_IP4}
nmcli con add type vlan id ${BRIDGE_VLAN} ifname ${BRIDGE_INTERFACE_NAME} con-name ${BRIDGE_NAME}-1 dev ${BRIDGE_INTERFACE} master ${BRIDGE_NAME}
nmcli con up ${BRIDGE_INTERFACE_NAME}
nmcli con up ${BRIDGE_NAME}

nmcli con show
ping `echo ${BRIDGE_IP4} | sed  "s%/.*%%g"`
```

---
## Autres infos sur les bridges


Lister les interfaces disponibles
``` bash
nmcli con show
# NAME               UUID                                  TYPE      DEVICE   
# eno1               3d92f7fc-8ee4-42f4-b93c-e357b07bd462  ethernet  eno1     
# eno4               ee16be5d-8886-374e-a741-8f106b1fb49c  ethernet  eno4     
# eno2               10972ccf-f6e7-31da-844a-860ba37e48ad  ethernet  eno2     
# eno3               14b459d6-6ce1-3c61-96b2-119543addaec  ethernet  eno3     
```

Créer le bridge, exemple pour le bridge vers le réseau Infrastructure
``` bash
BRIDGE_NAME=brinfra
BRIDGE_INTERFACE=eno1

nmcli con add ifname ${BRIDGE_NAME} type bridge con-name ${BRIDGE_NAME}
nmcli con add type bridge-slave ifname ${BRIDGE_INTERFACE} master ${BRIDGE_NAME}
``` 

Afficher le résultat
``` bash
nmcli con show
# NAME               UUID                                  TYPE      DEVICE   
# eno1               3d92f7fc-8ee4-42f4-b93c-e357b07bd462  ethernet  eno1     
# eno4               ee16be5d-8886-374e-a741-8f106b1fb49c  ethernet  eno4     
# eno2               10972ccf-f6e7-31da-844a-860ba37e48ad  ethernet  eno2     
# eno3               14b459d6-6ce1-3c61-96b2-119543addaec  ethernet  eno3     
# brinfra            8bf37ded-c6c5-4210-ba8d-1d33f6aa1c6b  bridge    brinfra  
# bridge-slave-eno1  2777060d-a319-4fdd-be1b-f319c2a0ea69  ethernet  --   
```

``` bash
ip a 
# 2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
#     link/ether e4:43:4b:83:10:2c brd ff:ff:ff:ff:ff:ff
#     altname enp25s0f0
#     inet 10.129.172.11/24 brd 10.129.172.255 scope global noprefixroute eno1
#        valid_lft forever preferred_lft forever

# 18: brinfra: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
#     link/ether 2a:cd:33:14:e0:ec brd ff:ff:ff:ff:ff:ff
```
