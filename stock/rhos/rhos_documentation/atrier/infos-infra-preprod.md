


director
interface : enp8s0
IP : 
subnet : ctlplane-subnet
[ctlplane-subnet]
cidr = 10.129.173.0/24
dhcp_start = 10.129.173.100
dhcp_end = 10.129.173.199
inspection_iprange = 10.129.173.90,10.129.173.99
gateway = 10.129.173.1
masquerade = true


br-ctlplane: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 52:54:00:57:70:99 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.16


enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:32:18:94 brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.16

 ip route
default via 10.129.172.1 dev enp1s0 proto static metric 102 

undercloud_public_host = 10.129.173.16
undercloud_admin_host =  10.129.173.17


undercloud_public_host = 10.129.173.16
undercloud_admin_host =  10.129.173.17


CTRL
ens10f0np0 UP
ens10f1np1 DOWN

ens1f0 UP
ens1f1 UP
ens2f0 DOWN
ens2f1 DOWN



ovs-vsctl list-ifaces br-ex
ens10f0np0
ens1f1
patch-provnet-2130c4f6-3a6d-4e4f-b4ad-0798fc36fddd-to-br-int
vlan174
vlan175
vlan176
vlan177
vlan178


br-int

br-ex
 bond1 : ens10f0np0 / ens1f1
 vlan174 : 192.168.174.51/24, 192.168.174.9/32, 192.168.174.7/32
 175
 vlan176 : 192.168.175.51/24
 vlan177 : 192.168.177.51/24
 vlan178 : 10.129.176.51/22
 