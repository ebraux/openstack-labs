

---
# MOSK 


``` yaml
cat  60-kaas-lcm-netplan.yaml 
network:
  version: 2
  bonds:
    bond0:
      dhcp4: false
      dhcp6: false
      interfaces:
      - eno2
      - enp94s0f0
      mtu: 1500
      parameters:
        mode: active-backup
    bond1:
      dhcp4: false
      dhcp6: false
      interfaces:
      - eno3
      - enp94s0f1
      mtu: 1500
      parameters:
        mode: active-backup
  bridges:
    br-tenant:
      addresses:
      - 192.168.179.17/24
      dhcp4: false
      dhcp6: false
      interfaces:
      - tenant
    ceph-cl:
      addresses:
      - 192.168.176.17/24
      dhcp4: false
      dhcp6: false
      interfaces:
      - ceph-cl-v
    ceph-rep:
      addresses:
      - 192.168.177.17/24
      dhcp4: false
      dhcp6: false
      interfaces:
      - ceph-rep-v
    k8s-ext:
      addresses:
      - 10.129.175.17/24
      dhcp4: false
      dhcp6: false
      interfaces:
      - k8s-ext-v
    k8s-pods:
      addresses:
      - 10.129.174.17/24
      dhcp4: false
      dhcp6: false
      interfaces:
      - k8s-pods-v
  ethernets:
    eno1:
      addresses:
      - 10.129.172.110/24
      dhcp4: false
      dhcp6: false
      gateway4: 10.129.172.1
      match:
        macaddress: e4:43:4b:ea:73:b0
      mtu: 1500
      nameservers:
        addresses:
        - 192.44.75.10
      set-name: eno1
    eno2:
      dhcp4: false
      dhcp6: false
      match:
        macaddress: e4:43:4b:ea:73:b1
      mtu: 1500
      set-name: eno2
    eno3:
      dhcp4: false
      dhcp6: false
      match:
        macaddress: e4:43:4b:ea:73:b2
      mtu: 1500
      set-name: eno3
    eno4:
      dhcp4: false
      dhcp6: false
      match:
        macaddress: e4:43:4b:ea:73:b3
      mtu: 1500
      set-name: eno4
    enp94s0f0:
      dhcp4: false
      dhcp6: false
      match:
        macaddress: f8:f2:1e:aa:d9:f0
      mtu: 1500
      set-name: enp94s0f0
    enp94s0f1:
      dhcp4: false
      dhcp6: false
      match:
        macaddress: f8:f2:1e:aa:d9:f1
      mtu: 1500
      set-name: enp94s0f1
  vlans:
    ceph-cl-v:
      dhcp4: false
      dhcp6: false
      id: "176"
      link: bond0
    ceph-rep-v:
      dhcp4: false
      dhcp6: false
      id: "177"
      link: bond1
    k8s-ext-v:
      dhcp4: false
      dhcp6: false
      id: "175"
      link: bond0
    k8s-pods-v:
      dhcp4: false
      dhcp6: false
      id: "174"
      link: bond0
    pr-floating:
      dhcp4: false
      dhcp6: false
      id: "178"
      link: bond1
    tenant:
      dhcp4: false
      dhcp6: false
      id: "179"
      link: bond1
```