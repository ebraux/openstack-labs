# Gestion des images "Openstack" 

---
## Vérifier que les images ipxe sont présentes

``` bash
ll /var/lib/ironic/httpboot
# -rw-r--r--. 1 42422 42422 758 15 nov.  15:45 boot.ipxe
# -rw-r--r--. 1 42422 42422 473 15 nov.  15:38 inspector.ipxe
```

---
## Charger les images pour l'undercloud

Charger dans l'Openstack utilisé par l'undercloud (par Ironic), les images utilisées pour booter les serveurs dans la phase d'introspection :

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_installing-the-overcloud-images_overcloud-images](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_installing-the-overcloud-images_overcloud-images)
Image de base utilisée ensuite pour créer les serveur avec Ironic pour l'Overcloud

Installation des packages, si nécéssaire :
``` bash
sudo dnf install rhosp-director-images-uefi-x86_64 rhosp-director-images-ipa-x86_64
ll /usr/share/rhosp-director-images

```

Intégration des images dans Openstack

- Récupération des images, depuis les packages :
``` bash
cd 
source ~/stackrc
mkdir /home/stack/images; cd $_
```

Les images de l'agent Ironic pour le boot pxe 
``` bash
tar -xvf /usr/share/rhosp-director-images/ironic-python-agent-latest.tar
```

Les images pour l'introspection
``` bash
# mandatory
tar -xvf  /usr/share/rhosp-director-images/overcloud-hardened-uefi-full-latest.tar

```

- Intégration dans l'Openstack de l'undercloud
``` bash
# image en raw
openstack overcloud image upload  \
  --image-path /home/stack/images/

ls -l /var/lib/ironic/images/
# total 2355856
# -rw-r--r--. 1 root 42422 6442450944 15 nov.  16:00 overcloud-hardened-uefi-full.raw

ls -l /var/lib/ironic/httpboot
# total 472256
# -rwxr-xr-x. 1 root  42422  12192152 15 nov.  16:00 agent.kernel
# -rw-r--r--. 1 root  42422 471386988 15 nov.  16:00 agent.ramdisk
# -rw-r--r--. 1 42422 42422       758 15 nov.  15:45 boot.ipxe
# -rw-r--r--. 1 42422 42422       473 15 nov.  15:38 inspector.ipxe
```
> Les images ne sont plus gérée spar glance, mais directement par Ironic. On ne peut donc plus utiliser la commande `openstack image list`




### Charger les images minimal utilisées pour le déploiement de l'overcloud


Installation des packages, si nécessaire :
``` bash
sudo dnf install rhosp-director-images-minimal
# /usr/share/rhosp-director-images/overcloud-minimal-17.1-20230802.2.x86_64.tar
# /usr/share/rhosp-director-images/overcloud-minimal-17.1-20230907.1.x86_64.tar

```

Intégration des images dans Openstack

- Récupération des images, depuis les packages :
``` bash
cd 
source ~/stackrc
mkdir /home/stack/images; cd $_

openstack overcloud image upload \
  --image-path /home/stack/images/ \
  --image-type os \
  --os-image-name overcloud-minimal.qcow2

# tar -xvf /usr/share/rhosp-director-images/overcloud-full-latest-17.1.tar
# tar -xvf /usr/share/rhosp-director-images/overcloud-minimal-latest-17.1.tar
# openstack overcloud image upload \
#   --image-path /home/stack/images/ \
#   --image-type os \
#   --os-image-name overcloud-full.qcow2

```
- Intégration dans l'Openstack de l'undercloud
``` bash
openstack overcloud image upload \
  --image-path /home/stack/images/ \
  --image-type os \
  --os-image-name overcloud-minimal.qcow2

openstack image list
# +--------------------------------------+---------------------------+--------+
# | ID                                   | Name                      | Status |
# +--------------------------------------+---------------------------+--------+
# | 9cc7f30d-f1b0-4cf4-9931-aec60f37c823 | overcloud-full            | active |
# | 1059418a-7f6b-4a03-8be2-b14c793baa1a | overcloud-full-initrd     | active |
# | 4600424d-c797-4afa-9acd-718478fc80e1 | overcloud-full-vmlinuz    | active |
# | b4adcef1-9728-4693-a04b-b1e1e505a47d | overcloud-minimal         | active |
# | 4c1311a3-6c4b-48d3-b407-5981d4e1b47b | overcloud-minimal-initrd  | active |
# | 35317639-f731-4dc5-9757-b6fd4865f9d4 | overcloud-minimal-vmlinuz | active |
# +--------------------------------------+---------------------------+--------+
```
