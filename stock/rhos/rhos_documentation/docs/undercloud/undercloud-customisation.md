---
# Operations de post-configuration de l'undercloud

---
## Configuration du dns sur le subnet "ctrlplane"
``` bash
openstack subnet set --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 ctlplane-subnet
openstack subnet show ctlplane-subnet
```

---
## personnalisation du cluster


You can provide custom configuration for services beyond the available undercloud.conf parameters by configuring Puppet hieradata on the director : 
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#configuring-hieradata-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#configuring-hieradata-on-the-undercloud)


Configuration réseau
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#proc_configuring-undercloud-network-interfaces_installing-director-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#proc_configuring-undercloud-network-interfaces_installing-director-on-the-undercloud)
