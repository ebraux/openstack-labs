# Gestion de la registry locale

Les images sont récupérées depuis la registry Redhat, et sont copiées dans la registry locale. Il faut donc que le process d'installation puisse se connecter à ces 2 registry.

Les problèmes de connexion sont souvent dus à des problèmes de proxy, principalement au paramètrage de `no_proxy` (voir [Configuration du proxy](./proxy.md))

La registry locale est déployée par Director, et est servie par le serveur Web Apache. le fichier de configuration se trouve dans le dossier `/etc/httpd/conf.d/image-serve.conf`

- Le port d'écoute est le 8787
- Pour accéder au service il faut utiliser le nom `hostname`.ctlplane.`overcloud_domain_name`
- Ce nom est défini à partir des information du fichier undecloud.conf
  - hostname, mais sans le domaine.
  - overcloud_domain_name sna smodification
- L'ip correspondantt à ce nom est celle définie par undercloud_public_host
- La résolution DNS est définie dans le fichier /etc/hosts de la machine director

Le service doit être actif :
``` bash
sudo ss -ltunp | grep 8787
# tcp   LISTEN 0      128                *:8787             *:*    users:(("httpd",pid=45983,fd=4),("httpd",pid=2663,fd=4),("httpd",pid=2657,fd=4),("httpd",pid=2649,fd=4),("httpd",pid=2243,fd=4)) 
```  

Tester la connection à la registry locale (actuellement vide):
``` bash
curl -s http://rhos-director-br-01.ctlplane.overcloud-prod.com:8787/v2/ | jq
#{}
```

Tester la connection à la registry RedHat (elle répond, c'est l'essentiel) :
``` bash
curl -s https://registry.redhat.io/v2/ | jq
# {
#   "errors": [
#     {
#       "code": "UNAUTHORIZED",
#       "message": "Access to the requested resource is not authorized"
#     }
#   ]
# }
```

Sinon interroger la registry
``` bash
sudo podman search --limit 1000 "registry.redhat.io/rhosp" | grep rhosp-rhel8 | awk '{ print $2 }' | grep -v beta | sed "s/registry.
redhat.io\///g" | tail -n+2 

sudo podman search --limit 1000 "registry.redhat.io/rhosp-rhel8/openstack" --format="{{ .Name }}" | sort > satellite_images

sudo podman search --limit 1000 "registry.redhat.io/rhceph" | grep rhceph-4-dashboard-rhel8

sudo podman search --limit 1000 "registry.redhat.io/rhceph" | grep rhceph-4-rhel8

sudo podman search --limit 1000 "registry.redhat.io/openshift" | grep ose-prometheus
```

Lancer manuellement le chargement des images :
``` bash
sudo openstack tripleo container image prepare -e /home/stack/containers-prepare-parameter.yaml --output-env-file /home/stack/container-parameters.yaml
```

Le fichier `/home/stack/container-parameters.yaml` contient les variable pointant vers les images.



