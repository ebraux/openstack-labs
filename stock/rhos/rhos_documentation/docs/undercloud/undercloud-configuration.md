---
# Installation de Director 

>  A réaliser en tant qu'utilisateur "stack" sur la machine Director

---
## Mise en place du fichier de configuration

S'assurer qu'on est bien avec l'utilisateur stack, dans le home dir
``` bash
id
# uid=1001(stack) gid=1001(stack) groups=1001(stack) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
``` 
``` bash
cd ~
```

Copie du fichier de configuration par défaut
``` bash
cp \
  /usr/share/python-tripleoclient/undercloud.conf.sample \
  /home/stack//undercloud.conf
```

Puis modifier le fichier pour correspondre à la configuration.

---
## Accès système : services et endpoints de l'undercloud

L'undercloud est une instance d'Openstack, donc un système distribué, dont les composant échangent via le réseau : mode client serveur pour la base de données, accès aux endpoint pour les API, ...

L'adresse IP aurait par exemple pu être l'adresse principale de la VM, dans le réseau infra. Mais comme les services et API ne sont utilisé que dans le contexte de l'undercloud, une IP dans le subnet de provisioning ets utilisée.

Une interface dédiée pour cette IP a été crée.

Dans la configuration de l'undercloud, cette IP est définie par la variable `[DEFAULT]/undercloud_admin_host`, dans le fichier `undercloud.conf`. Le format est uniquement une IP, pas le format CIDR.

Director ne prend pas en charge la création de cette IP. Il faut donc créer l'interface, et la configurer avant de faire le déploiement.

> Si cette IP n'est pas valide, des message comme "Erreur d'accès Mysql" apparaissent lors du déploiement


---
## Configuration du réseau de provisioning pour l'overcloud

L'undercloud va déployer l'ensemble des noeuds de l'overcloud, en utilisant le réseau de provisioning. Ce réseau est un réseau physique existant, et accessible sur l'ensemble des noeuds à déployer, auquel correspond un "provider Network", qui va être créé dans l'undercloud.

Director va gérer entièrement ce réseau :

- en créant un bridge connecté au réseau physique, et en créant une adresse IP sur ce bridge
- en créant dans l'Openstack de l'Undercloud, un "provisionning network" correspondant

Le subnet est utilisé à la fois pour donner une IP temporaire aux nouveaux serveurs durant  la phase d'introspection des (inspection_iprange), et pour attribuer une IP définitive aux serveurs en utilisant une plage DHCP (dhcp_start et dhcp_end).

Director prend en charge la création du bridge, et sa configuration. Il a besoin d'une interface réseau  dédiée, que lui seul va gérer, et via laquelle le réseau de provisioning est accessible en natif, sans Vlan.

Le paramétrage de cette configuration, se fait dans le fichier de configuration `undercloud.conf` :

- l'interface à utiliser : [DEFAULT] local_interface :
- Adresse IP à utiliser : [DEFAULT] local_ip :   (uniquement une IP, pas le format CIDR)
- Le réseau à créer dans l'Openstack de l'Undercloud
  - [DEFAULT]local_subnet, fait référence à une section contienant les informations sur le subnet
  - [nom_du_local_subnet] : contient les infos du subnet, le cidr, la zone de 

Important : la gateway doit répondre au ping. En général, on indique l'IP de  `local_interface`, le routage étant ensuite assuré au niveau de la machine Director, si besoin.

---
## Configuration du subnet de provisionning : ctlplane-subnet

Le subnet de provisionning est utilisé dans plusieurs  cas d'usage différents. Des espaces d'adresses IP sont donc définis :

 - De 2 à 89 : pour des serveurs statiques (director, NTP, Ceph, ...)
 - De 90 à 99 : Pendant la phase d'introspection des serveurs
 - De 100 à 199 : Pour le déploiement définitif des serveurs
 - De 200 à 254 : non utilisé

> L'adresse "1" est utilisée pour la gateway


Le subnet de provisionnig `ctlplane-subnet` correspond à la vision Openstack de ce réseau, et donc aux cas d'usage "introspection" et "déploiement définitif".

Ce qui donne la configuration suivante : 

- section réseau de provisionning : `[ctlplane-subnet]`
  - cidr = 10.129.173.0/24
  - gateway = 10.129.173.1
  - DHCP :
    - dhcp_start = 10.129.173.100
    - dhcp_end = 10.129.172.199
    - *director determines the allocation pools by removing the values set for the local_ip, gateway, undercloud_admin_host, undercloud_public_host, and inspection_iprange*
  - `inspection_iprange` = 10.129.172.90,10.129.172.99
  - `masquerade` : autorise un accès externe depuis le réseau de provisionning via director = true
  

---
## Endpoints de l'undercloud

Les noeud déployés pour l'overcloud vont devoir communiquer avec l'undercloud, par un endpoint.
Ce endpoint peut être accessible en http, ou en https.

Dans la configuration de l'undercloud, cette IP est définie par la variable `[DEFAULT]/undercloud_public_host`, dans le fichier `undercloud.conf`. Le format est uniquement une IP, pas le format CIDR.

Les serveur de l'overcloud auront accès à l'undercloud via le bridge qui a été crée (br-ctrlplane). Comme une interface a été crée dans ce bridge, et une IP associé à cette interface (local_ip), cette IP peut être utilisée pour les échanges entre les noeuds déployé dans l'overcloud, et l'openstack de l'undercloud.

Dans tous les cas, elle doit être différente de l'IP de services (undercloud_admin_host)

---
## Accès aux ressources externes

Les serveurs de ressource de l'école (DNS, NTP, Proxy, ...) ne sont pas accessibles directement depuis le subnet de provisionning.

Une machein "relay" a donc été mise en place pour ces service, accessible à l'adresse `10.129.173.14`


``` bash
undercloud_nameservers = 10.129.173.14
undercloud_ntp_servers = 10.129.173.14
```
---
## Exemple de configuration

``` bash
[DEFAULT]
local_interface =  enp9s0
local_subnet = ctlplane-subnet
local_ip = 10.129.173.17/24
local_mtu = 1500

undercloud_public_host = 10.129.173.17
undercloud_admin_host =  10.129.173.16

subnets = ctlplane-subnet

undercloud_nameservers = 10.129.173.14
undercloud_ntp_servers = 10.129.173.14
# ...

[ctlplane-subnet]
cidr = 10.129.173.0/24
dhcp_start = 10.129.173.100
dhcp_end = 10.129.173.199
inspection_iprange = 10.129.173.90,10.129.173.99
gateway = 10.129.173.17
masquerade = true

```

---
## Customisation du réseau

Pat défaut director va créer un fichier de configuration dans `/etc/os-net-config/config.yaml`.

Cette configuration ne correspond pas tout à fait à ce qu'on veut, car les 2 IP `undercloud_public_host = 10.129.173.17` et `undercloud_admin_host =  10.129.173.16` sont portées pour le bridge br-ctlplane. ce qui nécessite d'avoir une IP externe pour valider la gateway (par exemple 10.129.173.1), et on n'en a pas, car le réseau n'est pas routé

On créer doncun fichier de customisation `` : 
``` yaml
---
network_config:
- type: ovs_bridge
  name: br-ctlplane
  use_dhcp: false
  ovs_extra:
  - br-set-external-id br-ctlplane bridge-id br-ctlplane
  addresses:
  - ip_netmask: 10.129.173.17/24
  - ip_netmask: 10.129.173.17/32
  routes: []
  dns_servers: ['10.129.173.14']
  domain: []
  members:
    - type: interface
      name: enp9s0
      primary: true
      mtu: 1500
```

La config standard correspond, mais elle peut être adaptée si besoin : [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_configuring-undercloud-network-interfaces_installing-director-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_configuring-undercloud-network-interfaces_installing-director-on-the-undercloud)


---
## Customisation des templates de déploiement (Heat)

Indiquer un fichier de customisation dans la variable `[DEFAULT]/custom_env_files`

Exemple :
``` bash
custom_env_files = /home/stack/custom-undercloud-params.yaml
```

Créer le fichier `/home/stack/custom-undercloud-params.yaml`, et renseigner des variables de customisation

- `Debug`: activer ou non le debug =  True
- `AdminPassword`: undercloud admin user password. = celui utilisé surt toute l'infra
- `AdminEmail` : undercloud admin user email address. = emmanuel.braux@imt-atlantique.fr

exemple : 
``` yaml
parameter_defaults:
  Debug: True
  AdminPassword: "xxxxxx"
  AdminEmail: "emmanuel.braux@imt-atlantique.fr"
  NeutronEnableIsolatedMetadata: 'True'
```

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#common-heat-parameters-for-undercloud-configuration](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#common-heat-parameters-for-undercloud-configuration)


---
## Paramétrage spécifiques de certains composants

Permet par exemple de paramétrer le fonctionnement d'Ironic, de Nova, ...

Indiquer un fichier de customisation dans la variable `[DEFAULT]/custom_env_files`

Exemple :
``` bash
custom_env_files = /home/stack/hieradata.yaml
```

Créer le fichier `/home/stack/hieradata.yaml`

Exemple, mis en place pour la configuration de Nova :
``` yaml
nova::compute::force_raw_images: False
```

> Attention, ce fichier ne peut pas être vide. Si aucune fonctionnalité ne doit être adaptée, il faut mettre la ligne en commentaire.

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#configuring-hieradata-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#configuring-hieradata-on-the-undercloud)

---
## Télémétrie

La collecte d'informations sur le fonctionnement des composants de l'undercloud (gnocchi, aodh, panko) est prévue.

Mais il n'y a pas d'outils directement intégrés pour les exploiter. Et ces données ont au final que peut d'importance.

La télémétrie de l'undercloud est donc  désactivée, au moins dans un premier temps :

- `enable_telemetry = false` 

## Clean Node

``` bash
clean_nodes = false
```

Le nettoyage de noeud peut prendre trop de temps, ou planter.
Si un nettoyage échoue, le noeud passe en "clean failed", et la seule solution est alors de la supprimer, et de relancre l'introspection.
 Pour éviter ces problème, RedHat recommande de désactiver le clean-node.
 

https://access.redhat.com/solutions/7015068
