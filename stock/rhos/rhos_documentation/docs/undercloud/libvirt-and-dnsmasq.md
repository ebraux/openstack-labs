---
# Libvirt et dnsmasq

## Désactivation de virbr0

Par défaut sur les vm, dnsmasq tourne et assurere des fonction de serveurs DHCP.
Ce mécanisme empèche le déploiement du serveur DHCP utilisé par "l'ironic-inspector"

"Take a look and see if dhcpd is running outside of the podman container. You probably installed libvirt on the Director. That package installed dhcpd which is probably running outside of the podman container."
[https://www.reddit.com/r/openstack/comments/o28rgg/introspection_not_reaching_dhcp_stage/](https://www.reddit.com/r/openstack/comments/o28rgg/introspection_not_reaching_dhcp_stage/)

"The virbr0 bridge interface is created by libvirtd's default network configuration. libvirtd is the service which provides a basis for the host to act as a hypervisor."
- [https://access.redhat.com/solutions/27195](https://access.redhat.com/solutions/27195)


Port 67 : https://www.speedguide.net/port.php?port=67



Vérification des requetes DHCP sur la machine director :

``` bash
sudo tcpdump -nli br-ctlplane 
sudo tcpdump  -ei br-ctlplane  udp and \( port 67 or port 68 \)
sudo tcpdump -i any port 67 or port 68 or port 69
```
On a des `get DHCP request`, mais pas de reply.
- [https://serverfault.com/questions/1060053/dhcp-not-working-for-bridged-qemu-virtual-machine](https://serverfault.com/questions/1060053/dhcp-not-working-for-bridged-qemu-virtual-machine)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/recommendations_for_large_deployments/assembly-debugging-recommendations-and-known-issues_recommendations-large-deployments#ref-deployment-debugging_recommendations-large-deployments](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/recommendations_for_large_deployments/assembly-debugging-recommendations-and-known-issues_recommendations-large-deployments#ref-deployment-debugging_recommendations-large-deployments)

``` bash
netstat -ltunp | grep dnsmasq 
# udp        0      0 0.0.0.0:67              0.0.0.0:*                           3296/dnsmasq        
```

``` bash
ps -ef | grep 3296
# dnsmasq     3296       1  0 17:33 ?        00:00:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
# root        3204    3296  0 17:33 ?        00:00:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
```

``` bash
systemctl status tripleo_ironic_inspector_dnsmasq.service
# -> failed
```

Le serveur qui écoute par défaut sur le port 67 est celui déployé par Libvirt, et qui n'est pas configurée pour répondre aux requètes des serveurs déployés.

Solution : désactiver libvirt dans la VM director
``` bash
systemctl stop libvirtd.service
systemctl disable libvirtd.service
reboot
```

- [https://thelinuxcluster.com/2019/06/02/remove-virbr0-interfaces-from-centos-7/](https://thelinuxcluster.com/2019/06/02/remove-virbr0-interfaces-from-centos-7/)
- [https://www.thegeekdiary.com/how-to-remove-virbr0-and-lxcbr0-interfaces-on-centos-rhel-5-and-rhel-7/](https://www.thegeekdiary.com/how-to-remove-virbr0-and-lxcbr0-interfaces-on-centos-rhel-5-and-rhel-7/)

Autre pistes :

Des limitations sur la machine manager : 
- régles de sécurité : Vérification de disable MAC Address-spoofing filter
  - [https://access.redhat.com/solutions/284493](https://access.redhat.com/solutions/284493)
``` bash
ebtables -t nat -L
# Bridge table: nat

# Bridge chain: PREROUTING, entries: 0, policy: ACCEPT

# Bridge chain: OUTPUT, entries: 0, policy: ACCEPT

# Bridge chain: POSTROUTING, entries: 0, policy: ACCEPT
```
- Supprimer le réseau "defaut" sur la vm manager :
  - [https://www.cyberciti.biz/faq/linux-kvm-disable-virbr0-nat-interface/](https://www.cyberciti.biz/faq/linux-kvm-disable-virbr0-nat-interface/)
- vérifier dans la config système :
``` bash
    nf_call_iptables 0
    nf_call_ip6tables 0
    nf_call_arptables 0
```
- et des modules de filtrage sur les bridges `rmmod br_netfilter`
  - [https://discussion.fedoraproject.org/t/dhcp-not-working-in-vm-connected-to-host-bridge/73300/2](https://discussion.fedoraproject.org/t/dhcp-not-working-in-vm-connected-to-host-bridge/73300/2)
