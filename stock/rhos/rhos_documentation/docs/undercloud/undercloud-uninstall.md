# Uninstall l'undercloud

Tout d'abord supprimer l'overcloud
``` bash
openstack stack  delete overcloud
```

Vérifier la bonne suppression
``` bash
openstack stack list 
```
> il faut parfois répéter l'opération plusieurs fois


Supprimer les noeuds connus par Ironic
``` bash
openstack baremetal node delete $(openstack baremetal node list -f value -c UUID)
```

Vérifier
``` bash
openstack baremetal node list
```

Supprimer les container utilisés par Director
``` bash
sudo podman rm -f $(sudo podman ps -aq)
sudo podman ps -a
```

Et faire le ménage dans les images Docker
``` bash
sudo podman rmi $(sudo podman images -q)
sudo podman images
```

Supprimer les données (mot de passe, ...)
``` bash
rm /home/stack/undercloud-passwords.conf
```

Arrêter les services
``` bash
sudo systemctl list-unit-files | grep openstack | awk '{print $1}' | xargs -I {} sudo systemctl stop {}

sudo systemctl list-unit-files | grep neutron | awk '{print $1}' | xargs -I {} sudo systemctl stop {}
#sudo systemctl stop docker
sudo systemctl stop podman
#sudo systemctl stop keepalived
```

Supprimer les fichier de configuration
``` bash
sudo rm -rf \
  /var/lib/mysql \
  /var/lib/rabbitmq \
  /opt/stack/.undercloud-setup /root/stackrc /home/stack/stackrc \
  /var/opt/undercloud-stack \
  /var/lib/ironic-discoverd/discoverd.sqlite
  /var/lib/ironic-inspector/inspector.sqlite \
  /home/stack/.instack \
  /root/tripleo-undercloud-passwords \
  /var/lib/registry \
  /httpboot/ /tftpboot/ /srv/* \
  /var/lib/docker \
  /etc/os-net-config/config.json
```
Supprimer les ressourecs liées aux composant d'Openstack
``` bash
dirs="ceilometer heat glance horizon ironic ironic-discoverd keystone neutron nova swift haproxy"; 

for dir in $dirs; do sudo rm -rf /etc/$dir ; sudo rm -rf /var/log/$dir ;  sudo rm -rf /var/lib/$dir; done
```

Supprimer les packages
``` bash
sudo yum remove -y rabbitmq-server mariadb haproxy openvswitch keepalived $(rpm -qa | grep openstack) python-tripleoclient docker os-collect-config os-net-config
``` 

Faire le ménage dans les fichiers d econfiguration d'Apache HTTPD
``` bash
sudo rm -rf /etc/httpd/conf.d/10*
sudo rm -rf /etc/httpd/conf.d/25*
sudo rm -rf /etc/httpd/conf.d/openstack-tripleo-ui.conf.rpmsave
sudo sed -i '/:3000/d ; /:35357/d ; /:5000/d ; /:8042/d ; /:8774/d ; /:8777/d' /etc/httpd/conf/ports.conf
``` 

Faire le ménage dans les certificats générés
``` bash
sudo rm -rf /etc/pki/tls/certs/undercloud*
sudo rm -rf /etc/pki/tls/private/undercloud*
sudo rm -rf /etc/pki/ca-trust/source/anchors/*local*
#/etc/pki/ca-trust/source/anchors/cm-local-ca.pem \
```

Supprimer la configuration réseau de br-ctlplane
``` bash
#ovs-vsctl list-br
sudo ovs-vsctl  del-br br-ctlplane
sudo ovs-vsctl  del-br br-int

#sudo rm -rf /etc/sysconfig/network-scripts/ifcfg-br-ctlplane
#sudo mv  /etc/os-net-config/config.json  /etc/os-net-config/config.json.orig
#sudo sed -i '/ovs/d ; /OVS/d'  /etc/sysconfig/network-scripts/ifcfg-eth1
```

Apparement plus nécéssaire avec RHOS 16
``` bash
#!!! Starting from RHOSP 12 /root/.my.cnf file should also be removed:
#sudo rm /root/.my.cnf
```

Attention à bien supprimer les données des anciens conteneurs dan, `/var/lib/config-data/`, sinon podman va relancer les conteneurs en utilisant les anciennes configurations, donct les crédentials.


Ce qui par exemple ne permettra pas l'accès au services communs, comme Mysql ou rabbitMq.

``` bash
sudo rm -rf /var/lib/config-data
#sudo rm -rf /etc/cloud/

```

A la fin, rebooter la machine
``` bash
sudo reboot
```



---
## source :

- [https://access.redhat.com/solutions/2210421](https://access.redhat.com/solutions/2210421)
  