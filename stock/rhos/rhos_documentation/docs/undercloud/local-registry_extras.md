---

---
## Détails de la Configuration retenue

- ne pas télécharger les images CEPh : c'est un serveur externe qui est utilisé
- centraliser les images sur le serveur Director, pour éviter que chaque serveur essaye de les récupérer sur la Red Hat Container Catalog, à chaque mise à jour .


---


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#undercloud-container-registry](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_installing-director-on-the-undercloud#undercloud-container-registry)

Red Hat Enterprise Linux 8.4 no longer includes the docker-distribution package, which installed a Docker Registry v2. To maintain the compatibility and the same level of feature, the director installation creates an Apache web server with a vhost called image-serve to provide a registry. This registry also uses port 8787/TCP with SSL disabled. The Apache-based registry is not containerized, which means that you must run the following command to restart the registry:

``` bash
$ sudo systemctl restart httpd
```

La config est dans `/etc/httpd/conf.d/image-serve.conf`

Les logs : 

- /var/log/httpd/image_serve_access.log
- /var/log/httpd/image_serve_error.log.

Le contenu est dans  `/var/lib/image-serve`. On retrouve les élment sd'une registry :

-  Un dossier `v2`
-  Un fichier `_catalog`, avec la liste des repositories
-  Les images dans le dossier `rhosp-rhel8`


The Apache-based registry does not support podman push nor buildah push commands, which means that you cannot push container images using traditional methods. To modify images during deployment, use the container preparation workflow, such as the ContainerImagePrepare parameter. To manage container images, use the container management commands:

- `openstack tripleo container image list` : Lists all images stored on the registry.
- `openstack tripleo container image show` : Show metadata for a specific image on the registry.
- `openstack tripleo container image push` : Push an image from a remote registry to the undercloud registry.
- `openstack tripleo container image delete` Delete an image from the registry.


---
## Préparation des images : `ContainerImagePrepare`

Ensuite : "Modify the containers-prepare-parameter.yaml to suit your requirements."
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-container-images_preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-container-images_preparing-for-director-installation)




---
## Gestion des tags des images :

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#ref_guidelines-for-container-image-tagging_preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#ref_guidelines-for-container-image-tagging_preparing-for-director-installation)


Gestion des tags :

- `modify_append_tag` : String to append to the tag for the destination image. For example, if you pull an image with the tag 16.2.3-5.161 and set the modify_append_tag to -hotfix, the director tags the final image as 16.2.3-5.161-hotfix.
- `tag_from_label` : 
  - Use the value of specified container image metadata labels to create a tag for every image and pull that tagged image. For example, if you set tag_from_label: {version}-{release}, director uses the version and release labels to construct a new tag. For one container, version might be set to 16.2.3 and release might be set to 5.161, which results in the tag 16.2.3-5.161. Director uses this parameter only if you have not defined tag in the set dictionary.
  - The tag_from_label parameter generates the tag from the label metadata of the latest container image release it inspects from the Red Hat Container Registry. For example, the labels for a certain container might use the following version and release metadata
- The container images use multi-stream tags based on the Red Hat OpenStack Platform version. This means that there is no longer a latest tag.
- The default value for tag is the major version for your OpenStack Platform version. For this version it is 16.2. This always corresponds to the latest minor version and release.
- To change to a specific minor version for OpenStack Platform container images, set the tag to a minor version. For example, to change to 16.2.2, set `tag` to 16.2.2.
  - When you set tag, director always downloads the latest container image release for the version set in tag during installation and updates.
  - If you do not set tag, director uses the value of tag_from_label in conjunction with the latest major version.
- The tag parameter always takes precedence over the tag_from_label parameter. To use tag_from_label, omit the tag parameter from your container preparation configuration.

Utilisation :
- fixer une version majeur 16.2 (Défaut)
``` bash
  tag: 16.2
```

- fixer une version majeur 16.2 (Défaut)
``` bash
  # tag: 16.2
  ...
  tag_from_label: '{version}-{release}'
```

---
##  Configure director to not pull the Ceph Storage containers images from the Red Hat Container Registry.

``` bash
parameter_defaults:
  ContainerImagePrepare:
  - push_destination: true
    excludes:
      - ceph
      - prometheus
    set:
      ...
```


---

---
## Chargement des images 

- Nécessite que le fichier /home/stack/containers-prepare-parameter.yaml existe
- nécessite que la regitry ait été déployée, via l'installation de Director
- 
``` bash
sudo openstack tripleo container image prepare \
   -e /home/stack/containers-prepare-parameter.yaml \
   --output-env-file /home/stack/containers-parameters.yaml
```

Executé en tant que root :
- `/root/.tripleo/environments`
- `/root/container_image_prepare.log`

Résultat 
``` bash
sudo podman images
```


---
## Gestion des images

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#con_undercloud-container-registry_installing-director-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#con_undercloud-container-registry_installing-director-on-the-undercloud)