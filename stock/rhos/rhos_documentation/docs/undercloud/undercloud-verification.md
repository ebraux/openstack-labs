# Vérifier la configuration de l'undercloud

---
## création des fichiers de ressources
Vérification de la génération des mots de passe
``` bash
cat /home/stack/output/undercloud-passwords.conf 
```

Vérification du fichier d'environnement
``` bash
cat /home/stack/stackrc
```

---
## Les conteneurs

Vérification de la liste des composants déployés
``` bash
# tous les conteneur, utilisés pour le déploiement
sudo podman ps -a --format "{{.Names}} {{.Status}}" | wc
#     90     494    3619

# les conteneurs actifs
sudo podman ps  --format "{{.Names}} {{.Status}}" | wc
#     46     230    1565
```

---
## Le service de déploiement Baremetal/Ironic

``` bash
openstack baremetal conductor list
# +-----------------------------+-----------------+-------+
# | Hostname                    | Conductor Group | Alive |
# +-----------------------------+-----------------+-------+
# | rhos-director-br-01.imta.fr |                 | True  |
# +-----------------------------+-----------------+-------+
```

``` bash
openstack baremetal driver list
# +---------------------+-----------------------------+
# | Supported driver(s) | Active host(s)              |
# +---------------------+-----------------------------+
# | idrac               | rhos-director-br-01.imta.fr |
# | ilo                 | rhos-director-br-01.imta.fr |
# | ipmi                | rhos-director-br-01.imta.fr |
# | redfish             | rhos-director-br-01.imta.fr |
# +---------------------+-----------------------------+
```

---
## Les images à déployer

``` bash
openstack image list
# +--------------------------------------+---------------------------+--------+
# | ID                                   | Name                      | Status |
# +--------------------------------------+---------------------------+--------+
# | 9cc7f30d-f1b0-4cf4-9931-aec60f37c823 | overcloud-full            | active |
# | 1059418a-7f6b-4a03-8be2-b14c793baa1a | overcloud-full-initrd     | active |
# | 4600424d-c797-4afa-9acd-718478fc80e1 | overcloud-full-vmlinuz    | active |
# | b4adcef1-9728-4693-a04b-b1e1e505a47d | overcloud-minimal         | active |
# | 4c1311a3-6c4b-48d3-b407-5981d4e1b47b | overcloud-minimal-initrd  | active |
# | 35317639-f731-4dc5-9757-b6fd4865f9d4 | overcloud-minimal-vmlinuz | active |
# +--------------------------------------+---------------------------+--------+
```

---
## Nova
``` bash
openstack compute service list
# +----+----------------+----------------------------------------+----------+---------+-------+----------------------------+
# | ID | Binary         | Host                                   | Zone     | Status  | State | Updated At                 |
# +----+----------------+----------------------------------------+----------+---------+-------+----------------------------+
# |  1 | nova-conductor | rhos-director-br-01.overcloud-prod.com | internal | enabled | up    | 2023-09-28T13:38:37.000000 |
# |  2 | nova-scheduler | rhos-director-br-01.overcloud-prod.com | internal | enabled | up    | 2023-09-28T13:38:43.000000 |
# |  9 | nova-compute   | rhos-director-br-01.overcloud-prod.com | nova     | enabled | up    | 2023-09-28T13:38:37.000000 |
# +----+----------------+----------------------------------------+----------+---------+-------+----------------------------+
```