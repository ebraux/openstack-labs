# Déploiment de l'undecloud


---
## Déploiment

Exécuter en tant qu'utilisateur `stack`, depuis le homedir :

``` bash
openstack undercloud install
```

``` bash
########################################################
Deployment successful!
########################################################
Writing the stack virtual update mark file /var/lib/tripleo-heat-installer/update_mark_undercloud
##########################################################
The Undercloud has been successfully installed.
Useful files:
  Password file is at /home/stack/output/undercloud-passwords.conf
  The stackrc file is at ~/stackrc
Use these files to interact with OpenStack services, and ensure they are secured.
##########################################################
```

---
## Erreur "Failed containers: mysql_upgrade_db" 

``` bash
ERROR: Can't run container mysql_upgrade_db
  stderr: Reading datadir from the MariaDB server failed.
  Got the following error when executing the 'mysql' command line client
    ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
    FATAL ERROR: Upgrade failed
```


               "Binds": [
                    "/var/lib/config-data/puppet-generated/mysql:/var/lib/kolla/config_files/src:ro,rprivate,rbind",
                    "/etc/localtime:/etc/localtime:ro,rprivate,rbind",
                    "/etc/hosts:/etc/hosts:ro,rprivate,rbind",
                    "/var/lib/mysql:/var/lib/mysql:rw,rprivate,rbind",
                    "/var/log/containers/mysql:/var/log/mariadb:rw,rprivate,rbind",
                    "/var/lib/kolla/config_files/mysql.json:/var/lib/kolla/config_files/config.json:rw,rprivate,rbind"

datadir = /var/lib/mysql, correspond au `/var/lib/mysql`` local                   


/Stage[main]/Mysql::Server::Config/File[mysql-config-file]/content:
Error: Could not prefetch mysql_user provider 'mysql': Execution of '/usr/bin/mysql --defaults-extra-file=/root/.my.cnf -NBe SELECT CONCAT(User, '@',Host) AS User FROM mysql.user' returned 1: ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)

Execution of '/usr/bin/mysql --defaults-extra-file=/root/.my.cnf -NBe show databases' returned 1: ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)

---
## Vérification du réseau


``` bash
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:32:18:94 brd ff:ff:ff:ff:ff:ff
    inet 10.129.172.16/24 brd 10.129.172.255 scope global noprefixroute enp1s0
       valid_lft forever preferred_lft forever
3: enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:db:85:2a brd ff:ff:ff:ff:ff:ff
    inet 10.29.20.16/24 brd 10.29.20.255 scope global noprefixroute enp7s0
       valid_lft forever preferred_lft forever
4: enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:57:70:99 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.16/24 brd 10.129.173.255 scope global noprefixroute enp8s0
       valid_lft forever preferred_lft forever
5: enp9s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master ovs-system state UP group default qlen 1000
    link/ether 52:54:00:51:e3:60 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::5054:ff:fe51:e360/64 scope link 
       valid_lft forever preferred_lft forever
6: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether be:f0:02:ea:c6:2f brd ff:ff:ff:ff:ff:ff
8: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether b2:d8:58:b2:b7:43 brd ff:ff:ff:ff:ff:ff
10: br-ctlplane: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 52:54:00:51:e3:60 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.17/24 brd 10.129.173.255 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.173.16/32 brd 10.129.173.16 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet 10.129.173.17/32 brd 10.129.173.17 scope global br-ctlplane
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe51:e360/64 scope link 
       valid_lft forever preferred_lft forever
```

```bash
sudo ovs-vsctl show
```


configuration réseau : `cat /etc/os-net-config/config.yaml`



---
## Vérification de la registry

``` bash
sudo ss -ltunp | grep 8787
tcp   LISTEN 0      511                *:8787             *:*    users:(("httpd",pid=2078,fd=4),("httpd",pid=2076,fd=4),("httpd",pid=2074,fd=4),("httpd",pid=1840,fd=4))
```

``` bash
curl -s http://rhos-director-br-01.ctlplane.overcloud-prod.com:8787/v2/ | jq
{}
``` 
``` bash
curl -s http://rhos-director-br-01.ctlplane.overcloud-prod.com:8787/v2/_catalog | jq
```

---
## Vérification du déploiement de l'undercloud
``` bash

openstack port list
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
| ID                                   | Name | MAC Address       | Fixed IP Addresses                                                           | Status |
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
| 6960d5ce-85d9-4f49-bebd-3d4dc2c83b2b |      | fa:16:3e:c8:98:6e | ip_address='10.129.173.99', subnet_id='7efb56d0-cac0-4502-88de-8abb1663c3ef' | ACTIVE |
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
(undercloud) [stack@rhos-director-br-01 ~]$ openstack port set --name director 6960d5ce-85d9-4f49-bebd-3d4dc2c83b2b
(undercloud) [stack@rhos-director-br-01 ~]$ openstack port list
+--------------------------------------+----------+-------------------+------------------------------------------------------------------------------+--------+
| ID                                   | Name     | MAC Address       | Fixed IP Addresses                                                           | Status |
+--------------------------------------+----------+-------------------+------------------------------------------------------------------------------+--------+
| 6960d5ce-85d9-4f49-bebd-3d4dc2c83b2b | director | fa:16:3e:c8:98:6e | ip_address='10.129.173.99', subnet_id='7efb56d0-cac0-4502-88de-8abb1663c3ef' | ACTIVE |
+--------------------------------------+----------+-------------------+------------------------------------------------------------------------------+--------+

```
---
## les containers :

[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_installing-director_installing-director-on-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_installing-director-on-the-undercloud#proc_installing-director_installing-director-on-the-undercloud)