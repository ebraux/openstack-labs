# Préparation des images

 > A réaliser en tant qu'utilisateur "stack" sur la machine Director

Le déploiement des composants d'openstack, pour undercloud, et sur les machines déployées dans l'overcloud, ets réalisé en utilisant des conteneurs. 

Il faut configurer quelle images Director va utilser pour faire ce déploiment.



Generate the default container image preparation file: `/home/stack/containers-prepare-parameter.yaml`
``` bash
sudo openstack tripleo container image prepare default \
  --local-push-destination \
  --output-env-file /home/stack/containers-prepare-parameter.yaml


# parameter_defaults:
  # ContainerImagePrepare:
  # - push_destination: true
  #   set:
  #     ceph_alertmanager_image: ose-prometheus-alertmanager
  #     ceph_alertmanager_namespace: registry.redhat.io/openshift4
  #     ceph_alertmanager_tag: v4.6
  #     ceph_grafana_image: rhceph-6-dashboard-rhel9
  #     ceph_grafana_namespace: registry.redhat.io/rhceph
  #     ceph_grafana_tag: latest
  #     ceph_image: rhceph-6-rhel9
  #     ceph_namespace: registry.redhat.io/rhceph
  #     ceph_node_exporter_image: ose-prometheus-node-exporter
  #     ceph_node_exporter_namespace: registry.redhat.io/openshift4
  #     ceph_node_exporter_tag: v4.6
  #     ceph_prometheus_image: ose-prometheus
  #     ceph_prometheus_namespace: registry.redhat.io/openshift4
  #     ceph_prometheus_tag: v4.6
  #     ceph_tag: latest
  #     name_prefix: openstack-
  #     name_suffix: ''
  #     namespace: registry.redhat.io/rhosp-rhel9
  #     neutron_driver: ovn
  #     rhel_containers: false
  #     tag: '17.1'
  #   tag_from_label: '{version}-{release}'


```

- `--local-push-destination`
   - Pulls the necessary images from the Red Hat Container Catalog and pushes them to the registry on the undercloud.
   - Director uses this registry as the container image source. To pull directly from the Red Hat Container Catalog, omit this option.


Changer les droits sur le fichier généré
``` bash
sudo chown stack:stack  /home/stack/containers-prepare-parameter.yaml
```

Ajouter  les informations d'authentification pour l'accès à la registry redhat
``` bash
parameter_defaults:
  ContainerImageRegistryCredentials:
    registry.redhat.io:
      DISI1: DiSi2003
  ContainerImagePrepare:
  - push_destination: true
    set:
      ceph_alertmanager_image: ose-prometheus-alertmanager
       ...
```


