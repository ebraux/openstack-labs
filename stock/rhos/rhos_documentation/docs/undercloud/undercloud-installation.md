## Installation de Tripleo


> à réaliser en tant qu'utilisateur "stack" sur la machine Director

---
## Installation

- Vérifier que la release est bien la 9.2
``` bash
cat /etc/redhat-release 
#Red Hat Enterprise Linux release 9.2 (Plow)
```

Activation des repositories pour Director 

Repositoies pour RHOS17.1 
```bash
sudo subscription-manager repos --disable=*

sudo subscription-manager repos \
--enable=rhel-9-for-x86_64-baseos-eus-rpms \
--enable=rhel-9-for-x86_64-appstream-eus-rpms \
--enable=rhel-9-for-x86_64-highavailability-eus-rpms \
--enable=openstack-17.1-for-rhel-9-x86_64-rpms \
--enable=fast-datapath-for-rhel-9-x86_64-rpms
```

Update et reboot
```bash
sudo dnf update -y
sudo reboot
```


Installation des packages Director/tripleo
```bash
sudo dnf install -y python3-tripleoclient

```


```bash
sudo dnf clean all && sudo rm -rf /var/cache/dnf  && sudo dnf upgrade -y
```


---
## Configuration

S'assurer qu'on est bien avec l'utilisateur stack, dans le home dir
``` bash
id
# uid=1001(stack) gid=1001(stack) groups=1001(stack) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
``` 
``` bash
cd ~
```

Copie du fichier de configuration par défaut
``` bash
cp \
  /usr/share/python-tripleoclient/undercloud.conf.sample \
  ~/undercloud.conf
```

Création du dossier d'output de l'installation
``` bash
mkdir output
```

