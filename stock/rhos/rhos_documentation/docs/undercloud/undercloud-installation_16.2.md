## Installation de Tripleo


> à réaliser en tant qu'utilisateur "stack" sur la machine Director

---
## Installation

- Vérifier que la release est bien la 8.4
``` bash
cat /etc/redhat-release 
#Red Hat Enterprise Linux release 8.4 (Ootpa)
```

Activation des repositories pour Director 

Repositoies pour RHOS16.2 
```bash
sudo subscription-manager repos --disable=*

sudo subscription-manager repos \
--enable=rhel-8-for-x86_64-baseos-tus-rpms \
--enable=rhel-8-for-x86_64-appstream-tus-rpms \
--enable=rhel-8-for-x86_64-highavailability-tus-rpms \
--enable=ansible-2.9-for-rhel-8-x86_64-rpms \
--enable=openstack-16.2-for-rhel-8-x86_64-rpms \
--enable=fast-datapath-for-rhel-8-x86_64-rpms

sudo dnf module reset -y container-tools
sudo dnf module enable -y container-tools:3.0

```


Installation des packages Director/tripleo
```bash
sudo dnf install -y python3-tripleoclient
```

Installation des packages CEPH V4
```bash
sudo subscription-manager repos --enable=rhceph-4-tools-for-rhel-8-x86_64-rpms
sudo dnf install -y ceph-ansible
```




```bash
sudo dnf clean all && sudo rm -rf /var/cache/dnf  && sudo dnf upgrade -y
```


---
## Configuration

S'assurer qu'on est bien avec l'utilisateur stack, dans le home dir
``` bash
id
# uid=1001(stack) gid=1001(stack) groups=1001(stack) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
``` 
``` bash
cd ~
```

Copie du fichier de configuration par défaut
``` bash
cp \
  /usr/share/python-tripleoclient/undercloud.conf.sample \
  ~/undercloud.conf
```

Création du dossier d'output de l'installation
``` bash
mkdir output
```

