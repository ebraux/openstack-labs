

```
undercloud_hostname = rhos-director-br-01.imta.fr


generate_service_certificate = true
#undercloud_service_certificate =


```

---
``` bash 
enable_routed_networks = true
output_dir = /home/stack/output

enable_telemetry = false
undercloud_update_packages = true
custom_env_files = /home/stack/custom-undercloud-params.yaml

```
---
## Parametrage de la phase d'inspection

``` bash
# --------------------------------
# Paramètres pour gérer l'inspection
# --------------------------------
#inspection_interface = br-ctlplane
#enabled_hardware_types = ipmi,redfish,idrac
ipxe_enabled = True
inspection_enable_uefi = False

```

---
## Paramétrage du déploiement de l'Overcloud
- Gestion des images à utiliser pour le déploiement : fichier `containers-prepare-parameter.yaml`
- Définition des réseaux à déployer pour l'Overcloud : fichier `network_data.yaml`

``` bash
# --------------------------------
# Gestion du déploiement 
# --------------------------------
overcloud_domain_name = localdomain

clean_nodes = true
cleanup = false
container_images_file = /home/stack/containers-prepare-parameter.yaml
#container_insecure_registries = '10.129.172.22:5000'
```






### Config du serveur director
  - `inspection_interface`: 
      - The bridge that director uses for node introspection. 
      - This is a custom bridge that the director configuration creates.
      - Leave this as the default br-ctlplane.
      - 
  - `undercloud_hostname` : rhos-director-br-01.imta.fr
  - 
  - `local_interface` : interface réseau à utiliser pour le boot pxe (sur subnet 10.29.20.0/24)
  - 
  - `local_ip` : IP sur le réseau de provisionning 10.129.173.16/24
  - 
  - `local_subnet` : subnet du réseau de provisionning : ctlplane-subnet
  - 

  - `generate_service_certificate` : géneration de ertificats autosigné pour l'accès aux API de l'undercloud = true
      - The undercloud installation saves the resulting certificate /etc/pki/tls/certs/undercloud-[undercloud_public_vip].pem.
      - The CA defined in the certificate_generation_ca parameter signs this certificate.




  
### Gestion des images

  - container_images_file : fichier de configuration des images à utiliser pour le déploiement = `/home/stack/containers-prepare-parameter.yaml`
  - évolutions possible 
    - container_registry_mirror : registry mirroir
    - container_insecure_registries : une registry avec certificats non officiel

### Configuration des noeuds
  - `clean_nodes` : supprime les données sur les serveurs cible = activé
  - `cleanup` : suppression des données temporaires utilisées pour le déploiement = désactivé, au moins dans un premier temeps, pour le suisvi/debug

### Configuration du déploiement
  - `custom_env_files` :localisation du fichier de customisation de l'installation = `/home/stack/templates/custom-undercloud-params.yaml`
  - `output_dir` : dossier où stocker les output des différents scripts de déploiement = `/home/stack/output`

  - 

