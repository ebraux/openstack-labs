

Le mécanisme de gestion des interfaces, et de la config réseau est os-net-config.

Il s'appuie sur le fichier/etc/os-net-config.
Il est généré par rapport au contenu des fichier 'nic.'

Quelques règles, un peu pénibles : 

- il ne prend en compte que les interfaces actives
- on ne peut pas créer un bond avec une seule interface

---
## spécifier les interfaces à utiliser

Il est possible de fournir une liste des interfaces à utiliser à os-net-config. Il faut créer un fichier mapping.yaml

Director peut créer ce fichier, avec la template `firstboot/os-net-config-mappings.yaml`



- [https://bugzilla.redhat.com/show_bug.cgi?id=1227955]
  - This sounds like the intended behavior for os-net-config because by default we are only looking for active NICs on the system
  - The syntax is just standard yaml file with mappings under "interface_mapping", for example:
  - You would have to generate the file, for example by a first boot script:https://github.com/openstack/tripleo-heat-templates/blob/master/firstboot/os-net-config-mappings.yaml
- [https://docs.openstack.org/os-net-config/latest/usage.html](https://docs.openstack.org/os-net-config/latest/usage.html)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_additional-network-configuration#configuring-custom-interfaces)