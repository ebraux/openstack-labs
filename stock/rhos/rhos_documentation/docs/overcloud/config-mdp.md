# Cofiguration des mots de passe

## mot de passe root des instances


``` bash
Hi,

i'm glad to read that everything is working now.

If you still need to customize hosts password, you have two options:

customize the overcloud image using virt-customize:.
virt-customize -a {image} --root-password password:{mypassowrd}
merge /usr/share/openstack-tripleo-heat-templates/firstboot/os-net-config-mappings.yaml and /usr/share/openstack-tripleo-heat-templates/firstboot/userdata_root_password.yaml creating a third custom template to pass to OS::TripleO::NodeUserData, as described here [0]
Let me know if you still have questions, otherwise feel free to close the case if support is not needed anymore.

[0] https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/features/extra_config.html

Kind regards,

Flavio Piccioni
Senior Technical Support Engineer
Red Hat EMEA
```
