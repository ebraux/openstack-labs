# Configuration des rôles



Un role permet de définir comment une machine sera déployée :

Il comprend :

- La liste des services qui seront déployés sur le noeud (Keyston, nova, ....)
- La liste des réseaux auxquels le noeud sera connecté
- Un template pour le format du nom qui sera donné au noeud lors du déploiment
- Le nombre de noeud de ec type à déployer par défaut
- Des paramètres par défaut
- des tags ??


Par défaut Director propose des rôles prédéfinis. 

Pour afficher la liste des rôles disponibles :
``` bash
openstack overcloud roles list
# +--------------------------------+
# | Role Name                      |
# +--------------------------------+
# | BlockStorage                   |
# | CellController                 |
# | CephAll                        |
# | CephFile                       |
# | CephObject                     |
# | CephStorage                    |
# | Compute                        |
# | ComputeAlt                     |
# | ComputeDVR                     |
# | ComputeHCI                     |
# | ComputeHCIOvsDpdk              |
# | ComputeInstanceHA              |
# | ComputeLiquidio                |
# | ComputeLocalEphemeral          |
# | ComputeOvsDpdk                 |
# | ComputeOvsDpdkRT               |
# | ComputeOvsDpdkSriov            |
# | ComputeOvsDpdkSriovRT          |
# | ComputePPC64LE                 |
# | ComputeRBDEphemeral            |
# | ComputeRealTime                |
# | ComputeSriov                   |
# | ComputeSriovIB                 |
# | ComputeSriovRT                 |
# | Controller                     |
# | ControllerAllNovaStandalone    |
# | ControllerNoCeph               |
# | ControllerNovaStandalone       |
# | ControllerOpenstack            |
# | ControllerSriov                |
# | ControllerStorageDashboard     |
# | ControllerStorageNfs           |
# | Database                       |
# | DistributedCompute             |
# | DistributedComputeHCI          |
# | DistributedComputeHCIDashboard |
# | DistributedComputeHCIScaleOut  |
# | DistributedComputeScaleOut     |
# | HciCephAll                     |
# | HciCephFile                    |
# | HciCephMon                     |
# | HciCephObject                  |
# | IronicConductor                |
# | Messaging                      |
# | Minimal                        |
# | Networker                      |
# | NetworkerSriov                 |
# | NovaManager                    |
# | Novacontrol                    |
# | ObjectStorage                  |
# | Standalone                     |
# | Telemetry                      |
# | Undercloud                     |
# | UndercloudMinion               |
# +--------------------------------+
```


Red Hat OpenStack Platform usually consists of nodes in pre-defined roles, such as nodes in Controller roles, Compute roles, and different storage role types. Each of these default roles contains a set of services that are defined in the core heat template collection.


---
## Choix des rôles pour le déploiement

La liste des rôles utilisables dans le déploiement est définie dans le fichier `roles_data.yaml`.

Director génére automatiquement un fichier `/usr/share/openstack-tripleo-heat-templates/roles_data.yaml`, qui contient la liste des rôles par défaut : Controller, Compute, BlockStorage, ObjectStorage, CephStorage

Pour générer un fichier personnalisé, utiliser la commande
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller ComputeDVR
```

Cette commande va recopier le contenu de chaque fichier de rôle, et créer un nouveau fichier roles_data.yaml


Dans notre cas, nous avons besoin des roles : Controller et ComputeDVR. pour générer la configuration correspondante :
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller ComputeDVR
```

Attention : quand on modfier la liste des rôle, il faut ensuite à nouveau générer les fichiers de template heat pour la configuration du réseau.


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/advanced_overcloud_customization/chap-roles](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/advanced_overcloud_customization/chap-roles)


---
## Définir un nouveau role

Pour définir un nouveau rôle, il faut : 
créer un ficher dans le dossier `/usr/share/openstack-tripleo-heat-templates/roles`.



- Créer un dossier `roles` dans le répertoire de déploiement
- Copier un rôle de base dans ce dossier depuis un des rôles contenus dans le dossier `/usr/share/openstack-tripleo-heat-templates/roles`
- Le modifier

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-roles-data-file](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-roles-data-file)


