


Supprimer l'overcloud
``` bash
(undercloud) [stack@os-director-01 templates]$ openstack overcloud delete overcloud
Are you sure you want to delete this overcloud [y/N]? y
Deleting stack overcloud...
Started Mistral Workflow tripleo.stack.v1.delete_stack. Execution ID: 0e5b6b93-75c8-4127-9b2f-c63db212de91
Waiting for messages on queue 'tripleo' with no timeout.
Deleting plan overcloud...
Success.
```

``` bash
delete la stck, puis le pla.
si plante, on delete la stack à la main

openstack overcloud delete overcloud
Are you sure you want to delete this overcloud [y/N]? y
/usr/lib/python3.6/site-packages/tripleoclient/v1/overcloud_delete.py:136: ResourceWarning: unclosed file <_io.BufferedReader name=6>
  python_interpreter=python_interpreter)
Undeploying stack overcloud...
No stack found ('overcloud'), skipping delete
Deleting plan overcloud...
``` 




On peut aussi créer un plan de déploiement :

```bash
openstack overcloud plan create --templates /usr/share/openstack-tripleo-heat-templates my-overcloud
openstack overcloud plan deploy my-overcloud
```

### Operation scomplémentaires

```bash
openstack overcloud delete overcloud
```

Une fois déployé, on peut le tester

```bash
mkdir ~/my-overcloud-validation
cd ~/my-overcloud-validation
openstack container save overcloud-validation

openstack orchestration template validate --show-nested \
    --template ~/overcloud-validation/overcloud.yaml \
    -e ~/overcloud-validation/overcloud-resource-registry-puppet.yaml \
    -e [ENVIRONMENT FILE] -e [ENVIRONMENT FILE]
```

```bash
openstack overcloud plan deploy my-overcloud
openstack overcloud plan delete my-overcloud
```



## le 

```bash
total 12
-rw-rw-r--. 1 stack stack  773 16 nov.  05:11 inventory-network-config.yaml
-rw-------. 1 stack stack 3875 16 nov.  05:11 tripleo-ansible-inventory.yaml
-rw-r--r--. 1 stack stack 1970 16 nov.  05:11 tripleo-overcloud-virtual-ips.yaml
[stack@rhos-director-br-01 overcloud]$ pwd
/home/stack/overcloud-deploy/overcloud

puis -rw-r--r--. 1 stack stack 1439 16 nov.  05:12 tripleo-overcloud-baremetal-deployment.yaml
```
