

Utilisation du script deploy.sh, qui lance la commande `openstack overcloud deploy`, avec les bonne options.


Par exemple : 
``` bash
#!/bin/bash
#set -xe

source ~/stackrc

openstack overcloud deploy  \
  --timeout 360 \
  --verbose \
  --roles-file /home/stack/templates/roles_data.yaml \
  --answers-file /home/stack/templates/overcloud-answers.yaml \
  --log-file /home/stack/overcloudDeploy.log\
  --ntp-server 10.129.173.14
```