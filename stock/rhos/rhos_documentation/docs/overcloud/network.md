
---
## méthode de configuration

la configuration du réseau peut être configurée :

 - dans un fichier spécifique "network_data.yaml
   - chaque réseau est déclaré individuelement
   - appleé avec l'option "-n"

- dans un fichier de configuration 
  - appelé par le fichier de réponse
  - on renseigne directement les variables pour heat.

---
## Configuration mise en place :

Interface principale, relié au réseau de provisionning

Un Bridge br-trunk , avec les vlans ...
- sur crtl :  InternalApi, Tenant, Storage, Storgemgmt
- sur compute : InternalApi, Tenant, Storage

Le bridge par défaut br-ex, avec le réseau external
 - le réseau 172 ?? pas beosin dan snotre cas
 - mais le réseau de provisioning external par défaut : vlan 178
 
Pas d'interface dédiée pour d'autres réseau de provisioning 

NeutronBridgeMappings: 'datacentre:br-ex,provider178:br-trunk:storage:br-trunk'
  NeutronNetworkVLANRanges: 'datacentre:1:1000,provider178:178:178,storage:176:176'

Idéalement, ca aurait été :
 - interface pricipale
 - interface bridge truk, dont external
 - interface storage, et storage management
  