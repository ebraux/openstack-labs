---
# Personnalisation de la configuration des noeuds


## Définition du mot de passe root

Sur les noeuds déployés :

Principe : mise place d'une configuration cloud-init, qui active l'authentification avec l'utilisateur root en ssh, et qui défini un mot de passe
