# Getsion du réseau


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network)



---
## Default :


"DVR is enabled by default in new ML2/OVN deployments" : [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#limits-ml2ovn-network-driver_work-ovn](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#limits-ml2ovn-network-driver_work-ovn)


- Open Virtual Network (OVN) is the default mechanism driver used with ML2. [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#modular-layer2-network_network-overview](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#modular-layer2-network_network-overview)
- Red Hat chose ML2/OVN as the default mechanism driver for all new deployments starting with RHOSP  [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#ml2-mech-drivers_network-overview](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#ml2-mech-drivers_network-overview)
- 

In a default installation ovn-dbs is included in the Controller role and runs on Controller nodes. [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/assembly_work-with-ovn_rhosp-network#ovn_composable_service_work-ovn](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/assembly_work-with-ovn_rhosp-network#ovn_composable_service_work-ovn)

---
## Architecture

Lien avec les réseaux externes : Provider networks

- flat and Vlan


Connectivyté ente les instances (virtualisation de réseau)

 - connectivity to VM instances  : project network
 - network type drivre = GNEVE
 - Network mechanism drivers : Open Virtual Networking (OVN) 


OVN supporte FLAT, VLAN, VxLAn et GENEVE : [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#con_open-virtual-network_network-overview](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#con_open-virtual-network_network-overview)

``` bash
parameter_defaults:
  ...
  NeutronMechanismDrivers: ansible,ovn,baremetal
```


Using bridge mappings, you can associate a physical network name (an interface label) to a bridge created with OVS or OVN to allow provider network traffic to reach the physical network.

---
## OVN

[https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#ml2-mech-drivers_network-overview](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/networking-overview_rhosp-network#ml2-mech-drivers_network-overview)

---
## DVR

Distributed Virtual Routing (DVR) offers an alternative routing design based on VRRP that deploys the L3 agent and schedules routers on every Compute node.



---
## L2 Population

Using the L2 Population driver you can enable broadcast, multicast, and unicast traffic to scale out on large overlay networks.
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#config-l2pop-driver_common-network-tasks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html-single/networking_guide/index#config-l2pop-driver_common-network-tasks)


Liste des composants :

- Controller :
    - ML2 plug-in with OVN mechanism driver
    - OVN northbound (NB) database (ovn-nb) - port 6641
    - OVN northbound service (ovn-northd)
    - OVN southbound (SB) database (ovn-sb) - port 6642



- Compute and Gateway
    - OVN controller (ovn-controller)  (Where `OS::Tripleo::Services::OVNController` is defined)
    - OVN metadata agent (ovn-metadata-agent) (Where `OS::Tripleo::Services::OVNMetadataAgent` is defined)

OVS database server (OVSDB) ??

Plan [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/assembly_work-with-ovn_rhosp-network#ovn-list-of-components_work-ovn](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/assembly_work-with-ovn_rhosp-network#ovn-list-of-components_work-ovn)



- The OVN composable service ovn-dbs is deployed in a container called ovn-dbs-bundle
- ensure that the service is co-located on the same node as the pacemaker service, which controls the OVN database containers.



---
## Connexion externe


La connexion avec le réseau externe : mécanisme

Par défaut, la connexion avec le réseau : replacement de la chiane de caractère "bridge_name" dans le fichier de ""the custom NIC configuration template" : "datacneter:br-ex"  (à vérifier).

Possibilité de la définir manuellement. In the YAML environment file under parameter_defaults, use the NeutronBridgeMappings to specify which OVS bridges are used for accessing external networks.

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/connect-instance_rhosp-network#config-flat-prov-networks_connect-instance](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/networking_guide/connect-instance_rhosp-network#config-flat-prov-networks_connect-instance)

``` yaml
    - type: ovs_bridge
        name: br-net1
        mtu: 1500
        use_dhcp: false
        members:
        - type: interface
            name: eth0
            mtu: 1500
            use_dhcp: false
            primary: true
```


---
## Docs provider network

- [https://docs.openstack.org/networking-ovn/queens/admin/refarch/provider-networks.html](https://docs.openstack.org/networking-ovn/queens/admin/refarch/provider-networks.html)

---
## Diag

Vérification de l'activation de OVN

fichier containers-prepare-parameter.yaml 
``` yaml
 neutron_driver: ovn
```

Fichier custom-network-configuration.yaml:
``` yaml
  # NeutronNetworkType: vlan
```
tripleo-heat-templates/environments/services/neutron-ovn-dvr-ha.yaml:  NeutronNetworkType: ['geneve' , 'vxlan', 'vlan', 'flat']
tripleo-heat-templates/environments/network-environment.j2.yaml:  NeutronNetworkType: 'geneve,vlan'


Ensure that ovn_metadata_agent is running on Controller and Networker nodes.
``` bash
sudo podman ps | grep ovn_metadata
# doit être du type
# a65125d9588d  undercloud-0.ctlplane.localdomain:8787/rh-osbs/rhosp16-openstack-neutron-metadata-agent-ovn:16.2_20200813.1  kolla_start           23 hours ago  Up 21 hours ago         ovn_metadata_agent
```
---> KO


Ensure that Controller nodes with OVN services or dedicated Networker nodes have been configured as gateways for OVS.
``` bash
sudo ovs-vsctl get Open_Vswitch . external_ids:ovn-cms-options
# doit être du type
# enable-chassis-as-gw
```
---> OK


Sur tous les compute node


ovs-vsctl 



test de connectivité du réseau

- Create an external network (public1) as a flat network and associate it with the configured physical network (datacentre).
- Configure it as a shared network (using --share) to let other users create VM instances that connect to the external network directly.

openstack network create --share --provider-network-type flat --provider-physical-network datacentre --external public01

--> Error while executing command: ConflictException: 409, Unable to create the flat network. Physical network datacentre is in use.
Déjà utilisé par le réseau external prcédment créé

``` bash
openstack network list
+--------------------------------------+----------+--------------------------------------+
| ID                                   | Name     | Subnets                              |
+--------------------------------------+----------+--------------------------------------+
| 0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20 | external | cd238d9d-a277-4b73-b9a2-01d20e8a2d66 |
| cb6074db-0f57-4e82-9d5a-7b583acbeb49 | net-demo | 6b046bfb-d02b-49a8-8df8-02393f7324fe |
+--------------------------------------+----------+--------------------------------------+
```

``` bash
openstack network  show external  -c provider:network_type -c  provider:physical_network
+---------------------------+------------+
| Field                     | Value      |
+---------------------------+------------+
| provider:network_type     | flat       |
| provider:physical_network | datacentre |
+---------------------------+------------+
``` 

Présence des bridges
``` bash
sudo ovs-vsctl list-br
br-ex
br-int
```
--> 2 bridge br-ex et br-int -> OK



Interconcxion br-ex et réseau physique


Bridge br-ex : connexion externe
- doit être relé à l'interface physique
  - gestion des VLAn ?
- doit être relié à br-int
 
Bridge br-ex : connexion entre les noeuds
- doit etre relié à un port en mode GENEVE à chacun des noeds du cluster (controller et compute)
- doit être relié à br-ex


ens10f0np0 : interface du valn 173 en natif
ens1f0 : interface Vlan Opensatck principale -> tous les vlan OS arrivent en mode trunk
ens1f1 : interface Vlan Opensatck principale secondaire

``` bash
335a18af-594a-47b5-8655-8cca32170a7e
    Bridge br-ex
        fail_mode: standalone

        Port ens1f0
            Interface ens1f0

        Port br-ex
            Interface br-ex
                type: internal

        Port patch-provnet-f760d2da-080e-4269-b265-7538b41ee9d9-to-br-int
            Interface patch-provnet-f760d2da-080e-4269-b265-7538b41ee9d9-to-br-int
                type: patch
                options: {peer=patch-br-int-to-provnet-f760d2da-080e-4269-b265-7538b41ee9d9}

        Port vlan176
            tag: 176
            Interface vlan176
                type: internal

        Port vlan174
            tag: 174
            Interface vlan174
                type: internal

        Port vlan177
            tag: 177
            Interface vlan177
                type: internal

        Port vlan178
            tag: 178
            Interface vlan178
                type: internal
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal

    Bridge br-int
        fail_mode: secure
        datapath_type: system

        Port br-int
            Interface br-int
                type: internal

        Port patch-br-int-to-provnet-f760d2da-080e-4269-b265-7538b41ee9d9
            Interface patch-br-int-to-provnet-f760d2da-080e-4269-b265-7538b41ee9d9
                type: patch
                options: {peer=patch-provnet-f760d2da-080e-4269-b265-7538b41ee9d9-to-br-int}

        Port ovn-008a85-0
            Interface ovn-008a85-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.52"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}

        Port ovn-704852-0
            Interface ovn-704852-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.101"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}
        
        Port ovn-82a5bf-0
            Interface ovn-82a5bf-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.102"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}

        Port ovn-d3ffb6-0
            Interface ovn-d3ffb6-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.103"}
        
        Port ovn-c08b6c-0
            Interface ovn-c08b6c-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.53"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}

    ovs_version: "2.15.8"
``` 


Extenal network + subnet

Project network + subnet 
router + gateway to external network + interface to project network

Ping to 

2 instances deployed on 2 different computes :
- ping between the instance within the project network : OK  172.16.1.8 and 172.16.1.54
- ping to the internal gateway (router interace - 172.16.1.1) : OK
- ping to the external gateway (router gateway - 10.129.178.115) : OK
- ping to the gateway of external network : 10.129.176.1  --> KO
- gateway config in the VM : default via 172.16.1.1
- ping vers un autre routeur (10.129.179.220) : OK

Le pb viendrait du lien entre le routeur et le réseau physique. 
- Qui porte le 10.129.176.1 ?
- comment est géré le lien avec l'interface physique




config neutre _api sur controller
``` ini
[ml2]
type_drivers=geneve,vxlan,vlan,flat
tenant_network_types=geneve,vlan
mechanism_drivers=ovn
path_mtu=0
extension_drivers=qos,port_security,dns
overlay_ip_version=4

[securitygroup]
firewall_driver=iptables_hybrid

[ml2_type_geneve]
max_header_size=38
vni_ranges=1:65536

[ml2_type_vxlan]
vxlan_group=224.0.0.1
vni_ranges=1:65536

[ml2_type_vlan]
network_vlan_ranges=datacentre:1:1000

[ml2_type_flat]
flat_networks=datacentre

[ovn]
ovn_nb_connection=tcp:192.168.174.7:6641
ovn_sb_connection=tcp:192.168.174.7:6642
ovsdb_connection_timeout=180
ovsdb_probe_interval=60000
neutron_sync_mode=log
ovn_l3_mode=True
vif_type=ovs
ovn_metadata_enabled=True
enable_distributed_floating_ip=True
dns_servers=
```

plugins/networking-ovn/networking-ovn.ini
``` bash
# Enable distributed floating IP support.
# If True, the NAT action for floating IPs will be done locally and
# not in the centralized gateway. This saves the path to the external
# network. This requires the user to configure the physical network
# map (i.e. ovn-bridge-mappings) on each compute node. (boolean value)
#enable_distributed_floating_ip = false
```

``` bash
  NeutronBridgeMappings:
    description: >
      The OVS logical->physical bridge mappings to use. See the Neutron
      documentation for details. Defaults to mapping br-ex - the external
      bridge on hosts - to a physical name 'datacentre' which can be used
      to create provider networks (and we use this for the default floating
      network) - if changing this either use different post-install network
      scripts or be sure to keep 'datacentre' as a mapping network name.
    type: comma_delimited_list
    default: "datacentre:br-ex"
    tags:
      - role_specific
```

 ovn::controller::ovn_bridge_mappings: NeutronBridgeMappings


overcloud.yaml
  HypervisorNeutronPhysicalBridge:
    default: 'br-ex'
    description: >
      An OVS bridge to create on each hypervisor. This defaults to br-ex the
      same as the control plane nodes, as we have a uniform configuration of
      the openvswitch agent. Typically should not need to be changed.
    type: string
  HypervisorNeutronPublicInterface:
    default: nic1
    description: What interface to add to the HypervisorNeutronPhysicalBridge.
    type: string


---
## test OVN

Sur un controlleur (il faut la bdd onn)


ovn-nbctl ls-list 
# 3c7ba9ca-8f3e-4291-a7e6-e074cebac211 (neutron-0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20)
# 594ab32b-a610-40da-a620-93acd462851c (neutron-cb6074db-0f57-4e82-9d5a-7b583acbeb49)

On retrouve les 2 Réseaux.

ovn-nbctl ls-list 
3c7ba9ca-8f3e-4291-a7e6-e074cebac211 (neutron-0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20)
594ab32b-a610-40da-a620-93acd462851c (neutron-cb6074db-0f57-4e82-9d5a-7b583acbeb49)



sh-4.4# ovn-nbctl show
switch 3c7ba9ca-8f3e-4291-a7e6-e074cebac211 (neutron-0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20) (aka external)
    port fbae0627-608b-42fa-8a3f-23cd0e15dcac
        type: localport
        addresses: ["fa:16:3e:19:82:ac 10.129.176.50"]

    port provnet-f760d2da-080e-4269-b265-7538b41ee9d9
        type: localnet
        addresses: ["unknown"]
    port 427df680-d062-4a97-961e-6e14647ac3c4
        type: router
        router-port: lrp-427df680-d062-4a97-961e-6e14647ac3c4
    port e5085d66-0863-46cc-8a06-b715f1e52239
        type: router
        router-port: lrp-e5085d66-0863-46cc-8a06-b715f1e52239


switch 594ab32b-a610-40da-a620-93acd462851c (neutron-cb6074db-0f57-4e82-9d5a-7b583acbeb49) (aka net-demo)
    port 1260a79b-eaad-4b8b-b12b-3c0dbc62dffa
        addresses: ["fa:16:3e:8c:58:2e 172.16.1.54"]

    port a7d9a860-2691-4dc9-9815-7210d8ab02ec
        type: router
        router-port: lrp-a7d9a860-2691-4dc9-9815-7210d8ab02ec

    port c8b6bf05-0093-403e-85d9-ee29293eb70c
        addresses: ["fa:16:3e:26:ce:58 172.16.1.8"]

    port 6df33cb8-ffd1-47d8-af25-a98eddb599b1
        type: localport
        addresses: ["fa:16:3e:c5:7c:f0 172.16.1.2"]

router 4a256642-37ee-4fe6-afdd-877229b8f107 (neutron-8f85a2dd-2b1e-4b4a-8cd2-289e4c64aa94) (aka routeur-demo2)
    port lrp-427df680-d062-4a97-961e-6e14647ac3c4
        mac: "fa:16:3e:ed:a8:e3"
        networks: ["10.129.179.220/22"]
        gateway chassis: [008a8586-4694-40b3-9aed-9603f090a0c7 c08b6c08-186c-4d91-a4f8-1d262b158483 f387e632-8207-4356-a26e-b219870002f4]

router e90d5696-babc-4d70-b8d1-70467d44425d (neutron-914b8b4d-4138-43e1-94db-d5c2d685ff46) (aka router-demo)
    port lrp-e5085d66-0863-46cc-8a06-b715f1e52239
        mac: "fa:16:3e:b7:85:cf"
        networks: ["10.129.178.115/22"]
        gateway chassis: [c08b6c08-186c-4d91-a4f8-1d262b158483 f387e632-8207-4356-a26e-b219870002f4 008a8586-4694-40b3-9aed-9603f090a0c7]
    port lrp-a7d9a860-2691-4dc9-9815-7210d8ab02ec
        mac: "fa:16:3e:c8:bb:b7"
        networks: ["172.16.1.1/24"]
    nat 47dd21e8-78ee-4665-b340-d6bcc268a103
        external ip: "10.129.178.115"
        logical ip: "172.16.1.0/24"
        type: "snat"
    nat 7b0413d6-9c28-4e2a-9439-918d0f4c4824
        external ip: "10.129.179.39"
        logical ip: "172.16.1.8"
        type: "dnat_and_snat"
    nat 80033f4a-0853-4d71-9e3b-86b08583808b
        external ip: "10.129.177.207"
        logical ip: "172.16.1.54"
        type: "dnat_and_snat"

https://www.openvswitch.org/support/dist-docs-2.5/tutorial/OVN-Tutorial.md.html


ovn-nbctl ls-list
3c7ba9ca-8f3e-4291-a7e6-e074cebac211 (neutron-0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20)
594ab32b-a610-40da-a620-93acd462851c (neutron-cb6074db-0f57-4e82-9d5a-7b583acbeb49)

sh-4.4# ovn-nbctl get Logical_Switch neutron-0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20  external_ids
{"neutron:mtu"="1500", "neutron:network_name"=external, "neutron:revision_number"="2"}

sh-4.4# ovn-nbctl get Logical_Switch neutron-cb6074db-0f57-4e82-9d5a-7b583acbeb49  external_ids
{"neutron:mtu"="1442", "neutron:network_name"=net-demo, "neutron:revision_number"="2"}


https://docs.openstack.org/neutron/latest/contributor/testing/ml2_ovn_devstack.html


openstack network create provider-178 --share \
--provider-physical-network providernet \
--provider-network-type vlan
--provider-segment 178



``` bash
ovn-sbctl show

Chassis "c08b6c08-186c-4d91-a4f8-1d262b158483"
    hostname: overcloud-controller-2.localdomain
    Encap geneve
        ip: "192.168.175.53"
        options: {csum="true"}
    Port_Binding cr-lrp-e5085d66-0863-46cc-8a06-b715f1e52239

Chassis "008a8586-4694-40b3-9aed-9603f090a0c7"
    hostname: overcloud-controller-1.localdomain
    Encap geneve
        ip: "192.168.175.52"
        options: {csum="true"}
    Port_Binding cr-lrp-427df680-d062-4a97-961e-6e14647ac3c4

Chassis "f387e632-8207-4356-a26e-b219870002f4"
    hostname: overcloud-controller-0.localdomain
    Encap geneve
        ip: "192.168.175.51"
        options: {csum="true"}

Chassis "d3ffb6e8-2356-4473-9a30-f8de3bd45664"
    hostname: overcloud-novacompute-2.localdomain
    Encap geneve
        ip: "192.168.175.103"
        options: {csum="true"}

Chassis "70485227-169f-48c2-bc29-2ea1a5f07a77"
    hostname: overcloud-novacompute-0.localdomain
    Encap geneve
        ip: "192.168.175.101"
        options: {csum="true"}
    Port_Binding "c8b6bf05-0093-403e-85d9-ee29293eb70c"

Chassis "82a5bf2b-edbc-496d-a865-11b4bc9c880e"
    hostname: overcloud-novacompute-1.localdomain
    Encap geneve
        ip: "192.168.175.102"
        options: {csum="true"}
    Port_Binding "1260a79b-eaad-4b8b-b12b-3c0dbc62dffa"

```

openstack network list 
+--------------------------------------+----------+--------------------------------------+
| ID                                   | Name     | Subnets                              |
+--------------------------------------+----------+--------------------------------------+
| 0662b75e-3423-4f3f-8ac5-1a5cb9bb3f20 | external | cd238d9d-a277-4b73-b9a2-01d20e8a2d66 |
| cb6074db-0f57-4e82-9d5a-7b583acbeb49 | net-demo | 6b046bfb-d02b-49a8-8df8-02393f7324fe |
+--------------------------------------+----------+--------------------------------------+



openstack baremetal node list
+--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name            | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+
| e4f88d1f-d262-4988-9fc4-23a0eb087183 | rhos-ctrl-br-01 | ca2dac13-6256-4ad4-afda-cf40bd4d8787 | power on    | active             | False       |
| f4c78751-c63e-421a-8eaf-29f11df90753 | rhos-ctrl-br-02 | 9b08db1d-a1ed-455f-8694-c93e21620154 | power on    | active             | False       |
| aee72e41-c6b3-4868-b6ac-69693e371477 | rhos-ctrl-br-03 | 36be3e6b-4633-4c30-a719-34e02c6e8583 | power on    | active             | False       |
| 7c981c9b-da70-4d61-b1cc-3e0bbe10fcfa | rhos-comp-br-01 | fd3faf19-7cc2-401c-a32d-bd8dd333f900 | power on    | active             | False       |
| 53fd5c53-a4e2-47bb-8209-0b8633c2998b | rhos-comp-br-02 | 996e88c7-5d25-4e8b-ad1d-922fca787ee9 | power on    | active             | False       |
| ec0d50b3-98a9-4801-a672-b81e03efc7a3 | rhos-comp-br-03 | ac976252-6e0c-4173-ab86-6281ccca8591 | power on    | active             | False       |
| 7bf267c6-e996-4c86-b389-0a5aee6d125c | rhos-comp-br-07 | None                                 | power off   | available          | False       |
| bb62b740-76f3-45c1-a0fc-421663b661cb | rhos-comp-br-08 | None                                 | power off   | available          | False       |
| 3162ba65-192d-4d74-96d7-792dfafd7ae6 | rhos-comp-br-09 | None                                 | power off   | available          | False       |
| 72504bcb-11f7-49c3-887e-47fb3612cb93 | rhos-comp-br-10 | None                                 | power off   | clean failed       | True        |
| ca75c116-ffa9-4475-b2d9-343f77afe84c | rhos-comp-br-12 | None                                 | power on    | clean failed       | True        |
| 71a03e8c-1fd7-4d91-93f1-37f6952ddcc9 | rhos-comp-br-11 | None                                 | power on    | clean failed       | True        |
+--------------------------------------+-----------------+--------------------------------------+-------------+--------------------+-------------+


undercloud) [stack@rhos-director-br-01 ~]$ openstack server list
+--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+
| ID                                   | Name                    | Status | Networks                | Image          | Flavor  |
+--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+
| 9b08db1d-a1ed-455f-8694-c93e21620154 | overcloud-controller-0  | ACTIVE | ctlplane=10.129.173.51  | overcloud-full | control |
| ca2dac13-6256-4ad4-afda-cf40bd4d8787 | overcloud-controller-2  | ACTIVE | ctlplane=10.129.173.53  | overcloud-full | control |
| 36be3e6b-4633-4c30-a719-34e02c6e8583 | overcloud-controller-1  | ACTIVE | ctlplane=10.129.173.52  | overcloud-full | control |
| fd3faf19-7cc2-401c-a32d-bd8dd333f900 | overcloud-novacompute-0 | ACTIVE | ctlplane=10.129.173.101 | overcloud-full | compute | ->  rhos-comp-br-01 
| 996e88c7-5d25-4e8b-ad1d-922fca787ee9 | overcloud-novacompute-1 | ACTIVE | ctlplane=10.129.173.102 | overcloud-full | compute |
| ac976252-6e0c-4173-ab86-6281ccca8591 | overcloud-novacompute-2 | ACTIVE | ctlplane=10.129.173.103 | overcloud-full | compute |
+--------------------------------------+-------------------------+--------+-------------------------+----------------+---------+


