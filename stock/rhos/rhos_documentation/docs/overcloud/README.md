
- [Contraintes liées à la configuration du nom des Noeud](node-naming-constraints.md)
- [Contraintes liées à la getsion des interfaces sur les noeuds](node-mapping-interfaces.md)

``` bash
 https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_configuration-hooks
"Important
You can only register the NodeUserData resources to one heat template per resource. Subsequent usage overrides the heat template to use."
```