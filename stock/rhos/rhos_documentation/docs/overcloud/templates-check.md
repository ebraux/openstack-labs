# Vérification des templates


---
## NodeUserData

``` bash
"Important
You can only register the NodeUserData resources to one heat template per resource. Subsequent usage overrides the heat template to use."
```

``` bash
grep -R NodeUserData *
# ne doit renvoyer qu'un seul résultat
```
source :

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_configuration-hooks](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/advanced_overcloud_customization/assembly_configuration-hooks)
