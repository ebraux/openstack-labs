

les capabilities sont définie dans le fichier le liste de noeuds.

mais on peut aussi les modifier si beoisn :


Initialisation
``` bash
NODE=rhos-ctrl-br-1
```

Afficher les capabilities des noeuds
``` bash
openstack baremetal node list -f json --long -c Name -c Properties
```

Afficher les capabilities d'un noeud spécifique
``` bash
openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities
#boot_option:local,node:controller1,cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true
```

Définir les capabilities d'un noeud
``` bash
openstack baremetal node set --property capabilities="boot_option:local" $NODE
```


Modifier des capabilities : on utilise les 2 commandes précédentes
- la commande pour définir les capabilities
- la commande pour lire les capabilities, qui permet de lire celle existantes, avant de les modifier/compléter

- Ajouter des données : ex ajout de "node:NOM_DU_SERVEUR" au début de la liste des capabilities
``` bash
openstack baremetal node set --property capabilities="node:${NODE},$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities)" $NODE
```
- Modifier des données existantes. ex remplacer le contenu de "node:xxx".
 - Mettre au point la chaine de remplacement de caractére, avec sed
``` bash
# données facilelement définissables, remplacer le profile, "computedvr" par "compute"
openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/profile:computedvr/profile:compute/g"

# données variables, remplacer le nom du node, "node:<chaine à remplacre>," par le nouveau nom
openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/node:[^,]*/node:${NODE}/g")
```
  - Puis mettre à jour le properties, en utilisant cette chanine de remplacement
``` bash
openstack baremetal node set --property capabilities="$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/node:[^,]*/node:${NODE}/g")" $NODE
```
