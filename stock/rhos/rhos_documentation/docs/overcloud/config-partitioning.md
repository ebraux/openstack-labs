

Le partitionnement du filesystem est fait lors de la phase de provisionnig d'un noeud.
Il n'y a pas de solution pour le modifier par la suite, autre que de le faire manuellement, en bootant en mode single, et en modifiant le configuration ce qui n'est pas recommandé.

Il est réalisé avec l'utilitaire `growvol` , via le playbook `/usr/share/ansible/tripleo-playbooks/cli-overcloud-node-growvols.yaml`.

Dans ce playbook, le partitionnement par défaut est défini dans la variable `role_growvols_args`.

``` yaml
 vars:
    role_growvols_args:
      default:
        /=8GB
        /tmp=1GB
        /var/log=10GB
        /var/log/audit=2GB
        /home=1GB
        /var=100%
      Controller:
        /=8GB
        /tmp=1GB
        /var/log=10GB
        /var/log/audit=2GB
        /home=1GB
        /var=90%
        /srv=10%
      ObjectStorage:
        /=8GB
        /tmp=1GB
        /var/log=10GB
        /var/log/audit=2GB
        /home=1GB
        /var=10%
        /srv=90%
```

Pour modifier le partitionnement, il faut définir un nouveau partitionnement dans la configuration mise en place lors du déploiement, c'est à dire dans le fichier `overcloud-baremetal-deploy.yaml`.

Au niveau soit d'un role ajouter une section `ansible_playbooks`, et surcharger les variables du playbook.

> Pas possible au niveau d'une instance apparement : "Supported way is to do this per role using ansible_playbooks".


Exemple pour tous les noeuds du role 'Controller
``` yaml
- name: Controller
  hostname_format: '%stackname%-controller-%index%'
  count: 3
  defaults:
    network_config:
    ...
      
  ansible_playbooks:
  - playbook: /usr/share/ansible/tripleo-playbooks/cli-overcloud-node-growvols.yaml
    extra_vars:
      role_growvols_args:
        default:
          /=8GB
          /tmp=1GB
          /var/log=100GB
          /var/log/audit=2GB
          /home=1GB
          /var=100%
```

Autre syntaxe
``` yaml
  ansible_playbooks:
    - playbook: /usr/share/ansible/tripleo-playbooks/cli-overcloud-node-growvols.yaml
      extra_vars:
        growvols_args: >
          /=8GB
          /tmp=1GB
          /var/log=10GB
          /var/log/audit=1GB
          /home=1GB
          /srv=10GB
          /var=100%
```

- Correct partitioning should be configured after "openstack overcloud node provision" command was executed for baremetal node.
- Correct partitioning configuration should be provided in overcloud-baremetal-deploy.yaml via ansible_playbooks
- ansible_playbooks for controller role should contain cli-overcloud-node-growvols.yaml 
- extra_vars is ansible_playbooks property and should be defined for each ansible_playbooks appearance if extra variables are needed for mentioned playbooks.
- ansible_playbooks should be defined per role and should contain a list of playbooks. Each playbook in the list should have its own extra_vars property.


---
## Ressource

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/installing_and_managing_red_hat_openstack_platform_with_director/index#proc_provisioning-bare-metal-nodes-for-the-overcloud_ironic_provisioning](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/installing_and_managing_red_hat_openstack_platform_with_director/index#proc_provisioning-bare-metal-nodes-for-the-overcloud_ironic_provisioning)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#ref_bare-metal-node-provisioning-attributes_ironic_provisioning](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#ref_bare-metal-node-provisioning-attributes_ironic_provisioning)