
[https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/using_octavia_for_load_balancing-as-a-service/understand-lb-service_rhosp-lbaas](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/using_octavia_for_load_balancing-as-a-service/understand-lb-service_rhosp-lbaas)


installer les images dans l'undercloud
```bash
sudo dnf install octavia-amphora-image-x86_64.noarch
```
Installe les images necessire pour Octavia dans le dossier /usr/share/openstack-octavia-amphora-images.

``` bash
ls -1 /usr/share/openstack-octavia-amphora-images
  ...
octavia-amphora-latest-16.2.qcow2
octavia-amphora-latest-16.2-x86_64.qcow2
octavia-amphora-latest.qcow2
octavia-amphora.qcow2
 ...
```

# Updating Subscription Management repositories.
# Last metadata expiration check: 0:22:49 ago on sam. 30 sept. 2023 19:24:41 CEST.
# Package octavia-amphora-image-x86_64-16.2-20230412.1.el8ost.noarch is already installed.
# Dependencies resolved.
# Nothing to do.
# Complete!

```
---
## gestion des images 
  OctaviaAmphoraImageFilename:
    description: Filename for the amphora image. Image files are expected to be
                 located in directory /usr/share/openstack-octavia-amphora-images.
                 Using the default of an empty string will cause a distro
                 specific default to be used. (e.g.
                 /usr/share/openstack-octavia-amphora-images/amphora-x64-haproxy.qcow2
                 on CentOS and /usr/share/openstack-octavia-amphora-images/octavia-amphora.qcow2
                 on Red Hat Enterprise Linux).

---
## Gestion des certificats

The Red Hat OpenStack Platform (RHOSP) Load-balancing service (octavia) controller uses the server certificate authority certificates and keys to uniquely generate a certificate for each Load-balancing service instance (amphora).

RHOSP director generates certificates and keys and automatically renews them before
they expire. If you use your own certificates, **you must remember to renew them**.


Use OctaviaCaCert, OctaviaCaKey, OctaviaCaKeyPassphrase, OctaviaClient and OctaviaServerCertsKeyPassphrase cert to configure secure production environments.



3.3. Configuring Load-balancing service certificates and keys

source ~/stackrc

créaer un fichier d'environnement pour Octavia
vi /home/stack/templates/my-octavia-environment.yaml


amphora control subnet 
e load-balancing management network


