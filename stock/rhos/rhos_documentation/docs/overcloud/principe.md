



Générer le fichier de rôles
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller Compute
```
Plus d'infos [oles](./config-roles.md)

Générer la configuration de templater

``` bash
cd /usr/share/openstack-tripleo-heat-templates
./tools/process-templates.py -o /home/stack/openstack-tripleo-heat-templates-rendered -n /home/stack/templates/network_data.yaml -r /home/stack/templates/roles_data.yaml
```

sont généré automatiquement pour la partie réseau :
/usr/share/openstack-tripleo-heat-templates/environments/network-isolation.j2.yaml 




- les données doivnet être inclues après le chargement des template de référence


