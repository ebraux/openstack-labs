


``` yaml
- name: Controller
  count: 1
  hostname_format: controller-%index%
  defaults:
    profile: control
    config_drive:
      cloud_config:
        # unique NFS ID
        bootcmd:
          - 'host_specific_id="$(hostname)-4c102717-5b4b-52f4-9593-8470be25fc7a"'
          - 'echo "options nfs nfs4_unique_id=${host_specific_id}" >/etc/modprobe.d/nfsv4_id.conf'
          - '[ -e /sys/module/nfs/parameters/nfs4_unique_id ] && echo -n ${host_specific_id} >/sys/module/nfs/parameters/nfs4_unique_id'
    network_config:
    ...
```