



https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/overcloud_parameters/ref_compute-nova-parameters_overcloud_parameters


``` bash
#
# Virtual CPU to physical CPU allocation ratio. For more information, refer to
# the documentation. (floating point value)
# Minimum value: 0.0
#cpu_allocation_ratio=<None>
cpu_allocation_ratio=0.0
```
NovaCPUAllocationRatio 


#
# Virtual RAM to physical RAM allocation ratio. For more information, refer to
# the documentation. (floating point value)
# Minimum value: 0.0
#ram_allocation_ratio=<None>
ram_allocation_ratio=1.0

#
# Virtual disk to physical disk allocation ratio. For more information, refer to
# the documentation. (floating point value)
# Minimum value: 0.0
#disk_allocation_ratio=<None>
disk_allocation_ratio=0.0



#
# Number of host CPUs to reserve for host processes. For more information, refer
# to the documentation. (integer value)
# Minimum value: 0
#reserved_host_cpus=0

#
# Amount of memory in MB to reserve for the host so that it is always available
# to host processes. The host resources usage is reported back to the scheduler
# continuously from nova-compute running on the compute node. To prevent the
# host
# memory from being considered as available, this option is used to reserve
# memory for the host. For more information, refer to the documentation.
# (integer value)
# Minimum value: 0
#reserved_host_memory_mb=512
reserved_host_memory_mb=4096

#
# Amount of disk resources in MB to make them always available to host. The
# disk usage gets reported back to the scheduler from nova-compute running
# on the compute nodes. To prevent the disk resources from being considered
# as available, this option can be used to reserve disk space for that host. For
# more information, refer to the documentation. (integer value)
# Minimum value: 0
#reserved_host_disk_mb=0
