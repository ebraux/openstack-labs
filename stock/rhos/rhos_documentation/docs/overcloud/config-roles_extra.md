# roles

Par défaut Director propose des rôles prédéfinis. Pour afficher la liste des rôles disponibles :
``` bash
openstack overcloud role list
```

Les 3 rôles principaux sont :

- Compute
- Controller
- CephStorage



Pour afficher la liste des rôles disponibles
``` bash
openstack overcloud roles list
# Waiting for messages on queue 'tripleo' with no timeout.
# +--------------------------------+
# | Role Name                      |
# +--------------------------------+
# | BlockStorage                   |
# | CellController                 |
# | CephAll                        |
# | CephFile                       |
# | CephObject                     |
# | CephStorage                    |
# | Compute                        |
# | ComputeAlt                     |
# | ComputeDVR                     |
# | ComputeHCI                     |
# | ComputeHCIOvsDpdk              |
# | ComputeInstanceHA              |
# | ComputeLiquidio                |
# | ComputeLocalEphemeral          |
# | ComputeOvsDpdk                 |
# | ComputeOvsDpdkRT               |
# | ComputeOvsDpdkSriov            |
# | ComputeOvsDpdkSriovRT          |
# | ComputePPC64LE                 |
# | ComputeRBDEphemeral            |
# | ComputeRealTime                |
# | ComputeSriov                   |
# | ComputeSriovIB                 |
# | ComputeSriovRT                 |
# | Controller                     |
# | ControllerAllNovaStandalone    |
# | ControllerNoCeph               |
# | ControllerNovaStandalone       |
# | ControllerOpenstack            |
# | ControllerSriov                |
# | ControllerStorageDashboard     |
# | ControllerStorageNfs           |
# | Database                       |
# | DistributedCompute             |
# | DistributedComputeHCI          |
# | DistributedComputeHCIDashboard |
# | DistributedComputeHCIScaleOut  |
# | DistributedComputeScaleOut     |
# | HciCephAll                     |
# | HciCephFile                    |
# | HciCephMon                     |
# | HciCephObject                  |
# | IronicConductor                |
# | Messaging                      |
# | Minimal                        |
# | Networker                      |
# | NetworkerSriov                 |
# | NovaManager                    |
# | Novacontrol                    |
# | ObjectStorage                  |
# | Standalone                     |
# | Telemetry                      |
# | Undercloud                     |
# | UndercloudMinion               |
# +--------------------------------+
```
Ce qui correspond au contenu du dossier`/usr/share/openstack-tripleo-heat-templates/roles`.

Director génére automatique un fichier `/usr/share/openstack-tripleo-heat-templates/roles_data.yaml`, qui contient la liste des rôles par défaut : Controller, Compute, BlockStorage, ObjectStorage, CephStorage

Dans notre cas, nous avons besoin des roles : Controller et ComputeDVR. pour générer la configuration correspondante :
``` bash
openstack overcloud roles generate -o /home/stack/roles_data.yaml Controller Compute
```

Attention : quand on modfier la liste des rôle, il faut ensuite à nouveau générer les fichiers de template heat pour la configuration du réseau.


- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/advanced_overcloud_customization/chap-roles](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/13/html/advanced_overcloud_customization/chap-roles)


### Pour définir un nouveau rôle

Les fichiers de rôles sont stockés dans `/usr/share/openstack-tripleo-heat-templates/roles`.

- Créer un dossier `roles` dans le répertoire de déploiment
- Copier un rôle de base dans ce dossier.
- Le modifier

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-roles-data-file](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.0/html/spine_leaf_networking/configuring-the-overcloud#creating-a-roles-data-file)

