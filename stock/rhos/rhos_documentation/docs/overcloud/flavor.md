---
## création du flavor pour le rôle ComputeDVR

Sur la base du flavo "compute" :

``` bash
openstack flavor create \
  --ram 4096 \
  --disk 40 \
  --ephemeral 0 \
  --vcpus 1 \
  --public \
  --property capabilities="profile='computedvr', resources:CUSTOM_BAREMETAL='1', resources:DISK_GB='0', resources:MEMORY_MB='0', resources:VCPU='0'" \
  computedvr
```