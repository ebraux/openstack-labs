# Suppression de l'overcloud


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_removing-an-overcloud-stack_performing-basic-overcloud-administration-tasks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_removing-an-overcloud-stack_performing-basic-overcloud-administration-tasks)


``` bash
source ~/stackrc
openstack baremetal node list
```
- Instance UUID :f2027c0f-308b-475d-90f3-5b9ea255069a 
-  power on
- Provisioning State     | active    
- 
``` bash
openstack overcloud delete \
  -b /home/stack/templates/deployment-datas.yaml \
  --networks-file  /home/stack/templates/network_data.yaml \
  --network-ports \
  overcloud
```

``` bash
openstack baremetal node list 
# openstack baremetal node list -c Name -c 'Instance UUID' -c 'Power State' -c 'Provisioning State'
```
- Instance UUID : none
- Provisioning State : available

``` bash
rm -rf ~/overcloud-deploy/overcloud
rm -rf ~/config-download/overcloud
```


openstack network list
+--------------------------------------+------------------+--------------------------------------+
| ID                                   | Name             | Subnets                              |
+--------------------------------------+------------------+--------------------------------------+
| 4f0b7187-1e23-4e53-a7e7-10c7e9cb5aea | ctlplane         | b2874002-0cb2-4480-9d62-432506b76b74 |
| dfb15afd-37ad-49ac-979e-6cc583bbee8c | ovn_mac_addr_net |                                      |
+--------------------------------------+------------------+--------------------------------------+


openstack port list
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
| ID                                   | Name | MAC Address       | Fixed IP Addresses                                                           | Status |
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
| 759a40f9-ca4c-4c7e-867a-c43117db9730 |      | fa:16:3e:bb:ab:04 | ip_address='10.129.173.99', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74' | ACTIVE |
+--------------------------------------+------+-------------------+------------------------------------------------------------------------------+--------+
