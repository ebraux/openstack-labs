
---
## Affecter un un profile à un noued

- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_using-director-introspection-to-collect-bare-metal-node-hardware-information_basic](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_using-director-introspection-to-collect-bare-metal-node-hardware-information_basic)

| Nom  | role | profile |
| ---  | ---- | ------- |
| rhos-ctrl-br-[01-03] | Controller | controller |
| rhos-comp-br-[01-12] | ComputeDVR | computedvr |


Initialisation pour un Controller
``` bash
NODE=rhos-ctrl-br-01
PROFILE=control
```

Initialisation pour un compute 
``` bash
NODE=rhos-comp-br-01
PROFILE=compute
```


``` bash
# ajouter à la config le profile et l'option boot local
openstack baremetal node set --property capabilities="profile:${PROFILE},boot_option:local,$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/boot_mode:[^,]*,//g")" $NODE
```


Une fois terminé, afficcher les profils
``` bash
openstack overcloud profiles list
# +--------------------------------------+-----------------+-----------------+-----------------+-------------------+
# | Node UUID                            | Node Name       | Provision State | Current Profile | Possible Profiles |
# +--------------------------------------+-----------------+-----------------+-----------------+-------------------+
# | e4f88d1f-d262-4988-9fc4-23a0eb087183 | rhos-ctrl-br-01 | available       | control         |                   |
# | f4c78751-c63e-421a-8eaf-29f11df90753 | rhos-ctrl-br-02 | available       | None            |                   |
# | aee72e41-c6b3-4868-b6ac-69693e371477 | rhos-ctrl-br-03 | available       | None            |                   |
# | 7bf267c6-e996-4c86-b389-0a5aee6d125c | rhos-comp-br-07 | available       | None            |                   |
# | bb62b740-76f3-45c1-a0fc-421663b661cb | rhos-comp-br-08 | available       | compute         |                   |
# | 3162ba65-192d-4d74-96d7-792dfafd7ae6 | rhos-comp-br-09 | available       | None            |                   |
# | 72504bcb-11f7-49c3-887e-47fb3612cb93 | rhos-comp-br-10 | available       | None            |                   |
# +--------------------------------------+-----------------+-----------------+-----------------+-------------------+
```


Pour remplacer un profil
``` bash
openstack baremetal node set --property capabilities="$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/profile:compute/profile:computedvr/g")" $NODE
openstack baremetal node set --property capabilities="$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/profile:computedvr/profile:compute/g")" $NODE
```
Pour supprimer un profil
``` bash
openstack baremetal node set --property capabilities="$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities | sed "s/profile:compute,//g")" $NODE
```
openstack baremetal node set --property capabilities="node:$NODE,boot_option:local,profile:control,cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true" $NODE
