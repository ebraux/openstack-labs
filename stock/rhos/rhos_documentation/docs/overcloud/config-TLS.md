## getsion du certificats pour accès TLS

Génerer un certificat, en suivant la procédure [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.1/html/advanced_overcloud_customization/assembly_enabling-ssl-tls-on-overcloud-public-endpoints#doc-wrapper]


Configurer le certificat
``` bash
cp -r /usr/share/openstack-tripleo-heat-templates/environments/ssl/enable-tls.yaml /home/stack/templates/.
```

Insérer la trust anchor du certificta
``` bash
cp -r /usr/share/openstack-tripleo-heat-templates/environments/ssl/inject-trust-anchor-hiera.yaml ~/templates/.
```

- [https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/features/ssl.html]
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/12/html/advanced_overcloud_customization/sect-enabling_ssltls_on_the_overcloud](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/12/html/advanced_overcloud_customization/sect-enabling_ssltls_on_the_overcloud)
