


## Configuration des noeuds sur lequels déployer des services

La liste des noeuds est définie dans un fichier au format yaml ou json (`node.yaml` ou `instackenv-initial.json`

Pour chaque noeud les éléments minimum à définir sont :

- name: le nom de la machine
- L'accès à la cartes IPMI
  - pm_addr : adresse IP de la carte IPMI
  - pm_type: le type de driver pour accéder à la carte ipmi
  - pm_user: utilisateur
  - pm_password: mot de passe
- La configuration réseau à mettre e place sur le noeud pour l'accès au réseau de provisioning durant l'introspection
  - ports/address: l'adresse MAC de la carte à utiliser pour la connexion 
  - ports/physical_network : nom de l'interface


D'autres éléments peuvent être définis, soit pour améliorer la getsion des noeuds, soit parfois pour des contraintes de déploiment:

- "arch": "x86_64",
- "cpu": "2",
- "disk": "40",
- "memory": "8192",
- "capabilities": "node:controller0,boot_option:local"


## Selection des noeuds lors du déploiement 

Pour sélectionner sur quel noeud déployer quel service, il faut définir des "capabilities" (critères) qui seront utilisés par "scheduler", via le mécanisme de "scheduler hint".

Et ensuite renseigner ces informations de Scheduler hints dans la configuration de l'overcloud, via les variables  `ControllerSchedulerHints`, `ComputeSchedulerHints`, ...


Une solution consiste à "tagguer" les noeuds, avec les informations de profil et de gabarit.
- soit pendant le déploiement, par rapport à des profiles (profil)
- soit dans la défintion des noeuds , par rapport au nom (node)
La solution retenue consiste à définir les "capabilities" directement dans le fichier de description de noeuds, ce qui permet une affectation précise par machine, et non juste par catégorie de machine.

Dans les 2 cas, il faut s'assurer que la valeur définie dans le "Scheduler hints", corresponde bien à la données qui a été renseigné

Ex, pour les noeud du controle-plabe, au format `rhos-prod-ctrl-br-X` : 
- Définition du noeud de control-plane
``` json
{
    "name": "rhos-prod-ctrl-br-0",
    "arch": "x86_64",
    "cpu": "2",
    "disk": "40",
    "memory": "8192",
    "mac": [ "52:54:00:00:f9:01" ],
    "pm_addr": "172.25.249.101",
    "pm_type": "pxe_ipmitool",
    "pm_user": "admin",
    "pm_password": "password",
    "pm_port": "623",
    "capabilities": "node:rhos-prod-ctrl-br-0,boot_option:local"
},
```
- Définition de `ControllerSchedulerHints`, dans le fichier de template `00-node-info.yaml`
``` bash
  ControllerSchedulerHints: 
    'capabilities:node': 'rhos-prod-ctrl-br-%index%'
```

## Choix du gabarit

`OvercloudControllerFlavor` et `OvercloudComputeFlavor`, ...dans le fichier de template `00-node-info.yaml`.
Des gabarits spécifiques sont définis dans l'undercloud.

Le gabarit générique `baremetal` qui est utilisé, pour plus de simplicité

## Dimensionnement du Cluster

`ControllerCount`, `ComputeCount` dans le fichier de template `00-node-info.yaml`.




| Scheduler name for controllers | 'controller%index%' | |
| Gabarit des controller | baremetal | 
| Nombre de controller | 3 | 00-node-info.yaml |
| Scheduler name for computes | 'compute%index%' | |
| Gabarit des controller | baremetal |  |
| Nombre de compute | 3  | 00-node-info.yaml |

| overcloud endpoint Name | - | 02-custom-domain.yaml |

| Nom des controllers | 'controller%index%' | |
| Nom des computes | 'compute%index%' | |