

How to properly shutdown/restart a Red Hat OpenStack deployment
- [https://access.redhat.com/solutions/1977013](https://access.redhat.com/solutions/1977013)


https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_shutting-down-and-starting-up-the-undercloud-and-overcloud#proc_shutting-down-instances-on-overcloud-compute-nodes_shutting-down-and-starting-up-the-undercloud-and-overcloud

systemctl stop 'tripleo_*'