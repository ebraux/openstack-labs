
``` bash
ansible -i ./overcloud-deploy/overcloud/tripleo-ansible-inventory.yaml all --list
#   hosts (18):
#     undercloud
#     rhos-ctrl-br-01
#     rhos-ctrl-br-02
#     rhos-ctrl-br-03
#     rhos-comp-br-02
#     rhos-comp-br-03
#     rhos-comp-br-04
#     rhos-comp-br-05
#     rhos-comp-br-07
#     rhos-comp-br-08
#     rhos-comp-br-09
#     rhos-comp-br-10
#     rhos-comp-ds-br-01
#     rhos-comp-ds-br-02
#     rhos-comp-ds-br-03
#     rhos-comp-ds-br-04
#     rhos-comp-ds-br-05
#     rhos-comp-ds-br-06
```

``` bash
ansible -i ./overcloud-deploy/overcloud/tripleo-ansible-inventory.yaml neutron_api -m shell -a hostname
# [WARNING]: Could not match supplied host pattern, ignoring: neutron_api
# [WARNING]: No hosts matched, nothing to do
```

``` bash
(undercloud) $ ansible <hosts> -i ./overcloud-deploy/tripleo-ansible-inventory.yaml <playbook> <options>
```

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_running-the-dynamic-inventory-script_performing-basic-overcloud-administration-tasks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_running-the-dynamic-inventory-script_performing-basic-overcloud-administration-tasks)
