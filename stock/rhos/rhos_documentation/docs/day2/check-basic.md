# Commande pour vérifier l'état de l'infra


---
## Openstack

``` bash
openstack network agent list -c 'Agent Type' -c Host -c Alive -c State
# +------------------------------+---------------------------------------+-------+-------+
# | Agent Type                   | Host                                  | Alive | State |
# +------------------------------+---------------------------------------+-------+-------+
# | OVN Controller agent         | rhos-comp-br-08.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-08.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller Gateway agent | rhos-ctrl-br-01.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-05.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-05.overcloud-prod.com | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-10.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-10.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-04.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-04.overcloud-prod.com | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-02.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-02.overcloud-prod.com | :-)   | UP    |
# | OVN Controller Gateway agent | rhos-ctrl-br-02.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-01.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-01.overcloud-prod.com | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-03.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-03.overcloud-prod.com | :-)   | UP    |
# | OVN Controller Gateway agent | rhos-ctrl-br-03.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-09.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-09.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-ds-br-06.overcloud-prod.com | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-ds-br-06.overcloud-prod.com | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-02.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-02.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-03.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-03.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-04.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-04.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-05.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-05.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-07.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-07.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-01.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-01.overcloud-prod.com    | :-)   | UP    |
# | OVN Controller agent         | rhos-comp-br-11.overcloud-prod.com    | :-)   | UP    |
# | OVN Metadata agent           | rhos-comp-br-11.overcloud-prod.com    | :-)   | UP    |
# +------------------------------+---------------------------------------+-------+-------+
```

---
## Ceph

Se connecter à un des serveurs CEPH, et lancer le shell cephadmin
``` bash
ssh RH-cephadm 
# Last login: Sat Nov 18 15:28:18 2023 from 10.129.41.208

[root@rhcs-ceph-br-11 ~]# cephadm shell
# Inferring fsid 9dc844fc-6a76-11ee-bc3e-e4434b831188
# Using recent ceph image registry.redhat.io/rhceph/rhceph-5-rhel8@sha256:ac0d0515974fd001c9aab2d147811a8f4d0655351e0f7f1ecfb4fc9346b36ed2
```

Vérifier l'état de santé du cluster
``` bash
[ceph: root@rhcs-ceph-br-11 /]# ceph -s
#  cluster:
#     id:     9dc844fc-6a76-11ee-bc3e-e4434b831188
#     health: HEALTH_OK
 
#   services:
#     mon: 3 daemons, quorum rhcs-ceph-br-11.imta.fr,rhcs-ceph-br-12,rhcs-ceph-br-13 (age 4h)
#     mgr: rhcs-ceph-br-11.imta.fr.milkle(active, since 8w), standbys: rhcs-ceph-br-12.prhcyu
#     osd: 15 osds: 15 up (since 4h), 15 in (since 4h)
 
#   data:
#     pools:   5 pools, 513 pgs
#     objects: 1.64k objects, 12 GiB
#     usage:   37 GiB used, 18 TiB / 18 TiB avail
#     pgs:     513 active+clean
```