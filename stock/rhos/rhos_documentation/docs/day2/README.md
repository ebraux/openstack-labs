# Exploitation, Day2

- Gestion de la plateforme
    - [Vérifications basiques](./check-basic.md)
    - [Arrêt/redémarrage](./stop-start.md)

- Gestion des Noeuds de compute :
    - [Ajout](./compute-add.md)
    - [Suppression](./compute-remove.md)
    - [Migration](./compute-migrate.md)


- Mise à jour des [certificats](./certificat-update.md) pour Horizon et les API

- Support RedHat :
    - [Portail de support](support-redhat.md)
    - [sos-report] (./sos-report.md)