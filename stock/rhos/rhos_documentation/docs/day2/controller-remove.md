# Remplacement d'un controller

---
## Principe

- Si le contrôleur est encore en service, désactiver les services
- Ensuite, dans tous les cas
    - Vérifier qu'il n'y a pas de traces de l'ancien contrôleur
    - Désactiver la gestion de MariaDB par pacemaker, pour éviter les probl-me de cluster avec les arrêt/redémarrage

Attention, un des contrôleur a un rôle spécifique dans la configuration d'OVN ( `bootstrap`). Il faut l'identifier et le traiter différemment des autres, sinon il y a un risque de perte de connectivités des instances.


doc : 
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_replacing-a-bootstrap-controller-node_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_replacing-a-bootstrap-controller-node_replacing-controller-nodes)


---
## Préparation


Charger l'environnement de l'undercloud : 
``` bash
cd
source stackrc
```

Vérifier l'état du déploiement. 
``` bash
openstack overcloud status
# +------------+-------------------+
# | Stack Name | Deployment Status |
# +------------+-------------------+
# | overcloud  |   DEPLOY_SUCCESS  |
# +------------+-------------------+
```

Installer le client Mysql 
``` bash
sudo dnf -y install mariadb
```

Et configurer l'accès à la base
``` bash
sudo cp /var/lib/config-data/puppet-generated/mysql/root/.my.cnf /root/
```

Faire un backup de la base
``` bash
mkdir -p /home/stack/backup
ll  /home/stack/backup
sudo mysqldump --all-databases --quick --single-transaction | gzip > /home/stack/backup/dump_db_undercloud.sql.gz
ll  /home/stack/backup
```

Vérifier l'espace disque disponible : au moins 10GB
``` bash
df -h
```



---
## Identification du serveur et collecte des informations

Lister les serveurs :
``` bash
metalsmith -c 'Node Name' -c 'Hostname' -c 'State' -c 'IP Addresses' list
# +--------------------+--------------------+--------+-------------------------+
# | Node Name          | Hostname           | State  | IP Addresses            |
# +--------------------+--------------------+--------+-------------------------+
# ...
# | rhos-ctrl-br-02    | rhos-ctrl-br-02    | ACTIVE | ctlplane=10.129.173.52  |
# | rhos-ctrl-br-03    | rhos-ctrl-br-03    | ACTIVE | ctlplane=10.129.173.53  |
# | rhos-ctrl-br-01    | rhos-ctrl-br-01    | ACTIVE | ctlplane=10.129.173.51  |
# ...
```

Créer des variables utilisées par la suite pour les différentes opérations
``` bash
source ~/stackrc 
export SERVER_NAME=rhos-ctrl-br-03
export SERVER_IP=10.129.173.53
# Un autre controlleur que celui a supprimer, pour
#  les commandes de gestion du cluster
export PCS_MANAGER=10.129.173.51
export SERVER_UUID=$(metalsmith -c UUID -f value show $SERVER_NAME)
export CONTROLLERS_IP_LIST='10.129.173.51 10.129.173.52 10.129.173.53'
```



---
## Vérifier l'état du cluster

Suivre la procédure de [Vérification de l'état globale du cluster](./check-cluster.md), et valider le bon fonctionnement de pacemaker, galera, rabbimq et du "fencing" du cluster.

---
## Désactiver la gestion du "fencing" du cluster 

Désactiver le fencing :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property set stonith-enabled=false"
```

Vérifier :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property config stonith-enabled"
# Cluster Properties:
#  stonith-enabled: false
```

---
## Désactivation du contrôleur [Si le contrôleur est toujours actif]

### Désactiver Pacemaker sur le noeud

- Afficher la liste des serveur actif dans pacemaker
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status | grep -w ${SERVER_NAME}"
#   * Online: [ rhos-ctrl-br-01 rhos-ctrl-br-02 rhos-ctrl-br-03 ]
```
- Arrêter pacemaker sur le noeud
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs cluster stop ${SERVER_NAME}"
# rhos-ctrl-br-03: Stopping Cluster (pacemaker)...
# rhos-ctrl-br-03: Stopping Cluster (corosync)...
```
- Vérifier
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status |grep -w ${SERVER_NAME}"
#  * OFFLINE: [ rhos-ctrl-br-03 ]
```


### Arrêter les conteneurs utilisés pour la gestion de NOVA

``` bash
ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_api.service"
ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_api_cron.service"
#ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_compute.service"
ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_conductor.service"
ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_metadata.service"
#ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_placement.service"
ssh tripleo-admin@${SERVER_IP} "sudo systemctl stop tripleo_nova_scheduler.service"
```




---
## Gestion de pacemaker [Uniquement si suppression définitive]

Supprimer le noeud de la configuration de pacemaker
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs cluster node remove ${SERVER_NAME} --skip-offline --force"
# Destroying cluster on hosts: 'rhos-ctrl-br-03'...
# rhos-ctrl-br-03: Successfully destroyed cluster
# Sending updated corosync.conf to nodes...
# rhos-ctrl-br-01: Succeeded
# rhos-ctrl-br-02: Succeeded
# rhos-ctrl-br-01: Corosync configuration reloaded
```

!!! Ne surtout pas faire ça !!!
sinon ensuite c'est la galère, il faut manuellement [réintégrer un neoud au cluster pcs](../debug/pacemaker.md)
Supprimer le noeud de la liste des noeuds connus par pacemaker
``` bash
# ssh tripleo-admin@${PCS_MANAGER} "sudo pcs host deauth ${SERVER_NAME}"
```

Supprimer la ressource de stonith

- Lister les ressources de type stonith
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs stonith status"
#   * stonith-fence_ipmilan-00620bade462	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
#   * stonith-fence_ipmilan-00620bad1430	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
#   * stonith-fence_ipmilan-00620bac48b4	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-02

# Fencing Levels:
#  Target: rhos-ctrl-br-01
#    Level 1 - stonith-fence_ipmilan-00620bac48b4
#  Target: rhos-ctrl-br-02
#    Level 1 - stonith-fence_ipmilan-00620bade462
#  Target: rhos-ctrl-br-03
#    Level 1 - stonith-fence_ipmilan-00620bad1430
```
- Supprimer celle correspondant au contrôleur à supprimer
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs stonith delete stonith-fence_ipmilan-00620bad1430"
```

  
Vérifier que les ressources fonctionnent toujours correctement
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
```

---
## Gestion du cluster Galera [Dans tous les cas]

Pour éviter que Pacemaker ne bloque le fonctionnement de galera car le cluster n'est pas pleinement opérationnel, désactiver la gestion de Galera par pacemaker.
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource unmanage galera-bundle"
```

Vérifier
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
#    * Container bundle set: galera-bundle [cluster.common.tag/mariadb:pcmklatest] (unmanaged): 
#     * galera-bundle-0	(ocf:heartbeat:galera):	 Promoted rhos-ctrl-br-01 (unmanaged)
#     * galera-bundle-1	(ocf:heartbeat:galera):	 Stopped (unmanaged) 
#     * galera-bundle-2	(ocf:heartbeat:galera):	 Promoted rhos-ctrl-br-02 (unmanaged)
```

---
## Gestion de OVN  [Si le contrôleur est toujours actif]


Pas très clair sur ce qu'il faudrait faire avec un serveur HS :

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_preparing-the-cluster-for-controller-node-replacement_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_preparing-the-cluster-for-controller-node-replacement_replacing-controller-nodes)
    - 7. Remove the OVN northbound database server for the replaced Controller node from the cluster

### OVN northbound database server

Obtenir l'ID de la "OVN northbound database server" du serveur a remplacer
``` bash
ssh tripleo-admin@${SERVER_IP} sudo podman exec ovn_cluster_north_db_server ovs-appctl -t /var/run/ovn/ovnnb_db.ctl cluster/status OVN_Northbound 2>/dev/null|grep -A4 Servers:
# Servers:
#     6599 (6599 at tcp:192.168.174.51:6643) last msg 1026 ms ago
#     8bb7 (8bb7 at tcp:192.168.174.52:6643) last msg 14107704 ms ago
#     2110 (2110 at tcp:192.168.174.53:6643) (self)
```
L'id est celui indiqué par "self", quand on est sur le serveur. ici 2110
``` bash
export SERVER_OVN_NORTHBOUND_DATABASE_ID=2110
```
Sinon, il faut se connecter sur tous les serveurs actifs, et relever à quie corresoindnt les différents ID. 

Supprimer la base
``` bash
ssh tripleo-admin@${SERVER_IP} sudo podman exec ovn_cluster_north_db_server ovs-appctl -t /var/run/ovn/ovnnb_db.ctl cluster/kick OVN_Northbound ${SERVER_OVN_NORTHBOUND_DATABASE_ID}
```


### OVN southbound database server

Obtenir l'ID de la "OVN southbound database server" du serveur a remplacer
``` bash
ssh tripleo-admin@${SERVER_IP} sudo podman exec ovn_cluster_south_db_server ovs-appctl -t /var/run/ovn/ovnsb_db.ctl cluster/status OVN_Southbound 2>/dev/null|grep -A4 Servers:
# Servers:
#     73f2 (73f2 at tcp:192.168.174.53:6644) (self)
#     c3c5 (c3c5 at tcp:192.168.174.51:6644) last msg 4262 ms ago
#     3727 (3727 at tcp:192.168.174.52:6644) last msg 14670276 ms ago
```
L'id est celui indiqué par "self", quand on est sur le serveur. ici 73f2
``` bash
export SERVER_OVN_SOUTHBOUND_DATABASE_ID=73f2
```
Sinon, il faut se connecter sur tous les serveurs actifs, et relever à qui correspondent les différents ID. 

Supprimer la base
``` bash
ssh tripleo-admin@${SERVER_IP} sudo podman exec ovn_cluster_south_db_server ovs-appctl -t /var/run/ovn/ovnsb_db.ctl cluster/kick OVN_Southbound ${SERVER_OVN_SOUTHBOUND_DATABASE_ID}
```

### Eviter que le noeud ne rejoingne le cluster

Désactiver les services d'OVN
``` bash
ssh tripleo-admin@${SERVER_IP} sudo systemctl disable --now tripleo_ovn_cluster_south_db_server.service tripleo_ovn_cluster_north_db_server.service
# Removed "/etc/systemd/system/multi-user.target.wants/tripleo_ovn_cluster_north_db_server.service".
# Removed "/etc/systemd/system/multi-user.target.wants/tripleo_ovn_cluster_south_db_server.service".
```

Supprimer les bases de données
``` bash
ssh tripleo-admin@${SERVER_IP} sudo rm -rfv /var/lib/openvswitch/ovn/.ovn* /var/lib/openvswitch/ovn/ovn*.db
# removed '/var/lib/openvswitch/ovn/.ovnnb_db.db.~lock~'
# removed '/var/lib/openvswitch/ovn/.ovnsb_db.db.~lock~'
# removed '/var/lib/openvswitch/ovn/ovnnb_db.db'
# removed '/var/lib/openvswitch/ovn/ovnsb_db.db'
```

---
## Opération à réaliser en cas de remplacement de l'OVN bootstrap Node


Identification du controlleur de bootstrap
``` bash
ssh tripleo-admin@ctrl1 "sudo hiera -c /etc/puppet/hiera.yaml ovn_dbs_short_bootstrap_node_name"
# rhos-ctrl-br-01

ssh tripleo-admin@ctrl2 "sudo hiera -c /etc/puppet/hiera.yaml ovn_dbs_short_bootstrap_node_name"
# rhos-ctrl-br-01

ssh tripleo-admin@ctrl3 "sudo hiera -c /etc/puppet/hiera.yaml ovn_dbs_short_bootstrap_node_name"
# rhos-ctrl-br-01
```
Ici, le contrôleur de bootstrap est donc rhos-ctrl-br-01


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_replacing-a-bootstrap-controller-node_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_replacing-controller-nodes#proc_replacing-a-bootstrap-controller-node_replacing-controller-nodes)
- [https://access.redhat.com/solutions/5662621](https://access.redhat.com/solutions/5662621)



Add a template file to override pacemaker_short_bootstrap_node_name and mysql_short_bootstrap_node_name hieradata, so that bootstrap tasks are executed in one of the existing controller nodes.

Modifier le contenu de la template `templates/prod-environment/99-extraconfig.yaml`
``` yaml
  parameter_defaults:
    ExtraConfig:
      pacemaker_short_bootstrap_node_name: rhos-ctrl-br-02
      mysql_short_bootstrap_node_name: rhos-ctrl-br-02
```
Activer cette template dans `templates/overcloud-answers.yaml`

Replace NODE_NAME with the name of an existing Controller node that you want to use in bootstrap operations after the replacement process.

!! attention, si `ExtraConfig` est déjà utilisé dan sune autre template, il sera ignoré, ou écrasera l'existant (c'est le dernier qui gagne)


- Vérifier à la fin : [https://bugzilla.redhat.com/show_bug.cgi?id=2222543](https://bugzilla.redhat.com/show_bug.cgi?id=2222543).

---
## Dé-provisionnig du noeud [Dans tous les cas]


Modifier le fichier `template/overcloud-baremetal-deploy.yaml`, contenant le descriptif de l'infra, pour réduire le nombre de contrôleur. 
``` yaml
- name: Controller
  hostname_format: '%stackname%-controller-%index%'
  count: 2
  defaults:
  ...
```

Dans ce même fichier, identifier la description du compute à supprimer, et modifier son attribut `provisioned` à `false`
``` yaml
  - hostname: rhos-ctrl-br-03
    name: rhos-ctrl-br-03
    provisioned: false
    resource_class: baremetal
    networks:
    ...
```

Et lancer la commande de suppression
``` bash
cd ~/scripts
./node-delete.sh 
```

Vérifier la suppression 
``` bash
openstack baremetal node show ${SERVER_NAME} -f json -c provision_state -c power_state
{
  "power_state": "power off",
  "provision_state": "available"
}
```

> Si la suppression fait suite à une panne du serveur, le `provision_state` peut être à `error`. Ce n'est pas important. L'important c'est qu'aucune `Instance UUID` (instance d'overcloud active) ne soit associées au serveur baremetal.


Le noeud est désormais un noeud banalisé dans l'infrastrsucture Undercloud. Appliquer la [procédure de suppression d'un noeud](./node-remove.md)

---
## Ré-activer les services du cluster

### Galera

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource refresh galera-bundle"
# Cleaned up galera-bundle-podman-0 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-0 on rhos-ctrl-br-01
# Cleaned up galera-bundle-0 on rhos-ctrl-br-02
# Cleaned up galera-bundle-0 on rhos-ctrl-br-01
# Cleaned up galera-bundle-podman-1 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-1 on rhos-ctrl-br-01
# Cleaned up galera-bundle-1 on rhos-ctrl-br-02
# Cleaned up galera-bundle-1 on rhos-ctrl-br-01
# Cleaned up galera-bundle-podman-2 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-2 on rhos-ctrl-br-01
# Cleaned up galera-bundle-2 on rhos-ctrl-br-02
# ... got reply
# Cleaned up galera-bundle-2 on rhos-ctrl-br-01
# Cleaned up galera:0 on galera-bundle-0
# Cleaned up galera:2 on galera-bundle-2
# Configuration prevents cluster from stopping or starting unmanaged 'galera-bundle'
# Waiting for 13 replies from the controller
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply (done)
```

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource manage galera-bundle"
```

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
```

### Stonith

!!! Attention, ne ré-activer le stonith que si le cluster disose de 3 noeuds. Sinon le cluster Galera se bloque.

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property set stonith-enabled=true"
```

### Pacemaker
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
```


---
## Au niveau du cluster Openstack (overcloud)


Charger l'environnement "overcloud" :
``` bash
source ~/overcloud-deploy/overcloud/overcloudrc
```

### Ménage au niveau des agents réseau

Vérifier les "network agent"
``` bash
openstack network agent list | grep XXX
```

Supprimer les agents dont Alive et 'xxx" et le "Host" correspond :

- à "("Chassis" register deleted)"
- au nom du controlleur venant d'être supprimé

``` bash
openstack network agent delete <ID>
```

### Ménage au niveau du service Cinder

``` bash
openstack volume service list
```

Supprimer les enrtées pour le service "cinder-backup" (normalement non actif)
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo podman exec -it cinder_api cinder-manage service remove cinder-backup ${SERVER_NAME}"
# Host not found. Failed to remove cinder-backup on rhos-ctrl-br-03.
# Service cinder-backup could not be found on host rhos-ctrl-br-03.
```
Supprimer les enrtées pour le service "cinder-scheduler"
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo podman exec -it cinder_api cinder-manage service remove cinder-scheduler  ${SERVER_NAME}"
# Service cinder-scheduler on host rhos-ctrl-br-03 removed.
```

### Cluster RabbitMQ

!! attention ne pas faire

Afficher la configuration du cluster RabitMQ
``` bash
ssh tripleo-admin@${PCS_MANAGER}  "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
```

Si des références au noeud venant d'être supprimé existent toujours, utiliser la commande
``` bash
ssh tripleo-admin@${PCS_MANAGER}  "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl forget_cluster_node <node_name>
```



---
## Validation

Lancer un provisioning : 
``` bash
cd ~/scripts
./03-node-provision.sh 
```
> Le provisioning va permettre de mettre à jour la base de référence du moteur de déploiement heat.

Suivi d'un déploiement : 
``` bash
./04-deploy.sh
```
