# Ajouter un noeud au cluster

---
## Principe

La méthode pour ajouter un noeud est la même, quel que soit le types de noeud (Controller, Compute, ...).

La différence se fait ensuite au moment du provisioning.

Attention à bien respecter les normes de nommage, pour rester cohérent avec le nommage des neouds en fonction de leur rôle  une fois provisionnés

---
## Ajouter un noeud `baremetal`

``` bash
source ~/stackrc
```

Editer les fichier contenant la liste des noeuds, `~/nodes/nodes.yaml`, pour ajouter le noeud :
``` yaml
  - name: "rhos-comp-br-06"
    arch: "x86_64"
    ports:
      - address: "e0:db:55:1d:15:b8"
        physical_network: ctlplane
    pm_type: "ipmi"
    pm_user: "root"
    pm_password: "xxxxxx"
    pm_addr: "10.29.20.106"
    capabilities: "node:rhos-comp-br-06,boot_option:local"
```
- Attention, le nom dans `name`, et dans `capabilities:node` doivent correspondre
- Bien modifier et valider l'accès à la carte ipmi
- Remplacer "xxxxxx" par le mot de passe valide
- Bien vérifier l'adresse MAC de la carte qui servira à booter en PXE dans `ports:address`


Importer le serveur

``` bash
openstack overcloud node import /home/stack/nodes/nodes.yaml
# Node UUID e3c458b3-2b49-4f47-9824-78c74171d1bb is already registered
# Node UUID 783831e8-ba46-40ed-9b89-53740cf54946 is already registered
# Successfully registered node UUID bae23030-8bb8-4656-84f6-c511f9f4eba5
# ...
```

Créer des variables utilisées par la suite pour les différentes opérations
``` bash 
export SERVER_NAME=rhos-ctrl-br-03
```

Vérifier que l'import s'est correctement déroulé. Le status du noeud doit être `manageable`.
``` bash
openstack baremetal node show -f json -c name -c power_state -c provision_state -c maintenance ${SERVER_NAME}
# {
#   "maintenance": false,
#   "name": "rhos-ctrl-br-03",
#   "power_state": "power off",
#   "provision_state": "manageable"
# }

```


Le noeud doit être :

- Power State : power off
- Provisioning State : manageable
- Maintenance : False



Lancer l'introspection pour rendre le noeud comme disponible pour l'overcloud
``` bash 
openstack overcloud node introspect ${SERVER_NAME}  --provide 
# ...
```

Le noeud doit :

- s'éteindre, si il était allumé
- démarrer
- booter en pxe, sur l'image d'introspection
- s'éteindre une fois l'introspection terminée

Vérifier que l'introspection s'est correctement déroulée. Le status du noeud doit être `available`.
``` bash
openstack baremetal node show -f json -c name -c power_state -c provision_state -c maintenance ${SERVER_NAME}
# {
#   "maintenance": false,
#   "name": "rhos-ctrl-br-03",
#   "power_state": "power off",
#   "provision_state": "available"
# }```


