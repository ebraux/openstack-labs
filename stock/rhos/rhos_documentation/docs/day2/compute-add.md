
# Ajout d'un noeud


Provisionner le noeud dans l'overcloud
``` bash
cd scripts/
./03-node-provision.sh
```

- Le noeud doit :
  - Démarrer
  - booter en pxe, sur l'image de RHEL9.2
  - rebooter, et démarre sur le disque local
  - avoir le hostname `rhos-comp-br-06`
- Ensuite Ansible est utilisé pour appliquer la configuration du noeud
  - configuration du réseau avec os-net-config
  - 
- 
- s'éteindre une fois l'introspection terminée

---
##


Déclarer  le noeud
``` yaml
  - hostname: rhos-comp-br-06
    name: rhos-comp-br-06
    provisioned: true
    resource_class: baremetal
    networks:
    - network: ctlplane
      vif: true
      fixed_ip: 10.129.173.106
    - network: internal_api
      subnet: internal_api_subnet      
      fixed_ip: 192.168.174.106
    - network: tenant
      subnet: tenant_subnet
      fixed_ip: 192.168.175.106
    - network: storage
      subnet: storage_subnet
      fixed_ip: 192.168.176.106
    - network: external
      subnet: external_subnet
      fixed_ip: 10.129.176.26
    network_config:
      net_config_data_lookup:
        rhos-comp-br-06:
          nic1: "e0:db:55:1d:15:b8"
          nic2: "e0:db:55:1d:15:b9"
          nic3: "e0:db:55:1d:15:ba"
          nic4: "00:0a:f7:7d:2f:b8"
          nic5: "00:0a:f7:7d:2f:b9"
```

- nom doit être identique pour les 3 entrées `hostname:`, `name:` et `network_config:net_config_data_lookup:`
- vérifier que les adresse IP des subnets `ctlplane`, `internal_api`, `tenant` et `storage` respectent bien la règle fixée, et que les IP finissent bien par le numéro correspondant au serveur
- vérifier que l'adresse IP du subnet `external`, respecte bien la règle spécifique à ce réseau, et que l'IP finit bien par le numéro correspondant au serveur
- vérifier la liste des 5 interfaces réseau nic1 à nic5 dans `network_config:net_config_data_lookup:`
- vérifier que  `provisioned: true`

Increment the count parameter for the roles that you want to scale up. 
``` yaml
- name: Compute
  hostname_format: '%stackname%-compute-%index%'
  count: 10
  defaults:
```


Vérifier que l'intégration s'est correctement déroulée, Le status du noeud doit être `active`, et un uid d'instance doit lui avoir été associé.
``` bash
openstack baremetal node list -c Name -c '' -c 'Power State' -c 'Provisioning State' -c 'Maintenance' | grep rhos-comp-br-06
# | rhos-comp-br-06    | power off   | available          | False       |
``````