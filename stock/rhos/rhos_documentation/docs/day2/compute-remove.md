# Supprimer un noeud de compute

---
## Mise en garde

Attention, les opération doivent être faite dans l'ordre sans se tromper.

Sinon, il faut rattraper la situation en mode manuel, et c'est tout de suite beaucoup plus complexe.

Les opération sont a réaliser depuis le serveur "Director".


---
## Désactivation du noeud dans Openstack (overcloud)

Charger l'environnement "overcloud" :
``` bash
source ~/overcloud-deploy/overcloud/overcloudrc
```

Pour lister les noeuds déclarés dans le cluster :
``` bash
openstack hypervisor list -c 'Hypervisor Hostname' -c 'State'
# +---------------------------------------+-------+
# | Hypervisor Hostname                   | State |
# +---------------------------------------+-------+
# | rhos-comp-br-09.overcloud-prod.com    | up    |
# | rhos-comp-ds-br-01.overcloud-prod.com | up    |
# ...
```
Identifier/valider le nom du neoud à désactiver, et son état.

Pour simpleifier les manipulations futures, déclarer le nom de ce noeud dans une variable. Exemple pour `rhos-comp-br-11.overcloud-prod.com`. 
``` bash
export NODE_TO_REMOVE=rhos-comp-br-11
```

C'est un compute, le seul service est `nova-compute`.
``` bash
openstack compute service list -c Binary -c Host -c Status | grep ${NODE_TO_REMOVE}.overcloud-prod.com
# | nova-compute   | rhos-comp-br-11.overcloud-prod.com    | enabled |
```

Désactivation du service
``` bash
openstack compute service set ${NODE_TO_REMOVE}.overcloud-prod.com nova-compute --disable

openstack compute service list -c Binary -c Host -c Status | grep ${NODE_TO_REMOVE}.overcloud-prod.com
# | nova-compute   | rhos-comp-br-11.overcloud-prod.com    | disabled |
```

Vérifier qu'aucune instance ne tourne sur ce serveur, sinon il faut les migrer, ou les supprimer.
``` bash
openstack server list --host ${NODE_TO_REMOVE}.overcloud-prod.com --all
#
```

---
## Opération au niveau Undercloud


Chargement de l'environnement de type Undercloud
``` bash
source ~/stackrc
```

Vérifier que le noeud à supprimer est bien dans a liste des serveurs gérés par l'undercloud
``` bash
openstack baremetal node list
```

Modifier le fichier `template/overcloud-baremetal-deploy.yaml`, contenant le descriptif de l'infra, pour réduire le nombre de compute. Attention à bien identifier le type de compute, entre `Compute`, `ComputeDS`, ...
``` yaml
- name: Compute
  hostname_format: '%stackname%-compute-%index%'
  count: 9
  defaults:
  ...
```

Dans ce même fichier, identifier la description du compute à supprimer, et modifier son attribut `provisioned` à `false`
``` yaml
  - hostname: rhos-comp-br-11
    name: rhos-comp-br-11
    provisioned: false
    resource_class: baremetal
    networks:
    ...
```

Et lancer la commande de suppression
``` bash
cd ~/scripts
./node-delete.sh 
```

Vérifier la suppression 
``` bash
openstack baremetal node list
```

Et surtout 
``` bash
openstack baremetal node show ${NODE_TO_REMOVE} -f json -c provision_state -c power_state
{
  "power_state": "power off",
  "provision_state": "available"
}
```
> Si la suppression fait suite à une panne du serveur, le `provision_state` peut être à `error`. Ce n'est pas important. L'important c'est qu'aucune `Instance UUID` (instance d'overcloud active) ne soit associées au serveur baremetal.


Le noeud est désormais un noeud banalisé dans l'infrastructure Undercloud. Appliquer la [procédure de suppression d'un noeud](./node-remove.md)

---
## Validation

Lancer un provisioning : 
``` bash
cd scripts
./03-node-provision.sh 
```
> Le provisioning va permettre de mettre à jour la base de référence du moteur de déploiement heat.

Suivi d'un déploiement : 
``` bash
./04-deploy.sh
```

---
## Sources 

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_scaling-overcloud-nodes#proc_removing-or-replacing-a-compute-node_scaling-overcloud-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_scaling-overcloud-nodes#proc_removing-or-replacing-a-compute-node_scaling-overcloud-nodes)