# Ajouter un contrôler


---
## Valider le bon fonctionnement de l'overcloud

Afin de s'assurer que l'Overcloud est en mesure d'accueillir un nouveau noeud

1. Vérifier l'état du cluster

Suivre la procédure de [Vérification de l'état globale du cluster](./check-cluster.md), et valider le bon fonctionnement de pacemaker, galera, rabbimq et du "fencing" du cluster.


2. Appliquer les procédures de provisioning et déploiement de l'Overcloud

- Lancer un provisioning : 
``` bash
cd ~/scripts
./03-node-provision.sh 
```
> Le provisioning va permettre de mettre à jour la base de référence du moteur de déploiement heat.

- Suivi d'un déploiement : 
``` bash
./04-deploy.sh
```

- Vérifier l'état du déploiement. 
``` bash
openstack overcloud status
# +------------+-------------------+
# | Stack Name | Deployment Status |
# +------------+-------------------+
# | overcloud  |   DEPLOY_SUCCESS  |
# +------------+-------------------+
```

---
## Ajouter le noeud à l'undercloud

Appliquer la procédure pour ajouter le noeud aux serveurs baremetal gérés par l'undercloud : [ajouter un noeud à l'undercloud](./node-add.md)


---
## Préparer l'environnement

Charger l'environnement de l'undercloud : 
``` bash
cd
source stackrc
```

Installer le client Mysql 
``` bash
sudo dnf -y install mariadb
```

Et configurer l'accès à la base
``` bash
sudo cp /var/lib/config-data/puppet-generated/mysql/root/.my.cnf /root/
```

Faire un backup de la base
``` bash
mkdir -p /home/stack/backup
ll  /home/stack/backup
sudo mysqldump --all-databases --quick --single-transaction | gzip > /home/stack/backup/dump_db_undercloud.sql.gz
ll  /home/stack/backup
```

Vérifier l'espace disque disponible : au moins 10GB
``` bash
df -h
```
Créer des variables utilisées par la suite pour les différentes opérations
``` bash
# Un autre controlleur que celui a supprimer, pour
#  les commandes de gestion du cluster
export PCS_MANAGER=10.129.173.51
export SERVER_NAME=rhos-ctrl-br-03
export SERVER_IP=10.129.173.53

```

---
## Désactiver la gestion du "fencing" du cluster 

Désactiver le fencing :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property set stonith-enabled=false"
```

Vérifier :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property config stonith-enabled"
# Cluster Properties:
#  stonith-enabled: false
```


---
## Gestion du cluster Galera 

Pour éviter que Pacemaker ne bloque le fonctionnement de galera car le cluster n'est pas pleinement opérationnel, désactiver la gestion de Galera par pacemaker.
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource unmanage galera-bundle"
```

Vérifier
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
#   * Container bundle set: galera-bundle [cluster.common.tag/mariadb:pcmklatest] (unmanaged):
#     * galera-bundle-0	(ocf:heartbeat:galera):	 Promoted rhos-ctrl-br-02 (unmanaged)
#     * galera-bundle-1	(ocf:heartbeat:galera):	 Promoted rhos-ctrl-br-01 (unmanaged)
```

---
## Déclarer le noeud dans la configuration de l'overcloud


Modifier le fichier `template/overcloud-baremetal-deploy.yaml`, contenant le descriptif de l'infra, pour incrémenter le nombre de contrôleur. 
``` yaml
- name: Controller
  hostname_format: '%stackname%-controller-%index%'
  count: 3
  defaults:
  ...
```

Dans ce même fichier, ajouter la description du contrôleur a ajouter. 
``` yaml
  - hostname: rhos-ctrl-br-03
    name: rhos-ctrl-br-03
    provisioned: false
    resource_class: baremetal
    networks:
    ...
```
Bien vérifier :

- nom doit être identique pour les 3 entrées `hostname:`, `name:` et `network_config:net_config_data_lookup:`
- vérifier que les adresse IP des subnets `ctlplane`, `internal_api`, `tenant` et `storage` respectent bien la règle fixée, et que les IP finissent bien par le numéro correspondant au serveur
- vérifier que l'adresse IP du subnet `external`, respecte bien la règle spécifique à ce réseau, et que l'IP finit bien par le numéro correspondant au serveur
- vérifier la liste des 5 interfaces réseau nic1 à nic5 dans `network_config:net_config_data_lookup:`
- vérifier que  `provisioned: true`
- Que les adresses MAC sont uniques
- Que l'adresse MAC de correspond bien à celle déclarée lors de l'introspection


---
## Provisionner le noeud dans l'overcloud
``` bash
cd ~/scripts/
./03-node-provision.sh
```

- Le noeud doit :
  - Démarrer
  - Booter en pxe, sur l'image de RHEL9.2
  - Rebooter, et démarre sur le disque local
  - Avoir la bonne configuration pour être prêt pour le déploiment des composants d'Openstack : : hostname, partitions, configuration réseau, ...

metalsmith list
+--------------------------------------+--------------------+--------------------------------------+--------------------+--------+-------------------------+
| UUID                                 | Node Name          | Allocation UUID                      | Hostname           | State  | IP Addresses            |
+--------------------------------------+--------------------+--------------------------------------+--------------------+--------+-------------------------+
| 8d9a56fa-156b-4e93-8b50-24b0f9aa8362 | rhos-comp-br-09    | a8e48e27-a823-429f-9fb3-c9f784cec015 | rhos-comp-br-09    | ACTIVE | ctlplane=10.129.173.109 |
| ace3efdb-3f59-4d3a-a163-fb7ab2087901 | rhos-comp-ds-br-02 | 50c6dcff-8941-4c89-bed6-c04f6b8afc26 | rhos-comp-ds-br-02 | ACTIVE | ctlplane=10.129.173.122 |
| 1ee4d0ae-a94f-4910-9db5-74b024376062 | rhos-comp-ds-br-01 | 43d8a776-5cfb-4a81-91ee-dea2aab1420f | rhos-comp-ds-br-01 | ACTIVE | ctlplane=10.129.173.121 |
| a7c053eb-77a3-42e1-b8c4-63cebb1382e8 | rhos-ctrl-br-02    | bcfac8b7-3e69-4c37-a04a-a074233ff93a | rhos-ctrl-br-02    | ACTIVE | ctlplane=10.129.173.52  |
| a7b918d0-492f-44c1-b6a7-9a8d5a2ce7d8 | rhos-ctrl-br-01    | 528eff38-060c-48fe-9b87-9784ff05f5e6 | rhos-ctrl-br-01    | ACTIVE | ctlplane=10.129.173.51  |
| e1e081eb-a23b-4048-8904-209a0cd0fd0a | rhos-comp-br-02    | 4dc4a52b-80dc-4c33-8e82-61c261f7d7cb | rhos-comp-br-02    | ACTIVE | ctlplane=10.129.173.102 |
| 622ffd24-03d5-49ee-9160-6dd14b2c8915 | rhos-comp-br-03    | 98fc4b42-5ff6-4f3c-94f2-49efdb9b8991 | rhos-comp-br-03    | ACTIVE | ctlplane=10.129.173.103 |
| e3c458b3-2b49-4f47-9824-78c74171d1bb | rhos-comp-br-04    | d34d6dc1-f627-4696-becc-9f6f8d39a38e | rhos-comp-br-04    | ACTIVE | ctlplane=10.129.173.104 |
| 783831e8-ba46-40ed-9b89-53740cf54946 | rhos-comp-br-05    | 59ff9bfd-c0c4-4eab-bd17-4c455e21a08b | rhos-comp-br-05    | ACTIVE | ctlplane=10.129.173.105 |
| c4c64f84-1705-4542-9e6e-73942e039136 | rhos-comp-ds-br-03 | 0189da25-e2bf-4bb7-b567-0397cffe9d07 | rhos-comp-ds-br-03 | ACTIVE | ctlplane=10.129.173.123 |
| 10048b86-bb49-4ecd-926c-10c33e922f66 | rhos-comp-ds-br-05 | f462dcd5-63ba-4b6b-8301-b8bbd5c3192e | rhos-comp-ds-br-05 | ACTIVE | ctlplane=10.129.173.125 |
| 56401303-a266-402c-800a-4eb63ba96512 | rhos-comp-br-08    | 86a2a3ad-9fad-496a-b1b3-e2785765a7f8 | rhos-comp-br-08    | ACTIVE | ctlplane=10.129.173.108 |
| 5146726e-cf3a-4631-8adf-bac51552927b | rhos-comp-br-01    | 8ed7e5c1-746b-4379-878e-f3c2c1d7f79a | rhos-comp-br-01    | ACTIVE | ctlplane=10.129.173.101 |
| 9854406a-5d4a-430f-8947-6176489b0ab1 | rhos-comp-br-07    | 8b2bf118-db01-4832-ae0e-1d5ebb60e959 | rhos-comp-br-07    | ACTIVE | ctlplane=10.129.173.107 |
| b6ca61f5-4c79-4118-a495-9ade4944f0aa | rhos-comp-ds-br-04 | a7860a39-bfe8-4917-9f5a-d575f93ead6f | rhos-comp-ds-br-04 | ACTIVE | ctlplane=10.129.173.124 |
| 692be1e1-3fce-4210-8cdd-9fe7079fbfea | rhos-comp-br-10    | 3ba32388-1026-47d9-9903-fd742da77866 | rhos-comp-br-10    | ACTIVE | ctlplane=10.129.173.110 |
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | b994cf49-1bcd-4745-81c0-95a60cff9f62 | rhos-comp-ds-br-06 | ACTIVE | ctlplane=10.129.173.126 |
| 2492178a-f855-4c55-a049-94a173a0578c | rhos-ctrl-br-03    | b5f8192f-fb7d-4e8a-a31b-15b23908527c | rhos-ctrl-br-03    | ACTIVE | ctlplane=10.129.173.53  |
+--------------------------------------+--------------------+--------------------------------------+--------------------+--------+-------------------------+



``` bash
ssh tripleo-admin@${SERVER_IP} "sudo hostname -f"

ssh tripleo-admin@${SERVER_IP} "sudo df -h"
# vérifier entres autre /var/log

ssh tripleo-admin@${SERVER_IP} "sudo ip a"
# entre autres les vlan 173 à 178
```



---
## Provisionner le noeud dans l'overcloud

- 
- s'éteindre une fois l'introspection terminée



---
## Vérifier le bon fonctionnement des services gérés par pacemaker
### Pacemaker

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
```


---
## Ré-activer la gestion du "fencing" du cluster 

Désactiver le fencing :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property set stonith-enabled=true"
```

Vérifier :
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property config stonith-enabled"
# Cluster Properties:
#  stonith-enabled: false
```



---
## Ré-activer les services de galera

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource refresh galera-bundle"
# Cleaned up galera-bundle-podman-0 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-0 on rhos-ctrl-br-01
# Cleaned up galera-bundle-0 on rhos-ctrl-br-02
# Cleaned up galera-bundle-0 on rhos-ctrl-br-01
# Cleaned up galera-bundle-podman-1 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-1 on rhos-ctrl-br-01
# Cleaned up galera-bundle-1 on rhos-ctrl-br-02
# Cleaned up galera-bundle-1 on rhos-ctrl-br-01
# Cleaned up galera-bundle-podman-2 on rhos-ctrl-br-02
# Cleaned up galera-bundle-podman-2 on rhos-ctrl-br-01
# Cleaned up galera-bundle-2 on rhos-ctrl-br-02
# ... got reply
# Cleaned up galera-bundle-2 on rhos-ctrl-br-01
# Cleaned up galera:0 on galera-bundle-0
# Cleaned up galera:2 on galera-bundle-2
# Configuration prevents cluster from stopping or starting unmanaged 'galera-bundle'
# Waiting for 13 replies from the controller
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply
# ... got reply (done)
```

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs resource manage galera-bundle"
```

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status"
```


---
## Validation

Lancer un provisioning : 
``` bash
cd scripts
./03-node-provision.sh 
```
> Le provisioning va permettre de mettre à jour la base de référence du moteur de déploiement heat.

Suivi d'un déploiement : 
``` bash
./04-deploy.sh
```


