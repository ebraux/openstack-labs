# Vérification de l'état globale du cluster


---
## Initialisation de l'environnement

Charger l'environnement de l'undercloud : 
``` bash
cd
source stackrc
```
Afficher la liste des noeuds
``` bash
metalsmith -c 'Node Name' -c 'Hostname' -c 'State' -c 'IP Addresses' list
```

Créer des variables utilisées par la suite pour les différentes opérations
``` bash
export CONTROLLERS_IP_LIST='10.129.173.51 10.129.173.52 10.129.173.53'
# Choisir un controlleur pour les commandes de gestion du cluster
export PCS_MANAGER=10.129.173.51
```

---
## Vérifier le status de pacemaker
``` bash
ssh tripleo-admin@${PCS_MANAGER} 'sudo pcs status'
```
- Les Noeuds de contrôleur doivent être "Online".
- Pour haproxy et rabbitmq les service doivent être "Started"
- Pour mariadb, les services doivent être "Promoted"
- Pour redis, un service doit être "Promoted", et les autres "Unpromoted"

``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status | grep -w Online | grep -w $SERVER_NAME"
#  * Online: [ rhos-ctrl-br-01 rhos-ctrl-br-02 rhos-ctrl-br-03 ]
```
---
## Vérifier le status de Mariadb
``` bash
for i in ${CONTROLLERS_IP_LIST} ; do echo "*** $i ***" ; ssh tripleo-admin@$i "sudo podman exec \$(sudo podman ps --filter name=galera-bundle -q) mysql -e \"SHOW STATUS LIKE 'wsrep_local_state_comment'; SHOW STATUS LIKE 'wsrep_cluster_size';\""; done
```
- Les 3 noeuds doivent être avoir les variable identiques :
    - wsrep_local_state_comment = Synced
    - wsrep_cluster_size = 3

---
## Vérifier le status de RabbitMQ
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
``` 
- Les 3 noeuds doivent apparaître, et avoir le status "running"

---
## Vérifier la gestion du "fencing" du cluster
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs property config stonith-enabled"
# Cluster Properties:
#  stonith-enabled: true
```
- Elle doit être à true
 