# Suppression d'un noeud




Vérifier si les ports associé au serveur ont bien été supprimés
``` bash
openstack port list | grep ${SERVER_NAME}
# --> aucune ligne
```

Supprimer le serveur de la listes des `baremetal`
``` bash
openstack baremetal node delete ${SERVER_NAME}
# Deleted node rhos-ctrl-br-03
```


Vérifier que le noeud a bien été supprimé
``` bash
openstack baremetal node show -f json -c name -c power_state -c provision_state -c maintenance ${SERVER_NAME}
# Node rhos-ctrl-br-02 could not be found. (HTTP 404)
```

Et vérifier la liste des noeud baremetal
``` bash
openstack baremetal node list -c Name -c 'Provisioning State'
# +--------------------+--------------------+
# | Name               | Provisioning State |
# +--------------------+--------------------+
# | rhos-comp-br-09    | active             |
# | rhos-comp-ds-br-02 | active             |
``` 

``` bash
openstack baremetal node list | grep ${SERVER_NAME}
# --> aucune ligne
```

Supprimer les informations concernant le noeud dans le fichier de gestion des noeuds du cluster `~/nodes/nodes.yaml`


---
## Commandes complémentaires

Lister les ports
``` bash
``` bash
openstack port list -c Name -c 'Fixed IP Addresses'
```

Commande `metalsmith`
``` bash
metalsmith -c  'Node Name' -c Hostname -c State list
