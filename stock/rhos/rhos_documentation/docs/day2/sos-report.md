
# Utiliser sos report


Installer le package sos :
``` bash
sudo dnf  install -y sos
```

Vérifier la version de sos report, si elle est inférieure à 4.3

``` bash
sudo sos report --all-logs
```

- sur un noeud Openstack, il faut que l'environnement soit configuré pour que sos report puisse se connecter à openstack. Il faut donc avoir préalablement copié le fichier de ressource  :
``` bash
sudo -i
source /home/heat-admin/stackrc
sos report --all-logs
```

- sur un noeud CEPH
``` bash
sudo sos report --all-logs -e ceph_common,ceph_ansible,ceph_mds,ceph_mgr,ceph_mon,ceph_osd,ceph_rgw
```

Un fichier 'sosreport ...' est créé dans le dossier `/var/tmp/`. 

---
## Envoyer un fichier sosreport au support

Soit directement
``` bash
curl -u  disi1 -F "file=@/var/tmp/sosreport-rhos-ctrl-br-01-03734869-2024-02-12-kbefbsw.tar.xz" -F "description=sosreport for rhos-ctrl-br-01" -X POST https://api.access.redhat.com/support/v1/cases/03734869/attachments/ -D -

```
- [https://access.redhat.com/articles/6960871](https://access.redhat.com/articles/6960871)

Soit en le transférant sur un poste de travail, et en l'envoyant via le navigateur. Les droits d'accès sont limités à root, pour le récupérer via scp, en tant que tripleo-admin, il faut modifier les droits :
``` bash
chmod 666 /var/tmp/sosreport-....
```

---
## anciennes versions

If your sos package version is 4.3 or later, please run the following to generate the sos report and upload it to the case.
``` bash
# sos report --all-logs -e ceph_common,ceph_ansible,ceph_mds,ceph_mgr,ceph_mon,ceph_osd,ceph_rgw
```
For any other sos package version, please run the following and upload.
``` bash
# sos report --all-logs
```



