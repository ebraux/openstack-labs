# Mise à jour du certificats


---
## Verification du certificat

``` bash
# Validité
openssl verify  wildcard_openstack.imt-atlantique.fr_plus_CHAINE_CA.pem

# Date of expiration
openssl x509 -enddate -noout -in wildcard.openstack.imt-atlantique.fr.pem
openssl x509 -enddate -noout -in wildcard_openstack.imt-atlantique.fr_plus_CHAINE_CA.pem
```

``` bash
openssl x509 -in wildcard.openstack.imt-atlantique.fr.pem -text -noout
``` 

---
## Mise en place du certificat

Le certificat pour accéder à https://openstack.imt-atlantique.fr est géré dans le fichier `/home/stack/templates/enable-tls.yaml`.

La configuration est décomposée en 3 parties :

- `SSLCertificate` : 
    - Le certificat, sans la clé SSL.
    - Correspond au contenu du fichier `wildcard.openstack.imt-atlantique.fr.pem` 
- `SSLIntermediateCertificate` :
    - La chaîne de certification intermédiaire, 
    - Optionnel, mais nécessaire si l'autorité de certification n'est pas connue du système hôte (si il ets trop ancien par exemple)
    - Correspond au fichier `wildcard.openstack.imt-atlantique.fr.key`
    - Est également présent avec le certificat dans le fichier `wildcard_openstack.imt-atlantique.fr_plus_CHAINE_CA.pem`
- `SSLKey` :
    - La clé SSL
    - Correspond au contenu du fichier `wildcard.openstack.imt-atlantique.fr.key`


Une fois le fichier configuré, il faut redéployer l'infra. ce qui peut prendre plus de 40 minutes.
``` bash
cd /ome/stack/scripts
./deploy.sh
```