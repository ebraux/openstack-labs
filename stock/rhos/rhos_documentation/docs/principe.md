
# Principe

## Déploiement d'un système distribué

Openstack est un système distribué :

- déploiement/gestion de nombreux serveurs
- de nombreux composants, interagissants entre eux
- ...

Pour déployer de type d'architecture plusieurs solutions :

- déploiement manuel : installation des serveurs, puis de chaque composants, configuration, ...
- utilisation d'outils de déploiement d'infrastructure : 
automatisation du déploiement et de la configuration de composants (Ansible, Puppet, ...)
- utilisation d'outils permettant de déployer des serveurs, OS, et de gérer les configuration réseau (Terraform, ...)

Openstack est lui même capable de déployer des infrastructures virtuelles (VM, SDN, ... ). Mais également des serveurs physiques (Baremetal). Et propose un outil d'orchestration du déploiement des ressources (Heat).

## Déploiement de RedHat Openstack

Le principe est donc de déployer un Openstack minimum, qui sera ensuite utilisé pour déployer l'infrastructure nécessaire au déploiement du cluster Openstack cible. 

C'est le projet TripleO (Openstack On Openstack) [https://docs.openstack.org/tripleo-docs/latest/](https://docs.openstack.org/tripleo-docs/latest/).

- l'Openstack "minimum" est l'Undercloud déployé en général sur une seule machine
- l'Openstack "cible" est l'Overcloud déployé sur plusieurs serveurs.

L'Undercloud n'est utilisé que par les administrateurs de l'infrastructure, pour gérer le déploiement de l'Overcloud. Il n'a pas besoin de beaucoup de ressource, car il n’exécute aucune charge de travail, mais uniquement une charge lors des déploiement et mises à jour de l'Overcloud.

L'Overcloud est le Cloud d'Entreprise, mis à disposition des utilisateurs.


Le déploiement de l'Overcloud est réalisé par l'orchestrateur de ressources de l'Undercloud, à partir de fichiers de template, qui peuvent être personnalisés.

L'Undercloud va créer les réseaux, déployer les serveurs en mode "Baremetal", et les configurer. Puis, une fois l'infrastructure en place, lancer le déploiement des différents composants d'Openstack sur les serveurs.

La gestion des fichiers de configuration et du déploiement des composants est réalisée avec Puppet (historique) et Ansible.

Les composants d'Openstack sont déployés sous forme de conteneurs, à partir des images du projet Kolla [https://docs.openstack.org/kolla/latest/](https://docs.openstack.org/kolla/latest/), re-packagées et adaptées par RedHat. Ces images peuvent être personnalisées.
