# Accès à la console avec VNC

Certaine machine ne sont pas très à jour, et l'interface idarc ne fonctionne pas bien.

Un des problèmes pricipaux, c'ets l'impossibilité de se connecter à la console.

POur contourner ce problème, possibilité d'activer l'accès console via VNC


- dans le menu  `Parametres d'IDrac > Réseau`
  - dans le tab Services
    - dans le panel Serveur VNC
      - activer le serveur VNC
      - Définiir un MDP

Le serveur est ensuite accessible avec un outil comme TigerVNC, à l'adresse `10.29.20.xxx:5901`


Si la mise sous tension ne réagi pas non plus, utiliser les commandes ipmi

``` bash
ipmitool -I lanplus -U xxxxxx -P xxxxxxxx -H 10.29.20.xxx chassis power on

ipmitool -I lanplus -U xxxxxx -P xxxxxxxx -H 10.29.20.xxx chassis power off

ipmitool -I lanplus  -U xxxxxx -P xxxxxxxx -H 10.29.20.xxx chassis power status
```