# Mise à jour des serveurs DELL

- [https://www.dell.com/support/kbdoc/fr-fr/000130533/dell-poweredge-proc%C3%A9dure-de-mise-%C3%A0-jour-micrologiciel-via-https-connexion-%C3%A0-idrac](https://www.dell.com/support/kbdoc/fr-fr/000130533/dell-poweredge-proc%C3%A9dure-de-mise-%C3%A0-jour-micrologiciel-via-https-connexion-%C3%A0-idrac)

idrac : 
- [https://www.dell.com/support/manuals/fr-fr/integrated-dell-remote-access-cntrllr-8-with-lifecycle-controller-v2.00.00.00/idrac8_ug_pub-v1/updating-device-firmware](https://www.dell.com/support/manuals/fr-fr/integrated-dell-remote-access-cntrllr-8-with-lifecycle-controller-v2.00.00.00/idrac8_ug_pub-v1/updating-device-firmware)
- [https://www.dell.com/support/kbdoc/fr-fr/000132986/dell-emc-catalog-links-for-poweredge-servers]
- [https://www.dell.com/support/manuals/fr-fr/integrated-dell-remote-access-cntrllr-8-with-lifecycle-controller-v2.00.00.00/idrac8_ug_pub-v1/updating-device-firmware-using-racadm?guid=guid-2201e599-f8e4-477d-8bee-777e27909489&lang=en-us]