# consulter les infos suite à l'introspection


### Info sur une machine

``` bash
openstack baremetal node show rhos-comp-br-07
```

ou au format json + filtre
``` bash
openstack baremetal node show <node> -f json -c properties | jq -r .properties.capabilities
```

Renvoie notament 
``` bash
| uuid                   | 7bf267c6-e996-4c86-b389-0a5aee6d125c 
| name                   | rhos-comp-br-07 
| maintenance            | False  
| provision_state        | available
| properties             | {'local_gb': '277', 'cpus': '32', 'cpu_arch': 'x86_64', 'memory_mb': '131072', 'capabilities': 'cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true'}  
```

REM : on peut modifier une valeur manuellement:
``` bash
openstack baremetal node set \
 --property capabilities="boot_option:local" <node>
 ```
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_manually-configuring-bare-metal-node-hardware-information_basic](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_manually-configuring-bare-metal-node-hardware-information_basic)


Afficher toute les données collectée lor de l'introspection
``` bash
openstack baremetal introspection data save rhos-ctrl-br-01 | jq
```

Notament :
- la configuration des disques
``` bash
 openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".inventory.disks"
```
- les interfaces réseau
``` bash
# uniquement celle qui a étét activé pour le provisionning (au Vlan de PROV)
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".interfaces"

# liste de interfaces
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".numa_topology.nics"

# liste avec un peu pls de détails
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".inventory.interfaces"
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".all_interfaces"
 
# infos complétes 
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".extra.network"
```


### config disque
openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".extra.disk"

openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".inventory.root_disk"

openstack baremetal introspection data save rhos-ctrl-br-01 |  jq ".extra.disk""extra": {
    "disk": {

- cpu : cpu
"inventory": 
"extra": {
    "disk": {
 "root_disk": {


    "boot": {
      "current_boot_mode": "bios",
      "pxe_interface": "00:62:0b:ac:48:b4"
    },
      "disks": [

    "hostname": "localhost.localdomain"


### Connexions réseau

``` bash
openstack baremetal introspection interface list ${NODE}
```

``` bash
openstack baremetal port list --long 
openstack baremetal port list --long -c "Node UUID" -c "Local Link Connection"
```

Renvoie les infos :

- Address : adresse MAC
- Node UUID : id de la machine 
- Local Link Connection : info de connexion au switch
    -  l'adresse MAC pricipale du Switch,
    -  le numéro d eport sur le switch
- PXE boot enabled : etat de la config PXE
- Physical Network : réseau utilisé pour accèder à la machine (le brideg ctrlplane)
- et en plus
    - UUID 
    - Created At :
    - Extra :
    - Portgroup UUID
    - Updated At
    - Internal Info
    - Is Smart NIC port 

Exemple pour afficher les machine et le port de switch sur lequel leur interface de provisionning est connecté

``` bash
$ openstack baremetal port list --long -c "Node UUID" -c "Local Link Connection"
+--------------------------------------+-----------------------------------------------------------+
| Node UUID                            | Local Link Connection                                     |
+--------------------------------------+-----------------------------------------------------------+
| e4f88d1f-d262-4988-9fc4-23a0eb087183 | {'switch_id': 'f0:78:16:37:7c:00', 'port_id': 'Gi1/0/11'} |
| f4c78751-c63e-421a-8eaf-29f11df90753 | {'switch_id': 'f0:78:16:37:7c:00', 'port_id': 'Gi1/0/17'} |
| aee72e41-c6b3-4868-b6ac-69693e371477 | {'switch_id': 'f0:78:16:37:7c:00', 'port_id': 'Gi1/0/19'} |
...
+--------------------------------------+-----------------------------------------------------------+
```

REM : si e Local link n'est pas renseigné automatiquement, c'est possible d ele faire manuellement
``` bash
openstack baremetal port set <port_uuid> \
--local-link-connection switch_id=52:54:00:00:00:00 \
--local-link-connection port_id=p0
```

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_using-director-introspection-to-collect-bare-metal-node-hardware-information_basic](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_using-director-introspection-to-collect-bare-metal-node-hardware-information_basic)

