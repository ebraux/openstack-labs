

---
## Activer iPMI sur les serveur HPE avec ILO 5


Ajouter un utilisateur "root". Cet utilisateur doit avoir les droits d'admin, en ipmi.

- Dans l'interface d'admin, menu "Administration"
- Dans l'onglet "User Administration"
- 
- 

Acctiver l'IPMI via le LAN :

- Dans l'interface d'admin, menu "Security"
- Dans la colonne "Network#, éditer "IPMI/DCMI over LAN", pour le configurer à "Enabled"
- Créer un nouvel utilisateur, et lui affecter le rôle "Administrator"


---
## Commandes IMPI pour tests

``` bash
ipmitool -I lanplus -U **** -P **** -H 10.29.20.xx sensor
ipmitool -I lanplus -U **** -P **** -H 10.29.20.xx chassis power status
```


---
## Utilisation avec serveurs DELL


``` bash
ipmitool -I lanplus -U **** -P **** -H 10.29.20.xx  -v raw 0x30 0x21
ipmitool -I lanplus -U **** -P **** -H 10.29.20.xx  -v raw 0x30 0x9F
ipmitool -I lanplus -U **** -P **** -H 10.29.20.xx  -v -R 12 -N 5 chassis power status
```

``` bash
racadm update -f Catalog.xml.gz -e downloads.dell.com/Catalog -a TRUE -t HTTPS

-ph <proxy ip> -pu <proxy user> -pp <proxy pass> -po <proxy port> \
  -pt <proxy type>

racadm update -f Catalog.xml.gz -e downloads.dell.com/Catalog -a TRUE -t HTTPS -ph proxy.enst-bretagne.fr  -po 8080 -pu '' -pp '' -pt HTTP

racadm jobqueue view -i JID_826340643119

```

---
## Automtiser la config 
- [https://medium.com/@what_if/automate-enabling-of-ipmi-over-lan-access-on-hpe-ilo-7d9d8c55b83e](https://medium.com/@what_if/automate-enabling-of-ipmi-over-lan-access-on-hpe-ilo-7d9d8c55b83e)