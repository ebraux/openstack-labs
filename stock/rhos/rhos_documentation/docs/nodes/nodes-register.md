

---
## Register the bare-metal nodes for your overcloud.

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_registering-nodes-for-the-overcloud_basic](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_configuring-a-basic-overcloud#proc_registering-nodes-for-the-overcloud_basic)
Tester la connectivité "ipmi"



Créer un fichier `nodes.yaml`, décrivant les noeuds à intégrer :

- Nom
- Adresse MAC
- physical_network : interface permettant d'accéder au VLAN du subnet de provisionning (173)
- IDRAC/ILO info

``` yaml
- name: "rhos-comp-br-01"
  ports:
      - address: "e4:43:4b:ea:b2:b0"
        physical_network: ctlplane
  pm_type: "idrac"
  pm_user: "root"
  pm_password: "xxxxxx"
  pm_addr: "10.29.20.101"
```

La choix du pm_type dépend des machines. Par défaut choisir `impi`.

``` yaml
nodes:
  - name: "node01"
    ports:
      - address: "aa:aa:aa:aa:aa:aa"
        physical_network: ctlplane
        local_link_connection:
          switch_id: 52:54:00:00:00:00
          port_id: p0
    cpu: 4
    memory: 6144
    disk: 40
    arch: "x86_64"
    pm_type: "ipmi"
    pm_user: "admin"
    pm_password: "p@55w0rd!"
    pm_addr: "192.168.24.205"
  - name: "node02"
```

Pour tester la syntaxe : 
``` bash
openstack overcloud node import --validate-only  /home/stack/nodes.yaml
```

Modififcation de l'IP de l'interface su le bridge 'brptov', pour qu'elle corresonde à la gateway 10.129.173.1
