# Gestion de noeuds

---
## Principe

Les noeuds doivent  :
- accès par carte IPMI (Idrac, Ilo, ...)
  - IP
  - User/mdp
- relevé des Interfaces, et des adresses MAC
  - adresse MAC de l'interface utilisée pour le boot PXE
  - adresse MAC de toutes les interfaces 
  - état UP ou Down de chaque interface

Une fois le noeud prêt : 

1. Enregistrer le noeud dans l'undercloud, (importer) pour qu'il puisse le gérer La machine ets alors dans le 'Provisioning State'  `manageable`
2. Lancer l'introspection du noeuds : boot sur un système limité, pour relever. les caractéristiques de la machine. La machine passe alors dans le 'Provisioning State'  `available`

Le noeud est alors utilisable dans le déploiement de l'overcloud. Un noeud utilisé pa l'overcloud passe dans le 'Provisioning State' `active`


---
## Détail des opérations

- [Préparation des noeuds](./nodes-preparation.md)
- [Enregistrement des noeuds dans l'undercloud](./nodes-register.md)
- [Lancement de l'introspection](./nodes-introspection.md)
- [Utilisation des données d'introspection](./nodes-introspection-datas.md)


---
## Autres opérations
- [Utilisation d'IPMI](./ipmi.md)
- [Informations spécifiques sur les serveurs DELL](./servers-DELL/README.md)
- [Informations spécifiques sur les serveurs HPE](./servers-HPE/README.md)

