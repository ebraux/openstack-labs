---
## Intégration des noeuds

``` bash
source ~/stackrc
openstack overcloud node import ~/nodes.yaml
# Waiting for messages on queue 'tripleo' with no timeout.
# 3 node(s) successfully moved to the "manageable" state.
# Successfully registered node UUID e4f88d1f-d262-4988-9fc4-23a0eb087183
# Successfully registered node UUID f4c78751-c63e-421a-8eaf-29f11df90753
# Successfully registered node UUID aee72e41-c6b3-4868-b6ac-69693e371477
```


Et vérifier
``` bash
openstack baremetal node list
# +--------------------------------------+------------+---------------+-------------+--------------------+-------------+
# | UUID                                 | Name       | Instance UUID | Power State | Provisioning State | Maintenance |
# +--------------------------------------+------------+---------------+-------------+--------------------+-------------+
# | e4f88d1f-d262-4988-9fc4-23a0eb087183 | os-ctrl-01 | None          | power off   | manageable         | False       |
# | f4c78751-c63e-421a-8eaf-29f11df90753 | os-ctrl-02 | None          | power off   | manageable         | False       |
# | aee72e41-c6b3-4868-b6ac-69693e371477 | os-ctrl-03 | None          | power off   | manageable         | False       |
# +--------------------------------------+------------+---------------+-------------+--------------------+-------------+

```


https://access.redhat.com/solutions/6895151


``` bash
openstack tripleo validator run  --group pre-introspection -i inventory.yaml
openstack tripleo validator run  --group pre-introspection -i inventory.yaml
# +--------------------------------------+---------------------------------+--------+------------+----------------+-------------------+-------------+
# | UUID                                 | Validations                     | Status | Host_Group | Status_by_Host | Unreachable_Hosts | Duration    |
# +--------------------------------------+---------------------------------+--------+------------+----------------+-------------------+-------------+
# | 0f86e94d-862b-4949-bf0b-d4d63cbf6340 | check-cpu                       | PASSED | localhost  | localhost      |                   | 0:00:01.330 |
# | a7d82a39-00f6-4088-99f6-a59e91de8b39 | check-disk-space                | PASSED | localhost  | localhost      |                   | 0:00:05.095 |
# | dafcd2c7-472e-4176-a201-dfc37900c2cc | check-ram                       | PASSED | localhost  | localhost      |                   | 0:00:01.333 |
# | 6bcbe35c-afc2-4695-a99c-b8e067cb1da9 | check-selinux-mode              | PASSED | localhost  | localhost      |                   | 0:00:01.893 |
# | 33cde26d-c06e-4926-af6d-05f111c77a33 | check-network-gateway           | FAILED | undercloud | undercloud     |                   | 0:00:06.442 |
# | 692b5a26-7a0d-4f72-9444-e30a005f7db2 | undercloud-disk-space           | PASSED | undercloud | undercloud     |                   | 0:00:05.064 |
# | aab9b0bf-b833-4e20-94b5-24df93f430e7 | ctlplane-ip-range               | PASSED | undercloud | undercloud     |                   | 0:00:02.817 |
# | ebca1d78-e2e9-497c-9e6e-583fa92d53ab | undercloud-neutron-sanity-check | PASSED | undercloud | undercloud     |                   | 0:00:10.908 |
# | 8b5a71e7-be96-4e63-959d-b2e7d29a10c0 | dhcp-introspection              | PASSED | undercloud | undercloud     |                   | 0:00:06.755 |
# | 84928f2f-3594-4b25-8205-20fd643df786 | undercloud-tokenflush           | PASSED | undercloud | undercloud     |                   | 0:00:01.460 |
# +--------------------------------------+---------------------------------+--------+------------+----------------+-------------------+-------------+

```

---
## Lancement de l'introspaction
``` bash
openstack overcloud node introspect --all-manageable --provide
# Waiting for introspection to finish...
# Waiting for messages on queue 'tripleo' with no timeout.
# Introspection of node completed:e4f88d1f-d262-4988-9fc4-23a0eb087183. Status:SUCCESS. Errors:None
# Introspection of node completed:aee72e41-c6b3-4868-b6ac-69693e371477. Status:SUCCESS. Errors:None
# Introspection of node completed:f4c78751-c63e-421a-8eaf-29f11df90753. Status:SUCCESS. Errors:None
# Successfully introspected 3 node(s).
```


### Autres commandes

``` bash

openstack overcloud node introspect --provide --all-manageable
openstack overcloud node introspect --provide os-ctrl-01

openstack baremetal node maintenance unset rhos-comp-br-09

openstack overcloud node introspect --provide <node1>

openstack baremetal port list --long -c UUID -c "Node UUID" -c "Local Link Connection"


openstack baremetal node show os-ctrl-01

openstack baremetal node list --long show os-ctrl-01
```

`openstack overcloud node clean`

``` bash
openstack baremetal introspection status controller0
```

relancer une introspaection
``` bash
openstack baremetal node manage rhos-ctrl-br-03
openstack baremetal node list
openstack overcloud node introspect --provide rhos-ctrl-br-03
```


---
## Suivi  / debug

### Suvi du processus :

``` bash
sudo journalctl -l -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq -u openstack-ironic-conductor -f
```

``` bash
openstack baremetal node list
+--------------------------------------+------------+---------------+-------------+--------------------+-------------+
| UUID                                 | Name       | Instance UUID | Power State | Provisioning State | Maintenance |
+--------------------------------------+------------+---------------+-------------+--------------------+-------------+
| e4f88d1f-d262-4988-9fc4-23a0eb087183 | os-ctrl-01 | None          | power on    | clean wait         | False       |
| f4c78751-c63e-421a-8eaf-29f11df90753 | os-ctrl-02 | None          | power on    | clean wait         | False       |
| aee72e41-c6b3-4868-b6ac-69693e371477 | os-ctrl-03 | None          | power off   | available          | False       |
+--------------------------------------+------------+---------------+-------------+--------------------+-------------+
```

### Instance bloquée en `state = enroll`
``` bash
... +-------------+--------------------+-------------+
... | Power State | Provisioning State | Maintenance |
... +-------------+--------------------+-------------+
... | None        | enroll             | False       |
```
Pb de contact avec la carte IPMI.
- tester l'accès IPMI
- passer le noeud en 'manageable'
``` bash
openstack baremetal node manage  os-ctrl-01
```

### Instance bloquée en `Maintenance = True`

``` bash
openstack baremetal node maintenance unset rhos-comp-br-09
```

