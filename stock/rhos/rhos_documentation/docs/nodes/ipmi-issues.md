# IPMI Issues

---
## IPMI timeout 
https://bugs.launchpad.net/tripleo/+bug/1987364

I have managed to find a "solution" to this issue:
Edit the `/var/lib/config-data/puppet-generated/ironic/etc/ironic/ironic.conf` so that it contains
```
[ipmi]
command_retry_timeout = 90
min_command_interval = 10
use_ipmitool_retries = true
```

The issue here being that ironic triggers `ipmitool` too frequently, one on top of another. Therefore I am using `use_ipmitool_retries` to avoid that overlap. Then ipmi commands take rather long time on the current network and old hardware, i.e. a call of `ipmitool power status` can take more time than `min_command_interval`, so I increase `command_retry_timeout` to compensate for that. Then I increase `min_command_interval` so that it doesn't flood the ipmi as much and it can report the `power status`


---
## Error: Unable to establish IPMI v2 / RMCP+ session

pb eb cas de changement de carte mère, ou d ecarte Idrac, le mot de passe idrac et IPMI peuvent être différents.
solution : ressaisir le mot de passe idrac

Menu Overview -> IDRAC SETTINGS -> User Authentication
-> Click on the userID of your admin account -> Next
-> check "change your password" checkbox and enter the same (or new) password
-> Apply

- [https://stackoverflow.com/questions/51948745/error-unable-to-establish-ipmi-v2-rmcp-session](https://stackoverflow.com/questions/51948745/error-unable-to-establish-ipmi-v2-rmcp-session)