# Configuration de l'accès IPMI

- Configurer la carte pour 
    - Définir une adresse IP + gateway dans le subnet 10.29.20.0/24
    - Configurer un utilisateur avec le login/mdp du cluster pour qu'il puisse se connecter
    - Configurer les droits d'accès de cet utilisateur en mode "adminitrateur"
    - Activer l' "IPMI other LAN"

Tester l'accès IPMI

Si besoin installer ipmitool :
``` bash
sudo dnf install -y ipmitool
```
Et lancer les tests, avec :

- '-U' = utilisateur IPMI
- '-P' = le mot de passe IPMI
- '-H' = l'adresse IP de la machine
- '-L' : niveau de droit d'accès (défaut = ADMINISTRATOR)
- '-R' : retry (défaut = 4)
- '-N' : seconds  : Specify timeout [default=1] 
- '-I' : Interface :
    - 'lan' : IPMI v1.5 LAN Interface
    - 'lanplus' : IPMI v2.0 RMCP+ LAN Interface 
    - ...

``` bash
ipmitool -I lanplus -U ******** -P ******** -H 10.29.20.53 sensor
ipmitool -I lanplus -U ******** -P ******** -H 10.29.20.53 chassis power status
ipmitool -I lanplus -U ******** -P ******** -H 10.29.20.53  -L ADMINISTRATOR -R 3 -N 5  power status
```
