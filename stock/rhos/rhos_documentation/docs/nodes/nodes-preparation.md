# Préparation d'un node pour intégrer le cluster

---
## Préparation de la configuration réseau

Pour que l'introspection puisse fonctionner, il faut :

- Que Tripleo puisse interagir avec le serveur pour le relancer en utilisant IPMI
- Que les serveurs puissent contacter le serveur DHCP lancé sur la machine 

- Connecter l'interface IPMI (ilo, idrac) sur un port de switch, configuré sur le VLAN 420 en mode natif
- Connecter l'interface NIC1 sur un port de switch, configuré sur le VLAN 173 en mode natif

---
## Configuration de l'accès en DHCP

- Configurer le boot en mode PXE sur l'interface NIC1
- Relever l'adresse MAC de l'interface NIC1

Pour les machines HPE : 
  - [https://support.hpe.com/hpesc/public/docDisplay?docId=sf000068961en_us&docLocale=en_US](https://support.hpe.com/hpesc/public/docDisplay?docId=sf000068961en_us&docLocale=en_US)


---
## Configuration du type de BIOS ((Legacy/UEFI))

Pour que la phase de boot PXE + chargement d'un mini système pour l'introspection fonction, il faut :

- soit configurer le BIOS en mode legacy (pas UEFI)
- soit configurer le BIOS en UEFI, mais dans ce cas préparer une configuretion spécifique pour le noeud avant de pouvoir lancer l'introspection

POur simplifier la configuration, et puisque toutes les machines le supportent, le BIOS a été configuré en mode Legacy sur tous les noeuds

