# Principe de l'architecture technique

Sur le Director
- l'infra OVS pour gérer le réseau
- sous forme de conteneur :
    - une infra Openstack avec  : RabbitMQ/Mariadb /memcache/haproxy
    - et les composants Openstack : keystone, neutron et Ironic, sur base d'image Kolla

En plus, le heat-ephemeral
 - lancé à la demande pour la gestion des template heat,
 - sous forme de conteneur, sur base d'image Kolla
 - stocke ses données dans un fichier à plat
 - sert principalement pour la conversion de tout l'ancien mécanisme de template basé sur heat et heira, en playbook Ansible.


--> c'est bien un Openstack sans Nova
--> et sans glance, puisque ironic Ironic utilise des images qcow stockée en local, pour déployer ensuite les OS sur les machines baremetal
--> avec juste Ironic + OVS, on pourrait déployer une infra de machines baremetal. Mais Openstack c’est quand même confortable :-)
--> la solution de RedHat pour déployer des infra baremetal, avec soit Openstack oit Kubernetes, ça pourrait bien être ça. Ou un mini K8S avec Kind dans lequel es lancé Ironic, comme le fait Mirantis :-)


Sur les Noeuds du cluster :
- l'infra OVS pour gérer le réseau
- sous forme de conteneur :
    - tous les composants d'penstack sur base d'image Kolla
    - les composants OVN, sur base d'image kolla aussi je pense


Le déploiement se fait avec des commandes `openstack overcloud ...`, qui lancent le heat-ephemeral, génèrent un inventory pour Ansible, et ensuite principalement lancent des playbook Ansible.
On a une commande pour provisionner les noeuds :  `openstack overcloud provision  ...`. Installation et configuration du système,  configuration du réseau : 
Et une commande pour déployer les composants Openstack   `openstack overcloud deploy ...`

--> les noeuds pourraient être provisionnés par un autre mécanisme, on peut d'ailleurs désormais intégrer des noeuds existants.


