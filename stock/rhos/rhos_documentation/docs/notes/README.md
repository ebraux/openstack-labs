
# Notes

- Tests d'utilisation de Satellite: 
    - [Creation de la VM](./satellite/vm-rh-satellite.md)
    - [Registry d'images de conteneurs](./satellite/image-registry.md)
- Autres/divers :
    - [ansible-config.md](./others/ansible-config.md)
    - [config-imt-atlantique.md](./others/config-imt-atlantique.md)
    - [inventory.md](./others/inventory.md)
    - [mirantis_deploiment.md](./others/mirantis_deploiment.md)
    - [Questions.md](./others/Questions.md)

``` bash
├── configuration
│   └── disk-selection.md
├── customization
│   ├── customize-nodes.md
│   └── gpu-config.md
├── day2
│   ├── backup.md
│   ├── controller-replace.md
│   ├── day2-update.md
│   ├── diag-libvirt.md
│   ├── diag.md
│   ├── diag-network.md
│   ├── load-balancer.md
│   ├── manage-file-system.md
│   └── manager-network-diag.md
├── debug
│   ├── ceph-debug.md
│   ├── certificats.md
│   ├── compute-service.md
│   ├── conteneurs-debug.md
│   ├── deploy-ephemeral-heat.md
│   ├── deploy.md
│   ├── galera.md
│   ├── galera-promoted.md
│   ├── galera-unpromoted-and-failed.md
│   ├── glance_api_cron.md
│   ├── HA-debug.md
│   ├── ha-proxy.md
│   ├── images-debug.md
│   ├── instance-not-accessible.md
│   ├── interfaces-config-debug.md
│   ├── ironic-debug.md
│   ├── kvm.md
│   ├── network-debug.md
│   ├── network-debug-vlan.md
│   ├── network-infos.md
│   ├── network.md
│   ├── network-os-net-config.md
│   ├── object-storage.md
│   ├── octavia-cert.md
│   ├── ovn-debug.md
│   ├── ovn.md
│   ├── ovn-openflow-debug.md
│   ├── PCS-Cluster.pdf
│   ├── performance.md
│   ├── podman.md
│   ├── port-conflict.md
│   ├── provisioning-false.md
│   ├── pxe-dhcp-debug.md
│   ├── quotas-limits.md
│   ├── rabbitMQ-debug.md
│   ├── README.md
│   ├── rhos-comp-06.md
│   ├── sos-report.md
│   ├── status.md
│   ├── system-services.md
│   ├── troubleshooting-general.md
│   └── volume-debug.md
├── evolutions
│   ├── ansible.md
│   ├── keystone.md
│   ├── metrologie.md
│   └── upgrade.md
├── migration_17.1
│   ├── 17.1-evolution.md
│   ├── notes.md
│   ├── post-upgarde.md
│   ├── README.md
│   ├── re-deploy.md
│   ├── TODO
│   ├── upgrading-overcloud.md
│   ├── upgrading-systems.md
│   └── upgrading-undercloud.md
```