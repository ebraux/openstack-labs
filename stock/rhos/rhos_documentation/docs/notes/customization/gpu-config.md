

- Chapter 15. Configuring virtual GPUs for instances : [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/configuring_the_compute_service_for_instance_creation/assembly_configuring-virtual-gpus-for-instances_vgpu](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/configuring_the_compute_service_for_instance_creation/assembly_configuring-virtual-gpus-for-instances_vgpu)

``` bash
Avec GPU
R720 xd dans bair 24, emp 27 /28
branché et alumé
idrac : connectée
- RHOS-COMP16.
  RHOS-GPU-01

Adresse : MAC de boot : E0:DB:55:1D:15:B8
Vérifier qu'elle est bien en PXE
4 interfaces.

boot UEFI.


balnc NIC1
stack d01-02 port 2

bleu NIC2
stack d01-02 port 5
```