


La façon la plus simple, c'ets de mettre la configuration dans la config globale.

On peut ensuite mettre des parametres au nieveau des roles.

Si on veur faire de la customization au niveau de snoeuds, alors il faut utiliser le mécanisme de "hieradata", qui permet également des customisation au niveau globale, des rôles mais aussi des neouds, si on précise l'UUID.

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_configuration-hooks#proc_puppet-customizing-hieradata-for-individual-nodes_configuration-hooks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_configuration-hooks#proc_puppet-customizing-hieradata-for-individual-nodes_configuration-hooks)


---
## 

Les parametres modifiables sont listés dans la doc
```
I understand that you are using OSP-17.1 and the parameter: NovaCPUAllocationRatio is valid.

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/overcloud_parameters/index#ref_compute-nova-parameters_overcloud_parameters
```

---
```
You have to scale-in only his node and assign new role only for **this**  node to get new CPU configuration. 

Step-1 : 
Migrate all the instances on this node and scale-in:

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/installing_and_managing_red_hat_openstack_platform_with_director/index#proc_removing-or-replacing-a-compute-node_scaling-overcloud-nodes

Scale-out node with new role name and assign needed allocation parameter. 
https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_composable-services-and-custom-roles#proc_creating-a-new-role_composable-services-and-custom-roles

Example of template:
parameter_defaults:
  <role_name>Parameters:
      


Let me know if you have any further queries or concerns. 
```