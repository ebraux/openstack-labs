

11.1. Upgrading the overcloud images

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/performing-post-upgrade-actions#upgrading_the_overcloud_images](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/performing-post-upgrade-actions#upgrading_the_overcloud_images)

``` bash
cd ~
ll images
# total 3351584
# -rw-r--r--. 1 stack stack  455265247 12 avril  2023 ironic-python-agent.initramfs
# -rwxr-xr-x. 1 stack stack   10050928 12 avril  2023 ironic-python-agent.kernel
# -rw-r--r--. 1 stack stack   88152126 12 avril  2023 overcloud-full.initrd
# -rw-r--r--. 1 stack stack 1318780928 12 avril  2023 overcloud-full.qcow2
# -rw-r--r--. 1 stack stack      35644 12 avril  2023 overcloud-full-rpm.manifest
# -rw-r--r--. 1 stack stack      90480 12 avril  2023 overcloud-full-signature.manifest
# -rw-r--r--. 1 stack stack   10050928 12 avril  2023 overcloud-full.vmlinuz
# -rw-r--r--. 1 stack stack   97786216 12 avril  2023 overcloud-minimal.initrd
# -rw-r--r--. 1 stack stack 1440940032 12 avril  2023 overcloud-minimal.qcow2
# -rw-r--r--. 1 stack stack      30999 12 avril  2023 overcloud-minimal-rpm.manifest
# -rw-r--r--. 1 stack stack      79821 12 avril  2023 overcloud-minimal-signature.manifest
# -rwxr-xr-x. 1 stack stack   10743152 12 avril  2023 overcloud-minimal.vmlinuz

mv images/ images.16.2
mkdir images

cd ~/images

for i in /usr/share/rhosp-director-images/overcloud-full-latest-17.1.tar /usr/share/rhosp-director-images/ironic-python-agent-latest-17.1.tar; do tar -xvf $i; done
# overcloud-full.qcow2
# overcloud-full.initrd
# overcloud-full.vmlinuz
# overcloud-full-rpm.manifest
# overcloud-full-signature.manifest
# ironic-python-agent.initramfs
# ironic-python-agent.kernel

cd ~

```


## 11.2. Updating CPU pinning parameters

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/performing-post-upgrade-actions#updating-the-cpu-pinning-parameter-post-upgrade](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/performing-post-upgrade-actions#updating-the-cpu-pinning-parameter-post-upgrade)

``` bash
openstack flavor create upgrade ```