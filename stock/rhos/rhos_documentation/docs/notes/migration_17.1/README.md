# Migration de 16.2 en 17.1

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/index](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/index)


---
## Versions

- OpenStack Platform 17.1 uses Red Hat Enterprise Linux (RHEL) 9.2
- You can view your current RHOSP and RHEL versions in the /etc/rhosp-release and /etc/redhat-release files.

``` bash
cat /etc/redhat-release
Red Hat Enterprise Linux release 8.4 (Ootpa)

cat /etc/rhosp-release
Red Hat OpenStack Platform release 16.2.5 (Train)
```

### Ceph 
If you have deployed a Red Hat Ceph Storage system separately and then used director to deploy and configure OpenStack, before you can upgrade your Red Hat OpenStack Platform (RHOSP) deployment from version 16.2 to version 17.1, you must upgrade your Red Hat Ceph Storage cluster from version 4 to version 5. 

- Vérifier la version de CEPH sur le cluster
``` bash
ssh root@rhcs-ceph-br-11podman images
# REPOSITORY                                                  TAG         IMAGE ID      CREATED       SIZE
# registry.redhat.io/rhceph/rhceph-5-rhel8                    latest      23e040ac1e4f  2 months ago  1.02 GB
```

