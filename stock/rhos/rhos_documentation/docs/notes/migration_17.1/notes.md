# Notes



---
## system_upgrade.yaml file

If you upgrade from Red Hat OpenStack Platform (RHOSP) 13 to 16.1 or 16.2, or from RHOSP 16.2 to 17.1, do not include the system_upgrade.yaml file in the --answers-file answer-upgrade.yaml file. If the system_upgrade.yaml file is included in that file, the environments/lifecycle/upgrade-prepare.yaml file overwrites the parameters in the system_upgrade.yaml file. To avoid this issue, append the system_upgrade.yaml file to the openstack overcloud upgrade prepare command. For example:

``` bash
$ openstack overcloud upgrade prepare --answers-file answer-upgrade.yaml /
-r roles-data.yaml /
-n networking-data.yaml /
-e system_upgrade.yaml /
-e upgrade_environment.yaml /
```
With this workaround, the parameters that are configured in the system_upgrade.yaml file overwrite the default parameters in the environments/lifecycle/upgrade-prepare.yaml file.


---
## known-issues

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_about-the-red-hat-openstack-platform-framework-for-upgrades_about-upgrades#known-issues-that-might-block-an-upgrade](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_about-the-red-hat-openstack-platform-framework-for-upgrades_about-upgrades#known-issues-that-might-block-an-upgrade)


gestion des repository

- core
  - rhel-9-for-x86_64-baseos-eus-rpms
  - rhel-9-for-x86_64-appstream-eus-rpms
  - rhel-9-for-x86_64-highavailability-eus-rpms
  - openstack-17.1-for-rhel-9-x86_64-rpms
  - fast-datapath-for-rhel-9-x86_64-rpms
  - rhel-8-for-x86_64-baseos-tus-rpms
  - rhel-8-for-x86_64-appstream-tus-rpms
  - openstack-17.1-for-rhel-8-x86_64-rpms
- controllers :
  - core
    - rhel-9-for-x86_64-baseos-eus-rpms
    - rhel-9-for-x86_64-appstream-eus-rpms
    - rhel-9-for-x86_64-highavailability-eus-rpms
    - openstack-17.1-for-rhel-9-x86_64-rpms
    - fast-datapath-for-rhel-9-x86_64-rpms
    - rhel-8-for-x86_64-baseos-tus-rpms
    - rhel-8-for-x86_64-appstream-tus-rpms
    - openstack-17.1-for-rhel-8-x86_64-rpms
  - rhel-8-for-x86_64-highavailability-tus-rpms
  - ceph-6-tools-for-rhel-9-x86_64-rpms                                        
- compute :
  - core
    - rhel-9-for-x86_64-baseos-eus-rpms
    - rhel-9-for-x86_64-appstream-eus-rpms
    - rhel-9-for-x86_64-highavailability-eus-rpms
    - openstack-17.1-for-rhel-9-x86_64-rpms
    - fast-datapath-for-rhel-9-x86_64-rpms
    - rhel-8-for-x86_64-baseos-tus-rpms
    - rhel-8-for-x86_64-appstream-tus-rpms
    - openstack-17.1-for-rhel-8-x86_64-rpms
  - ceph-6-tools-for-rhel-9-x86_64-rpms  

