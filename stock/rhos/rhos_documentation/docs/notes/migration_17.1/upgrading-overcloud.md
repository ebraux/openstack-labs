# Mise à jour de l'overcloud


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#doc-wrapper](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#doc-wrapper)


---
## Disabling fencing in the overcloud

Fencing is the process of isolating a failed node to protect the cluster and the cluster resources. Without fencing, a failed node might result in data corruption in a cluster. Director uses Pacemaker to provide a highly available cluster of Controller nodes.

Pacemaker uses a process called STONITH to fence failed nodes. STONITH is an acronym for "Shoot the other node in the head". STONITH is disabled by default and requires manual configuration so that Pacemaker can control the power management of each node in the cluster.

Deploying a highly available overcloud without STONITH is not supported. You must configure a STONITH device for each node that is a part of the Pacemaker cluster in a highly available overcloud. For more information on STONITH and Pacemaker, see Fencing in a Red Hat High Availability Cluster and Support Policies for RHEL High Availability Clusters.

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#proc_deploying-fencing_fencing](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#proc_deploying-fencing_fencing)

Se connecter sur un controller
``` bash
ssh tripleo-admin@10.129.172.50
```

Vérifier l'état du cluster HA
``` bash
sudo pcs status
```
- Pas d'erreur sur les reeources
- 3 éléments par bundle started, master ou slave

``` bash
sudo pcs property show
# Cluster Properties:
#  OVN_REPL_INFO: rhos-ctrl-br-0
#  cluster-infrastructure: corosync
#  cluster-name: tripleo_cluster
#  dc-version: 2.0.5-9.el8_4.5-ba59be7122
#  have-watchdog: false
#  last-lrm-refresh: 1698701377
#  redis_REPL_INFO: rhos-ctrl-br-0
#  stonith-enabled: true
```

Désactiver le STONITH

``` bash
sudo pcs property set stonith-enabled=false
```

Et vérifier
``` bash
sudo pcs property show
# Cluster Properties:
#   ...
#  stonith-enabled: false
```


Générer le fichier de fencing

Sur le serveur director
``` bash
source stackrc
openstack overcloud generate fencing --output templates/prod-environment/01-fencing.yaml nodes/nodes-controller.json
```

Modifier le fichier, pour changer la valeur : `EnableFencing: false`

Ajouter la prise en charge du fichier dans le script de deploy de l'overcloud `/home/stack/templates/overcloud-answers.yaml`
``` yaml
    # --- fencing configuration  ---
    #  from : https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#proc_deploying-fencing_fencing
  - /home/stack/templates/prod-environment/01-fencing.yaml
```

---
## Undercloud node database backup

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/backing_up_and_restoring_the_undercloud_and_control_plane_nodes/index#doc-wrapper](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/backing_up_and_restoring_the_undercloud_and_control_plane_nodes/index#doc-wrapper)
  
``` bash
# get inventory
cp output/tripleo-ansible-inventory.yaml  ~/tripleo-inventory.yaml

mkdir backup/overcloud
cd backup/overcloud

openstack undercloud backup --db-only --inventory ~/tripleo-inventory.yaml
```

---
## Modification de la définition des roles

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#updating-composable-services-in-custom-roles_data-files-initial-steps](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#updating-composable-services-in-custom-roles_data-files-initial-steps)

Ajout ou suppression de certaines ressources

---
## Modification des fichiers de configuration réseau

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#network-configuration-file-conversion_initial-steps](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/initial-steps-for-overcloud-preparation_preparing-overcloud#network-configuration-file-conversion_initial-steps)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_configuring-overcloud-networking_installing-director-on-the-undercloud#proc_manually-converting-NIC-templates-to-jinja-ansible-format_overcloud_networking](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_configuring-overcloud-networking_installing-director-on-the-undercloud#proc_manually-converting-NIC-templates-to-jinja-ansible-format_overcloud_networking)

Un mécanisme va les convertir en Jinja2. Mais tout n'est pas supporté.

Dans les fichiers de config, les éléments ci dessous posent problème :

- 'get_file'
- 'get_resource'
- 'str_replace'
- 'yaql'

Mais heureusement, c'est dans les ressources  `OsNetConfigImpl` et `MinViableMtu`.

Il faut tout de même fusionner les ressources `MinViableMtuBondEx' et `MinViableMtuBondStorage`


``` bash
cd template/prod-configuration/nics-config

cp /usr/share/openstack-tripleo-heat-templates/tools/convert_heat_nic_config_to_ansible_j2.py

python3 convert_heat_nic_config_to_ansible_j2.py --standalone  --networks_file /home/stack/templates/network_data.yaml  compute.yaml 

python3 convert_heat_nic_config_to_ansible_j2.py --standalone  --networks_file /home/stack/templates/network_data.yaml  computetrunk.yaml 

python3 convert_heat_nic_config_to_ansible_j2.py --standalone  --networks_file /home/stack/templates/network_data.yaml  controller.yaml 
```

et Mettre à jour le fichier `network-environment.yaml` : `template/prod-environment/33-network-environment.yaml`

Remplacer
``` bash
resource_registry:
  # Network Interface templates to use (these files must exist). You can
  # override these by including one of the net-*.yaml environment files,
  # such as net-bond-with-vlans.yaml, or modifying the list here.
  OS::TripleO::Controller::Net::SoftwareConfig: /home/stack/templates/prod-configuration/nics-config/controller.yaml
  OS::TripleO::Compute::Net::SoftwareConfig: /home/stack/templates/prod-configuration/nics-config/compute.yaml
  OS::TripleO::ComputeTrunk::Net::SoftwareConfig: /home/stack/templates/prod-configuration/nics-config/computetrunk.yaml
 
```
Par
``` bash
parameter_defaults:
  ControllerNetworkConfigTemplate: '/home/stack/templates/prod-configuration/nics-config/controller.j2'
  ComputeNetworkConfigTemplate: '/home/stack/templates/prod-configuration/nics-config/compute.j2'
  ComputeTrunkNetworkConfigTemplate: '/home/stack/templates/prod-configuration/nics-config/computetrunk.j2'
```

---
## Performing the overcloud adoption and preparation


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/running-the-overcloud-upgrade-preparation_overcloud-adoption](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/running-the-overcloud-upgrade-preparation_overcloud-adoption)

Vérifier les fichiers dans `~/overcloud-deploy directory`:

- tripleo-<stack>-passwords.yaml
- tripleo-<stack>-network-data.yaml
- tripleo-<stack>-virtual-ips.yaml
- tripleo-<stack>-baremetal-deployment.yaml

Fichier de mot de passe
``` bash
cp ~/overcloud-deploy/overcloud/tripleo-overcloud-passwords.yaml  ~/overcloud-deploy/overcloud/overcloud-passwords.yaml
```

Configuration réseau
``` bash
cp ~/overcloud-deploy/overcloud/tripleo-overcloud-network-data.yaml ~/

mkdir ~/overcloud_adopt

openstack overcloud network provision --debug \
--output /home/stack/overcloud_adopt/generated-networks-deployed.yaml tripleo-overcloud-network-data.yaml
``` 

Virtual IPs
``` bash
cp ~/overcloud-deploy/overcloud/tripleo-overcloud-virtual-ips.yaml ~/

openstack overcloud network vip provision --debug \
--stack overcloud --output \
/home/stack/overcloud_adopt/generated-vip-deployed.yaml tripleo-overcloud-virtual-ips.yaml
```

provision the overcloud nodes.
``` bash
cp ~/overcloud-deploy/overcloud/tripleo-overcloud-baremetal-deployment.yaml ~/
```

Edit ~/tripleo-overcloud-baremetal-deployment.yaml, to correct template: null
`/home/stack/templates/prod-configuration/nics-config/compute.j2`

```bash
openstack overcloud node provision --debug --stack overcloud \
--output /home/stack/overcloud_adopt/baremetal-deployment.yaml \
tripleo-overcloud-baremetal-deployment.yaml
```



---
## Generation de la liste des images
sauvegarde  the containers-prepare-parameter.yaml file that you used for the undercloud upgrade:
``` bash
cp containers-prepare-parameter.yaml containers-prepare-parameter.yaml.orig
```

Afficher la liste des rôles : 
``` bash
sudo awk '/tripleo_role_name/ {print "--role " $2}' \
/var/lib/mistral/overcloud/tripleo-ansible-inventory.yaml
# --role Controller
# --role Compute
# --role ComputeTrunk
```

``` bash
export NAMESPACE='"namespace":"registry.redhat.io/rhosp-rhel9",'
export EL8_NAMESPACE='"namespace":"registry.redhat.io/rhosp-rhel8",'
export NEUTRON_DRIVER='"neutron_driver":"ovn",'
export EL8_TAGS='"tag":"17.1",'
export EL9_TAGS='"tag":"17.1",'
export CONTROL_PLANE_ROLES='--role Controller'
export COMPUTE_ROLES='--role Compute, --role ComputeTrunk'
export CEPH_TAGS='"ceph_tag":"latest",'

```

``` bash
python3 /usr/share/openstack-tripleo-heat-templates/tools/multi-rhel-container-image-prepare.py \
     ${COMPUTE_ROLES} \
     ${CONTROL_PLANE_ROLES} \
     --local-push-destination \
     --enable-multi-rhel \
     --excludes collectd \
     --excludes nova-libvirt \
     --minor-override "{${EL8_TAGS}${EL8_NAMESPACE}${CEPH_TAGS}${NEUTRON_DRIVER}\"no_tag\":\"not_used\"}" \
     --major-override "{${EL9_TAGS}${NAMESPACE}${CEPH_TAGS}${NEUTRON_DRIVER}\"no_tag\":\"not_used\"}" \
     --output-env-file \
    /home/stack/containers-prepare-parameter.yaml
```

Ajouter l'authentification
``` yaml
parameter_defaults:

  ContainerImageRegistryCredentials:
    registry.redhat.io:
      DISI1: DiSi2003

  ContainerImagePrepare:

```

---
## Gestion de ceph-ansible

``` bash
 sudo rpm -q ceph-ansible
ceph-ansible-6.0.28.6-1.el8cp.noarch
```

Ceph-ansible n'est plus utilisé avec ceph5, et donc RHOS 17.1
Mais provoque un conflit

sudo dnf remove ceph-ansible

Run the Red Hat Ceph Storage external upgrade process with the ceph tag
``` bash
source stackrc
openstack overcloud external-upgrade run \
   --skip-tags "ceph_health,opendev-validation, ceph_ansible_remote_tmp" \
   --stack overcloud \
   --tags ceph,facts 2>&1
```

Configure the Red Hat Ceph Storage nodes to use cephadm
``` bash
openstack overcloud external-upgrade run \
    --skip-tags ceph_health,opendev-validation,ceph_ansible_remote_tmp \
    --stack overcloud \
    --tags cephadm_adopt  2>&1

``` bash
openstack overcloud upgrade run \
--stack overcloud \
--skip-tags ceph_health,opendev-validation,ceph_ansible_remote_tmp \
--tags setup_packages --limit ceph_osd,ceph_mon,Undercloud \
--playbook /home/stack/overcloud-deploy/overcloud/config-download/overcloud/upgrade_steps_playbook.yaml 2>&1
```

``` bash
ANSIBLE_LOG_PATH=/home/stack/cephadm_enable_user_key.log \
ANSIBLE_HOST_KEY_CHECKING=false \
ansible-playbook -i /home/stack/overcloud-deploy/overcloud/config-download/overcloud/tripleo-ansible-inventory.yaml \
  -b -e ansible_python_interpreter=/usr/libexec/platform-python /usr/share/ansible/tripleo-playbooks/ceph-admin-user-playbook.yml \
 -e tripleo_admin_user=ceph-admin \
 -e distribute_private_key=true \
  --limit ceph_osd,ceph_mon,Undercloud
  ```

---
## Vérifications

Fichiers pour adoption :

- baremetal-deployment.yaml : 
  - liste des serveur déployé. nombe, config réseau
- generated-networks-deployed.yaml :
  - liste des réseaux déployés : subnet, Vlan,   ...
- generated-vip-deployed.yaml : list des IP fixes, pour les API. 


---
## lancement du prépare
``` bash
$ source stackrc
$ chmod 755 /home/stack/overcloud_upgrade_prepare.sh
$ sh /home/stack/overcloud_upgrade_prepare.sh
```

---
## récupération des images

```
openstack overcloud external-upgrade run --stack overcloud --tags container_image_prepare
```

---
## lancement de la migration

```
    openstack overcloud upgrade run --yes --stack overcloud --debug --limit allovercloud,undercloud --playbook all
```



---
## autres


### create a snaphot
openstack overcloud backup snapshot --inventory ~/tripleo-inventory.yaml

remove backup snapshot
``` bash
openstack overcloud backup snapshot --remove --inventory ~/tripleo-inventory.yaml
```

``` bash
openstack overcloud upgrade run --yes --stack overcloud --debug --limit allovercloud,undercloud --playbook all"
```

### Telemetry
The OpenStack Telemetry services are deprecated in favor of Service Telemetry Framework (STF) for metrics and monitoring. The legacy telemetry services are only available in RHOSP 17.1 to help facilitate the transition to STF and will be removed in a future version of RHOSP.