# mise à jour des OS en 9.2


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/upgrading-the-undercloud-operating-system#setting-the-ssh-root-permission-parameter-on-the-undercloud-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/upgrading-the-undercloud-operating-system#setting-the-ssh-root-permission-parameter-on-the-undercloud-undercloud)



edit the file and set the PermitRootLogin parameter: `PermitRootLogin no`

(portant le fichier est géré pa Ansible ...

Rotation de la clé, ca elle est <1024
``` bash
ssh-keygen -l -f ~/.ssh/id_rsa.pub
# 1024 ...

ansible-playbook \
-i ~/overcloud-deploy/overcloud/tripleo-ansible-inventory.yaml \
/usr/share/ansible/tripleo-playbooks/ssh_key_rotation.yaml

ll  /home/stack/backup_keys/

ssh-keygen -l -f ~/.ssh/id_rsa.pub
# 4096 SHA256:CCBmbyHSWvHrI7nx+aA1YHR8j5O081FA/LNUi39d4rA stack@rhos-director-br-01.imta.fr (RSA)

```

create the file /home/stack/templates/system_upgrade.yaml 
``` bash
parameter_defaults:
  UpgradeLeappDevelSkip: "LEAPP_UNSUPPORTED=1 LEAPP_DEVEL_SKIP_CHECK_OS_RELEASE=1 LEAPP_NO_NETWORK_RENAMING=1 LEAPP_DEVEL_TARGET_RELEASE=9.2"
  UpgradeLeappDebug: false
  UpgradeLeappEnabled: true
  LeappActorsToRemove: ['checkifcfg','persistentnetnamesdisable','checkinstalledkernels','biosdevname']
  LeappRepoInitCommand: |
     subscription-manager repos --disable=*
     subscription-manager repos --enable rhel-8-for-x86_64-baseos-tus-rpms --enable rhel-8-for-x86_64-appstream-tus-rpms --enable openstack-17.1-for-rhel-8-x86_64-rpms
     subscription-manager release --set=8.4
  UpgradeLeappCommandOptions: "--enablerepo=rhel-9-for-x86_64-baseos-eus-rpms --enablerepo=rhel-9-for-x86_64-appstream-eus-rpms --enablerepo=rhel-9-for-x86_64-highavailability-eus-rpms --enablerepo=openstack-17.1-for-rhel-9-x86_64-rpms --enablerepo=fast-datapath-for-rhel-9-x86_64-rpms"
```

sudo dnf update
sudo yum remove rubygem-irb
sudo yum install rubygem-irb rubygem-rdoc
https://access.redhat.com/solutions/6649111


 sudo dnf remove ruby-irb


Lancer la mise à jour
``` bash
source stackrc
openstack undercloud upgrade --yes --system-upgrade \
/home/stack/templates/system_upgrade.yaml
```



``` bash
sudo reboot
```
Vérification

``` bash
cat /etc/redhat-release
Red Hat Enterprise Linux release 8.4 (Ootpa)

cat /etc/rhosp-release
Red Hat OpenStack Platform release 16.2.5 (Train)
```
---
## upgrade des controlleurs



``` bash

export NAMESPACE='"namespace":"registry.redhat.io/rhosp-rhel9",'
export EL8_NAMESPACE='"namespace":"registry.redhat.io/rhosp-rhel8",'
export NEUTRON_DRIVER='"neutron_driver":"ovn",'
export EL8_TAGS='"tag":"17.1",'
export EL9_TAGS='"tag":"17.1",'
export CONTROL_PLANE_ROLES='--role Controller'
export COMPUTE_ROLES='--role Compute --role ComputeTrunk'
export CEPH_TAGS='"ceph_tag":"latest",'

python3 \
/usr/share/openstack-tripleo-heat-templates/tools/multi-rhel-container-image-prepare.py \
     ${COMPUTE_ROLES} \
     --local-push-destination \
     --enable-multi-rhel \
     --excludes collectd \
     --excludes nova-libvirt \
     --minor-override \
"{${EL8_TAGS}${EL8_NAMESPACE}${CEPH_TAGS}${NEUTRON_DRIVER}\"no_tag\":\"not_used\"}" \
     --major-override \
     "{${EL9_TAGS}${NAMESPACE}${CEPH_TAGS}${NEUTRON_DRIVER}\"no_tag\":\"not_used\"}" \
     --output-env-file \
/home/stack/containers-prepare-parameter.yaml
``` 


définir le bootstap controller node 

``` bash
ssh heat-admin@ctrl0 "sudo hiera -c /etc/puppet/hiera.yaml pacemaker_short_bootstrap_node_name"
# rhos-ctrl-br-0  
```
```



---
## Compute


---
## post

### 11.3. Updating the default machine type for hosts after an upgrade to RHOSP 17
``` bash
sudo podman exec -i -u root nova_api \
  nova-manage libvirt list_unset_machine_type
No instances found without hw_machine_type set.
```

### 11.4. Re-enabling fencing in the overcloud

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/performing-post-upgrade-actions
``` bash
source ~/stackrc
ssh tripleo-admin@rhos-ctrl-br-0 "sudo pcs property set stonith-enabled=true"
```