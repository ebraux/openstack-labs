

## création des réseaux

https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_configuring-and-provisioning-overcloud-network-definitions_network_provisioning


Comme ils existent déjà, ça provoque une erreur.
Mais ça aurait du générer le fichier /home/stack/templates/overcloud-networks-deployed.yaml

On reprend celui créé pendnat l'adoption, et on le copie dans tempalte

check : 
``` bash

source stackrc
openstack network list
# +--------------------------------------+------------------+--------------------------------------+
# | ID                                   | Name             | Subnets                              |
# +--------------------------------------+------------------+--------------------------------------+
# | 1165223b-d212-40b7-ba49-bd160839538b | management       | e20743ef-12e1-413f-8b23-1da6534fe2b9 |
# | 15f25f96-9120-4e7b-bc58-2fbc202eca22 | storage_mgmt     | 6a279ab5-17fc-41d3-97b8-f84bbb564ddd |
# | 470550b5-977c-4b6c-bc16-7d80d91c1859 | storage          | e317c415-7d1d-495d-b49a-ca4b0fd4ca31 |
# | 6e950b67-c549-403f-83be-131ad843b0ca | tenant           | a5fcc614-ca2f-4ae0-9988-7b1c487d5803 |
# | 710f1ae1-4193-4066-a691-3fa3f6c63f7c | ovn_mac_addr_net |                                      |
# | 7cb3f79d-5f80-4246-a00d-1f9520060f12 | ctlplane         | 148687f4-cc3f-4368-9694-3b4452d8b121 |
# | 9a3ed4a4-7aa0-45e5-bba6-b5f561ca24d3 | internal_api     | 3595028c-ab71-4f56-88ae-ee1bc43c2aa8 |
# | 9b4206e3-8819-4304-8a4b-5050f1129b38 | external         | c3bfbadb-5c87-4009-84a9-b1ad7156f13f |
# +--------------------------------------+------------------+--------------------------------------+

openstack subnet list
# +--------------------------------------+---------------------+--------------------------------------+------------------+
# | ID                                   | Name                | Network                              | Subnet           |
# +--------------------------------------+---------------------+--------------------------------------+------------------+
# | 148687f4-cc3f-4368-9694-3b4452d8b121 | ctlplane-subnet     | 7cb3f79d-5f80-4246-a00d-1f9520060f12 | 10.129.173.0/24  |
# | 3595028c-ab71-4f56-88ae-ee1bc43c2aa8 | internal_api_subnet | 9a3ed4a4-7aa0-45e5-bba6-b5f561ca24d3 | 192.168.174.0/24 |
# | 6a279ab5-17fc-41d3-97b8-f84bbb564ddd | storage_mgmt_subnet | 15f25f96-9120-4e7b-bc58-2fbc202eca22 | 192.168.177.0/24 |
# | a5fcc614-ca2f-4ae0-9988-7b1c487d5803 | tenant_subnet       | 6e950b67-c549-403f-83be-131ad843b0ca | 192.168.175.0/24 |
# | c3bfbadb-5c87-4009-84a9-b1ad7156f13f | external_subnet     | 9b4206e3-8819-4304-8a4b-5050f1129b38 | 10.129.176.0/22  |
# | e20743ef-12e1-413f-8b23-1da6534fe2b9 | management_subnet   | 1165223b-d212-40b7-ba49-bd160839538b | 10.129.173.0/24  |
# | e317c415-7d1d-495d-b49a-ca4b0fd4ca31 | storage_subnet      | 470550b5-977c-4b6c-bc16-7d80d91c1859 | 192.168.176.0/24 |
# +--------------------------------------+---------------------+--------------------------------------+------------------+

#(undercloud)$ openstack network show <network>
#(undercloud)$ openstack subnet show <subnet>
```

---
## getsion des vip

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_configuring-and-provisioning-network-vips-for-the-overcloud_network_provisioning](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_configuring-and-provisioning-network-vips-for-the-overcloud_network_provisioning)
  
idem. on recupère le fichier généré lors de l'adoption

vérificetion
``` bash
openstack port list 
+--------------------------------------+--------------------------------------------+-------------------+--------------------------------------------------------------------------------+--------+
| ID                                   | Name                                       | MAC Address       | Fixed IP Addresses                                                             | Status |
+--------------------------------------+--------------------------------------------+-------------------+--------------------------------------------------------------------------------+--------+

| 18997e68-de97-4185-9310-1261663999ec | storage_mgmt_virtual_ip                    | fa:16:3e:b0:3e:c6 | ip_address='192.168.177.9', subnet_id='6a279ab5-17fc-41d3-97b8-f84bbb564ddd'   | DOWN   |
| 1bd52821-1a7e-4bb6-809f-36d6209c34fd | public_virtual_ip                          | fa:16:3e:bc:ed:28 | ip_address='10.129.176.9', subnet_id='c3bfbadb-5c87-4009-84a9-b1ad7156f13f'    | DOWN   |
| 22b9bdd2-9585-41b2-bc27-4d42da1775ea | control_virtual_ip                         | fa:16:3e:b7:e5:68 | ip_address='10.129.173.9', subnet_id='148687f4-cc3f-4368-9694-3b4452d8b121'    | DOWN   |
| 50ce7b70-fcb9-4738-bb30-8100fad0028f | storage_virtual_ip                         | fa:16:3e:d1:93:42 | ip_address='192.168.176.9', subnet_id='e317c415-7d1d-495d-b49a-ca4b0fd4ca31'   | DOWN   |
| 5e1f0994-8a0d-4bc3-be1c-5d7eaff0d13f | internal_api_virtual_ip                    | fa:16:3e:ee:56:11 | ip_address='192.168.174.9', subnet_id='3595028c-ab71-4f56-88ae-ee1bc43c2aa8'   | DOWN   |

| 05619df7-d001-4c04-b866-998029bfcf57 | ovn_dbs_virtual_ip                         | fa:16:3e:71:e9:eb | ip_address='192.168.174.7', subnet_id='3595028c-ab71-4f56-88ae-ee1bc43c2aa8'   | DOWN   |
| 8a1c97fc-c012-4bcb-9640-bb597bc37bfc | redis_virtual_ip                           | fa:16:3e:28:96:9f | ip_address='192.168.174.8', subnet_id='3595028c-ab71-4f56-88ae-ee1bc43c2aa8'   | DOWN   |

+--------------------------------------+--------------------------------------------+-------------------+--------------------------------------------------------------------------------+--------+

```
control_virtual_ip



---
##

validation run --group pre-introspection \
 --inventory tripleo-inventory.yaml


Attention, cahngement de façon de faire

On déploie sépareémnt :
 les réseaux
  -  [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#assembly_provisioning-the-overcloud-networks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#assembly_provisioning-the-overcloud-networks)
 
 les vip
 - [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_configuring-and-provisioning-network-vips-for-the-overcloud_network_provisioning](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_configuring-and-provisioning-network-vips-for-the-overcloud_network_provisioning)
  

 Et les neouds.
 - [[internal_api_subnet](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_provisioning-bare-metal-nodes-for-the-overcloud_ironic_provisioning)](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_provisioning-and-deploying-your-overcloud#proc_provisioning-bare-metal-nodes-for-the-overcloud_ironic_provisioning)
   - création du fichier
   - et ensuite génération : 10.Provision the overcloud nodes:


 enusite à partir des fichcier s"deployed", on déploie l'overcloud


---
## deprovisioning
``` bash
$ metalsmith list
+--------------------------------------+---------------------+--------------------------------------+---------------------+-------+--------------+
| UUID                                 | Node Name           | Allocation UUID                      | Hostname            | State | IP Addresses |
+--------------------------------------+---------------------+--------------------------------------+---------------------+-------+--------------+
| 25e74342-1e77-435c-a6c5-3ad2674e2e3a | rhos-comptrunk-br-0 | 8830a290-9bd8-44a8-80e3-c68f595e459e | rhos-comptrunk-br-0 | ERROR |              |
+--------------------------------------+---------------------+--------------------------------------+---------------------+-------+--------------+
(undercloud) [stack@rhos-director-br-01 scripts]$ metalsmith undeploy rhos-comptrunk-br-0
Unprovisioning started for node rhos-comptrunk-br-0 (UUID 25e74342-1e77-435c-a6c5-3ad2674e2e3a)

```


avec `HostnameFormatDefault: 'rhos-comptrunk-br-%index%'` dans la definition de role, il faut oblitoirement 1 instance du role ...



----
## netw pc RHV et MAC spoofing

https://access.redhat.com/solutions/2110541





openstack baremetal node show 11e4b493-2f61-4a09-9a17-de0e1643e819
                                      |
| driver_info            | {'deploy_kernel': 'file:///var/lib/ironic/httpboot/agent.kernel', 'rescue_kernel': 'file:///var/lib/ironic/httpboot/agent.kernel', 'deploy_ramdisk': 'file:///var/lib/ironic/httpboot/agent.ramdisk', 'rescue_ramdisk': 'file:///var/lib/ironic/httpboot/agent.ramdisk', 'ipmi_username': 'root', 'ipmi_password': '******', 'ipmi_address': '10.29.20.53'}                                                                                                   |
| driver_internal_info   | {'last_power_state_change': '2023-11-14T13:05:35.237401', 'is_whole_disk_image': True, 'deploy_steps': None, 'deploy_boot_mode': 'uefi'}                                                                                                                                                                                                                                                                                                                      |
| extra                  | {'metalsmith_attached_ports': ['c81a6c02-d012-4625-9865-8a52229431ff'], 'metalsmith_created_ports': ['c81a6c02-d012-4625-9865-8a52229431ff']}                                                                                                                                                                                                                                              metalsmith undeploy  rhos-ctrl-br-0                                   

---
le boot par défuat c'ets maintenant UEFI

Avant il fallait le forcer :
``` bash 
openstack baremetal node set --property capabilities="boot_mode:uefi,<capability_1>,...,<capability_n>" <node>
```

https://bugs.launchpad.net/tripleo/+bug/1964397


openstack baremetal node set --property capabilities='profile:control,boot_option:local' aebce0ac-be39-4355-aafc-4fd3bb462ccd

NODE=rhos-ctrl-br-0
openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities

openstack baremetal node set --property capabilities="boot_mode:bios,$(openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities)" $NODE

openstack baremetal node show ${NODE} -f json -c properties | jq -r .properties.capabilities