# Undercloud upgrade

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_upgrading-the-undercloud_upgrading-the-undercloud#proc_enabling-repositories-for-the-undercloud_upgrading-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_upgrading-the-undercloud_upgrading-the-undercloud#proc_enabling-repositories-for-the-undercloud_upgrading-the-undercloud)
  
## Enabling repositories for the undercloud

``` bash
sudo subscription-manager repos --disable=*
sudo subscription-manager repos \
--enable=rhel-8-for-x86_64-baseos-tus-rpms \
--enable=rhel-8-for-x86_64-appstream-tus-rpms \
--enable=rhel-8-for-x86_64-highavailability-tus-rpms \
--enable=openstack-17.1-for-rhel-8-x86_64-rpms \
--enable=fast-datapath-for-rhel-8-x86_64-rpms
```

Switch the container-tools module version to RHEL 8 on all nodes:
``` bash
sudo dnf -y module switch-to container-tools:rhel8
```

Install the command line tools for director installation and configuration
``` bash
sudo dnf install -y python3-tripleoclient
```


---
## Validating RHOSP before the upgrade

``` bash
source ~/stackrc
```

Install the packages for validation
``` bash
sudo dnf -y update openstack-tripleo-validations python3-validations-libs validations-common
```

Copy the inventory from mistral:
``` bash
sudo chown stack:stack /var/lib/mistral/.ssh/tripleo-admin-rsa
sudo cat /var/lib/mistral/overcloud/tripleo-ansible-inventory.yaml > inventory.yaml
```
---
## FIX validation process

shh access denied  :

- Some pre-upgrade validations failed during FFU to 17.1 due to Permission denied
- [https://access.redhat.com/solutions/7040292](https://access.redhat.com/solutions/7040292)
``` bash
sudo chown -R stack:stack /var/lib/mistral/.ssh
```

### Podman version Error
- The package-version validation failed due to version mismatching issue of podman during FFU to 17.1
- [https://access.redhat.com/solutions/7040492](https://access.redhat.com/solutions/7040492)
- RHOSP16.2 will use podman v3.2 but the validation exepcts podman v1.6.
- This validation's failure is ignorable.  

### check-latest-packages-version : localhost: No packages given to check. 
- The check-latest-packages-version validation failed due to No packages given to check during FFU to 17.1
- [https://access.redhat.com/solutions/7040488](https://access.redhat.com/solutions/7040488)
- This validation's failure is ignorable.
- Pour la corriger, ajouter une var avec la liste de package dans le fichier inventory
``` yaml
Undercloud:
  hosts:
    undercloud: {}
  vars:
    ...
    username: admin
    packages_list: ['podman'] <================ (*) Add
```

### undercloud: An unhandled exception occurred while running th
- The collect-flavors-and-verify-profiles validation failed due to Authorization Failure during FFU to 17.1
- [https://access.redhat.com/solutions/7040470](https://access.redhat.com/solutions/7040470)
- "Authorization Failure. A user and password or token is required"
- Ajouter le login et mdp dans le fichier /home/stack/inventory.yaml
``` yaml
Undercloud:
  hosts:
    undercloud: {}
  vars:
:
    overcloud_horizon_url: http://10.0.0.132:80/dashboard
    overcloud_keystone_url: http://10.0.0.132:5000
    plan: overcloud
    project_name: admin
    undercloud_service_list: [tripleo_nova_compute, tripleo_heat_engine, tripleo_ironic_conductor,
      tripleo_swift_container_server, tripleo_swift_object_server, tripleo_mistral_engine]
    undercloud_swift_url: https://192.168.24.2:13000
    username: admin
    password: MnfaEpwqBHiXAdprSsiHKffHx <========================== (*) Add
```

tripleo_glance_api_cron healthcheck
tripleo_swift_container_updater.service
---
## 
- 
Run the validation
``` bash
validation run -i inventory.yaml --group pre-upgrade
```

Les logs individuels sont dans le dossier `/var/log/validations/`.

Pour exécuter les validation de façon indépendantes

- les packages de validation sont dans le dossier `/usr/share/ansible/validation-playbooks/`
- il est possible de les executer manuellement, en mode debug.

``` bash  
 validation run -i inventory.yaml --validation /usr/share/ansible/validation-playbooks/package-version.yaml -vvv
 ```


Liste :
- check-disk-space-pre-upgrade
- check-ftype
F check-latest-packages-version
- check-ram
- check-rsyslog
- check-reboot
- collect-flavors-and-verify-profiles
- compute-tsx
F container-status
- image-serve
- ironic-boot-configuration
- node-health
- nova-status
- ntp
- openstack-endpoints
- package-version
- repos
F service-status
F stack-health
- system-encoding
- tripleo-latest-packages-version
- undercloud-disabled-services
F undercloud-disk-space-pre-upgrade
- undercloud-service-status
- undercloud-sysctl
- validate-selinux



---
## Prepare conteneurs

Back up the 16.2 containers-prepare-parameter.yaml file:
``` bash
cp containers-prepare-parameter.yaml \
containers-prepare-parameter.yaml.16.2
```

Generate the default container image preparation file:
``` bash
openstack tripleo container image prepare default \
  --local-push-destination \
  --output-env-file containers-prepare-parameter.yaml
```

Modify config file
- Ajouter les infos de connexion à le registry ReddHat
``` yaml
parameter_defaults:
  
  ContainerImageRegistryCredentials:
    registry.redhat.io:
      DISI1: DiSi2003

  ContainerImagePrepare:
    ...
``` 
- Configurer les images pour CEPH (même si on ne les utilise pas). Adapter les noms des images en conséquence
``` yaml
parameter_defaults:
  
  ContainerImagePrepare:
  - push_destination: true
    set:
      ceph_image: rhceph-5-rhel8
      ceph_grafana_image: rhceph-5-dashboard-rhel8
``` 

Regénérer le fichier de variables des images (sinon ça plante sur des problèmes d'authentification)
``` bash
sudo openstack tripleo container image prepare -e /home/stack/containers-prepare-parameter.yaml --output-env-file /home/stack/container-parameters.yaml
```

---
## Modification de la config de l'undercloud

Dans le fichier undercloud.conf, vérifier la valeur de
``` bash
container_images_file = /home/stack/containers-prepare-parameter.
custom_env_files = /home/stack/custom-undercloud-params.yaml
```

POur pouvoir continuer a utiliser le format "undercloud.conf', avec RHOS 17.1, il faut ajouter une variable de customisation.

Dans le fichier `custom-undercloud-params.yaml` ajouter
``` yaml
parameter_defaults:
  # ...
  SkipRhelEnforcement: true
```

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_upgrading-the-undercloud_upgrading-the-undercloud#updating-the-undercloud-conf-file-upgrading-the-undercloud](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/framework_for_upgrades_16.2_to_17.1/assembly_upgrading-the-undercloud_upgrading-the-undercloud#updating-the-undercloud-conf-file-upgrading-the-undercloud)

---
## Lancement de la mise à jour

``` bash
 openstack undercloud upgrade --no-validations
 ```

Si ça plante sur la tache 'migrate existing introspection data'
- relancer mysql
``` bash
sudo podman restart mysql
```
- relancer l'upgrade
``` bash

```
- Openstack undercloud upgrade fails on task migrate existing introspection data
- [https://access.redhat.com/solutions/7029979](https://access.redhat.com/solutions/7029979)


---
## Vérification post-installation

``` bash
cat /etc/redhat-release
Red Hat Enterprise Linux release 8.4 (Ootpa)

cat /etc/rhosp-release
Red Hat OpenStack Platform release 17.1.1 (Wallaby)
```

La version de RHEL n'a pas changé, mais on est passé de la version 16.2 à la version 17.1 de RHOS.