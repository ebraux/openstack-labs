

---
## FS sur un controller


Un gros VG, avec un "LV Pool".
et dedans les LV.
/var prend presque tout ...


``` bash

disque : 893.17 GiB
VG Name               vg


lv_thinpool                
  LV Size                <891.13 GiB
  Allocated pool data    2.01%


/dev/mapper/vg-lv_root    11G  1.8G  8.8G  18% /
lv_root
  LV Size                10.52 GiB
  Mapped size            16.17%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_tmp    1.2G   43M  1.2G   4% /tmp
lv_tmp
  LV Size                1.16 GiB
  Mapped size            0.90%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_var    799G   15G  784G   2% /var
lv_var
  LV Size                798.51 GiB
  Mapped size            1.08%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_log    9.6G  7.2G  2.4G  75% /var/log
lv_log
  LV Size                <9.55 GiB
  Mapped size            76.12%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_audit  2.1G   86M  2.0G   5% /var/log/audit
lv_audit
  LV Size                <2.05 GiB
  Mapped size            3.45%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_home   1.2G   43M  1.2G   4% /home
lv_home
  LV Size                1.16 GiB
  Mapped size            0.89%
  LV Pool name           lv_thinpool

/dev/mapper/vg-lv_srv     69G  692M   68G   1% /srv
lv_srv
  LV Size                <68.19 GiB
  Mapped size            0.27%
  LV Pool name           lv_thinpool
```

---
## Modifier la taille des partitions


A la main :
Stopper les services "tripleo"
``` bash
systemctl stop 'tripleo_*'
```

Stopper les services gérés par pcs

- Vérifier l'état des services
  - galera : Promoted
  - haproxy: Started
  - rabbitmq : Started
  - redis : Promoted ou Unpromoted
``` bash
pcs status
```
- Mettre le noeud en mode "standby
``` bash
pcs node standby rhos-ctrl-br-03
```
- Vérifier que les services sont bien arrêtés :
  - galera : Stopped
  - haproxy: Stopped
  - rabbitmq : Stopped
  - redis : Stopped
``` bash
pcs status
```
Vérifier que plus aucun container ne tourne :
``` bash
podman ps
```
Le pb, c'est qu'il reste du monde qui utilise /var/log
``` bash
lsof /var/log
```
Test de reboot en mode single : echec
- [https://access.redhat.com/solutions/2794121](https://access.redhat.com/solutions/2794121)



Déplacer les fichiers
``` bash
mkdir /var/log.tmp && rsync -av /var/log/* /var/log.tmp --progress
umount /var/log
mv /var/log.tmp /var/log
```
Empécher le montage au redémarrage
``` bash
```


Avec Director :
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/installing_and_managing_red_hat_openstack_platform_with_director/index#proc_configuring-whole-disk-partitions-for-object-storage_ironic_provisioning](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/installing_and_managing_red_hat_openstack_platform_with_director/index#proc_configuring-whole-disk-partitions-for-object-storage_ironic_provisioning)
