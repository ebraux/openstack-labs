# Remplacement d'un controller

---
## Principe

- Si le controleur est encore en service, désactiver les services
- Ensuite, dans tous les cas
    - Vérifier qu'il n'y a pas de traces de l'ancien controleur
    - Désactiver la gestion de MariaDB par pacemaker, pour éviter les probl-me de cluster avec les arrêt/redémarrage
    - Intégrer le nouveau serveur
    - Réactiver la gestion de MariaDB par pacemaker.

doc : 
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/director_installation_and_usage/index#assembly_replacing-controller-nodes](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html-single/director_installation_and_usage/index#assembly_replacing-controller-nodes)


---
## Préparation


Charger l'environnement de l'undercloud : 
``` bash
cd
source stackrc
```

Vérifier l'état du déploiement. The overcloud stack and its subsequent child stacks should have either a CREATE_COMPLETE or UPDATE_COMPLETE.
``` bash
openstack stack list --nested
openstack stack list --nested | grep -v _COMPLETE

```

Installer le client Mysql 
``` bash
sudo dnf -y install mariadb
```

Et configurer l'accès à la base
``` bash
sudo cat /root/.my.cnf
sudo cp /var/lib/config-data/puppet-generated/mysql/root/.my.cnf /root/
```

Faire un backup de la base
``` bash
mkdir /home/stack/backup
ll  /home/stack/backup
sudo mysqldump --all-databases --quick --single-transaction | gzip > /home/stack/backup/dump_db_undercloud.sql.gz
ll  /home/stack/backup
```


---
## Identification du serveur et collecte des informations

``` bash
openstack server list -c Name -c Networks
# +----------------+-------------------------+
# | Name           | Networks                |
# +----------------+-------------------------+
# | rhos-ctrl-br-0 | ctlplane=10.129.173.50  |
# | rhos-ctrl-br-2 | ctlplane=10.129.173.52  |
# | rhos-ctrl-br-1 | ctlplane=10.129.173.51  |
# | rhos-comp-br-1 | ctlplane=10.129.173.101 |
# | rhos-comp-br-2 | ctlplane=10.129.173.102 |
# | rhos-comp-br-0 | ctlplane=10.129.173.100 |
# +----------------+-------------------------+
```
``` bash
export SERVER_NAME=rhos-ctrl-br-0
export SERVER_IP=10.129.173.50
export PCS_MANAGER=10.129.173.51
```

---
## Désactivation du contrôleur si il est toujours actif

Se connecter à l'instance et désactiver pacemaker
``` bash
# désactiver fencing
ssh heat-admin@$SERVER_IP  "sudo pcs property set stonith-enabled=false"

ssh heat-admin@$SERVER_IP "sudo pcs status | grep -w Online | grep -w $SERVER_NAME"
# * Online: [ rhos-ctrl-br-0 rhos-ctrl-br-1 rhos-ctrl-br-2 ]

ssh heat-admin@$SERVER_IP "sudo pcs cluster stop $SERVER_NAME"
# rhos-ctrl-br-0: Stopping Cluster (pacemaker)...
# rhos-ctrl-br-0: Stopping Cluster (corosync)...

ssh heat-admin@$SERVER_IP "sudo pcs status | grep -w Online | grep -w $SERVER_NAME"
# Error: error running crm_mon, is pacemaker running?
#   crm_mon: Error: cluster is not available on this node


```

Identifier l'instance Openstack, et le noeud baremetal concernés
``` bash
SERVER_NAME=rhos-ctrl-br-0

INSTANCE=$(openstack server list --name $SERVER_NAME -f value -c ID)
echo $INSTANCE

NODE=$(openstack baremetal node list -f csv --quote minimal | grep $INSTANCE | cut -f1 -d,)
echo $NODE
```

Placer le noeud en mode Maintenance
``` bash
openstack baremetal node maintenance set $NODE
```

Arrêter les services nova tournants sur le controller à désactiver
``` bash
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_api.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_api_cron.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_compute.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_conductor.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_metadata.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_placement.service"
ssh heat-admin@$SERVER_IP "sudo systemctl stop tripleo_nova_scheduler.service"
```
---
## Operation communes


### Désactivation du noeud dans la gestion par Pacemaker

Modifier la configuration de paceMaker, pour supprimer le serveur
``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs cluster node remove $SERVER_NAME --skip-offline --force"
```
Et vérifier l'état des ressources gérées par pacemaker. le serveur ne doit plus être dans la liste des noeuds actuifs, et les instances de ressources doivent être en état "Stopped":
``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs status"
# ...
# Node List:
# * Online: [ rhos-ctrl-br-1 rhos-ctrl-br-2 ]
# ... 
#  * Container bundle set: galera-bundle [cluster.common.tag/openstack-mariadb:pcmklatest]:
#     * galera-bundle-0	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-1
#     * galera-bundle-1	(ocf::heartbeat:galera):	 Stopped
#     * galera-bundle-2	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-2
# ...
```

Supprimer le noeud de la liste des noeuds connus par Pacemaker
``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs host deauth $SERVER_NAME"
```

Vérifier que les ressources fonctionnent toujours correctement
``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs status"
```

---
### Supprimer le noeud 

``` bash
openstack server list
openstack server delete $INSTANCE
openstack server list
```

``` bash
openstack baremetal node list
openstack baremetal node delete $NODE
openstack baremetal node list
```

``` bash
openstack port list | grep $SERVER_IP
# | 9b8ac389-6f5e-4ee4-83c3-ea7a8cc8ddf7 | Controller-port-0       | 00:62:0b:ac:48:b4 | ip_address='10.129.173.50', subnet_id='148687f4-cc3f-4368-9694-3b4452d8b121'  | ACTIVE |

openstack port delete   9b8ac389-6f5e-4ee4-83c3-ea7a8cc8ddf7 

```

``` bash
$ openstack baremetal node list -c Name -c "Instance UUID"
+--------------------------------------+----------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name           | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+----------------+--------------------------------------+-------------+--------------------+-------------+
| c38788ae-92cd-4ce2-af40-ae11129e0b68 | rhos-ctrl-br-2 | None                                 | power off   | available          | False       |
| c3e6fcf8-0c87-46be-b9cb-d696f47749a8 | rhos-comp-br-0 | d0f5ebbc-138b-45e6-877f-460bd1fe3ce9 | power on    | active             | False       |
| b3c69264-2349-487b-ab89-504c127a87dc | rhos-comp-br-1 | f6661fdb-b3e2-41af-8205-d6ac012f6b59 | power on    | active             | False       |
| b745194e-e9fa-47aa-b95d-541460c85f4e | rhos-comp-br-2 | 3234f09c-b4b9-4ab4-b979-a855ab8b3bcb | power on    | active             | False       |
| d0d354a4-7501-4bc7-a822-71e5b0faca80 | rhos-ctrl-br-0 | 4bd3ebcf-666f-4e50-9539-fe601109b819 | power off   | active             | False       |
| 4a2917bc-8921-42fd-8adc-26461ac9378f | rhos-comp-br-3 | None                                 | power off   | available          | False       |
| 74d5dee8-0d05-4dc4-b1b0-a81830359921 | rhos-ctrl-br-1 | 4789b9b2-f7b4-49ae-af79-f467374a601c | power on    | active             | False       |
+--------------------------------------+----------------+--------------------------------------+-------------+--------------------+-------------+
```

---
## Vérifier les services

### MariaDB
``` bash
for i in 10.129.173.51 10.129.173.52 ; do echo "*** $i ***" ; ssh heat-admin@$i "sudo podman exec \$(sudo podman ps --filter name=galera-bundle -q) mysql -e \"SHOW STATUS LIKE 'wsrep_local_state_comment'; SHOW STATUS LIKE 'wsrep_cluster_size';\""; done
# *** 10.129.173.51 ***
# Variable_name	Value
# wsrep_local_state_comment	Synced
# Variable_name	Value
# wsrep_cluster_size	1
```

``` bash
ssh heat-admin@$PCS_MANAGER  "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
# Cluster status of node rabbit@rhos-ctrl-br-1 ...
# Basics

# Cluster name: rabbit@rhos-ctrl-br-1.overcloud-prod.com

# Disk Nodes

# rabbit@rhos-ctrl-br-1

# Running Nodes

# rabbit@rhos-ctrl-br-1

# Versions

# rabbit@rhos-ctrl-br-1: RabbitMQ 3.8.16 on Erlang 23.3.4.18

# Maintenance status

# Node: rabbit@rhos-ctrl-br-1, status: not under maintenance

# Alarms

# (none)

# Network Partitions

# (none)

# Listeners

# Node: rabbit@rhos-ctrl-br-1, interface: [::], port: 15672, protocol: http, purpose: HTTP API
# Node: rabbit@rhos-ctrl-br-1, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
# Node: rabbit@rhos-ctrl-br-1, interface: 192.168.174.51, port: 5672, protocol: amqp, purpose: AMQP 0-9-1 and AMQP 1.0

# Feature flags

# Flag: drop_unroutable_metric, state: disabled
# Flag: empty_basic_get_metric, state: disabled
# Flag: implicit_default_bindings, state: disabled
# Flag: maintenance_mode_status, state: disabled
# Flag: quorum_queue, state: disabled
# Flag: user_limits, state: disabled
# Flag: virtual_host_metadata, state: disabled

```


``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs property show stonith-enabled"
# Cluster Properties:
#  stonith-enabled: false
```

---
### Désactivation du cluster de MariaDB

The overcloud database must continue to run during the replacement procedure. To ensure that Pacemaker does not stop Galera during this procedure, select a running Controller node and run the following command on the undercloud with the IP address of the Controller node:
``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs resource unmanage galera-bundle"
```

``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs status"  
# * Container bundle set: galera-bundle [cluster.common.tag/openstack-mariadb:pcmklatest] (unmanaged):
#   * galera-bundle-0	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-1 (unmanaged)
#   * galera-bundle-1	(ocf::heartbeat:galera):	 Stopped (unmanaged)
#   * galera-bundle-2	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-2 (unmanaged)
```




---
## Opératoins post

### Ré-activation du cluster de MariaDB

``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs status"  
#  ...
# * Container bundle set: galera-bundle [cluster.common.tag/openstack-mariadb:pcmklatest] (unmanaged):
#   * galera-bundle-0	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-1 (unmanaged)
#   * galera-bundle-1	(ocf::heartbeat:galera):	 Stopped (unmanaged)
#   * galera-bundle-2	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-2 (unmanaged)
```

``` bash
ssh heat-admin@$PCS_MANAGER "sudo pcs resource manage galera-bundle"
```
```


---
## Si problème de quorum avec pacemaker 

Afficher les noeuds actifs du cluster

``` bash
systemctl status pacemaker
# ...
# ... redis-bundle-1 )   due to no quorum (blocked)
# ...
```

``` bash
corosync-quorumtool -s
# Quorum information
# ------------------
# Date:             Mon Oct 30 16:06:16 2023
# Quorum provider:  corosync_votequorum
# Nodes:            1
# Node ID:          2
# Ring ID:          2.11
# Quorate:          No

# Votequorum information
# ----------------------
# Expected votes:   3
# Highest expected: 3
# Total votes:      1
# Quorum:           2 Activity blocked
# Flags:            

# Membership information
# ----------------------
#     Nodeid      Votes Name
#          2          1 rhos-ctrl-br-1 (local)
```


``` bash
pcs cluster corosync
totem {
    version: 2
    cluster_name: tripleo_cluster
    transport: knet
    token: 10000
    crypto_cipher: aes256
    crypto_hash: sha256
}

nodelist {
    node {
        ring0_addr: 192.168.174.50
        name: rhos-ctrl-br-0
        nodeid: 1
    }

    node {
        ring0_addr: 192.168.174.51
        name: rhos-ctrl-br-1
        nodeid: 2
    }

    node {
        ring0_addr: 192.168.174.52
        name: rhos-ctrl-br-2
        nodeid: 3
    }
}

quorum {
    provider: corosync_votequorum
}

logging {
    to_logfile: yes
    logfile: /var/log/cluster/corosync.log
    to_syslog: yes
    timestamp: on
}
```

Et forcer l'éléction d'un noeud
``` bash
pcs quorum update wait_for_all=0 --skip-offline

corosync-quorumtool -v 3 -n 2
```
---

---
## Autres galera

bootstrap controller node
``` bash
ssh heat-admin@$PCS_MANAGER "sudo hiera -c /etc/puppet/hiera.yaml pacemaker_short_bootstrap_node_name"
# rhos-ctrl-br-0  
```
``` bash
pcs node standby rhos-ctrl-br-01
pcs node standby rhos-ctrl-br-03

# Cluster Summary:
#   * Stack: corosync
#   * Current DC: rhos-ctrl-br-1 (version 2.0.5-9.el8_4.5-ba59be7122) - partition WITHOUT quorum
#   * Last updated: Mon Oct 30 15:49:51 2023
#   * Last change:  Mon Oct 30 15:48:32 2023 by root via cibadmin on rhos-ctrl-br-1
#   * 11 nodes configured
#   * 34 resource instances configured

# Node List:
#   * Node rhos-ctrl-br-0: OFFLINE (standby)
#   * Node rhos-ctrl-br-2: OFFLINE (standby)
#   * Online: [ rhos-ctrl-br-1 ]


```

OpenStack Galera will not start with a failed Controller node and galera-bundle resources in Slave
https://access.redhat.com/solutions/5981551


pcs stonith status
# NO stonith devices configured

``` bash
pcs resource config galera
 Resource: galera (class=ocf provider=heartbeat type=galera)
  Attributes: additional_parameters=--open-files-limit=16384 cluster_host_map=rhos-ctrl-br-0:rhos-ctrl-br-0.internalapi.overcloud-prod.com;rhos-ctrl-br-1:rhos-ctrl-br-1.internalapi.overcloud-prod.com enable_creation=true log=/var/log/mysql/mysqld.log wsrep_cluster_address=gcomm://rhos-ctrl-br-0.internalapi.overcloud-prod.com,rhos-ctrl-br-1.internalapi.overcloud-prod.com
  Meta Attrs: container-attribute-target=host master-max=2 ordered=true
  Operations: demote interval=0s timeout=120s (galera-demote-interval-0s)
              monitor interval=20s timeout=30s (galera-monitor-interval-20s)
              monitor interval=10s role=Master timeout=30s (galera-monitor-interval-10s)
              monitor interval=30s role=Slave timeout=30s (galera-monitor-interval-30s)
              promote interval=0s on-fail=block timeout=300s (galera-promote-interval-0s)
              start interval=0s timeout=120s (galera-start-interval-0s)
              stop interval=0s timeout=120s (galera-stop-interval-0s)
```


    wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com,rhos-ctrl-
        br-02.internalapi.overcloud-prod.com,rhos-ctrl-br-03.internalapi.overcloud-prod.com


pcs resource disable galera-bundle
pcs resource unmanage galera-bundle


pcs resource update galera wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com

---
## autres

``` bash
openstack hypervisor list
# +----+--------------------------------------+-----------------+---------------+-------+
# | ID | Hypervisor Hostname                  | Hypervisor Type | Host IP       | State |
# +----+--------------------------------------+-----------------+---------------+-------+
# |  4 | b3c69264-2349-487b-ab89-504c127a87dc | ironic          | 10.129.173.17 | up    |
# |  5 | c3e6fcf8-0c87-46be-b9cb-d696f47749a8 | ironic          | 10.129.173.17 | up    |
# |  6 | b745194e-e9fa-47aa-b95d-541460c85f4e | ironic          | 10.129.173.17 | up    |
# | 10 | 74d5dee8-0d05-4dc4-b1b0-a81830359921 | ironic          | 10.129.173.17 | up    |
# | 11 | 56a90aff-0b53-45e3-b2e0-ada86f2a5960 | ironic          | 10.129.173.17 | up    |
# | 12 | 7ea856a8-9aad-4e2c-a17a-8fad52d2edf2 | ironic          | 10.129.173.17 | up    |
# +----+--------------------------------------+-----------------+---------------+-------+
```

``` bash
openstack server list -c Name -c Networks
+----------------+-------------------------+
| Name           | Networks                |
+----------------+-------------------------+
| rhos-ctrl-br-1 | ctlplane=10.129.173.51  |
| rhos-comp-br-1 | ctlplane=10.129.173.101 |
| rhos-comp-br-2 | ctlplane=10.129.173.102 |
| rhos-comp-br-0 | ctlplane=10.129.173.100 |
+----------------+-------------------------+
```




CEPH-5
Adapter 3 - 10Gb 2-port Base-T BCM57416 OCP3 Adapter
1	00:62:0b:ac:96:0c	N/A	N/A	 OK	N/A 
2	00:62:0b:ac:96:0d	N/A	N/A	 Unknown	N/A

Adapter 1 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:ba:0a	N/A	N/A	 OK	N/A
2	30:3e:a7:02:ba:0b	N/A	N/A	 OK	N/A

Adapter 2 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:a6:ba	N/A	N/A	 Unknown	N/A
2	30:3e:a7:02:a6:bb	N/A	N/A	 Unknown	N/A


CEPH-4

Adapter 3 - 10Gb 2-port Base-T BCM57416 OCP3 Adapter
1	00:62:0b:ae:09:5e	N/A	N/A	 OK	N/A
2	00:62:0b:ae:09:5f	N/A	N/A	 Unknown	N/A

Adapter 1 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:a4:c6	N/A	N/A	 OK	N/A
2	30:3e:a7:02:a4:c7	N/A	N/A	 OK	N/A

Adapter 2 - Intel(R) Eth E810-XXVDA2
1	30:3e:a7:02:a6:50	N/A	N/A	 Unknown	N/A
2	30:3e:a7:02:a6:51	N/A	N/A	 Unknown	N/A


103
Activé	1	E4:43:4B:EA:67:70	NIC	68:4f:64:e9:7c:55	ethernet1/1/3
Activé	2	E4:43:4B:EA:67:71	NIC	68:4f:64:e9:7c:55	ethernet1/1/7
Activé	3	E4:43:4B:EA:67:72	NIC	68:4f:64:e9:7c:55	ethernet1/1/8
Arrêté	4	E4:43:4B:EA:67:73 	NIC	Aucun lien	Aucun lien

F8:F2:1E:AA:CE:B0
F8:F2:1E:AA:CE:B1

WARNING: Following parameter(s) are defined but not currently used in the deployment plan. These parameters may be valid but not in use due to the service or deployment configuration. CtlplaneNetworkAttributes, EC2MetadataIp, NeutronEnableForceMetadata, NeutronEnableIsolatedMetadata
Deploying templates in the directory /tmp/tripleoclient-8g3wi0cf/tripleo-heat-templates
