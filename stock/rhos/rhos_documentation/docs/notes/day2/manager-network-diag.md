# diag de la config réseau des manager

Lister les réseaux
``` bash
virsh net-list --all
#  Name            State    Autostart   Persistent
# --------------------------------------------------
#  brexternal      active   yes         yes
#  brinfra         active   yes         yes
#  brinternal      active   yes         yes
#  bripmi          active   yes         yes
#  brprov          active   yes         yes
#  brstorage       active   yes         yes
#  brstoragemgmt   active   yes         yes
#  brvxlan         active   yes         yes
```


Lister les VM
``` bash
virsh list --all
#  Id   Name                         State
# --------------------------------------------
#  1    rhos-director-01.imta.fr     running
#  2    rh-satellite-br-01.imta.fr   running
#  3    rh-relay-br-01.imta.fr       running
```

Pour chaque VM, lister les interfaces utilisées
```bash
virsh domiflist rhos-director-01.imta.fr
#  Interface   Type     Source       Model    MAC
# ---------------------------------------------------------------
#  vnet0       bridge   brinfra      virtio   52:54:00:32:18:94
#  vnet1       bridge   bripmi       virtio   52:54:00:db:85:2a
#  vnet2       bridge   brprov       virtio   52:54:00:57:70:99
#  vnet3       bridge   brprov       virtio   52:54:00:51:e3:60
#  vnet4       bridge   brexternal   virtio   52:54:00:66:78:a7
#  vnet5       bridge   brinternal   virtio   52:54:00:1e:1a:df
```

```bash
virsh domiflist  rh-satellite-br-01.imta.fr
#  Interface   Type     Source    Model    MAC
# ------------------------------------------------------------
#  vnet6       bridge   brinfra   virtio   52:54:00:67:ce:b1
```

```bash
virsh domiflist  rh-relay-br-01.imta.fr
#  Interface   Type     Source    Model    MAC
# ------------------------------------------------------------
#  vnet7       bridge   brinfra   virtio   52:54:00:ae:fc:f8
#  vnet8       bridge   brprov    virtio   52:54:00:19:84:ba
```