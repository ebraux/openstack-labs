
## "Decreased security: httpd serving unencrypted (HTTP) traffic"
mobiles :

## This system is vulnerable because: Key files have incorrect permissions

``` bash
[root@rhos-director-br-01 ~]# ll /etc/pki/tls/private/overcloud_endpoint.pem
-rw-r-----. 1 root root 4564 12 juin  22:07 /etc/pki/tls/private/overcloud_endpoint.pem
```

Correction
``` bash
chmod 400 '/etc/pki/tls/private/overcloud_endpoint.pem'
```


## This host has ansible-2.9.27-1.el8ae installed. Ansible Core is available for RHEL 8.7


avant:
``` bash
ansible --version
ansible 2.9.27
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3.6/site-packages/ansible
  executable location = /bin/ansible
  python version = 3.6.8 (default, Apr 28 2022, 06:08:06) [GCC 8.4.1 20200928 (Red Hat 8.4.1-1)]
```

Correction
```
```


## The host is running chronyd with 4.18.0-425.3.1.el8.x86_64. The RTC synchronization does not work as listed below:

Correction :

- Disable kvmclock by adding "no-kvmclock" to the kernel parameter:
``` bash
grubby --update-kernel=ALL --args="no-kvmclock"
```
- Propagate the configured kernel options to the new kernels:
``` bash 
grub2-mkconfig -o /etc/grub2.cfg
```
Reboot the system:
``` bash
reboot
```