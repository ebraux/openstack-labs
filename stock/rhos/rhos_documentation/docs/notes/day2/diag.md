


parallaxBackgroundSize:  mal d'infos : [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_replacing-controller-nodes)

Check the current status of the overcloud stack on the undercloud:
``` bash
$ source stackrc
(undercloud)$ openstack stack list --nested
```