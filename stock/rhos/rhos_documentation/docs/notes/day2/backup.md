

---
## Undercloud Database

-[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_replacing-controller-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_replacing-controller-nodes)

Install the database client tools:
``` bash
(undercloud)$ sudo dnf -y install mariadb
```

Configure root user access to the database:
``` bash
(undercloud)$ sudo cp /var/lib/config-data/puppet-generated/mysql/root/.my.cnf /root/.
```

Perform a backup of the undercloud databases:
``` bash
(undercloud)$ mkdir /home/stack/backup
(undercloud)$ sudo mysqldump --all-databases --quick --single-transaction | gzip > /home/stack/backup/dump_db_undercloud.sql.gz
```

---
## scripts Ansible

utilisation de Relax-and-Recover (ReaR). 
 Ansible Playbooks needed to run the backup-and-restore role `/usr/share/ansible/roles/backup-and-restore`

 Les variables paramétrables sont définies dans : `/usr/share/ansible/roles/backup-and-restore/defaults/main.yml`


---
## lab
configure the utility server as a backup node.
``` bash
(undercloud) [stack@director ~]$ ansible-playbook -v -i ~/nfs-inventory.ini \
> --extra="ansible_ssh_common_args='-o StrictHostKeyChecking=no'" \
> --become --become-user root --tags bar_setup_nfs_server \
> ~/bar_nfs_setup.yaml
```


~/nfs-inventory.ini
``` bash
[BACKUP_NODE]
backup ansible_host=10.129.172.19 ansible_user=cloud-user #ansible_become_password=student
```