

rem : les member doivent être accessible par le LB. Il faut donc créer un groupe de sécurité permettant cet accès


1. Source your credentials file.
``` bash
$ source ~/overcloudrc
```

2. Create a load balancer (lb1) on a public subnet (externat_subnet).
``` bash
openstack loadbalancer create --name lb1 --vip-subnet-id external-subnet
```

3. Verify the state of the load balancer.
``` bash
openstack loadbalancer show lb1
```

4. Before going to the next step, ensure that the provisioning_status is ACTIVE.
   
5. Create a listener (listener1) on a port (80).
``` bash
openstack loadbalancer listener create \
  --name listener1 \
  --protocol HTTP \
  --protocol-port 80\
  lb1
```

6. Verify the state of the listener.
``` bash
openstack loadbalancer listener show listener1
```

7. Create the listener default pool (pool1).
``` bash
openstack loadbalancer pool create \
  --name pool1 \
  --lb-algorithm ROUND_ROBIN \
  --listener listener1 \
  --protocol HTTP
```

8. Create a health monitor on the pool (pool1) that connects to the back-end servers and tests the path (/).
``` bash
openstack loadbalancer healthmonitor create \
  --delay 15 \
  --max-retries 4 \
  --timeout 10 \
  --type HTTP \
  --url-path / \
  pool1
```

9. Add load balancer members (172.16.1.35 and 172.16.1.185) on the private subnet
(subnet-admin ) to the default pool.
``` bash
openstack loadbalancer member create \
  --subnet-id subnet-admin  \
  --address 172.16.1.35 \
  --protocol-port 80 \
  pool1

openstack loadbalancer member create \
  --subnet-id subnet-admin  \
  --address 172.16.1.185 \
  --protocol-port 80 \
  pool1
```


``` bash
openstack loadbalancer member list pool1 -c address -c protocol_port -c provisioning_status  -c operating_status
+---------------------+--------------+---------------+------------------+
| provisioning_status | address      | protocol_port | operating_status |
+---------------------+--------------+---------------+------------------+
| ACTIVE              | 172.16.1.35  |            80 | ERROR            |
| ACTIVE              | 172.16.1.185 |            80 | ERROR            |
+---------------------+--------------+---------------+------------------+
```

---
## tets avec une VIP
Create a load balancer (lb1) on a private subnet (private_subnet).

openstack loadbalancer create --name lb2 --vip-subnet-id subnet-admin
openstack loadbalancer show lb2

openstack loadbalancer listener create --name listener2 --protocol HTTP --protocol-port 80 lb2
openstack loadbalancer listener list --loadbalancer lb2
openstack loadbalancer listener show listener2

openstack loadbalancer pool create --name pool2 --lb-algorithm ROUND_ROBIN --listener listener2 --protocol HTTP
openstack loadbalancer pool show pool2
openstack loadbalancer listener list --loadbalancer lb2 -c name -c default_pool_id


openstack loadbalancer healthmonitor create --delay 15 --max-retries 4 --timeout 10 --type HTTP --url-path / pool2
openstack loadbalancer healthmonitor show a8f82c8c-084d-4806-a094-76072f11e7a1


openstack loadbalancer member create \
  --subnet-id subnet-admin  \
  --address 172.16.1.35 \
  --protocol-port 80 \
  pool2

openstack loadbalancer member create \
  --subnet-id subnet-admin  \
  --address 172.16.1.185 \
  --protocol-port 80 \
  pool2


openstack loadbalancer member list  pool2


openstack floating ip create external
10.129.178.164

openstack floating ip list -c 'Floating IP Address' -c 'Fixed IP Address'
openstack loadbalancer show lb2 
openstack loadbalancer show lb2 -c vip_port_id

openstack floating ip set --port e6b7a6d5-bef6-44cb-b102-45bdf503e5b8 10.129.178.164

---
## source

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/pdf/using_octavia_for_load_balancing-as-a-service/red_hat_openstack_platform-16.2-using_octavia_for_load_balancing-as-a-service-en-us.pdf](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/pdf/using_octavia_for_load_balancing-as-a-service/red_hat_openstack_platform-16.2-using_octavia_for_load_balancing-as-a-service-en-us.pdf)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/using_octavia_for_load_balancing-as-a-service/troubleshoot-maintain-lb-service_rhosp-lbaas](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/using_octavia_for_load_balancing-as-a-service/troubleshoot-maintain-lb-service_rhosp-lbaas)

---
## Pip

Install Octavia. dernière version 3.0.5

``` bash
Collecting python-octaviaclient
  Downloading python_octaviaclient-3.5.0-py3-none-any.whl (113 kB)
     |████████████████████████████████| 113 kB 1.8 MB/s 
Requirement already satisfied: oslo.serialization!=2.19.1,>=2.18.0 in /home/cloud-user/staging/os-operation/.direnv/python-3.9/lib/python3.9/site-packages (from python-octaviaclient) (5.2.0)

Collecting python-neutronclient>=6.7.0
  Downloading python_neutronclient-11.0.0-py3-none-any.whl (293 kB)
     |████████████████████████████████| 293 kB 5.5 MB/s 

Collecting openstacksdk>=0.15.0
  Downloading openstacksdk-1.5.0-py3-none-any.whl (1.7 MB)
     |████████████████████████████████| 1.7 MB 36.9 MB/s 

Collecting oslo.log>=3.36.0
  Downloading oslo.log-5.3.0-py3-none-any.whl (76 kB)

     |████████████████████████████████| 76 kB 9.6 MB/s 
Collecting pyinotify>=0.9.6
  Downloading pyinotify-0.9.6.tar.gz (60 kB)
     |████████████████████████████████| 60 kB 13.6 MB/s 

Collecting oslo.context>=2.21.0
  Downloading oslo.context-5.2.0-py3-none-any.whl (20 kB)

Collecting python-dateutil>=2.7.0
  Downloading python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
     |████████████████████████████████| 247 kB 29.7 MB/s 

```


[python-octaviaclient] listener create sends some unneeded unset attributes
https://bugs.launchpad.net/octavia/+bug/2037253

Installing collected packages: python-dateutil, pyinotify, oslo.context, openstacksdk, oslo.log, os-client-config, python-neutronclient, python-octaviaclient
    Running setup.py install for pyinotify ... done
  Attempting uninstall: openstacksdk
    Found existing installation: openstacksdk 0.62.0
    Uninstalling openstacksdk-0.62.0:
      Successfully uninstalled openstacksdk-0.62.0


Instal octavia < 3.5.0  (3.4.0)

``` bash

Collecting python-octaviaclient<3.5.0
  Downloading python_octaviaclient-3.4.0-py3-none-any.whl (112 kB)
     |████████████████████████████████| 112 kB 1.8 MB/s 

Collecting python-neutronclient>=6.7.0
  Downloading python_neutronclient-10.0.0-py3-none-any.whl (293 kB)
     |████████████████████████████████| 293 kB 5.5 MB/s 
  Downloading python_neutronclient-9.0.0-py3-none-any.whl (429 kB)
     |████████████████████████████████| 429 kB 44.2 MB/s 


Installing collected packages: openstacksdk, python-neutronclient, python-octaviaclient
  Attempting uninstall: openstacksdk
    Found existing installation: openstacksdk 1.5.0
    Uninstalling openstacksdk-1.5.0:
      Successfully uninstalled openstacksdk-1.5.0
  Attempting uninstall: python-neutronclient
    Found existing installation: python-neutronclient 11.0.0
    Uninstalling python-neutronclient-11.0.0:
      Successfully uninstalled python-neutronclient-11.0.0
  Attempting uninstall: python-octaviaclient
    Found existing installation: python-octaviaclient 3.5.0
    Uninstalling python-octaviaclient-3.5.0:
      Successfully uninstalled python-octaviaclient-3.5.0

Successfully installed openstacksdk-0.62.0 python-neutronclient-9.0.0 python-octaviaclient-3.4.0
```


python3-openstacksdk.noarch       0.36.5-2.20220111021051.feda828.el8ost                      @openstack-16.2-for-rhel-8-x86_64-rpms 

ython3-openstackclient.noarch                         4.0.2-2.20221012154730.54bf2c0.el8ost                       @openstack-16.2-for-rhel-8-x86_64-rpms  
---
## enforce security


source : 

- How to enable communication between Octavia amphoras and pool members? [https://access.redhat.com/solutions/5017151](https://access.redhat.com/solutions/5017151)
  
Issue

- I want to allow inbound traffic for listener endpoints on pool members from amphoras only
- How to strictly limit inbound traffic on Octavia pool members?
- How can I use load balancer ID to define security groups' rules?

Resolution

Currently Octavia automatically creates security groups and attach them to amphoras' instances to allow inbound connections to listener ports. Created security groups are used for the same ports that are used to connect to pool members. As a result, it is possible possible to add security group's rule to pool member instances to allow inbound connections from all amphoras that belong to specific load balancer in a following manner (LB-ID is load balancer ID, SG-ID is a security group's ID which is used on pool member instances:

``` bash
$ openstack security group rule create --ingress --protocol tcp --dst-port PORT-NUM --remote-group "lb-$(openstack loadbalancer show LB-ID -c id -f value)" SG-ID
```

Please use openstack help security group rule create command to learn more about possible arguments to define security group's rule.

