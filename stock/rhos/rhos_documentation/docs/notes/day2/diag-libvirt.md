

## Vérifier que la machine est bien compatible avec les VMs

Vérifier que les outils libvirt/Qemu sont dispnivbles

Définition des `os-type` supportés
``` bash

osinfo-query os
```

https://www.golinuxcloud.com/virt-install-examples-kvm-virt-commands-linux/

Vérifier les réseaux disponibles :
``` bash
virsh net-list
```