


https://docs.mirantis.com/container-cloud/latest/operations-guide/single/index.html#connect-cluster
https://docs.mirantis.com/container-cloud/latest/operations-guide/single/index.html#document-manage-mgmt

https://kind.sigs.k8s.io/#community


Dell et Ironic :
- https://www.delltechnologies.com/asset/en-us/products/servers/technical-support/managing-dell-emc-hardware-with-openstack-ironic-idrac-driver.pdf
- https://www.delltechnologies.com/asset/en-us/products/servers/technical-support/managing-dell-emc-hardware-with-openstack-ironic-idrac-driver-2-0.pdf

## Prérequis

PXE network : 

Effectivement avec les Idrac 9, il faut activer ipmi sur le réseau. Ce qui était fait par défaut sur les Idrac 6.
  https://www.dell.com/support/manuals/fr-ch/dell-xc6420/idrac9_4.00.00.00_ug_new/configuring-ipmi-over-lan-using-web-interface?guid=guid-9d26d775-2bb1-451a-ab57-8509f3b8a66c&lang=en-us

To configure IPMI over LAN:

1. In the iDRAC Web interface, go to iDRAC Settings > Connectivity: The Network page is displayed.
2. Under IPMI Settings, specify the values for the attributes and click Apply : The IPMI over LAN settings are configured.

## Préparation Machine "seed node"


### Description Machine

- Machine en Ubuntu 18.04
- installation basique, via pxe+kickstart
- domaine "imta.fr"


### Configuration du réseau

Par défaut install IMT-Atlantique : netplan configuré, avec IP via DHCP : 
- Interface: eno1
- IP: 10.129.172.14/24
- gateway: 10.129.172.1
- broadcast : 10.129.172.255

Configuration à réaliser :

- configurer "eno1", avec une IP dan le réseau DISI : 10.129.41.131/24
- configurer une interface "br0" dans le érseau PXE (le nom "br0" doit êtr respécté, ou doit êtr modifié ensuite ensuite dans les fichiers de configuration)



Config Netplan pour interface dans réseau DISI : /etc/netplan/01-netcfg.yaml
```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    eno1:
        dhcp4: false
        dhcp6: false
        addresses:
          - 10.129.41.131/24
        gateway4: 10.129.41.1
        nameservers:
          addresses:
            - 192.44.75.10
            - 192.108.115.2
```

Config Netplan pour Bride br0 dans le érseau PXE : /etc/netplan/02-netcfg.yaml

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    eno2:
        dhcp4: false
        dhcp6: false

  bridges:
      br0:
          addresses:
            - 10.129.172.14/24
          dhcp4: false
          dhcp6: false
          gateway4: 10.129.172.1
          interfaces:
            - eno2
          nameservers:
              addresses:
              # Please, adjust for your environment
              - 192.44.75.10
              - 192.108.115.2
          parameters:
              forward-delay: 4
              stp: false
```

Infos : 

- https://netplan.io/troubleshooting/
- https://netplan.io/examples/


### Installation des prérequis

```bash
sudo apt install bridge-utils
sudo apt install ipmitool tree 
sudo apt install docker.io
sudo usermod -aG docker $USER
```

### Configuration du proxy

Config des accès proxy pour le système
```bash
sudo tee -a /etc/environment << 'EOF'
export http_proxy=http://proxy.priv.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
EOF
source /etc/environment

# Test
curl -m 10 https://binary.mirantis.com"
```


Config des accès proxy  pour Docker Daemon
```bash
# creation du dossier de configuration du service docker
sudo mkdir -p /etc/systemd/system/docker.service.d

# creation du fichier de configuration du proxy
sudo tee /etc/systemd/system/docker.service.d/proxy.conf << 'EOF'
[Service]
Environment='HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080' 'HTTP_PROXY=http://proxy.enst-bretagne.fr:8080' 'NO_PROXY=27.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr'
EOF

cat /etc/systemd/system/docker.service.d/proxy.conf

# relancer Docker
sudo systemctl daemon-reload
sudo systemctl restart docker


# Test : 
docker pull alpine
```


Config des accès proxy  pour Docker Client
```bash
# creation du dossier de configuration du service docker
mkdir ~/.docker

# creation du fichier de configuration du client Docker
tee ~/.docker/config.json << 'EOF'
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://proxy.enst-bretagne.fr:8080",
     "httpsProxy": "http://proxy.enst-bretagne.fr:8080",
     "noProxy": "27.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"
   }
 }
}
EOF

cat ~/.docker/config.json

# test
docker run --rm alpine sh -c "apk add --no-cache curl; \
curl -m 10 https://binary.mirantis.com"
```


## Installation de MCC

https://docs.mirantis.com/container-cloud/latest/deployment-guide/deploy-bm-mgmt/bm-mgmt-deploy.html

Répartition des IP du PXE Network :

- 10.129.172.1 : PXE_NW_GW
- 10.129.172.13 : [OLD] RHOS-director
- 10.129.172.14 : seed Node
- 10.129.172.20: KAAS_BM_PXE_IP
- 10.129.172.24 : [OLD] RHOS-director
- 10.129.172.29 : [OLD] RHOS-director
- 10.129.172.30 : [OLD] RHOS-director
- 10.129.172.40,10.129.172.59: KAAS_BM_BM_DHCP_RANGE
- 10.129.172.61-10.129.172.80 : METALLB_ADDR_POOL
- 10.129.172.61 : KEYCLOAK_FLOATING_IP
- 10.129.172.62 : IAM_FLOATING_IP
- 10.129.172.63 : PROXY_FLOATING_IP
- 10.129.172.90 : LB_HOST
- 10.129.172.100-10.129.172.150 : IPAM_POOL_RANGE


###  Test accès IPMI 
```bash
ipmitool -I lanplus -H '10.29.20.11' -U 'root' -P 'mcc2021' chassis power status
ipmitool -I lanplus -H '10.29.20.12' -U 'root' -P 'mcc2021' chassis power status
ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' chassis power status
---
Chassis Power is on
```

ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' bmc info
ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' lan print
ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' shell

https://community.pivotal.io/s/article/How-to-work-on-IPMI-and-IPMITOOL?language=en_US


ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' raw 0x30 0x19
ipmitool -U <</span>redacted> -P <</span>redacted> -H 10.4.0.10 

-I intf       Interface to use
Interfaces:
    open         Linux OpenIPMI Interface [default]
    imb          Intel IMB Interface
    lan          IPMI v1.5 LAN Interface
    lanplus      IPMI v2.0 RMCP+ LAN Interface

### Préparation des templates

mkdir MCC2
cd MCC2
wget https://binary.mirantis.com/releases/get_container_cloud.sh
chmod 0755 get_container_cloud.sh
./get_container_cloud.sh
cd kaas-bootstrap/

cp -p ../mirantis.lic  .
 
mkdir templates.backup
cp -r templates/*  templates.backup/



Proxy
```bash
export no_proxy=localhost,.svc
printf -v allip '%s,' .{0..255}
export no_proxy="$no_proxy,${allip%,}"

export no_proxy=${no_proxy},$(echo 192.168.100.{1..255} | sed 's/ /,/g')

```

Encodage du login/mdp pour les accès IPMI
```bash
echo -n root| base64
---
cm9vdA==
```

```bash
echo -n OStack2021| base64
---
T1N0YWNrMjAyMQ==
```


Infos pour config des template
```bash
# --- cluster.yaml
SET_LB_HOST=10.129.172.90
SET_METALLB_ADDR_POOL=10.129.172.61-10.129.172.80
# --- ipam-objects.yaml
SET_IPAM_CIDR=10.129.172.0/24
SET_PXE_NW_GW=10.129.172.1
SET_PXE_NW_DNS=192.44.75.10
SET_IPAM_POOL_RANGE=10.129.172.100-10.129.172.150
SET_LB_HOST=10.129.172.90
SET_METALLB_ADDR_POOL=10.129.172.61-10.129.172.80
# --- bootstrap.sh
KAAS_BM_PXE_IP=10.129.172.20
KAAS_BM_PXE_MASK=24
KAAS_BM_PXE_BRIDGE=br0
KAAS_BM_BM_DHCP_RANGE=10.129.172.40,10.129.172.59
KEYCLOAK_FLOATING_IP=10.129.172.61
IAM_FLOATING_IP=10.129.172.62
PROXY_FLOATING_IP=10.129.172.63
# --- IPMI config- baremetalhosts.yaml
SET_MACHINE_0_IPMI_USERNAME=cm9vdA==
SET_MACHINE_0_IPMI_PASSWORD=bWNjMjAyMQ==
SET_MACHINE_0_MAC=b8:2a:72:d2:e5:9c
SET_MACHINE_0_BMC_ADDRESS=10.29.20.11
SET_MACHINE_1_IPMI_USERNAME=cm9vdA==
SET_MACHINE_1_IPMI_PASSWORD=bWNjMjAyMQ==
SET_MACHINE_1_MAC=d4:be:d9:af:c2:cd
SET_MACHINE_1_BMC_ADDRESS=10.29.20.12
SET_MACHINE_2_IPMI_USERNAME=cm9vdA==
SET_MACHINE_2_IPMI_PASSWORD=bWNjMjAyMQ==
SET_MACHINE_2_MAC=d4:be:d9:af:c3:9c
SET_MACHINE_2_BMC_ADDRESS=10.29.20.13
```

Infos pour config pour CEPH : templates/bm/kaascephcluster.yaml.template 
- https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-ceph/ceph-advanced-config.html
- https://docs.mirantis.com/mos/latest/deployment-guide/deploy-ceph.html

- c'est un réseau Interne ?
```yaml
     clusterNet: 10.10.10.0/24
      publicNet: 10.10.11.0/24
```
- ou un réseau Externe ?
```yaml
  clusterNet: 192.168.177.0/24
  publicNet: 192.168.176.0/24
```
- ou la valeur par défaut  ?
```yaml
     clusterNet: 0.0.0.0/0
      publicNet: 0.0.0.0/0
```
- ou le réseau unique?
```yaml
    clusterNet: 10.129.172.0/24
    publicNet: 10.129.172.0/24
```


```bash
cd $HOME
mkdir MCC; cd $_
wget https://binary.mirantis.com/releases/get_container_cloud.sh
chmod 0755 get_container_cloud.sh
./get_container_cloud.sh
```

Récupérer le fichier de licence, et le mettre en place
```bash
cd $HOME/MCC
mv ressources/mirantis.lic kaas-bootstrap/
```

Mettre en place les fichiers de config
```bash
cd $HOME/MCC
mv kaas-bootstrap/templates kaas-bootstrap/templates.DIST
mv ressources/templates kaas-bootstrap/templates
```

> Faire la config pour les fichier de template du dossier bm (Bare Metal)


### validation et lancement


Configuration de l'environnement
```bash
export KAAS_BM_ENABLED="true"
#
export KAAS_BM_PXE_IP="10.129.172.20"
export KAAS_BM_PXE_MASK="24"
export KAAS_BM_PXE_BRIDGE="br0"
#
export KAAS_BM_BM_DHCP_RANGE="10.129.172.40,10.129.172.59"
#
export KEYCLOAK_FLOATING_IP="10.129.172.61"
export IAM_FLOATING_IP="10.129.172.62"
export PROXY_FLOATING_IP="10.129.172.63"

unset KAAS_BM_FULL_PREFLIGHT
```

Validation
```bash
cd $HOME/MCC/kaas-bootstrap
./bootstrap.sh preflight
```

- beaucoup de ligne en warning `Wxxxx ... a ignorer`
- vérifier à la fin `I0602 17:36:17.730455    7728 bootstrap_preflight.go:111] preflight check successfully finished`
```


Validation approfondie
```bash
export KAAS_BM_FULL_PREFLIGHT="true"
./bootstrap.sh preflight
```

La validation approfondie crée un container et un volume, qu'il faudra supprimer si la procédure a été interrompue

```bash
docker ps -a
---
CONTAINER ID   IMAGE                                                                 COMMAND                  CREATED             STATUS                        PORTS     NAMES
bf4a2a2a41dc   mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8   "/usr/local/bin/entr…"   About an hour ago   Exited (137) 25 minutes ago             clusterapi-control-plane
```

```bash
docker volume ls
DRIVER    VOLUME NAME
local     0b9a3942b2f8818ba7354aa31ba2a1628e6061765a10e5ff6d5495047a164bce
```

```bash
docker stop  clusterapi-control-plane
docker rm clusterapi-control-plane
docker volume prune -f
#docker volume rm 0b9a3942b2f8818ba7354aa31ba2a1628e6061765a10e5ff6d5495047a164bce
```

Arret des noeuds
```bash
ipmitool -I lanplus -H '10.29.20.11' -U 'root' -P 'mcc2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.12' -U 'root' -P 'mcc2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' chassis power off;

ipmitool -I lanplus -H '10.29.20.11' -U 'root' -P 'mcc2021' chassis power status
ipmitool -I lanplus -H '10.29.20.12' -U 'root' -P 'mcc2021' chassis power status
ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'mcc2021' chassis power status

ipmitool -I lanplus -H '10.29.20.24' -U 'root' -P 'OStack2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.25' -U 'root' -P 'OStack2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.26' -U 'root' -P 'OStack2021' chassis power off;


ipmitool -I lanplus -H '10.29.20.13' -U 'root' -P 'OStack2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.14' -U 'root' -P 'OStack2021' chassis power off; \
ipmitool -I lanplus -H '10.29.20.15' -U 'root' -P 'OStack2021' chassis power off;


```

---



### Lancement

```bash
./bootstrap.sh all
```

```bash
kind [create cluster --kubeconfig=/root/.kube/kind-config-clusterapi --image=mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8 --name=clusterapi]
```

---
## Infos complémentaire

### Troubleshooting

In case of deployment problems, the first step is to dump all logs:

  ```bash
  ./bootstrap.sh collect_logs
  ```

It will collect bootstrap and management cluster logs/events.

<aside class="warning">
Use this command before `Cleanup` operations
</aside>


Cleanup :  After you're done with the bootstrapped cluster, first delete all clusters created using it, then run:

  ```bash
  ./bootstrap.sh cleanup
  ```

It will delete all resources occupied by the KaaS and its own cluster.


### test connectivité dans le système 


```bash
ip a

eth0@if12: 172.18.0.2/16
veth0-docker@if14: 10.129.172.20/24
vethxxx: 10.244.0.1/32 ...
```

```bash
default via 172.18.0.1 dev eth0 
10.129.172.0/24 dev veth0-docker proto kernel scope link src 10.129.172.20 
10.244.0.2 dev veth9e498e01 scope host 
10.244.0.3 dev veth02eb492f scope host 
10.244.0.4 dev veth37bf30b6 scope host 
10.244.0.5 dev vetha998210a scope host 
10.244.0.6 dev veth7f70303b scope host 
10.244.0.10 dev veth0ea3d7f9 scope host 
10.244.0.11 dev vethb48dd220 scope host 
10.244.0.13 dev veth3fbc3289 scope host 
10.244.0.18 dev vetha91ed2b6 scope host 
10.244.0.20 dev veth49d79267 scope host 
172.18.0.0/16 dev eth0 proto kernel scope link src 172.18.0.2 
```

```bash
declare -x http_proxy="http://proxy.enst-bretagne.fr:8080"
declare -x https_proxy="http://proxy.enst-bretagne.fr:8080"
declare -x no_proxy="172.18.0.0/16,fc00:f853:ccd:e793::/64,127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr,172.18.10.0,registry.internal.lan,10.96.0.0/12,10.244.0.0/16,clusterapi-control-plane,.svc,.svc.cluster,.svc.cluster.local"
```

```bash
apt update
apt upgrade
```

```bash
cat /etc/lsb-release 
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.10
DISTRIB_CODENAME=groovy
DISTRIB_DESCRIPTION="Ubuntu 20.10"
```

root@clusterapi-control-plane:/var/log/containers# vim ironic-operator-5b9757f86d-sjk2g_kaas_controller-d84684a7b92e440ed3c32b475d3713c9b138dcbde9c72a972d85828918e8e2ae.log
```bash
021-06-03T12:00:36.93437821Z stderr F time="2021-06-03T12:00:36Z" level=error msg="Failed to get configmap baremetal-provider-config: configmaps \"baremetal-provider-config\" not found"
2021-06-03T12:00:37.133983587Z stderr F time="2021-06-03T12:00:37Z" level=error msg="Failed to get configmap baremetal-provider-config: configmaps \"baremetal-provider-config\" not found"
2021-06-03T12:00:37.334501896Z stderr F time="2021-06-03T12:00:37Z" level=error msg="Failed to get configmap baremetal-provider-config: configmaps \"baremetal-provider-config\" not found"
```

ironic-8d7bb4574-p7fzg_kaas_wait-for-mariadb-7b5e418c4b2c1d9274f5b9bf6274d432f27911a4a0ca22e237e2185df02423d2.log
```bash
2021-06-02T18:15:03.915748601Z stderr F ^Gmysqladmin: connect to server at 'mariadb-sql' failed
2021-06-02T18:15:03.915777316Z stderr F error: 'Access denied for user 'root'@'10.244.0.20' (using password: NO)'
```

### config de proxy ...

```bash
http://ironic-kaas-bm:6385/v1/","error":"Get \"http://ironic-kaas-bm:6385/v1\": dial tcp 10.107.174.67:6385
---
root@clusterapi-control-plane:/var/log/containers# curl http://ironic-kaas-bm:6385/v1/
curl: (5) Could not resolve proxy: proxy.enst-bretagne.fr
---
root@clusterapi-control-plane:/var/log/containers# curl http://10.107.174.67
curl: (5) Could not resolve proxy: proxy.enst-bretagne.fr
---
echo $no_proxy
172.18.0.0/16,fc00:f853:ccd:e793::/64,127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr,172.18.10.0,registry.internal.lan,10.96.0.0/12,10.244.0.0/16,clusterapi-control-plane,.svc,.svc.cluster,.svc.cluster.local
```

### boot pxe

root@clusterapi-control-plane:/var/log/pods/kaas_dnsmasq-7d5d694f9b-ls29h_936e6a76-b69a-47be-bb30-61c5c8f3efca

dhcpd/0.log | grep os-ctrl


tftp/0.log 
```bash
2021-06-03T14:39:59.974556983Z stdout F Jun  3 14:39:59 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.43
2021-06-03T14:42:52.454982456Z stdout F Jun  3 14:42:52 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.44
2021-06-03T14:42:52.455003838Z stdout F Jun  3 14:42:52 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.44
2021-06-03T14:42:52.992625097Z stdout F Jun  3 14:42:52 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.44
2021-06-03T14:44:28.706186135Z stdout F Jun  3 14:44:28 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.45
2021-06-03T14:44:28.706204911Z stdout F Jun  3 14:44:28 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.45
2021-06-03T14:44:29.216947597Z stdout F Jun  3 14:44:29 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.45
2021-06-03T14:52:11.647465776Z stdout F Jun  3 14:52:11 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.33
2021-06-03T14:52:11.647494686Z stdout F Jun  3 14:52:11 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.33
2021-06-03T14:52:12.220057726Z stdout F Jun  3 14:52:12 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.33
2021-06-03T14:53:42.282763623Z stdout F Jun  3 14:53:42 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.34
2021-06-03T14:53:42.282788313Z stdout F Jun  3 14:53:42 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.34
2021-06-03T14:53:42.70646362Z stdout F Jun  3 14:53:42 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.34
2021-06-03T14:56:27.371674193Z stdout F Jun  3 14:56:27 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.35
2021-06-03T14:56:27.37169561Z stdout F Jun  3 14:56:27 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.35
2021-06-03T14:56:27.811051875Z stdout F Jun  3 14:56:27 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.35
2021-06-03T15:01:32.837152303Z stdout F Jun  3 15:01:32 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.36
2021-06-03T15:01:32.837180328Z stdout F Jun  3 15:01:32 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.36
2021-06-03T15:01:33.408761836Z stdout F Jun  3 15:01:33 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.36
2021-06-03T15:06:25.440537274Z stdout F Jun  3 15:06:25 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.37
2021-06-03T15:06:25.440567825Z stdout F Jun  3 15:06:25 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.37
2021-06-03T15:06:25.913382821Z stdout F Jun  3 15:06:25 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.37
2021-06-03T15:08:26.294752609Z stdout F Jun  3 15:08:26 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.38
2021-06-03T15:08:26.294780916Z stdout F Jun  3 15:08:26 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.38
2021-06-03T15:08:26.771529335Z stdout F Jun  3 15:08:26 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.38
2021-06-03T15:10:51.518015253Z stdout F Jun  3 15:10:51 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.39
2021-06-03T15:10:51.518037288Z stdout F Jun  3 15:10:51 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.39
2021-06-03T15:10:52.071412948Z stdout F Jun  3 15:10:52 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.39
2021-06-03T15:19:57.400192701Z stdout F Jun  3 15:19:57 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.33
2021-06-03T15:19:57.40020823Z stdout F Jun  3 15:19:57 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.33
2021-06-03T15:19:57.851243256Z stdout F Jun  3 15:19:57 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.33
2021-06-03T15:20:16.654696426Z stdout F Jun  3 15:20:16 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.34
2021-06-03T15:20:16.654713888Z stdout F Jun  3 15:20:16 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.34
2021-06-03T15:20:17.195263802Z stdout F Jun  3 15:20:17 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.34
2021-06-03T15:20:41.376024378Z stdout F Jun  3 15:20:41 dnsmasq-tftp[1]: error 8 User aborted the transfer received from 10.129.172.35
2021-06-03T15:20:41.376067458Z stdout F Jun  3 15:20:41 dnsmasq-tftp[1]: failed sending /volume/tftpboot/ipxe.efi to 10.129.172.35
2021-06-03T15:20:41.882222527Z stdout F Jun  3 15:20:41 dnsmasq-tftp[1]: sent /volume/tftpboot/ipxe.efi to 10.129.172.35
```


### bootstrap MCC
```bash
I0602 17:59:56.063628   15261 bootstrap_preflight.go:102] preflight check started...
I0602 17:59:56.081560   15261 clusterdeployer.go:104] Creating bootstrap cluster
I0602 17:59:56.081576   15261 bootstrap.go:9] Preparing bootstrap cluster
I0602 17:59:56.081590   15261 existing.go:22] Using existing Kind cluster
I0602 17:59:56.081596   15261 clusterdeployer.go:110] Fetching bootstrap kubeconfig
I0602 17:59:56.528416   15261 clusterdeployer.go:120] Connecting to bootstrap cluster
I0602 17:59:56.558356   15261 clusterdeployer.go:156] Initialize Tiller in bootstrap cluster.
I0602 18:00:36.579778   15261 baremetal.go:458] Added baremetal-operator chart to install
I0602 18:00:36.579797   15261 baremetal.go:458] Added baremetal-public-api chart to install
I0602 18:00:36.580046   15261 clusterdeployer.go:270] Deploy baremetal-operator in kaas namespace
```

```bash
I0602 18:00:36.580076   15261 cluster.go:186] Trying to find chart kaas-bm/baremetal-operator in repos
I0602 18:00:36.580139   15261 cluster.go:170] Found chart "kaas-bm/baremetal-operator" version "4.1.3" in "kaas-bm" repo
I0602 18:00:36.580150   15261 cluster.go:202] Downloading the chart... chart URL "https://binary.mirantis.com/bm/helm/baremetal-operator-4.1.3.tgz"
I0602 18:00:37.920322   15261 cluster.go:271] Release status: Code: DEPLOYED
I0602 18:00:37.920478   15261 clusterdeployer.go:270] Deploy baremetal-public-api in kaas namespace
I0602 18:00:37.920503   15261 cluster.go:186] Trying to find chart kaas-bm/baremetal-public-api in repos
I0602 18:00:37.920586   15261 cluster.go:170] Found chart "kaas-bm/baremetal-public-api" version "4.1.3" in "kaas-bm" repo
I0602 18:00:37.920600   15261 cluster.go:202] Downloading the chart... chart URL "https://binary.mirantis.com/bm/helm/baremetal-public-api-4.1.3.tgz"
I0602 18:00:38.263279   15261 cluster.go:271] Release status: Code: DEPLOYED
I0602 18:00:38.263534   15261 clusterdeployer.go:270] Deploy metallb in metallb-system namespace
I0602 18:00:38.263585   15261 cluster.go:202] Downloading the chart... chart URL "https://binary.mirantis.com/core/helm/metallb-1.20.2.tgz"
I0602 18:00:39.057856   15261 cluster.go:271] Release status: Code: DEPLOYED
I0602 18:00:39.057917   15261 cluster.go:719] missing GVK in the mapper: metal3.io/v1alpha1, Kind=BareMetalHost
```

```bash
I0602 18:00:39.059002   15261 cluster.go:690] found resource 'baremetalhostprofiles' for Kind=BareMetalHostProfile in version metal3.io/v1alpha1
I0602 18:00:39.059023   15261 cluster.go:690] found resource 'baremetalhostprofiles/status' for Kind=BareMetalHostProfile in version metal3.io/v1alpha1
I0602 18:00:39.059031   15261 cluster.go:690] found resource 'baremetalhosts' for Kind=BareMetalHost in version metal3.io/v1alpha1
I0602 18:00:39.059039   15261 cluster.go:690] found resource 'baremetalhosts/status' for Kind=BareMetalHost in version metal3.io/v1alpha1

I0602 18:00:39.230320   15261 baremetal.go:234] Wait for BareMetalHost default/master-0 inspection and IP assign..
```

```bash
I0602 18:48:05.090916   43153 bootstrap.go:9] Preparing bootstrap cluster
I0602 18:48:05.091156   43153 kind.go:91] Running: kind [create cluster --kubeconfig=/root/.kube/kind-config-clusterapi --image=mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8 --name=clusterapi]
I0602 18:48:38.754588   43153 kind.go:95] Ran: kind [create cluster --kubeconfig=/root/.kube/kind-config-clusterapi --image=mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8 --name=clusterapi] Output: Creating cluster "clusterapi" ...
 • Ensuring node image (mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8) 🖼  ...
 ✓ Ensuring node image (mirantis.azurecr.io/general/external/docker.io/kindest/node:v1.18.8) 🖼
 • Preparing nodes 📦   ...
 ✓ Preparing nodes 📦 
 • Writing configuration 📜  ...
 ✓ Writing configuration 📜
 • Starting control-plane 🕹️  ...
 ✓ Starting control-plane 🕹️
 • Installing CNI 🔌  ...
 ✓ Installing CNI 🔌
 • Installing StorageClass 💾  ...
 ✓ Installing StorageClass 💾
Set kubectl context to "kind-clusterapi"
You can now use your cluster with:

kubectl cluster-info --context kind-clusterapi --kubeconfig /root/.kube/kind-config-clusterapi

```


---

# Verif VLAN

MCC-01

```bash
show mac address-table interface Gi1/0/2
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 420    d4be.d9af.c2d5    DYNAMIC     Gi1/0/2


sw-d01-04>show mac address-table interface Gi1/0/22
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 172    d4be.d9af.c2cd    DYNAMIC     Gi1/0/22


sw-d01-04>show mac address-table interface Gi1/0/8 
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
```

## MCC -02
```bash
show mac address-table interface Gi1/0/4
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 420    d4be.d9af.c3a4    DYNAMIC     Gi1/0/4

sw-d01-04>show mac address-table interface Gi1/0/14
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----

sw-d01-04>show mac address-table interface Gi1/0/7 
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
```


## MCC-03

```bash
sw-d01-04>show mac address-table interface Gi1/0/1
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 420    549f.3503.dbde    DYNAMIC     Gi1/0/1

sw-d01-04>show mac address-table interface Gi1/0/15
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----


sw-d01-04>show mac address-table interface Gi1/0/16
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
```


```bash
ipmitool -I lanplus -H 10.29.20.13 -p  -U **** -P **** -v -R 12 -N 5 chassis power status
---
Chassis Power is on
---
Invalid parameter given or out of range for '-p'.
  -p port       Remote RMCP port [default=623]
```

```bash
ipmitool -I lanplus -H 10.29.20.11 -p  -U **** -P **** -v raw 0x30 0x9F
---
Discovered IPMB address 0x0
```

```bash
ipmitool -I lanplus -H 10.29.20.32 -U root -P OStack2021 sdr elist all
```

## MCC

Principe :
- 3 machines sur lesquels sera déployé MCC
- prérequis : accès idrac

https://docs.mirantis.com/container-cloud/latest/deployment-guide/deploy-bm-mgmt.html


| Machine | ip Idrac| Modéle |
|  |   |  |   | 










---
# Ressources debug 

* https://docs.mirantis.com/container-cloud/latest/release-notes/releases/2-6-0/known-2-6-0.html#ki-bm-2-6
* https://docs.mirantis.com/container-cloud/latest/operations-guide/single/index.html
* 




static IP : 10.29.241.xxx
static gateway : 10.29.241.1
static subnet mask : 255.255.255.0

DNS server : 192.44.75.10
DNS server : 192.108.115.2

root
OStack2021

---
Idracs stack D1-02/2 de 13 à 19
13 CEPH-OSD-01 - 70ZRCM2 - 241
14 OS-CTRL-01  - 6C9V173 - 231
15 OS-COMP-201 - BKHTYR2 - 221
16 CEPH-OSD-02 - 94K0NN2 - 242
17 OS-CTRL-02  - 7C9V173 - 232
18 CEPH-OSD-03 - 710VCM2 - 243
19 OS-CTRL-03  - 8C9V173 - 233

---
Interface sur les machines
R740XD
- eno 1 à 4
- ens3f  1 à 4
R640
- eno 1 à 4
- ens1f  0 à 1
- ens2f  0 à 1

# ----
En gros, j'ai configuré l'interface de management.
Je lui ai donné une IP sur le réseau ""provisioning/admin" d'Openstack (vlan 172, 10.129.172.0/24, routé, mais normalement accessible que depuis le Vlan DISI) qui sert pour les IP réelles des machines.
L'IP c'est :  10.129.172.201/24

Et je l'ai sécurisé avec un login/mot de passe renforcé : admin/admin ...
Il est accessible via ssh.

Plus d'infos sur ce que j'avais fait :  https://edisi.telecom-bretagne.eu/architectures/rhos/10_network_infra/switch_DELL/

Switch #1
MGMT
  IP :  10.129.20.201/24
  port : SW-D01-03 port 22
ports
 1/4  : p2 --> VSS
 5/7  : comp 51
 9/11 : ctrl 1
13/15 : CEPH1
17/19 : -
21/23 : ctrl 2
25 -> 30 : interconnexion
31/33 : CEPH2
35/37 : -
39/41 : CEPH3
43/45 : ctrl3
47/49 : -
51/53 : -

Switch #2

MGMT
  IP :  10.129.20.202/24
  port : SW-D01-03 port 23
ports
 1/4  : 
 5/7  : 
 9/11 : 
13/15 : 
17/19 : 
21/23 : 
25 -> 30 : interconnexion
31/33 : 31 - ,32 à 34 CEPH2
35/37 : -
39/41 : CEPH3
43/45 : ctrl3
47/49 : -
51/53 : -


---
Ci dessous un résumé des Vlans.

Vlan avec accès sur réseau IMT Atlantique
- External API Network (accès au tableau de bord horizon, et aux service) : VLAN 174, classe IP 192.168.174.0/24 --> actuellement c'est pas un Vlan, mais 1 IP dans le réseau "Admin", avec une ouverture des droits via ACL. C'est un des points à valider avec le presta
- External Provider Network Entreprise : VLAN 178, , classe IP 10.129.176.0/22  --> le réseau pour permettre aux instances d'êter accessibles, et de sortir sur le réseau de l'école
- Internet Network externe Internet : VLAN 179 , classe IP  ???  -> pour faire une config du style websalsa. on en avait juste parlé ensemble, ce n'était pas finalisé

Vlan d "admin" :   confortable avoir un accès depuis Vlan DISI mais pas indispensable. on peut passer par une gateway (enfin 2 pour pas avoir de SPOF)
  - Vlan IPMI (accès cartes Idrac ...) :  VLAN 420, classe IP 10.29.20.0/24
  - Vlan provisioning/admin (adresse IP des machines): VLAN 172, classe IP 10.129.172.0/24 

VLan Internes :
- les réseaux de projet en interne (Vxlan): VLAN 173, classe IP 192.168.173.0/24
- Internal Network API (échnages internes entre compsants) : VLAN  175 , classe IP 192.168.175.0/24
- Storage Network (acces Au stockage CEPH) : VLAN 176, classe IP  192.168.176.0/24
- Storage Management (Synchro entre noeuds CEPH): VLAN  177, classe IP 192.168.177.0/24


---
os-director-01.imta.fr: 10.129.172.13 -> OK
  #1 SW-D01-0x pxx
  #2 SW-D01-0x pxx
  #3 SW-D01-0x pxx
  #4 SW-D01-0x pxx
  idrac: 10.29.20.20

os-director-02.imta.fr: 10.129.172.14 -> OK
  #1 SW-D01-04 p13
  #2 SW-D01-03 p5
  #3 SW-D01-03 p6
  pas d'idrac

ceph-poc-01.imta.fr: 10.129.172.31  -> OK
ceph-poc-02.imta.fr: 10.129.172.32  -> OK
ceph-poc-03.imta.fr: 10.129.172.33  -> OK

os-crtl-poc-01
  idrac: 10.29.20.31

os-crtl-poc-02
  idrac: 10.29.20.32

os-crtl-poc-03
  idrac: 10.29.20.31

os-monitoring-01: 10.29.241.6
  #1 D1-02/1 p21
  #2 D1-02/1 p22
  rem : systéme ubuntu, disque 300G
---
https://galaxy.ansible.com/michaelrigart/interfaces


---
Découpage du Vlan 10.129.172.0/24

Découpage en 4 parties, en prenant en compte un éventuel découpage en sous réseaux
 - systéme et outil  (crtl plane OS, proxy, gateway,...)
   - 10.129.172.0/26 (masque 255.255.255.192): 10.129.172.0 -10.129.172.63
 - compute OS
   - 10.129.172.64/26 (masque 255.255.255.192): 10.129.172.64 -10.129.172.127
 - CEPH
   - 10.129.172.128/26 (masque 255.255.255.192): 10.129.172.128 -10.129.172.191
 - Workers K8S  et autres machine en Baremetal
   - 10.129.172.192/26 (masque 255.255.255.192): 10.129.172.128 -10.129.172.254

Découpage pour les cartes iDrac  : 10.
 - en correspondance avec les IP du vlan Vlan 10.129.172.0/24
 - prendre quelques IP dans la classe "systéme et outil" pour le réseau en début de la classe. min 6, max 9.
   - switch DELL 10G : 2, voir 3 à terme
   - switch 1Go Cisco IPMI : 1, voir 2 à terme
   - switch 1Go Cisco : 3 voir 4 à terme
  
---
Config Reseau

Config DHCP Vlan ADMIN
```bash
subnet 10.129.172.0 netmask 255.255.255.0
                {
                one-lease-per-client on;
                default-lease-time 1200;
                max-lease-time 1800;
                option routers 10.129.172.1;
                option subnet-mask 255.255.255.0;
                option broadcast-address 10.129.172.255;
                option domain-name "imta.fr";
                option domain-name-servers 192.44.75.10, 192.108.115.2;
                option time-servers 192.44.75.10;
                option ntp-servers 192.44.75.10;
                filename "clientlourd/pxelinux.0";
                next-server 10.29.89.3;
```


CEPH-OSD-01
| Param | Value |
| ------ | ------ |
| Service Tag | 70ZRCM2 |
| EQI |  |
| Modèle | PowerEdge R740xd |
| nom DNS | ceph-osd-01.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 50:9a:4c:b1:11:78 |
| Idrac IP | 10.29.241.241 | 10.129.20.131
| Idrac Switch | stack D1-02/2 |
| Idrac port | 13 |
| NIC Integrated 1 | Intel(R) 10GbE 4P X710 rNDC |
| NIC Slot 3 | Intel(R) 10GbE 4P X710 Adapter |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:67:70 |
| Vlan admin IP | 10.129.172.131 |
| Vlan admin Switch |  |
| Vlan admin port |  |


CEPH-OSD-02
| Param | Value |
| ------ | ------ |
| Service Tag | 94K0NN2 |
| EQI |  |
| Modèle | PowerEdge R740xd |
| nom DNS | ceph-osd-02.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 58:8a:5a:f0:83:3c |
| Idrac IP | 10.29.241.242 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 16 |
| NIC Integrated 1 | Intel(R) 10GbE 4P X710 rNDC |
| NIC Slot 3 | Intel(R) 10GbE 4P X710 Adapter |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:BE:80 |
| Vlan admin IP | 10.129.172.132 |
| Vlan admin Switch |  |
| Vlan admin port |  |

CEPH-OSD-03
| Param | Value |
| ------ | ------ |
| Service Tag | 710VCM2 |
| EQI |  |
| Modèle | PowerEdge R740xd |
| nom DNS | ceph-osd-03.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 50:9a:4c:b1:10:76 |
| Idrac IP | 10.29.241.243 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 19 |
| NIC Integrated 1 | Intel(R) 10GbE 4P X710 rNDC |
| NIC Slot 3 | Intel(R) 10GbE 4P X710 Adapter |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:C2:B0 |
| Vlan admin IP | 10.129.172.133 |
| Vlan admin Switch |  |
| Vlan admin port |  |

OS-CTRL-01
| Param | Value |
| ------ | ------ |
| Service Tag | 6C9V173 |
| EQI |  |
| Modèle | PowerEdge R640 |
| nom DNS | os-ctrl-01.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 70:b5:e8:cd:8b:b8 |
| Idrac IP | 10.29.241.231 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 14 |
| NIC Integrated 1 | Intel(R) Ethernet 10G 4P X710 SFP+ rNDC |
| NIC Slot 1 | Intel(R) X710 10GbE Controller |
| NIC Slot 2 | Intel(R) X710 10GbE Controller |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:B2:B0 |
| Vlan admin IP | 10.129.172.21 |
| Vlan admin Switch |  |
| Vlan admin port |  |


OS-CTRL-02
| Param | Value |
| ------ | ------ |
| Service Tag | 7C9V173 |
| EQI |  |
| Modèle | PowerEdge R640 |
| nom DNS | os-ctrl-02.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 70:b5:e8:cd:8b:b2 |
| Idrac IP | 10.29.241.232 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 17 |
| NIC Integrated 1 | Intel(R) Ethernet 10G 4P X710 SFP+ rNDC |
| NIC Slot 1 | Intel(R) X710 10GbE Controller |
| NIC Slot 2 | Intel(R) X710 10GbE Controller |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:67:A0 | 
| Vlan admin IP | 10.129.172.22 |
| Vlan admin Switch |  |
| Vlan admin port |  |


OS-CTRL-03
| Param | Value |
| ------ | ------ |
| Service Tag | 8C9V173 |
| EQI |  |
| Modèle | PowerEdge R640 |
| nom DNS | os-ctrl-03.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 70:b5:e8:cd:8d:86 |
| Idrac IP | 10.29.241.233 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 19 |
| NIC Integrated 1 | Intel(R) Ethernet 10G 4P X710 SFP+ rNDC |
| NIC Slot 1 | Intel(R) X710 10GbE Controller |
| NIC Slot 2 | Intel(R) X710 10GbE Controller |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:67:50 |
| Vlan admin IP | 10.129.172.23 |
| Vlan admin Switch |  |
| Vlan admin port |  |


OS-COMP-71 
| Param | Value |
| ------ | ------ |
| Service Tag | BKHTYR2 |
| EQI |  |
| Modèle | PowerEdge R640 |
| nom DNS | os-comp-71.imta.fr |
| Idrac version | 9 |
| Idrac MAC | 4c:d9:8f:06:9d:6a |
| Idrac IP | 10.29.241.221 |
| Idrac Switch | stack D1-02/2 |
| Idrac port | 15 |
| NIC Integrated 1 | Intel(R) 10GbE 4P X710 rNDC |
| NIC Slot 1 | Intel(R) 10GbE 2P X710 Adapter |
| NIC Slot 2 | Intel(R) 10GbE 2P X710 Adapter |
| Vlan Admin MAC/PXE  | E4:43:4B:EA:73:B0 |
| Vlan admin IP | 10.129.172.71 |
| Vlan admin Switch |  |
| Vlan admin port |  |


## config Rséeau DNS/DHCP 

### Config DHCP

host ceph-osd-01      { hardware ethernet E4:43:4B:EA:67:70 ; fixed-address 10.129.172.131; }
host ceph-osd-02      { hardware ethernet E4:43:4B:EA:BE:80 ; fixed-address 10.129.172.132; }
host ceph-osd-03      { hardware ethernet E4:43:4B:EA:C2:B0 ; fixed-address 10.129.172.133; }
host os-ctrl-01       { hardware ethernet E4:43:4B:EA:B2:B0 ; fixed-address 10.129.172.21;  }
host os-ctrl-02       { hardware ethernet E4:43:4B:EA:67:A0 ; fixed-address 10.129.172.22;  }
host os-ctrl-03       { hardware ethernet E4:43:4B:EA:67:50 ; fixed-address 10.129.172.23;  }
host os-comp-71       { hardware ethernet E4:43:4B:EA:73:B0 ; fixed-address 10.129.172.71;  }

### Config DNS

ceph-osd-01 10.129.172.131
ceph-osd-02 10.129.172.132
ceph-osd-03 10.129.172.133
os-ctrl-01  10.129.172.21
os-ctrl-02  10.129.172.22
os-ctrl-03  10.129.172.23
os-comp-71  10.129.172.71


---

#  config CEPH


Gestion des disques au Boot des machines Dell avec Kickstart Ubuntu :

- si un disque "Non Raid "est configuré, il est utilisé par kickstart pour installer le système
- si plusieurs disques virtuels sont configurés, kickstart prend le premier qui vient, et ne tient pas compte de la configuration du disque de boot faite au niveau de la carte RAID. L'installation peut donc se faire sur un autre VD que celui qu a été défini pour le boot.

Pour être sur de ne pas avoir de problème, il faut donc :

-  avoir un seul Virtual Disque configuré en RAID, et configuré au niveau de la carte RAID comme disque de boot (ou en tout cas faire en sorte que ce soit le premier de la liste)
- placer tous les disques en mode "RAID", sans les intégrer à un disque virtuel
- pour être vraiment sur d'éviter le spb, il est conseillé de placer les disques système sur les emplacements avec les ID les plus faibles (0 et 1)



Config machine

| Nom	| État | Numéro de logement |	Taille | Condition de la sécurité	| Protocole du bus	| Type de média	| Disque de secours	| Endurance d'écriture évaluée restante |
| Solid State Disk 0:1:0	| Non RAID |  0 |  447.13 Go | Non pris en charge	| SATA	| SSD	| Non	| 99% |
|	Physical Disk 0:1:1		  | Non RAID |  1 | 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:2		  | Non RAID |  2 | 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:3		  | Non RAID |  3 | 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Solid State Disk 0:1:6	| Non RAID |  6 |  447.13 Go | Non pris en charge		| SATA		| SSD		| Non	99%
|	Physical Disk 0:1:7		  | Non RAID | 	7 | 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:8		  | Non RAID |  8	| 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:9		  | Non RAID |  9	| 7452.04 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:12		| En ligne | 12	|  278.88 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable
|	Physical Disk 0:1:13		| En ligne | 13 |  278.88 Go | Non pris en charge		| SAS		| HDD		| Non		| Non applicable

