

---
13 CEPH-OSD-01 - 70ZRCM2 - 241
16 CEPH-OSD-02 - 94K0NN2 - 242
18 CEPH-OSD-03 - 710VCM2 - 243

rh-manager-br-01 : R640  BKHSYR2 https://10.29.20.11/
rh-manager-br-02 : R630  J6P69F2 https://10.29.20.12/


``` bash
 openstack baremetal introspection data save rhos-comp-br-02 |  jq ".inventory.boot.current_boot_mode"
 ```
- name: rhos-comp-br-01
  serial: CZCFS12
  model: PowerEdge R620
  ipmi_address:   
  boot: UEFI
  status: IDRAC à mettre à jour

- name: rhos-comp-br-02
  serial: CGTGLG2
  model: PowerEdge R630
  ipmi_address:  10.29.20.102
  boot: UEFI

- name: rhos-comp-br-03
  serial: 47K28Z1
  model: PowerEdge R620
  ipmi_address:  10.29.20.103
  boot: UEFI

- name: rhos-comp-br-04
  serial: 9ZCFS12
  model: PowerEdge R620
  ipmi_address:  10.29.20.104
  boot: UEFI
  status: IDRAC à mettre à jour

- name: rhos-comp-br-05
  serial: DZCFS12
  model: PowerEdge R620
  ipmi_address:  10.29.20.105
  boot: UEFI

- name: rhos-comp-br-06
  serial: CZ58KY1
  model: PowerEdge R620
  ipmi_address:  10.29.20.106
  boot: UEFI
  status: IDRAC à mettre à jour

- name: rhos-comp-br-07
  serial: GZCFS12
  ipmi_address:  10.29.20.107
  boot: UEFI
  status: IDRAC à mettre à jour

- name: rhos-comp-br-08
  serial: 6RM8G32
  model: PowerEdge R620
  ipmi_address:  10.29.20.108  
  boot: UEFI
 status: IDRAC à mettre à jour

- name: rhos-comp-br-09
  serial: MINWINPC
  model: PowerEdge R630
  ipmi_address:  10.29.20.109

- name: rhos-comp-br-10
  serial: HPL8G32 
  model: PowerEdge R620
  ipmi_address:  10.29.20.110


- name:  rhos-comp-ds-br-01
  serial: 7C9V173
  ipmi_address:  10.29.20.121

- name:  rhos-comp-ds-br-02
  serial 8C9V173 
  ipmi_address:  10.29.20.122

- name:  rhos-comp-ds-br-03
  serial: BKDQYR2
  model: PowerEdge R640
  ipmi_address:  10.29.20.123
  boot: UEFI

- name:  rhos-comp-ds-br-04
  serial: 6C9V173
  ipmi_address:  10.29.20.124
  boot: UEFI


- name:  rhos-comp-ds-br-05
  serial: BKHTYR2
  ipmi_address:  10.29.20.125
  model: PowerEdge R640
  boot: UEFI

- name:  rhos-comp-ds-br-06
  serial: BKJLYR2
  ipmi_address:  10.29.20.126
