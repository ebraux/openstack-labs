## Config spécifique environnement IMT-Atlantique

---
## Modifier la config DNS
``` bash
nmcli con mod ens10f0np0 ipv4.dns 10.129.172.11
nmcli con mod ens10f0np0 ipv4.dns-search imta.fr

systemctl restart NetworkManager

```


DNS server : 192.44.75.10
DNS server : 192.108.115.2

root
OStack2021


---
## Configuration de l'accès aux ressources externes via le proxy

### Pour le gestionnaire de paquet, DNF

Ajouter l'accès au proxy pour dnf
fichier /etc/dnf/dnf.conf, ajouter 
``` bash
proxy=http://proxy.enst-bretagne.fr:8080
```
https://www.cyberciti.biz/faq/how-to-use-dnf-command-with-a-proxy-server-on-fedora/


### Pour subscription manager de paquet, RHSM

Ajouter l'accès proxy pour subscription manager
fichier : /etc/rhsm/rhsm.conf, modifier
``` bash
# an http proxy server to use
#proxy_hostname =
proxy_hostname = proxy.enst-bretagne.fr

# The scheme to use for the proxy when updating repo definitions, if needed
# e.g. http or https
proxy_scheme = http

# port for http proxy server
#proxy_port =
proxy_port = 8080
```

---
## Enregistrement du serveur auprès de RedHat

Enregistrer la machine
``` bash
subscription-manager register
# Registering to: subscription.rhsm.redhat.com:443/subscription
# Username: DISI1
# Password: 
# The system has been registered with ID: 45ca7596-c245-4615-ba40-2541e6e303ee
# The registered system name is: manager-01
```
Définir les souscriptions disponibles
``` bash
subscription-manager list --available
``` 

``` bash
Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
Pool ID: 2c9486ec83d72eeb0183fab02d0436ff
```

Attacher la machine au pool 
``` bash
subscription-manager attach --pool=2c9486ec83d72eeb0183fab02d0436ff
# Successfully attached a subscription for: Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
```

---
## Mettre à jour

``` bash
dnf update -y
# Updating Subscription Management repositories.
# Unable to read consumer identity
# This system is not registered with an entitlement server. You can use subscription-manager to register.
# Error: There are no enabled repositories in "/etc/yum.repos.d", "/etc/yum/repos.d", "/etc/distro.repos.d".
```

---
## Installation des paquets communs
 
``` bash
dnf install -y git tar vim curl
```



### configurer le proxy
``` bash
echo "http_proxy=http://10.129.172.11:3128/"  >> /etc/environment
echo "https_proxy=http://10.129.172.11:3128/" >> /etc/environment
echo "no_proxy=localhost,10.129.172.0/24"     >> /etc/environment

echo "export http_proxy=http://10.129.172.11:3128/"   >  /etc/profile.d/http_proxy.sh
echo "export https_proxy=http://10.129.172.11:3128/"  >> /etc/profile.d/http_proxy.sh
echo "export no_proxy=localhost,10.129.172.0/24"      >> /etc/profile.d/http_proxy.sh

curl http://www.google.com
```


Ajouter l'accès au proxy pour dnf
fichier /etc/dnf/dnf.conf, ajouter 
``` bash
proxy=http://10.129.172.11:3128
```
https://www.cyberciti.biz/faq/how-to-use-dnf-command-with-a-proxy-server-on-fedora/


Ajouter l'accès proxy pour subscription manager
fichier : /etc/rhsm/rhsm.conf, modifier
``` bash
# an http proxy server to use
#proxy_hostname =
proxy_hostname = proxy.enst-bretagne.fr

# The scheme to use for the proxy when updating repo definitions, if needed
# e.g. http or https
proxy_scheme = http

# port for http proxy server
#proxy_port =
proxy_port = 8080
```


https://access.redhat.com/documentation/fr-fr/red_hat_certificate_system/9/html/planning_installation_and_deployment_guide/firewall_configuration


