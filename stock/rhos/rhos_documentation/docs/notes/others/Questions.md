

- Lancer des commandes "ironic"
  - lancer des commandes
  - lister les entrées MAC/IP quand elle sont déclrarées
  - lister les node au niveau d'Ironic (clean_failed)


- autoscale de l'overcloud par l'undercloud ?
en 17.1 : https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/17.1/html/auto-scaling_for_instances/assembly-configuring-and-deploying-the-overcloud-for-autoscaling_assembly-configuring-and-deploying-the-overcloud-for-autoscaling



- Mise à jour 
  - voir en quelle version on est (mineure)
  - mise à jour ?
the patch to fix this issue contains the below-updated rpm package that will exist in the z6 release (16.2.6) and it should be out next month, it should solve the issue.
openstack-octavia-ui-4.0.3-2.20230727225138.ecb3ecd.el8osttrunk.src.rpm
uiniquement tag 16.2 dans les images.
Mettre à jour les images, et redéployer ?


- migration en 17.1
  - possible ? contraintes (ceph, ...)
  - processus ?
  - validation ?

---
Pacemaker.
sur controller0, pcs status

# Failed Resource Actions:
#   * galera_monitor_10000 on galera-bundle-2 'not running' (7): call=187, status='complete', exitreason='Group mysql doesn't exist', last-rc-change='2023-10-02 11:19:49Z', queued=0ms, exec=0ms


---

``` bash
systemctl list-units -t service triple*
  UNIT                                        LOAD   ACTIVE SUB     DESCRIPTION                         
● tripleo_glance_api_cron_healthcheck.service loaded failed failed  glance_api_cron healthcheck
```

 systemctl status tripleo_glance_api_cron_healthcheck.service
● tripleo_glance_api_cron_healthcheck.service - glance_api_cron healthcheck
   Loaded: loaded (/etc/systemd/system/tripleo_glance_api_cron_healthcheck.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Wed 2023-10-04 07:01:54 UTC; 26s ago
  Process: 449032 ExecStart=/usr/bin/podman exec --user root glance_api_cron /usr/share/openstack-tripleo-common/healthcheck/cron glance (code=exited, status=1/FAILURE)
 Main PID: 449032 (code=exited, status=1/FAILURE)

Oct 04 07:01:53 rhos-ctrl-br-2 systemd[1]: Starting glance_api_cron healthcheck...
Oct 04 07:01:54 rhos-ctrl-br-2 podman[449032]: 2023-10-04 07:01:54.077562115 +0000 UTC m=+0.118105362 container exec abae77d08d9d291d6f1772e6d966e69c27187967411b74f571415dc84dd05301 (image=rhos-director->
Oct 04 07:01:54 rhos-ctrl-br-2 systemd[1]: tripleo_glance_api_cron_healthcheck.service: Main process exited, code=exited, status=1/FAILURE
Oct 04 07:01:54 rhos-ctrl-br-2 systemd[1]: tripleo_glance_api_cron_healthcheck.service: Failed with result 'exit-code'.
Oct 04 07:01:54 rhos-ctrl-br-2 systemd[1]: Failed to start glance_api_cron healthcheck.


ExecStart=/usr/bin/podman exec --user root glance_api_cron /usr/share/openstack-tripleo-common/healthcheck/cron glance 