# Analyse des flow OpensFlow



The ovs-appctl ofproto/trace command allows the simulation of different packet types
entering an OVN bridge on a given interface. To trace packets from a specific instance, first obtain
the MAC address and TAP interface name. From the applicable compute host, use the virsh
dumpxml domain command.

Récupérer le nom de l'hypervisueur et de la VM.
``` bash
openstack server show
```

Récupérer dans la config de la VM, le nom de la tap qui lui est associée.
Cette TAP correspond à l'interface connecté à br-int.


``` bash
[root@compute ~]# virsh dumpxml instance-00000008
...output omitted...
<interface type='bridge'>
<mac address='fa:16:3e:79:0c:05'/>
<source bridge='br-int'/>
<virtualport type='openvswitch'>
<parameters interfaceid='10447eb8-a243-4c61-877f-1f9a5f3c553e'/>
</virtualport>
<target dev='tap10447eb8-a2'/>
<model type='virtio'/>
<mtu size='1442'/>
<alias name='net0'/>
<address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
</interface>
...output omitted...
```

Tout d'abord, vérifier que la tap est bien connectée à br-int, avec `ovs-vsctl show`


Ensuite, simuler un flow réseau pour tester l'interface avec `ovs-appctl ofproto/trace` 

- on précise `br-int`,car l'instance est connectée à br-int
- on reseigne le nom de l'interface "tap ..." et l'adresse mac 
- ...

``` bash
ovs-appctl ofproto/trace br-int \
 in_port="tap10447eb8-a2",\
 tcp,\
 nw_src=192.168.1.11,\
 dl_src=fa:16:3e:79:0c:05,\
 nw_dst=192.168.1.8,\
 tcp_dst=22
```

Toute regles de getsion d eflow sont appliquées les une après les autres.
Si on obtient au final `No match`, c'ets que l'IP destination n'existe pas.

---
##  debug de la connexion externe sur un compute


``` bash
ovs-vsctl list open . | grep external_ids
```

- `ovn-bridge-mappings` : lien pour la connectivité externe ex datacenter:br-ex
- `ovn-cms-options`:  si 'enable-chassis-as-gw', le node peut êtr utilisé comme gateway pour external connectivity. nécéssaire avce DVR

---
## service ssu rle cmpute

local OVS componenet ne sont pas sous forme de conteneurs, mais gérés au niveau système.
``` bash
systemctl list-units | grep -€ '(ovs-vswitchd|ovsdb-server)'
# ovs-vswitchd.service ...
# ovsdb.service ...
```
2 conteneurs ovn
``` bash
podman ps | grep -i ovn_
# ovn_metadat_agent
# ovn_controller
```
- controler :
  - sonnct à la soutDB, 
  - act as OVS controller
    - monitor et control network traffis
    - configure openflow rules sur l'OVS local
- metadata
  - expose HAProxy service
    - proxy metadatas API requets to Nova

Si on fait un pstree -a sur le process du conteneur, on voit un process haproxy qui tourne. le fichier de conf à l'ID du network ??

Quand une instance est créee, un namespace résesaue ets créé. On peut le voir avec `ip netns`, "ovnmeta-ID"
Et si on fait un ip a sur ce namaspace
``` bash
ip netns exec ovnmeta-ID ip a
# la tap assocoée à l'instance 
```
TIP : On peut m^me utiliser cette connexion pour accéder à un einstance localement
``` bash
ip netns exec ovnmeta-ID ssh cirros@IP-INSTANCE
