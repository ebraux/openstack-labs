# Instance pas accessibles

---
## A voir côté utilisateur

Vérifier que l'instance fonctionne
``` bash
openstack server list -c Name  -c Status -c State
```

Vérifier qu'elle a bien une IP flottante
``` bash
openstack server show  -c Name -c Networks MY_INSTANCE
```


Vérifier les security group associé à l'instance
``` bash
openstack server show MY_INSTANCE -f json -c security_groups
```
Et vérifier le security groupe
``` bash
openstack security group rule list MY_INSTANCE_SECURITY_GROUPE -f json
```
 
---
## A voir côté infra

Pour le routeur sur lequel le subnet de l'instance est relié :

Afficher la liste des routeur, et vérifier que le routeur est bien Actif et UP
``` bash
openstack router list -c Name -c Status -c State
+--------------+--------+-------+
| Name         | Status | State |
+--------------+--------+-------+
| router-admin | ACTIVE | UP    |
+--------------+--------+-------+
```

- vérifier qu'une gateway est bien configurée vers le réseau externe
 
Vérifier le routeur à laquelle l'instance est connectée
``` bash
openstack router show router-admin  -f json  -c external_gateway_info
# {
#   "external_gateway_info": {
#     "network_id": "6be6855f-b212-4771-8e31-843aaa205910",
#     "external_fixed_ips": [
#       {
#         "subnet_id": "49001d11-4d28-45a7-b28a-9d2c5f536361",
#         "ip_address": "10.129.176.136"
#       }
#     ],
#     "enable_snat": true
#   }
# }
```
- Si la valeur de `external_gateway_info`, alors la gateway n'a pas été correctement configurée sur le router. Il faut en ajouter une. 
- Si une gateway a bien été configurée, le ping doit répondre sur l'IP indiquée dans `ip_address`. Si ce n'est pas le cas, il faut s'attaquer à la partie virtualisation de réseau gérée par OVN [OVN Debug](./ovn-debug.md)



---
## Spécifique SSH

- Vérifier le security group
- Vérifier la Keypair

Si la keypair n'est pas présente, 

- non renseignée lors de la création
- script cloud-init en erreur
- pb réseau lors de l'initialisation : c'est cloud-init qui récupère la keypair lors d e l'initialisation, en interrogeant l'API de metadonnées. Il peut arriver qu'OVN mette trop de temps à mettre les flow nécessires en place.

Pour pourvoir définir la cause, il faut pouvoir accèder au fichier de logs de cloud-init, `/var/log/cloud-init.log` dans l'instance. 

Le plus simple c'est de recréer l'instance.

Dans les logs, si il y a un probléme d'accès au serveur de metadatas, on retrouve :
``` bash
[ 134.170335] cloud-init[475]: 2014-07-01 07:33:22,857 -
url_helper.py[WARNING]: Calling 'http://192.168.0.1//latest/meta-data/instance-
id' failed [0/120s]:
request error [HTTPConnectionPool(host='192.168.0.1', port=80): Max retries
exceeded with url: //latest/meta-data/instance-id (...)
[Errno 113] No route to host)]
```
Si le serveur de metadata n'est pas accessible, c’est pas simple à résoudre.

Il faut d'abord vérifier que l'option `NeutronEnableIsolatedMetadata` a bien été activée lors du déploiement de l'overcloud
