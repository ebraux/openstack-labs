

---
## logs

afficher les derniers logs
``` bash
docker logs --since 30s -f <container_name_or_id>
docker logs --tail 20 -f <container_name_or_id>
```


- [https://stackoverflow.com/questions/41091634/how-to-clean-docker-container-logs](https://stackoverflow.com/questions/41091634/how-to-clean-docker-container-logs)


trouver le sfichier de logs sur l'hôte
``` bash
podman inspect  --format='{{.HostConfig.LogConfig.Path}}'  <container-id>
```