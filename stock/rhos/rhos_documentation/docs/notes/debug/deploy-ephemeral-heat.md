


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_scaling-overcloud-nodes#proc_removing-a-compute-node-manually_scaling-overcloud-nodes](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_scaling-overcloud-nodes#proc_removing-a-compute-node-manually_scaling-overcloud-nodes)
- [https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/ephemeral_heat.html](https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/ephemeral_heat.html)


- [https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/ansible_config_download.html](https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/ansible_config_download.html)

-[https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_launching-ephemeral-heat-process_performing-basic-overcloud-administration-tasks](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/installing_and_managing_red_hat_openstack_platform_with_director/assembly_performing-basic-overcloud-administration-tasks#proc_launching-ephemeral-heat-process_performing-basic-overcloud-administration-tasks)

``` bash
unset OS_CLOUD
unset OS_PROJECT_NAME
unset OS_PROJECT_DOMAIN_NAME
unset OS_USER_DOMAIN_NAME
OS_AUTH_TYPE=none
OS_ENDPOINT=http://127.0.0.1:8006/v1/admin
export OS_CLOUD=heat
```

``` bash
openstack tripleo launch heat --heat-dir /home/stack/overcloud-deploy/overcloud/heat-launcher --restore-db
# 2023-11-21 18:11:08,903 - tripleoclient.heat_launcher - INFO - Skipping container image pull.
# 2023-11-21 18:11:08,912 - tripleoclient - INFO - Launching Heat pod
# 2023-11-21 18:11:08,913 - tripleoclient.heat_launcher - INFO - Checking that database is up
# 2023-11-21 18:11:09,519 - tripleoclient.heat_launcher - INFO - Checking that message bus (rabbitmq) is up
# 2023-11-21 17:11:14.932 2 INFO migrate.versioning.api [-] 72 -> 73... 
# 2023-11-21 17:11:15.106 2 INFO migrate.versioning.api [-] done
#  ...
# 2023-11-21 17:11:15.289 2 INFO migrate.versioning.api [-] 85 -> 86... 
# 2023-11-21 17:11:15.341 2 INFO migrate.versioning.api [-] done
# 2023-11-21 18:11:18,777 - tripleoclient.heat_launcher - INFO - Restoring db from /home/stack/overcloud-deploy/overcloud/heat-launcher/heat-db.sql-1700544262.1488206.tar.bzip2
# Pod:
# 6a739d86846eb07e50c00279cf73ae4241b6f9b6ab91aca995b8c4ef6f26baaa
# Containers:
# 0f00ca8403a19b395d8abd5f2b05af287d29d4b7a020d790af2cb168eb258c43
# de49ef67ff0196a4660b0e531716ef641a2fcf0a761ebb8b2d1472a056cc68a0

# 2023-11-21 18:11:35,929 - tripleoclient - INFO - Writing heat clouds.yaml

```

Vérifier que 'est bien lancé
``` bash
sudo podman pod ps
# POD ID        NAME            STATUS      CREATED             INFRA ID      # OF CONTAINERS
# 6a739d86846e  ephemeral-heat  Running     About a minute ago  f29ae5f93314  3
```

``` bash
export OS_CLOUD=heat
openstack stack list
# +--------------------------------------+------------+---------+-----------------+----------------------+--------------+
# | ID                                   | Stack Name | Project | Stack Status    | Creation Time        | Updated Time |
# +--------------------------------------+------------+---------+-----------------+----------------------+--------------+
# | 60da754c-b795-4ab1-a473-701b39755655 | overcloud  | admin   | CREATE_COMPLETE | 2023-11-21T05:25:04Z | None         |
# +--------------------------------------+------------+---------+-----------------+----------------------+--------------+
```

Lancer les commande d'interrogation. et une fois terminé
``` bash
 openstack tripleo launch heat --kill
 ```

---
## Exemple d'interrogation

``` bash
openstack stack resource show overcloud ControllerServers -f yaml
# attributes:
#   value:
#     '0': 2a076ed9-51a6-4761-9196-8edbb322efbd
#     '1': f264b681-e232-41eb-be13-ca4f10c59d0b
#     '2': 888cd505-0c59-4373-b115-48d6f4cfd292
# creation_time: '2023-11-21T05:25:05Z'
# description: ''
# links:
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655/resources/ControllerServers
#   rel: self
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655
#   rel: stack
# logical_resource_id: ControllerServers
# physical_resource_id: overcloud-ControllerServers-sqhxhvbxedzz
# required_by:
# - AllNodesExtraConfig
# - AllNodesDeploySteps
# resource_name: ControllerServers
# resource_status: CREATE_COMPLETE
# resource_status_reason: state changed
# resource_type: OS::Heat::Value
# updated_time: '2023-11-21T05:25:05Z'
```

``` bash
openstack stack resource show overcloud ComputeServers -f yaml
# attributes:
#   value:
#     '0': ce0ca994-a3c5-4346-87eb-ee886af73677
#     '1': 490b03ff-106c-4f39-84d3-504d517044b6
#     '2': 7be0de89-a3db-462a-b209-41fe552bf686
#     '3': 926db463-0570-44ea-b7be-c7d3c63ee0eb
#     '4': 3c0e34b8-a26d-4089-9a64-e2eb9a55d474
#     '5': 0e7d4583-40d5-4220-82de-7c26f3cbfa45
#     '6': 533ffcea-7d40-44a5-ae82-052df83c5488
#     '7': 3447ee92-83a2-4580-ab1a-d98fc962c931
#     '8': 4730b3bb-af68-416b-9a17-8a00c2e7ee2e
#     '9': 005718b9-4da1-4c06-ad78-575a62bf8a40
# creation_time: '2023-11-21T05:25:05Z'
# description: ''
# links:
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655/resources/ComputeServers
#   rel: self
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655
#   rel: stack
# logical_resource_id: ComputeServers
# physical_resource_id: overcloud-ComputeServers-zdhu4fcr5t3h
# required_by:
# - AllNodesExtraConfig
# - AllNodesDeploySteps
# resource_name: ComputeServers
# resource_status: CREATE_COMPLETE
# resource_status_reason: state changed
# resource_type: OS::Heat::Value
# updated_time: '2023-11-21T05:25:05Z'

```

``` bash
openstack stack resource show overcloud ComputeDSServers -f yaml
# attributes:
#   value:
#     '0': e15413bd-2380-433c-9042-9763657eab93
#     '1': 69cbd9b5-f7da-42e8-b071-f7e790835f36
#     '2': d2046c7c-27a1-4399-ad5e-776c7f8540d7
#     '3': e7c21fa8-176c-445a-b0ef-5ace19e18894
#     '4': 0b17e226-2405-4625-8b76-9c4d50bceeea
# creation_time: '2023-11-21T05:25:05Z'
# description: ''
# links:
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655/resources/ComputeDSServers
#   rel: self
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655
#   rel: stack
# logical_resource_id: ComputeDSServers
# physical_resource_id: overcloud-ComputeDSServers-pmp7rlwdyhgv
# required_by:
# - AllNodesExtraConfig
# - AllNodesDeploySteps
# resource_name: ComputeDSServers
# resource_status: CREATE_COMPLETE
# resource_status_reason: state changed
# resource_type: OS::Heat::Value
# updated_time: '2023-11-21T05:25:05Z'
```

``` bash
openstack stack resource show overcloud Compute -f yaml
# attributes:
#   attributes: null
#   refs: null
#   refs_map: null
#   removed_rsrc_list: []
# creation_time: '2023-11-21T05:25:05Z'
# description: ''
# links:
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655/resources/Compute
#   rel: self
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud/60da754c-b795-4ab1-a473-701b39755655
#   rel: stack
# - href: http://127.0.0.1:8006/v1/admin/stacks/overcloud-Compute-jqzjws2rdl3a/806547f5-6091-477a-9219-e313dfeb65bc
#   rel: nested
# logical_resource_id: Compute
# physical_resource_id: 806547f5-6091-477a-9219-e313dfeb65bc
# required_by:
# - BlacklistedIpAddresses
# - BlacklistedHostnames
# - AnsibleHostVars
# - ComputeIpListMap
# - ComputeNetworkHostnameMap
# - ComputeServers
# - HostsEntryValue
# - ServerIdMap
# - AllNodesDeploySteps
# resource_name: Compute
# resource_status: CREATE_COMPLETE
# resource_status_reason: state changed
# resource_type: OS::Heat::ResourceGroup
# updated_time: '2023-11-21T05:25:05Z'

```

``` bash
openstack stack resource show overcloud -f yaml
```



ansible -i ./overcloud-deploy/overcloud/tripleo-ansible-inventory.yaml all --list


