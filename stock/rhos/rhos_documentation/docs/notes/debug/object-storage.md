# Operation / bug avec Swift

---
## Container listing

https://access.redhat.com/solutions/3077811

Unable to view Swift containers in Horizon

Issue
- When logged in to the Horizon Dashboard, non-admin users are unable to access the Object Storage tab
- This error is seen when 'Object Storage' is selected: Error: Unable to get the Swift container listing.
- This error is seen when users attempt to create containers: Unable to create containers.

Ensure the user has the swiftoperator role:
``` bash
$ openstack role add --user <UID> --project <project> swiftoperator
```

Root Cause : User only has the member role and wasn't permitted to interact with swift:
``` bash
[stack@ospdir ~]$ openstack role assignment list --user <UID> --project <project> --name
+----------+---------------+-------+-----------------------+--------+-----------+
| Role     | User          | Group | Project               | Domain | Inherited |
+----------+---------------+-------+-----------------------+--------+-----------+
| _member_ | <user>        |       | <project>             |        | False     |
+----------+---------------+-------+-----------------------+--------+-----------+
```

---
## utiliser des token d'accès

Unable to access a public container in Swift integrated with Ceph RGW and Keystone
https://access.redhat.com/solutions/6116021