
# Debug des conteneurs


---
###  Information sur podman

Infos sur Podman (version, configuration, registries, ...)
``` bash
podman info 
```

---
## Lister les conteneurs
``` bash
podman ps
``` bash
podman ps -a
``` 

Améliorer la mise ne forme
``` bash
podman ps --format " {{ .Names }}" 
 nova_conductor
 nova_scheduler
 nova_vnc_proxy
 nova_api
 nova_metadata
 nova_api_cron
``` 
Selectionner uniquement certain conteneurs

``` bash
podman ps --filter "name=nova" --format "{{ .Names }}"
# nova_conductor
# nova_scheduler
# nova_vnc_proxy
# nova_api
# nova_metadata
# nova_api_cron
``` 
> équivalent à `podman ps --format " {{ .Names }}" | grep nova` 

---
## single et HA conteneurs
Configuration 

lancé en mode single : la commande `paunch`
Lancés en mode HA : utilise pacemaker, avec la commande `pcs`
    - status
    - start
    - stop

En mode HA : lancés par ansible playbook, et le nom contient bundle dans la plupart des cas : 
``` bash
podman ps --format "{{ .Names }} {{ .Status }}" | grep bundle
# galera-bundle-podman-0 Up 42 hours ago
# rabbitmq-bundle-podman-0 Up 42 hours ago
# redis-bundle-podman-0 Up 42 hours ago
# ovn-dbs-bundle-podman-0 Up 42 hours ago
# haproxy-bundle-podman-0 Up 41 hours ago
```

Logs d'un conteneur

podman logs keystone
podman history
podman info 

podman inspect
``` bash
podman inspect keystone

podman inspect  \
 --format "{{json .State.Healthcheck}}" | jq .

podman inspect keystone  --format "{{json .Config.CreateCommand}}" | jq .

```




---
## healthcheck

Voir le ststus du healtcheck sur un conteneur
``` bash
podman inspect keystone  --format "{{json .State.Healthcheck}}" | jq .
# {
#   "Status": "",
#   "FailingStreak": 0,
#   "Log": null
# }
```

Lancer manuellement la commande de healthcheck
``` bash
podman exec  keystone /openstack/healthcheck
# 300 192.168.174.50:35357 0.002669 seconds
```
Si besoin, retrouver la commande de healtcheck utilisée 
``` bash
podman inspect  \
 --format "{{json .State.Healthcheck}}" | jq .

# si pas de résultat, faire une recherche 
#  dans la  CreateCommand
#  normalement ans la partie config_data
podman inspect keystone  \
  --format "{{json .Config.CreateCommand}}" \
  | jq . \
  | jq 'map(select(. | contains("healthcheck")))'
# config_data={ ... , \"healthcheck\": {\"test\": \"/openstack/healthcheck\"}, ...
```

---
## Exporter le contenu d'un conteneur

Un conteneur, c'est avant tout des binaires lancées dans un espace système limité, depuis. leur File system retse un dossier
Pour exporte ce system de fichier: 

``` bash
podman export keystone -o keystone.tar
```


---
## Configuration d'un conteneur 


La configuration des containers est gérée par Kolla.

Kolla est un projet qui fourni 
- sources des images
- outils de déploiement

La configuration est gérée par une commande de bootstrap : `kolla_start`

- executé au moment de l'initialisation des services dans le conteneur
- copie la config depuis le host vers le conteneur
- format de la config
  - un fichier json : `/var/lib/kolla/config_files/<service>.json`
  - un dossier



fichier `/var/lib/kolla/config_files/<service>.json`
Informations sur les infos utilisés pour lancer les conteneurs : `

- la commande à lancer
- les fichiers (de configuration) à monter dans le conteneur
- les droits à appliquer
- ...

ex : pour keystone : 
``` bash
jq . /var/lib/kolla/config_files/keystone.json
```

Il est monté dans le conteneur, dans le chemin `/var/lib/kolla/config_files/config.json` (le nom du service est remplacé par 'config')
``` bash
podman inspect nova_scheduler --format \
 "{{json .HostConfig.Binds}}" | jq 'map(select(. | contains("config.json")))'
 ```



---
## Logs

podman logs  nova_scheduler

Les logs des conteneurs sont stockés dans le dossier local `/var/log/containers/<service>/<logfile>.log`

Pour identifier les fichiers de logs spécifiques à un conteneur : 
``` bash
podman inspect nova_scheduler \
  --format \
  "{{json .HostConfig.Binds}}" \
    | jq 'map(select(. | contains("/var/log")))'
# [
#   "/var/log/containers/nova:/var/log/nova:rw,rprivate,rbind"
# ]
```


Pour afficher les logs, on utilise tail
``` bash
tail -f /var/log/containers/nova/nova-compute.log
tail -f /var/log/containers/nova/*.log
```

Par défaut, le niveau de log est assez faible.
Pour le modifier, il faut modifier le fichier de configuration du service
- modifier le fichier dans `/var/lib/config-data/puppet-generated/<service>/<fichier>`. Par exemple pour nova_libvirt config :
``` bash
crudini --set \
  /var/lib/config-data/puppet-generated/nova_libvirt/etc/nova/nova.conf \
  DEFAULT debug True
```
- relancer le service
``` bash
systemctl restart tripleo_nova_compute
```

Les messages d'erreur sont signalé par CRITICAL, TRACE ou ERROR.

