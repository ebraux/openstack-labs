


The service `tripleo_glance_api_cron_healthcheck.service` is running command 
``` bash
/usr/bin/podman exec \
  --user root \
  glance_api_cron \
  /usr/share/openstack-tripleo-common/healthcheck/cron glance
```  
(code=exited, status=1/FAILURE)`/usr/share/openstack-tripleo-common/healthcheck/cron glance`
which return and FAILUER Exite code
`(code=exited, status=1/FAILURE)`

In the container glance_api_cron, the healthcheck is configured as
/usr/share/openstack-tripleo-common/healthcheck/cron glance

---
## Service status

``` bash
systemctl status tripleo_glance_api_cron_healthcheck.service
● tripleo_glance_api_cron_healthcheck.service - glance_api_cron healthcheck
   Loaded: loaded (/etc/systemd/system/tripleo_glance_api_cron_healthcheck.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Wed 2023-10-04 07:01:54 UTC; 26s ago
  Process: 449032 ExecStart=/usr/bin/podman exec --user root glance_api_cron /usr/share/openstack-tripleo-common/healthcheck/cron glance (code=exited, status=1/FAILURE)
```

Container glance_api_cron configuration
``` bash
podman inspect glance_api_cron --format "{{json .Config.CreateCommand}}" | jq . | jq 'map(select(. | contains("healthcheck")))'
[
  "config_data={\"environment\": {\"KOLLA_CONFIG_STRATEGY\": \"COPY_ALWAYS\", \"TRIPLEO_CONFIG_HASH\": \"1b11b7b41419b134d00a76c5bf462a27\"}, \"healthcheck\": {\"test\": \"/usr/share/openstack-tripleo-common/healthcheck/cron glance\"}, \"image\": \"rhos-director-br-01.ctlplane.overcloud-prod.com:8787/rhosp-rhel8/openstack-glance-api:16.2\", \"net\": \"host\", \"privileged\": false, \"restart\": \"always\", \"start_order\": 2, \"user\": \"root\", \"volumes\": [\"/etc/hosts:/etc/hosts:ro\", \"/etc/localtime:/etc/localtime:ro\", \"/etc/pki/ca-trust/extracted:/etc/pki/ca-trust/extracted:ro\", \"/etc/pki/ca-trust/source/anchors:/etc/pki/ca-trust/source/anchors:ro\", \"/etc/pki/tls/certs/ca-bundle.crt:/etc/pki/tls/certs/ca-bundle.crt:ro\", \"/etc/pki/tls/certs/ca-bundle.trust.crt:/etc/pki/tls/certs/ca-bundle.trust.crt:ro\", \"/etc/pki/tls/cert.pem:/etc/pki/tls/cert.pem:ro\", \"/dev/log:/dev/log\", \"/etc/puppet:/etc/puppet:ro\", \"/var/log/containers/glance:/var/log/glance:z\", \"/var/log/containers/httpd/glance:/var/log/httpd:z\", \"/var/lib/kolla/config_files/glance_api_cron.json:/var/lib/kolla/config_files/config.json\", \"/var/lib/config-data/puppet-generated/glance_api:/var/lib/kolla/config_files/src:ro\", \"/var/lib/glance:/var/lib/glance:shared\"]}"
]
# /usr/share/openstack-tripleo-common/healthcheck/cron glance
```

---
## Config for default `/openstack/healthcheck`

``` bash
podman exec   glance_api_cron ls -l /openstack/healthcheck 
#lrwxrwxrwx. 1 root root 58 Aug 31 17:28 /openstack/healthcheck -> /usr/share/openstack-tripleo-common/healthcheck/glance-api
```

``` bash
/openstack/healthcheck 
# 300 192.168.174.52:9292 0.001638 seconds
```
