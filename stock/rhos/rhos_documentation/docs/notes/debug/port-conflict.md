

2023-12-05 15:44:40.224042 | 52540032-1894-1c4a-44fa-000000000018 |       TASK | Provision instance network ports
2023-12-05 15:45:11.167709 | 52540032-1894-1c4a-44fa-000000000018 |      FATAL | Provision instance network ports |
 localhost | error={"changed": false,
  "error": "ConflictException: 409: Client Error for url: https://10.129.173.17:13696/v2.0/ports,
   IP address 192.168.174.106 already allocated in subnet 5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd",
    "msg": "Error managing network ports ConflictException: 409: Client Error for url: https://10.129.173.17:13696/v2.0/ports, IP address 192.168.174.106 already allocated in subnet 5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd",
    "node_port_map": {}, "success": false}

    (undercloud) [stack@rhos-director-br-01 scripts]$ openstack port list | grep 106
| 1b357659-74fc-4a08-add0-c9d8aeb0129d | rhos-comp-br-06-ctlplane       | e0:db:55:1d:15:b8 | ip_address='10.129.173.106', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74'  | ACTIVE |
(undercloud) [stack@rhos-director-br-01 scripts]$ openstack port list | grep rhos-comp-br-06
| 1b357659-74fc-4a08-add0-c9d8aeb0129d | rhos-comp-br-06-ctlplane       | e0:db:55:1d:15:b8 | ip_address='10.129.173.106', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74'  | ACTIVE |

openstack subnet list
+--------------------------------------+---------------------+--------------------------------------+------------------+
| ID                                   | Name                | Network                              | Subnet           |
+--------------------------------------+---------------------+--------------------------------------+------------------+
| 0154cf01-adf8-43d1-9173-bd6b3bd23685 | external_subnet     | a6dff2df-e527-4e5e-aa32-17bd286675dd | 10.129.176.0/22  |
| 5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd | internal_api_subnet | 2e878df8-1af4-475f-bf87-f2b9ed6eec86 | 192.168.174.0/24 |
| 5aea7b7f-a8c7-4e1b-930c-8cd30f458455 | storage_mgmt_subnet | 13f9c05d-8892-434b-9b08-d230606d6e34 | 192.168.177.0/24 |
| 97a932bd-6f78-44ce-9f84-3df31165ebea | fencing_subnet      | 9aa78d31-8d95-4c08-afa8-c408d0e42fe8 | 10.29.20.0/24    |
| 9b7867ff-0641-44e7-bf5c-e9c51b0969e9 | storage_subnet      | 0bf0496f-83fc-4609-bcb4-c5b0343eed58 | 192.168.176.0/24 |
| b2874002-0cb2-4480-9d62-432506b76b74 | ctlplane-subnet     | 4f0b7187-1e23-4e53-a7e7-10c7e9cb5aea | 10.129.173.0/24  |
| ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33 | tenant_subnet       | 47148849-fd1d-4ade-9eba-c4a175184c65 | 192.168.175.0/24 |
+--------------------------------------+---------------------+--------------------------------------+------------------+

openstack port list --network 2e878df8-1af4-475f-bf87-f2b9ed6eec86
+--------------------------------------+--------------------------------+-------------------+--------------------------------------------------------------------------------+--------+
| ID                                   | Name                           | MAC Address       | Fixed IP Addresses                                                             | Status |
+--------------------------------------+--------------------------------+-------------------+--------------------------------------------------------------------------------+--------+
| 0d0cf1e3-3658-4a71-84b2-fd64f475f9b0 | rhos-comp-ds-br-06_InternalApi | fa:16:3e:d9:0b:27 | ip_address='192.168.174.126', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 1072cee6-d8b0-401a-b137-1d0deacb5a54 | rhos-comp-br-08_InternalApi    | fa:16:3e:cd:94:7b | ip_address='192.168.174.108', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 15dc6554-b2d2-431e-9957-a531aedd437b | rhos-comp-br-04_InternalApi    | fa:16:3e:ba:51:3b | ip_address='192.168.174.104', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 2020fe84-da66-41fb-9db2-2bd8201a9de1 | rhos-comp-ds-br-03_InternalApi | fa:16:3e:67:82:54 | ip_address='192.168.174.123', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 256634e6-9f21-458f-b781-bc2732a687f1 | rhos-ctrl-br-01_InternalApi    | fa:16:3e:d6:7c:73 | ip_address='192.168.174.51', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd'  | DOWN   |
| 34924a33-ce56-4d9f-b075-b157610257d9 | rhos-comp-br-02_InternalApi    | fa:16:3e:97:68:06 | ip_address='192.168.174.102', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 353d40c4-6b55-4d29-b427-285044881192 | rhos-comp-ds-br-02_InternalApi | fa:16:3e:e6:9a:0f | ip_address='192.168.174.122', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 4a8373a6-70f7-4814-b3a2-5d6d4fb67611 | rhos-comp-br-07_InternalApi    | fa:16:3e:57:86:05 | ip_address='192.168.174.107', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 59b9340e-553a-46b7-afb2-edf86a3420ed | rhos-comp-ds-br-04_InternalApi | fa:16:3e:2b:8e:72 | ip_address='192.168.174.124', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 6aa6bf50-d03c-4cc9-a9d4-b444ba3892f0 | redis_virtual_ip               | fa:16:3e:60:c0:df | ip_address='192.168.174.187', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 8c60dfc3-20ff-4a1d-8c02-d74d1043f8cb | rhos-comp-br-03_InternalApi    | fa:16:3e:27:21:f1 | ip_address='192.168.174.103', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| 955e4453-726d-4dd8-95f6-1ca3db032574 | rhos-comp-br-05_InternalApi    | fa:16:3e:c7:55:f4 | ip_address='192.168.174.105', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| b4816f12-3e13-461c-a18a-53fa5233fb44 | rhos-comp-br-01_InternalApi    | fa:16:3e:2b:2d:39 | ip_address='192.168.174.101', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| ba48d5dc-e5b1-4073-b5dd-de542f847cb8 | rhos-ctrl-br-02_InternalApi    | fa:16:3e:28:b1:f0 | ip_address='192.168.174.52', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd'  | DOWN   |
| bee322d1-2298-4c28-b8e1-4aacf304344a | rhos-comp-br-10_InternalApi    | fa:16:3e:0d:cc:05 | ip_address='192.168.174.110', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| c06c3a08-cc07-48ea-92cd-c955cd7dedab | rhos-comp-br-09_InternalApi    | fa:16:3e:f3:a7:69 | ip_address='192.168.174.109', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| c7bfa75f-a940-49a3-b947-f1de85e793e9 | rhos-comp-ds-br-05_InternalApi | fa:16:3e:34:87:02 | ip_address='192.168.174.125', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| cb0694db-702b-4c41-8974-04192525ca70 | rhos-ctrl-br-03_InternalApi    | fa:16:3e:2d:48:af | ip_address='192.168.174.53', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd'  | DOWN   |
| e1c70d6a-0552-49d1-a21f-f25fcbc52734 | rhos-comp-ds-br-01_InternalApi | fa:16:3e:ca:8e:29 | ip_address='192.168.174.121', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| f6dc945b-e7eb-4ab3-bd16-a0cbe1955c24 | internal_api_virtual_ip        | fa:16:3e:cf:3b:0f | ip_address='192.168.174.9', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd'   | DOWN   |
+--------------------------------------+--------------------------------+-------------------+--------------------------------------------------------------------------------+--------+

(undercloud) [stack@rhos-director-br-01 scripts]$ openstack port list --network 2e878df8-1af4-475f-bf87-f2b9ed6eec86 | grep 106
(undercloud) [stack@rhos-director-br-01 scripts]$ 


