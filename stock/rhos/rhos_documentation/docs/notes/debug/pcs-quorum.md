---
## Si problème de quorum avec pacemaker 

Afficher les noeuds actifs du cluster

``` bash
systemctl status pacemaker
# ...
# ... redis-bundle-1 )   due to no quorum (blocked)
# ...
```

``` bash
corosync-quorumtool -s
# Quorum information
# ------------------
# Date:             Mon Oct 30 16:06:16 2023
# Quorum provider:  corosync_votequorum
# Nodes:            1
# Node ID:          2
# Ring ID:          2.11
# Quorate:          No

# Votequorum information
# ----------------------
# Expected votes:   3
# Highest expected: 3
# Total votes:      1
# Quorum:           2 Activity blocked
# Flags:            

# Membership information
# ----------------------
#     Nodeid      Votes Name
#          2          1 rhos-ctrl-br-1 (local)
```


``` bash
pcs cluster corosync
totem {
    version: 2
    cluster_name: tripleo_cluster
    transport: knet
    token: 10000
    crypto_cipher: aes256
    crypto_hash: sha256
}

nodelist {
    node {
        ring0_addr: 192.168.174.50
        name: rhos-ctrl-br-0
        nodeid: 1
    }

    node {
        ring0_addr: 192.168.174.51
        name: rhos-ctrl-br-1
        nodeid: 2
    }

    node {
        ring0_addr: 192.168.174.52
        name: rhos-ctrl-br-2
        nodeid: 3
    }
}

quorum {
    provider: corosync_votequorum
}

logging {
    to_logfile: yes
    logfile: /var/log/cluster/corosync.log
    to_syslog: yes
    timestamp: on
}
```

Et forcer l'éléction d'un noeud
``` bash
pcs quorum update wait_for_all=0 --skip-offline

corosync-quorumtool -v 3 -n 2
```
---
