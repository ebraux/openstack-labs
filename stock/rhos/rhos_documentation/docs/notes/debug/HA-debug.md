# debug du mode HA des composants

---
## fonctionner avec un seul noeud

I placer 2 server serveurs en standby
```bash
pcs node standby rhos-ctrl-br-0
pcs node standby rhos-ctrl-br-2
```

et modified le quorum
```bash
pcs quorum update wait_for_all=0 --skip-offline
corosync-quorumtool -v 3 -n 2
```

---
##
Utilisation de pacemaker

afficher le status
``` bash
pcs status
# Cluster name: tripleo_cluster
# Cluster Summary:
#   * Stack: corosync
#   * Current DC: rhos-ctrl-br-1 (version 2.0.5-9.el8_4.5-ba59be7122) - partition with quorum
#   * Last updated: Wed Oct  4 06:31:43 2023
#   * Last change:  Mon Oct  2 11:57:19 2023 by root via cibadmin on rhos-ctrl-br-0
#   * 15 nodes configured
#   * 47 resource instances configured

# Node List:
#   * Online: [ rhos-ctrl-br-0 rhos-ctrl-br-1 rhos-ctrl-br-2 ]
#   * GuestOnline: [ galera-bundle-0@rhos-ctrl-br-0 galera-bundle-1@rhos-ctrl-br-1 galera-bundle-2@rhos-ctrl-br-2 ovn-dbs-bundle-0@rhos-ctrl-br-0 ovn-dbs-bundle-1@rhos-ctrl-br-1 ovn-dbs-bundle-2@rhos-ctrl-br-2 rabbitmq-bundle-0@rhos-ctrl-br-0 rabbitmq-bundle-1@rhos-ctrl-br-1 rabbitmq-bundle-2@rhos-ctrl-br-2 redis-bundle-0@rhos-ctrl-br-0 redis-bundle-1@rhos-ctrl-br-1 redis-bundle-2@rhos-ctrl-br-2 ]

# Full List of Resources:
#   * ip-10.129.173.9	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-0
#   * ip-10.129.176.9	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-1
#   * ip-192.168.174.8	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-2
#   * ip-192.168.174.9	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-0
#   * ip-192.168.176.9	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-1
#   * ip-192.168.177.9	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-2
#   * Container bundle set: haproxy-bundle [cluster.common.tag/openstack-haproxy:pcmklatest]:
#     * haproxy-bundle-podman-0	(ocf::heartbeat:podman):	 Started rhos-ctrl-br-0
#     * haproxy-bundle-podman-1	(ocf::heartbeat:podman):	 Started rhos-ctrl-br-1
#     * haproxy-bundle-podman-2	(ocf::heartbeat:podman):	 Started rhos-ctrl-br-2
#   * Container bundle set: galera-bundle [cluster.common.tag/openstack-mariadb:pcmklatest]:
#     * galera-bundle-0	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-0
#     * galera-bundle-1	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-1
#     * galera-bundle-2	(ocf::heartbeat:galera):	 Master rhos-ctrl-br-2
#   * Container bundle set: rabbitmq-bundle [cluster.common.tag/openstack-rabbitmq:pcmklatest]:
#     * rabbitmq-bundle-0	(ocf::heartbeat:rabbitmq-cluster):	 Started rhos-ctrl-br-0
#     * rabbitmq-bundle-1	(ocf::heartbeat:rabbitmq-cluster):	 Started rhos-ctrl-br-1
#     * rabbitmq-bundle-2	(ocf::heartbeat:rabbitmq-cluster):	 Started rhos-ctrl-br-2
#   * Container bundle set: redis-bundle [cluster.common.tag/openstack-redis:pcmklatest]:
#     * redis-bundle-0	(ocf::heartbeat:redis):	 Master rhos-ctrl-br-0
#     * redis-bundle-1	(ocf::heartbeat:redis):	 Slave rhos-ctrl-br-1
#     * redis-bundle-2	(ocf::heartbeat:redis):	 Slave rhos-ctrl-br-2
#   * Container bundle set: ovn-dbs-bundle [cluster.common.tag/openstack-ovn-northd:pcmklatest]:
#     * ovn-dbs-bundle-0	(ocf::ovn:ovndb-servers):	 Master rhos-ctrl-br-0
#     * ovn-dbs-bundle-1	(ocf::ovn:ovndb-servers):	 Slave rhos-ctrl-br-1
#     * ovn-dbs-bundle-2	(ocf::ovn:ovndb-servers):	 Slave rhos-ctrl-br-2
#   * ip-192.168.174.7	(ocf::heartbeat:IPaddr2):	 Started rhos-ctrl-br-0
#   * Container bundle: openstack-cinder-volume [cluster.common.tag/openstack-cinder-volume:pcmklatest]:
#     * openstack-cinder-volume-podman-0	(ocf::heartbeat:podman):	 Started rhos-ctrl-br-1

# Failed Resource Actions:
#   * galera_monitor_10000 on galera-bundle-2 'not running' (7): call=187, status='complete', exitreason='Group mysql doesn't exist', last-rc-change='2023-10-02 11:19:49Z', queued=0ms, exec=0ms

# Daemon Status:
#   corosync: active/enabled
#   pacemaker: active/enabled
#   pcsd: active/enabled
```

Nom du cluster : tripleo_cluster
Etat du cluster et des services (node part of the cluster)

Bundle group , avec leur status.
 vérifier qu'ils sont bien 'Started'


---
## Action en erreur

Supprimer une action en erreur, qui a été traitée :
``` bash
pcs resource cleanup  galera-bundle
```
---
## Gérer un bundle spécifique

Obtenir la configuration

- image utilisée pour lancer 
- utilisateur
- logs path
- mapping

---
## relancer un service

``` bash
pcs resource restart  galera-bundle

# puis
pcs status

```


``` bash
pcs resource config galera-bundle
```

---
## config spécifique sur les controlleur en mode HA

Ils sont contrôlés pas pacemaker. Il ne doivent donc pas essayer de se relancer tout seuls

``` bash
podman inspect haproxy-bundle-podman-0  --format "{{json .HostConfig.RestartPolicy.MaximumRetryCount}}"
0
```

> c'ets le cas de tous les conteneurs, mais pour ceux là, c'est critique.


---
## Mettre un service en erreur


- Identifeir un conteiner rabbitmq
``` bash
podman ps --filter "name=rabbit"
# CONTAINER ID  IMAGE                                             COMMAND               CREATED       STATUS           PORTS   NAMES
# a61f33b79b4a  cluster.common.tag/openstack-rabbitmq:pcmklatest  /bin/bash /usr/lo...  45 hours ago  Up 45 hours ago          rabbitmq-bundle-podman-0
```
- Identifier l'id du processus pacemaker
``` bash
podman exec  rabbitmq-bundle-podman-0 \
  ps -efwww | grep pacemaker
# root           8       1  0 Oct02 ?        00:03:03 /usr/sbin/pacemaker_remoted
# ici l'ID ets le 8
```
- tuer le processus
``` bash
podman exec rabbitmq-bundle-podman-0 kill -9 8
```
- Vérifier le status dans pcs
``` bash
pcs status
# Failed Resource Actions:
#   * rabbitmq-bundle-0_monitor_30000 on rhos-ctrl-br-0 'error' (1): call=29, status='Error', exitreason='', last-rc-change='2023-10-04 08:24:16Z', queued=0ms, exec=0ms
```
- pcs va ensuite relancer le container (ça va très vite)
``` bash 
watch podman ps --filter "name=rabbit"
```
- purger les "actions"
``` bash
pcs resource cleanup   rabbitmq-bundle
pcs status
```


---
## pacemaker


- [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_high_availability_clusters/index](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_high_availability_clusters/index)


le service pacemaker

``` bash
systemctl status pcsd.service
```

Afficher l'état du cluster 
``` bash
 pcs cluster status
 ```
Arr^ter le service
``` bash
pcs cluster stop --all
```


Gérer les ressources 
``` bash
pcs resource list

pcs resource describe resourcetype

 pcs resource config resourcetype
```

Effacer les erreurs, et relancer une ressource
``` bash
pcs resource cleanup WebSite
```


When a resource is in unmanaged mode, the resource is still in the configuration but Pacemaker does not manage the resource.



 ---
 ## Fencing

 - [https://access.redhat.com/solutions/15575](https://access.redhat.com/solutions/15575)
 - [https://access.redhat.com/articles/2881341](https://access.redhat.com/articles/2881341)
 - How to configure fence_ipmilan with Red Hat Enterprise Linux High Availability Pacemaker Add On : [https://access.redhat.com/solutions/2271811](https://access.redhat.com/solutions/2271811)

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#doc-wrapper](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#doc-wrapperq)



Lister les agents disponibles : `pcs stonith list [filter]`
``` bash
pcs stonith list 
# fence_compute - Fence agent for the automatic resurrection of OpenStack compute instances
# fence_evacuate - Fence agent for the automatic resurrection of OpenStack compute instances
# fence_idrac - Fence agent for IPMI
# fence_ilo3 - Fence agent for IPMI
# fence_ilo4 - Fence agent for IPMI
# fence_ilo5 - Fence agent for IPMI
# fence_imm - Fence agent for IPMI
# fence_ipmilan - Fence agent for IPMI
# fence_ipmilanplus - Fence agent for IPMI
# fence_kdump - fencing agent for use with kdump crash recovery service
# fence_redfish - I/O Fencing agent for Redfish
# fence_rhevm - Fence agent for RHEV-M REST API
# fence_sbd - Fence agent for sbd
# fence_virt - Fence agent for virtual machines
# fence_watchdog - Dummy watchdog fence agent
# fence_xvm - Fence agent for virtual machines
```

Afficher les options disponibles pour un agent  (doc)
``` absh
pcs stonith describe [stonith_agent]
```


Afficher la config :
``` bash
pcs stonith status
#NO stonith devices configured
```

Tester les accès dans le cas d'un fence_ipmi :
``` bash
# fence_ipmilan -a <ilo/drac/imm_ip> -P -l <ilo/drac/imm_username> -p <ilo/drac/imm_password> -o status

fence_ipmilan -a 10.29.20.50 -P -l root -p xxxxxx -o status
# Status: OFF

fence_ipmilan -a 10.29.20.51 -P -l root -p xxxxxx -o status
# Status: ON

fence_ipmilan -a 10.29.20.52 -P -l root -p xxxxxx -o status
# Status: OFF

fence_ipmilan -a 10.29.20.53 -P -l root -p xxxxxx -o status
# Status: ON

fence_ipmilan -a 10.29.20.54 -P -l root -p xxxxxx -o status
# Status: ON
```

Vérifier les temps de réponse
``` bash
# time fence_ipmilan -a <ilo/drac/imm_ip> -P -l <ilo/drac/imm_username> -p <ilo/drac/imm_password> -o status

time fence_ipmilan -a 10.29.20.51 -P -l root -p xxxxxx -o status
# Status: ON
# real	0m0.330s
# user	0m0.124s
# sys	0m0.015s
```

Création des stonith device in pacemake
``` bash 
pcs stonith create ipmi-fence-rhos-ctrl-br-0 fence_ipmilan pcmk_host_list="rhos-ctrl-br-0" ip="10.29.20.50" username="root" password="xxxxxx" lanplus=1  power_wait=4

pcs stonith create ipmi-fence-rhos-ctrl-br-1 fence_ipmilan pcmk_host_list="rhos-ctrl-br-1" ip="10.29.20.51" username="root" password="xxxxxx" lanplus=1  power_wait=4

pcs stonith create ipmi-fence-rhos-ctrl-br-2 fence_ipmilan pcmk_host_list="rhos-ctrl-br-2" ip="10.29.20.52" username="root" password="xxxxxx" lanplus=1  power_wait=4
```

Vérifier la configuration
``` bash
pcs stonith status
  # * ipmi-fence-rhos-ctrl-br-1	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-2
  # * ipmi-fence-rhos-ctrl-br-0	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-0
  # * ipmi-fence-rhos-ctrl-br-2	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-1

pcs stonith config  ipmi-fence-rhos-ctrl-br-1
# Resource: ipmi-fence-rhos-ctrl-br-1 (class=stonith type=fence_ipmilan)
#   Attributes: ipmi-fence-rhos-ctrl-br-1-instance_attributes
#     ip=10.29.20.51
#     lanplus=1
#     password=xxxxxx
#     pcmk_host_list=rhos-ctrl-br-1
#     power_wait=4
#     username=root
#   Operations:
#     monitor: ipmi-fence-rhos-ctrl-br-1-monitor-interval-60s
#       interval=60s

```

---
## Getsion du Quorum

- [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_high_availability_clusters/assembly_configuring-cluster-quorum-configuring-and-managing-high-availability-clusters](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_high_availability_clusters/assembly_configuring-cluster-quorum-configuring-and-managing-high-availability-clusters)



---
## configuration

``` bash
crm_attribute --node rhos-ctrl-br-0 --query
# scope=nodes  name=haproxy-role value=true
# scope=nodes  name=galera-role value=true
# scope=nodes  name=rabbitmq-role value=true
# scope=nodes  name=rmq-node-attr-last-known-rabbitmq value=rabbit@rhos-ctrl-br-0.internalapi.overcloud-prod.com
# scope=nodes  name=redis-role value=true
# scope=nodes  name=ovn-dbs-role value=true
# scope=nodes  name=cinder-volume-role value=true
```
