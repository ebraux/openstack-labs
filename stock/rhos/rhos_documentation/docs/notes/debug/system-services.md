# Getsion des service ssystem

``` bash
systemctl list-units -t service triple*
```

``` bash
systemctl status tripleo_nova_metadata.service
# ● tripleo_nova_metadata.service - nova_metadata container
#    Loaded: loaded (/etc/systemd/system/tripleo_nova_metadata.service; enabled; vendor preset: disabled)
#    Active: active (running) since Mon 2023-10-02 11:20:01 UTC; 1 day 18h ago
#  Main PID: 154521 (conmon)
#     Tasks: 2 (limit: 820740)
#    Memory: 1.5M
#    CGroup: /system.slice/tripleo_nova_metadata.service
#            └─154521 /usr/bin/conmon --api-version 1 -c ed52e65d299259ec8255ae1efc0607004ce31b3a5ace0b9b9a26f52d7dca28ad -u ed52e65d299259ec8255ae1efc0607004ce31b3a5ace0b9b9a26f52d7dca28ad -r /usr/bin/run>

# Warning: Journal has been rotated since unit was started. Log output is incomplete or unavailable.

```