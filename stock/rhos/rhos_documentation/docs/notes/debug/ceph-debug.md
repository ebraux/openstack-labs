

vérifie rla config CEPH dans un fichier de conf
``` bash
grep -Ei 'rbd|ceph'  /etc/glance/glance-api.conf  | grep -v ^#
```

Liste les autorisationS
``` bash
ceph auth list
```

Lister les pool
``` bash 
ceph osd lspools
```

Lister le contenu d'un pool
 rados -p images
