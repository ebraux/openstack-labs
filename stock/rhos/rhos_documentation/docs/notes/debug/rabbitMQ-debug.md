# Debug de rabbitMq


Gestion de utilisateur, queues, ...

rabbitmqctl

Vérifier que RabbitMQ fonctionn

``` bash
[user@host]$ podman exec -ti \
 rabbitmq-bundle-podman-0 rabbitmqctl status
...output omitted...
{listeners,[{clustering,25672,"::"},{amqp,5672,"172.24.1.1"}]},
...output omitted...
```

Vérifier la config de RabbitMQ
``` bash
sudo pcs resource config rabbitmq-bundle
Bundle: rabbitmq-bundle
Podman: image=cluster.common.tag/gls ...
```


https://access.redhat.com/solutions/2113011

---
## Afficher le status du cluster

Commande `rabbitmqctl cluster_status` à lancer sur un des noeuds de control-plane :

- sur ctrl1
``` bash
ssh tripleo-admin@ctrl1 "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
```

- sur ctrl2
``` bash
ssh tripleo-admin@ctrl2 "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
```

- sur ctrl3
``` bash
ssh tripleo-admin@ctrl3 "sudo podman exec \$(sudo podman ps -f name=rabbitmq-bundle -q) rabbitmqctl cluster_status"
```

---
## Relance de RabbitMQ

Etat intial
``` bash
  * Container bundle set: rabbitmq-bundle [cluster.common.tag/rabbitmq:pcmklatest]:
    * rabbitmq-bundle-0	(ocf:heartbeat:rabbitmq-cluster):	 Stopped
    * rabbitmq-bundle-1	(ocf:heartbeat:rabbitmq-cluster):	 Stopped rhos-ctrl-br-01
```

Relance du service avec pcs
``` bash
sudo pcs resource cleanup  rabbitmq-bundle
```

Etat après relance
``` bash
  * Container bundle set: rabbitmq-bundle [cluster.common.tag/rabbitmq:pcmklatest]:
    * rabbitmq-bundle-0	(ocf:heartbeat:rabbitmq-cluster):	 Started rhos-ctrl-br-02
    * rabbitmq-bundle-1	(ocf:heartbeat:rabbitmq-cluster):	 Started rhos-ctrl-br-01
```