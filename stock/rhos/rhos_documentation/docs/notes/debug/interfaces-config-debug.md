

config réseau de controller0
---
## Les interafaces
Lister les interfaces
``` bash
sudo nmcli con show
NAME                UUID                                  TYPE      DEVICE 
Wired connection 1  1b633747-5b97-39ea-96b2-898a75dfd964  ethernet  --     
Wired connection 2  9b6ae4d8-efe0-3ce7-b4d5-0e974836e4a1  ethernet  --     
Wired connection 3  a557f9b1-f6b4-3f08-a7af-76daed663e4d  ethernet  --     
Wired connection 4  a5d125c1-ebb0-3f48-9a39-d07f8e3c0aa7  ethernet  --     
Wired connection 5  f5634663-8bd4-313b-b116-1d4728403138  ethernet  --     
```

Pour voir les devices
``` bash
nmcli device show
```


``` bash
[heat-admin@controller0 ~]$ ip a

# interface carte fille : NIC1 sur réseau de provisioning
2: ens10f0np0: UP
    link/ether 00:62:0b:ac:48:b4 brd ff:ff:ff:ff:ff:ff
    inet 10.129.173.50/24 brd 10.129.173.255 scope global dynamic ens10f0np0

# interface carte fille : NIC2 non connectée
3: ens10f1np1: DOWN
    link/ether 00:62:0b:ac:48:b5 brd ff:ff:ff:ff:ff:ff

# interface carte Additionelle#1 : NIC1 devrait être pour br-ex
4: ens1f0: UP
    link/ether 30:3e:a7:02:ba:3a brd ff:ff:ff:ff:ff:ff

# interface carte Additionelle#1 : NIC2 devrait être pour br-trunk
5: ens1f1: UP
    link/ether 30:3e:a7:02:ba:3b brd ff:ff:ff:ff:ff:ff

# interface carte Additionelle#2: NIC1 non connectée
6: ens2f0: DOWN
    link/ether 30:3e:a7:02:ba:2a brd ff:ff:ff:ff:ff:ff

# interface carte Additionelle#2: NIC2 non connectée
7: ens2f1: DOWN
    link/ether 30:3e:a7:02:ba:2b brd ff:ff:ff:ff:ff:ff

```

---
## Les bridges
Lister les bridges : Il devrait y avoir br-ex, et br-trunk
``` bash
sudo ovs-vsctl list-br
```



---
## Les services

``` bash
sudo systemctl status network
● network.service - LSB: Bring up/down networking
   Loaded: loaded (/etc/rc.d/init.d/network; generated)
   Active: active (exited) since Thu 2023-09-28 23:06:40 EDT; 3h 32min ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 0 (limit: 820740)
   Memory: 0B
   CGroup: /system.slice/network.service

Sep 28 23:06:40 controller0 systemd[1]: Starting LSB: Bring up/down networking...
Sep 28 23:06:40 controller0 network[2566]: WARN      : [network] You are using 'network' service provided by 'network-scripts', which are now deprecated.
Sep 28 23:06:40 controller0 network[2566]: WARN      : [network] 'network-scripts' will be removed in one of the next major releases of RHEL.
Sep 28 23:06:40 controller0 network[2566]: WARN      : [network] It is advised to switch to 'NetworkManager' instead for network management.
Sep 28 23:06:40 controller0 network[2566]: Bringing up loopback interface:  [  OK  ]
Sep 28 23:06:40 controller0 network[2566]: Bringing up interface ens10f0np0:  [  OK  ]
Sep 28 23:06:40 controller0 systemd[1]: Started LSB: Bring up/down networking.
```

``` bash
sudo systemctl status NetworkManager
● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2023-09-28 23:05:40 EDT; 3h 34min ago
     Docs: man:NetworkManager(8)
 Main PID: 2325 (NetworkManager)
    Tasks: 3 (limit: 820740)
   Memory: 9.5M
   CGroup: /system.slice/NetworkManager.service
           └─2325 /usr/sbin/NetworkManager --no-daemon

Sep 28 23:36:01 controller0 NetworkManager[2325]: <info>  [1695958561.0529] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens10f0np0" pid=23418 uid=0 result="success"
Sep 28 23:36:01 controller0 NetworkManager[2325]: <info>  [1695958561.0803] device (ens10f0np0): carrier: link connected
Sep 28 23:36:03 controller0 NetworkManager[2325]: <info>  [1695958563.3009] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens10f0np0" pid=23488 uid=0 result="success"
Sep 28 23:36:03 controller0 NetworkManager[2325]: <info>  [1695958563.3300] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens10f0np0" pid=23509 uid=0 result="success"
Sep 28 23:36:03 controller0 NetworkManager[2325]: <info>  [1695958563.3727] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens1f0" pid=23548 uid=0 result="success"
Sep 28 23:36:03 controller0 NetworkManager[2325]: <info>  [1695958563.3868] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens1f0" pid=23559 uid=0 result="success"
Sep 28 23:36:03 controller0 NetworkManager[2325]: <info>  [1695958563.4578] device (ens1f0): carrier: link connected
Sep 28 23:37:04 controller0 NetworkManager[2325]: <info>  [1695958624.6935] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens1f1" pid=23708 uid=0 result="success"
Sep 28 23:37:04 controller0 NetworkManager[2325]: <info>  [1695958624.7073] audit: op="connections-load" args="/etc/sysconfig/network-scripts/ifcfg-ens1f1" pid=23719 uid=0 result="success"
Sep 28 23:37:04 controller0 NetworkManager[2325]: <info>  [1695958624.7874] device (ens1f1): carrier: link connected
```

---
## net-config

 cat /etc/os-net-config/config.json | jq -s add > net-config.json

 Dans le config.json, on retrouve bien la définition :

 - Interface :
   - nic1
 - OVS Bridge br-ex :
   - bond1 en mode active-backup,
   - nic2
   - Vlan 178 et l'IP qui correspond
 - OVX Bridge br-trunk
   - bond2 en mode active-backup,
   - nic3
   - Vlans 174, 175, 176 et les IP qui correspondnet


Active la création des bridges, mais il ne sont pas rattachés à des interfaces
``` bash
sudo ovs-vsctl list-br
br-ex
br-trunk
```

``` bash
 vs-vsctl show
# a29f6843-d917-4ad1-b881-8f6b942d7570
#     Bridge br-trunk
#         fail_mode: standalone
#         Port br-trunk
#             Interface br-trunk
#                 type: internal
#     Bridge br-ex
#         fail_mode: standalone
#         Port br-ex
#             Interface br-ex
#                 type: internal
#     ovs_version: "2.15.8"```
```

Commandes lancées par Director
``` bash```
os-net-config -c /etc/os-net-config/dhcp_all_interfaces.yaml -v --detailed-exit-codes --cleanup
```

lo is not an active nic",
        "[2023/09/29 05:45:15 AM] [INFO] eno3 is an embedded active nic",
        "[2023/09/29 05:45:15 AM] [INFO] eno1 is an embedded active nic",
        "[2023/09/29 05:45:15 AM] [INFO] ens1f0 is not an active nic",
        "[2023/09/29 05:45:15 AM] [INFO] eno4 is not an active nic",
        "[2023/09/29 05:45:15 AM] [INFO] eno2 is an embedded active nic",
        "[2023/09/29 05:45:15 AM] [INFO] No DPDK mapping available in path (/var/lib/os-net-config/dpdk_mapping.yaml)",
        "[2023/09/29 05:45:15 AM] [INFO] Active nics are ['eno1', 'eno2', 'eno3']",
        "[2023/09/29 05:45:15 AM] [INFO] nic3 mapped to: eno3",
        "[2023/09/29 05:45:15 AM] [INFO] nic1 mapped to: eno1",
        "[2023/09/29 05:45:15 AM] [INFO] nic2 mapped to: eno2",
        "[2023/09/29 05:45:15 AM] [INFO] adding interface: eno1",
        "[2023/09/29 05:45:15 AM] [INFO] adding custom route for interface: eno1",
        "[2023/09/29 05:45:15 AM] [INFO] adding bridge: br-ex",
        "[2023/09/29 05:45:15 AM] [INFO] adding bond: bond1",
        "[2023/09/29 05:45:15 AM] [INFO] adding interface: eno2",
        "[2023/09/29 05:45:15 AM] [INFO] adding interface: nic4",
        "[2023/09/29 05:45:15 AM] [INFO] adding vlan: vlan178",
        "[2023/09/29 05:45:15 AM] [INFO] adding custom route for interface: vlan178",
        "[2023/09/29 05:45:15 AM] [INFO] adding bridge: br-trunk",
 
[ERROR] stdout: ERROR     : [/etc/sysconfig/network-scripts/ifup-eth] Device nic4 does not seem to be present, delaying initialization."

---
## config "network-scripts"

/etc/sysconfig/network-scripts

ifcfg-vlan178
``` bash
# This file is autogenerated by os-net-config
DEVICE=vlan178
ONBOOT=yes
HOTPLUG=no
NM_CONTROLLED=no
PEERDNS=no
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=br-ex
OVS_OPTIONS="tag=178"
MTU=1500
BOOTPROTO=static
IPADDR=10.129.176.50
NETMASK=255.255.252.0
```

br-ex
``` bash
# This file is autogenerated by os-net-config
DEVICE=br-ex
ONBOOT=yes
HOTPLUG=no
NM_CONTROLLED=no
DEVICETYPE=ovs
TYPE=OVSBridge
OVS_EXTRA="set bridge br-ex fail_mode=standalone -- del-controller br-ex"
DNS1=10.129.173.14
```

ifcfg-bond1
``` bash
# This file is autogenerated by os-net-config
DEVICE=bond1
ONBOOT=yes
HOTPLUG=no
NM_CONTROLLED=no
PEERDNS=no
DEVICETYPE=ovs
TYPE=OVSPort
OVS_BRIDGE=br-ex
DEVICETYPE=ovs
TYPE=OVSBond
BOND_IFACES="ens1f1"
OVS_OPTIONS="bond_mode=active-backup"
MTU=1500
```
ifcfg-ens1f1
``` bash
# This file is autogenerated by os-net-config
DEVICE=ens1f1
ONBOOT=yes
HOTPLUG=no
NM_CONTROLLED=no
PEERDNS=no
BOOTPROTO=none
MTU=1500
```
ifdown/ifup ifcfg-ens1f1
``` bash
ip a show ens1f1
5: ens1f1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 30:3e:a7:02:ba:3b brd ff:ff:ff:ff:ff:ff
    inet6 fe80::323e:a7ff:fe02:ba3b/64 scope link 
       valid_lft forever preferred_lft forever
```

ifdown/ifup : ifcfg-bond1
ovs-vsctl: add-bond requires at least 2 interfaces, but only 1 were specified

### Test En ajoutant une interfarec, m^me down au bond, il se lance :

- dupliquer manuellement ifcfg-ens1f1 en ifcfg-ens2f1, modifier le nom dans le fichier, et lançant l'interface,
- ajouter ens2f1 au bond `BOND_IFACES="ens1f1 ens2f1"`, et relancer le bond.
- 

``` bash
ovs-vsctl show
a29f6843-d917-4ad1-b881-8f6b942d7570
    Bridge br-trunk
        fail_mode: standalone
        Port br-trunk
            Interface br-trunk
                type: internal
    Bridge br-ex
        fail_mode: standalone
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal
        Port br-ex
            Interface br-ex
                type: internal
    ovs_version: "2.15.8"
```

``` bash
ip a

5: ens1f1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 30:3e:a7:02:ba:3b brd ff:ff:ff:ff:ff:ff


7: ens2f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 30:3e:a7:02:ba:2b brd ff:ff:ff:ff:ff:ff


27: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 32:ba:b4:77:1f:46 brd ff:ff:ff:ff:ff:ff

28: vlan178: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether f2:9b:ff:c8:7c:3a brd ff:ff:ff:ff:ff:ff
    inet 10.129.176.50/22 brd 10.129.179.255 scope global vlan178

```

### Test avec os-config

``` bash
sudo ls -l /etc/sysconfig/network-scripts/
-rw-r--r--. 1 root root   112 Sep 28 23:36 ifcfg-eno1
-rw-r--r--. 1 root root   112 Sep 28 23:36 ifcfg-eno2
-rw-r--r--. 1 root root   112 Sep 28 23:36 ifcfg-eno3
-rw-r--r--. 1 root root   254 Apr 12 08:33 ifcfg-lo
```

``` bash
sudo cp /etc/os-net-config/config.json /etc/os-net-config/config.json.ORI
jq . /etc/os-net-config/config.json | sudo vi -

```
Ajout de l'interface nic4 dans net-config.json
``` json
          "members": [
            {
              "mtu": 1500,
              "name": "nic2",
              "primary": true,
              "type": "interface"
            },
            {
              "mtu": 1500,
              "name": "nic4",
              "primary": false,
              "type": "interface"
            }
```

ovs-appctl bond/show bond1

Et pour appliquer la config 
``` bash
sudo os-net-config -c /etc/os-net-config/config.json -vvv
```


ça crée une interface, mais qui s'apelle nic4, et donc pas de correspondance sur le système....
Comment gérer la correspondance ? 
Pour afficher le mapping des  interfaces avec os-net-config
``` bash
os-net-config -i
```
Pour modifier le mapping, if faut crée un fichier mapping.yaml

[https://docs.openstack.org/os-net-config/latest/usage.html#interface-mapping](https://docs.openstack.org/os-net-config/latest/usage.html#interface-mapping)

ERROR     : [/etc/sysconfig/network-scripts/ifup-eth] Device nic4 does not seem to be present, delaying initialization.
en corrigeant en eno4, ça marche.


test avec un fichier 
```bash
interface_mapping:
  nic1: e4:43:4b:ea:b2:b0
  nic2: e4:43:4b:ea:b2:b1
  nic3: e4:43:4b:ea:b2:b2
  nic4: f8:f2:1e:aa:80:e0
  nic5: f8:f2:1e:aa:80:e1
```
On obtient bien 
``` bash
os-net-config -i |jq
# {
#   "nic1": "eno1",
#   "nic2": "eno2",
#   "nic3": "eno3",
#   "nic4": "ens1f0",
#   "nic5": "ens1f1"
# }
```
et si on réaplique, là c'ets bon
``` bash
[root@compute0 os-net-config]# ovs-vsctl show
e826b3b4-d7ac-4a88-85a8-4919c128b60c
    Bridge br-ex
        fail_mode: standalone
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal
        Port bond1
            Interface ens1f0
            Interface eno2
        Port br-ex
            Interface br-ex
                type: internal
    Bridge br-trunk
        fail_mode: standalone
        Port vlan174
            tag: 174
            Interface vlan174
                type: internal
        Port br-trunk
            Interface br-trunk
                type: internal
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal
        Port bond2
            Interface ens1f1
            Interface eno3
        Port vlan176
            tag: 176
            Interface vlan176
                type: internal
    ovs_version: "2.15.8"
```
https://opendev.org/openstack/os-net-config/src/branch/master/etc/os-net-config/samples/mapping.yaml
après un `sudo systemctl restart network`
 on  a : 

 ``` bash
 [heat-admin@compute0 ~]$ sudo ovs-vsctl show
e350968e-48e8-4985-ac6c-ca04f22b24a1
    Bridge br-trunk
        fail_mode: standalone
        Port br-trunk
            Interface br-trunk
                type: internal
        Port vlan176
            tag: 176
            Interface vlan176
                type: internal
        Port vlan174
            tag: 174
            Interface vlan174
                type: internal
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal
    Bridge br-ex
        fail_mode: standalone
        Port bond1
            Interface eno2
            Interface eno4
        Port br-ex
            Interface br-ex
                type: internal
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal
    ovs_version: "2.15.8"
```

``` bash
[heat-admin@controller0 ~]$ sudo ovs-vsctl show
a29f6843-d917-4ad1-b881-8f6b942d7570
    Bridge br-ex
        fail_mode: standalone
        Port br-ex
            Interface br-ex
                type: internal
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal
        Port bond1
            Interface ens1f1
            Interface ens2f1
    Bridge br-trunk
        fail_mode: standalone
        Port vlan177
            tag: 177
            Interface vlan177
                type: internal
        Port vlan176
            tag: 176
            Interface vlan176
                type: internal
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal
        Port br-trunk
            Interface br-trunk
                type: internal
        Port vlan174
            tag: 174
            Interface vlan174
                type: internal
    ovs_version: "2.15.8"
```

br-ex est actif, et le ping fonctionne entre les IP du Vlan 178



---
##
- [https://access.redhat.com/solutions/3087741?band=se&seSessionId=c95b3b97-33ff-42df-ba67-b2df8de33d92&seSource=Recommendation%20Aside&seResourceOriginID=27de89c7-bb7f-40ba-9ead-830bdc6bf66b](https://access.redhat.com/solutions/3087741?band=se&seSessionId=c95b3b97-33ff-42df-ba67-b2df8de33d92&seSource=Recommendation%20Aside&seResourceOriginID=27de89c7-bb7f-40ba-9ead-830bdc6bf66b)


os-net-config mapping.yaml will only map active interfaces, will not map NO CARRIER interfaces in Red Hat OpenStack Platform
- [https://access.redhat.com/solutions/3087741](https://access.redhat.com/solutions/3087741)

Mapping interfaces to aliases with NetConfigDataLookup not working as expected
-[https://access.redhat.com/solutions/5975981](https://access.redhat.com/solutions/5975981)