

set 

and use name mapping.

Add a new host, by creating an entry, and  increase the count variable.

If `provisioned: false`


``` yaml
  - hostname: rhos-comp-ds-br-06
    name: rhos-comp-ds-br-06
    provisioned: false
    resource_class: baremetal
    networks:
    - network: ctlplane
      vif: true
      fixed_ip: 10.129.173.126
    - network: internal_api
      subnet: internal_api_subnet
      fixed_ip: 192.168.174.126
    - network: tenant
      subnet: tenant_subnet
      fixed_ip: 192.168.175.126
    - network: storage
      subnet: storage_subnet
      fixed_ip: 192.168.176.126
    - network: external
      subnet: external_subnet
      fixed_ip: 10.129.176.46
    network_config:
      net_config_data_lookup:
        rhos-comp-ds-br-06:
          nic1: "e4:43:4b:ea:c3:60"
          nic2: "e4:43:4b:ea:c3:61"
          nic3: "e4:43:4b:ea:c3:62"
          nic4: "f8:f2:1e:aa:6d:c0"
          nic5: "f8:f2:1e:aa:6d:c1"

```

``` bash
[stack@rhos-director-br-01 ~]$ source stackrc 
(undercloud) [stack@rhos-director-br-01 ~]$ openstack baremetal node list
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name               | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
| 8d9a56fa-156b-4e93-8b50-24b0f9aa8362 | rhos-comp-br-09    | a8e48e27-a823-429f-9fb3-c9f784cec015 | power on    | active             | False       |
| ace3efdb-3f59-4d3a-a163-fb7ab2087901 | rhos-comp-ds-br-02 | 50c6dcff-8941-4c89-bed6-c04f6b8afc26 | power on    | active             | False       |
| 1ee4d0ae-a94f-4910-9db5-74b024376062 | rhos-comp-ds-br-01 | 43d8a776-5cfb-4a81-91ee-dea2aab1420f | power on    | active             | False       |
| a7c053eb-77a3-42e1-b8c4-63cebb1382e8 | rhos-ctrl-br-02    | bcfac8b7-3e69-4c37-a04a-a074233ff93a | power on    | active             | False       |
| a087c418-7b24-425b-b5e9-8acd49c320ca | rhos-ctrl-br-03    | 9ff5c389-4235-47d2-bdf2-924dc978af07 | power on    | active             | False       |
| a7b918d0-492f-44c1-b6a7-9a8d5a2ce7d8 | rhos-ctrl-br-01    | 528eff38-060c-48fe-9b87-9784ff05f5e6 | power on    | active             | False       |
| e1e081eb-a23b-4048-8904-209a0cd0fd0a | rhos-comp-br-02    | 4dc4a52b-80dc-4c33-8e82-61c261f7d7cb | power on    | active             | False       |
| 622ffd24-03d5-49ee-9160-6dd14b2c8915 | rhos-comp-br-03    | 98fc4b42-5ff6-4f3c-94f2-49efdb9b8991 | power on    | active             | False       |
| e3c458b3-2b49-4f47-9824-78c74171d1bb | rhos-comp-br-04    | d34d6dc1-f627-4696-becc-9f6f8d39a38e | power on    | active             | False       |
| 783831e8-ba46-40ed-9b89-53740cf54946 | rhos-comp-br-05    | 59ff9bfd-c0c4-4eab-bd17-4c455e21a08b | power on    | active             | False       |
| c4c64f84-1705-4542-9e6e-73942e039136 | rhos-comp-ds-br-03 | 0189da25-e2bf-4bb7-b567-0397cffe9d07 | power on    | active             | False       |
| 10048b86-bb49-4ecd-926c-10c33e922f66 | rhos-comp-ds-br-05 | f462dcd5-63ba-4b6b-8301-b8bbd5c3192e | power on    | active             | False       |
| 56401303-a266-402c-800a-4eb63ba96512 | rhos-comp-br-08    | 86a2a3ad-9fad-496a-b1b3-e2785765a7f8 | power on    | active             | False       |
| 5146726e-cf3a-4631-8adf-bac51552927b | rhos-comp-br-01    | 8ed7e5c1-746b-4379-878e-f3c2c1d7f79a | power on    | active             | False       |
| 9854406a-5d4a-430f-8947-6176489b0ab1 | rhos-comp-br-07    | 8b2bf118-db01-4832-ae0e-1d5ebb60e959 | power on    | active             | False       |
| b6ca61f5-4c79-4118-a495-9ade4944f0aa | rhos-comp-ds-br-04 | a7860a39-bfe8-4917-9f5a-d575f93ead6f | power on    | active             | False       |
| 692be1e1-3fce-4210-8cdd-9fe7079fbfea | rhos-comp-br-10    | 3ba32388-1026-47d9-9903-fd742da77866 | power on    | active             | False       |
| dcc2c554-eca0-4066-aa0a-9ec4236b68fe | rhos-comp-br-11    | 2d4d21c4-8add-4642-a519-25d940633f04 | power on    | active             | False       |
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | power on    | active             | False       |
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
```

``` bash
metalsmith list
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
| UUID                                 | Node Name          | Allocation UUID                      | Hostname               | State  | IP Addresses            |
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
| 8d9a56fa-156b-4e93-8b50-24b0f9aa8362 | rhos-comp-br-09    | a8e48e27-a823-429f-9fb3-c9f784cec015 | rhos-comp-br-09        | ACTIVE | ctlplane=10.129.173.109 |
| ace3efdb-3f59-4d3a-a163-fb7ab2087901 | rhos-comp-ds-br-02 | 50c6dcff-8941-4c89-bed6-c04f6b8afc26 | rhos-comp-ds-br-02     | ACTIVE | ctlplane=10.129.173.122 |
| 1ee4d0ae-a94f-4910-9db5-74b024376062 | rhos-comp-ds-br-01 | 43d8a776-5cfb-4a81-91ee-dea2aab1420f | rhos-comp-ds-br-01     | ACTIVE | ctlplane=10.129.173.121 |
| a7c053eb-77a3-42e1-b8c4-63cebb1382e8 | rhos-ctrl-br-02    | bcfac8b7-3e69-4c37-a04a-a074233ff93a | rhos-ctrl-br-02        | ACTIVE | ctlplane=10.129.173.52  |
| a087c418-7b24-425b-b5e9-8acd49c320ca | rhos-ctrl-br-03    | 9ff5c389-4235-47d2-bdf2-924dc978af07 | rhos-ctrl-br-03        | ACTIVE | ctlplane=10.129.173.53  |
| a7b918d0-492f-44c1-b6a7-9a8d5a2ce7d8 | rhos-ctrl-br-01    | 528eff38-060c-48fe-9b87-9784ff05f5e6 | rhos-ctrl-br-01        | ACTIVE | ctlplane=10.129.173.51  |
| e1e081eb-a23b-4048-8904-209a0cd0fd0a | rhos-comp-br-02    | 4dc4a52b-80dc-4c33-8e82-61c261f7d7cb | rhos-comp-br-02        | ACTIVE | ctlplane=10.129.173.102 |
| 622ffd24-03d5-49ee-9160-6dd14b2c8915 | rhos-comp-br-03    | 98fc4b42-5ff6-4f3c-94f2-49efdb9b8991 | rhos-comp-br-03        | ACTIVE | ctlplane=10.129.173.103 |
| e3c458b3-2b49-4f47-9824-78c74171d1bb | rhos-comp-br-04    | d34d6dc1-f627-4696-becc-9f6f8d39a38e | rhos-comp-br-04        | ACTIVE | ctlplane=10.129.173.104 |
| 783831e8-ba46-40ed-9b89-53740cf54946 | rhos-comp-br-05    | 59ff9bfd-c0c4-4eab-bd17-4c455e21a08b | rhos-comp-br-05        | ACTIVE | ctlplane=10.129.173.105 |
| c4c64f84-1705-4542-9e6e-73942e039136 | rhos-comp-ds-br-03 | 0189da25-e2bf-4bb7-b567-0397cffe9d07 | rhos-comp-ds-br-03     | ACTIVE | ctlplane=10.129.173.123 |
| 10048b86-bb49-4ecd-926c-10c33e922f66 | rhos-comp-ds-br-05 | f462dcd5-63ba-4b6b-8301-b8bbd5c3192e | rhos-comp-ds-br-05     | ACTIVE | ctlplane=10.129.173.125 |
| 56401303-a266-402c-800a-4eb63ba96512 | rhos-comp-br-08    | 86a2a3ad-9fad-496a-b1b3-e2785765a7f8 | rhos-comp-br-08        | ACTIVE | ctlplane=10.129.173.108 |
| 5146726e-cf3a-4631-8adf-bac51552927b | rhos-comp-br-01    | 8ed7e5c1-746b-4379-878e-f3c2c1d7f79a | rhos-comp-br-01        | ACTIVE | ctlplane=10.129.173.101 |
| 9854406a-5d4a-430f-8947-6176489b0ab1 | rhos-comp-br-07    | 8b2bf118-db01-4832-ae0e-1d5ebb60e959 | rhos-comp-br-07        | ACTIVE | ctlplane=10.129.173.107 |
| b6ca61f5-4c79-4118-a495-9ade4944f0aa | rhos-comp-ds-br-04 | a7860a39-bfe8-4917-9f5a-d575f93ead6f | rhos-comp-ds-br-04     | ACTIVE | ctlplane=10.129.173.124 |
| 692be1e1-3fce-4210-8cdd-9fe7079fbfea | rhos-comp-br-10    | 3ba32388-1026-47d9-9903-fd742da77866 | rhos-comp-br-10        | ACTIVE | ctlplane=10.129.173.110 |
| dcc2c554-eca0-4066-aa0a-9ec4236b68fe | rhos-comp-br-11    | 2d4d21c4-8add-4642-a519-25d940633f04 | rhos-comp-br-11        | ACTIVE | ctlplane=10.129.173.111 |
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | overcloud-compute-ds-6 | ACTIVE | ctlplane=10.129.173.145 |
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
```

``` bash
 openstack  port list | grep overcloud-compute-ds-6
# | 14c440fe-bf1b-4b1e-8a08-36d9b74ddf59 | overcloud-compute-ds-6_External           | fa:16:3e:62:af:bd | ip_address='10.129.177.147', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'  | DOWN   |
# | a2ce7d56-9c25-4e9f-9798-c0fef6c9fcb8 | overcloud-compute-ds-6_Tenant             | fa:16:3e:ed:98:c9 | ip_address='192.168.175.112', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
# | c00a915b-cc51-4021-9b2b-c750bae8f647 | overcloud-compute-ds-6_InternalApi        | fa:16:3e:2b:d8:4b | ip_address='192.168.174.198', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
# | da0606bf-dec4-4939-8266-f98c9467f6de | overcloud-compute-ds-6_Storage            | fa:16:3e:cd:4a:d5 | ip_address='192.168.176.147', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
| f5a8aa52-3ca4-488e-9f80-5613ef56ac10 | overcloud-compute-ds-6-ctlplane           | e4:43:4b:ea:c3:60 | ip_address='10.129.173.145', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74'  | ACTIVE |
```

ssh tripleo-admin@10.129.173.145


hostname -f
overcloud-compute-ds-6


``` bash
[root@overcloud-compute-ds-6 ~]# ip a
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#     inet 127.0.0.1/8 scope host lo
#        valid_lft forever preferred_lft forever
#     inet6 ::1/128 scope host 
#        valid_lft forever preferred_lft forever
# 2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
#     link/ether e4:43:4b:ea:c3:60 brd ff:ff:ff:ff:ff:ff
#     altname enp25s0f0
#     inet 10.129.173.145/24 brd 10.129.173.255 scope global dynamic eno1
#        valid_lft 85831sec preferred_lft 85831sec
#     inet6 fe80::e643:4bff:feea:c360/64 scope link 
#        valid_lft forever preferred_lft forever
# 3: eno2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
#     link/ether e4:43:4b:ea:c3:61 brd ff:ff:ff:ff:ff:ff
#     altname enp25s0f1
#     inet6 fe80::e643:4bff:feea:c361/64 scope link 
#        valid_lft forever preferred_lft forever
# 4: eno3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
#     link/ether e4:43:4b:ea:c3:62 brd ff:ff:ff:ff:ff:ff
#     altname enp25s0f2
#     inet6 fe80::e643:4bff:feea:c362/64 scope link 
#        valid_lft forever preferred_lft forever
# 5: eno4: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
#     link/ether e4:43:4b:ea:c3:63 brd ff:ff:ff:ff:ff:ff
#     altname enp25s0f3
# 6: ens1f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
#     link/ether f8:f2:1e:aa:6d:c0 brd ff:ff:ff:ff:ff:ff
#     altname enp59s0f0
# 7: ens1f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
#     link/ether f8:f2:1e:aa:6d:c1 brd ff:ff:ff:ff:ff:ff
#     altname enp59s0f1
```


---
# Tets remove

Decrease count, and let  `provisioned: false`
- le provisioning plante
- mais rien ne chnage dan sla config des host et des ports
  

failed :
``` bash
2023-12-04 18:12:20.371965 | 52540032-1894-9e8e-be34-0000000005b6 |       TASK | Render network_config from template
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: ansible.errors.AnsibleUndefinedVariable: 'neutron_physical_bridge_name' is undefined. 'neutron_physical_bridge_name' is undefined
2023-12-04 18:12:20.989518 | 52540032-1894-9e8e-be34-0000000005b6 |      FATAL | Render network_config from template | overcloud-compute-ds-6 | error={"changed": false, "msg": "AnsibleUndefinedVariable: 'neutron_physical_bridge_name' is undefined. 'neutron_physical_bridge_name' is undefined"}
```

``` bash 
$ openstack baremetal node list
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name               | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | power on    | active             | False       |
+--------------------------------------+--------------------+--------------------------------------+-------------+--------------------+-------------+
```

``` bash
metalsmith list
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
| UUID                                 | Node Name          | Allocation UUID                      | Hostname               | State  | IP Addresses            |
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | overcloud-compute-ds-6 | ACTIVE | ctlplane=10.129.173.145 |
+--------------------------------------+--------------------+--------------------------------------+------------------------+--------+-------------------------+
```

``` bash
openstack  port list | grep overcloud-compute-ds-6
| 14c440fe-bf1b-4b1e-8a08-36d9b74ddf59 | overcloud-compute-ds-6_External    | fa:16:3e:62:af:bd | ip_address='10.129.177.147', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'  | DOWN   |
| a2ce7d56-9c25-4e9f-9798-c0fef6c9fcb8 | overcloud-compute-ds-6_Tenant      | fa:16:3e:ed:98:c9 | ip_address='192.168.175.112', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
| c00a915b-cc51-4021-9b2b-c750bae8f647 | overcloud-compute-ds-6_InternalApi | fa:16:3e:2b:d8:4b | ip_address='192.168.174.198', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| da0606bf-dec4-4939-8266-f98c9467f6de | overcloud-compute-ds-6_Storage     | fa:16:3e:cd:4a:d5 | ip_address='192.168.176.147', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
| f5a8aa52-3ca4-488e-9f80-5613ef56ac10 | overcloud-compute-ds-6-ctlplane    | e4:43:4b:ea:c3:60 | ip_address='10.129.173.145', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74'  | ACTIVE |
```

---
## tets redeploy sans erreur dans le fichier de config

increase count, and set  `provisioned: true`
- le provisioning plante
- mais rien ne chnage dan sla config des host et des ports

``` bash
PLAY [Overcloud Node Grow Volumes] *********************************************
2023-12-04 18:19:43.550007 | 52540032-1894-9c38-91d5-000000000018 |       TASK | Wait for provisioned nodes to boot
2023-12-04 18:19:54.840225 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | rhos-comp-ds-br-01
2023-12-04 18:19:54.842195 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | rhos-comp-ds-br-01 | 0:00:11.333621 | 11.27s
2023-12-04 18:19:54.887114 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | rhos-comp-ds-br-03
2023-12-04 18:19:54.888135 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | rhos-comp-ds-br-03 | 0:00:11.379574 | 11.26s
2023-12-04 18:19:54.964236 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | overcloud-compute-ds-6
2023-12-04 18:19:54.965435 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | overcloud-compute-ds-6 | 0:00:11.456872 | 11.41s
2023-12-04 18:19:54.978438 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | rhos-comp-ds-br-05
2023-12-04 18:19:54.979284 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | rhos-comp-ds-br-05 | 0:00:11.470723 | 11.30s
2023-12-04 18:19:55.050301 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | rhos-comp-ds-br-04
2023-12-04 18:19:55.053089 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | rhos-comp-ds-br-04 | 0:00:11.544524 | 11.40s
2023-12-04 18:19:55.064717 | 52540032-1894-9c38-91d5-000000000018 |         OK | Wait for provisioned nodes to boot | rhos-comp-ds-br-02
2023-12-04 18:19:55.065854 | 52540032-1894-9c38-91d5-000000000018 |     TIMING | Wait for provisioned nodes to boot | rhos-comp-ds-br-02 

[WARNING]: Unhandled error in Python interpreter discovery for host rhos-comp-
ds-br-06: Failed to connect to the host via ssh: ssh: Could not resolve
hostname rhos-comp-ds-br-06: Name or service not known

2023-12-04 18:30:00.699653 | 52540032-1894-9c38-91d5-000000000018 |      FATAL | Wait for provisioned nodes to boot | rhos-comp-ds-br-06 | error={"changed": false, "elapsed": 616, "msg": "timed out waiting for ping module test: Data could not be sent to remote host \"rhos-comp-ds-br-06\". Make sure this host can be reached over ssh: ssh: Could not resolve hostname rhos-comp-ds-br-06: Name or service not known\r\n"}
```


metalsmith
``` bash
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | overcloud-compute-ds-6 | ACTIVE | ctlplane=10.129.173.145 |
```
``` bash
| 6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | e696ccdc-0ffc-46aa-b3d9-8a26b6472967 | power on    | active             | False       |
```

ports :
``` bash
openstack  port list | grep overcloud-compute-ds-6
| 14c440fe-bf1b-4b1e-8a08-36d9b74ddf59 | overcloud-compute-ds-6_External    | fa:16:3e:62:af:bd | ip_address='10.129.177.147', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'  | DOWN   |
| a2ce7d56-9c25-4e9f-9798-c0fef6c9fcb8 | overcloud-compute-ds-6_Tenant      | fa:16:3e:ed:98:c9 | ip_address='192.168.175.112', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
| c00a915b-cc51-4021-9b2b-c750bae8f647 | overcloud-compute-ds-6_InternalApi | fa:16:3e:2b:d8:4b | ip_address='192.168.174.198', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| da0606bf-dec4-4939-8266-f98c9467f6de | overcloud-compute-ds-6_Storage     | fa:16:3e:cd:4a:d5 | ip_address='192.168.176.147', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
| f5a8aa52-3ca4-488e-9f80-5613ef56ac10 | overcloud-compute-ds-6-ctlplane    | e4:43:4b:ea:c3:60 | ip_address='10.129.173.145', subnet_id='b2874002-0cb2-4480-9d62-432506b76b74'  | ACTIVE |
```

``` bash
openstack  port list | grep rhos-comp-ds-br-06 
| 1c29ca1b-26a5-4f2d-b3c0-7957a66f0f44 | rhos-comp-ds-br-06_Tenant          | fa:16:3e:85:59:45 | ip_address='192.168.175.126', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
| 8da2edba-349f-4452-8e52-3b40f1091e2c | rhos-comp-ds-br-06_InternalApi     | fa:16:3e:ab:ce:de | ip_address='192.168.174.126', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| cdb65a2d-87a8-4dd3-a65b-9ef6e486a0d8 | rhos-comp-ds-br-06_Storage         | fa:16:3e:3c:2b:22 | ip_address='192.168.176.126', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
| e50b2852-18ba-4325-be7b-32399a07377b | rhos-comp-ds-br-06_External        | fa:16:3e:0c:88:03 | ip_address='10.129.176.46', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'   | DOWN   |
```

---
# test de suppression manuelle du sesrveur

``` bash
metalsmith undeploy  6b0e5035-a1ac-4770-97c7-4296577dde3c
Unprovisioning started for node rhos-comp-ds-br-06 (UUID 6b0e5035-a1ac-4770-97c7-4296577dde3c)
```

``` bash
 openstack  port list | grep rhos-comp-ds-br-06 
| 1c29ca1b-26a5-4f2d-b3c0-7957a66f0f44 | rhos-comp-ds-br-06_Tenant          | fa:16:3e:85:59:45 | ip_address='192.168.175.126', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
| 8da2edba-349f-4452-8e52-3b40f1091e2c | rhos-comp-ds-br-06_InternalApi     | fa:16:3e:ab:ce:de | ip_address='192.168.174.126', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| cdb65a2d-87a8-4dd3-a65b-9ef6e486a0d8 | rhos-comp-ds-br-06_Storage         | fa:16:3e:3c:2b:22 | ip_address='192.168.176.126', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
| e50b2852-18ba-4325-be7b-32399a07377b | rhos-comp-ds-br-06_External        | fa:16:3e:0c:88:03 | ip_address='10.129.176.46', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'   | DOWN   |
```

``` bash
openstack  port list | grep overcloud-compute-ds-6
| 14c440fe-bf1b-4b1e-8a08-36d9b74ddf59 | overcloud-compute-ds-6_External    | fa:16:3e:62:af:bd | ip_address='10.129.177.147', subnet_id='0154cf01-adf8-43d1-9173-bd6b3bd23685'  | DOWN   |
| a2ce7d56-9c25-4e9f-9798-c0fef6c9fcb8 | overcloud-compute-ds-6_Tenant      | fa:16:3e:ed:98:c9 | ip_address='192.168.175.112', subnet_id='ced0a4c2-bbc4-4af4-b57b-886d5a2b2c33' | DOWN   |
| c00a915b-cc51-4021-9b2b-c750bae8f647 | overcloud-compute-ds-6_InternalApi | fa:16:3e:2b:d8:4b | ip_address='192.168.174.198', subnet_id='5a49a9e0-6de3-4eae-a217-0c2e35b0dfcd' | DOWN   |
| da0606bf-dec4-4939-8266-f98c9467f6de | overcloud-compute-ds-6_Storage     | fa:16:3e:cd:4a:d5 | ip_address='192.168.176.147', subnet_id='9b7867ff-0641-44e7-bf5c-e9c51b0969e9' | DOWN   |
```

Metalsmith a bien réinitialisé le serveur
``` bash 
6b0e5035-a1ac-4770-97c7-4296577dde3c | rhos-comp-ds-br-06 | None                                 | power off   | available          | False       |
```

Sur la console du serveur, il est bien arrêté.


Metalsmith a uniquement supprimé le port actif, du réseau ctrlplane.

Suppression des ports manuellement :
``` bash
openstack  port delete rhos-comp-ds-br-06_Tenant rhos-comp-ds-br-06_InternalApi rhos-comp-ds-br-06_Storage rhos-comp-ds-br-06_External

openstack  port delete overcloud-compute-ds-6_External overcloud-compute-ds-6_Tenant overcloud-compute-ds-6_InternalApi overcloud-compute-ds-6_Storage 
```

puis relance du provisionnig.
Et là, c'est bon !!!