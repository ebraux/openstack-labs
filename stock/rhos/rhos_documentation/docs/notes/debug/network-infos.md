

2023-10-12 12:43:22.525 8 WARNING nova.compute.manager [req-895728f2-db98-444c-a584-3f411d32b315 a042bc60637848b2b5edc273cab29fef 9ed88ff00730456a9509fdcdabfb7b9a - default default] [instance: e938ba38-61fe-4e32-92d2-36f90ec99b77] Received unexpected event network-vif-plugged-b1587318-a3a2-4373-a590-12fe14f6cfcd for instance with vm_state active and task_state None.
2023-10-12 12:45:27.472 8 INFO nova.compute.manager [req-0dfb15cf-8a03-4372-9933-fa1d85277f98 b4fa2908a99943c9b65d8e282fb2e373 e3381cd32b1a4a37892cbdb2f2156d0b - default default] [instance: e938ba38-61fe-4e32-92d2-36f90ec99b77] Attaching volume 93016bf5-5b52-49fd-a5b0-f96777f21d1a to /dev/vdb



sur compute0
ip netns exec ovnmeta-1d925193-5fae-401a-928a-50d01902e522 ip a 
ip netns exec ovnmeta-1d925193-5fae-401a-928a-50d01902e522 ip route
ip netns exec ovnmeta-1d925193-5fae-401a-928a-50d01902e522 ping 172.16.1.225

Le ping depuis ne namespace de metadonnées ne fonctionne pas.


    <interface type='bridge'>
      <mac address='fa:16:3e:9f:ec:6c'/>
      <source bridge='br-int'/>
      <virtualport type='openvswitch'>
        <parameters interfaceid='9a5055c9-366a-445a-9714-81f6361084f3'/>
      </virtualport>
      <target dev='tap9a5055c9-36'/>
      <model type='virtio'/>
      <driver name='vhost' rx_queue_size='512'/>
      <mtu size='1442'/>
      <alias name='net0'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
    </interface>



    Bridge br-int
        fail_mode: secure
        datapath_type: system
      ...
        Port tap9a5055c9-36
            Interface tap9a5055c9-36



ovs-appctl ofproto/trace br-int in_port="tap9a5055c9-36",tcp,nw_src=172.16.1.225,dl_src=fa:16:3e:9f:ec:6c,nw_dst=172.16.1.209,tcp_dst=22
...
33. reg15=0,metadata=0x4, priority 50, cookie 0x6894bd36
    drop

dnf install -y vim  libvirt-client  nmap
openstack reboot
openstack reboot --hard

virsh stop instance-00000002
virsh destroy instance-00000002  --gracefull

il n'y a que le qui marche
virsh destroy instance-00000002

L'instance se relance automatiquement et fonctionne.
Mais pas d'attachement de volume

tcpdump -nnei tap9a5055c9-36 -vvv

2 instances se ping, sur 2 compute différents.
le réseau de tenant fonctionne.

WARNING: Following parameter(s) are defined but not currently used in the deployment plan. These parameters may be valid but not in use due to the service or deployment configuration. CtlplaneNetworkAttributes, EC2MetadataIp, NeutronEnableForceMetadata, NeutronEnableIsolatedMetadata
WARNING: Following parameter(s) are defined but not currently used in the deployment plan. These parameters may be valid but not in use due to the service or deployment configuration. CtlplaneNetworkAttributes, EC2MetadataIp, NeutronEnableForceMetadata, NeutronEnableIsolatedMetadata


ajouter dan sle rôle compute
``` bash
    StorageMgmt:
      subnet: storage_mgmt_subnet
```

virsh -c qemu:///system list --all

virsh domblkerror instance-00000002
setlocale: No such file or directory
Segmentation fault (core dumped)

virsh -c qemu:///system  domblkerror instance-00000002

virsh dominfo instance-00000002
virsh console tinstance-00000005

[root@rhos-comp-br-0 log]# virsh domblkerror instance-00000005
setlocale: No such file or directory
No errors found



==> libvirt/libvirtd.log <==
2023-10-13 04:29:32.178+0000: 39557: warning : qemuDomainObjBeginJobInternal:953 : Cannot start job (query, none, none) for domain instance-00000002; current job is (modify, none, none) owned by (39558 remoteDispatchDomainAttachDeviceFlags, 0 <null>, 0 <null> (flags=0x0)) for (17371s, 0s, 0s)
2023-10-13 04:29:32.178+0000: 39557: error : qemuDomainObjBeginJobInternal:975 : Timed out during operation: cannot acquire state change lock (held by monitor=remoteDispatchDomainAttachDeviceFlags)
2023-10-13 04:29:32.390+0000: 39512: error : virNetSocketReadWire:1802 : Cannot recv data: Connection reset by peer





export LIBGUESTFS_BACKEND=direct
virt-df -h -d  instance-00000002
Filesystem                                Size       Used  Available  Use%
instance-00000002:/dev/sda1               961M        43M       886M    5%
instance-00000002:/dev/sda15              8.0M       1.1M       6.9M   14%



 virt-inspector -d  instance-00000002
<?xml version="1.0"?>
<operatingsystems>
  <operatingsystem>
    <root>/dev/sda1</root>
    <name>linux</name>
    <arch>x86_64</arch>
    <product_name>CirrOS 0.6.2</product_name>
    <major_version>0</major_version>
    <minor_version>6</minor_version>
    <hostname>server-validation</hostname>
    <osinfo>unknown0.6</osinfo>
    <mountpoints>
      <mountpoint dev="/dev/sda1">/</mountpoint>
    </mountpoints>
    <filesystems>
      <filesystem dev="/dev/sda1">
        <type>ext3</type>
        <label>cirros-rootfs</label>
        <uuid>f1511162-06fb-4482-9dab-9a0c76633fb2</uuid>
      </filesystem>
    </filesystems>
    <applications/>
  </operatingsystem>
</operatingsystems>
