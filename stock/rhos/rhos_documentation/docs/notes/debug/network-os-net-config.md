Gestion de la configuration du réseau avec os-net-config

Permet de générer la config réseu de la machine, à partir du contenu des fichier

- /etc/os-net-config/config.json
- /etc/os-net-config/mapping.yaml

Utilisé pendant le déploiement, et peut également être lancé à la main.

``` bash
os-net-config -c /etc/os-net-config/config.json -v --detailed-exit-codes
```