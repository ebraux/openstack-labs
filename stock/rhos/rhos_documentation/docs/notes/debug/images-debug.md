# Debug du service Glance

Tester le bon fonctionne su service, et des conteneurs.

``` bash
Restrict the maximum size of an image using the image_size_cap option. Storage limits, per user, are configured using the user_storage_quota option.
```

Si on a une errreur 403, c'est que l'image est protégée
``` bash
[user@host]$ openstack image delete rhel8-web
Failed to delete image with name or ID '21b3b8ba-e28e-4b41-9150-ac5b44f9d8ef':
403 Forbidden
Image 21b3b8ba-e28e-4b41-9150-ac5b44f9d8ef is protected and cannot be deleted.
(HTTP 403)
Failed to delete 1 of 1 images.
[user@host]$ openstack image set --unprotected rhel8-web
[user@host]$ openstack image delete rhel8-web
```