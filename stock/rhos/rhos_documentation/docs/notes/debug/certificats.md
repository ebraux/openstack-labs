


---
## pour l'undercloud, si mise à jour:


Les certificats sont générés sur la machine director, puis intégrée au conteneur HAproxy


Config sur la machine director
``` bash
ll  /etc/pki/tls/certs/haproxy
# total 4
# -rw-------. 1 root root 1460 14 nov.  06:46 overcloud-haproxy-external.crt

ll  /etc/pki/tls/certs/haproxy-external-cert.crt
# -rw-------. 1 root root 1460 14 nov.  06:46 /etc/pki/tls/certs/haproxy-external-cert.crt
```

Config dan le conteneur haproxy
``` bash
podman exec -it haproxy cat /etc/haproxy/haproxy.cfg | grep 'ssl crt'
%   bind 10.129.173.17:13385 transparent ssl crt /etc/pki/tls/private/overcloud_endpoint.pem
%   bind 10.129.173.17:13050 transparent ssl crt /etc/pki/tls/private/overcloud_endpoint.pem
%   bind 10.129.173.17:13000 transparent ssl crt /etc/pki/tls/private/overcloud_endpoint.pem
%   bind 10.129.173.17:13696 transparent ssl crt /etc/pki/tls/private/overcloud_endpoint.pem
```

``` bash
podman exec -it haproxy ls -l /etc/pki/tls/private/overcloud_endpoint.pem
# -rw-r-----. 1 root root 3180 Nov  2 15:28 /etc/pki/tls/private/overcloud_endpoint.pem
```

Dans `/var/lib/kolla/config_files/haproxy.json` on voit que '/' ets un merge de plusieurs sources, dont "/var/lib/kolla/config_files/src-tls".
``` bash
podman exec -it haproxy ls -l /var/lib/kolla/config_files/src-tls/etc/pki/tls/private
total 4
# -rw-r-----. 1 root root 3164 Nov 14 06:46 overcloud_endpoint.pem
```
Les dates ne correspondent pas ...

Le certificat n'a pa été mis à jour.

Redémarrage de HAproxy
``` bash
podman restart haproxy
podman exec -it haproxy ls -l /etc/pki/tls/private/overcloud_endpoint.pem
# -rw-r-----. 1 root root 3164 Nov 14 06:46 /etc/pki/tls/private/overcloud_endpoint.pem
```

Après redémarrage de haproxy, c'est bon.

---
## Vérifier l'expiration

- ERROR (SSLError): SSL exception [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed : Cannot run Openstack commands on the undercloud[https://access.redhat.com/solutions/3357871](https://access.redhat.com/solutions/3357871)

sudo openssl pkcs12 -in /var/lib/certmonger/local/creds -out /etc/pki/ca-trust/source/anchors/undercloud-ca.pem -nokeys -nodes -passin pass:""
$ sudo update-ca-trust extract

Vérified qu'il y a bien une CA 'local'
``` bash
getcert list-cas -v
CA 'SelfSign':
	self-identifies as: SelfSign (certmonger 0.79.17)
	is-default: no
	ca-type: INTERNAL:SELF
	next-serial-number: 01
	config-path: /var/lib/certmonger/cas/20230609152043
CA 'IPA':
	self-identifies as: IPA (certmonger 0.79.17)
	is-default: no
	ca-type: EXTERNAL
	helper-location: /usr/libexec/certmonger/ipa-submit
	config-path: /var/lib/certmonger/cas/20230609152043-1
CA 'dogtag-ipa-renew-agent':
	self-identifies as: Dogtag (IPA,renew,agent) (certmonger 0.79.17)
	is-default: no
	ca-type: EXTERNAL
	helper-location: /usr/libexec/certmonger/dogtag-ipa-renew-agent-submit
	config-path: /var/lib/certmonger/cas/20230609152043-3
CA 'local':
	self-identifies as: Local Signing Authority (certmonger 0.79.17)
	is-default: no
	ca-type: EXTERNAL
	helper-location: /usr/libexec/certmonger/local-submit
	config-path: /var/lib/certmonger/cas/20230609152043-4
```
