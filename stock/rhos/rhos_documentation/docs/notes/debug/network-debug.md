# Réseau

---
## Lister la config réseau des interface

ip
ip addr
ip link
``` bash
ip a | grep " ens1f[0,1,2].*state"
```

> le STATE doit être à UP


``` bash
ovs-vsctl list-ifaces br-ex
```

Visualiser les bonds
``` bash
ovs-appctl bond/list
ovs-appctl lacp/show bondex
```

---
## Lister la configuration Virtuelle mise en place (Vlan, bonds, bridges, ...)



---
## Lister les ports ouvert, et les process connectés aux sockets réseau

ss


netstat

lsof : lister les fichier ouverts / utilisés, et les sockets si option -S
``` lsof
podman exec -it nova_vnc_proxy lsof -i:6080 -S
# -S on cherche une socket
# -i précise le port, ici 6080
```

---
## Observer les paquets circulant sur une interface
tcpdump

Observer le traffic sur une interface.
``` bash
tcpdump -nnei <interface> -vvv
```

`-vvv` active le mode verbose, et permet de voir le traffic plus détaillé, notament les infos de Vlan.


### arp



ssh commande

généréer des logs de connexion
ssh -v
ssh -vvv


---
## OVN

ovn-trace : permet de simuler des requêtes pour vérifier si elle circulent correctement dans les flows définis dans openflow par OVN.

Exemple traces des requêtes DHCP depuis une instance
- récupérer le port utilisé par l'instance
``` bash
 openstack port list ...
```
- récupérer l'adresse MAC de l'interface de l'instance connectée au port
- Lancer a simulation
``` bash
ovn-trace demo-network1 --summary \
  'inport == "d05dd107-9226-405f-a77e-ec6fd4f80049" \
  && eth.src == fa:16:3e:ec:41:f5 \
  && eth.dst == ff:ff:ff:ff:ff:ff \
  && ip4.src == 0.0.0.0 \
  && ip4.dst == 255.255.255.255 \
  && udp.src == 68 && udp.dst == 67'
```
- `demo-network1`: DATAPATH, which specifies the origin of the sample packet; ici demo-network1 
- `--summary` : ???
- MICROFLOW, which specifies the sample packet to be simulated.
  - `inport == ".."` : le port de l'instance
  - `eth.src == fa:16:3e:ec:41:f5` : adresse MAC source
  - `eth.dst == ff:ff:ff:ff:ff:ff : MAC destination, ici toutes`
  - `ip4.src== 0.0.0.0` : open to everything
  - `ip4.dst == 255.255.255.255` : netmask as well ???
  - ` udp.src == 68 && udp.dst == 67` : filtre requêtes UDP vers port 68 et 67


 