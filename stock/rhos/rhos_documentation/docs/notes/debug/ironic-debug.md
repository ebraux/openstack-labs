
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/17.0/html-single/director_installation_and_usage/index#proc_troubleshooting-node-registration_troubleshooting-director-errors]
---
## Suivi  / debug

### Suvi du processus :

``` bash
sudo journalctl -l -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq -u openstack-ironic-conductor -f




``` bash
systemctl sta

Monitor the introspection progress logs in a separate terminal window:

(undercloud)$ sudo tail -f /var/log/containers/ironic-inspector/ironic-inspector.log
tus tripleo_ironic_inspector_dnsmasq.service
# -> failed
```

---
## fonctionnement des containers

ils sont lancé via des serveices de systemd, avec podman.
Il y a des service qui lancent des container comme "tripleo_ironic_api.service"
et d'autre qu lancnet de scommandes dans les containers (healchec) et qui sont dons en status wait. par exmeple : tripleo_ironic_api_healthcheck.service

Accèder aux containers :

```bash
podman ps
podman exec -   
```

Supprimer un container
```bash
podman rm -f ironic_api
```

Lister le simages disponibles sur la machine
```bash
podman images --format {{.Repository}}:{{.Tag}} {{.Repository}}:{{.Tag}}
```

podman create --help

Infos sur comment les container sont lancé : /var/lib/tripleo-config/
 
/usr/bin/podman exec --user root ironic_api env

cat  container-startup-config-readme.txt
cd /var/lib/tripleo-config/container-startup-config

POur trouver comment est lancé un container
```bash
find . -name ironic_api\*
```

vim ./step_4/ironic_api.json
Dans chaque dossier, on retrouve un fichier json pas container (avant c'était un gos fichier par niveau)
Dans la commande, on doit préciser le nom du container, mailgré le faait que aitenant c'ets de fichiser individuels. c'ets un peu con, mais c'ets comme ça.
```bash
paunch debug --file ./step_4/ironic_api.json  --container ironic_api --action print-cmd
```

Supprimer tous les containers
```bash
podman rm -f $(podman ps -a -q)
```




```bash

sudo journalctl -u openstack-ironic-inspector -u openstack-ironic-inspector-dnsmasq

sudo systemctl  openstack-ironic-inspector
/usr/bin/python2 /usr/bin/ironic-inspector --config-file /etc/ironic-inspector/inspector-dist.conf --config-file /etc/ironic-inspector/inspector.conf

sudo systemctl  status openstack-ironic-inspector-dnsmasq
/sbin/dnsmasq --conf-file=/etc/ironic-inspector/dnsmasq.conf


log_dir=/var/log/ironic-inspector

```


---
## 

``` bash
ll /var/lib/ironic-inspector/dhcp-hostsdir/
total 44
-rw-r--r--. 1 42461 42461 18 14 nov.  08:03 00:62:0b:ac:48:b4
-rw-r--r--. 1 42461 42461 18 14 nov.  08:03 00:62:0b:ad:14:30
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:23 00:62:0b:ad:e4:62
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 c8:1f:66:bd:ed:31
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 e4:43:4b:ea:67:50
# -rw-r--r--. 1 42461 42461 25 14 nov.  08:11 e4:43:4b:ea:67:70
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 e4:43:4b:ea:67:a0
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 e4:43:4b:ea:73:b0
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 e4:43:4b:ea:b2:b0
# -rw-r--r--. 1 42461 42461 18 14 nov.  08:03 e4:43:4b:ea:c3:60
-rw-r--r--. 1 42461 42461 12 14 nov.  08:03 unknown_hosts_filter

Nov 15 09:07:02 dnsmasq-dhcp[7]: DHCPDISCOVER(br-ctlplane) e4:43:4b:ea:c3:60 no address available


```

---
## Debug du processus DHCP / Boot PXE

### fichier d elogs à suivre

Prise en compte de la machine pa ironic
``` bash
ll /var/lib/ironic-inspector/dhcp-hostsdir/<MAC-ADDRESS-PXE>
```

Suivi des des requetes DHCP pour récupérer une IP d'introspection
``` bash
tail -f /var/log/containers/ironic-inspector/dnsmasq.log
```

Suivi du Boot PXE
``` bash
tail -f /var/log/containers/ironic/dnsmasq.log
```




pour la partie detection, et création du fichier hostdir, voir : 
``` bash
ironic-conductor.log-rhos-ctrl-br-1
 [root@rhos-director-br-01 ironic]# pwd
/var/log/containers/ironic
```


### Workflow pour l'introspection

Pour le boot pxe lors de l'introspection: un IP 
1. Prise en compte de la machine : `/var/log/containers/ironic-inspector/dnsmasq.log`
``` bash 
Nov 15 17:19:43 dnsmasq[2]: inotify, new or changed file /var/lib/ironic-inspector/dhcp-hostsdir/00:62:0b:ad:e4:62
Nov 15 17:19:43 dnsmasq-dhcp[2]: read /var/lib/ironic-inspector/dhcp-hostsdir/00:62:0b:ad:e4:62
```
2. récupération d'une IP dans le pool d'introspection : `/var/log/containers/ironic-inspector/dnsmasq.log`
``` bash
Nov 15 17:23:14 dnsmasq-dhcp[2]: DHCPDISCOVER(br-ctlplane) 00:62:0b:ad:e4:62
Nov 15 17:23:14 dnsmasq-dhcp[2]: DHCPOFFER(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:15 dnsmasq-dhcp[2]: DHCPREQUEST(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:15 dnsmasq-dhcp[2]: DHCPACK(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:17 dnsmasq-dhcp[2]: DHCPDISCOVER(br-ctlplane) 00:62:0b:ad:e4:62
Nov 15 17:23:17 dnsmasq-dhcp[2]: DHCPOFFER(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:21 dnsmasq-dhcp[2]: DHCPDISCOVER(br-ctlplane) 00:62:0b:ad:e4:62
Nov 15 17:23:21 dnsmasq-dhcp[2]: DHCPOFFER(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:29 dnsmasq-dhcp[2]: DHCPREQUEST(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:29 dnsmasq-dhcp[2]: DHCPACK(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:56 dnsmasq-dhcp[2]: DHCPDISCOVER(br-ctlplane) 00:62:0b:ad:e4:62
Nov 15 17:23:56 dnsmasq-dhcp[2]: DHCPOFFER(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:56 dnsmasq-dhcp[2]: DHCPREQUEST(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
Nov 15 17:23:56 dnsmasq-dhcp[2]: DHCPACK(br-ctlplane) 10.129.173.81 00:62:0b:ad:e4:62
```
3. Puis boot PXE :  `/var/log/containers/ironic/dnsmasq.log`
``` bash
Nov 15 17:23:15 dnsmasq-tftp[2]: error 8 User aborted the transfer received from 10.129.173.81
Nov 15 17:23:15 dnsmasq-tftp[2]: sent /var/lib/ironic/tftpboot/snponly.efi to 10.129.173.81
Nov 15 17:23:15 dnsmasq-tftp[2]: sent /var/lib/ironic/tftpboot/snponly.efi to 10.129.173.81
```
4. On repasse en mode "ignorer" : `/var/log/containers/ironic-inspector/dnsmasq.log`
``` bash
Nov 15 17:28:35 dnsmasq[2]: inotify, new or changed file /var/lib/ironic-inspector/dhcp-hostsdir/00:62:0b:ad:e4:62
Nov 15 17:28:35 dnsmasq-dhcp[2]: read /var/lib/ironic-inspector/dhcp-hostsdir/00:62:0b:ad:e4:62
```


### Workflow Installation

Rien dans `/var/log/containers/ironic-inspector/dnsmasq.log`, car il a déjà une IP dpnéne par le DHCP du réseau ctlplane

Mais on a la trace du boot pxe : `/var/log/containers/ironic/dnsmasq.log`
``` bash
Nov 15 18:00:49 dnsmasq-tftp[2]: error 8 User aborted the transfer received from 10.129.173.51
Nov 15 18:00:49 dnsmasq-tftp[2]: sent /var/lib/ironic/tftpboot/snponly.efi to 10.129.173.51
Nov 15 18:00:49 dnsmasq-tftp[2]: sent /var/lib/ironic/tftpboot/snponly.efi to 10.129.173.51
```

---
## Gestion des lease DHCP

Pour afficher les leases DHCP

Identifer le fichier de lease
``` bash
podman exec -it ironic_inspector_dnsmasq  cat /etc/dnsmasq.conf | grep dhcp-leasefile
#dhcp-leasefile=/var/lib/dnsmasq/dnsmasq.leases
```
Et afficher so contenu
``` bash
podman exec -it ironic_inspector_dnsmasq  cat /var/lib/dnsmasq/dnsmasq.leases
1700070719 18:66:da:ec:3a:b4 10.129.173.96 * 01:18:66:da:ec:3a:b4
1700070880 b8:2a:72:d2:e5:9d 10.129.173.91 overcloud-novacompute-0 01:b8:2a:72:d2:e5:9d
1700070879 b8:2a:72:d2:e5:9c 10.129.173.85 * 01:b8:2a:72:d2:e5:9c
1700070907 e4:43:4b:ea:c3:60 10.129.173.81 * 01:e4:43:4b:ea:c3:60
1700070789 c8:1f:66:bd:eb:4a 10.129.173.80 * 01:c8:1f:66:bd:eb:4a
1700070745 c8:1f:66:bd:eb:49 10.129.173.98 * 01:c8:1f:66:bd:eb:49
1700070772 18:66:da:ec:3a:b5 10.129.173.94 * 01:18:66:da:ec:3a:b5
1700070891 e4:43:4b:ea:67:a0 10.129.173.92 * 01:e4:43:4b:ea:67:a0
1700070865 e4:43:4b:ea:67:a2 10.129.173.84 * 01:e4:43:4b:ea:67:a2
1700070876 b8:2a:72:d2:f0:ce 10.129.173.97 * 01:b8:2a:72:d2:f0:ce
1700070712 e4:43:4b:ea:67:a1 10.129.173.95 * 01:e4:43:4b:ea:67:a1
1700070721 b8:2a:72:d2:f0:cd 10.129.173.93 * 01:b8:2a:72:d2:f0:cd
```

Pour modifier les lease, il faut arrêter dnsmasq, modifier manuellement le fichier, et relancer dnsmasq.

Dans le cas présent, c'est un conteneur.Le fichier de lease est stocké dans le conteneur.
Il faudrait donc
se connecter au conteneur
faire un kill du process dnsmasq
modifier le fichier
se déconnecter et relancer le conteneur
 
Le dossier dhcp-hostsdir : `/var/lib/ironic-inspector/dhcp-hostsdir`, lui, est peristant, et accessible directement depuis la machine hôte : `/var/lib/ironic-inspector/dhcp-hostsdir`.


---
quand on fait le import, création  d'un port "baremetal", mais pas de port "openstack"
``` bash
openstack baremetal port list 
+--------------------------------------+-------------------+
| UUID                                 | Address           |
+--------------------------------------+-------------------+
| 24ede682-c5cd-4e2b-b20b-5abfbca19401 | e4:43:4b:ea:c3:60 |
| b8b7b39c-40fc-451c-8307-6d8df758e877 | 00:62:0b:ad:e4:62 |
+--------------------------------------+-------------------+

openstack baremetal port show  b8b7b39c-40fc-451c-8307-6d8df758e877
+-----------------------+--------------------------------------+
| Field                 | Value                                |
+-----------------------+--------------------------------------+
| address               | 00:62:0b:ad:e4:62                    |
| created_at            | 2023-11-15T11:01:15+00:00            |
| extra                 | {}                                   |
| internal_info         | {}                                   |
| is_smartnic           | False                                |
| local_link_connection | {}                                   |
| node_uuid             | 34107147-8f7b-44ed-ae11-e57c8fe3150f |
| physical_network      | ctlplane                             |
| portgroup_uuid        | None                                 |
| pxe_enabled           | True                                 |
| updated_at            | None                                 |
| uuid                  | b8b7b39c-40fc-451c-8307-6d8df758e877 |
+-----------------------+--------------------------------------+

```

détail du node :
``` bash

| bios_interface | no-bios  |
| boot_interface | ipxe  |
| driver  | ipmi   |
| driver_info     | {
    'deploy_kernel': 'file:///var/lib/ironic/httpboot/agent.kernel',
    'rescue_kernel': 'file:///var/lib/ironic/httpboot/agent.kernel',
    'deploy_ramdisk': 'file:///var/lib/ironic/httpboot/agent.ramdisk', 
    'rescue_ramdisk': 'file:///var/lib/ironic/httpboot/agent.ramdisk', 
    'ipmi_username': 'root', 
    'ipmi_password': '******', 
    'ipmi_address': '10.29.20.51'} |
| driver_internal_info   | {}  |
```

Introspection :
- reboot
- 




---
## The PXE filter driver DnsmasqFilter, state=uninitialized:
Unable to introspect new baremetal nodes
https://access.redhat.com/solutions/6193931

``` bash
grep -r uninitialized iro*/*.log
ironic-inspector/ironic-inspector.log:2023-11-15 10:15:14.114 6 WARNING ironic_inspector.pxe_filter.base [-] Filter driver DnsmasqFilter, state=uninitialized disabling periodic sync task because of an invalid state.: ironic_inspector.pxe_filter.base.InvalidFilterDriverState: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic-inspector/ironic-inspector.log:automaton.exceptions.NotFound: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic-inspector/ironic-inspector.log:ironic_inspector.pxe_filter.base.InvalidFilterDriverState: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic-inspector/ironic-inspector.log:2023-11-15 12:11:45.129 6 ERROR ironic_inspector.node_cache [-] [node: 34107147-8f7b-44ed-ae11-e57c8fe3150f state starting] Processing the error event because of an exception <class 'ironic_inspector.pxe_filter.base.InvalidFilterDriverState'>: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition) raised by ironic_inspector.introspect._do_introspect: ironic_inspector.pxe_filter.base.InvalidFilterDriverState: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic-inspector/ironic-inspector.log:2023-11-15 12:11:45.143 6 DEBUG ironic_inspector.node_cache [-] [node: 34107147-8f7b-44ed-ae11-e57c8fe3150f state error] Committing fields: {'finished_at': datetime.datetime(2023, 11, 15, 11, 11, 45, 130119), 'error': "The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)"} _commit /usr/lib/python3.9/site-packages/ironic_inspector/node_cache.py:152
ironic-inspector/ironic-inspector.log:2023-11-15 12:12:18.180 6 ERROR ironic_inspector.node_cache [-] [node: 34107147-8f7b-44ed-ae11-e57c8fe3150f state starting] Processing the error event because of an exception <class 'ironic_inspector.pxe_filter.base.InvalidFilterDriverState'>: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition) raised by ironic_inspector.introspect._do_introspect: ironic_inspector.pxe_filter.base.InvalidFilterDriverState: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic-inspector/ironic-inspector.log:2023-11-15 12:12:18.193 6 DEBUG ironic_inspector.node_cache [-] [node: 34107147-8f7b-44ed-ae11-e57c8fe3150f state error] Committing fields: {'finished_at': datetime.datetime(2023, 11, 15, 11, 12, 18, 181492), 'error': "The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)"} _commit /usr/lib/python3.9/site-packages/ironic_inspector/node_cache.py:152
ironic/ironic-conductor.log:2023-11-15 12:12:16.906 7 ERROR ironic.drivers.modules.inspector [req-9face538-cef4-467c-8c4d-3ff8b4cc110b - - - - -] Inspection failed for node 34107147-8f7b-44ed-ae11-e57c8fe3150f with error: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
ironic/ironic-conductor.log:2023-11-15 12:13:16.909 7 ERROR ironic.drivers.modules.inspector [req-9face538-cef4-467c-8c4d-3ff8b4cc110b - - - - -] Inspection failed for node 34107147-8f7b-44ed-ae11-e57c8fe3150f with error: The PXE filter driver DnsmasqFilter, state=uninitialized: my fsm encountered an exception: Can not transition from state 'uninitialized' on event 'sync' (no defined transition)
 [root@rhos-director-br-01 containers]# 
```
---
Anciennes docs :

``` bash
ironic node list
ironic node-show
```     
