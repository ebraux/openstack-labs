


Pas très calir.
a regarder de plus près sur des projets actifs ...


Limits are the resource limitations that are allowed for each project.
 An administrator can configure limits
 Users can query their rate and absolute limits.
Rate limits control the frequency at which users can issue specific API requests.
configuré au niveau du fichier de configuration des API


Quotas
Quota sets provide quota management support.

---
## limits
Limites fixées, implémanté pour Nova et Cinder uniquement
``` bash
openstack limits  show --absolute

openstack limits  show --absolute --project demo
```
``` bash
--absolute : Show absolute limits
--rate : Show rate limits
--reserved :  Include reservations count [only valid with --absolute]
```

``` bash
openstack limits  show --absolute
# +--------------------------+-------+
# | Name                     | Value |
# +--------------------------+-------+
# | maxTotalInstances        |    10 |
# | maxTotalCores            |    20 |
# | maxTotalRAMSize          | 51200 |
# | maxServerMeta            |   128 |
# | maxTotalKeypairs         |   100 |
# | maxServerGroups          |    10 |
# | maxServerGroupMembers    |    10 |
# | totalRAMUsed             |     0 |
# | totalCoresUsed           |     0 |
# | totalInstancesUsed       |     0 |
# | totalServerGroupsUsed    |     0 |
# | maxTotalVolumes          |    10 |
# | maxTotalSnapshots        |    10 |
# | maxTotalVolumeGigabytes  |  1000 |
# | maxTotalBackups          |    10 |
# | maxTotalBackupGigabytes  |  1000 |
# | totalVolumesUsed         |     0 |
# | totalGigabytesUsed       |     0 |
# | totalSnapshotsUsed       |     0 |
# | totalBackupsUsed         |     0 |
# | totalBackupGigabytesUsed |     0 |
# +--------------------------+-------+
```


---
## quotas
``` bash
 openstack quota show
 openstack quota show accounts
```