

### Tests

Pour tester les échange entre machine dan le bridge correspondant à un Vlan
``` bash
tcpdump -nn -e  -i br0 '(icmp  and vlan)'
```
Il faut ensuite faire un ping vers une IP de VM attaché au bridge, depuis une machine distante. En local, il n'y a pas de tag du VLAN.

Pour tester les échanges entre machines dans le bridge correspondant à un Vlan. Dans le Bridge, les paquets ne sont pas tagués, un tcpdump simple suffit
``` bash
tcpdump -nn -e  -i br0.173 icmp
```
On peut lancer les 2 tcpdump en même temps, pour voir les paquets avec tags sur `br0`, puis sans tag sur `br0.173`