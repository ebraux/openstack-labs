

---
## Principe

Passer en utilisateur root :
``` bash
sudo -i
```

D'abord vérifier l'état des services.
``` bash
systemctl list-units -t service triple*
```

Pour chaque service on peut vérifier les 3 points :

1. Loaded
2. Active
3. Enabled
   
``` bash
systemctl status tripleo_nova_metadata.service
```

- [Plus d'infos sur la gestion des services system](./system-services.md)

Et si tout est correct vérifier ensuite l'état des conteneurs avec podman.

``` bash
podman ps --format "{{ .Names }} {{ .Status }}"
```

- [Plus d'infos sur la gestion des conteneurs](./conteneurs-debug.md)

