
# Gestion des certificats Internes pour Octavia


- [https://bugzilla.redhat.com/show_bug.cgi?id=1645536](https://bugzilla.redhat.com/show_bug.cgi?id=1645536)
- [https://access.redhat.com/solutions/4909471](https://access.redhat.com/solutions/4909471)



``` bash

 OctaviaCaKeyPassphrase: sI2Z3HT2YViZksmAVp9oT277N
 export CA_PASSPHRASE_OLD=K1WoVd6LAaoLpGCDV2KXqAqQm
 export CA_PASSPHRASE=sI2Z3HT2YViZksmAVp9oT277N
 
 openssl rsa -aes256  \
   -passin env:CA_PASSPHRASE_OLD \
   -passout env:CA_PASSPHRASE \
   -in /tmp/octavia-ssl/private/cakey.old.pem \
   -out /tmp/octavia-ssl/private/cakey.pem

   ca_private_key_passphrase=sI2Z3HT2YViZksmAVp9oT277N
```

Hi,

Moving `/var/lib/config-data/puppet-generated/octavia/etc/octavia/certs/private/cakey.pem` on each controllers does not work.

```
 "TASK [octavia_overcloud_config : count unique CAs and keys] ********************", "ok: [rhos-ctrl-br-01]", "",
 "TASK [octavia_overcloud_config : fail if CA or private key do not match in all Octavia nodes] ***", "skipping: [rhos-ctrl-br-01]", "",
 "TASK [octavia_overcloud_config : fail if the number of CA and private key doesn't match] ***", "fatal: [rhos-ctrl-br-01]: FAILED! => {\"changed\": false, \"msg\": \"Inconsistent Octavia configuration detected:\
 Mismatched count for CAs and private keys on controllers.\\n\"}", "
```

Moving all the certs files is necessary to make the deployment successfull :
- `/var/lib/config-data/puppet-generated/octavia/etc/octavia/certs/ca_01.pem`
- `/var/lib/config-data/puppet-generated/octavia/etc/octavia/certs/private/cakey.pem`


And then update the LB, with  `openstack loadbalancer failover`
 https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html-single/configuring_load_balancing_as_a_service/index#lb-update-run-lb-service-instances_update-upgrade-lb-service

 I Can now run `openstack deploy` command, and LB are available for users