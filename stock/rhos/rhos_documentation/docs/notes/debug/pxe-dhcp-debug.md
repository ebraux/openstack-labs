# Debug PXE-DHCP process pendnat l'introspection


---
## Utilisation du PXE/DHCP par Ironic

Ironic intègre un mécanisme de pxe-DHCP.

Lorsqu'on lance la commande `openstack overcloud node introspect --provide <node1>`, une entrée est créée avec l'adress MAC renseignée lors du provisionning du noeud, et une adresse IP de la plage d'introspection.

> En dehors de la phase d'introspection, il n'y a aucune entrée présenet, et le node ne peux pas booter en PXE.

Lister les info du noeud pour obtenir l'adress MAC

Lister la config du réseau de provisionnig pour voir la plage d'IP

Lister les ports, pour vérifer que des adresses IP  sont bien disponibles dans cette plage


---
## Suivi des requètes DHCP

Vérification des requetes DHCP sur la machine director :

``` bash
sudo tcpdump -nli br-ctlplane 
sudo tcpdump  -ei br-ctlplane  udp and \( port 67 or port 68 \)
sudo tcpdump -i any port 67 or port 68 or port 69
```


Exemple de requet DHCP qui fonctionne :
```bash
20:39:11.272935 00:62:0b:ac:48:b4 (oui Unknown) > Broadcast, ethertype IPv4 (0x0800), length 590: 0.0.0.0.bootpc > 255.255.255.255.bootps: BOOTP/DHCP, Request from 00:62:0b:ac:48:b4 (oui Unknown), length 548
20:39:14.278475 52:54:00:51:e3:60 (oui Unknown) > Broadcast, ethertype IPv4 (0x0800), length 348: rhos-director-br-01.ctlplane.overcloud-prod.com.bootps > 255.255.255.255.bootpc: BOOTP/DHCP, Reply, length 306
20:39:18.303163 00:62:0b:ac:48:b4 (oui Unknown) > Broadcast, ethertype IPv4 (0x0800), length 590: 0.0.0.0.bootpc > 255.255.255.255.bootps: BOOTP/DHCP, Request from 00:62:0b:ac:48:b4 (oui Unknown), length 548
20:39:18.305415 52:54:00:51:e3:60 (oui Unknown) > Broadcast, ethertype IPv4 (0x0800), length 348: rhos-director-br-01.ctlplane.overcloud-prod.com.bootps > 255.255.255.255.bootpc: BOOTP/DHCP, Reply, length 306
```
On a des `get DHCP request`, mais pas de reply.
- [https://serverfault.com/questions/1060053/dhcp-not-working-for-bridged-qemu-virtual-machine](https://serverfault.com/questions/1060053/dhcp-not-working-for-bridged-qemu-virtual-machine)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/recommendations_for_large_deployments/assembly-debugging-recommendations-and-known-issues_recommendations-large-deployments#ref-deployment-debugging_recommendations-large-deployments](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/16.2/html/recommendations_for_large_deployments/assembly-debugging-recommendations-and-known-issues_recommendations-large-deployments#ref-deployment-debugging_recommendations-large-deployments)

``` bash
netstat -ltunp | grep dnsmasq 
# udp        0      0 0.0.0.0:67              0.0.0.0:*                           3296/dnsmasq        
```

``` bash
ps -ef | grep 3296
# dnsmasq     3296       1  0 17:33 ?        00:00:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
# root        3204    3296  0 17:33 ?        00:00:00 /usr/sbin/dnsmasq --conf-file=/var/lib/libvirt/dnsmasq/default.conf --leasefile-ro --dhcp-script=/usr/libexec/libvirt_leaseshelper
```
