# compute srevice

``` bash
openstack compute service list
openstack compute service list -c Host -c Binary -c Zone
openstack compute service list -c Host -c Binary -c Status -c State
```
Les controller sont dans la zone 'internal', les compute dans la zone 'nova'.
``` bash
openstack compute service list -c Host -c Binary -c Zone
```


Chaque controller doit faire tourner les services :

- nova-conductor
- nova-scheduler

Chaque compute doit faire tourner les services :

- nova-compute

Tous les services doivent être `Status=enabled` , et `State=UP`.
``` bash
openstack compute service list -c Host -c Binary -c Zone
```
