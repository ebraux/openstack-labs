# debug les volumes

Vérifier la configuration de Cinder, et le lien avec CEPH.

``` bash
grep ^rbd_ \
  /var/lib/config-data/puppet-generated/cinder/etc/cinder/cinder.conf

# rbd_pool=volumes
# rbd_user=openstack
```

Et ensuite vérifier les infos dans CEPH :

- les droits de connction avec `ceph auth list`
``` bash
client.openstack
key: AQCOT5FfAAAAABAA99q5XJEQtrgrFpF5MoA6pg==
caps: [mgr] allow *
caps: [mon] profile rbd
caps: [osd] profile rbd pool=vms, profile rbd pool=volumes, profile rbd
pool=images
```
- les pools avec `ceph osd ls-pools`


---
## attacher et détacher les volumes

Si un volume est resté en état "detaching", utiliser la commande cinder, pour le rendre `available`

``` bash
cinder reset-state --state available volume_id
```


---
## suppression d'un volume

Pour forcer la suppression d'un volume
``` bash
openstack volume delete --force volume_id
```


---
## Logs

PLusieurs fichiers de logs, dans /var/log/containers :

- API
- Volume service
- scheduler service
- cinder-manage


Si on a un message ci dessous dans le log du scheduler, c'ets générallement un problème avec RabbitMQ.
``` bash
201 (...) Failed to run task
cinder.scheduler.flows.create_volume.ScheduleCreateVolumeTask;volume:create:
No valid host was found. No weighed hosts available
```

--- 
## Interface avec RabbitMQ

Vérifier la configuration de Cinder, et le lien avec Rabbit MQ.

``` bash
grep ^transport_url  \
  /var/lib/config-data/puppet-generated/cinder/etc/cinder/cinder.conf

grep rabbit  \
  /var/lib/config-data/puppet-generated/cinder/etc/cinder/cinder.conf
```

Le port par défaut est `5672`, l'utilisateur par défaut est `guest`, et en HA chaque instance doit être déclarée. Exemple :
``` bash
transport_url=rabbit://guest:UF7F50L96uWECrQzB0Qxi5JaY@rhos-ctrl-br-0.internalapi.overcloud-prod.com:5672,guest:UF7F50L96uWECrQzB0Qxi5JaY@rhos-ctrl-br-1.internalapi.overcloud-prod.com:5672,guest:UF7F50L96uWECrQzB0Qxi5JaY@rhos-ctrl-br-2.internalapi.overcloud-prod.com:5672/?ssl=0
```

