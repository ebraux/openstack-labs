# OVN

OVN Pilote la config réseau, qui est implémentée par OVS
Afficher le mapping entre les interfaces physique, et le résaeu virtule géré par OVS

``` bash
sudo ovs-vsctl show
```

Cette commande affiche la configuration bridge, bond, vla, et le lien avec les interfaces physique du serveur.

On a en plus le bridge br-int, qui relie tous les noeuds du cluster, pour :

- la gestion du traffic pour les réseau de projet, entre les VM (GENEVE sur le subnet 'tenant').
- la gestion du traffic entre les instances de Load Ballancer Octavia


Sur les controlleurs, ont trouve également le couple `patch-br-int-to-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af et `patch-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af-to-br-int` permet de connecter le OVN et le réseau de provisionnig dans Openstack ??? - A éclaircir -

``` bash
Note that the TAP device connects the instance to br-int. Also note that the
creation of the provider1-104 network resulted in a pair of interfaces connecting
br-int to br-prov1 with a patch port. The interface names contain the ID of the
provider1-104 network.
```

---
## Interfaces physiques


Sur les serveurs, toute sle interfaces ne sont pas (encore) connectées.

En général, 3 interfaces sont connectées (UP). Celles qui ne sont pas connectée sont DOWN.

Lister les interfaces
``` bash
 ip link show
#  ...
#  2: ens10f0np0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
  ...
#     link/ether 00:62:0b:ac:48:b4 brd ff:ff:ff:ff:ff:ff
# 3: ens10f1np1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN mode DEFAULT group default qlen 1000
  ...
```
- l'interface ens10f0np0 est connectée à un switch, elle est  UP
- par contre l'interface ens10f1np1 est DOWN. elle n'est pas connectée (c'est normal dans le cas présent)


---
##  Exemple de configuration OVN sur un controleur

- interface de provisioning : n’apparaît pas, car n'est pas gérée avec un brige OVN.
    - interface physique ens10f0np0

- bridge br-ex
  - bond bondex
    - interfaces physiques ens1f0 et ens2f0
    - port br-ex, lien avec l'extérieur
    - vlan 178
    - le "patch" pour se connecter au bridge br-int `patch-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af-to-br-int`

- bridge br-trunk
  - bond bondtrunk
    - interfaces physiques ens1f1 et ens2f1
    - vlan 174, 175, 176, 177

- bridge br-int
   - pas d'interface physique
   - le patch vers le réseau de provisioning
   - Des ports correspondant aux interfaces reliées au réseau de tenant des 2 autres controlleurs
   - Des ports correspondant aux interfaces reliées aux réseaux de tenant des computes
   - le port `o-hm0` utilisé par le réreau interne d'Octavia

 
 ``` bash
sudo ovs-vsctl show
3a34783e-053e-4146-8ad0-3a674395d1ea

    Bridge br-ex
        fail_mode: standalone

        Port bondex
            Interface ens2f0
            Interface ens1f0

        Port br-ex
            Interface br-ex
                type: internal
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal

        Port patch-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af-to-br-int
            Interface patch-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af-to-br-int
                type: patch
                options: {peer=patch-br-int-to-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af}


    Bridge br-trunk
        fail_mode: standalone
        Port br-trunk
            Interface br-trunk
                type: internal

        Port bondtrunk
            Interface ens1f1
            Interface ens2f1

        Port vlan174
            tag: 174
            Interface vlan174
                type: internal
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal
        Port vlan176
            tag: 176
            Interface vlan176
                type: internal
        Port vlan177
            tag: 177
            Interface vlan177
                type: internal

    Bridge br-int
        fail_mode: secure
        datapath_type: system

        Port br-int
            Interface br-int
                type: internal
                
        Port patch-br-int-to-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af
            Interface patch-br-int-to-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af
                type: patch
                options: {peer=patch-provnet-fdbbd3ae-70b3-41f3-ab04-f22c1a2f48af-to-br-int}

        Port ovn-52c92f-0
            Interface ovn-52c92f-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.51"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}
        Port ovn-bdf802-0
            Interface ovn-bdf802-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.52"}
                bfd_status: {diagnostic="No Diagnostic", flap_count="1", forwarding="true", remote_diagnostic="No Diagnostic", remote_state=up, state=up}

        Port ovn-b6e19f-0
            Interface ovn-b6e19f-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.100"}
        Port ovn-f3fe74-0
            Interface ovn-f3fe74-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.101"}
        Port ovn-8ffefb-0
            Interface ovn-8ffefb-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.102"}
        
        Port o-hm0
            Interface o-hm0
                type: internal
    ovs_version: "2.15.8"
```

---
##  Exemple de configuration OVN sur un compute


- interface de provisioning : n’apparaît pas, car n'est pas gérée avec un brige OVN.
    - interface : eno1

- bridge br-ex
  - bond bondex : interfaces physiques ens1f0 et ens2f0
    - port br-ex, lien avec l'extérieur
    - vlan 178

- bridge br-trunk
  - bond bondtrunk
    - interfaces physiques ens1f1 et ens2f1
    - vlan 174, 175, 176, 177

- bridge br-int   
   - pas d'interface physique
   - Des ports correspondant aux interfaces reliées au réseau de tenant des autres compute
   - Des ports correspondant aux interfaces reliées aux réseaux de tenant des controlleurs
   - le port `o-hm0` utilisé par le réreau interne d'Octavia (uniquement présent si un Load balancer est actif sur ce noeud - à confirmer - )


``` bash
sudo ovs-vsctl show
74f642ca-904a-4945-a5de-93a3f7c1e104
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true

    Bridge br-ex
        fail_mode: standalone
        Port bondex
            Interface ens1f0
            Interface eno2
        Port br-ex
            Interface br-ex
                type: internal
        Port vlan178
            tag: 178
            Interface vlan178
                type: internal

    Bridge br-trunk
        fail_mode: standalone
        Port vlan175
            tag: 175
            Interface vlan175
                type: internal
        Port bondtrunk
            Interface eno3
            Interface ens1f1
        Port br-trunk
            Interface br-trunk
                type: internal
        Port vlan176
            tag: 176
            Interface vlan176
                type: internal
        Port vlan174
            tag: 174
            Interface vlan174
                type: internal

    Bridge br-int
        fail_mode: secure
        datapath_type: system
        Port br-int
            Interface br-int
                type: internal

        Port ovn-74b286-0
            Interface ovn-74b286-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.50"}
        Port ovn-52c92f-0
            Interface ovn-52c92f-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.51"}
        Port ovn-bdf802-0
            Interface ovn-bdf802-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.52"}


        Port ovn-f3fe74-0
            Interface ovn-f3fe74-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.101"}
        Port ovn-8ffefb-0
            Interface ovn-8ffefb-0
                type: geneve
                options: {csum="true", key=flow, remote_ip="192.168.175.102"}




```