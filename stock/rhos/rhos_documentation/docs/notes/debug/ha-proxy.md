

``` bash
pcs resource config haproxy-bundle
#   Podman: image=cluster.common.tag/haproxy:pcmklatest replicas=3 run-command="/bin/bash /usr/local/bin/kolla_start" network=host options="--user=root --log-driver=k8s-file --log-opt
#       path=/var/log/containers/stdouts/haproxy-bundle.log -e KOLLA_CONFIG_STRATEGY=COPY_ALWAYS"
#   Storage Mapping:
#     source-dir=/var/lib/kolla/config_files/haproxy.json target-dir=/var/lib/kolla/config_files/config.json options=ro (haproxy-cfg-files)
#     source-dir=/var/lib/config-data/puppet-generated/haproxy/ target-dir=/var/lib/kolla/config_files/src options=ro (haproxy-cfg-data)
#     source-dir=/etc/hosts target-dir=/etc/hosts options=ro (haproxy-hosts)
#     source-dir=/etc/localtime target-dir=/etc/localtime options=ro (haproxy-localtime)
#     source-dir=/var/lib/haproxy target-dir=/var/lib/haproxy options=rw,z (haproxy-var-lib)
#     source-dir=/etc/pki/ca-trust/extracted target-dir=/etc/pki/ca-trust/extracted options=ro (haproxy-pki-extracted)
#     source-dir=/etc/pki/tls/certs/ca-bundle.crt target-dir=/etc/pki/tls/certs/ca-bundle.crt options=ro (haproxy-pki-ca-bundle-crt)
#     source-dir=/etc/pki/tls/certs/ca-bundle.trust.crt target-dir=/etc/pki/tls/certs/ca-bundle.trust.crt options=ro (haproxy-pki-ca-bundle-trust-crt)
#     source-dir=/etc/pki/tls/cert.pem target-dir=/etc/pki/tls/cert.pem options=ro (haproxy-pki-cert)
#     source-dir=/dev/log target-dir=/dev/log options=rw (haproxy-dev-log)
#     source-dir=/etc/pki/tls/private/overcloud_endpoint.pem target-dir=/var/lib/kolla/config_files/src-tls/etc/pki/tls/private/overcloud_endpoint.pem options=ro (haproxy-cert)

```

``` bash
ip a | grep 'inet '
```

pour endpoint public :

``` bash
ip a | grep 'inet ' | grep 10.129.176.9
```


---
## firewall 

``` bash
systemctl status firewalld
# ○ firewalld.service - firewalld - dynamic firewall daemon
#      Loaded: loaded (/usr/lib/systemd/system/firewalld.service; disabled; preset: enabled)
#      Active: inactive (dead)
#        Docs: man:firewalld(1)
```
``` bash
firewall-cmd --state
# not running
```