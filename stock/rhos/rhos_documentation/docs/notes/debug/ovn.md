 
 high level  3 principaux composantss :
 - OVN ML2 plugin : converti la getsion des objets réseau d'Openstack géré par Neutron(routeur, network, ...), en un "toppologie OVN", indépendnate de l'infar matérielle sous-jaccente.
   - infos sont stockées dans OVN Northbound Database
- OVN Northd Daemon 
  - transforme les informations "logiques (routeurs, ports, ...) issues de la OVN Northbound Database, en Logical OVN flows.
  - stocke les données dans la OVN Southbound Database
- Local OVN Controller
  - sur chaque noeud
  - "act as opevswitch controller
  - récupére les logical FLOW OVN  dans la OVN Southbound Database, et les converti en Openflow Rules.
  - les openflow rules sont implémentée dans l'instance locale d'OVS de chaque compute node
  

  Implémentation : 
  set `neutronMecanismeDrrivers : OVN`
  L3 OVN Plugins called 'ovn-router' : `NeutronServicePlugins: "qos,ovn-router,trink"`

  Le daemon OVN-Northd et les 2 database tournent sur les controlleurs.
  Pacemaker choisi un Master parmis les instances
  ``` bash
  pcs stastus
  # bundle ovn
  # 1 master
  # 2 slaves
  ```
   ssh sur le master, puis identification du contaienr :
   
``` bash
podman ps | grep ovn-dbs-bundle | awk '{print $NF}'
# ovn-dbs-bundle-docker-0
```

Regarder ls process qui tournent :
``` bash
podman exec  ovn-dbs-bundle-docker-0 ps -ef | grep "ovs-db-server-"
# - ovsdb-server  -> /etc/openvswitch/ovnnb_db.db
# - ovsdb-server  -> /etc/openvswitch/ovnsb_db.db
```
``` bash
podman exec  ovn-dbs-bundle-docker-0 ps -ef | grep "northd"
# ovn-northd ...
```

Le process daemen ne tourne que sur le master.

---
## query OVN
 Il n'y a qu'un master
 ``` bash
pcs constraint | grep ovn-dbs-bundle
# IP-DU-MASTER... (with-rsc-role:mastrer)
```
``` bash
ovn-nbctl --db=tcp:IP-DU-MASTER:6641 show
# par defaut rien
ovn-sbctl --db=tcp:IP-DU-MASTER:6642 show
# les noeud (chassis) du cluster
```


On peut exporter les variables
``` bash
export OVN_NB_DB=tcp:IP-DU-MASTER:6641
export OVN_SB_DB=tcp:IP-DU-MASTER:6642
ovn-nbctl  show
ovn-sbctl  show
```

Lister les logical flow
``` bash
ovn-sbctl lflow-list
```
Par défaut, c'est vide, mais si on crée des réseaux et subnet,
``` bash
ovn-nbctl  show
# des objets "switch", qui correspondnet aux réseau
# et des ports

ovn-sbctl lflow-list
# les logical flow.
```
  
Si on crée un routeur, avec une gareway
``` bash
ovn-nbctl  show
# un objet "riuter" en plus
# un port en plus sur le switch, pour se connecter au router


