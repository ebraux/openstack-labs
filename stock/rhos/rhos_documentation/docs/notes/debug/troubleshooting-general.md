# Commandes et infos de Troubleshooting

---
## Getsion des processus

Lister les process et leurs ports
``` bash
pgrep -l nova
```

Liste les container
``` bash
# Ctifes uniquement
podman ps

podman ps | wc
# environ 50 sur un controller


# Actifs et arrêtés
podman ps -a

podman ps -a | wc
     88    1056   18105
```
> normal que certain conteneurs soient arr^tés, ils sont utilisés uniquement pour des actions spécifiques : initialisation, configuration, ...

Voir les logs d'une container, m^me si il est arrêté
``` bash
podman logs
```

Lister les fichier ouverts / utilisés, et les sockets si option -S
``` lsof
lsof
```


---
## Client Openstack

On peut lancer les commandes openstack en mode "debug", et placer le résulat dans un fichier 
``` bash
openstack project create toto \
  --debug \
  --log-file logs/project-demo.log
```

Dans les logs :
 
 - on retrouve toutes les interactions avec les API
 - on peut voir quelle opération est en erreur, et du coup l'interaction avec quel composant
 - on peut récupérer la Request-ID, qui permet de tracer l'opération dans les autres logs





