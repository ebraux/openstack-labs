

https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/installing_satellite_server_in_a_connected_network_environment/index



https://gitlab-ci-token:glpat-WT1cNcs9yCCK6VdqUR9e@gitlab.imt-atlantique.fr/rhos_tempo/rhcs-cluster-management.git




!! Full forward and reverse DNS resolution using a fully-qualified domain name
10.129.172.11 rh-manager-br-01.imta.fr
10.129.172.12 rh-manager-br-02.imta.fr
10.129.172.14 rh-relay-br-01.imta.fr

10.129.172.16 rhos-director-br-01.imta.fr

---
## Prérequis :

| Doc | Appliqué |
| --- | ---- |
| OS Redhat 8.x | 8.7 |
| 4-core 2.0 GHz CPU at a minimum  | 8 |
| A minimum of 20 GB RAM | 24GB |
| A minimum of 4 GB RAM of swap space | 8GB |
| Storage /usr 5GB, /var/log 10 MB | 45GB |
| Storage /var/lib/[pulp|pgsql|qpidd] 300GB+20GB+25MB | 400GB LVM |
| Storage /var/log 10 MB | 10 GB |


Résolution DNS :
- `10.129.172.15 rh-satellite-br-01.imta.fr` 


Prérequis accès réseau


| Host name | Port | Protocol |
| --- | --- | --- |
| subscription.rhsm.redhat.com | 443 | HTTPS |
| cdn.redhat.com | 443 | HTTPS |
| *.akamaiedge.net | 443 | HTTPS |
| cert.console.redhat.com (if using Red Hat Insights) | 443 | HTTPS |
| api.access.redhat.com (if using Red Hat Insights) | 443 | HTTPS |
| cert-api.access.redhat.com (if using Red Hat Insights) | 443 | HTTPS |

---
## Installation de la VM

Installation VM RHEL 8.7
- création de la VM


### Gestion du réseau

- modification de la config Réseau pour se connecter au bridge
- activation auto de l'interface au reboot


### Gestion des disques  

Config disque : 80Go

``` bash
Disk /dev/vda: 80 GiB, 85899345920 bytes, 167772160 sectors
Device     Boot   Start       End   Sectors Size Id Type
/dev/vda1  *       2048   2099199   2097152   1G 83 Linux
/dev/vda2       2099200 167772159 165672960  79G 8e Linux LVM

Sur /dev/vda2 :
Disk /dev/mapper/rhel-root: 47.7 GiB
Disk /dev/mapper/rhel-swap: 8 GiB
Disk /dev/mapper/rhel-home: 23.3 GiB

Filesystem             Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root   48G  2.4G   46G   5% /
```
- suppression du volume /home (fdisk, mkfs.xfs, tar, fstab, ...)
- création d'un LV sur le disque annexe sur la machine hôte
- attachemement du LV sur la vm sur /var/lib  (fdisk, mkfs.xfs, tar, fstab, ...)

### Initialisation de la machine

- config proxy
- enregistrement RH
- update
- Install : git vim tar chrony




## Getsion des rpository

Rechercher le pool Satellite
``` bash
subscription-manager list --all --available --matches 'Red Hat Satellite Infrastructure Subscription'
# +-------------------------------------------+
#     Available Subscriptions
# +-------------------------------------------+
# Subscription Name:   Red Hat Satellite Infrastructure Subscription
# Provides:            Red Hat Beta
#                      Red Hat Software Collections (for RHEL Server)
#                      Red Hat Enterprise Linux for x86_64
#                      Red Hat CodeReady Linux Builder for x86_64
#                      Red Hat Ansible Engine
#                      Red Hat Enterprise Linux Load Balancer (for RHEL Server)
#                      Red Hat Discovery
#                      Red Hat Satellite 5 Managed DB
#                      Red Hat Satellite Capsule
#                      Red Hat Enterprise Linux Server
#                      Red Hat Software Collections Beta (for RHEL Server)
#                      Red Hat Satellite Proxy
#                      Red Hat Satellite with Embedded Oracle
#                      Red Hat Enterprise Linux High Availability for x86_64
#                      Red Hat Satellite
# SKU:                 MCT3718
# Contract:            
# Pool ID:             2c94fddb828947660182c3f5d0db7db8
# Provides Management: Yes
# Available:           48
# Suggested:           1
# Service Type:        L1-L3
# Roles:               
# Service Level:       Premium
# Usage:               
# Add-ons:             
# Subscription Type:   Standard
# Starts:              11/26/2018
# Ends:                10/20/2023
# Entitlement Type:    Physical
```



Disable all repositories:
subscription-manager repos --disable "*"


subscription-manager repos \
--enable=rhel-8-for-x86_64-baseos-rpms \
--enable=rhel-8-for-x86_64-appstream-rpms \
--enable=satellite-6.12-for-rhel-8-x86_64-rpms \
--enable=satellite-maintenance-6.12-for-rhel-8-x86_64-rpms


dnf module enable -y satellite:el8

dnf install -y satellite

dnf install -y sos

---
## Configuration

https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/installing_satellite_server_in_a_connected_network_environment/installing_server_connected_satellite#Configuring_Installation_satellite

``` bash
satellite-installer --scenario satellite \
--foreman-initial-organization "IMT_Atlantique" \
--foreman-initial-location "Brest" \
--foreman-initial-admin-username root \
--foreman-initial-admin-password xxxxxx
```

  Success!
  * Satellite is running at https://rh-satellite-br-01.imta.fr
      Initial credentials are root / xxxxxx

  * To install an additional Capsule on separate machine continue by running:

      capsule-certs-generate --foreman-proxy-fqdn "$CAPSULE" --certs-tar "/root/$CAPSULE-certs.tar"
  * Capsule is running at https://rh-satellite-br-01.imta.fr:9090

  The full log is at /var/log/foreman-installer/satellite.log
Package versions are being locked.



ouvrir les ports
``` bash
firewall-cmd --permanent --add-service=dns
firewall-cmd --permanent --add-port=9090/tcp
firewall-cmd --reload
```

---
## configuration du proxy

``` bash
unset http_proxy
unset https_proxy
unset no_proxy

hammer http-proxy create --name=imta-br-proxy \
--url http://proxy.enst-bretagne.fr:8080

hammer settings set --name=content_default_http_proxy --value=imta-br-proxy
```

- [https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html-single/installing_satellite_server_in_a_connected_network_environment/index#Configuring_Server_with_an_HTTP_Proxy_satellite](https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html-single/installing_satellite_server_in_a_connected_network_environment/index#Configuring_Server_with_an_HTTP_Proxy_satellite)


---
## configuration de Satellite

Via l'interface Web

- menu Administer / settings

---
## Ajouter les repo


Surtout ne pas oublier de les synchroniser. sinon, ils sont vide, et l'installation de spaqets sur les clients échainet ensuite

via interface garphique

ou en ligne d ecommande :
https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/installing_satellite_server_in_a_connected_network_environment/performing-additional-configuration#Enabling_the_Client_Repository_satellite

https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html-single/managing_content/index#Synchronizing_Repositories_content-management



---
## Ajouter les images

- [https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.6/html/content_management_guide/managing_container_images#Importing_Container_Images](https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.6/html/content_management_guide/managing_container_images#Importing_Container_Images)


- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation)


---
## Création du manifest

- créer une "subscription allocation" : 
  - [https://access.redhat.com/management/subscription_allocations](https://access.redhat.com/management/subscription_allocations)
  - Create New subscription allocation
    - Name : satellite-br
    - Type : Satellite 6.12
- y associer des souscriptions
  - onglet "Subscriptions"
  - "Add Subscriptions" 
  - add : 
    - 11 x Red Hat Ceph Storage, Premium (Up to 256TB on a maximum of 12 Physical Nodes)
    - 2 x Red Hat Enterprise Linux Academic Server, Self-support (16 sockets) (Up to 1 guest) with Satellite
    - 10 000 x Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
    - 20 x Red Hat Satellite Infrastructure Subscription
- exporter le fichier manifest
  - "Export Manifest"
  - Sauvagredr le fichier en local
- se connecter à Satellite  [https://rh-satellite-br-01.imta.fr](https://rh-satellite-br-01.imta.fr)
- Menu Contenu / Subscriptions
  - "Manage manifest"
  - Choose file / import manifest


https://access.redhat.com/solutions/11025
https://access.redhat.com/solutions/1249873
https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/installing_satellite_server_in_a_connected_network_environment/installing_server_connected_satellite#Importing_a_Red_Hat_Subscription_Manifest_into_Server_satellite
 

---
## configuration avec Insight


To install Red Hat Insights on Satellite Server, enter the following command:
``` bash
# satellite-maintain packages install insights-client
```

To register Satellite Server with Red Hat Insights, enter the following command:
``` bash
# satellite-installer --register-with-insights
```

https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/installing_satellite_server_in_a_connected_network_environment/performing-additional-configuration#using-insights-with-satellite-server_satellite


---
## Installation d'outils en rapport avec Satellite

- CEPH : [https://access.redhat.com/articles/1750863](https://access.redhat.com/articles/1750863)

Ajouter les souscriptions

- For Red Hat Ceph Storage 5 on RHEL8
  - Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs x86_64
  - Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs x86_64
  - Red Hat Ceph Storage Tools 5 for RHEL 8 x86_64 RPMs
  - Red Hat Ansible Engine 2.9 for RHEL 8 x86_64 RPMs
  - rhel-8-for-x86_64-highavailability-rpms for 'ceph-resource-agents' users  

---
## Configuration de l'authrntification avec le cli "hammer" 

Utilisationde hammer depuis la machine satellite : rh-satellite-br-01

Vérifier le fichier `/root/.hammer/cli.modules.d\foreman.yml`

Il doit contenir :
``` bash
:foreman:
  :host: 'https://rh-satellite-br-01.imta.fr/'
  # Credentials. You'll be asked for the interactively if you leave them blank here
  :username: 'root'
  :password: 'xxxxxxxx'
```

- Il ne doit pas y avoir de fichier : `/root/.hammer/cli_config.yml`, car c'est pour les ancienne version de satellite
- En cas de mise à jour du MDP dans satellite, ce fichier n'ets pas mis à jour
- [https://access.redhat.com/solutions/3436701](https://access.redhat.com/solutions/3436701)




---
## Création d'un content view


Bug dans l'interface graphique, la page "contenus / content view" est vide 
Créatio  à la main : [https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.11/html/managing_content/managing_content_views_content-management](https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.11/html/managing_content/managing_content_views_content-management)


``` bash
hammer content-view list
----------------|---------------------------|---------------------------|-----------|---------------------|---------------
CONTENT VIEW ID | NAME                      | LABEL                     | COMPOSITE | LAST PUBLISHED      | REPOSITORY IDS
----------------|---------------------------|---------------------------|-----------|---------------------|---------------
1               | Default Organization View | Default_Organization_View | false     | 2023/05/02 20:13:16 |               
----------------|---------------------------|---------------------------|-----------|---------------------|---------------
```

``` bash
hammer organization  list
# ---|----------------|----------------|-------------|---------------
# ID | TITLE          | NAME           | DESCRIPTION | LABEL         
# ---|----------------|----------------|-------------|---------------
# 1  | IMT_Atlantique | IMT_Atlantique |             | IMT_Atlantique
# ---|----------------|----------------|-------------|---------------
```

``` bash
hammer repository list --organization "IMT_Atlantique"
# ---|------------------------------------------------------------|-------------------------------------|--------------|---------------------------------------------------------------------------------
# ID | NAME                                                       | PRODUCT                             | CONTENT TYPE | URL                                                                             
# ---|------------------------------------------------------------|-------------------------------------|--------------|---------------------------------------------------------------------------------
# 18 | Red Hat Ansible Engine 2.9 for RHEL 8 x86_64 RPMs          | Red Hat Ansible Engine              | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/ansible/2.9/os         
# 1  | Red Hat Ansible Engine 2 for RHEL 8 x86_64 RPMs            | Red Hat Ansible Engine              | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/ansible/2/os           
# 17 | Red Hat Ceph Storage Tools 5 for RHEL 9 x86_64 RPMs        | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/layered/rhel9/x86_64/rhceph-tools/5/os      
# 3  | Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8   | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel8/8/x86_64/appstream/os                 
# 5  | Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8.7 | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel8/8.7/x86_64/appstream/os               
# 6  | Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8      | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel8/8/x86_64/baseos/os                    
# 8  | Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8.7    | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel8/8.7/x86_64/baseos/os                  
# 9  | Red Hat Enterprise Linux 9 for x86_64 - AppStream RPMs 9   | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel9/9/x86_64/appstream/os                 
# 10 | Red Hat Enterprise Linux 9 for x86_64 - AppStream RPMs 9.1 | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/rhel9/9.1/x86_64/appstream/os               
# 11 | Red Hat Satellite Capsule 6.12 for RHEL 8 x86_64 RPMs      | Red Hat Satellite Capsule           | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/sat-capsule/6.12/os    
# 13 | Red Hat Satellite Client 6 for RHEL 8 x86_64 RPMs          | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/sat-client/6/os        
# 14 | Red Hat Satellite Client 6 for RHEL 9 x86_64 RPMs          | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/layered/rhel9/x86_64/sat-client/6/os        
# 15 | Red Hat Satellite Maintenance 6.12 for RHEL 8 x86_64 RPMs  | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/sat-maintenance/6.12/os
# 16 | Red Hat Satellite Utils 6.12 for RHEL 8 x86_64 RPMs        | Red Hat Enterprise Linux for x86_64 | yum          | https://cdn.redhat.com/content/dist/layered/rhel8/x86_64/sat-utils/6.12/os      
# ---|------------------------------------------------------------|-------------------------------------|--------------|---------------------------------------------------------------------------------
```

``` bash
hammer content-view create \
--description "CEPH_Content_View" \
--name "CEPH_Content_View" \
--organization "IMT_Atlantique" \
--repository-ids 9,10,17
```

``` bash
hammer  content-view list
# ----------------|---------------------------|---------------------------|-----------|---------------------|---------------
# CONTENT VIEW ID | NAME                      | LABEL                     | COMPOSITE | LAST PUBLISHED      | REPOSITORY IDS
# ----------------|---------------------------|---------------------------|-----------|---------------------|---------------
# 2               | CEPH_Content_View         | CEPH_Content_View         | false     |                     | 9, 10, 17     
# 1               | Default Organization View | Default_Organization_View | false     | 2023/05/02 20:13:16 |               
# ----------------|---------------------------|---------------------------|-----------|---------------------|---------------
```

o add a repository to an existing Content View, 
``` bash
hammer content-view add-repository \
--name "My_Content_View" \
--organization "My_Organization" \
--repository-id repository_ID
```

### Création d'une "Activation key"


- Content → Activation keys
  - "Create Activation keys"

subscription-manager register --org="IMT_Atlantique" --activationkey="ceph"

---
## création des plans de synchronistaion

fait via l'interface graphique

---
## Création d'une registry d'image Docker

- [https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.12/html/managing_content/managing_container_images_content-management](https://access.redhat.com/documentation/fr-fr/red_hat_satellite/6.12/html/managing_content/managing_container_images_content-management)
- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation)


### Création du product

Create the custom Red Hat Container Catalog product:
``` bash
hammer product create \
--description "Red Hat Container Catalog" \
--name "RedHat_Container_Catalog" \
--organization "IMT_Atlantique" \
--sync-plan "Daily"
```





### Pour Ceph


Create the repository for the container images: ex registry.redhat.io/rhceph/rhceph-5-rhel8:latest

Liste des images pour Ceph : 
- [https://access.redhat.com/documentation/fr-fr/red_hat_ceph_storage/5/html/installation_guide/red-hat-ceph-storage-installation#registry.redhat.io/configuring-a-private-registry-for-a-disconnected-installation_install](https://access.redhat.com/documentation/fr-fr/red_hat_ceph_storage/5/html/installation_guide/red-hat-ceph-storage-installation#registry.redhat.io/configuring-a-private-registry-for-a-disconnected-installation_install)
registry.redhat.io/rhceph/rhceph-5-rhel8:latest
registry.redhat.io/rhceph/snmp-notifier-rhel8:latest
registry.redhat.io/rhceph/rhceph-5-dashboard-rhel8:latest
registry.redhat.io/openshift4/ose-prometheus:v4.10
registry.redhat.io/openshift4/ose-prometheus-node-exporter:v4.10
registry.redhat.io/openshift4/ose-prometheus-alertmanager:v4.10

``` bash
export IMAGE_UPSTREAM_NAME=
hammer repository create \
--content-type "docker" \
--organization "IMT_Atlantique" \
--product "RedHat_Container_Catalog" \
--url "https://registry.redhat.io/" \
--upstream-username "DISI1" \
--upstream-password "DiSi2003" \
--docker-upstream-name ${IMAGE_UPSTREAM_NAME} \
--name ${IMAGE_UPSTREAM_NAME}

```


Synchronize the repository:
``` bash
hammer repository synchronize \
--name ${IMAGE_UPSTREAM_NAME} \
--organization "IMT_Atlantique" \
--product "RedHat_Container_Catalog"
```
---
## Update

https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/upgrading_and_updating_red_hat_satellite/updating_satellite


subscription-manager repos --enable \
satellite-maintenance-6.12-for-rhel-8-x86_64-rpms


satellite-maintain upgrade list-versions
satellite-maintain upgrade check --target-version 6.12.z
satellite-maintain upgrade run --target-version 6.12.z


---
## Mise à jour de satellite

Par défaut, foreman-protector bloque les mise à jour.

"Previously, when using yum to update or install packages on the Satellite base operating system might also update the packages related to Red Hat Satellite and resulted in system inconsistency. With this release, Red Hat Satellite prevents users from installing and updating packages with yum. Instead, use foreman-maintain packages install, satellite-maintain packages install, foreman-maintain packages update and satellite-maintain packages update commands. Note that foreman-maintain and satellite-maintain runs the satellite-installer --upgrade script after installing packages and therefore some services are restarted. You can disable this feature and control the stability of the system yourself if you want. This feature is not enabled on Red Hat Capsule Server.
Pour pouvoir faire une mise à jour :"


``` bash
satellite-maintain packages update
``` 

Alternative :
``` bash
satellite-maintain packages unlock
dnf update
satellite-maintain packages lock
```


- [https://access.redhat.com/solutions/4591281](https://access.redhat.com/solutions/4591281)
