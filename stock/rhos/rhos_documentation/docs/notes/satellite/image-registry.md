
---
## Configuration des dépots sur Satellite

-  [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/director_installation_and_usage/assembly_preparing-for-director-installation#proc_preparing-a-satellite-server-for-container-images_preparing-for-director-installation)


``` bash
hammer organization  list
# ---|----------------|----------------|-------------|---------------
# ID | TITLE          | NAME           | DESCRIPTION | LABEL         
# ---|----------------|----------------|-------------|---------------
# 1  | IMT_Atlantique | IMT_Atlantique |             | IMT_Atlantique
# ---|----------------|----------------|-------------|---------------
```

``` bash
hammer product list --organization "IMT_Atlantique"
# ----|----------------------------------------------------------------------------------|---------------------------|----------------|--------------|----------------
# ID  | NAME                     | DESCRIPTION               | ORGANIZATION   | REPOSITORIES | SYNC STATE
# ----|--------------------------|---------------------------|----------------|--------------|------
# ...
# 115 | RedHat_Container_Catalog | Red Hat Container Catalog | IMT_Atlantique | 123 | planned
# ...
# ----|--------------------------|---------------------------|----------------|--------------|------
```


``` bash
while read IMAGE; do \
  IMAGE_NAME=$(echo $IMAGE | cut -d"/" -f3 | sed "s/openstack-//g") ; \
  IMAGE_NOURL=$(echo $IMAGE | sed "s/registry.redhat.io\///g") ; \
  hammer repository create \
  --organization "IMT_Atlantique" \
  --product "RedHat_Container_Catalog" \
  --content-type docker \
  --url https://registry.redhat.io \
  --docker-upstream-name $IMAGE_NOURL \
  --upstream-username "DISI1" \
  --upstream-password "DiSi2003" \
  --name $IMAGE_NAME ; done < satellite_images
```

``` bash
hammer repository create \
  --organization "IMT_Atlantique" \
  --product "RedHat_Container_Catalog" \
  --content-type docker \
  --url https://registry.redhat.io \
  --docker-upstream-name rhceph/rhceph-4-rhel8 \
  --upstream-username "DISI1" \
  --upstream-password "DiSi2003" \
  --name rhceph-4-rhel8
```

``` bash
hammer product synchronize \
  --organization "IMT_Atlantique" \
  --name "RedHat_Container_Catalog"
```

``` bash
 hammer docker tag list --repository "base" \
  --organization "IMT_Atlantique" \
  --product "RedHat_Container_Catalog" \
  --lifecycle-environment "production"
```  
