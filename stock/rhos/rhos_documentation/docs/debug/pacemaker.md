
---
## Principe Fencing/STONITH

Fencing is the process of isolating a failed node to protect the cluster and the cluster resources. Without fencing, a failed node might result in data corruption in a cluster. Director uses Pacemaker to provide a highly available cluster of Controller nodes.

Pacemaker uses a process called STONITH to fence failed nodes. STONITH is an acronym for "Shoot the other node in the head". STONITH is disabled by default and requires manual configuration so that Pacemaker can control the power management of each node in the cluster.

Deploying a highly available overcloud without STONITH is not supported. You must configure a STONITH device for each node that is a part of the Pacemaker cluster in a highly available overcloud. For more information on STONITH and Pacemaker, see Fencing in a Red Hat High Availability Cluster and Support Policies for RHEL High Availability Clusters.

- [https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#proc_deploying-fencing_fencing](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.1/html/managing_high_availability_services/assembly_fencing-controller-nodes_rhosp#proc_deploying-fencing_fencing)
---
## Obtenir des informations sur le cluster

``` bash
sudo pcs property show
# Cluster Properties:
#  cluster-infrastructure: corosync
#  cluster-name: tripleo_cluster
#  dc-version: 2.1.5-9.el9_2-a3f44794f94
#  have-watchdog: false
#  last-lrm-refresh: 1707224262
#  redis_REPL_INFO: rhos-ctrl-br-01
#  stonith-enabled: true

```


---
## Configuration anti split-brain : stonith


- Voir [Gérer le stonith](pacemaker-stonith.md)

Il faut aussi le configurer dans  du fencing dans le déploiement de l'infra
Fichier `01-fencing.yaml`

- Variable : `EnableFencing`, en temps normal à True
- Déclaration des device pour les différents noeuds


---
## Gérer les noeuds


``` bash
pcs cluster start --all
```

``` bash
sudo pcs node standby rhos-ctrl-br-0
```

- Arrêter pacemaker sur le noeud
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs cluster stop ${SERVER_NAME}"
# rhos-ctrl-br-03: Stopping Cluster (pacemaker)...
# rhos-ctrl-br-03: Stopping Cluster (corosync)...
```
- Vérifier
``` bash
ssh tripleo-admin@${PCS_MANAGER} "sudo pcs status |grep -w ${SERVER_NAME}"
#  * OFFLINE: [ rhos-ctrl-br-03 ]
```
---
## Gérer les ressources

``` bash
sudo pcs resource cleanup
```

``` bash
pcs resource config galera 

sudo pcs resource manage galera-bundle

pcs resource status  galera
```


---
## Ressources

- Mitigation/Recovery from an OpenStack Clustering Outage Replacing a Controller [https://access.redhat.com/solutions/3200852](https://access.redhat.com/solutions/3200852)