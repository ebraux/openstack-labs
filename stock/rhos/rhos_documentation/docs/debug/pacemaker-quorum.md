# Gestion du Quorum sur le cluster

``` bash
pcs quorum status
Quorum information
------------------
Date:             Sat Nov 11 13:50:25 2023
Quorum provider:  corosync_votequorum
Nodes:            3
Node ID:          1
Ring ID:          1.196
Quorate:          Yes

Votequorum information
----------------------
Expected votes:   3
Highest expected: 3
Total votes:      3
Quorum:           2  
Flags:            Quorate 

Membership information
----------------------
    Nodeid      Votes    Qdevice Name
         1          1         NR rhos-ctrl-br-0 (local)
         2          1         NR rhos-ctrl-br-1
         3          1         NR rhos-ctrl-br-2
```


---
## Gestion du cluster (Quorum)

Vérification du Quorum (il ne doit pas être "bloqued")
``` bash
corosync-quorumtool -s
# Quorum information
# ------------------
# Date:             Wed Nov 29 05:32:38 2023
# Quorum provider:  corosync_votequorum
# Nodes:            3
# Node ID:          1
# Ring ID:          1.7a
# Quorate:          Yes

# Votequorum information
# ----------------------
# Expected votes:   3
# Highest expected: 3
# Total votes:      3s
# Quorum:           2  
# Flags:            Quorate 

# Membership information
# ----------------------
#     Nodeid      Votes Name
#          1          1 rhos-ctrl-br-01 (local)
#          2          1 rhos-ctrl-br-02
#          3          1 rhos-ctrl-br-03
```

Election forcée pour rhos-ctrl-br-01
``` bash
corosync-quorumtool -v 3 -n 1

corosync-quorumtool -s
# Quorum information
# ------------------
# Date:             Wed Nov 29 05:38:48 2023
# Quorum provider:  corosync_votequorum
# Nodes:            3
# Node ID:          1
# Ring ID:          1.7a
# Quorate:          Yes

# Votequorum information
# ----------------------
# Expected votes:   5
# Highest expected: 5
# Total votes:      5
# Quorum:           3  
# Flags:            Quorate 

# Membership information
# ----------------------
#     Nodeid      Votes Name
#          1          3 rhos-ctrl-br-01 (local)
#          2          1 rhos-ctrl-br-02
#          3          1 rhos-ctrl-br-03
```
