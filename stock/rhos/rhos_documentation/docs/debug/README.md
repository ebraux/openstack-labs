# Informations de debug

---
## Cluster de base de données Galera

- [Informations générales](./galera.md)
- [Resynchronisation du cluster](./galera-resynchronise.md)
- [Cas désespéré, reconstruction depuis un noeud](./galera-start-single.md)

---
## Gestion  du réseau : OVN

- [Resynchronisation du cluster](./ovn-split-brain.md)
- [Cas désespéré, reconstruction depuis un noeud](./ovn-rebuild-database.md)

---
## Gestion du HA: pacemaker/corosync

- [Informations générales](./pacemaker.md)
- [Corosync](./pacemaker-corosync.md)
- [Gestion du stonith (anti split-brain)](./pacemaker-stonith.md)
- [Gestion du Quorum](./pacemaker-quorum.md)

