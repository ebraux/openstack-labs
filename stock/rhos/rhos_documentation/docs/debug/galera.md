# Cluster Galera

---
## Principe

Normalement, le cluster est complétement géré par pacemaker.

Pacemaker est capable de relancer le cluster :

- Définir quel est le dernier noeud arrêté et le lancer
- Monter le cluster avec les autres noeud
- Relancer le noeud bootstrap

En tant normal, le cluster fonctionne en mode "multi-master". Les 3  noeuds sont marqué "promoted".


Le cluster Galera s'appuie sur le mécanisme de STONITH :

- Le mécanisme de STONITH permet d'éviter le split-brain
- Si le stonith n'est pas OK, le cluster se bloque ?
- La relation entre les 2 n'est pas encore bien claire ....

Un des noeuds a aussi un role particulier dans la gestion du cluster : le "Bootstrap controller node" en cas de perte de ce noeud, il y a des manipe spécifiques à faire pour promouvoir un autre neoud.


---
## "Master", "Slave", "Promoted", "Unpromoted"


En 16.2, PCS gérait des services en "Master/Slave". Depuis la 17.0, c'est en "Prmoted/Unpromoted". Les docs ne sont pas toutes à jour.

Tous les noeuds du cluster doivent être "Promoted".

``` bash
CHANGES IN PCS-0.11
       This section summarizes the most important changes in commands done in pcs-0.11.x compared to pcs-0.10.x. For detailed description of current commands see above.

   Legacy role names
       Roles  'Master'  and  'Slave'  are  deprecated  and should not be used anymore. Instead use 'Promoted' and 'Unpromoted' respectively. Similarly, --master has been deprecated and replaced with
       --promoted.
```

---
## Afficher et analyser la config

``` bash
sudo pcs status
```


``` bash
pcs resource config galera
# ...
# wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com,rhos-ctrl-br-02.internalapi.overcloud-prod.com,rhos-ctrl-br-03.internalapi.overcloud-prod.com
# ...
# wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com,rhos-ctrl-br-02.internalapi.overcloud-prod.com,rhos-ctrl-br-03.internalapi.overcloud-prod.com
# ...
#      master-max=3
# ...
```

``` bash
sudo pcs stonith status
```

Bootstrap controller node
``` bash
ssh heat-admin@$PCS_MANAGER "sudo hiera -c /etc/puppet/hiera.yaml pacemaker_short_bootstrap_node_name"
# rhos-ctrl-br-0  
```
---
## Lancer une remise en service par PCS

Désactiver la gestion d galera par le cluster
``` bash
sudo pcs resource disable galera-bundle
sudo pcs resource unmanage galera-bundle
```

Réactiver la getsion d galera par le cluster
``` bash
sudo pcs resource cleanup
sudo pcs resource manage galera-bundle
sudo pcs resource enable galera-bundle
```

Ressources : 

- OpenStack Galera will not start with a failed Controller node and galera-bundle resources in Slave : [https://access.redhat.com/solutions/5981551](https://access.redhat.com/solutions/5981551)



---
## En cas de perte d'un noeud  ou de cluster désynchronisé

Si la "Lancer une remise en service par PCS" ne fonctionne pas, voir [Resynchronisation du cluster](./galera-resynchronise.md)
 
 (Attention, c'est la galère ...)

---
## Autres

commandes à comprendre et à utiliser :  `sudo pcs resource refresh galera-bundle`


---
## Ressources


- How Galera works and how to rescue Galera clusters in the context of Red Hat OpenStack Platform [https://access.redhat.com/solutions/3215501](https://access.redhat.com/solutions/3215501)

- OpenStack Galera will not start with a failed Controller node and galera-bundle resources in Slave
https://access.redhat.com/solutions/5981551


- Galera : 'Failure, Attempted to promote Master instance of galera before bootstrap node has been detected.' https://access.redhat.com/solutions/4102481

- [https://galeracluster.com/library/documentation/quorum-reset.html](https://galeracluster.com/library/documentation/quorum-reset.html)

- Containerized Openstack Galera Recovery [https://access.redhat.com/solutions/4044281](https://access.redhat.com/solutions/4044281)
