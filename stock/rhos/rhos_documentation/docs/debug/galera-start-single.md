
# Validation du démarrage de mariaDB sur un seul noeud

Normalement, à cette étape, le conteneur doit démarrer

``` bash
[root@rhos-ctrl-br-01 mysql]# podman exec galera-bundle-podman-0  mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status';"
# Variable_name	Value
# wsrep_cluster_status	Primary
```

``` bash
[root@rhos-ctrl-br-01 mysql]# podman exec galera-bundle-podman-0  mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_last_committed';"
# Variable_name	Value
# wsrep_last_committed	4719948
```

Il reste en mode unpromoted ...

Dans les logs : 
``` bash
log_op_output) 	info: galera_promote_0[6008] error output [ Schema validation of configuration is disabled (enabling is encouraged and prevents common misconfigurations) ]
(log_op_output) 	info: galera_promote_0[6008] error output [ Schema validation of configuration is disabled (enabling is encouraged and prevents common misconfigurations) ]
(log_op_output) 	info: galera_promote_0[6008] error output [ ERROR 1045 (28000): Access denied for user 'clustercheck'@'localhost' (using password: YES) ]
(log_op_output) 	info: galera_promote_0[6008] error output [ ocf-exit-reason:Unable to retrieve wsrep_cluster_status, verify check_user 'clustercheck' has permissions to view status ]
(log_op_output) 	info: galera_promote_0[6008] error output [ ocf-exit-reason:local node <rhos-ctrl-br-01> is started, but not in primary mode. Unknown state. ]
(log_op_output) 	info: galera_promote_0[6008] error output [ ocf-exit-reason:Failed initial monitor action ]
(log_finished) 	info: galera promote (call 1456, PID 6008) exited with status 1 (Failed initial monitor action) (execution time 6.662s)
```

Vérification que les mots de passe sont bien présents dans le conteneur

``` bash
podman exec galera-bundle-podman-0 cat /root/.my.cnf 
# [client]
# user=root
# password="HaQlAWtYDk"

# [mysql]
# user=root
# password="HaQlAWtYDk"
```

``` bash
podman exec galera-bundle-podman-0 cat /etc/sysconfig/clustercheck 
# MYSQL_USERNAME=clustercheck

# MYSQL_PASSWORD='07c7j6D5N3sVatdCcEsFWze10'

# MYSQL_HOST=localhost
```

Vérification que l'utilisateur clustercheck (entre autres) est bien présent dans mysql
``` bash
podman exec galera-bundle-podman-0  mysql -e "SELECT user,host,password FROM mysql.user;"
# +--------------+-----------+-------------------------------------------+
# | User         | Host      | Password                                  |
# +--------------+-----------+-------------------------------------------+
# | mariadb.sys  | localhost |                                           |
# | root         | localhost | *8213EF8811D52AB917061169E73E0D2C93A29372 |
# | mysql        | localhost | invalid                                   |
# | root         | %         | *8213EF8811D52AB917061169E73E0D2C93A29372 |
# | clustercheck | localhost | *2FECE619C28F28FA33D7130D20AC576176C2027A |
# | cinder       | %         | *15AE98AD64098B6ADD0F6C2E3F544CFB742F8A86 |
# | glance       | %         | *56E9C8B8C91BB98B0DF5BC7D2F4F60412A8CE4F7 |
# | heat         | %         | *9F0E9F958DA9FAE55C01E05ECFD183EA55BF872E |
# | keystone     | %         | *F8D7E99797DEA85E2393878D44D15900D2C01EE6 |
# | neutron      | %         | *148A71E44C8149B426BC3E8921BA06DB8DA39AF8 |
# | nova         | %         | *B89AA31B41F81AD4ED2A6A2FD0CBD8D2CAD26F2B |
# | nova_api     | %         | *B89AA31B41F81AD4ED2A6A2FD0CBD8D2CAD26F2B |
# | placement    | %         | *4C32D1B4A21C8679EB79BA1EF8018FA6B350C8F3 |
# | octavia      | %         | *D64DD35E3C05DEA419FF55758725D5CF7B4B7CAF |
# +--------------+-----------+-------------------------------------------+
```

Vérification que la config dans le conteneur est bien la dernière générée par puppet

``` bash
[root@rhos-ctrl-br-01 sysconfig]# cat /var/lib/config-data/puppet-generated/mysql/etc/sysconfig/clustercheck
# MYSQL_USERNAME=clustercheck

# MYSQL_PASSWORD='07c7j6D5N3sVatdCcEsFWze10'
               

# MYSQL_HOST=localhost
```

Tout colle au nievau des mots de passe, test de connexion, toujours en erreur.
``` bash
podman exec galera-bundle-podman-0 sh

mysql -u clustercheck -p
#  password: 
```
Changement manuel du mot de passe
``` bash
podman exec galera-bundle-podman-0 sh

mysql
SQL> use mysql;
SQL> ALTER USER 'clustercheck'@'localhost' IDENTIFIED BY '07c7j6D5N3sVatdCcEsFWze10';
```


