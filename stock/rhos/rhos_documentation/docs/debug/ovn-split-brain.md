# Gestion des base de donnees d'OVN


- [https://bugzilla.redhat.com/show_bug.cgi?id=1948620](https://bugzilla.redhat.com/show_bug.cgi?id=1948620)
- 
---
## Vérifier les bases de données

Pour chaque serveur, obtenir la liste des serveur, et vérifier que les ID sont cohérents. A faire pour la northbound database, et la southbound database.

``` bash
export SERVER_IP=10.129.173.52
```

### northbound database

- lister la config
``` bash
sudo podman exec ovn_cluster_north_db_server ovs-appctl -t /var/run/ovn/ovnnb_db.ctl cluster/status OVN_Northbound
```
- Supprimer un serveur
```bash
sudo podman exec ovn_cluster_north_db_server ovs-appctl -t /var/run/ovn/ovnnb_db.ctl cluster/kick OVN_Northbound <ID>
```
- rejoindre le cluster
``` bash
sudo podman exec ovn_cluster_north_db_server rm /var/run/ovn/ovnnb_db.db /var/lib/ovn/.ovnnb_db.db.~lock~

sudo podman exec ovn_cluster_north_db_server ovsdb-tool join-cluster /var/lib/ovn/ovnnb_db.db OVN_Northbound tcp:192.168.174.51:6643 tcp:192.168.174.53:6643
#               IP/port local                IP/PORT noeud du cluster

sudo podman restart ovn_cluster_north_db_server
sudo podman restart ovn_cluster_northd
```

### Southbound database

La c'est beaucoup moins clair .... Je crois que les DB Southe ne sont pas en cluster de toute façon....

- lister la config
``` bash
#sudo podman exec ovn_cluster_south_db_server ovs-appctl -t /var/run/ovn/ovnsb_db.ctl cluster/status OVN_Southbound 
```
- Supprimer un serveur
```bash
#podman exec ovn_cluster_south_db_server ovs-appctl -t /var/run/ovn/ovnsb_db.ctl cluster/kick  OVN_Southbound <ID>
```
- rejoindre le cluster
``` bash
#podman exec ovn_cluster_south_db_server rm /var/run/ovn/ovnsb_db.db /var/lib/ovn/.ovnsb_db.db.~lock~

#podman exec ovn_cluster_south_db_server ovsdb-tool join-cluster /var/lib/ovn/ovnsb_db.db OVN_Southbound  tcp:192.168.174.51:6644 tcp:192.168.174.53:6644
#                    IP/port local                IP/PORT noeud du cluster
```



commande à distance
``` bash
ssh tripleo-admin@${SERVER_IP} sudo podman exec ovn_cluster_north_db_server ovs-appctl -t /var/run/ovn/ovnnb_db.ctl cluster/status OVN_Northbound 2>/dev/null|grep -A4 Servers:
```



---
ressources :

- Recover from partitioned clustered OVN database [https://access.redhat.com/solutions/7024434](https://access.redhat.com/solutions/7024434)
- Replacement of the first controller node fails at step 1 if the same hostname is used for a new node : [https://access.redhat.com/solutions/5662621](https://access.redhat.com/solutions/5662621)
- How to enable DEBUG in OVN Controller? [https://access.redhat.com/solutions/4270652](https://access.redhat.com/solutions/4270652)
- autres
    - ovn_controller down on 2 compute and cannot be started again : [https://access.redhat.com/solutions/5073021](https://access.redhat.com/solutions/5073021)
    - OVN northdb and southdb crashed after applying a new tag [https://access.redhat.com/solutions/5018411] (https://access.redhat.com/solutions/5018411)