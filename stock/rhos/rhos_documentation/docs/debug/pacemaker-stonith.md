# gestion du stonith


---
## Afficher le status

``` bash
sudo pcs property show
# Cluster Properties:
#   ...
#  stonith-enabled: false
```

Afficher les informations sur le stonith
``` bash
sudo pcs stonith status
#   * stonith-fence_ipmilan-00620bade462	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
#   * stonith-fence_ipmilan-00620bac48b4	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-02

# Fencing Levels:
#  Target: rhos-ctrl-br-01
#    Level 1 - stonith-fence_ipmilan-00620bac48b4
#  Target: rhos-ctrl-br-02
#    Level 1 - stonith-fence_ipmilan-00620bade462
```



---
## Activer/Désactiver le STONITH

``` bash
sudo pcs property set stonith-enabled=false
sudo pcs property set stonith-enabled=true
```
Désactivation du "Stonith"
``` bash
[root@rhos-ctrl-br-01 mysql]# pcs stonith status
#   * stonith-fence_ipmilan-00620bade462	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
#   * stonith-fence_ipmilan-00620bad1430	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
#   * stonith-fence_ipmilan-00620bac48b4	(stonith:fence_ipmilan):	 Stopped

# Fencing Levels:
#  Target: rhos-ctrl-br-01
#    Level 1 - stonith-fence_ipmilan-00620bac48b4
#  Target: rhos-ctrl-br-02
#    Level 1 - stonith-fence_ipmilan-00620bade462
#  Target: rhos-ctrl-br-03
#    Level 1 - stonith-fence_ipmilan-00620bad1430
```

``` bash
[root@rhos-ctrl-br-01 mysql]# pcs stonith disable  stonith-fence_ipmilan-00620bade462
[root@rhos-ctrl-br-01 mysql]# pcs stonith disable  stonith-fence_ipmilan-00620bad1430
[root@rhos-ctrl-br-01 mysql]# pcs stonith disable  stonith-fence_ipmilan-00620bac48b4
```

``` bash
pcs stonith status
#   * stonith-fence_ipmilan-00620bade462	(stonith:fence_ipmilan):	 Stopped (disabled)
#   * stonith-fence_ipmilan-00620bad1430	(stonith:fence_ipmilan):	 Stopped (disabled)
#   * stonith-fence_ipmilan-00620bac48b4	(stonith:fence_ipmilan):	 Stopped (disabled)

# Fencing Levels:
#  Target: rhos-ctrl-br-01
#    Level 1 - stonith-fence_ipmilan-00620bac48b4
#  Target: rhos-ctrl-br-02
#    Level 1 - stonith-fence_ipmilan-00620bade462
#  Target: rhos-ctrl-br-03
```
