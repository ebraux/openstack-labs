# OVN : reconstruire la base de données


---
## Principe

Après un problème lié au reploiement du noeud de bootstrap, le cluster d'OVN a été scindé en 2 (split-brain).

La [procédure pour sortir du split-brain](./ovn-split-brain.md) a fonctionné pour le service NorthBound, mais pas pour le service SouthBound.

Les 3 noeud sont dans un même cluster, mais :

- rhos-ctrl-br-01 is follower, but 'joining cluster'
- rhos-ctrl-br-02 is follower, but 'disconnected from the cluster (election timeout)'
- rhos-ctrl-br-03 is candidate, but 'disconnected from the cluster (election timeout)'

3 serveurs correspondent à rhos-ctrl-br-02, un seul est actif, mais les autres ne peuvent être supprimés. Ce qui doit provoquer le timeout.

``` bash
sudo podman exec ovn_cluster_south_db_server ovn-appctl -t /var/run/ovn/ovnsb_db.ctl cluster/kick OVN_Southbound  5591
failed to send removal request
ovn-appctl: /var/run/ovn/ovnsb_db.ctl: server returned an error
```

La solution consiste à purger le cluster, et le relancer à partir de la base d'un des noeuds, retenue comme base de référence. 

- Choix de la base de référence
    - Le noeud rhos-ctrl-01 a été réinstallé en dernier, et était hors de cluster. N epas le choisir
    - Il reste les 2 noeuds rhos-ctrl-02  ou rhos-ctrl-03
    - rhos-ctrl-03 a été choisi car il était en mode "candidate"
- Choix du nouveau serveur de bootstrap : pas de contrainte. Ici rhos-ctrl-01

---
## Arrêt du cluster

Arrêter le process sur tous les controlleurs
``` bash
systemctl stop tripleo_ovn_cluster_south_db_server 
systemctl status tripleo_ovn_cluster_south_db_server
```

Faire un backup
``` bash
cp -rp /var/lib/openvswitch/ovn /var/lib/openvswitch/ovn_bkp
```

Et purger les données
``` bash
rm /var/lib/openvswitch/ovn/{.,}ovnsb_db.*
```
---
## Gestion de la base de données

Copier la base de référence depuis le noeud retenu
```bash
# depuis rhos-ctrl-03
scp /var/lib/openvswitch/ovn_bkp/ovnsb_db.db stack@rhos-director-br-01.ctlplane:
```

Puis la transférer sur le nouveau noeud de boostrap
```bash
# depuis rhos-director-br-01
scp ovnsb_db.db ctrl1:/var/lib/openvswitch/ovn_bkp/ovnsb_db.db
```

La convertir du mode cluster au mode standalone
``` bash
ovsdb-tool cluster-to-standalone ovnsb_db_standalone.db ovnsb_db.db
ls -ltr
```
Supprimer le fichier de l'ancienne base
``` bash
rm ovnsb_db.db
```
Et convertir la base standalone, en mode de cluster de un seul noeud
``` bash
ovsdb-tool create-cluster ovnsb_db.db ovnsb_db_standalone.db tcp:192.168.174.51:6644
```

Relancer le conteneur `tripleo_ovn_cluster_south_db_server`
``` bash
systemctl start tripleo_ovn_cluster_south_db_server
```
Vérifier l'état du cluster
``` bash
ovs-appctl -t /var/lib/openvswitch/ovn/ovnsb_db.ctl cluster/status OVN_Southbound
# ef9d
# Name: OVN_Southbound
# Cluster ID: 1cb7 (1cb71675-5165-4823-bd9c-017d63ee16ed)
# Server ID: ef9d (ef9df234-8dc2-4375-9178-5b689ec59203)
# Address: tcp:192.168.174.51:6644
# Status: cluster member
# Role: leader
# Term: 1
# Leader: self
# Vote: self

# Last Election started 15133 ms ago, reason: timeout
# Last Election won: 15133 ms ago
# Election timer: 1000
# Log: [2, 95]
# Entries not yet committed: 0
# Entries not yet applied: 0
# Connections:
# Disconnections: 0
# Servers:
#     ef9d (ef9d at tcp:192.168.174.51:6644) (self) next_index=2 match_index=94
```

- Le status du noeud doit ête `cluster member`
- Il doit être `leader`
- La liste des serveurs doit êtr composée de 1 serveur (lui même)


Vérifier que les conteneurs sont "healthy"
``` bash
podman ps |grep ovn
```

---
## Ré-intégrer les autres noeuds


Relancer le service `tripleo_ovn_cluster_south_db_server`  sur 1 autre noeud :
``` bash
systemctl start tripleo_ovn_cluster_south_db_server
```

Le fichier de configuration et la base de données sont stockés séparément :

- Il rejoint automatiquement le cluster
- Puis synchronise la BDD.

Vérifier que la base `ovnsb_db.ctl` a été créée
``` bash
ls -ltrh /var/lib/openvswitch/ovn
```
Vérifier le status sur le noeud de bootstrap, et sur le noeud en court :
``` bash
ovs-appctl -t /var/lib/openvswitch/ovn/ovnsb_db.ctl cluster/status OVN_Southbound
```

- sur le noeud de bootstrap : le nombre de serveurs passe à 2
- sur le noeud en court :
    - Le status du noeud doit ête `cluster member`
    - Il doit être `follower`
    - Le leader doit être le noeud de bootstrap
    - La liste des serveurs doit être composée de 2 serveur : le noeud de bootstrap, et lui même
        - Status: cluster member

Vérifier que les conteneur sont "healthy"

``` bash
podman ps |grep ovn
```


Effectuer la même opération sur le dernier noeud.



---
## Autres infos

Fichier de configuration généré par Ansible : `cat /var/lib/config-data/ansible-generated/ovn/etc/sysconfig/ovn_cluster`