
---
## ERROR 'Can't read file /etc/corosync/corosync.conf: No such file or directory'

``` bash
sudo pcs status
# Error: error running crm_mon, is pacemaker running?
#   error: Could not connect to launcher: Connection refused
#   crm_mon: Connection to cluster failed: Connection refused
```


Corosync ne se lance pas
``` bash
systemctl status corosync
# ...
# Feb 06 14:01:40 rhos-ctrl-br-03 systemd[1]: Starting Corosync Cluster Engine...
# Feb 06 14:01:40 rhos-ctrl-br-03 corosync[19853]: Can't read file /etc/corosync/corosync.conf: No such file or directory
# Feb 06 14:01:40 rhos-ctrl-br-03 systemd[1]: corosync.service: Main process exited, code=exited, status=8/n/a
# Feb 06 14:01:40 rhos-ctrl-br-03 systemd[1]: corosync.service: Failed with result 'exit-code'.
# Feb 06 14:01:40 rhos-ctrl-br-03 systemd[1]: Failed to start Corosync Cluster Engine.
```

effectivement
``` bash
ll /etc/corosync/
# -rw-r--r--. 1 root root 1917 Nov 15  2022 corosync.conf.example
# drwxr-xr-x. 2 root root    6 Nov 15  2022 uidgid.d
```

La configuration de corosync est générée automatiquement quand un noeud rejoint le cluster.


Pour authentifier à nouveau un noeud, depuis un noeud du cluster
``` bash
sudo pcs host auth rhos-ctrl-br-03 addr=192.168.174.53 
```
Les login/mot de passe sont sur la machine director
Le login est hacluster, et le mot de passe est stocké sur la machine director

``` bash
cd /home/stack/config-download/overcloud
grep -R hacluster *
# Controller/config_settings.yaml:hacluster_pwd: XImCIQpGRAPZkRyx
# group_vars/Controller:  hacluster_pwd: XImCIQpGRAPZkRyx
```


Puis le ré-intégrer au cluster
``` bash
sudo pcs cluster node add  rhos-ctrl-br-03 addr=192.168.174.53 
# Disabling sbd...
# rhos-ctrl-br-03: sbd disabled
# Sending 'corosync authkey', 'pacemaker authkey' to 'rhos-ctrl-br-03'
# rhos-ctrl-br-03: successful distribution of the file 'corosync authkey'
# rhos-ctrl-br-03: successful distribution of the file 'pacemaker authkey'
# Sending updated corosync.conf to nodes...
# rhos-ctrl-br-03: Succeeded
# rhos-ctrl-br-02: Succeeded
# rhos-ctrl-br-01: Succeeded
# rhos-ctrl-br-02: Corosync configuration reloaded
```

Vérification
``` bash
ll /etc/corosync/
# total 12
# -r--------. 1 root root  256 Feb  6 17:40 authkey
# -rw-r--r--. 1 root root  672 Feb  6 17:40 corosync.conf
# -rw-r--r--. 1 root root 1917 Nov 15  2022 corosync.conf.example
# drwxr-xr-x. 2 root root    6 Nov 15  2022 uidgid.d
```

Relancer les services sur le Noeud HS
``` bash
sudo systemctl restart corosync
sudo systemctl status corosync

sudo systemctl restart pcsd
sudo systemctl status pcsd
```

Et relance des services du cluster
``` bash
sudo  pcs cluster start --all
```

Puis vérification
``` bash
sudo pcs status
# Cluster name: tripleo_cluster
# Status of pacemakerd: 'Pacemaker is running' (last updated 2024-02-06 17:59:00Z)
# ...
# Node List:
#   * Online: [ rhos-ctrl-br-01 rhos-ctrl-br-02 rhos-ctrl-br-03 ]
#   * GuestOnline: [ galera-bundle-0 galera-bundle-1 rabbitmq-bundle-0 rabbitmq-bundle-1 redis-bundle-0 redis-bundle-1 ]
```