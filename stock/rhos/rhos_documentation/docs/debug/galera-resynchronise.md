# Resynchronisation du cluster


---
## Description

Avec `pcs status`, les 3 noeuds sont "unpromoted.

Quand un des noeuds essaye de passer "propoted", il devient FAILED.

Le cluster est "désynchronisé".

Il faut tout d'abord essayer de relancer la procédure de synchronisation de pcs *(voir [Debug-Galera](./galera.md)*).

Sinon, si pacemaker n'arrive plus à appliquer automatiquement la procédure d'initialisation, il  faut faire les opérations manuellement: 

1. Ne démarrer qu'un seul noeud, pour le passer en "promoted"
2. réintégrer les autres noeuds



---
## Diagnostique de l'état du cluster


Définir quels sont les noueds du cluster.

Puis pour chaque noeud :

- Vérifier si il est sain ou nom :
    - Fichier "/var/lib/mysql/grastate.dat" : `safe_to_bootstrap: 1`
    - Variable Mysql : `wsrep_cluster_status	Primary`
- L'indice de la dernière transaction
    - Variable Mysql : `wsrep_last_committed	51405429`


Définir l'IP du noeud
``` bash
export SERVER_IP=10.129.173.51
```
 Lancer les commandes de diagnostique :
``` bash
ssh tripleo-admin@${SERVER_IP} "sudo podman exec \$(sudo podman ps -f name=galera-bundle -q)  cat /var/lib/mysql/grastate.dat"
# # GALERA saved state
# version: 2.1
# uuid:    0294aad1-8459-11ee-a21e-a7566c0bf657
# seqno:   -1
# safe_to_bootstrap: 1
```

 
``` bash
ssh tripleo-admin@${SERVER_IP} "sudo podman exec \$(sudo podman ps -f name=galera-bundle -q)  cat  /var/lib/mysql/gvwstate.dat"
# my_uuid: 0c5b684f-8459-11ee-a0b6-ff4609b89f35
# #vwbeg
# view_id: 3 0c5b684f-8459-11ee-a0b6-ff4609b89f35 5
# bootstrap: 0
# member: 0c5b684f-8459-11ee-a0b6-ff4609b89f35 0
# #vwend
```

``` bash
ssh tripleo-admin@${SERVER_IP} "sudo podman exec \$(sudo podman ps -f name=galera-bundle -q)  mysql -e \"SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status';\""
podman exec galera-bundle-podman-0  
# Variable_name	Value
# wsrep_cluster_status	Primary
```

``` bash
ssh tripleo-admin@${SERVER_IP} "sudo podman exec \$(sudo podman ps -f name=galera-bundle -q)  mysql -e \"SHOW GLOBAL STATUS LIKE 'wsrep_last_committed';\""
# Variable_name	Value
# wsrep_last_committed	6716
```

Refaire ce opération sur chaque noeud, pour définir quel sera le noeud de bootstrap du cluster : 

Normalement on prends celui qui a le `wsrep_last_committed` le plus élevé. Il doit également sain.

Si ce n'est pas possible, choisir un des noeud. Il y a alors un risque de perte de données


---
## Modification au niveau de la définition de la ressource :

Désactiver le stonith, pour permettre le démarrage d'une ressource sans quorum
``` bash
sudo pcs property set stonith-enabled=false
```

Désactiver la getsion de Galera par pacemaker
``` bash
pcs resource unmanage galera
```

S'assurer que le noeud retenu est actif (ici rhos-ctrl-br-01)
``` bash
sudo pcs node unstandby rhos-ctrl-br-01
```
Et désactiver les autres
``` bash
sudo pcs node standby rhos-ctrl-br-02
sudo pcs node standby rhos-ctrl-br-03
```
Afficher l'état des ressources dans pacemaker
``` bash
sudo pcs status
```


Afficher la configuration initiale pour `wsrep_cluster_address` et `cluster_host_map`:
``` bash
pcs resource config galera
# wsrep_cluster_address=gcomm://rhos-ctrl-br-0.internalapi.overcloud-prod.com,rhos-ctrl-br-1.internalapi.overcloud-prod.com,rhos-ctrl-br-2.internalapi.overcloud-prod.com
# ...
# cluster_host_map=rhos-ctrl-br-0:rhos-ctrl-br-0.internalapi.overcloud-prod.com;rhos-ctrl-br-1:rhos-ctrl-br-1.internalapi.overcloud-prod.com;rhos-ctrl-br-2:rhos-ctrl-br-2.internalapi.overcloud-prod.com
```

Modifier les valeurs :
``` bash
pcs resource update galera wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com

pcs resource update galera cluster_host_map=rhos-ctrl-br-01:rhos-ctrl-br-01.internalapi.overcloud-prod.com
```

Vérifier
``` bash
sudo pcs resource config galera | grep wsrep_cluster_address
#     wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com

sudo pcs resource config galera | grep cluster_host_map
#     cluster_host_map=rhos-ctrl-br-01:rhos-ctrl-br-01.internalapi.overcloud-prod.com
```


---
## Modifier la config de MariaDB sur le noeud de bootstrap

sur chaque noeud :
Faire une sauvegarde du fichier `/var/lib/config-data/puppet-generated/mysql/etc/my.cnf.d/galera.cnf` car il sera remis en place ensuite.


Afficher la configuration concernant `wsrep`
``` bash
cat /var/lib/config-data/puppet-generated/mysql/etc/my.cnf.d/galera.cnf  | grep wsrep
# wsrep_auto_increment_control = 1
# wsrep_causal_reads = 0
# wsrep_certify_nonPK = 1
# wsrep_cluster_address = gcomm://rhos-ctrl-br-0.internalapi.overcloud-prod.com,rhos-ctrl-br-1.internalapi.overcloud-prod.com,rhos-ctrl-br-2.internalapi.overcloud-prod.com
# wsrep_cluster_name = galera_cluster
# wsrep_convert_LOCK_to_trx = 0
# wsrep_debug = 0
# wsrep_drupal_282555_workaround = 0
# wsrep_on = ON
# wsrep_provider = /usr/lib64/galera/libgalera_smm.so
# wsrep_provider_options = gcache.recover=no;gmcast.listen_addr=tcp://192.168.174.51:4567;
# wsrep_retry_autocommit = 1
# wsrep_slave_threads = 1
# wsrep_sst_method = rsync
```

Puis l'éditer pour modifier `wsrep_cluster_address`.
``` bash
wsrep_cluster_address = gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com
```

---
## Réactiver la gestion du cluster par Pacemaker Avec un seul noeud


a vérifier ??
``` bash
pcs resource status  galera
#   * Container bundle set: galera-bundle [cluster.common.tag/mariadb:pcmklatest]:
#     * Replica[0]
#       * galera	(ocf:heartbeat:galera):	 Promoted galera-bundle-0 (unmanaged)
#     * Replica[1]
#       * galera	(ocf:heartbeat:galera):	 Stopped (unmanaged)
#     * Replica[2]
#       * galera	(ocf:heartbeat:galera):	 Stopped (unmanaged)
```

``` bash
pcs resource manage galera-bundle
# ou pcs resource manage galera

pcs resource cleanup
```
Mysql doit fonctionner, avec un seul noeud


POur vérifier la prise en compte des modifs 
``` bash
sudo podman exec $(sudo podman ps -f name=galera-bundle -q) mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status';"
# Variable_name	Value
# wsrep_cluster_status	Primary

# sudo podman exec $(sudo podman ps -f name=galera-bundle -q) mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_provider_options';"
# #

# sudo podman exec $(sudo podman ps -f name=galera-bundle -q) mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_address';"
```


``` bash
pcs resource status  galera
#   * Container bundle set: galera-bundle [cluster.common.tag/mariadb:pcmklatest]:
#     * Replica[0]
#       * galera	(ocf:heartbeat:galera):	 Promoted galera-bundle-0
#     * Replica[1]
#       * galera	(ocf:heartbeat:galera):	 Stopped
#     * Replica[2]
#       * galera	(ocf:heartbeat:galera):	 Stopped
```

Si ça ne fonctionne pas,voir [Validation du démarrage de mariaDB sur un seul noeud](./galera-start-single.md)


---
## Reconstruire le cluster

Re-modifier le fichier `/var/lib/config-data/puppet-generated/mysql/etc/my.cnf.d/galera.cnf` sur chaque noeud.

Ré-confgurer  :  `wsrep_cluster_address` et `cluster_host_map`


---
## réactiavtion des autres noeuds


``` bash
pcs resource update galera  wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com,rhos-ctrl-br-02.internalapi.overcloud-prod.com
```

```bash
pcs node unstandby rhos-ctrl-br-02
```

```bash
pcs stastus 
```


``` bash
pcs resource update galera  wsrep_cluster_address=gcomm://rhos-ctrl-br-01.internalapi.overcloud-prod.com,rhos-ctrl-br-02.internalapi.overcloud-prod.com,rhos-ctrl-br-03.internalapi.overcloud-prod.com
```

```bash
pcs node unstandby rhos-ctrl-br-03
```

```bash
pcs stastus 
```


---
## ré-activation du stonith
``` bash
[root@rhos-ctrl-br-01 mysql]# pcs stonith enable  stonith-fence_ipmilan-00620bade462
[root@rhos-ctrl-br-01 mysql]# pcs stonith enable  stonith-fence_ipmilan-00620bad1430
[root@rhos-ctrl-br-01 mysql]# pcs stonith enable  stonith-fence_ipmilan-00620bac48b4
```

[root@rhos-ctrl-br-01 ~]# pcs stonith status
  * stonith-fence_ipmilan-00620bade462	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-03
  * stonith-fence_ipmilan-00620bad1430	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-01
  * stonith-fence_ipmilan-00620bac48b4	(stonith:fence_ipmilan):	 Started rhos-ctrl-br-03

Fencing Levels:
 Target: rhos-ctrl-br-01
   Level 1 - stonith-fence_ipmilan-00620bac48b4
 Target: rhos-ctrl-br-02
   Level 1 - stonith-fence_ipmilan-00620bade462
 Target: rhos-ctrl-br-03
   Level 1 - stonith-fence_ipmilan-00620bad1430

``` bash
pcs property set stonith-enabled=true
```


---
## Infos dans les logs

``` bash
(log_op_output) 	info: galera_monitor_10000[25089] error output [ ocf-exit-reason:local node <rhos-ctrl-br-1> (galera node <rhos-ctrl-br-1.internalapi.overcloud-prod.com>) is started, but is not a member of the wsrep_cluster_address <gcomm://rhos-ctrl-br-0.internalapi.overcloud-prod.com> ]
```


---
## Ressources

- OpenStack Galera will not start with a failed Controller node and galera-bundle resources in Slave : [https://access.redhat.com/solutions/5981551](https://access.redhat.com/solutions/5981551)
