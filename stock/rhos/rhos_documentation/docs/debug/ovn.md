

créer un alias pour les commandes de gestion des bases
``` bash
alias ovn-sbctl="sudo podman exec -it ovn_cluster_south_db_server ovn-sbctl --no-leader"
```

Commande de requetage d'infos sur la SB database
``` bash
ovn-sbctl list encap
ovn-sbctl list chassis
```

Commandes de gestion des chassis
``` bash
ovn-sbctl list encap |grep -a3 192.168.175.51

_uuid               : d4f8933e-8958-40df-a66c-fef32b51b88f
chassis_name        : "8e08abda-4f2e-46cd-9226-0fac3409c186"
ip                  : "192.168.175.51"
options             : {csum="true"}
type                : geneve
```

ovn-sbctl chassis-del 8e08abda-4f2e-46cd-9226-0fac3409c186

---
## Openstack network agent

The openstack network agents are recreated based on the data in the OVN DB,


---
## Lister les routeurs et autre ports

``` bash
sudo podman exec -ti ovn_cluster_north_db_server bash
ovn-nbctl show 
```


- [https://access.redhat.com/solutions/6244371](https://access.redhat.com/solutions/6244371)