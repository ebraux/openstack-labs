# Information sur les souscriptions RedHat


---
## Gestion manuelle des souscriptions

``` bash
subscription-manager
# Primary Modules: 

#   attach         Attach a specified subscription to the registered system, when system does not use Simple Content Access mode
#   list           List subscription and product information for this system
#   refresh        Pull the latest subscription data from the server
#   register       Register this system to the Customer Portal or another subscription management service
#   release        Configure which operating system release to use
#   remove         Remove all or specific subscriptions from this system
#   status         Show status information for this system's subscriptions and products
#   unregister     Unregister this system from the Customer Portal or another subscription management service

# Other Modules:   

#   addons         Deprecated, see 'syspurpose'
#   auto-attach    Set if subscriptions are attached on a schedule (default of daily)
#   clean          Remove all local system and subscription data without affecting the server
#   config         List, set, or remove the configuration parameters in use by this system
#   environments   Display the environments available for a user
#   facts          View or update the detected system information
#   identity       Display the identity certificate for this system or request a new one
#   import         Import certificates which were provided outside of the tool
#   orgs           Display the organizations against which a user can register a system
#   plugins        View and configure with 'subscription-manager plugins'
#   redeem         Attempt to redeem a subscription for a preconfigured system
#   repo-override  Manage custom content repository settings
#   repos          List the repositories which this system is entitled to use
#   role           Deprecated, see 'syspurpose'
#   service-level  Deprecated, see 'syspurpose'
#   subscribe      Deprecated, see attach
#   syspurpose     Convenient module for managing all system purpose settings
#   unsubscribe    Deprecated, see remove
#   usage          Deprecated, see 'syspurpose'
#   version        Print version information
```
---
## Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)

- Élément : RH00191
- Numéro de contrat : 15037875
- Subscription Number : 11906804
- Pool ID: 2c9486ec83d72eeb0183fab02d0436ff
- 
``` bash
Subscription Name:   Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
Provides:            Red Hat Enterprise Linux for Power, big endian - Extended Update Support
                     Red Hat Enterprise Linux Fast Datapath
                     Red Hat Enterprise Linux for SAP Applications for Power BE - Extended Update Support
                     Red Hat Enterprise Linux Server
                     dotNET on RHEL (for RHEL Server)
                     Red Hat Software Collections (for RHEL Server for IBM Power)
                     Red Hat Software Collections Beta (for RHEL Server for IBM Power)
                     Red Hat Enterprise Linux Atomic Host Beta
                     Red Hat Container Images
                     Red Hat Virtualization Host
                     Red Hat OpenStack
                     Red Hat CloudForms Beta
                     Red Hat Enterprise Linux Atomic Host
                     Red Hat Enterprise Linux High Availability for x86_64 - Extended Update Support
                     Red Hat Developer Toolset (for RHEL Server)
                     Red Hat Enterprise MRG Messaging
                     Red Hat Ceph Storage Calamari
                     Red Hat Software Collections (for RHEL Server)
                     Red Hat CloudForms
                     Red Hat Enterprise Linux Resilient Storage for Power, little endian
                     Red Hat Enterprise Linux Advanced Virtualization
                     Red Hat Ansible Engine
                     Red Hat CodeReady Linux Builder for Power, little endian - Extended Update Support
                     Red Hat Developer Tools Beta (for RHEL Server)
                     Red Hat Enterprise Linux for SAP Applications for Power BE
                     Red Hat Software Collections Beta (for RHEL Server)
                     Red Hat Developer Tools (for RHEL Workstation)
                     Cinderlib
                     Oracle Java (for RHEL Client)
                     Red Hat Developer Tools (for RHEL Server for IBM Power LE)
                     Red Hat EUCJP Support (for RHEL Server) - Extended Update Support
                     Oracle Java (for RHEL Workstation)
                     Red Hat Enterprise Linux for SAP Applications for x86_64 - Extended Update Support
                     dotNET on RHEL (for RHEL Workstation)
                     Red Hat Enterprise Linux Scalable File System (for RHEL Server)
                     Red Hat Enterprise Linux Resilient Storage for IBM Power LE - Extended Update Support
                     Red Hat Developer Tools (for RHEL Server for IBM Power)
                     Oracle Java (for RHEL Compute Node)
                     Red Hat Enterprise Linux for Power, little endian Beta
                     Red Hat Enterprise Linux Workstation
                     Red Hat Virtualization
                     Red Hat Enterprise Linux for x86_64 - Extended Update Support
                     Red Hat Enterprise Linux Advanced Virtualization (for RHEL Server for IBM Power LE)
                     dotNET on RHEL (for RHEL Compute Node)
                     Red Hat Gluster Storage Server for On-premise
                     Red Hat Beta
                     Red Hat Enterprise Linux for x86_64
                     Red Hat Gluster Storage Nagios Server
                     Red Hat Software Collections Beta (for RHEL Server for IBM Power LE)
                     Red Hat Developer Tools (for RHEL Server for ARM)
                     dotNET on RHEL Beta (for RHEL Workstation)
                     Red Hat CodeReady Linux Builder for x86_64
                     Oracle Java (for RHEL Server)
                     Red Hat Enterprise Linux High Performance Networking (for RHEL Server) - Extended Update Support
                     Red Hat CodeReady Linux Builder for x86_64 - Extended Update Support
                     Red Hat Enterprise Linux Advanced Virtualization Beta
                     Red Hat Developer Tools Beta (for RHEL Workstation)
                     Red Hat Enterprise Linux for Scientific Computing
                     dotNET on RHEL Beta (for RHEL Compute Node)
                     Red Hat Software Collections (for RHEL Server for ARM)
                     Red Hat Gluster Storage Web Administration (for RHEL Server)
                     Red Hat Enterprise Linux High Availability for x86_64
                     Red Hat Single Sign-On
                     Red Hat Developer Tools Beta (for RHEL Server for ARM)
                     Red Hat Enterprise Linux High Availability (for IBM Power LE) - Extended Update Support
                     Red Hat Enterprise Linux Load Balancer (for RHEL Server)
                     Red Hat OpenStack Beta
                     Red Hat Enterprise Linux Desktop
                     dotNET on RHEL Beta (for RHEL Server)
                     Red Hat CodeReady Linux Builder for Power, little endian
                     Red Hat Enterprise Linux for Power, little endian - Extended Update Support
                     Red Hat Enterprise Linux for SAP Applications for Power LE - Extended Update Support
                     Red Hat Software Collections (for RHEL Server for IBM Power LE)
                     Red Hat Enterprise Linux Advanced Virtualization Beta (for RHEL Server for IBM Power LE)
                     Red Hat Ceph Storage MON
                     Red Hat Container Images Beta
                     Red Hat Developer Tools (for RHEL Server)
                     Red Hat Developer Toolset (for RHEL Workstation)
                     Red Hat Enterprise Linux for Power, big endian
                     Red Hat Software Collections Beta (for RHEL Server for ARM)
                     Oracle Java (for RHEL Server) - Extended Update Support
                     Red Hat S-JIS Support (for RHEL Server) - Extended Update Support
                     Red Hat Enterprise Linux for SAP Applications for Power LE
                     Red Hat Enterprise Linux Load Balancer (for RHEL Server) - Extended Update Support
                     Red Hat Enterprise Linux Fast Datapath Beta for x86_64
                     Red Hat Virtualization Manager
                     Red Hat Software Collections Beta (for RHEL Workstation)
                     Red Hat Ceph Storage
                     Red Hat Gluster Storage Management Console (for RHEL Server)
                     Red Hat Enterprise Linux for Power 9
                     Red Hat Enterprise Linux Scalable File System (for RHEL Server) - Extended Update Support
                     JBoss Enterprise Application Platform
                     Red Hat Software Collections (for RHEL Workstation)
                     Red Hat Developer Tools Beta (for RHEL Server for IBM Power LE)
                     Red Hat Enterprise Linux for SAP Applications for x86_64
                     Red Hat JBoss Core Services
                     Red Hat Enterprise Linux Resilient Storage for x86_64 - Extended Update Support
                     Red Hat Enterprise Linux for Power, little endian
                     Red Hat Enterprise Linux Resilient Storage for x86_64
SKU:                 RH00191
Contract:            15037875
Pool ID:             2c9486ec83d72eeb0183fab02d0436ff
Provides Management: Yes
Available:           250000
Suggested:           1
Service Type:        L1-L3
Roles:               
Service Level:       Standard
Usage:               Production
Add-ons:             
Subscription Type:   Standard
Starts:              10/20/2022
Ends:                10/20/2023
Entitlement Type:    Physical
```

---
## Red Hat Ceph Storage, Premium (Up to 256TB on a maximum of 12 Physical Nodes)

- Élément : RS00036
- Numéro de contrat : 15037875
- Subscription Number : 11906804

``` bash
Subscription Name:   Red Hat Ceph Storage, Premium (Up to 256TB on a maximum of 12 Physical Nodes)
Provides:            Red Hat Ceph Storage
                     Red Hat Software Collections (for RHEL Server)
                     Red Hat Enterprise Linux for x86_64
                     Red Hat CodeReady Linux Builder for x86_64
                     Red Hat Ceph Storage MON
                     Red Hat Enterprise Linux Scalable File System (for RHEL Server)
                     Red Hat Ansible Engine
                     Red Hat OpenStack Director Deployment Tools Beta for IBM Power LE
                     Red Hat Storage Console Node
                     Red Hat OpenStack Director Deployment Tools Beta
                     Red Hat Enterprise Linux Server
                     Red Hat Ceph Storage OSD
                     Red Hat Ceph Storage Calamari
                     Red Hat OpenStack Director Deployment Tools
                     Red Hat OpenStack Director Deployment Tools for IBM Power LE
                     Red Hat Storage Console
SKU:                 RS00036
Contract:            15037875
Pool ID:             2c9486ec83d72eeb0183fab02ef63701
Provides Management: No
Available:           12
Suggested:           1
Service Type:        L1-L3
Roles:               
Service Level:       Premium
Usage:               
Add-ons:             
Subscription Type:   Stackable
Starts:              10/20/2022
Ends:                10/20/2023
Entitlement Type:    Physical
```



``` bash
De: "Red Hat Workflow" <customerservice-emea@redhat.com>
À: "Emmanuel BRAUX" <emmanuel.braux@imt-atlantique.fr>
Envoyé: Vendredi 21 Octobre 2022 15:30:57
Objet: Votre commande Red Hat a été validée

 
Cher/Chère client(e) Red Hat,

Merci d'avoir acheté un abonnement Red Hat. Nous avons reçu et traité la commande indiquée ci-dessous.

N° de bon de commande : n/a
N° de compte : 474687
Revendeur : STARTX SARL

*Si vous avez des questions concernant la commande, veuillez contacter le revendeur ou le distributeur qui vous a vendu l'abonnement*. Si vous avez acheté votre abonnement auprès de Red Hat ou que vous avez des questions concernant le produit ou le service, veuillez contacter un représentant du service clientèle Red Hat de votre région à l'adresse access.redhat.com/customerservice

Si ce compte n'est pas encore associé à un identifiant, vous allez recevoir un message électronique contenant des instructions relatives à la création d'un identifiant. Ce message électronique est le seul moyen vous permettant de créer un identifiant associé à votre compte. Merci de contacter le service clientèle à l'adresse ci-dessous si vous n'avez pas reçu ce message d'ici une heure: access.redhat.com/customerservice

 Détails de la commande:
********
Description : Red Hat Infrastructure for Academic Institutions Site Subscription, Standard (per FTE)
Élément : RH00191
Numéro de contrat : 15037875
Dates de mise en service : 20-OCT-2022 au 19-OCT-2023
Quantité : 1000
********
Description : Red Hat Ceph Storage, Premium (Up to 256TB on a maximum of 12 Physical Nodes)
Élément : RS00036
Numéro de contrat : 15037875
Dates de mise en service : 20-OCT-2022 au 19-OCT-2023
Quantité : 1
********
    

Votre commande Red Hat a été traitée avec succès.

--> Commencez à utiliser votre abonnement dès maintenant en cliquant sur le lien suivant: http://www.redhat.com/start
--> Consultez vos modalités d'abonnement et vos options d'assistance Web par le biais du service clients: http://access.redhat.com/subscriptions
--> Vous souhaitez parler à un conseiller Red Hat? Contactez-nous: http://www.redhat.com/contact
--> Merci de prendre connaissance des conditions générales d'utilisation du contrat d'entreprise : https://www.redhat.com/fr/about/licenses

Remarque: vous avez reçu cet avis car vous avez été désigné(e) comme administrateur technique de ce compte. Merci de faire suivre cet avis aux autres personnes de votre entreprise que cela concerne. Si vous avez besoin d'assistance, contactez le représentant du service clientèle Red Hat de votre région répertorié dans la liste suivante: access.redhat.com/customerservice

Les clients disposant d'abonnements actifs à l'assistance technique peuvent obtenir de l'aide en contactant un ingénieur Red Hat de leur région à l'adresse: https://access.redhat.com/support

Ce message est généré automatiquement.
```