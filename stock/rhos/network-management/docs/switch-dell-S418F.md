# Configuration des switchs DELL



---
## Initialisation


Le système de gestion des switch DELL est Onie(). Les switch DELL ont déjà un OS10 préinstallé.
Tant que le système n'est pas configuré, on a un shell avec le prompt "ONIE".
Une fois configuré, on a un shell avec le prompt "OS10#"


Connect to the serial console port: the serial settings are 115200 baud, 8 data bits, and no parity

Pour se connecter la première fois :

- OS10 login: admin
- Password: admin


```
"OS10 Enterprise Edition may come factory-loaded and is available for download from the Dell Digital Locker (DDL).
A factory-loaded OS10 image has a perpetual license installed.
An OS10 image that you download has a 120-day trial license and requires a perpetual license to run beyond the trial period.
See the Quick Start Guide shipped with your device and My Account FAQs for more information."
```

docs :

- [https://topics-cdn.dell.com/pdf/force10-s4048-on_reference-guide7_en-us.pdf(https://topics-cdn.dell.com/pdf/force10-s4048-on_reference-guide7_en-us.pdf)]
- doc vxrail : [https://infohub.delltechnologies.com/section-assets/vxrail-networking-guide-with-dell-emc-s4148-on-switches](https://infohub.delltechnologies.com/section-assets/vxrail-networking-guide-with-dell-emc-s4148-on-switches)
- doc profile : [https://infohub.delltechnologies.com/l/dell-emc-networking-virtualization-overlay-with-bgp-evpn-10/configure-switch-port-profile-s4148u-on-only-6](https://infohub.delltechnologies.com/l/dell-emc-networking-virtualization-overlay-with-bgp-evpn-10/configure-switch-port-profile-s4148u-on-only-6)

- [OpenSwitch OPX Installation Guide with
Enhancement Package](./files/all-products_esuprt_ser_stor_net_esuprt_networking_force10-s4048-on_owners-manual6_en-us.pdf)
  - [https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/force10-s4048-on_Owners-Manual6_en-us.pdf]
  - [https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/force10-s4048-on_Owners-Manual6_en-us.pdf](https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/force10-s4048-on_Owners-Manual6_en-us.pdf)

Plus d'infos sur ce que j'avais fait :  https://edisi.telecom-bretagne.eu/architectures/rhos/10_network_infra/switch_DELL/



---
## connexion



Pour se connecter la première fois :


OS10 login: admin
Password: admin



---
## Information sur le switch

Nom du switch
``` bash
OS10(conifg)# hostname stack10g-01
# stack10g-01(config)# 
```

``` bash
show license status

# System Information
# ---------------------------------------------------------
# Vendor Name          :   Dell EMC
# Product Name         :   S4148F-ON
# Hardware Version     :   A05
# Platform Name        :   x86_64-dellemc_s4148f_c2338-r0
# PPID                 :   CN0R2RKC282980550002
# Service Tag          :   9FMFPK2
# Product Base         :   
# Product Serial Number:   
# Product Part Number  :   
# License Details
# ----------------
# Software        :        OS10-Enterprise
# Version         :        10.5.2.11
# License Type    :        PERPETUAL
# License Duration:        Unlimited
# License Status  :        Active
# License location:        /mnt/license/9FMFPK2.lic
# ---------------------------------------------------------
```

Vérification de la licence ???

``` bash
enable                                                                    
configure terminal  
```

``` bash
show system

# Node Id              : 1
# MAC                  : 68:4f:64:e9:a3:55
# Number of MACs       : 256
# Up Time              : 8 weeks 3 days 21:38:14

# -- Unit 1 --
# Status                     : up
# System Identifier          : 1
# Down Reason                : unknown
# Digital Optical Monitoring : disable
# System Location LED        : off
# Required Type              : S4148F
# Current Type               : S4148F
# Hardware Revision          : A05
# Software Version           : 10.5.2.11
# Physical Ports             : 48x10GbE, 2x40GbE, 4x100GbE
# BIOS                          : 3.33.0.1-7    
# System CPLD                   : 1.1           
# Master CPLD                   : 1.0           
# Slave CPLD                    : 0.7           

# -- Power Supplies --
# PSU-ID  Status      Type    AirFlow   Fan  Speed(rpm)  Status
# ----------------------------------------------------------------
# 1       up          AC      REVERSE   1    11696       up         
# 2       up          AC      REVERSE   1    11664       up         

# -- Fan Status --
# FanTray  Status      AirFlow   Fan  Speed(rpm)  Status
# ----------------------------------------------------------------
# 1        up          REVERSE   1    10983       up         
# 2        up          REVERSE   1    11045       up         
# 3        up          REVERSE   1    10983       up         
# 4        up          REVERSE   1    10983       up         
```                               




---
## Informations sur la configuration

2 types de configuration :

- candidate-configuration
- running-configuration

Pour afficher les différences
``` bash
show diff candidate-configuration running-configuration
```

Afficher une version synthétique de la configuration
``` bash
show candidate-configuration compressed
```


---
## Opérations

Sauvegarder la config 
``` bash
write memory
```

Redémarrer le switch
``` bash
reload
```


---
## Configuration 


### Opérations réalisées :

- Configuration de l'interface de management, avec un IP sur le réseau IPMI
- Ajout des Vlan 172 à 179
- [TODO] création d'un utilisateur d'admin "root", avec le mot de passe IPMI
- [TODO] sécurisation de l'utilisateur "admin" (clé uniquement ?)


show running-configuration
Vlans :
 vlan172
 vlan173
 vlan174
 vlan175
 vlan176
 vlan177
 vlan178
 vlan178

Interfaces


# show vlan 
Codes: * - Default VLAN, M - Management VLAN, R - Remote Port Mirroring VLANs,
       @ - Attached to Virtual Network, P - Primary, C - Community, I - Isolated
Q: A - Access (Untagged), T - Tagged
    NUM    Status    Description                     Q Ports
*   1      Active                                    A Eth1/1/1 
    172    Active                                    A Eth1/1/2-1/1/24,1/1/31-1/1/54 
    173    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    174    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    175    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    176    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    177    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    178    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 
    179    Active                                    T Eth1/1/5-1/1/24,1/1/31-1/1/50 


**Management**
``` bash
interface mgmt1/1/1
 no shutdown
 no ip address dhcp
 ip address 10.29.20.201/24
 ipv6 address autoconfig
```

**Lien VSS**
- interface ethernet1/1/1
``` bash
description "Sw Dell --> VSS"
no shutdown
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 172,178
negotiation off
flowcontrol receive on
no flowcontrol transmit
no spanning-tree port type edge
```

**Boot 172**
interface range ethernet 1/1/2-1/1/4
interface range ethernet 1/1/51-1/1/54

``` bash
description "Infra Vlan"
no shutdown
switchport mode access
switchport access vlan 172
no switchport trunk allowed vlan 173-179
flowcontrol receive on
flowcontrol transmit off
spanning-tree port type edge
```


**Standard**
interface range ethernet 1/1/5-1/1/24
interface range ethernet 1/1/31-1/1/50

``` bash
description "RHOS Default"
no shutdown
switchport mode trunk
switchport access vlan 172
switchport trunk allowed vlan 173-179
flowcontrol receive on
flowcontrol transmit off
spanning-tree port type edge
```

 
``` bash
Switch #1
MGMT
  IP :  10.29.20.201/24
  port : SW-D01-03 port 22
ports
 1/4  : p2 --> VSS
 5/23 : VLANs Openstack
25 -> 30 : interconnexion
31/53 : VLANs Openstack


 

Switch #2

MGMT
  IP :  10.29.20.202/24
  port : SW-D01-03 port 23
ports
 1/23 : VLANs Openstack
25 -> 30 : interconnexion
31/53 : VLANs Openstack


### Interface de management

``` bash
OS10# config
OS10(config)# interface mgmt 1/1/1
OS10(conf-if-ma-1/1/1)# no ip address dhcp
OS10(conf-if-ma-1/1/1)# ip address 10.129.172.201/24
OS10(conf-if-ma-1/1/1)# no shutdown
OS10(conf-if-ma-1/1/1)# management route 0.0.0.0/0 10.129.172.1
OS10(conf-if-ma-1/1/1)# exit
OS10(config)# exit



Proceed to reboot the system? [confirm yes/no]:y
```

Vérification
``` bash
configure terminal
stack10g-01(config)# show interface mgmt 1/1/1
Management 1/1/1 is up, line protocol is up
Hardware is Eth, address is 68:4f:64:e9:a3:55
    Current address is 68:4f:64:e9:a3:55
Interface index is 11
Internet address is 10.29.20.201/24
Mode of IPv4 Address Assignment: MANUAL
Interface IPv6 oper status: Enabled
Link local IPv6 address: fe80::6a4f:64ff:fee9:a355/64
Virtual-IP is not set
Virtual-IP IPv6 address is not set
MTU 1532 bytes, IP MTU 1500 bytes
LineSpeed 1000M
Flowcontrol rx off tx off
ARP type: ARPA, ARP Timeout: 60
Last clearing of "show interface" counters: 8 weeks 3 days 21:47:34
Queuing strategy: fifo
Input statistics:
     Input 472968 packets, 53176797 bytes, 2 multicast
     Received 0 errors, 0 discarded
Output statistics:
     Output 380468 packets, 72259660 bytes, 173843 multicast
     Output 0 errors, Output  invalid protocol
Time since last interface status change: 8 weeks 3 days 21:43:23
```

### Utilisateur root

Création d'utilisateurs
``` bash
OS10(conf-if-ma-1/1/1)# username imta-admin password alpha404! role sysadmin
```

### iscsi

``` bash
iscsi enable
!
class-map type application class-iscsi
!
policy-map type application policy-iscsi
!
```


---
## configuration avancée

---
## Update OS

doc : 
- https://www.dell.com/support/manuals/fr-fr/dell-emc-smartfabric-os10/ee-upgrade-downgrade/upgrade-os10-on-vlt-nodes-with-minimal-traffic-loss?guid=guid-b8d9e1d5-e4a8-422f-bd4c-42a49a78d712&lang=en-us
- https://www.dell.com/community/Networking-General/OS10-upgrade/td-p/7668632



Vérifier les droits : Role should be sysadmin, privilege level 15

```bash
stack10g-02# show users
 
Index  Line    User           Role         Application    Idle     Login-Time                   Location               Privilege-Level
-----  -----   ------------   ------       ------------   -----    --------------------------   ---------------------  --------------- 
1      pts/0   admin          sysadmin     bash           0.8s     2021-07-30     T 14:46:50Z   10.129.41.176 [ssh]    15
```


Sur le stack-02

```bash
stack10g-02# image download scp://root@192.44.75.9:/usr/tftpdir/DELL/PKGS_OS10-Enterprise-10.4.3.7.289stretch-installer-x86_64.bin
stack10g-02# show image status
```
Quand le "Transfer Progress:" est  à 100%, pour vérifer que lebin est bien là :
```bash 
dir image
```

Ensuite, installer l'image
```bash 
image install image://PKGS_OS10-Enterprise-10.4.3.7.289stretch-installer-x86_64.bin
show image status
---
 - In progress: Installing
 - In progress: Configure filesystem (3 of 10)
 - In progress: Configure filesystem (5 of 10)
 - ...
```
==> là ça plante : `State Detail:          Failed: Operation not permitted`


Quand le "State Detail" indique
```bash 
boot system standby
write memory
copy config://startup.xml config://20210730_MAJ
reload
```

Attendre le redémarrage
```bash 
show vlt 1
```





### Switch Port Profiles
On peut définir des switch port profile (voir doc vxrail, page 16)
- doc vxrail : [https://infohub.delltechnologies.com/section-assets/vxrail-networking-guide-with-dell-emc-s4148-on-switches](https://infohub.delltechnologies.com/section-assets/vxrail-networking-guide-with-dell-emc-s4148-on-switches)


### Zero-touch deployment

Zero-touch deployment (ZTD) allows OS10 users to automate switch deployment: 28 Getting Started

- Upgrade an existing OS10 image. 
- Execute a CLI batch file to configure the switch.
- Execute a post-ZTD script to perform additional functions.

``` bash
show ztd-status

-----------------------------------
ZTD Status     : disabled
ZTD State      : init
Protocol State : idle
Reason         : 
-----------------------------------


---
## exemple config

# DELL #1  (68:4f:64:e9:a3:55)

```language
ssh admin@10.29.20.201
```

## Intiliatisation
```bash
# Lien VSS
interface range ethernet 1/1/1,1/1/2
description "Sw Dell --> VSS"
no shutdown
channel-group 50 mode active
no switchport
flowcontrol receive on

# Config LACP
interface range ethernet 1/1/25-1/1/30
no switchport mode trunk
no switchport access vlan 1
no switchport trunk allowed vlan 1,172-179
no shutdown
no switchport
mtu 9000
flowcontrol receive on

# Default configuration
interface range ethernet 1/1/3-1/1/24,1/1/31-1/1/54
description "Default"
no shutdown
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 173-179
mtu 9000
flowcontrol receive on
```

## Configuration
```bash
#MOSK CTRL/Compute NIC0
interface range ethernet 1/1/1,1/1/3,1/1/11,1/1/23,1/1/31,1/1/32,1/1/51,1/1/53 
description "MOSK-NIC0"
switchport mode trunk
switchport access vlan 172
no switchport trunk allowed vlan 173-179
mtu 9000


#MOSK CTRL/Compute BOND0
interface range ethernet 1/1/7,1/1/21,1/1/50,1/1/4,1/1/33,1/1/52,1/1/13,1/1/17,1/1/35
description "MOSK-BOND0"
switchport access vlan 1
switchport mode trunk
switchport trunk allowed vlan 173-179
mtu 9000

#MOSK CTRL/Compute BOND1
interface range ethernet 1/1/8,1/1/22,1/1/47,1/1/5,1/1/34,1/1/49,1/1/14,1/1/18,1/1/36
description "MOSK-BOND1"
switchport access vlan 1
switchport mode trunk
switchport trunk allowed vlan 173-179
mtu 9000
```




# DELL #2  (68:4f:64:e9:7c:55)

```language
ssh admin@10.29.20.201
```

## Intiliatisation
``` bash
# Lien VSS
interface range ethernet 1/1/1,1/1/2
description "Sw Dell --> VSS"
no shutdown
channel-group 50 mode active
no switchport
flowcontrol receive on

# Config LACP
interface range ethernet 1/1/25-1/1/30
no switchport mode trunk
no switchport access vlan 1
no switchport trunk allowed vlan 1, 172-179
no shutdown
no switchport
mtu 9000
flowcontrol receive on

# Default configuration
interface range ethernet 1/1/3-1/1/24,1/1/31-1/1/54
description "Default"
no shutdown
switchport mode trunk
switchport access vlan 1
no switchport trunk allowed vlan 172-179
switchport trunk allowed vlan 173-179
mtu 9000
flowcontrol receive on

```

## Configuration

```bash

#MOSK CTRL/Compute NIC0
interface range ethernet 1/1/3,1/1/51
description "MOSK-NIC0"
switchport mode trunk
switchport access vlan 172
no switchport trunk allowed vlan 173-179
mtu 9000


#MOSK CTRL/Compute BOND0
interface range ethernet 1/1/8,1/1/22,1/1/47,1/1/4,1/1/33,1/1/52,1/1/36
description "MOSK-BOND0"
switchport access vlan 1
switchport mode trunk
switchport trunk allowed vlan 173-179
mtu 9000

#MOSK CTRL/Compute BOND1
interface range ethernet 1/1/7,1/1/21,1/1/50,1/1/5,1/1/34,1/1/49,1/1/35
description "MOSK-BOND1"
switchport access vlan 1
switchport mode trunk
switchport trunk allowed vlan 173-179
mtu 9000
```

