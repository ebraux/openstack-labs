

switchs Nexus 9300, P/N N9K-C93108TC-EX, 

(48 ports 100M/1/10GBASE-T et 6 ports QSFP28 40/100 Gbit/s)


N9K-C93108TC-EX-PI Nexus 9300 with 48p 10G BASE-T and 6p 100G QSFP28, Reversed Airflow (Port side
intake), 2 x NXA-PAC-650W-PI, 4 x NXA-FAN-30CFM-B

Airflow :
front : les prises
back : les ventilo et les alim
besoin airflow : de back vers front
 - Forward airflow (Port-side Exhaust) : back to front ?
 - Reverse airflow (Port-side Intake) : front to back ?

 
  and port 

- Port-side Exhaust
    - Port-side exhaust airflow with blue coloring 
    - side exhaust means hot is exiting from the port side
    - Standard-airflow fan: This option provides front-to-back airflow, with ports aligned with the hot aisle (port-side exhaust). This option is ideal for ToR deployments to align the fabric extender ports with the server ports in a given rack
    - Port-side exhaust: In this configuration, air is drawn into the switch through the power supply side and expelled through the port side. Therefore, the cold aisle should be on the same side as the power supplies. This configuration is typically used when switches are installed in a reverse (ports to the back) orientation in the rack.
    - If the switch has port-side exhaust airflow (blue coloring for fan modules), you must locate the ports in the hot aisle.
    - If you have hot aisle isolation in your datacenter and you're mounting your switches with the ports facing the hot aisle (typically the back of the rack), you want port-side exhaust
    - Standard-airflow: This option provides front-to-back airflow, with ports aligned with the hot aisle (port-side exhaust).
-  port-side intake
    - Port-side intake airflow with burgundy coloring (
    - Port side intake means the cold air enter the switch where the ports are
    - Reversed-airflow fan: This option provides back-to-front airflow, with ports aligned with the cold aisle (port-side intake). This option is ideal for network rack deployments in which various network equipment ports need to be aligned
    - Port-side intake: In this configuration, air is drawn into the switch through the port side (the side with the ethernet or fiber ports) and expelled through the power supply side. This means that the cold aisle (where the cooler, ambient air is) should be on the same side as the ports. This configuration is typically used when switches are installed in a typical front-to-back (ports to the front) orientation in the rack.
    - If the switch has port-side intake airflow (burgundy coloring for fan modules), you must locate the ports in the cold aisle.
    -  If you're mounting the switches with the ports facing the cold aisle (typically the front of the rack), you want port-side intake.
    - Reversed-airflow: This option provides back-to-front airflow, with ports aligned with the cold aisle (port-side intake)."


- [https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-9300-series-switches/nb-06-cat9300-architecture-cte-en.html#Chassisairflow](https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-9300-series-switches/nb-06-cat9300-architecture-cte-en.html#Chassisairflow)
- [https://cordero.me/cisco-nexus-port-side-exhaust-vs-port-side-intake/](https://cordero.me/cisco-nexus-port-side-exhaust-vs-port-side-intake/)



Ressources :
- [https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-742283.html](https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-742283.html)
- Cisco Nexus 93108TC-EX-24 :  [https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-743149.html](https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-743149.html)
- [https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-736967.html](https://www.cisco.com/c/en/us/products/collateral/switches/nexus-9000-series-switches/datasheet-c78-736967.html)