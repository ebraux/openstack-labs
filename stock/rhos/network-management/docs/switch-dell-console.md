

https://www.ismoothblog.com/2019/07/access-cisco-switch-serial-console-linux.html

https://www.cyberciti.biz/hardware/5-linux-unix-commands-for-connecting-to-the-serial-console/
https://unix.stackexchange.com/questions/22545/how-to-connect-to-a-serial-port-as-simple-as-using-ssh
https://www.dell.com/community/Networking-General/Init-setup-on-Power-Connect-2848/td-p/3927765
https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/esuprt_net_fxd_prt_swtchs/powerconnect-2848_setup%20guide_en-us.pdf
https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/esuprt_net_fxd_prt_swtchs/powerconnect-2848_user's%20guide_en-us.pdf
https://dl.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_networking/esuprt_net_fxd_prt_swtchs/powerconnect-6248_reference%20guide2_en-us.pdf


apt install minicom

dmesg | grep tty
[    0.842269] printk: console [tty0] enabled
[    1.606861] 00:02: ttyS1 at I/O 0x2f8 (irq = 3, base_baud = 115200) is a 16550A
[    1.627318] 00:03: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A

2 ports série : ttyS0 et ttyS1

configurer la connexion :
- port /dev/ttys0

Configuration DELL 2848
- data rate to 9600 baud.
- data format : 8 data bits, 1 stop bit, and no parity.
- Set flow control to none.
- Emulation mode : VT100 for 

minicom -s
selectionner : 
"Configuration du port série"
``` bash
   +-----------------------------------------------------------------------+
    | A -                             Port série : /dev/modem               |
    | B - Emplacement du fichier de verrouillage : /var/lock                |
    | C -            Programme d'appel intérieur :                          |
    | D -            Programme d'appel extérieur :                          |
    | E -                      Débit/Parité/Bits : 115200 8N1               |
    | F -              Contrôle de flux matériel : Oui                      |
    | G -              Contrôle de flux logiciel : Non                      |
    |                                                                       |
    |    Changer quel réglage ?                                             |
    +-----------------------------------------------------------------------+
            | Ecran et clavier                      |
            | Enregistrer config. sous dfl          |
            | Enregistrer la configuration sous...  |
            | Sortir                                |
            | Sortir de Minicom                     |
            +---------------------------------------+
```

Configuration du port série. menu "A"

Menu E
+-----------------+---[Paramètres de communication]----+----------------+
    | A -             |                                    |0               |
    | B - Emplacement |     Actuellement : 115200 8N1      |                |
    | C -            P| Speed            Parity      Data  |                |
    | D -            P| A: <next>        L: None     S: 5  |                |
    | E -             | B: <prev>        M: Even     T: 6  |1               |
    | F -             | C:   9600        N: Odd      U: 7  |                |
    | G -             | D:  38400        O: Mark     V: 8  |                |
    |                 | E: 115200        P: Space          |                |
    |    Changer quel |                                    |                |
    +-----------------| Stopbits                           |----------------+
            | Ecran et| W: 1             Q: 8-N-1          |
            | Enregist| X: 2             R: 7-E-1          |
            | Enregist|                                    |
            | Sortir  |                                    |
            | Sortir d| Choix, ou <Entrée> pour sortir ?   |
            +---------+------------------------------------+

C / L /V / 1
--> Actuellement :  9600 8N1 


controle de flux "F"

résultat
 +-----------------------------------------------------------------------+
    | A -                             Port série : /dev/ttyS0               |
    | B - Emplacement du fichier de verrouillage : /var/lock                |
    | C -            Programme d'appel intérieur :                          |
    | D -            Programme d'appel extérieur :                          |
    | E -                      Débit/Parité/Bits : 9600 8N1                 |
    | F -              Contrôle de flux matériel : Non                      |
    | G -              Contrôle de flux logiciel : Non                      |
    |                                                                       |
    |    Changer quel réglage ?                                             |
    +-----------------------------------------------------------------------+

         | Enregistrer config. sous dfl          |       
            | Enregistrer la configuration sous...  | dell2848

