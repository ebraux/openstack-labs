
show running-config int GigabitEthernet1/0/23



Utiliser telnet. avec ssh, ça marche pas bien.

- `telnet stack-d01-02.net.enst-bretagne.fr`
- `telnet sw-d01-03.net.enst-bretagne.fr`

Top of rack :
telnet sw-d01-04.net.enst-bretagne.fr

telnet stack-d01-01.net.enst-bretagne.fr

telnet d1-8.net.enst-bretagne.fr
telnet d1-23.net.enst-bretagne.fr
telnet d1-30.net.enst-bretagne.fr
telnet d1-31.net.enst-bretagne.fr
telnet d1-32.net.enst-bretagne.fr


telnet d1-4.net.enst-bretagne.fr

MEE : 
telnet k2-12.net.enst-bretagne.fr
 GigabitEthernet0/5    (pas dans l'appli)
telnet d1-30.net.enst-bretagne.fr
 GigabitEthernet0/2    (pas dans l'appli)
telnet d1-8.net.enst-bretagne.fr
  GigabitEthernet0/2    (conf en 775_not1x)
telnet d1-8.net.enst-bretagne.fr
  GigabitEthernet0/2    (pas dans l'appli)
telnet d1-31.net.enst-bretagne.fr
 GigabitEthernet1/0/19 (pas dans l'appli)

rtfretis218
trfeXoNiuQe207


---
## Informations sur le switch

``` bash
sw-d01-04#show inventory
# NAME: "1", DESCR: "WS-C2960X-24TD-L"
# PID: WS-C2960X-24TD-L  , VID: V02  , SN: FCW1802A0JK
```

``` bash
show version 
# Cisco IOS Software, C2960X Software (C2960X-UNIVERSALK9-M), Version 15.2(5)E, RELEASE SOFTWARE (fc1)
# ...
# Model number      : WS-C2960X-24TD-L
#  ...
```

Information sur les modules :
``` bash
show interfaces
show interface TenGigabitEthernet1/0/1
```
---
## Afficher des informations

Voir la config globale :
``` bash
show startup-config
```

Résumé des interfaces
``` bash
show ip interface brief
```

Détail de la configuration d'une interface
``` bash
> show running-config int GigabitEthernet0/20
> show interfaces GigabitEthernet0/20
```

Voir les adresse mac
```bash
show mac address-table
show mac address-table interface Gi1/0/1
```

pour lTotal Mac Addresses for this criterion: 704
``` bash
> show mac address-table interface %s"

show mac address-table dynamic
show mac address-table dynamic vlan 173
show mac address-table dynamic int Gi1/0/1
```

## configuration des interfaces

Activer le mode configuration
``` bash
sw-d01-03#conf t
#Enter configuration commands, one per line.  End with CNTL/Z.
```
Sélectionner l'interface à modifier

- Individuellement
``` bash
sw-d01-03(config)#interface gi1/0/5
```
- Par "groupe" : exemple de l'interface 5 à la 10
``` bash
sw-d01-03(config)#interface range gi1/0/5 - 10
```
 Appliquer les modifications

 Sortir du mode config
 ``` bash
 end
 ```

Vérifier la config
``` bash
show running-config int GigabitEthernet1/0/21
```

Une fois validée, valider la config. Sinon elle sera effacée au prochain reboot
``` bash
write mem
```

Exemple de modification

``` bash
#  --- Openstack V2 : 173 ---
 description director NIC2
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 173
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
```

Pour supprimer un ligne, ajouter `no` devant
``` Bash
no switchport trunk native vlan 173
```



# valider la config
(config)# write mem
---
## Activation/désactivation des interfaces

desactiver une interface :

```bash
sw-d01-03#show interfaces Gi1/0/5 status 
Port      Name               Status       Vlan       Duplex  Speed Type 
Gi1/0/5                      connected    trunk      a-full a-1000 10/100/1000BaseTX

sw-d01-03#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
sw-d01-03(config)#interface gi1/0/5
sw-d01-03(config-if)#shutdown
sw-d01-03(config-if)#end

sw-d01-03#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
sw-d01-03(config)#interface gi1/0/5
sw-d01-03(config-if)#no shutdown      
sw-d01-03(config-if)#end              
```
  
passer en mode "modification
> ena
Password
#

show interfaces status






## commandes

```bash
configure terminal
```


```bash
write mem
show running-configuration
```

getsion des vlan : [http://www.cedricaoun.net/cisco/CCNA3%20VLAN%20suppression.pdf](http://www.cedricaoun.net/cisco/CCNA3%20VLAN%20suppression.pdf)

lldp : https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst2960/software/release/12-2_53_se/configuration/guide/2960scg/swlldp.html

---




---
## configuration


Gestion des user/mot de passe

`enable password` et `enable secret` ont toute deux la même fonction: définir le mot de passe pour accéder au mode privilégié du CLI.

`enable password` : le mot de passe défini est encodé en clair dans la config. Utilisé uniquement pas compatibilité
`enable secret` : le mot de passe est stocké sous forme de hashage MD5. plus sécurisé, à utiliser

mais bon ca se craque quend même facilement
- [https://www.ifm.net.nz/cookbooks/cisco-ios-enable-secret-password-cracker.html](https://www.ifm.net.nz/cookbooks/cisco-ios-enable-secret-password-cracker.html)

```
enable secret toto
```

```
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
```
- le chiffre « 5 » précède la version « cryptée » du mot de passe, le mot passe donné dans la commande est en fait une version cryptée et qu’il ne doit pas la repasser par le hashage MD5.
- On peut utiliser un type 5 ou un type 7.

