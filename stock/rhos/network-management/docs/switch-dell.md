


---
## Opérations

Voir la config globale :
``` bash
show startup-config
```

Vérifier la config
# show running-config int GigabitEthernet2/0/1

# valider la config
(config)# write mem

Configurer une interface

Configurer un range 
interface range ethernet 2/0/1-2/0/24
interface range GigabitEthernet 2/0/1-24


interface GigabitEthernet1/0/1 à 1/0/23
interface GigabitEthernet2/0/1 à 2/0/24
 description RHOS default
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 172
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk


interface GigabitEthernet1/0/24
 description vers VSS
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
!


interface Vlan1
 ip address 192.168.1.26 255.255.255.0
!
ip default-gateway 192.168.1.1
ip http server
ip http secure-server


ip domain-name enst-bretagne.fr


---
## configuration des vlans
``` bash
show vlan
# 1    default                          active
# 50   vl_StackAdmin                    active    
# 51   vl_Stack_non_route_1             active    
# 52   vl_Stack_non_route_2             active    
# 53   vl_StackPublic                   active    
# 54   vl_Stack_vert                    active    
# 55   vl_Stack_noir                    active    
# 56   vl_Stack_non_route_3             active    
# 57   vl_Stack_non_route_4             active    
# 115  vl_admin_1                       active    
# 118  vl_admin_2                       active    
# 148  imta_DISI                        active    
# 149  imta_ADMIN                       active    
# 172  Openstack_2                      active    
# 173  OpenStack173                     active    
# 174  OpenStack174                     active    
# 175  OpenStack175                     active    
# 176  OpenStack176                     active    
# 177  OpenStack177                     active    
# 178  Openstack2_Flot_Priv             active    
# 179  OpenStack179                     active    
# 668  vl_websalsa                      active    
# 1002 fddi-default                     act/unsup 
# 1003 trcrf-default                    act/unsup 
# 1004 fddinet-default                  act/unsup 
# 1005 trbrf-default                    act/unsup 
```

---
## configuration des prises 

###  --- Idrac ---
interface GigabitEthernet1/0/1
 description idrac Vlan
 switchport access vlan 420
 switchport mode access

### --- Openstack RHOS 172 ---
description  Openstack RHOS vlan 172-179
switchport mode trunk
switchport trunk native vlan 172
switchport trunk allowed vlan 172-179
spanning-tree portfast trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00

###  --- Openstack dfvs ---
description Openstack DFVS vlan 50-56
switchport trunk native vlan 50
switchport trunk allowed vlan 50-56
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk


###  --- Openstack Mingration ---
description Openstack DFVS-RHOS vlan 50-56  et 172-179, 50 native
switchport trunk native vlan 50
switchport trunk allowed vlan 50-56,172-179
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
