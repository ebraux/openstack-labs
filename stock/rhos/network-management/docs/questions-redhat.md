
# Questions

1/ Comment seront amenés les réseaux des IP Flottantes : ils ne figurent pas sur les schéma, ni dans les explications ?
Avec le DVR, il doivent arriver sur tous les noeuds control et compute, c'est bien ça ?

2/ Quelles sont les contraintes en terme de réseau (débit/latence) si nous répartissons les machines dans 2 salles (control-plane, compute node, storage node). De mon côté, j'avais noté lors de notre échange la contrainte d'une latence inférieure à 5ms A/R pour CEPH.

3/ J'ai également une question, pour le réseau "external" Accès aux Api Openstack.
Comme le endpoint d'accès aux API est unique, est-ce qu'une seule IP suffit, ou est-ce qu'il faut un réseau spécifique ?

4/ Réseau IPMI :
Est-ce que le réseau IMPI peut être partagé avec d'autre équippements ? (réseau, autres serveurs non RHOS ) ?


5/ Réseau de provisioning / Accès external / Réseau de "service"

Dans la documentation de mise en oeuvre de Director, on retrouve : 
"The undercloud requires a minimum of 2 x 1 Gbps Network Interface Cards: one for the Provisioning or Control Plane network and one for the External network."
De ce que je comprend, ce reseau "externe" doit permettre d'accéder au réseau IPMI, mais également aux ressourecs externes (dépôts Redhat, pour le paquets, les images, ... )
Dans le schéma que ce que tu proposes, il n'y a pas d'accès vers des ressources externes au niveau de la machine director.
Uniquement les réseaux Provionning, IPMI.
Est-ce que les machine de control ou de compute on besoin elles aussi d'un accès, ne serait-ce que pour s'enregister ?

Est-ce qu'il faut prévoir un accès de ce type depuis le réseau de provisionning.
Ou est-ce qu'il faut prévoir un réseau dédié pour ce type d'accès, différen de celui de provisioning ?

Je me pose aussi la question pour l'IP native de la machine Director, et pour celel hébergeant d'éventuels outils tierces (proxy, caches, RH Satellite, registry Docker, Logs et monitoring, ...)
Est-ce qu'il vaut mieux réserver des IPs dans le réseau de provisioning pour cet usage, ou utiliser un réseau dédiée ?

---

Suivi des accès sortants ?

NAT Gateway ?