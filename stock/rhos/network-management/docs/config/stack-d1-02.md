show interfaces status

Port      Name               Status       Vlan       Duplex  Speed Type 
Gi1/0/1   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/2   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/3   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/4   RHOS default       connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/5   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/6   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/7   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/8   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/9   RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/10  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/11  RHOS default       connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/12  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/13  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/14  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/15  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/16  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/17  RHOS default       connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/18  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/19  RHOS default       connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/20  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/21  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/22  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/23  RHOS default       notconnect   1            auto   auto 10/100/1000BaseTX
Gi1/0/24  vers VSS           connected    trunk      a-full a-1000 10/100/1000BaseTX
Te1/0/1                      notconnect   1            full    10G Not Present
Te1/0/2                      notconnect   1            full    10G Not Present
Gi2/0/1   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/2   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/3   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/4   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/5   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/6   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/7   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/8   RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/9   RHOS NIC1          notconnect   1            auto   auto 10/100/1000BaseTX
Gi2/0/10  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/11  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/12  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/13  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/14  RHOS NIC1          notconnect   1            auto   auto 10/100/1000BaseTX
Gi2/0/15  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/16  RHOS NIC1          notconnect   1            auto   auto 10/100/1000BaseTX
Gi2/0/17  RHOS NIC1          notconnect   1            auto   auto 10/100/1000BaseTX
Gi2/0/18  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/19  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/20  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/21  RHOS NIC1          notconnect   1            auto   auto 10/100/1000BaseTX
Gi2/0/22  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/23  RHOS NIC1          connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi2/0/24  Openstack DFVS-RHO connected    trunk      a-full a-1000 10/100/1000BaseTX
Te2/0/1                      notconnect   1            full    10G Not Present
Te2/0/2                      notconnect   1            full    10G Not Present
Fa0                          disabled     routed       auto   auto 10/100BaseTX

---
stack-d01-02#show inventory 
NAME: "1", DESCR: "WS-C2960X-24TD-L"
PID: WS-C2960X-24TD-L  , VID: V03  , SN: FCW1930B0PK

NAME: "Switch 1 - FlexStackPlus Module", DESCR: "Stacking Module"
PID: C2960X-STACK      , VID: V01  , SN: FDO18060CZB

NAME: "2", DESCR: "WS-C2960X-24TD-L"
PID: WS-C2960X-24TD-L  , VID: V03  , SN: FCW1917B4G8

NAME: "Switch 2 - FlexStackPlus Module", DESCR: "Stacking Module"
PID: C2960X-STACK      , VID: V01  , SN: FDO18060DV3


---
stack-d01-02#show running-config 

interface FastEthernet0
 no ip address
 shutdown
!
interface GigabitEthernet1/0/1
 description RHOS default
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 172
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
!         

!
interface GigabitEthernet1/0/23
 description RHOS default
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 172
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
!

  ...

interface GigabitEthernet1/0/24
 description vers VSS
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
!
interface GigabitEthernet1/0/25
 switchport mode trunk
!
interface GigabitEthernet1/0/26
 switchport mode trunk
!
interface TenGigabitEthernet1/0/1
 switchport mode trunk
!
interface TenGigabitEthernet1/0/2
 switchport mode trunk
!
interface GigabitEthernet2/0/1
 description RHOS NIC1
 switchport trunk allowed vlan 173-179
 switchport trunk native vlan 173
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
!
 ...

nterface GigabitEthernet2/0/23
 description RHOS NIC1
 switchport trunk allowed vlan 173-179
 switchport trunk native vlan 173
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
!
interface GigabitEthernet2/0/24
 description Openstack DFVS-RHOS vlan 50-56  et 172-179, 50 native
 switchport trunk allowed vlan 173-179
 switchport trunk native vlan 173
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
!
interface GigabitEthernet2/0/25
 switchport mode trunk
!
interface GigabitEthernet2/0/26
 switchport mode trunk
!
interface TenGigabitEthernet2/0/1
 switchport mode trunk
!
interface TenGigabitEthernet2/0/2
 switchport mode trunk
!
