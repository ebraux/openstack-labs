---
# Switch DELL

## config :

### Interface management

```bash
!
interface mgmt1/1/1
 no shutdown
 no ip address dhcp
 ip address 10.29.20.201/24
 ipv6 address autoconfig
!
```

### Le port #2 vers le VSS

```bash
interface ethernet1/1/2
 description "Sw Dell --> VSS"
 no shutdown
 switchport mode trunk
 switchport access vlan 1
 switchport trunk allowed vlan 172,178
 flowcontrol receive on
!
```

### Les ports 25 à 30

```bash
interface ethernet1/1/1
 no shutdown
 switchport access vlan 1
 flowcontrol receive on
```


### tous les autres

```bash
interface ethernet1/1/3
 no shutdown
 switchport mode trunk
 switchport access vlan 172
 switchport trunk allowed vlan 173-179
 flowcontrol receive on
 mtu 9000
!
```
