

stack-d01-01>show interface status                   

Port      Name               Status       Vlan       Duplex  Speed Type 
Gi1/0/1   FROM SW-D1-04      connected    trunk      a-full a-1000 10/100/1000BaseTX
Gi1/0/2   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/3   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/4   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/5   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/6   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/7   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/8   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/9   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/10  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/11  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/12  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/13  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/14  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/15  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/16  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/17  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/18  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/19  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/20  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/21  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/22  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/23  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi1/0/24  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Te1/0/1   stack-d01--->VSS-D notconnect   1            full    10G Not Present
Te1/0/2                      notconnect   1            full    10G Not Present
Gi2/0/1   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/2   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/3   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/4   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/5   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/6   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/7   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/8   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/9   idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/10  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/11  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/12  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/13  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/14  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/15  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/16  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/17  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/18  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/19  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/20  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/21  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/22  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/23  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Gi2/0/24  idrac Vlan         connected    420        a-full a-1000 10/100/1000BaseTX
Te2/0/1   stack-d01--->VSS-K notconnect   1            full    10G Not Present
Te2/0/2                      notconnect   1            full    10G Not Present
Po1       stack-d01 <--> VSS notconnect   unassigned   auto   auto 
Fa0                          notconnect   routed       auto   auto 10/100BaseTX

---
stack-d01-01>show inventory   
NAME: "2", DESCR: "WS-C2960X-24TD-L"
PID: WS-C2960X-24TD-L  , VID: V02  , SN: FOC1804S391

NAME: "Switch 2 - FlexStackPlus Module", DESCR: "Stacking Module"
PID: C2960X-STACK      , VID: V01  , SN: FOC181750DU

NAME: "1", DESCR: "WS-C2960X-24TD-L"
PID: WS-C2960X-24TD-L  , VID: V02  , SN: FOC1745S5DQ

NAME: "Switch 1 - FlexStackPlus Module", DESCR: "Stacking Module"
PID: C2960X-STACK      , VID: V01  , SN: FOC18146BBH

---
config


interface GigabitEthernet1/0/1
 description FROM SW-D1-04
 switchport trunk allowed vlan 1,420
!
interface GigabitEthernet1/0/2
 description idrac Vlan
 switchport access vlan 420
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge
!
--> idem jusque interface GigabitEthernet1/0/24

!
interface GigabitEthernet1/0/25
!
interface GigabitEthernet1/0/26
!
interface TenGigabitEthernet1/0/1
 description stack-d01--->VSS-D01
 switchport mode trunk
 storm-control broadcast level 1.00
 storm-control multicast level 1.00
 channel-group 1 mode active
!
interface TenGigabitEthernet1/0/2
!



interface GigabitEthernet2/0/1
 description idrac Vlan
 switchport access vlan 420
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge
!         
  ...

  !
interface GigabitEthernet2/0/24
 description idrac Vlan
 switchport access vlan 420
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge
!
interface GigabitEthernet2/0/25
!
interface GigabitEthernet2/0/26
!
interface TenGigabitEthernet2/0/1
 description stack-d01--->VSS-K02
 switchport mode trunk
 storm-control broadcast level 1.00
 storm-control multicast level 1.00
 channel-group 1 mode active
!
interface TenGigabitEthernet2/0/2
