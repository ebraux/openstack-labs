

# --- idrac Openstack
interface GigabitEthernet1/0/8
description IPMI Vlan
switchport access vlan 420
switchport mode access

#  --- Idrac ---
interface GigabitEthernet1/0/1
 description idrac Vlan
 switchport access vlan 420
 switchport mode access





interface GigabitEthernet1/0/1
 description Openstack V1 vlan 50-56
 switchport access vlan 172
 switchport trunk allowed vlan 50-56
 switchport trunk native vlan 50
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk


#  --- DISI ---
interface GigabitEthernet1/0/1
 description Vlan DISI
 switchport access vlan 148
 switchport mode access


#  --- Openstack ---
description Openstack V1 vlan 50-56
switchport trunk native vlan 50
switchport trunk allowed vlan 50-56
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk

(config-if)# end



# --- idrac Openstack
interface GigabitEthernet1/0/8
description idrac Vlan
switchport access vlan 420
switchport mode access




#  --- Websalsa ---<>


#  --- MEE ---
# VLAN 151  imta_Pe
description Poste+de+test
switchport trunk native vlan 151
switchport trunk allowed vlan 151,234,671
switchport mode trunk
spanning-tree portfast trunk


 description SL-MEE-BR-109.
 switchport access vlan 151
 switchport trunk native vlan 151
 switchport mode trunk
 no switchport nonegotiate
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast trunk


# --- disi-calcul ---
description disi-calcul-07
switchport access vlan 200
switchport mode access
spanning-tree portfast




# --- passerelle externe 2 ---
Current configuration : 231 bytes
!
interface GigabitEthernet1/0/5
 description passerelle+externe+secours
 switchport access vlan 670
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge
end

 switchport access vlan 668
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast




```bash
interface range ethernet 1/1/1-1/1/2
description "Sw Dell --> VSS"
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 172-179


interface range ethernet 1/1/3-1/1/24
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 173-179
mtu 9000


interface range ethernet 1/1/31-1/1/54
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 173-179
mtu 9000

interface range ethernet 1/1/25-1/1/30
switchport mode trunk
switchport access vlan 1
switchport trunk allowed vlan 1, 172-179
mtu 9000
```
