
## ***Besoins en Vlans  / subnets***



   * toutes les cartes Idrac et certaines interfaces de management des switchs sont connectés à un Vlan dédié "Out of Band", le Vlan 420. Il doit être accessible depuis le Vlan DISI
   * la nouvelle infra Openstack à déployer utilise les Vlan 172 à 179, et les subnets correspondants
   * le vlan 172 est utilisé pour la machine d'admin, et pour déployer les machines physiques, et y accéder. Il doit être accessible depuis le Vlan DISI. Il doit pouvoir accéder au Vlan OOB
   * le Vlan 178 est utilisé pour les IP qui seront attribuées aux VM déployées dans Openstack. Il doit être accessible sur le réseau interne d'IMT atlantique
   * le vlan 175 est utilisé pour accéder à Openstack (interface web et API), ainsi que pour des outil annexes (gestion de logs, ...). Il doit être accessible depuis le Vlan DISI.
   * les Vlans 173, 174, et 175 doivent pouvoir être utilisé en multisite, si un jour une infra multi-site devait être mise en place
   * les Vlans 176, 177, et 179 sont purement internes au cluster Openstack, et n'ont pas besoin d'être routés en dehors du cluster, local à Brest.
   * L'IP utilisée pour  accéder à Openstack doit être accessible au moins sur le réseau interne d'IMT atlantique, et si possible avec une IP publique.
     * une IP publique a été affectée à ce service
     * les nom DNS *.openstack.imt-atlantique.fr ont éét défini vers cette IP
     * un mécanisme de Reverse proxy basé sur HAProxy a été mis en place entre l'IP publique, et l'IP Opensatck interfec WEB et API du VLAn 175
   * l'IP publique utilisée pour Opensatck doit être accessible depuis le VLAN 172.  

Les subnets utilisés dans les Vlans sont : 

   * 420 : 10.29.20.0/24
   * 172 : 10.129.172.0/24
   * 173 : 10.129.173.0/24
   * 174 : 10.129.174.0/24
   * 175 : 10.129.175.0/24
   * 176 : 192.168.176.0/24
   * 177 : 192.168.177.0/24
   * 178 : 10.129.176.0/22  et 192.108.116.208/29
   * 179 : 192.168.179.0/24