
DELL : emplacement 33
D1-31 et D132

trou : empl 36 et 40 et 32

SFP

Baie 21 
  SFP : 30, 31, 34, 35
  Vide : 23, 23, 29, 32, 36 40
  
Baie 22 :
   34, 35, 31, 30 
   Vide : 22, 23, 29, 32, 36, 40
Baie 23 : 
  SFP : 30, 31, 33, 34 et 35   
  vide 23, 29, 32, 40 

Baie 24 :
  SFP : 30, 31, 33, 35
  vide : 29, 32, 34, 40
  

WS-C2960X-24TD-L
-> switchs 24 ports  + 2 modules SFP/SFP+  10G

Switch 1Go WS-C2960X-24TD-L , avec uplink 10G (2 modules SFP/SFP+  10G)
- 2 switchs 24 ports 
- 2 stacks de 2 switchs (soit 48ports) 

Switch 1G WS-C2960S-24TD-L, avec uplink 10G (2 modules SFP/SFP+  10G)
- 1 switchs

Switch 1Go WS-C2960G-24TC-L
- 2 switchs

Partagé avec la DISI, au K2
 - Switch 1Go WS-C2970G-24TS-E  avec uplink 1G (4x modules SFP  1G)

---
## stack stack-d01-01.net.enst-bretagne.fr

- modèle : 2x WS-C2960X-24TD-L
- utilisation : Stack  IMPI
- accès : `telnet stack-d01-01.net.enst-bretagne.fr`
- config : Tout en Vlan IPMI (420)

## stack stack-d01-02.net.enst-bretagne.fr

- modèle : 2x WS-C2960X-24TD-L
- utilisation : Stack Openstack
- accès : `telnet stack-d01-02.net.enst-bretagne.fr`
- config :
    -  vlan 172-179,
    -  native : aucun, sauf quelques 172 ou 173

## Switch  sw-d01-03.net.enst-bretagne.fr
- Modèle : WS-C2960X-24TD-L
- utilisation : Switch en complément pour IMPI 
- Accès : `telnet sw-d01-03.net.enst-bretagne.fr`
- config :  Tout en Vlan IPMI (420) 

## Switch  sw-d01-04.net.enst-bretagne.fr
- Modèle : WS-C2960X-24TD-L
- utilisation : Top Of Rak 
- Accès : `telnet sw-d01-04.net.enst-bretagne.fr`
- config :
    -  vlan 172-179,
    -  native : 172

## d1-30.net.enst-bretagne.fr

- Modèle : WS-C2960G-24TC-L
- accès : `telnet d1-30.net.enst-bretagne.fr`
- Utilisation
  - n'a pas l'air utilisé ...
  - MEE : VLAn 300, 151, 200,
- Uplink : 4x ports  1GE parmis les 24

## d1-8.net.enst-bretagne.fr

- Modèle : WS-C2960G-24TC-L
- accès : `telnet d1-8.net.enst-bretagne.fr`
- Utilisation
  - n'a pas l'air utilisé ...
  - MEE : VLAn 300, 151, 200,
  - DISI : 410, 670
- Uplink : 4x ports  1GE parmis les 24
  
## d1-31.net.enst-bretagne.fr

- Modèle : WS-C2960S-24TD-L
- accès : `telnet d1-31.net.enst-bretagne.fr`
- Utilisation
  - 16 ports utilisés
  - PeR : Natif 151 + Trunk 234,671
- Uplink : 
  - uplink 10G (2 modules SFP/SFP+ 10G) + Uplink 1G (2x Interfaces ?)
  - port 24 1GE utilisé



### k2-12.net.enst-bretagne.fr
- Modèle : WS-C2970G-24TS-E
  - accès : `telnet k2-12.net.enst-bretagne.fr`
- config :
  - DISI : Vlan 862, 873, 410, 852
  - MEE : Vlan 151, 667
- Uplink : 4x modulex SFP  1G


telnet d1-30.net.enst-bretagne.fr
 GigabitEthernet0/2    (pas dans l'appli)
telnet d1-8.net.enst-bretagne.fr
  GigabitEthernet0/2    (conf en 775_not1x)
telnet d1-8.net.enst-bretagne.fr
  GigabitEthernet0/2    (pas dans l'appli)
telnet d1-31.net.enst-bretagne.fr
 GigabitEthernet1/0/19 (pas dans l'appli)