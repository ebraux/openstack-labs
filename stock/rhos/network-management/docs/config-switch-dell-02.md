



introspection interface list rhos-ctrl-br-01
+------------+-------------------+-------------------+----------------+
| Interface  | MAC Address       | Switch Chassis ID | Switch Port ID |
+------------+-------------------+-------------------+----------------+
| ens1f0     | 30:3e:a7:02:ba:3a | 68:4f:64:e9:7c:55 | ethernet1/1/19 | rhos-ctrl-br-01 NIC2-1
| ens1f1     | 30:3e:a7:02:ba:3b | 68:4f:64:e9:7c:55 | ethernet1/1/20 | rhos-ctrl-br-01 NIC2-2
| ens1f0     | 30:3e:a7:02:a6:58 | 68:4f:64:e9:7c:55 | ethernet1/1/33 | rhos-ctrl-br-02 NIC2-1
| ens1f1     | 30:3e:a7:02:a6:59 | 68:4f:64:e9:7c:55 | ethernet1/1/34 | rhos-ctrl-br-02 NIC2-2
+------------+-------------------+----------------------+-------------------+----------------+
