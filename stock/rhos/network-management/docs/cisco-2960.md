# Infos sur les cisco 2960

---
## Modèles


ex : 2960S-48TC-L

Modèle de base
- 2960 : modèle de base
- 2960S : 2 ème génération,  à privilégier
    - **le uplink passe de 1G à 10G**
    - meilleurs performances
    - des fonctions de sécurité en plus

Nombre de ports : existe en 24 ou 48 ports.

Fonctionnalités :

 - les modèles T font uniquement de la data
 - les modèles 'L/P/LP/FP' on du POE.

Connectique Uplink : 

- Q/T : GE port (1G)
- C : GE/SFP port (1G)
- S : SFP module slot  (2 ou 4 xSFP - 1G)
- D : SFP/SFP+ module slot (2 xSFP+ - 10G)
- SQ : 2x SFP et  2x 10/100/1000BT

2960S-48TD-L et 1 x 2960S-48TS-L

Modèle d'IOS

Switch 48 ports, port de la data uniquement :
- privilégier le 2960S-48TD-L
- sinon, le 2960S-48CD-L ou 2960S-48TC-L peuvent convenir


 - Nous n'avons pas besoin de POE : donc pas besoin des modèles en , un modèle en T convient
Donc un modèle 
Ca donnerait donnerait un modèle  2960-48TC-L.
Ca vous parait cohérent ?
Des 24 ports suffiraient, il n'y en aurait pas dans des placards qui ne servent plus par hasard ?



Ressources
- [https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-2960-s-series-switches/data_sheet_c78-726680.html](https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-2960-s-series-switches/data_sheet_c78-726680.html)
- [https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-2960-x-series-switches/datasheet_c78-728232.html1](https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-2960-x-series-switches/datasheet_c78-728232.html)
- [https://community.cisco.com/t5/switching/difference-between-catalyst-model-ws-c2960x-48lps-l-and-ws/td-p/3834489](https://community.cisco.com/t5/switching/difference-between-catalyst-model-ws-c2960x-48lps-l-and-ws/td-p/3834489)