
# Cisco Stack


---
## config pour  machine VLAN 172

``` bash
interface GigabitEthernet1/0/16
 description RHOS default
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 172
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
```

## Configuration

2 switch en stack. Il faut gérer via des commandes VTP

``` bash
show switch        
# Switch/Stack Mac Address : ac7e.8a79.a480
#                                            H/W   Current
# Switch#  Role   Mac Address     Priority Version  State 
# ----------------------------------------------------------
# *1       Master ac7e.8a79.a480     1      4       Ready               
#  2       Member f078.1637.7c00     1      4       Ready  
```

https://www.clemanet.com/stack-switch.php

Pour configurer les Vlan, le switch doit être un "primary" : 
- [https://community.cisco.com/t5/switching/vtp-v3-primary/td-p/1829997](https://community.cisco.com/t5/switching/vtp-v3-primary/td-p/1829997)

``` bash
configure terminal 
(config)#vtp version 3
(config)#vtp mode server
(config)#end

vtp primary vlan  
#This system is becoming primary server for feature vlan 
#No conflicting VTP3 devices found.
#Do you want to continue? [confirm]

no vlan 999
```


``` bash
show vtp status
```

afficher les Vlan
``` bash
show vlan brief
```


 ## Configuration

Sta ndard

```bash
description RHOS default
switchport trunk allowed vlan 172-179
switchport trunk native vlan 172
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
```bash

```bash

#MOSK CTRL/Compute NIC0
interface Gi1/0/15,
description MOSK NIC0
switchport trunk allowed vlan 172
switchport trunk native vlan 172
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk


#MOSK CTRL/Compute BOND0

#MOSK CTRL/Compute BOND1
interface 
description MOSK BOND0
switchport trunk allowed vlan 172-179
switchport trunk native vlan 1
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
```
