# Profils de prises


- IPMI.
  - pas de vlan
  - si besoin vlan natif 420
  
- RHOS provisionned
    - 


interface ethernet1/1/19
 description "RHOS Default"
 no shutdown
 switchport mode trunk
 switchport access vlan 173
 switchport trunk allowed vlan 174-179
 flowcontrol receive on
 flowcontrol transmit off
 spanning-tree port type edge
!
interface ethernet1/1/20
 description "RHOS Default"
 no shutdown
 switchport mode trunk
 switchport access vlan 173
 switchport trunk allowed vlan 174-179
 flowcontrol receive on
 flowcontrol transmit off
 spanning-tree port type edge



RHOS provisionned NIC1 - PXE

NIC pour boot PXE.
  - pas de vlan
  - si besoin vlan natif 173



interface GigabitEthernet1/0/11
 description RHOS default
 switchport trunk allowed vlan 172-179
 switchport trunk native vlan 173
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk


!
