# Mise à jour des switches DELL


doc : 
- https://www.dell.com/support/manuals/fr-fr/dell-emc-smartfabric-os10/ee-upgrade-downgrade/upgrade-os10-on-vlt-nodes-with-minimal-traffic-loss?guid=guid-b8d9e1d5-e4a8-422f-bd4c-42a49a78d712&lang=en-us
- https://www.dell.com/community/Networking-General/OS10-upgrade/td-p/7668632



Vérifier les droits : Role should be sysadmin, privilege level 15

```bash
stack10g-02# show users
 
Index  Line    User           Role         Application    Idle     Login-Time                   Location               Privilege-Level
-----  -----   ------------   ------       ------------   -----    --------------------------   ---------------------  --------------- 
1      pts/0   admin          sysadmin     bash           0.8s     2021-07-30     T 14:46:50Z   10.129.41.176 [ssh]    15
```


Sur le stack-02

```bash
stack10g-02# image download scp://root@192.44.75.9:/usr/tftpdir/DELL/PKGS_OS10-Enterprise-10.4.3.7.289stretch-installer-x86_64.bin
stack10g-02# show image status
```
Quand le "Transfer Progress:" est  à 100%, pour vérifer que lebin est bien là :
```bash 
dir image
```

Ensuite, installer l'image
```bash 
image install image://PKGS_OS10-Enterprise-10.4.3.7.289stretch-installer-x86_64.bin
show image status
---
 - In progress: Installing
 - In progress: Configure filesystem (3 of 10)
 - In progress: Configure filesystem (5 of 10)
 - ...
```
==> là ça plante : `State Detail:          Failed: Operation not permitted`


Quand le "State Detail" indique
```bash 
boot system standby
write memory
copy config://startup.xml config://20210730_MAJ
reload
```

Attendre le redémarrage
```bash 
show vlt 1
```