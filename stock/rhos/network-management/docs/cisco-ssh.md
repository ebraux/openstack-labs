
``` bash
hostname sw-d01-04
ip domain-name enst-bretagne.fr
ip name-server 192.44.75.10
```

``` bash
sw-d01-04#show ssh
%No SSHv2 server connections running.
```

---
## Créer un utilisateur pour l'accès SSH

En mode config
``` bash
(config)#username _ebraux  privilege 15 secret 5 $1$H8AE$q5qIymVoKffuQVd540gs
```
 Si on utilise `password` plutot que `secret`, le mot de passe sera stocké en clair dans la config (c'est une commande  uniquement encore dispo pour compatibilité)

Pour mettre un mot de passe en clair, utiliser `secret 0 monpasswordsecret`

> Je n'ai pas trouvé comment passser de `password` à `secret` pour un utilisateur existant, à par le supprimer et le recréer. POur le supprimer, utiliser `no`. Par exmeple `no username _ebraux privilege 15 password 0 toto`

---
## Authentification par clé

> La clé est independante de l'utilistateur. Si on supprime l'utilisateur, ça ne supprime pas la clé.

Il faut une clé RSA 2048, si on utilise un autre format, on obtient l' message `%SSH: Failed to decode the Key Value`
``` bash
ssh-keygen -t rsa -b 2048 -C "emmanuel.braux@imt-atlantique.fr" -f $HOME/.ssh/ebraux_rsa_2048
```

Il va falloir la saisir en plusieurs lignes :
``` bash
fold -b -w 72  $HOME/.ssh/ebraux_rsa_2048.pub
```

Ensuite, il faut ajouter cette clé dans la config CISCO
``` bash
sw-d01-04#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
sw-d01-04(config)#ip ssh pubkey-chain
sw-d01-04(conf-ssh-pubkey)#username _ebraux

sw-d01-04(conf-ssh-pubkey-user)#key-string

sw-d01-04(conf-ssh-pubkey-data)#AAAAB3NzaC1yc2EAAAADAQABAAABAQC/1jbi+zO0OaXIv1gx/0m6Heda7H87/8bT
sw-d01-04(conf-ssh-pubkey-data)#b1cTUg2VMuN/8CixTApRM3FwMdnWFDS6LA7gEkS0kk5FqYlKMWFLo1eRVkJN6/l7PrVHasRz
sw-d01-04(conf-ssh-pubkey-data)#wbXzyn2whbI6aey7dObwO8wV9gQbpaFYEqHU5M9mmyv1LP+atGfXFw2tGuvb62wOAF6O9dsD
sw-d01-04(conf-ssh-pubkey-data)#PY9DQc+HRIBz6aD+sM/pmj47dbnvJzZCPUsoj0ry5XbIvKWgGAY7226aOLRSgyB7TtTWVHdI
sw-d01-04(conf-ssh-pubkey-data)#mIgN8mvmXbDho/aA8O25GKO1we6CxGzu04Q6CjhQJ1aRL0G82dG2pdcHQCMPiAehH2VHxjlC
sw-d01-04(conf-ssh-pubkey-data)#6BFzRUhKgo5mlf1QHjND

sw-d01-04(conf-ssh-pubkey-data)#exit
sw-d01-04(conf-ssh-pubkey-user)# exit
sw-d01-04(conf-ssh-pubkey)#exit
sw-d01-04(config)#end
sw-d01-04#
```


Vérifier qu'elle a bien été prise en compte
``` bash
show run | beg pubkey
# ip ssh pubkey-chain
#   username _ebraux
#    key-hash ssh-rsa 1FE032C847153D9AD232C2D4014633F2
# !
```
Comparer avec le fingerprint de la clé saisie. Le format est en md5.
``` bash
ssh-keygen -l -E md5 -f $HOME/.ssh/ebraux_rsa_2048.pub
# 2048 MD5:1f:e0:32:c8:47:15:3d:9a:d2:32:c2:d4:01:46:33:f2 emmanuel.braux@imt-atlantique.fr (RSA)
```

- [https://networklessons.com/uncategorized/ssh-public-key-authentication-cisco-ios](https://networklessons.com/uncategorized/ssh-public-key-authentication-cisco-ios)
- [https://nsrc.org/workshops/2016/renu-nsrc-cns/raw-attachment/wiki/Agenda/Using-SSH-public-key-authentication-with-Cisco.htm](https://nsrc.org/workshops/2016/renu-nsrc-cns/raw-attachment/wiki/Agenda/Using-SSH-public-key-authentication-with-Cisco.htm)


---
## configuration du SSH
 à faire en mode config

``` bash
#show ip ssh
SSH Enabled - version 1.99
Authentication methods:publickey,keyboard-interactive,password
Authentication Publickey Algorithms:x509v3-ssh-rsa,ssh-rsa
Hostkey Algorithms:x509v3-ssh-rsa,ssh-rsa
Encryption Algorithms:aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc,aes192-cbc,aes256-cbc
MAC Algorithms:hmac-sha1,hmac-sha1-96
Authentication timeout: 120 secs; Authentication retries: 3
Minimum expected Diffie Hellman key size : 1024 bits
IOS Keys in SECSH format(ssh-rsa, base64 encoded): TP-self-signed-538154496
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCkNG2vOderLTD/jvDqauoNJ7GF2o3F6m5S2uf7Iko8
2KhukDWsorQ/rIZkgEErchbTchE1YGyzRA0nRzd0Ew0Fu6Gk8tRMJMdMe4zjW2cO2K5xvQlxehRPcKNA
BFTyk3JdGFXNGGATldLWMK3znm3mytA/nkyiH28uzdyW7so8jQ==                 

sw-d01-04#show version
Cisco IOS Software, C2960X Software (C2960X-UNIVERSALK9-M), Version 15.2(5)E, RELEASE SOFTWARE (fc1)

--> il y a un K9, donc ça supporte l'encryption
```

- [https://www.clemanet.com/activation-ssh.php](https://www.clemanet.com/activation-ssh.php)
- [https://www.cisco.com/c/en/us/support/docs/security-vpn/secure-shell-ssh/4145-ssh.html](https://www.cisco.com/c/en/us/support/docs/security-vpn/secure-shell-ssh/4145-ssh.html)
- [https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst2960x/software/15-0_2_EX/security/configuration_guide/b_sec_152ex_2960-x_cg/b_sec_152ex_2960-x_cg_chapter_01001.html](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst2960x/software/15-0_2_EX/security/configuration_guide/b_sec_152ex_2960-x_cg/b_sec_152ex_2960-x_cg_chapter_01001.html)

```
sw-d01-04#crypto key generate rsa 2048
```

``` bash
Feature Information for SSH
Release	Feature Information
Cisco IOS 15.0(2)EX	
This feature was introduced.

Cisco IOS 15.2(1)E

The Reverse SSH Enhancements feature, which is supported for SSH Version 1 and 2, provides an alternative way to configure reverse Secure Shell (SSH) so that separate lines do not need to be configured for every terminal or auxiliary line on which SSH must be enabled. This feature also eliminates the rotary-group limitation.

This feature was supported on CAT4500-X, CAT4500E-SUP6E, CAT4500E-SUP6L-E, CAT4500E-SUP7E, CAT4500E-SUP7L-E.

The following command was introduced: ssh.
```