# Déploiement de l'infrastructure Openstack - CPER AIDA


- [Informations complémentaires sur les plans d'adressage](./ipam.md)
- [Configuration réseau avec Ansible](./ansible-config.md)

- [Configuration des switchs Cisco](./switch-cisco.md)
- [Configuration des switchs DELL](./switch-dell.md)
