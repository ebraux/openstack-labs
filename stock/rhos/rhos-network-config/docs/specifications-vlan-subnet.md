# Déploiement de l'infrastructure Openstack - CPER AIDA
## Spécifications des Vlan et Subnet à mettre en place

---
## Version du document

Versions du document :

| Version | Date | Contributeur | Commentaire |
| --- | --- | --- | --- |
| 0.1 | 11/04/2023 | Emmanuel Braux | Première version |
| 0.2 | 10/11/2023 | Emmanuel Braux | Ajout conservation Vlans hors production - recommendation RedHat "mise en place d’une plateforme de type bac à sable" |

---
## Préface

Ce document est basé sur :

- Un atelier d’architecture réalisé avec l'IMT Atlantique et Eric Marques, Redhat Infrastructure Consultant, autour de la mise en place d’une solution Red Hat Openstack Platform.
- Des échanges entre les administrateurs de la plateforme et l'équipe en charge de la configuration des réseaux d'IMT Atlantique

Audience : ce document est a diffusion restreinte.


---
## Synthèse

### Liste des réseaux Vlan et Subnets en production

| Nom | Routable | Vlan | Subnet | Remarque |
| ---- | ---- | ---- | ---- | ---- |
| IPMI | oui (director + admin) | 420 | 10.29.20.0/24 | Réseau local pour Brest, 10.29.x.x OK |
| Infrastructure | oui (DHCP/PXE/Proxy) | 172 | 10.129.172.0/24 | Réseau local pour Brest, 10.129.x.x **utile ?** |
| Provisioning | non (interne Openstack) | 173  | 10.129.173.0/24 | Réseau local pour Brest, 10.129.x.x **utile ?** |
| Internal API | non (interne Openstack) | 174  | 192.168.174.0/24 | Interne Openstack 192.168.x.x OK |
| Tenant | non (interne Openstack) | 175  | 192.168.175.0/24 | Interne Openstack 192.168.x.x OK |
| Storage Network | oui si possible (serveur externe avec accès stockage CEPH) | 176  | 192.168.176.0/24 | Pour accès par serveur de calcul hors Openstack, **est-ce que le 192.168.x.x est adapté ?**  |
| Storage management | non entre noeuds CEPH uniquement| 177  | 192.168.177.0/24 | Interne CEPH 192.168.x.x OK |
| Externe réseau entreprise | oui (IP flottantes internes IMT) | 178 | 10.129.176.0/22 | Réseau IMTA + IMT, 10.129.x.x nécessaire |
| Externe réseau public | oui (IP flottantes publiques) | 179 | 192.108.116.208/29 | réseau Public, IP publiques nécessaires |

### Liste des réseaux Vlan et Subnets hors production

| Nom | Routable | Vlan | Subnet | Remarque |
| ---- | ---- | ---- | ---- | ---- |
| - | - | 50 | - | Vlan pour Infrastructure de Validation |
| - | - | 51 | - | Vlan pour Infrastructure de Validation |
| - | - | 52 | - | Vlan pour Infrastructure de Validation |
| - | - | 53 | - | Vlan pour Infrastructure de Validation |
| - | - | 54 | - | Vlan pour Infrastructure de Validation |
| - | - | 55 | - | Vlan pour Infrastructure de Validation |
| - | - | 56 | - | Vlan pour Infrastructure de Validation |
| - | - | - | 192.168.173.0/24 | reste du 1er POC RHOS, plus utilisé |
| - | - | - | 192.168.179.0/24 | reste du déploiement Mirantis, plus utilisé |


### Validation des Interactions avec réseaux IMT-Atlantique

| VLAN | sens | accès | status |
| --- | --- | --- | --- |
| 172 | IN  | depuis admin | ***Destination Net Unreachable*** |
| 172 | OUT | PXE/DHCP/DNS IMT ATlantique | **Pas de DNS.** Reste Non Testé  |
| 172 | OUT | NTP IMT ATlantique | Non Testé  |
| 172 | OUT | proxy IMT ATlantique | **Non accessible**  |
| 172 | OUT | gitlab IMT ATlantique | Non Testé  |
| 172 | OUT | gitlab-disi IMT ATlantique | Non Testé  |
| 172 | OUT | accès direct | Non Testé |
| 173 | IN  | depuis admin | Non Testé |
| 173 | OUT | NTP IMT ATlantique | Non Testé |
| 173 | OUT | proxy IMT ATlantique | Non Testé |
| 173 | OUT | accès direct (optionnel) | Non Testé |


### Interactions entre Réseaux

| VLAN | sens | accès | status |
| --- | --- | --- | --- |
| 173 | IN  | depuis  Director | Non Testé  |
| 173 | OUT | vers les serveurs de gestion dans le VLAN 172 (Director, Satellite, ...) | Non Testé |


### Interconnexions entre switchs (test ping)

| VLAN | Interne stack Cisco | Interne Switchs DELL | Inter Switches |
| --- | --- | --- | --- |
| 172 | OK | OK DELL #1|  OK Cisco et DELL #1 |
| 173 | Non Testé | Non Testé | Non Testé |
| 174 | Non Testé | Non Testé | Non Testé |
| 175 | Non Testé | Non Testé | Non Testé |
| 176 | OK | OK DELL #1|  OK Cisco et DELL #1 |
| 177 | OK | OK DELL #1|  OK Cisco et DELL #1 |
| 178 | Non Testé | Non Testé | Non Testé |
| 179 | Non Testé | Non Testé | Non Testé |


---
## Réseau "IPMI"

*Utilisation :* 

- Pour la préparation du déploiement, Director interagit avec les serveurs de l'infrastructure via leur interface IPMI (RedFish, iDRAC DELL, Cartes ILO HPE, ....), pour interrogation sur les propriétés de serveur, arrêt/redémarrage, ...
- Director effectue de requêtes point à point, pas de broadcast, pas de grosse bande passante.
- Ce réseau peut être partagé :
    - avec d'autres serveurs
    - avec d'autres équipements, par exemple l'interface d'administration des switchs réseau via leurs interfaces de console/management

*Connexion :* 

- Tous les noeuds Openstack,
- Tous les noeuds CEPH
- Tous les switchs réseau
- Certains serveurs de Calcul MEE en attente d'intégration dans Openstack

*Interconnexions :*

- Entrant:
    - accessible depuis le "Director", pour pouvoir déployer les serveurs. Possibilité de dédier une interface sur le "Director".
    - par les administrateurs : accès HTTP(S) et SSH aux équippements
- Sortant :
    - vers Internet, pour récupération des mises à jour
        - configuration proxy possible sur les cartes d'administration serveurs
        - méthode de mise à jour des équipements réseau ? configuration proxy possible ?

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 420 | 10.29.20.0/24 |

---
## Réseau "Infrastructure"

*Utilisation :* 

- Interface principale des machines dont le déploiement n'est pas pris en charge par RHOS / Director
    - écosystème d'outils et services non spécifiques Openstack : proxy, RH Satellite, ...

*Connexion :*

- serveur Director
- serveur Satellite (smart Management)
- serveur de monitoring
- serveurs CEPH 
- serveurs de stockages secondaires
- certain serveurs de calcul 
- ...


*Interconnexions :*

- Entrant:
    - par les administrateurs
      - connexion distante aux serveurs accès SSH
      - connexion à des outils d'administration graphiques
          - principalement en HTTP(S), mais ports pas forcément standard : ex 3000 (grafana), ...
          - mais également graphiques (RedHAt Cokpit, ...)
- Sortant :
  - Accès aux serveurs utiles pour le déploiement : PXE/DHCP/DNS d'IMT-Atlantique
  - Accès aux serveur NTP d'IMT-Atlantique
  - Accès au serveur de proxy d'IMT-Atlantique
  - Accès aux gitlab d'IMT-Atlantique (gitlab et gitlab-disi) 
  - si possible accès Internet direct sans proxy à certains sites
      - sites de ressources RedHat  (Voir liste)
      - éventuellement certains sites de ressources utiles dans les phases de déploiement initial.

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 172 | 10.129.172.0/24 | 

---
## Réseau "Provisionning"

*Utilisation :* 

- Réseau utilisé par "Director" pour déployer les noeuds de l'infrastructure Openstack.
- Director gère intégralement les déploiements sur ce réseau, avec ses propres serveurs PXE/DHCP/DNS.
- Il doit donc être isolé de l'infrastructure interne IMT Atlantique.

*Connexion :*

- Serveur Director
- Tous les serveurs de l'infra Openstack (control et compute)

*Interconnexion :*

- Entrants:
  - depuis le "Director", en SSH pour pouvoir déployer les serveurs. Possibilité de dédier une interface sur le "Director".
  - par les administrateurs si possible
      - connexion distante aux serveurs accès SSH
      - connexion à des outils d'administration graphiques
          - principalement en HTTP(S), mais ports pas forcément standard : ex 3000 (grafana), ...
          - mais également graphiques (RedHAt Cokpit, ...)
- Sortants :
    - vers le Director pour récupérer les ressources. Possibilité de dédier une interface sur le "Director".
    - Éventuellement à prévoir, 
        - Accès aux serveur NTP d'IMT-Atlantique (possible de gérer un serveur NTP spécifique à l'infrastructure, relié au serveur NTP de l'école, sur serveur avec un double accès XXXX et provisionning)
        - Accès au serveur de proxy d'IMT-Atlantique (possible de gérer un serveur proxy spécifique à l'infrastructure, relié au serveur proxy de l'école, sur serveur avec un double accès XXXX et provisionning)
      
*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 173 | 10.129.173.0/24 | 


---
## Réseau "Internal API"

*Utilisation :* communication entre les composants du cluster Openstack, et accès aux API via les endpoints internes.

*Connexion :* Tous les serveurs de l'infra Openstack (control et compute)

*Interconnexions :* uniquement entre les noeuds du cluster. Pas de routage externe nécessaire

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 174 | 192.168.174.0/24  |

---
## Réseau "VxLAn"

*Utilisation :* 

- Réseaux de construction des tunnels VxLAN pour la virtualisation de réseau, pour les réseaux internes de projets.
- Communication entre les VMs déployées


*Connexion :* Tous les serveurs de l'infra Openstack (control et compute)

*Interconnexions :* uniquement entre les noeuds du cluster Openstack. Pas de routage externe nécessaire

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 175 | 192.168.175.0/24  | 


---
## Réseau "Storage"

*Utilisation :* Point d'accès au stockage.

*Connexion :* 

- Dans un premier temps utilisé pour l'accès à CEPH depuis les serveurs Openstack  (control et compute)
- Mais sera très certainement ouvert à des serveurs de calcul hors Openstack, notamment les serveurs MEE dans le réseau PeR
- Accueillera très certainement également d'autre systèmes de stockage que CEPH : serveur NFS, ...

*Recommendations :* 

- Lien 10G indispensable si utilisation du stockage pour le volume root des VMs.

*Interconnexions :*

- Entrant:
    - Depuis les serveurs Openstack control plane et compute, sur le réseau "internal".
        - Protocoles: RDB, NFS, SSH (SSHFS), 
    - Depuis des serveurs de calcul hors Openstack nécessitant un accès au stockage. 
        - Protocoles : RDB, NFS, SSH (SSHFS), 
        - possibilité de serveurs passerelle ?
- Sortant : pas de besoins sortants autres que pour les interconnexions avec serveurs de calcul hors Openstack nécessitant un accès au stockage. 

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 176 | 192.168.176.0/24  | 


---
## Réseau "Storage Management"

*Utilisation:* réplication et opérations entre les noeuds du cluster CEPH

*Connexion :* Tous les serveurs du cluster CEPH

*Recommendations :* 

- Lien 10G indispensable
- Si possible interface dédiée, et configuration "Jumbo Frame"

*Interconnexion :* uniquement entre les noeuds du cluster CEPH. Pas de routage externe nécessaire

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 177 | 192.168.177.0/24  | 


---
## Réseau "External Provider Network Entreprise"

*Utilisation:* 

- Réseau d' "IP flottantes", permet aux utilisateurs d'interagir avec les instances déployées.
    - Accès limité depuis certaines réseaux, pas d'accès direct depuis Internet
    - Pas de restriction sur les ports ouverts, c'est aux utilisateurs de gérer les accès.
    - Utilisé pour les accès sortants des VM déployées
- Accès API Publique : une plage d'IP de ce réseau (4 minimum) doit être dédié à l'accès aux API publiques :
    - limitation des accès sur ces IPs
        - accès au tableau de bord horizon (HTTP/HTTPS)
        - accès aux endpoint d'API des services (liste à confirmer)
        - pas d'accès SSH

*Connexion :* Tous les serveurs du cluster Openstack, car utilisation du mécanisme de Distributed Virtual Router (DVR)

*Interconnexion :*

- Entrant:
    - Ouverture complète depuis
        - Tous les réseaux "utilisateurs" de tous les sites d'IMT-Atlantique (campux, salsa, eduroam, VPN, ...)
        - Les réseaux utilisateurs de certaines écoles IMT (IMT Paris notamment , pour la formation continue)
- Sortant :
    - Accès aux serveurs DNS d'IMT-Atlantique
    - Accès aux serveurs NTP d'IMT-Atlantique
    - Accès au serveur de proxy d'IMT-Atlantique
    - Accès au serveur apt-cacher d'IMT-Atlantique
    - Accès aux gitlab d'IMT-Atlantique (gitlab et gitlab-disi) 
    - Accès aux réseaux de "services aux utilisateurs" de tous les sites d'IMT-Atlantique
    - si possible accès Internet direct sans proxy à certains sites 
        - sites de ressources de développement pour les utilisateurs (voir liste actuelle)
        - sites nécessitant d'autres protocoles que HTTP/HTTPS

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 178 | 10.129.176.0/22  | 

  
---
## Réseau "External Provider Network Public-Internet" 

*Utilisation:*

- Réseau d' "IP flottantes", permet aux utilisateurs d'interagir avec les instances déployées.
- Accès direct depuis Internet
- Pas de restriction sur les ports ouverts, c'est aux utilisateurs de gérer les accès.
- Utilisé pour les accès sortants des VM déployées
- Pourrais remplacer à terme "websalsa"

*Connexion :* Tous les serveurs du cluster Openstack, car utilisation du mécanisme de Distributed Virtual Router (DVR)

*Interconnexion :*

- Entrant :
    - Ouverture complète depuis Internet
- Sortant :
    - Accès aux gitlab d'IMT-Atlantique (gitlab et gitlab-disi) 
    - Éventuellement accès (mais pas indispensable si raisons de sécurité): 
        - aux serveurs DNS d'IMT-Atlantique
        - aux serveurs NTP d'IMT-Atlantique
        - au serveur de proxy d'IMT-Atlantique
        - au serveur apt-cacher d'IMT-Atlantique
        - aux réseaux de "services aux utilisateurs" de tous les sites d'IMT-Atlantique
    - **Accès Internet direct sans proxy ??**

*VLAN et Subnet proposés (utilisés dans Infra Openstack précédente) :*

| VLAN | Subnet |
| --- | --- |
| 179 | 192.108.116.208/29 | 

---
## Accès "Administrateur"

### Objectifs

- Administrer la plateforme Openstack, et les plateformes de stockage, et serveurs de calcul.

### Qui sont les administrateurs

- Administrateurs DISI
- Administrateurs MEE : E. Braux, JN. Bazin
- Éventuellement administrateurs d'un autre département
- Pas d'accès par les équipes de RedHat

Prévoir possibilité
- prestataire externe.


### Contraintes d'accès

- Réseau IPMI :
    -  Si possible un accès sans rebond via une passerelle/point d'accès, pour éviter entre autres les problèmes de :
       - latence sur les interface d'administration des serveurs anciennes génération (avant gen 14 pour DELL soit 40% du parc).
       - de reconnaissance du mode graphique sur les console WEB des serveurs HPE
- Réseau "Infrastructure" :
    - Si possible un accès sans rebond via une passerelle/point d'accès, mais moins critique que pour le réseau IPMI.
    - La liste des ports des outils d'administration pourra évoluer au cours de la vie de la plateforme.


### Solution proposée

Mise en place d'un accès de type VPN, permettant d’accéder aux réseaux IPMI et Infrastructure.

---
## Déploiement sur plusieurs salles

### Objectif

Pour des déploiements sur plusieurs salles pour des raisons de redondance, du Control-Plane Openstack ou de CEPH, il est nécessaire d'avoir 3 salles. Ce n'est pas notre cas.

Le déploiement sur plusieurs salles aurait pour objectif de répartir les serveurs compute entre les salles D1 et k2 pour des raison de consommation électrique, de dégagement calorifique, et d'encombrement des serveurs GPU.

### Contraintes

- Réseau Infrastructure : 
    - continuité du subnet, ou routage entre les subnets
- Provisionning :
    - continuité du subnet, ou routage entre les subnets
    - si plusieurs subnets, nécessité de mettre en place un mécanisme de "DHCP-relay", pour pouvoir diffuser le service PXE/DHCP/DNS de Director
- Storage
    - pas de multi subnet, ou multi-site
- Storage Management :
    - pas de multi subnet, ou multi-site
    - si subnet sur plusieurs salles, latence max 10 millisecondes aller/retour

### Solution proposée

Le prolongement des "Réseaux Openstack" et "Stockage" depuis le D1 vers le k2, avec si possible une fibre dédiée, et des équipements réseau dédié (un switch 10G Rj45) afin d'éviter les interactions avec le réseau IMT Atlantique.

Il pourrait également être pertinent d'optimiser le traffic entre des serveurs de calcul non Openstack, et le réseau de stockage.



---
## Annexes 

### Documents RedHat

- Plan de principe [rh-network-connexions](img/rh-network-connexions.png)
- Document d'architecture : [IMT-Openstack-Ceph](files/IMT-Openstack-Ceph.pdf)


### Dépôts de package redhat

| Host Name | Port | Protocol |
| --- | --- | ---|
| subscription.rhsm.redhat.com  | 443 | HTTPS |
| cdn.redhat.com | 443 | HTTPS |
| *.akamaiedge.net | 443 | HTTPS |
| registry.redhat.io | 443 | HTTPS |

### Commandes utilisées pour les tests

Tests : 

- IN, depuis admin
    - depuis un poste, avec activation vlan DISI 148
    -  test de ssh vers 10.129.172.82
- OUT PXE/DHCP/DNS IMT ATlantique
``` bash
nslookup  proxy.enst-bretagne.fr  192.44.75.10
```
- OUT NTP IMT ATlantique
``` bash
ntpdate -q ntp3.svc.enst-bretagne.fr
```
- OUT : proxy IMT ATlantique
``` bash
curl http://proxy.enst-bretagne.fr:8080
curl 192.108.115.2:8080
```
- OUT :  gitlab IMT ATlantique
``` bash
curl https://gitlab.imt-atlantique.fr
ssh  -T git@gitlab.imt-atlantique.fr
```
- OUT : gitlab-disi IMT ATlantique
``` bash
curl https://gitlab-disi.imt-atlantique.fr
ssh  -T git@gitlab-disi.imt-atlantique.fr
```
- OUT : accès direct
``` bash
curl https://gitlab-disi.imt-atlantique.fr  |
```

### Configuration des serveurs

- suffixe de domaine  : "imta.fr"
- serveurs DNS : 192.44.75.10, 192.108.115.2
- serveur NTP : 192.44.75.10


