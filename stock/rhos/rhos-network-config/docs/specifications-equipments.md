# Déploiement de l'infrastructure de calcul
## Spécifications des équipements à mettre en place

---
## Version du document

Versions du document :

| Version | Date | Contributeur | Commentaire |
| --- | --- | --- | --- |
| 0.1 | 19/04/2024 | Emmanuel Braux | Première version |


---
## Préface

Ce document est basé sur :

- Un atelier d’architecture réalisé avec l'IMT Atlantique et Eric Marques, Redhat Infrastructure Consultant, autour de la mise en place d’une solution Red Hat Openstack Platform.
- Des échanges entre les administrateurs de la plateforme et l'équipe en charge de la configuration des réseaux d'IMT Atlantique
- Les éléments avait été définis lors des échanges en avril 2023, mais n'avaient pas été intégrés dans les documents de spécification.
 
Audience : ce document est a diffusion restreinte.


---
## Configuration des équipements

Le standard DISI est :

- Les équipements sont connectés au VLAN 1, et disposent d'une adresse IP dans le subnet 192.168.1.0/24
- L'accès aux équipement passe par le lien Uplink, via le TRUNK.

La solution retenue est différente :

- Connexion au VLAN 420 (IPMI), et adresse dans le subnet 10.29.20.0/24
- L'accès aux équipement passe par l'interface dédié de monitoring

Les justification de ce choix sont :

- En cas de saturation de l'interface TRUNK, l'équipement n'est plus accessible
- Le port de monitoring est prévu pour l'accès en administration, et son utilisation est recommander comme une bonne pratique par RedHat et Cisco
- L'accès au subnet 192.168.1.x est limité à l'administration par la DISI
    - La configuration des interfaces des équipement sera gérée par des outils d'automatisation (Ansible pressenti).
    - Les serveurs d'automatisation de déploiement actuels n'ont pas accès à ce subnet, alors qu'ils ont bien accès au subnet IPMI.
    - L'accès par le subnet IPMI permet d'uniformiser les accès d'administration de bas niveau sur la plateforme

Sauf limitation technique, les 2 types d'accès pourront être mis en place.

