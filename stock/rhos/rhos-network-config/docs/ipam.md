# Plan d'adressage

---
## Mise en garde

Le principe de découpage défini est respecté au mieux, mais ce n'est pas toujours le cas (historique, exceptions, ...)

Le plan d'adressage est géré de façon manuelle, il n'est donc pas forcément tout à fait à jour

--
## Principe de découpage des Vlans

Découpage du Vlan 10.129.172.0/24

Découpage en 4 parties, en prenant en compte un éventuel découpage en sous réseaux
 - systéme et outil  (crtl plane OS, proxy, gateway,...)
   - 10.129.172.0/26 (masque 255.255.255.192): 10.129.172.0 -10.129.172.63
 - compute OS
   - 10.129.172.64/26 (masque 255.255.255.192): 10.129.172.64 -10.129.172.127
 - CEPH
   - 10.129.172.128/26 (masque 255.255.255.192): 10.129.172.128 -10.129.172.191
 - Workers K8S  et autres machine en Baremetal
   - 10.129.172.192/26 (masque 255.255.255.192): 10.129.172.128 -10.129.172.254

Découpage pour les cartes iDrac  : 10.
 - en correspondance avec les IP du vlan Vlan 10.129.172.0/24
 - prendre quelques IP dans la classe "systéme et outil" pour le réseau en début de la classe. min 6, max 9.
   - switch DELL 10G : 2, voir 3 à terme
   - switch 1Go Cisco IPMI : 1, voir 2 à terme
   - switch 1Go Cisco : 3 voir 4 à terme

---
## Mise en oeuvre

Essayer de mettre en correspondance les IP dans les différent VLAN

- 10 à 29 : serveur de gestion de l'infrastructure
    - 11 : serveur de management principal
    - 12 : serveur de management secondaire
    - 14 : VM serveur Proxy/passerelle si nécessaire
    - 15 : VM serveur Satellite
    - 16 : VM serveur Director
    - 17 : VM serveur Director - undercloud_public_host
    - 18 : VM serveur Director - undercloud_admin_host
    - 19 : VM Serveur Utility
    - 21 : cluster admin (prévision Proxmox, Swarm ou k8s -remplacement serveur de management)
    - 22 : cluster admin (idem)
    - 23 : cluster admin (idem)
- 30 à 49 : switches réseau
    - 31 : switch DE  LL IPMI #1
    - 32 : switch DELL IPMI #1
    - 33 : switch DELL SFP+ #1
    - 34 : switch DELL SFP+ #2
    - 35 : Stack CiSCO Switch #1
    - 36 : Stack CiSCO Switch #2
    - 37 : Stack CiSCO Switch #3
    - 38 : Stack CiSCO Switch #4
    - 39 : réservé switch PER
- 50 à 59 : serveurs control-plane Openstack
    - 50 : Openstack controller #1
    - 51 : Openstack controller #2
    - 52 : Openstack controller #3
- 60 à 61 : serveurs CEPH Standards
    - 61 : CEPH Standard Node #1
    - 62 : CEPH Standard Node #2
    - 63 : CEPH Standard Node #3
- 70 à 79 : Serveurs CEPH haute performance (disques NVME)
    - 71 : CEPH Haute Performance Node #1 
    - 72 : CEPH Haute Performance Node #2 
    - 73 : CEPH Haute Performance Node #3
    - 74 : CEPH Haute Performance Node #4
    - 75 : CEPH Haute Performance Node #5
- 80 à 98 : Introspection pour l'undercloud
- 99 : Director en interne à l'undercloud
- 100 à 199 : Compute Openstack
- 200 à 209 : Switches réseau DELL  (à migrer vers 30-49)
- 210 à 254 : serveurs calculs MEE
    - ...
    - 248 : sl-br-mee-xxx
    - 249 : sl-br-mee-xxx

## spécificité réseau IMPI

Configuration, dans la déclaration DHCP
``` bash
		 pool {
                        failover peer "dhcp";
                        range 10.29.20.200 10.29.20.250;
                        }
   	}
```

IPs spécifiques : 

- temporaire, IP IMPI pour machine manager-01 , au niveau système : 10.29.20.10
- 7 : IMPI pour FENCING/STONITH CTRL #1
- 8 : IMPI pour FENCING/STONITH CTRL #1
- 9 : IMPI pour FENCING/STONITH CTRL #1

