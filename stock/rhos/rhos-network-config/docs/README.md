# Déploiement Réseau pour l'infrastructure Openstack


- [Document de spécification des Vlans et Subnets](./specifications-vlan-subnet.md)
- [Document de spécification de l'architecture matérielle (en cours)](./specifications-network-architecture.md)
- [Document de spécification de configuration des équipements (en cours)](./specifications-equipments.md)
 
- [Plan d'adressage](./ipam.md)

---
## Doc de mise en oeuvre

- CISCO
    - [Commandes de base config CISCO](./cisco-commands.md)
    - [Configs CISCO de référence](./cisco-configs.md)
- DELL
    - [Informations sur la configuration LACP de switch DELL (plus utilisé)](./switch-dell-vlt-lacp)



