# Informations pour les tests

---
## Machines/IP pour tests : 

- accès idrac : 10.29.20.24,  10.29.20.25, ...
    - machine admin :  rh-utility-br-01.imta.fr [10.129.172.19/24], connecté à sw-d1-03. + une IP finissant par 19 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.19)
- machines pour tests connexion switch DELL
    - srv-1 : rhcs-ceph-br-11.imta.fr [10.129.172.61/24] + une IP finissant par 61 dans les Vlans 176 et 177  (ex pour vlan 176 : 192.168.176.61/24)
    - srv-2 : rhcs-ceph-br-21.imta.fr [10.129.172.71/24] + une IP finissant par 61 dans les Vlans 176 et 177  (ex pour vlan 176 : 192.168.176.71/24)
- machines pour tests connexion stack-d1-02 
    - srv-1 :  rh-manager-br-01.imta.fr [10.129.172.11/24] + une IP finissant par 11 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.11)
    - srv-2 :  rh-manager-br-02.imta.fr [10.129.172.12/24] + une IP finissant par 12 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.11)
       
---
## Détail procédure de test

Signification des tests effectués: 

- accessible depuis le vlan DISI  : depuis ma machine dans le Vlan DISI (VPN)
- accessible directement depuis le VLAN 172 : depuis la machine admin, avec Vlan autre que 172 désactivés.
- accessible dans le même switch Cisco  : depuis 2 machines connectées au même switch (utilisé pour Vlan 172)
- accessible stack Cisco : depuis ceph-poc-01 vers ceph-poc-02
- accessible entre les switchs/stack Cisco : depuis machine admin, vers ceph-poc-01 et/ou ceph-poc-02
- accessible entre les switchs Del : depuis OS-CTRL-01 vers OS-CTRL-02
- accessible entre les switchs Cisco et DELL : depuis admin et ceph-poc-01 et/ou ceph-poc-02, vers OS-CTRL-01 et/ou OS-CTRL-02

---
## Bilan

- **Vlan 420**: 
    - accessible depuis le Vlan DISI -> OK
    - accessible depuis campus distant : pratique, mais très très sécurisé ...
- **Vlan 172**
    - accessible depuis le vlan DISI  -> OK
    - accès au Vlan 420  -> OK
    - accessible dans le même switch Cisco -> OK
    - accessible entre les switchs Dell  -> OK
    - accessible entre les switches Cisco et DELL  -> OK
    - -> tout va bien pour ce Vlan
- **Vlan 175**
    - accessible au sein du même switch Cisco   -> OK
    - accessible entre les switch Cisco -> OK
    - accessible entre les switchs Dell *-> OK*
    - accessible entre les switchs Cisco et Dell*-> OK*
    - **accessible depuis le Vlan DISI -> NON**
    - **accessible directement depuis le VLAN 172 -> NON**
    - -> satisfaisant, car alternative mise en place : affecter une IP dans ce Vlan à la Machine d'admin
- **Vlan 178**
    - accessible au sein du même switch Cisco   -> OK
    - accessible entre les switch Cisco -> OK
    - *accessible entre les switchs Dell -> NON TESTÉ POUR L'INSTANT*
    - *accessible entre les switchs Cisco et Dell -> NON TESTÉ POUR L'INSTANT*
    - -> tout va bien pour ce Vlan
- **Vlan 173**
    - accessible au sein du même switch Cisco -> OK
    - *accessible entre les switch Cisco -> OK*
    -  *accessible entre les switchs Dell -> NON TESTÉ POUR L'INSTANT*
    - *accessible entre les switchs Cisco et Dell -> NON TESTÉ POUR L'INSTANT*
    - -> tout va bien pour ce Vlan
- **Vlan 174, 176, 177, 179**
    - accessible au sein du même switch Cisco   -> OK
    - accessible entre les switch Cisco -> OK
    -  *accessible entre les switchs Dell -> OK*
    - *accessible entre les switchs Cisco et Dell -> OK*
    - -> tout va bien pour ces Vlan

