# Configuration d'un switch Cisco

---
## Connexion

- avec telnet pour les anciens switchs, et les switch DISI
- avec SSH pour les nouveaux


---
## Principe d'administration et configuration

Par défaut on est en mode utilisateur, avec des fonctionnalité limitées

Pour passer en mode administrateur commande `enable` (ena). le prompt devient `#`
``` bash
> ena
# 
```

Ensuite, pour configurer une interface, on passe en mode `configure terminal` (conf t). le prompt devient `(config)#`
``` bash
# conf t             
Enter configuration commands, one per line.  End with CNTL/Z.
(config)#
```

Une fois les configurations effectuées, sortir du mode config en tapant `exit`
``` bash
(config)#exit
#
```

Les configurations sont appliquées en temps réel, mais par défaut sont uniquement appliqué en mémoire, et pas enregistrées. ce qui permet en cas de mauvaise configuration de redémarre le switch, et de revenir automatiquement à la dernière version sauvegardée.

Pour sauvegarder une config, utiliser la commande `write memory` ou `write mem` ou `wr` 
``` bash
#write mem
Building configuration...
[OK]

```


---
## Gestion de la configuration globale

Config détaillée du switch en cours
``` bash
# show running-config
# sh run
```

Config détaillée du switch stockée en mémoire
``` bash
# show startup-config
```


Détail de la configuration d'une interface
``` bash
# show running-config int GigabitEthernet0/20
```

---
## Configurer les interafces


Exemple, modifier GigabitEthernet1/0/5

En mode admin, afficher la configuration
``` bash
show running-config int gi1/0/5
```

Passer en mode `config`
``` bash
conf t
(config)#
```

Sélectionner l'interface
``` bash
(config)#interface gi1/0/5
(config-if)#
```

Modifier l'interface :

- Pour ajouter/modifier une configuration
``` bash
(config-if)# description Interface IMPI
```
- Pour supprimer une configuration : ajouter `no` en début de ligne
``` bash
(config-if)# no description Interface IMPI
```

Pour appliquer les modifications, sortir du mode config
``` bash
end
(config)#
```

Et vérifier
``` bash
show running-config int gi1/0/5
```

Si la config est valide, la sauvegarder
``` bash
write mem
```

Pour un traitement par "groupe" : exemple de l'interface 5 à la 10
``` bash
(config)#interface range gi1/0/5 - 10
```


---
## Inspecter les interfaces

Afficher l'état des interfaces (nom, état, ...)
``` bash
show  ip interface brief
```

Afficher des informations détaillées sur les interfaces
``` bash 
# show  interfaces
# sh int
```
Afficher des informations détaillées une interfaces spécifique
``` bash 
# show  interface GigabitEthernet1/0/1
sh int GigabitEthernet1/0/1
```

---
## Gestion des vlans

Afficher les vlan propagés sur le switch
``` bash
show vlan
```
