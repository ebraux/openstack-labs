# Déploiement de l'infrastructure Openstack - CPER AIDA
## Spécifications de l'architecture réseau à mettre en place

---
## Version du document

Versions du document :

| Version | Date | Contributeur | Commentaire |
| --- | --- | --- | --- |
| 0.1 | 06/04/2023 | Emmanuel Braux | Première version |
| 0.2 | 17/01/2024 | Emmanuel Braux | - Suppression informations LACP |
|     |            |                | - Suppression de la configuration Openstack DFVS |
|     |            |                | - Mise à jour de la configuration RHOS |
|     |            |                | - Suppression informations LACP |
| 0.3 | 25/03/2024 | Emmanuel Braux | - Mise à jour des configurations des switchs |
|     |            |                | - Suppression des paragraphes sur les actions et test à mener, désormais gérés à part |
| 0.4 | 25/03/2024 | Emmanuel Braux | - Correction de cohérence des noms des switchs après retours S.Bigaret |
| en cours | 29/03/2024 | Emmanuel Braux | - Ajout des informations de configuration DISI des nouveaux switch |
---
## Préface

Ce document est basé sur :

- Des échanges entre les administrateurs de la plateforme et l'équipe en charge de la configuration des réseaux d'IMT Atlantique

Audience : ce document est a diffusion restreinte.


---
## Configuration réseau actuellement en place

### Cisco sw-d01-04.net.enst-bretagne.fr

- Position : Baie 23, emplacement 42
- Utilisation:
    - en PRODUCTION
    - Top Of Rack pour accès vers le coeur de réseau
    - Serveurs d'admin plateforme Openstack/CEPH, connectés en 1GE
- Configuration :
    - Gi1/0/24  en uplink vers le coeur de réseau
    - Gi1/0/21 : vers sw-d01-03.net.enst-bretagne.fr
    - Gi1/0/23 : vers stack-d01-01.net.enst-bretagne.fr
    - Gi1/0/22 : non configuré
    - Gi1/0/1 à 20 : trunk avec les Vlans Openstack


### Cisco sw-d01-03.net.enst-bretagne.fr

- Position : baie 23, emplacement 41
- Utilisation:
    - en PRODUCTION. Peut être arrêté en concertation
    - réseau IPMI serveurs Openstack/CEPH, switchs, et serveurs de calcul MEE
- configuration :
    - Gi1/0/24 : uplink vers sw-d01-04.net.enst-bretagne.fr
    - Gi1/0/1 à 23 : vlan 420



### stack-d01-01.net.enst-bretagne.fr

- Position : Baie 21, emplacement 41 et 42
- Utilisation: 
    - en PRODUCTION. Peut être arrêté en concertation
    - réseau IPMI serveurs Openstack/CEPH, switchs, et serveurs de calcul MEE
- Configuration :
    - Gi1/0/1 "uplink" vers  sw-d01-04.net.enst-bretagne.fr


### stack-d01-02.net.enst-bretagne.fr

- Position : Baie 22, emplacement 41 et 42
- Utilisation: 
    - en PRODUCTION
    - connexion serveurs Openstack/CEPH
    - Les compute Openstack connecté en 1GE
- Configuration :
    - Gi1/0/24 "uplink" vers  coeur de réseau


### 10.29.20.201 (DELL#1)

- Position : Baie 23, emplacement 31
- Utilisation: 
    - en PRODUCTION
    - connexion serveurs Openstack/CEPH
    - Les serveurs Openstack du Control-Plane
    - Les serveurs du cluster CEPH
    - 6 serveurs Openstack Compute
- Configuration
    - mgmt1/1/1 : 10.29.20.201/24
    - ethernet1/1/1 : "uplink" vers  coeur de réseau
    - ethernet1/1/2 à 24 : trunk avec les Vlans Openstack
    - ethernet1/1/31 à 54 : trunk avec les Vlans Openstack


### 10.29.20.202 (DELL#2)

- Position : Baie 22, emplacement 31
- Utilisation: 
    - non utilisé
    - en attente de câblage à l'identique du Switch 10.29.20.201 (DELL#1) pour redondance (Bridge au niveau OS)
- Configuration
    - mgmt1/1/1 : 10.29.20.202/24
    - ethernet1/1/1 : "uplink" vers  coeur de réseau
    - ethernet1/1/2 à 24 : trunk avec les Vlans Openstack
    - ethernet1/1/31 à 54 : trunk avec les Vlans Openstack



---
## Configuration réseau cible


### Description de l'infrastructure réseau

- 2 switchs DELL  10G SFP+ 40p [sw-tor1] et [sw-tor1] (**T**op Of **R**ack)
- 6 switchs Cisco 2960 1GE 24p, réparti comme suit :
    - 2 switchs indépendants: [sw-nc1], [sw-nc2]  (**N**etwork **C**ore)
    - 2x2 stacks : [stack-int11] et [stack-int21]  (**INT**ernal)
- 2 switchs Cisco 2960 1GE 48p [sw-ipmi1] et [sw-ipmi2]
- 4 switchs Cisco Nexus 9300 10GE 48p [sw-int11], [sw-int12], [sw-int21], [sw-int22] (**INT**ernal)

Les switchs reliés en uplink au coeur de réseau IMT Atlantique sont :

- [sw-nc1] et [sw-nc2] : pour les accès aux réseaux externes ne concernant pas directement la configuration réseau des cluster CEPH et Openstack. Par exemple, les accès IPMI.
- [sw-tor1] et [sw-tor1] : pour l'ensemble des interactions entre les cluster CEPH et Openstack vers les réseaux externes.

Les 2 switchs [sw-tor1] et [sw-tor1] sont utilisés en mode 'top of rack". Y seront connectés en cascade :

- Les 4 switchs [sw-int11], [sw-int12], [sw-int21], [sw-int22]
- les 2 stacks [stack-int11] et [stack-int21]


Les Vlans 172 à 179 (Infrastructure de production), et 50 à 56 (infrastructure sandbox) sont amenés sur l'uplink des 2 switchs [sw-tor1] et [sw-tor1], et circulent ensuite vers les switchs et les stack qui y sont connectés en cascade.

Le Vlan 420 (IPMI), ainsi que les Vlan utilisés pour les serveurs du département MEE, sont amenés sur l'uplink des 2 switchs [sw-nc1], [sw-nc2]. Seul le vlan 420 est ensuite amené vers les 2 switchs [sw-ipmi1] et [sw-ipmi2].


### Schéma d'implémentation : 

``` bash
<coeur de réseau>
    |\_ sw-tor1, vlans 172 à 179 et 50 à 56 --> uplink vers coeur de réseau : ethernet1/1/1 
    |       |
    |       |\(ethernet1/1/25)_ sw-int11, vlans 172 à 179 et 50 à 56 - uplink Gi1/0/1
    |       |
    |       |\(ethernet1/1/29)_ sw-int12, vlans 172 à 179 et 50 à 6 - uplink Gi1/0/1
    |       |
    |       |\(ethernet1/1/2)_ stack-int12, vlans 172 à 179 et 50 à 56 - uplink Gi1/0/1
    |    
    |\_ sw-tor2, vlans 172 à 179 et 50 à 56 --> uplink vers coeur de réseau : ethernet1/1/1
    |      |
    |      |\(ethernet1/1/25)_ sw-int21, vlans 172 à 179 et 50 à 56 - uplink Gi1/0/1
    |      |
    |      |\(ethernet1/1/29)_ sw-int22, vlans 172 à 179 et 50 à 56 - uplink Gi1/0/1
    |      |
    |      |\(ethernet1/1/2)_ stack-int21, vlans 172 à 179 et 50 à 56 - uplink Gi1/0/1
    |
    |\_ sw-nc1, vlan 420 et vlans calcul MEE --> uplink vers coeur de réseau : Te1/0/1, ou à défaut Gi1/0/24 
    |      |
    |      |\(Gi1/0/23)_ sw-ipmi1, vlan420 - uplink Gi1/0/48
    |
    |\_ sw-nc2, vlan 420 et vlans calcul MEE -->  uplink vers coeur de réseau : Te1/0/1, ou à défaut Gi1/0/24  
           |
           |\(Gi1/0/23)_ sw-ipmi2, vlan420 - uplink Gi1/0/48
         
```

### Plan d'adressage et équivalence avec nom DISI 

| Nom InfCalcul | IP InfCalcul | Nom DISI                          | IP DISI       |   EQI    |
| ------------- | ------------ | --------------------------------- | ------------- | -------- |
| sw-nc1        |  10.29.20.31 | sw-d01-03.net.enst-bretagne.fr    | 192.168.1.207 |          |
| sw-nc2        |  10.29.20.41 | sw-d01-04.net.enst-bretagne.fr    | 192.168.1.242 |          |
| sw-ipmi1      |  10.29.20.32 | sw-idrac-calcul-01                | 192.168.1.179 | EQI10780 |
| sw-ipmi2      |  10.29.20.42 | sw-idrac-calcul-02                | 192.168.1.215 | EQI10781 |
| sw-tor1       |  10.29.20.33 |              -                    |  10.29.20.201 |          |
| sw-tor2       |  10.29.20.43 |              -                    |  10.29.20.202 |          |
| sw-int11      |  10.29.20.34 |              -                    |    -          |          |
| sw-int12      |  10.29.20.35 |              -                    |    -          |          |
| sw-int21      |  10.29.20.44 |              -                    |    -          |          |
| sw-int22      |  10.29.20.45 |              -                    |    -          |          |
| stack-int11   |  10.29.20.36 | stack-d01-01.net.enst-bretagne.fr | 192.168.1.189 |          |
| stack-int21   |  10.29.20.46 | stack-d01-02.net.enst-bretagne.fr |  192.168.1.26 |          |


### Implantation Physique des switchs 


| Empl |  Baie 24  |    Baie 21     |    Baie 22    |  Baie 23  |
| ---- | --------- | -------------  | ------------- | --------- |
|   42 |   sw-nc1  |  stack-int11/1 | stack-int21/1 |   sw-nc2  |
|   41 |  sw-ipmi1 |  stack-int11/2 | stack-int21/2 |  sw-ipmi2 |
|   40 |  sw-int12 |    sw-int11    |   sw-int21    |  sw-int22 |
| ...  |    ...    |      ...       |     ...       |    ...    |
|   31 |     -     |    sw-tor1     |   sw-tor2     |     -     |
| ...  |    ...    |      ...       |     ...       |    ...    |
|   21 |     -     |       -        |    D1-31      |   D1-08   |
|   20 |     -     |       -        |    D1-32      |   D1-30   |
| ...  |    ...    |      ...       |     ...       |    ...    |


### Uplink depuis le coeur de réseau

| Empl |   Baie 24  |   Baie 21   |   Baie 22   |    Baie 23    |
| ---- | ---------- | ----------  | ----------- | ------------- |
|   42 |   FC [???] |      -      |      -      |   FC [???]    |
|      | ou E [???] |             |             | ou E [Active] |
| ...  |    ...     |     ...     |     ...     |      ...      |
|   31 |     -      | FC [Active] | FC [Active] |       -       |
| ...  |    ...     |     ...     |     ...     |      ...      |
|   21 |     -      |      -      |  E [Active] |  E [Active]   |
|   20 |     -      |      -      |  E [Active] |  E [Active]   |
| ...  |    ...     |     ...     |     ...     |      ...      |


### Connectique complémentaire 

| Connexion                 | Cable                                                |
| ------------------------- | ---------------------------------------------------- |
| sw-nc1  -> sw-ipmi1       | RJ45 50cm                                            |
| sw-nc2  -> sw-ipmi2       | RJ45 50cm                                            |
| sw-tor1 -> sw-int11       | 100G QSFP28 Passive Direct Attach Copper Twinax - 1m |
| sw-tor1 -> sw-int12       | 100G QSFP28 Passive Direct Attach Copper Twinax - 5m |
| sw-tor1 -> stack-intl11/1 | 10G SFP+ Passive Direct Attach Copper Twinax - 2m - compatible DELL S4148F / CISCO SW-C2960S |
| sw-tor2 -> sw-int21       | 100G QSFP28 Passive Direct Attach Copper Twinax - 1m |
| sw-tor2 -> sw-int22       | 100G QSFP28 Passive Direct Attach Copper Twinax - 5m |
| sw-tor1 -> stack-intl21/1 | 10G SFP+ Passive Direct Attach Copper Twinax - 2m - compatible DELL S4148F / CISCO SW-C2960S |


---
### Configuration des serveurs

3 configurations différentes de machines ont été mises en place

1. Machines avec connexion SFP+/10G (connectées aux 2 switchs DELL)
    - Les machines les plus récentes
    - Principalement des serveurs DELL R640 et R740, et HPE
    - Correspond au control-plane Openstack, aux serveurs CEPH, et à 6 compute (ceux prévus pour les tests de stockage des disques des VMs sur le cluster CEPH)
    - 5 NICs réparties sur 2 cartes réseau minimum
2. Machines avec connexion RJ45/1G (connectées aux switchs CISCO)
    - tout type de machine :  R610, R710, R620, R630, R640, R740, ...
    - Les machines des "gestion" du cluster, et les compute Openstack
    - Les machines récentes sont équipées de cartes 10GE
    - 5 NICs réparties sur 2 cartes réseau minimum
3. Machine MEE non intégrée à l'infrastructure Openstack/CEPH
    - Principalement des serveurs DELL R640 et R740, et HPE   
    - Les machines récentes sont équipées de cartes 10GE
    - 1 NICs minimum
  
Sur les machines avec 5 NICs réparties sur 2 cartes réseau minimum :

- NIC1
    - Interface dédiée au VLAN 172
    - Au niveau OS : pas de Vlan configuré
    - Au niveau switch : connecté uniquement au switch #1, les prises sont configurées en mode access
    - PXE activé
- Carte 1 NIC2 et NIC3, et carte2 NIC1 et NIC2
    - Interfaces utilisées pour les VLANs 173 à 179
    - Au niveau OS :
        - interface en Bond actif/passif
            - Bond0 = NIC2 et carte2 NIC1,
            - Bond1 = NIC3 et carte2 NIC2
        - utilisation de bridges et d'interfaces virtuelle pour gestion des vlans
    - Au niveau switch:
        - Connecté à 2 switches, avec correspondance des prises. Par ex pour le bond0, si la NIC2 est sur l'interface 4 du switch#1, la carte2 NIC1 est sur l'interface 4 du switch#2
        - Prises configurée en mode Trunk
        - Pas de LACP activé
    - PXE désactivé


Sur les machines avec 1 NIC minimum :

- NIC1 :
    - interface connectée à un des VLAN utilisés par MEE

---
## Accès aux services

L'accès aux différents composants d'Openstack se fait via les API. 

- Une seule IP est utilisés pour tous les services.
- Un port spécifique est attribué à chaque service, les ports ci dessous doivent être ouverts pour permettre l'accès aux différents services :

    | Port   | Service |
    | ------ | --------- |
    | 80/443 | Dashboard (horizon) |
    |  13000 | Identity (keystone) |
    |  13004 | Orchestration (heat) |
    |  13005 | Cloudformation (cloudformation) |
    |  13080 | Console | novncproxy |
    |  13292 | Image (glance) |
    |  13696 | Network (neutron) |
    |  13774 | Compute (nova) |
    |  13776 | Volumev3 (cinder) |
    |  13778 | Placement (placement)|
    |  13808 | Object Store (swift) |
    |  13876 | Load Balancer (octavia) |

Le nom DNS pour accéder à Openstack (interface web et API) est "openstack.imt-atlantique.fr"

L'adresse IP pour accéder à Openstack (interface web et API) est "10.129.176.9"

Une adresse IP publique pour permettre un accès via un reverse-proxy entre le réseau publique, et l'infrastructure interne est réservée dans le Vlan Bastion 670 : "192.108.117.56".
