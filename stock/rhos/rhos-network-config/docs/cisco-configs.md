# Config de reference pour switch Cisco

---
## Interfaces 


IMPI, en native :
``` bash
description IPMI Vlan
switchport access vlan 420
switchport mode access
```

Openstack/CEPH Prod  par défaut : 
``` bash
description RHOS default
switchport trunk native vlan 173
switchport trunk allowed vlan 172-179
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
```

CEPH Prod  : 
``` bash
description CEPH
switchport trunk native vlan 172
switchport trunk allowed vlan 172,176,177
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
``` 


Vlans Openstack POC
``` bash
description Openstack VPOC1 vlan 50-56
switchport trunk native vlan 50
switchport trunk allowed vlan 50-56
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
```

VLAN 151  imta_Pe
``` bash
description imta_Pe
switchport access vlan 151
switchport mode access
spanning-tree portfast
```

DISI Calcul
``` bash
description disi-calcul
switchport access vlan 200
switchport mode access
spanning-tree portfast
```