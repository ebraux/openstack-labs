# Actions

---
## Actions à mener

### Configuration/cablage des switchs

- Déplacer le switch [sw-nc1]dans la baie  le switch "Cisco stack-d1-02" en cascade sur le "switch Cisco sw-d1-04"
- connecter l'interface de management de chaque switch Cisco au switch IMPI "Cisco stack-d1-01", et affecter un IP de management sur le réseau 420 à chaque équippement
- relier les switchs Cisco "switch Cisco sw-d1-03" et "stack Cisco stack-d1-02" en cascade sur les switchs DELL.
- Evaluer si il faut augmenter le nombre de connexion RJ45/1G, pour éventuellement ajouter un switch au stack, et/ou convertir le switch en stack


### Sécurisation

- Désactiver les VLAN qui n'ont pas à être routés hors infra Openstack sur le coeur de réseau (173, 174, 176, 177, et 179)
- limiter l'accès au VLAN 420 au VLAN DISI, et éventuellement aux serveurs d'admin DISI. En tout cas, supprimer l'accès depuis campus distant.
 
### Autres

- sur stack-d01-01 : Rétablir VTP. VTP client actif mais il ne connaît pas tous les Vlans. Il semble que la communication VTP soit stoppée depuis le `8-6-20 13:07:05.`.`  Réactiver le VTP, ou passer sur un mode de définition manuelle des VLANs.

### Accès aux interfaces d'Openstack  et outils de monitoring (Vlan 175) pour DISI et Prestataire

- rendre le Vlan 175 accessible depuis le Vlan DISI, pour l'accès aux différents outils d'admin et supervision
- si possible rendre le VLAN 175 accessible depuis le Vlan 172. pour permettre l'administration depuis une machine passerelle hors réseau DISI, pour le prestataire

---   
## Actions réalisées

- Rendre les vlans 174, 176, 177, 179 accessibles entre les switch DELL et les switchs CISCO
- Définir une IP publique
- Définir le modèle à mettre en place : NAT ou Proxy  -> Modèle Proxy avec HA Proxy Retenu
    - mettre en place un serveur HAproxy, avec une pâte dans réseau public (Vlan Bastion 670), et une pâte dans le Vlan 175 (ou un accès au Vlan 175)
   - mettre en place une déclaration DNS *.openstack.imt-atlantique.fr" vers l'IP externe "192.108.117.56"
- Config du "Vlan 173"
    - investiguer pour comprendre le pb de ping qui ne passe pas sur le Vlan 173
    - valider vlan 173  accessible entre les switch Cisco, et entre les switchs DELL et les switchs CISCO
    - Supprimer la configuration LACP des switchs DELL[EB], et du coeur de réseau [CLB]
- Connecter le stack "Cisco stack-d1-01" utiliser pour les accès IPMI, soit directement sur le VSS, soit sur un switch Hos de la config "Openstack", pour garantir un accès de management en cas de Pb sur le "réseau Openstack" [EB]


