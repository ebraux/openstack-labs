# Serveurs RHOS

---
## Ecosystéme de déploiement

L'écosystème du déploiment de Red Hat Openstack comprend plusieurs VM :

- VM "director"- rhos-director-01.imta.fr : contient l'overcloud, qui permet de gérer l'infrastructure
- VM de relai - rh-relay-br-01.imta.fr :  assure le relai vers les services réseau IMT atlantique (DNS, proxy, ...) car il ne sont pas accessibles depuis les subnet openstack 
- VM "utilaires" - rh-utility-br-01.imta.fr : 
    - accès à tous les subnets,
    - outils de gestion de l'overcloud : client Openstack, Ansible, Teraform, ...
- VM "Satellite" - rh-satellite-br-01.imta.fr : outil de gestion des distributions, et des souscriptions, recommandé par RedHat, mais trop complexe dans notre cas. A arrêter.


Ce sont des VMs sous KVM qui s'exécutent sur une machine physique "rh-manager-br-01.imta.fr".

Aucune sauvegarde ni redondance n'est en place.

- Elles ne peuvent pas être déployée sur l'infra VSphere car il faut un accès aux Vlan Openstack.
- Idem pour l'infra RHV.
- Une machine en backup "rh-manager-br-02.imta.fr" est prévue, mais rien n'a été mis en place pour backuper les VM.
- Une autre piste serait la mise en place d'un cluster ProxMox, qui pourrait assurer le double rôle :
    - cluster de virtualisation de VM pour ces VM
    - cluster de stockage CEPH pour Openstack, avec un support proxmox incluant CEPH, bien moin cher que celui de RedHat.
    - un POC Proxmox a été déployé pour évaluer les fonctionnalités sur 3 des 5 noeuds CEPH avec disque NVME

---
## Undercloud

La  VM "rhos-director-01.imta.fr" citée précédemment

---
## Overcloud

- 3 noeud de controlplane, machine récente HPE
- 6 noeuds de compute plutôt pas trop vieux et homogènes, avec connection 10G au stockage
- X noeuds de compute, serveurs DELL de génération 12 à 14, complètement hétérogène.


---
## Ceph

[voir doc CEPH](ceph.md)
