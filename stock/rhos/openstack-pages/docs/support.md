# Support

---
## Contexte

L'architecture est complexe.

Les serveurs utilisés pour la plupart anciens, sans contrats de support, et victimes des pannes.

Le choix a été fait de déployer une infrastructure en Haute Disponibilité du contrôle plane, afin de garantir une continuité du service sur l'aspect gestion/utilisation. Mais pas de HA au niveau des Compute.

Pour les déploiement en HA, des outils de gestion automatisée du déploiement on été utilisés, successivement Openstack Ansible, RedHat / Tripleo,  Mirantis openstack, for k8s, Kolla Ansible, ... Actuellement RedHat / Tripleo est utilisé.


L'infrastructure à déployer dépend et a possiblement un impact sur le reste de l'infrastructure :

- Configuration du réseau : Vlan internes, Subnet pour IP publiques, accès Internet : en lien avec NOC
- Configuration des serveurs : installation PXE/DHCP pour certaines des solutions
- 

---
## Pourquoi du support  ?

Les premières plateformes ont été déployées sans assistance, et sans support.

Une seule personne assurait le service.

Exemples de problèmes rencontrés :

- Relance de services en cas de perte d'un compute
- Non redémarrage des service  :
    - suite à arrêt violant non prévu (coupure électrique, ...)
    - mais également en cas d'arrêt redémarrage prévu
- Problème lié au HA : cluster galera, RabbitMQ
- Pertes de connectivité réseau : Bug OpenVswitch
- Problème de performances :
    - Mauvaises manip utilisateurs -> saturation du réseau
    - Équipement réseau défectueux
    - Mise en place de l'observabilité
    - ... 
- ...

- Contrainte en enseignement : plateforme opération sur les jours ouvrés à 8h00
- Contrainte en recherche : continuité des VM pour les traitements en recherche

Conséquences : 

- Heures, voir jours à investiguer certains problèmes.
- Parfois hors de heures ouvrées.
- Sans assistance autre que par la communauté, les forums, ...
- Ou le réseau de connaissances, qui veut bien donner un coup de main pour dépanner, mais avec certaines limites.


Nécessité donc :

- soit de monter une équipe
- soit de souscrire à un support externe
- soit de considérer le service comme expérimental :
    - Les utilisateurs considèrent tous les services comme de la production
    - Demande de créer un domaine "beta.imt-atlantique.fr" pour ce type de service refusé par DISI (C.Janin)
- soit d'arrêter le service


Choix retenus : 

- 2015-2018 : pas de support
- 2018-2019 : prestations ponctuelles (Cédric Roger)
- 2020 : un support externe RedHat (auto support)
- 2021 : un support externe Mirantis (auto support)
- 2022 : plus de support, limitation à l'enseignement
- 2023 : arrêt du service. Puis un support externe RedHat (cadre CPER AIDA)
- 2024 : support externe RedHat (cadre CPER AIDA)


La question se pose donc en termes de : service garanti aux utilisateurs, ressources internes et budget.

Et bien sûr bonne volonté et investissement personnel, car du support en "auto support" demande quand même une bonne dose de travail.
