# Serveurs Infra CEPH

Accès via compte "root", par clé ou mdp de l'infra Openstack.


Au 18 avril 2024 : 

- 3 serveurs en production
    - anciens serveurs compute, reconfigurés + ajout de disque, réaffectés temporairement
    - serveurs rhcs-ceph-br-11.imta.fr à rhcs-ceph-br-13.imta.fr
    - IP 10.129.172.61 à 10.129.172.63
    - IPMI : 10.29.20.61 à 10.29.20.63
- 5 serveurs en attente de mise en production (ou pas), en remplacement des 3 actuels
    - achetés sur le CPER d'Yvon Kermarrec, propriété département Info
    - rhcs-ceph-br-21.imta.fr à rhcs-ceph-br-25.imta.fr
    - IP 10.129.172.71 à 10.129.172.75
    - ILO : 10.29.20.71 à 10.29.20.75
    - installation temporaire d'un cluster promox sur rhcs-ceph-br-22.imta.fr à rhcs-ceph-br-24.imta.fr. Peut être supprimé
    - rhcs-ceph-br-21.imta.fr et rhcs-ceph-br-25.imta.fr non utilisés
   

