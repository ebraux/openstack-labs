# Haute disponibilité des VM

Les VM ne sont pas actuellement pas déployables en haute disponibilité. Mais ça serait possible.

La perte d'un noeud est compensée par le nombre de noeuds disponibles.Mais elle entraîne la perte des VM tournant sur ce noeud.

Pour pouvoir ne pas perdre de VMs, il faut espace de stockage partagé pour les disques des VMs (CEPH)


Pour avoir un stockage partagé sur les VM, il faut des performances suffisantes pour les accès disque : accès réseau, et écriture disque.

Les capacité par défaut jusqu'à présent sont :

- Réseau : 1Go
- Stockage : 7,2 rpm

Pour donner un idée, après une mauvaise config de l'infra Mirantis le stockage des VM avait basculé sur CEPH. les utilisateurs Rennais ont fait remonté le problème immédiatement : pus de 8mn pour installer Apache, contre 40s avec stockage local.

Depuis quelques computes ont été connecté en 10G SFP+ (les "compute-ds").

Des serveurs avec disque NVME ont été commandés pour CEPH, et des disques SSD ont été ajoutés dans les 3 serveurs utilisés.


Il serait donc possible de mettre en place des instances en HA sur ces compute

Il resterait à :

- configurer le volume VM pour utiliser des SSD au niveau de CEPH
- configurer le rôle "compute-ds" pour utiliser le stockage CEPH pour les disques racine dans RHOS
- créer des gabarits et configurer le scheduler pour déployer des instances spécifiquement sur ces computes
- tester