# Point d'entrée pour les documentations Openstack



---
## Lien vers les différentes sources de docs

### Configuration réseau pour RHOS / Prestation RedHat /  référence avec équipe NOC :

- Liens: 
    - site : [https://openstack.gitlab-disi-pages.imt-atlantique.fr/rhos-network-config/](https://openstack.gitlab-disi-pages.imt-atlantique.fr/rhos-network-config/)
    - dépôt : [https://gitlab-disi.imt-atlantique.fr/openstack/rhos-network-config](https://gitlab-disi.imt-atlantique.fr/openstack/rhos-network-config)
- Détail du contenu :
    - Architecture réseau physique mise en place
    - Description des VLANs
    - Plan d'adressage


 ---
 ## Docs suites au questions / réponses

- [Historique de la plateforme](./history/README.md)
  
- [Quels sont les serveurs CEPH](./ceph.md)
- [Quels sont les serveurs RHOS](./rhos-servers.md)
  
- [Possibilité d'avoir des VM en HA](./vm-ha.md)
- [Pourquoi du support](./support.md)
