#!/bin/bash


# --------------------------
#  environnement
#HOST_NAME='controller'
#HOST_IP=$(ifconfig | grep 'inet addr:172.16' | awk '{ print $2 }' | awk -F ':' '{ print $2 }')
#HOST_INTERFACE='enp0s8'
#OS_VERSION='ocata'
#DATABASE_PASSWORD='stack'
#DATABASE_USER='root'
#MYSQL_HOST_IP=$HOST_IP
#EXTERNAL_NETWORK_NAME=external

# --------------------------

# Keep track of the TOP directory
TOP_DIR=$(cd $(dirname "$0") && pwd)

TEMPLATE_DIR=$TOP_DIR/template
FILE_DIR=$TOP_DIR/files
TMP_DIR=$TOP_DIR/tmp

OS_WORKING_DIR=$TOP_DIR

# Sourcing the libs 
for flib in $TOP_DIR/lib/*; do
    source $flib;
done

# --- prepare host ---
system_update
config_hosts_file
install_tools


# --- installation infra ---
install_database_mysql
install_rabbitmq
install_memcached
install_apache2

# -- Menage -- 
apt clean


# --- Installation Openstack ---
config_cloud-archive

install_keystone
init_keystone


install_glance

install_nova

install_neutron
check_install_neutron
init_neutron

install_horizon

# --- initialisation de l'environnement Utilisateur ---

create_openrc_admin
create_openrc_demo
list_projects
add_cirros_image
add_cirros_flavor


