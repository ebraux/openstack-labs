#!/bin/bash


function install_neutron {
    # ----------------------------------------------------
    #   network service : neutron
    #     _ __   ___ _   _| |_ _ __ ___  _ __
    #    | '_ \ / _ \ | | | __| '__/ _ \| '_ \
    #    | | | |  __/ |_| | |_| | | (_) | | | |
    #    |_| |_|\___|\__,_|\__|_|  \___/|_| |_|
    #
    # ----------------------------------------------------
    # Installation du service Neutron
    # - configuraton keystone
    #    - utilisateur "neutron" dans le projet "Service"
    #    - creation d'un service de type "network"
    #    - endpoint "public", "internal" et "admin" identiques : http://controller:9696
    # - base de donnees Mysql
    # - utilisation de memcached
    # - ml2 plugin options : 
    #     flat, VLAN, and VXLAN networks
    #     VXLAN self-service networks
    #     Linux bridge and layer-2 population mechanisms
    #     port security extension driver
    # - configuration d'un reseau "externe"
    #   * uniquement disponible depuis la machine (non attache a une interface physique)
    #   * associe a la plage d'IP reservee 203.0.113.0/24 (TEST-NET-3)

    echo " # -------- Installation de Neutron ------------------"

    echo " # -- installation de linux-bridge --"
    apt install -y bridge-utils

    echo " --activation du bridging in the kernel --"
    modprobe bridge 8021q
    echo '8021q' >> /etc/modules

    echo " -- creation du bridge pour le reseau externe --"
    cp -p $TEMPLATE_DIR/network_50-bridge-ex.cfg  /etc/network/interfaces.d/50-bridge-ex.cfg
    systemctl restart networking

    echo " -- verification --"
    brctl show
    ip route
    ping -c 3 203.0.113.2


    echo " -- keystone access config --"
    source $TOP_DIR/keystone_credentials

    echo " -- declaration du composant dans keystone --"
    openstack user create --domain default --password stack neutron
    openstack role add --project service --user neutron admin
    openstack service create --name neutron --description "OpenStack Networking" network

    openstack endpoint create --region RegionOne network public http://controller:9696
    openstack endpoint create --region RegionOne network internal http://controller:9696
    openstack endpoint create --region RegionOne network admin http://controller:9696
    openstack endpoint list | grep network

    echo " -- verification --"
    openstack service list
    openstack endpoint list

    echo " -- Packages intallation --"
    apt install -y neutron-server neutron-plugin-ml2 \
    neutron-linuxbridge-agent neutron-l3-agent neutron-dhcp-agent \
    neutron-metadata-agent

    echo " -- Configuration : /etc/neutron/neutron.conf --"
    cp -p /etc/neutron/neutron.conf /etc/neutron/neutron.conf.DIST
    cp -p $TEMPLATE_DIR/neutron_neutron.conf /etc/neutron/neutron.conf
    cat /etc/neutron/neutron.conf

    echo " -- Configure the Modular Layer 2 (ML2) plug-in --"
    cp -p /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini.DIST
    cp -p $TEMPLATE_DIR/neutron_ml2_conf.ini  /etc/neutron/plugins/ml2/ml2_conf.ini
    cat /etc/neutron/plugins/ml2/ml2_conf.ini

    echo " -- Configure the Linux bridge agent --"
    sed -e "s/<--HOST_IP-->/${HOST_IP}/g" $TEMPLATE_DIR/neutron_linuxbridge_agent.ini > $TMP_DIR/neutron_linuxbridge_agent.ini 
    sed -i -e "s/<--HOST_INTERFACE-->/${HOST_INTERFACE}/g" $TMP_DIR/neutron_linuxbridge_agent.ini 
    cp -p /etc/neutron/plugins/ml2/linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini.DIST
    cp -p $TMP_DIR/neutron_linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini
    rm -f $TMP_DIR/neutron_linuxbridge_agent.ini
    cat /etc/neutron/plugins/ml2/linuxbridge_agent.ini

    echo " -- Configure L3 agent - Linux bridge --"
    cp -p /etc/neutron/l3_agent.ini /etc/neutron/l3_agent.ini.DIST
    cp -p $TEMPLATE_DIR/neutron_l3_agent.ini /etc/neutron/l3_agent.ini
    cat /etc/neutron/l3_agent.ini

    echo " -- Configure the DHCP agent --"
    cp -p /etc/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini.DIST
    cp -p $TEMPLATE_DIR/neutron_dhcp_agent.ini /etc/neutron/dhcp_agent.ini
    cat /etc/neutron/dhcp_agent.ini

    echo " -- Configure the metadata agent --"
    cp -p /etc/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini.DIST
    cp -p $TEMPLATE_DIR/neutron_metadata_agent.ini /etc/neutron/metadata_agent.ini
    cat /etc/neutron/metadata_agent.ini


    echo " -- creation de la base de donnees -- "
    mysql -u root -e "CREATE DATABASE neutron;"
    mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY 'stack';"
    mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY 'stack';"


    echo " -- initialisation de la BDD de Neutron --"
    neutron-db-manage --config-file /etc/neutron/neutron.conf \
      --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head

    echo " -- relance des services --"
    service neutron-server restart
    service neutron-linuxbridge-agent restart
    service neutron-dhcp-agent restart
    service neutron-metadata-agent restart
    service neutron-l3-agent restart

    #echo " -- relance des services de nova --"
    service nova-api restart

}


function check_install_neutron {

    echo " -- keystone access config --"
    source $TOP_DIR/keystone_credentials

    echo " -- verification --"
    openstack extension list --network
    openstack network agent list
    # Linux bridge agent
    # DHCP agent
    # Metadata agent
    # L3 agent
    # ....

}

function init_neutron {

    echo " # -------- Initialisation de Neutron ------------------"

    echo " -- keystone access config --"
    source $TOP_DIR/keystone_credentials

    echo " -- creation du reseau external --"
    # creation d'un reseau 
    openstack network create  --share --external \
      --provider-physical-network provider \
      --provider-network-type flat \
      ${EXTERNAL_NETWORK_NAME}

    # creation d'un sous reseau
    openstack subnet create --network ${EXTERNAL_NETWORK_NAME} \
      --allocation-pool start=203.0.113.101,end=203.0.113.250 \
      --dns-nameserver 8.8.4.4 --gateway 203.0.113.1 \
      --subnet-range 203.0.113.0/24 \
      ${EXTERNAL_NETWORK_NAME}

    echo "-- verification de la connexion --"
    #ping -c 3 203.0.113.1
    ping -c 3 203.0.113.2

    echo " -- verification des ip flottantes --"
    openstack floating ip create --project demo ${EXTERNAL_NETWORK_NAME}
    openstack floating ip list
    # suppression de l'IP flottante
}
