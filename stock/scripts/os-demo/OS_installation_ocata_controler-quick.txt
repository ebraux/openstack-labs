
# Commandes pour installation d'Openstack
#  - prerrequis
#  - keystone
#  - glance
#  - nova
#  - neutron
#  - horizon
#
# Source :
#  - http://docs.openstack.org/ocata/install-guide-ubuntu/
#  - http://tristan.lt/noolab/
#
# mot de passe generique : stack
#

# -------------------------------------------
#    Pr�paration de l'environement de TP
#
#     _____            _                                                 _
#    | ____|_ ____   _(_)_ __ ___  _ __  _ __   ___ _ __ ___   ___ _ __ | |_
#    |  _| | '_ \ \ / / | '__/ _ \| '_ \| '_ \ / _ \ '_ ` _ \ / _ \ '_ \| __|
#    | |___| | | \ V /| | | | (_) | | | | | | |  __/ | | | | |  __/ | | | |_
#    |_____|_| |_|\_/ |_|_|  \___/|_| |_|_| |_|\___|_| |_| |_|\___|_| |_|\__|
#
# -------------------------------------------

# ---------------------------------------
#   Configuration dans Openstack
# ---------------------------------------
# creation d'un groupe de s�curit�
#<projet>Openstack
# - entr�e TCP, port 80
# - entr�e TCP, port 443
# - entr�e TCP, port 6080
# - entr�e TCP, port 6082
# - entr�e TCP, port 5000 (identity)
# - entr�e TCP, port 9292 (image)
# - entr�e TCP, port 8774 (compute)
# - entr�e TCP, port 8778 (compute - placement)
# - entr�e TCP, port 6080 (compute - vnc)
# - entr�e TCP, port 6082 (compute - spice)
# - entr�e TCP, port 9696 (network)

# creation d'une instance
# - Nom : <projet>OS
# - Source : image "Ubuntu_16.04", sans volume
# - Gabarit : OS-small
# - R�seau : <projet>PrivateSubnet1
# - Groupes de s�curit� : <projet>AdminAccess, <projet>Openstack
# - Paire de cl� : Demo1
# - Configuration :
#       activation de la connection pour l'utilisateur Ubuntu avec mot de passe = "ubuntu"
#       configuration du proxy pour apt
# - affectation d'une IP publique



# ---------------------------   
# Configuration de l'environnement de la machine
# ---------------------------   
sudo su -

# apt catcher de Telecom Bretagne
echo 'Acquire::http::Proxy "http://apt-cacher-01.priv.enst-bretagne.fr:3142";' > /etc/apt/apt.conf.d/01proxy
cat /etc/apt/apt.conf.d/01proxy

# ---------------------------   
# r�cup�ration de sfichiers de configuration
# ---------------------------   
mkdir staging
cd staging

wget -r  --no-parent --no-directories --reject="index.html*" -e robots=off http://formations.telecom-bretagne.eu/syst/Cloud/openstack/TP/installation_ocata/



# --------------------------------------------------------------   
#    Prerequis
#
#     ____                                _
#    |  _ \ _ __ ___ _ __ ___  __ _ _   _(_)___
#    | |_) | '__/ _ \ '__/ _ \/ _` | | | | / __|
#    |  __/| | |  __/ | |  __/ (_| | |_| | \__ \
#    |_|   |_|  \___|_|  \___|\__, |\__,_|_|___/
#                            |_|
#
# --------------------------------------------------------------   


# --- acc�s aux packages Openstack, et mise � jour du syst�me ---
apt install software-properties-common
add-apt-repository -y cloud-archive:ocata
apt update && apt -y dist-upgrade
apt clean


# --- config r�seau ---
# nom de la machine :
HOST_NAME=$(hostname)

# Ip de la machine
HOST_IP=$(ifconfig | grep 'inet addr:172.16' | awk '{ print $2 }' | awk -F ':' '{ print $2 }')

HOST_INTERFACE='ens3'


echo $HOST_NAME
echo $HOST_IP
echo $HOST_INTERFACE

# configuration du fichier /etc/hosts
# - declaration du nom "controller"
# - correction du pb de resolution DNS pour le hostname
cat /etc/hosts 
cp -p /etc/hosts /etc/hosts.ORI
echo "${HOST_IP} controller ${HOST_NAME}" >> /etc/hosts
cat /etc/hosts 
ping controller

# --- Installation des composants Pr�requis ---
# -- base de donn�es : MariaDB (mysql, postgres, ...) --
# installation de  mariadb-server
apt install -y mariadb-server python-pymysql

# Configuration :  /etc/mysql/mariadb.conf.d/99-openstack.cnf
# - configuration de l'IP d'ecoute
# - activation de innoDB
# - augmenttaion du nombre de connections
# - getsion de l'UTF8
cat mariaDB_99-openstack.cnf
sed -i -e "s/<--HOST_IP-->/${HOST_IP}/g" mariaDB_99-openstack.cnf 
cat mariaDB_99-openstack.cnf
cp -p mariaDB_99-openstack.cnf /etc/mysql/mariadb.conf.d/99-openstack.cnf
service mysql restart

# securisation
mysql_secure_installation <<EOF

y
stack
stack
y
y
y
y
EOF
# ->    Enter current password for root (enter for none): <ENTER>
# ->    Set root password? [Y/n] Y
# ->    New password: stack
# ->    Re-enter new password: stack
# ->    Remove anonymous users? [Y/n] Y
# ->    Disallow root login remotely? [Y/n] Y
# ->    Remove test database and access to it? [Y/n] Y
# ->    Reload privilege tables now? [Y/n] Y



# Test : 
#mysql -u root -p
# -> Enter password:
# ->MariaDB [(none)]> show databases;
# ->MariaDB [(none)]> use mysql;
# ->MariaDB [mysql]> show tables;
# ->MariaDB [mysql]> quit;


# -- Message Queue : RabbitMQ --
apt install -y rabbitmq-server

# creation user
rabbitmqctl add_user openstack stack

# permissions
rabbitmqctl set_permissions openstack ".*" ".*" ".*"


# -- donn�es partag�es: memcache --
apt install -y memcached python-memcache

# Configuration : /etc/memcached.conf
# - configuration de l'IP d'ecoute
sed -i -e "s/<--HOST_IP-->/${HOST_IP}/g" memcached_memcached.conf
cp -p /etc/memcached.conf /etc/memcached.conf.DIST
cp -p memcached_memcached.conf /etc/memcached.conf
cat  /etc/memcached.conf
service memcached restart

# -- serveur WEB : Apache --
apt install -y apache2 libapache2-mod-wsgi

# Configuration /etc/apache2/sites-enabled/000-default.conf 
# - modification du servername par defaut = "controller"
cat apache_000-default.conf
cp -p /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.DIST
cp apache_000-default.conf /etc/apache2/sites-available/000-default.conf
service apache2 restart

# -- Menage -- 
apt clean


# --------------------------------------------------------------   
#   identity service : keystone
#
#      _  __              _
#     | |/ /___ _   _ ___| |_ ___  _ __   ___
#     | ' // _ \ | | / __| __/ _ \| '_ \ / _ \
#     | . \  __/ |_| \__ \ || (_) | | | |  __/
#     |_|\_\___|\__, |___/\__\___/|_| |_|\___|
#               |___/
#
# --------------------------------------------------------------   
# Options d'installation :
# - utilisation d'un backend Mysql
# - utilisation d'Apache
# - initialisation de l'infratsructure en mode "bootstrap"
#    - project : "admin"
#    - user : "admin"
#    - role : "admin"
#    - role "admin" affecte a "admin" pour le projet "admin"
#    - region : RegionOne
#    - endpoint "internal" et "admin" identiques : http://controller:35357/v3/
#    - endpoint "public" : http://controller:5000/v3/


# Cr�ation de la base de donn�es
mysql -u root -e "CREATE DATABASE keystone;"
mysql -u root -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%'         IDENTIFIED BY 'stack';"


# Installation des packages
apt install keystone

# Configuration : /etc/keystone/keystone.conf
# - acc�s � la base de donn�es
cp -p /etc/keystone/keystone.conf /etc/keystone/keystone.conf.DIST
cp -p keystone_keystone.conf /etc/keystone/keystone.conf


# Initialisation des donn�es 
keystone-manage db_sync

# Initialisation du "Fernet key repositories"
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone


# -- bootstrap de l'infrastructure --
keystone-manage bootstrap --bootstrap-password stack \
  --bootstrap-admin-url http://controller:35357/v3/ \
  --bootstrap-internal-url http://controller:35357/v3/ \
  --bootstrap-public-url http://controller:5000/v3/ \
  --bootstrap-region-id RegionOne


# Menage
# suppression de la base sqLite par d�faut
rm -f /var/lib/keystone/keystone.db

# fichiers � consulter :
# - /etc/keystone/keystone.conf.DIST
# - /etc/apache2/sites-available/keystone.conf




# --------------------------------------------------------------   
# Openstack Client
#
#      ___                       _             _       ____ _ _            _
#     / _ \ _ __   ___ _ __  ___| |_ __ _  ___| | __  / ___| (_) ___ _ __ | |_
#    | | | | '_ \ / _ \ '_ \/ __| __/ _` |/ __| |/ / | |   | | |/ _ \ '_ \| __|
#    | |_| | |_) |  __/ | | \__ \ || (_| | (__|   <  | |___| | |  __/ | | | |_
#     \___/| .__/ \___|_| |_|___/\__\__,_|\___|_|\_\  \____|_|_|\___|_| |_|\__|
#          |_|
#
# --------------------------------------------------------------   

# cr�ation des �l�ments compl�mentaires

# installation des "clients" Openstack
apt install -y python-openstackclient


#Exemple d'appel sans environnement :
openstack  \
  --os-auth-url http://controller:35357/v3 \
  --os-identity-api-version 3 \
  --os-project-domain-name Default \
  --os-user-domain-name Default \
  --os-project-name admin \
  --os-username admin \
  project list



# Exemple d'appel avec environnement pr�configur� :
# cr�ation du fichier d'environnement
cp clients_admin-openrc ~/admin-openrc

# Lancement de la commande
source ~/admin-openrc
openstack project list


# ----------------------------------------------------
# Create a domain, projects, users, and roles
# ----------------------------------------------------
#  -- service project --
openstack project create \
  --domain default \
  --description "Service Project" service

# observer le resultat
openstack project list
PROJECT_ID_SERVICE=$(openstack project list | grep '| service ' | awk '{ print $2 }')
openstack project show ${PROJECT_ID_SERVICE}

# -- demo project --
openstack project create \
  --domain default \
  --description "Demo Project" demo

# observer le resultat
openstack project list
PROJECT_ID_DEMO=$(openstack project list | grep '| demo ' | awk '{ print $2 }')
openstack project show ${PROJECT_ID_DEMO}

# -- demo user --
openstack user create \
  --domain default \
  --password stack \
  demo

#Remarque, on peut utilier --password-prompt

# observer le resultat
openstack user list
USER_ID_DEMO=$(openstack user list | grep '| demo ' | awk '{ print $2 }')
openstack user show ${USER_ID_DEMO}


# -- role "member" --
openstack role create member

# observer le resultat
openstack role list
ROLE_ID_MEMBER=$(openstack role list | grep '| member ' | awk '{ print $2 }')
openstack role show ${ROLE_ID_MEMBER}

# -- affecte role "member" � l'utilisateur demo, sur le projet demo --
openstack role add --project demo --user demo member

# -- affecte role "member" � l'utilisateur admin, sur le projet demo --
openstack role add --project demo --user admin member

# observer le resultat
openstack role  assignment list
openstack role  assignment list --names
openstack role  assignment list --names --project demo



# -- test de gestion des token "admin" --
openstack token issue



# ----------------------------------------------------
#   image service : glance
#
#           _
#      __ _| | __ _ _ __   ___ ___
#     / _` | |/ _` | '_ \ / __/ _ \
#    | (_| | | (_| | | | | (_|  __/
#     \__, |_|\__,_|_| |_|\___\___|
#     |___/
#
# ----------------------------------------------------
# Installation du service Glance
# - configuraton keystone
#    - utilisateur "glance" dans le projet "Service"
#    - cr�ation d'un service de type "image"
#    - endpoint "public", "internal" et "admin" identiques : http://controller:9292
# - base de donn�es Mysql
# - utilisation de memcached
# - backend de type file


# -- cr�ation de la base de donn�es --
mysql -u root -e "CREATE DATABASE glance;"
mysql -u root -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY 'stack';" 


# -- d�claration du composant dans keystone --
openstack user create --domain default --password stack glance
openstack role add --project service --user glance admin
openstack service create --name glance --description "OpenStack Image" image

openstack endpoint create --region RegionOne image public http://controller:9292
openstack endpoint create --region RegionOne image internal http://controller:9292
openstack endpoint create --region RegionOne image admin http://controller:9292
openstack endpoint list | grep image

# observation du r�sultat
openstack service list
openstack endpoint list


# -- Installation --
apt install glance


# Configuration
# - connection � la base de donn�es [database]
# - utilisation de keystone pour l'authentification [keystone_authtoken]
# - utilisation de memcached [keystone_authtoken]
# - utilisation de keystone pour la gestion du service [paste_deploy]
# - stockage en mode fichier [glance_store]
# - format d'images support�s [image_format]
#    Glance API : /etc/glance/glance-api.conf
cp -p /etc/glance/glance-api.conf /etc/glance/glance-api.conf.DIST
cp glance_glance-api.conf /etc/glance/glance-api.conf
#    Glance Registry : /etc/glance/glance-registry.conf
cp -p /etc/glance/glance-registry.conf /etc/glance/glance-registry.conf.DIST
cp  glance_glance-registry.conf /etc/glance/glance-registry.conf


# initialisation de la base de donn�es
glance-manage db_sync

# relance des services
service glance-registry restart
service glance-api restart


# Test : ajout d'une image "cirros-cloud" (tr�s l�g�re)
# http://download.cirros-cloud.net/0.3.5/cirros-0.3.5-x86_64-disk.img
openstack image create "cirros" \
  --file cirros-0.3.5-x86_64-disk.img \
  --disk-format qcow2 \
  --container-format bare \
  --public
    
# observation du r�sultat
openstack image list
IMAGE_ID_CIRROS=$(openstack image list | grep '| cirros ' | awk '{ print $2 }')
openstack image show ${IMAGE_ID_CIRROS}



# m�nage
rm -f /var/lib/glance/glance.sqlite

# fichiers � consulter :
# - /etc/glance/glance-api.conf.DIST
# - /etc/glance/glance-registry.conf.DIST


# ----------------------------------------------------
#   compute service : nova
#      _ __   _____   ____ _
#     | '_ \ / _ \ \ / / _` |
#     | | | | (_) \ V / (_| |
#     |_| |_|\___/ \_/ \__,_|
#
# ----------------------------------------------------
# Installation du service Nova
# - configuraton keystone
#    - utilisateur "nova" dans le projet "service"
#    - cr�ation d'un service de type "compute"
#    - endpoint "public", "internal" et "admin" identiques : http://controller:8774/v2.1
# - base de donn�es Mysql
# - utilisation de memcached
# - utilisation de neutron pour la getsion du r�seau


# cr�ation de la base de donn�es :
mysql -u root -e"CREATE DATABASE nova_api;"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY 'stack';"

mysql -u root -e"CREATE DATABASE nova;"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY 'stack';"

mysql -u root -e"CREATE DATABASE nova_cell0;"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e"GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY 'stack';"

# d�claration du composant dans keystone
openstack user create --domain default --password stack nova
openstack role add --project service --user nova admin
openstack service create --name nova --description "OpenStack Compute" compute
openstack endpoint create --region RegionOne compute public http://controller:8774/v2.1
openstack endpoint create --region RegionOne compute internal http://controller:8774/v2.1
openstack endpoint create --region RegionOne compute admin http://controller:8774/v2.1
openstack endpoint list | grep compute

# d�claration du "Placement service"
openstack user create --domain default --password stack placement
openstack role add --project service --user placement admin
openstack service create --name placement --description "Placement API" placement
openstack endpoint create --region RegionOne placement public http://controller:8778
openstack endpoint create --region RegionOne placement internal http://controller:8778
openstack endpoint create --region RegionOne placement admin http://controller:8778
openstack endpoint list | grep placement


# -- Installation --
apt install nova-api nova-conductor nova-consoleauth nova-novncproxy nova-scheduler nova-placement-api
apt install nova-compute

# bug Ditrib Ubuntu
# https://bugs.launchpad.net/openstack-manuals/+bug/1457423
chown -R nova:nova /var/lib/nova
chown -R nova:nova /var/log/nova

chmod -R 775 /var/lib/nova
chmod -R 775 /var/log/nova

# -- Configuration /etc/nova/nova.conf
- connection � la base de donn�es [database] [api_database]
- connexion � RabbitMQ : [DEFAULT]
- utilisation de keystone pour l'authentification [DEFAULT] [keystone_authtoken]
- configuration r�seau [DEFAULT]  
- configuration de la console VNC
- configuration de l'utilisation de glance pour stocker les images [glance]
- divers (osle_concurrency, ...)  [oslo_concurrency]
- configuration de l'hyperviseur [libvirt]
# Configuration : /etc/nova/nova.conf
# - configuration de l'IP d'ecoute
sed -i -e "s/<--HOST_IP-->/${HOST_IP}/g" nova_nova.conf
cp -p /etc/nova/nova.conf /etc/nova/nova.conf.DIST
cp -p nova_nova.conf /etc/nova/nova.conf

# -- Configuration /etc/nova/nova.conf
sed -i -e "s/<--HOST_IP-->/${HOST_IP}/g" COMPUTE_nova_nova.conf
cp -p /etc/nova/nova-compute.conf /etc/nova/nova-compute.conf.DIST
cp -p COMPUTE_nova_nova.conf /etc/nova/nova-compute.conf


# initialisation de la BDD de nova-api
nova-manage api_db sync

# Register the cell0 database
nova-manage cell_v2 map_cell0

#Create the cell1 cell
nova-manage cell_v2 create_cell --name=cell1 --verbose

# initialisation de la BDD de nova
nova-manage db sync

# Verify nova cell0 and cell1 are registered correctly
nova-manage cell_v2 list_cells

# relance des services
service nova-api restart
service nova-consoleauth restart
service nova-scheduler restart
service nova-conductor restart
service nova-novncproxy restart
service nova-compute restart



# -- activation de "compute"
nova-manage cell_v2 discover_hosts --verbose
openstack hypervisor list
# - controller

# -- verfication des services 
openstack compute service list
# - nova-consoleauth
# - nova-scheduler
# - nova-conductor
# - nova-compute


openstack catalog list
# - placement
# - nova
# - keystone
# - glance

#Check the cells and placement API are working successfully
nova-status upgrade check



# validation  
# creation d'un gabarit CIRROS
openstack flavor create --ram 256 --disk 0 --vcpus 1 cirros




# ----------------------------------------------------
#   network service : neutron
#     _ __   ___ _   _| |_ _ __ ___  _ __
#    | '_ \ / _ \ | | | __| '__/ _ \| '_ \
#    | | | |  __/ |_| | |_| | | (_) | | | |
#    |_| |_|\___|\__,_|\__|_|  \___/|_| |_|
#
# ----------------------------------------------------
# Installation du service Neutron
# - configuraton keystone
#    - utilisateur "neutron" dans le projet "Service"
#    - cr�ation d'un service de type "network"
#    - endpoint "public", "internal" et "admin" identiques : http://controller:9696
# - base de donn�es Mysql
# - utilisation de memcached
# - ml2 plugin options : 
#     flat, VLAN, and VXLAN networks
#     VXLAN self-service networks
#     Linux bridge and layer-2 population mechanisms
#     port security extension driver
# - configuration d'un reseau "externe"
#   * uniquement disponible depuis la machine (non attache a une interface physique)
#   * associ� � la plage d'IP reservee 203.0.113.0/24 (TEST-NET-3)

# activation du bridging in the kernel
modprobe bridge 8021q
echo '8021q' >> /etc/modules

# cr�ation du bridge pour le reseau externe
cp -p network_50-bridge-ex.cfg  /etc/network/interfaces.d/50-bridge-ex.cfg
systemctl restart networking

#verification
brctl show
ip route


# -- cr�ation de la base de donn�es --
mysql -u root -e "CREATE DATABASE neutron;"
mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY 'stack';"
mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY 'stack';" 


# -- d�claration du composant dans keystone --
openstack user create --domain default --password stack neutron
openstack role add --project service --user neutron admin
openstack service create --name neutron --description "OpenStack Networking" network

openstack endpoint create --region RegionOne network public http://controller:9696
openstack endpoint create --region RegionOne network internal http://controller:9696
openstack endpoint create --region RegionOne network admin http://controller:9696
openstack endpoint list | grep network

# observation du r�sultat
openstack service list
openstack endpoint list


# Installation des composants
apt install neutron-server neutron-plugin-ml2 \
  neutron-linuxbridge-agent neutron-l3-agent neutron-dhcp-agent \
  neutron-metadata-agent

# Configuration : /etc/neutron/neutron.conf
cp -p /etc/neutron/neutron.conf /etc/neutron/neutron.conf.DIST
cp -p neutron_neutron.conf /etc/neutron/neutron.conf
cat /etc/neutron/neutron.conf

# Configure the Modular Layer 2 (ML2) plug-in
cp -p /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini.DIST
cp -p neutron_ml2_conf.ini  /etc/neutron/plugins/ml2/ml2_conf.ini
cat /etc/neutron/plugins/ml2/ml2_conf.ini

# Configure the Linux bridge agent
sed -i -e "s/<--HOST_IP-->/${HOST_IP}/g" neutron_linuxbridge_agent.ini 
sed -i -e "s/<--HOST_INTERFACE-->/${HOST_INTERFACE}/g" neutron_linuxbridge_agent.ini 
cp -p /etc/neutron/plugins/ml2/linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini.DIST
cp -p neutron_linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini
cat /etc/neutron/plugins/ml2/linuxbridge_agent.ini

# Configure the Linux bridge agent
cp -p /etc/neutron/l3_agent.ini /etc/neutron/l3_agent.ini.DIST
cp -p neutron_l3_agent.ini /etc/neutron/l3_agent.ini
cat /etc/neutron/l3_agent.ini

# Configure the DHCP agent
cp -p /etc/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini.DIST
cp -p neutron_dhcp_agent.ini /etc/neutron/dhcp_agent.ini
cat /etc/neutron/dhcp_agent.ini

# Configure the metadata agent
cp -p /etc/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini.DIST
cp -p neutron_metadata_agent.ini /etc/neutron/metadata_agent.ini
cat /etc/neutron/metadata_agent.ini

# verification que la configuration de nova continet bien les infos pour neutron
cat /etc/nova/nova.conf

# initialisation de la BDD de Neutron
neutron-db-manage --config-file /etc/neutron/neutron.conf \
  --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head

# relance des services
service nova-api restart

service neutron-server restart
service neutron-linuxbridge-agent restart
service neutron-dhcp-agent restart
service neutron-metadata-agent restart

service neutron-l3-agent restart

# v�rification
openstack extension list --network
openstack network agent list
# Linux bridge agent
# DHCP agent
# Metadata agent
# L3 agent
# ....


# creation du reseau externe# creation d'un reseau
EXTERNAL_NETWORK_NAME=external
openstack network create  --share --external \
  --provider-physical-network provider \
  --provider-network-type flat \
  ${EXTERNAL_NETWORK_NAME}

# creation d'un sous r�seau
openstack subnet create --network ${EXTERNAL_NETWORK_NAME} \
  --allocation-pool start=203.0.113.101,end=203.0.113.250 \
  --dns-nameserver 8.8.4.4 --gateway 203.0.113.1 \
  --subnet-range 203.0.113.0/24 \
  ${EXTERNAL_NETWORK_NAME}


# verification
ping 203.0.113.2
openstack floating ip create --project demo ${EXTERNAL_NETWORK_NAME}
openstack floating ip list

# ----------------------------------
# dashboard : horizon
#       _   _            _
#      | | | | ___  _ __(_)_______  _ __
#      | |_| |/ _ \| '__| |_  / _ \| '_ \
#      |  _  | (_) | |  | |/ / (_) | | | |
#      |_| |_|\___/|_|  |_/___\___/|_| |_|
#
# ----------------------------------
# Installation du service Horizon
# - connection a keystone
# - configuration de la version des API (keystone, glance, nova)
# - utilisation de memcached
# - configuration des valeurs par defaut de l'infra (domaine, role, ...)

# Installation
apt install openstack-dashboard

# Configuration  /etc/openstack-dashboard/local_settings.py
cp -p /etc/openstack-dashboard/local_settings.py /etc/openstack-dashboard/local_settings.py.DIST
cp horizon_local_settings.py /etc/openstack-dashboard/local_settings.py

# Relance du service
service apache2 restart


INSTANCE_PUBLIC_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
#Url pour test de connexion : 
echo "http://${INSTANCE_PUBLIC_IP}/horizon"




# ----------------------------------
#  Configuration de l'infrastructure
#     ___        __               _                   _
#    |_ _|_ __  / _|_ __ __ _ ___| |_ _ __ _   _  ___| |_ _   _ _ __ ___
#     | || '_ \| |_| '__/ _` / __| __| '__| | | |/ __| __| | | | '__/ _ \
#     | || | | |  _| | | (_| \__ \ |_| |  | |_| | (__| |_| |_| | | |  __/
#    |___|_| |_|_| |_|  \__,_|___/\__|_|   \__,_|\___|\__|\__,_|_|  \___|
#
# ----------------------------------


Pour le projet "demo", en tant que "demo"
source clients_demo-openrc

# configuration
PROJECT_NAME=demo
NETWORK_NAME=${PROJECT_NAME}selfservice
SUBNET_NAME=${PROJECT_NAME}selfservice
ROUTER_NAME=${PROJECT_NAME}Routeur

#EXTERNAL_NETWORK_NAME=external
# ->  defini dans la config de neutron

# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous r�seau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 8.8.4.4 --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
#neutron router-interface-add router selfservice
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le r�seau externe
#neutron router-gateway-set router provider
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

# configuration DNS
#openstack subnet set --dns-nameserver 192.44.75.10 ${SUBNET_NAME}
#openstack subnet set --dns-nameserver 192.108.115.2 ${SUBNET_NAME}

# creation des groupes de s�curit�
openstack security group create --description 'Admin Basiques rules' ${PROJECT_NAME}BasicAdmin
openstack security group rule create --proto icmp --dst-port 0 ${PROJECT_NAME}BasicAdmin
openstack security group rule create --proto tcp --dst-port 22 ${PROJECT_NAME}BasicAdmin


# creation d'une instance
# m�morisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list | grep '| cirros ' | awk '{ print $2 }')
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list | grep ' cirros ' | awk '{ print $2 }')
echo ${IMAGE_CIRROS_ID}


# lancement d'une instance
# creation d'un instance de test
TEST_INSTANCE_NAME=${PROJECT_NAME}Test
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --nic net-id=${PROJECT_NAME}selfservice \
   --security-group ${PROJECT_NAME}BasicAdmin \
   ${TEST_INSTANCE_NAME}


# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=`openstack floating ip list | grep '| None             | None' | head -1 | awk '{ print $4 }'`
openstack server add floating ip ${TEST_INSTANCE_NAME} ${FREE_FLOATING_IP}


ping ${FREE_FLOATING_IP}




