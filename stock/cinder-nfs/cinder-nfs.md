

``` bash
sudo apt install nfs-kernel-server
```

``` bash
sudo mkdir -p /var/lib/cinder/nfs-volumes
sudo chown -R nobody:nogroup /var/lib/cinder/nfs-volumes
sudo chmod 777 /var/lib/cinder/nfs-volumes
```

Commande utilisée par le compute pour accéder aux volumes
``` bash
mount -t nfs 172.16.10.3:/var/lib/cinder/nfs-volumes /var/lib/nova/mnt/3d385b2e5eba7ca0ec2b31cfa9bc1d14
```


/etc/exports
echo "/var/lib/cinder/nfs-volumes *(rw,sync,no_subtree_check)" | sudo tee -a /etc/exports
/mnt/nfs_share 10.0.2.15/24(rw,sync,no_subtree_check)

 sudo exportfs -rv


sudo systemctl restart nfs-kernel-server


sudo ufw status



Il existe une multitude d'options. Je vous en détaille certaines les plus couramment utilisées :

- rw : Cette option permet d'avoir les droits de lecture et d'écriture sur le répertoire partagé.
- ro : Cette option permet d'avoir les droits de lecture seule sur le répertoire partagé.
- sync : Ne répond aux requêtes qu'après avoir écrit les modifications sur le stockage (lent). C'est le comportement par défaut.
- async : Cette option enfreint le protocole nfs et répond aux requêtes même si les modifications ne sont pas écrites sur le stockage. Peut être utile dans certains cas.
- subtree_check : Active la vérification des droits d'accès des sous-dossiers. C'est le comportement par défaut.
- no_subtree_check : Ignore la vérification des droits d'accès des sous-dossiers (perte d'un petit peu de sécurité mais peut améliorer la fiabilité)
- root_squash : Force le mapping de l'utilisateur root (UID=0) vers l'utilisateur anonyme.
- no_root_squash : Ne force pas le mapping de l'utilisateur root (UID=0) vers l'utilisateur anonyme
- no_all_squash : Ne force pas le mapping des utilisateurs (Option par défaut)
- all_squash : Force le mapping de tous les utilisateurs vers l'utilisateur anonyme. Utile pour un FTP public exporté par NFS
- anonuid=NNN : Définit l'UID de l'utilisateur anonyme (NNN est le nombre du UID concerné)
- anongid=NNN : Définit l'UID de l'utilisateur anonyme (NNN est le nombre du UID concerné)

---
## Client

sudo apt install nfs-common
sudo mkdir -p /mnt/cinder-nfs-volumes

sudo mount 172.16.10.3:/var/lib/cinder/nfs-volumes /mnt/cinder-nfs-volumes

touch /mnt/cinder-nfs-volumes/test.txt
cloud-user@controller-lab-01:/etc/iscsi$ ll /mnt/cinder-nfs-volumes


https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-22-04
https://vegastack.com/tutorials/how-to-install-and-configure-an-nfs-server-on-ubuntu-22-04/

https://www.linuxtricks.fr/wiki/nfs-parametrer-les-partages-avec-le-fichier-exports
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/s1-nfs-server-config-exports



---
Openstack

echo "172.16.10.3:/var/lib/cinder/nfs-volumes" | sudo tee /etc/cinder/nfs_shares
sudo chown root:cinder /etc/cinder/nfs_shares

sudo chmod 0640 /etc/cinder/nfs_shares