#!/bin/bash

# Disable Firewald 
sudo systemctl stop firewalld
sudo systemctl disable firewalld

# Disable SE Linux
sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config
sudo sed -i 's/enforcing/disabled/g' /etc/sysconfig/selinux

# Update packages
sudo dnf update -y

# Puppet version management : disable epel repo
sudo yum autoremove -y epel-release
sudo yum clean all

# Install Openstack Repo
sudo dnf config-manager --enable crb
sudo dnf install -y centos-release-openstack-yoga
sudo dnf update -y

# Install Tools and utilities
sudo dnf install -y libcurl-devel  openssl-devel
sudo dnf install -y git vim tree

# Enable password Authentification
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd.service


# Create stack user
groupadd stack
useradd -g stack       -s /bin/bash -d /home/stack  -m  stack
cd /etc/sudoers.d
echo "stack ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_stack_sh
chmod 440 /etc/sudoers.d/50_stack_sh
echo stack:stack | chpasswd

# Archivage de la clé insedure de vagrant pour pouvoir faire une box si besoin
#sudo cp -p /root/.ssh/authorized_keys /root/.ssh/authorized_keys.vagrant-insecure

