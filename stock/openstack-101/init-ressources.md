# Initialisation d'un environnement de demo


Pour initialiser un réseau public
-
Déployer un réseau externe
``` bash
export ENABLE_EXT_NET=1

export EXT_NET_CIDR='10.20.11.0/24'
export EXT_NET_RANGE='start=10.20.11.150,end=10.20.11.199'
export EXT_NET_GATEWAY='10.20.11.1'

export DEMO_NET_CIDR='192.168.1.0/24'
export DEMO_NET_GATEWAY='192.168.1.1'
export DEMO_NET_DNS='8.8.8.8'

source /etc/kolla/admin-openrc.sh
~/kolla-ansible/share/kolla-ansible/init-runonce
```

``` bash
openstack network list
openstack subnet list
openstack router list
```

Pour créer une instance

``` bash
openstack server create \
    --image cirros \
    --flavor m1.tiny \
    --key-name mykey \
    --network demo-net \
    demo1
```
