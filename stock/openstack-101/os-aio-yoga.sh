#!/bin/bash

# ----------------------------------------------------------
#      Openstack All In One specific
# ----------------------------------------------------------

# Création du bridge pour br-ex
sudo nmcli conn add type bridge con-name br-ex ifname br-ex
sudo nmcli conn modify br-ex ipv4.addresses '203.0.113.2/24'
sudo nmcli conn modify br-ex ipv4.method manual
sudo nmcli conn up br-ex

# connexion ssh as root for packstack, IP 172.16.50.51
#sudo mkdir /root/.ssh
#sudo chmod 700 /root/.ssh
#sudo ssh-keygen -q -t rsa -b 2048 -N '' -f /root/.ssh/id_rsa <<< y
#sudo ls -la /root
#sudo ls -la /root/.ssh
#sudo touch /root/.ssh/authorized_keys
#sudo cat /root/.ssh/id_rsa.pub | sudo tee -a /root/.ssh/authorized_keys

# récupération des fihiers de conf
cd ~
git clone https://gitlab.com/ebraux/openstack-labs-installation.git
cd openstack-labs-installation/
git checkout yoga-centos9s
cp -R docs/02_controller/conf ~/openstack_config

cd ~/openstack_config/
for CONFIG_FILE in `grep -l -R controller *`; do sed -i 's/controller/osaio/g' $CONFIG_FILE; done
for CONFIG_FILE in `grep -l -R '172.16.50.21'  *`; do sed -i 's/172.16.50.21/172.16.52.61/g' $CONFIG_FILE; done
sed -i 's/8080/8261/g' httpd_confd-listen.conf

# prérequis
sudo dnf install -y centos-release-openstack-yoga
sudo dnf config-manager --enable crb
sudo dnf update
sudo dnf upgrade -y
#
sudo dnf install -y  python3-openstackclient
#
sudo dnf install -y  vim nano jq
#
sudo yum install -y chrony
sudo mv  /etc/chrony.conf /etc/chrony.conf.DIST
sudo cp ~/openstack_config/chrony.conf /etc/chrony.conf
sudo chown root.root /etc/chrony.conf
sudo systemctl enable chronyd.service
sudo systemctl restart chronyd.service
#
sudo dnf install -y mariadb mariadb-server python3-PyMySQL
sudo cp ~/openstack_config/mysql_99-openstack.cnf /etc/my.cnf.d/99-openstack.cnf
sudo chown root.root /etc/ /etc/my.cnf.d/99-openstack.cnf
sudo systemctl enable mariadb.service
sudo systemctl start mariadb.service
#
sudo dnf install -y rabbitmq-server
sudo systemctl enable rabbitmq-server.service
sudo systemctl start rabbitmq-server.service
sudo rabbitmqctl add_user openstack stack
sudo rabbitmqctl set_permissions openstack ".*" ".*" ".*"
#
sudo dnf install -y memcached python3-memcached
sudo cp /etc/sysconfig/memcached /etc/sysconfig/memcached.DIST
sudo cp ~/openstack_config/memcached.conf /etc/sysconfig/memcached
sudo chown root.root /etc/sysconfig/memcached
sudo systemctl enable memcached.service
sudo systemctl start memcached.service
#
sudo dnf install -y etcd
sudo cp -p /etc/etcd/etcd.conf  /etc/etcd/etcd.conf.DIST
sudo cp ~/openstack_config/etcd_etcd.conf /etc/etcd/etcd.conf
sudo chown root.root /etc/etcd/etcd.conf
sudo systemctl enable etcd
sudo systemctl restart etcd
#
sudo dnf install -y httpd python3-mod_wsgi
sudo cp ~/openstack_config/httpd_confd-servername.conf /etc/httpd/conf.d/servername.conf
sudo chown root.root /etc/httpd/conf.d/servername.conf
sudo chmod 644 /etc/httpd/conf.d/servername.conf
sudo cp ~/openstack_config/httpd_confd-listen.conf /etc/httpd/conf.d/listen.conf
sudo chown root.root /etc/httpd/conf.d/listen.conf
sudo chmod 644 /etc/httpd/conf.d/listen.conf
sudo systemctl enable httpd
sudo systemctl restart httpd
echo "hello controller" | sudo tee /var/www/html/index.html
#
# --- Keystone ---
#
sudo mysql -e "CREATE DATABASE keystone;"
sudo mysql -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%'         IDENTIFIED BY 'stack';"
sudo mysql -e "FLUSH PRIVILEGES;"
sudo dnf install -y openstack-keystone
sudo mv  /etc/keystone/keystone.conf /etc/keystone/keystone.conf.DIST
sudo cp ~/openstack_config/keystone_keystone.conf /etc/keystone/keystone.conf
sudo chown root.keystone /etc/keystone/keystone.conf
sudo su -s /bin/sh -c "keystone-manage db_sync" keystone
sudo keystone-manage fernet_setup \
  --keystone-user keystone \
  --keystone-group keystone
sudo keystone-manage credential_setup \
  --keystone-user keystone \
  --keystone-group keystone
sudo keystone-manage bootstrap --bootstrap-password stack \
  --bootstrap-admin-url http://osaio:5000/v3/ \
  --bootstrap-internal-url http://osaio:5000/v3/ \
  --bootstrap-public-url http://osaio:5000/v3/ \
  --bootstrap-region-id RegionOne
sudo ln -s /usr/share/keystone/wsgi-keystone.conf /etc/httpd/conf.d/
sudo systemctl enable httpd.service
sudo systemctl restart httpd.service
sudo rm -f /var/lib/keystone/keystone.db
#
cp ~/openstack_config/openrc_admin_admin ~/admin-openrc
source ~/admin-openrc
openstack project create \
  --domain default \
  --description "Service Project" service
#
openstack project create \
  --domain default \
  --description "Demo Project" demo
openstack user create \
  --domain default \
  --password stack \
  demo
openstack role add --project demo --user demo member
openstack role add --project demo --user admin member
sudo cp ~/openstack_config/openrc_demo_demo ~/demo-openrc
#
# --- Glance ---
#
source ~/admin-openrc
openstack user create --domain default --password stack glance
openstack role add --project service --user glance admin
openstack service create --name glance --description "OpenStack Image" image
openstack endpoint create --region RegionOne image public http://osaio:9292
openstack endpoint create --region RegionOne image internal http://osaio:9292
openstack endpoint create --region RegionOne image admin http://osaio:9292
sudo yum install -y openstack-glance
sudo mv  /etc/glance/glance-api.conf /etc/glance/glance-api.conf.DIST
sudo cp ~/openstack_config/glance_glance-api.conf /etc/glance/glance-api.conf
sudo chown root.glance /etc/glance/glance-api.conf
sudo mysql -u root -e "CREATE DATABASE glance;"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY 'stack';" 
sudo su -s /bin/sh -c "glance-manage db_sync" glance
rm -f /var/lib/glance/glance.sqlite
sudo systemctl enable openstack-glance-api.service
sudo systemctl start openstack-glance-api.service
wget http://download.cirros-cloud.net/0.5.2/cirros-0.5.2-x86_64-disk.img
openstack image create "cirros" \
  --file cirros-0.5.2-x86_64-disk.img  \
  --disk-format qcow2 \
  --container-format bare \
  --public
rm -f cirros-0.5.2-x86_64-disk.img
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
openstack image create "ubuntu22.04" \
  --file jammy-server-cloudimg-amd64.img  \
  --disk-format qcow2 \
  --container-format bare \
  --public 
rm -f jammy-server-cloudimg-amd64.img
#
# --- Placement ---
#
openstack user create --domain default --password stack placement
openstack role add --project service --user placement admin
openstack service create --name placement --description "Placement API" placement
openstack endpoint create --region RegionOne placement public http://osaio:8778
openstack endpoint create --region RegionOne placement internal http://osaio:8778
openstack endpoint create --region RegionOne placement admin http://osaio:8778
sudo dnf install -y openstack-placement-api 
sudo mv  /etc/placement/placement.conf /etc/placement/placement.conf.DIST
sudo cp ~/openstack_config/placement_placement.conf /etc/placement/placement.conf
sudo chown root.placement /etc/placement/placement.conf
sudo mysql -u root -e"CREATE DATABASE placement;"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'%' IDENTIFIED BY 'stack';"
sudo su -s /bin/sh -c "placement-manage db sync" placement
sudo tee -a /etc/httpd/conf.d/00-placement-api.conf > /dev/null <<EOF
<Directory /usr/bin>
   <IfVersion >= 2.4>
      Require all granted
   </IfVersion>
   <IfVersion < 2.4>
      Order allow,deny
      Allow from all
   </IfVersion>
</Directory>
EOF
sudo systemctl restart httpd
#
# --- Nova
#
openstack user create --domain default --password stack nova
openstack role add --project service --user nova admin
openstack service create --name nova --description "OpenStack Compute" compute
openstack endpoint create --region RegionOne compute public http://osaio:8774/v2.1
openstack endpoint create --region RegionOne compute internal http://osaio:8774/v2.1
openstack endpoint create --region RegionOne compute admin http://osaio:8774/v2.1
sudo dnf install -y openstack-nova-api \
  openstack-nova-conductor \
  openstack-nova-novncproxy \
  openstack-nova-scheduler

sudo mv  /etc/nova/nova.conf /etc/nova/nova.conf.DIST
sudo cp ~/openstack_config/nova_nova.conf /etc/nova/nova.conf
sudo chown root.nova /etc/nova/nova.conf
sudo mysql -u root -e"CREATE DATABASE nova_api;"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"CREATE DATABASE nova;"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"CREATE DATABASE nova_cell0;"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY 'stack';"
sudo su -s /bin/sh -c "nova-manage api_db sync" nova
sudo su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
sudo  su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
sudo  su -s /bin/sh -c "nova-manage db sync" nova
sudo systemctl enable \
    openstack-nova-api.service \
    openstack-nova-scheduler.service \
    openstack-nova-conductor.service \
    openstack-nova-novncproxy.service
sudo systemctl restart \
    openstack-nova-api.service \
    openstack-nova-scheduler.service \
    openstack-nova-conductor.service \
    openstack-nova-novncproxy.service
openstack flavor create --ram 256  --disk 1  --vcpus 1 cirros
openstack flavor create --ram 512  --disk 1  --vcpus 1 m1.tiny
openstack flavor create --ram 1024 --disk 2  --vcpus 1 m1.small
openstack flavor create --ram 2048 --disk 4  --vcpus 1 m1.large
openstack flavor create --ram 4096 --disk 8  --vcpus 1 m1.xlarge

# --- Neutron  ---
openstack user create --domain default --password stack neutron
openstack role add --project service --user neutron admin
openstack service create --name neutron --description "OpenStack Networking" network
openstack endpoint create --region RegionOne network public http://osaio:9696
openstack endpoint create --region RegionOne network internal http://osaio:9696
openstack endpoint create --region RegionOne network admin http://osaio:9696
sudo dnf install -y openstack-neutron openstack-neutron-ml2 \
  openstack-neutron-linuxbridge ebtables
sudo mv  /etc/neutron/neutron.conf /etc/neutron/neutron.conf.DIST
sudo cp ~/openstack_config/neutron_neutron.conf /etc/neutron/neutron.conf
sudo chown root.neutron /etc/neutron/neutron.conf
sudo mv /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini.DIST
sudo cp ~/openstack_config/neutron_ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini
sudo chown root.neutron /etc/neutron/plugins/ml2/ml2_conf.ini
sudo ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
sudo mv /etc/neutron/plugins/ml2/linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini.DIST
sudo cp ~/openstack_config/neutron_linuxbridge_agent.ini /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo chown root.neutron /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo mv /etc/neutron/l3_agent.ini /etc/neutron/l3_agent.ini.DIST
sudo cp ~/openstack_config/neutron_neutron_l3_agent.ini /etc/neutron/l3_agent.ini
sudo chown root.neutron /etc/neutron/l3_agent.ini
sudo mv /etc/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini.DIST
sudo cp ~/openstack_config/neutron_dhcp_agent.ini /etc/neutron/dhcp_agent.ini
sudo chown root.neutron /etc/neutron/dhcp_agent.ini
sudo mv /etc/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini.DIST
sudo cp ~/openstack_config/neutron_metadata_agent.ini /etc/neutron/metadata_agent.ini
sudo chown root.neutron /etc/neutron/metadata_agent.ini
sudo mysql -u root -e "CREATE DATABASE neutron;"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY 'stack';" 
sudo su -s /bin/sh -c "neutron-db-manage \
  --config-file /etc/neutron/neutron.conf \
  --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" \
  neutron
sudo systemctl enable neutron-server.service \
  neutron-linuxbridge-agent.service neutron-dhcp-agent.service \
  neutron-metadata-agent.service \
  neutron-l3-agent.service
sudo  systemctl start neutron-server.service \
  neutron-linuxbridge-agent.service neutron-dhcp-agent.service \
  neutron-metadata-agent.service \
  neutron-l3-agent.service
sudo systemctl restart openstack-nova-api.service
openstack network create  --share --external \
  --provider-physical-network provider \
  --provider-network-type flat \
  external
openstack subnet create --network   external \
  --allocation-pool start=203.0.113.101,end=203.0.113.250 \
  --dns-nameserver 8.8.4.4 --gateway 203.0.113.1 \
  --subnet-range 203.0.113.0/24 \
    external  
# --- cinder ---
sudo systemctl enable  iscsid
sudo systemctl start  iscsid
sudo mkdir -p /var/lib/cinder/
sudo fallocate -l 2G /var/lib/cinder/cinder-volumes
sudo losetup /dev/loop2 /var/lib/cinder/cinder-volumes
sudo cp ~/openstack_config/cinder_cinder-lvm-losetup.service /etc/systemd/system/cinder-lvm-losetup.service
sudo chown root.root /etc/systemd/system/cinder-lvm-losetup.service
systemctl enable cinder-lvm-losetup.service
systemctl start cinder-lvm-losetup.service
sudo fdisk -l /dev/loop2
sudo pvcreate  /dev/loop2
sudo vgcreate cinder-volumes /dev/loop2
openstack user create --domain default --password stack cinder
openstack role add --project service --user cinder admin
openstack service create --name cinderv3 \
  --description "OpenStack Block Storage" volumev3
openstack endpoint create --region RegionOne \
  volumev3 public http://osaio:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev3 internal http://osaio:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev3 admin http://osaio:8776/v3/%\(project_id\)s
sudo dnf install -y openstack-cinder  \
  lvm2 device-mapper-persistent-data \
  targetcli python3-keystone
sudo mv  /etc/cinder/cinder.conf /etc/cinder/cinder.conf.DIST
sudo cp ~/openstack_config/cinder_cinder.conf /etc/cinder/cinder.conf
sudo chown root.cinder /etc/cinder/cinder.conf
sudo mysql -u root -e"CREATE DATABASE cinder;"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'localhost' IDENTIFIED BY 'stack';"
sudo mysql -u root -e"GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'%' IDENTIFIED BY 'stack';"
sudo  su -s /bin/sh -c "cinder-manage db sync" cinder
sudo systemctl enable openstack-cinder-api.service \
    openstack-cinder-scheduler.service \
    openstack-cinder-volume.service target.service
sudo systemctl start openstack-cinder-api.service \
    openstack-cinder-scheduler.service \
    openstack-cinder-volume.service target.service
sudo systemctl restart openstack-nova-api.service
openstack volume create --size 1 --description "Demo Volume" vol-demo
# --- Horizon ---
sudo dnf install -y openstack-dashboard
sudo mv /etc/openstack-dashboard/local_settings /etc/openstack-dashboard/local_settings.DIST
sudo cp ~/openstack_config/horizon_local_settings.py /etc/openstack-dashboard/local_settings
sudo chown root.root /etc/openstack-dashboard/local_settings
sudo mv /etc/httpd/conf.d/openstack-dashboard.conf /etc/httpd/conf.d/openstack-dashboard.conf.DIST
sudo cp ~/openstack_config/horizon-httpd-openstack-dashboard.conf /etc/httpd/conf.d/openstack-dashboard.conf
sudo chown root.root /etc/httpd/conf.d/openstack-dashboard.conf
sudo systemctl restart httpd.service memcached.service
mkdir -p /usr/share/openstack-dashboard/openstack_dashboard/conf
services_list=( keystone glance placement nova neutron cinder  )
for i in "${services_list[@]}"
do
	echo "$i"
    oslopolicy-policy-generator  --namespace ${i}   --output-file  /usr/share/openstack-dashboard/openstack_dashboard/conf/${i}_policy.yaml
done
#
# --- Nova Compute ---
#
sudo dnf install -y  openstack-nova-compute \
  libvirt python3-libvirt qemu-kvm virt-install \
  libguestfs-tools libvirt-devel virt-top
sudo mv  /etc/nova/nova-compute.conf /etc/nova/nova-compute.conf.DIST
sudo cp ~/openstack_config/nova_nova-compute.conf /etc/nova/nova-compute.conf
sudo chown root.nova /etc/nova/nova-compute.conf
sudo systemctl enable libvirtd.service openstack-nova-compute.service
sudo systemctl start libvirtd.service openstack-nova-compute.service
sudo nova-manage cell_v2 discover_hosts --verbose
#
# --- config User Stack
cp ~/admin-openrc /home/stack/admin-openrc
chown stack.stack /home/stack/admin-openrc
cp ~/demo-openrc  /home/stack/demo-openrc
chown stack.stack /home/stack/demo-openrc
#
# --- fix rsa sha1 not suppoterd anymore in centos9
# https://serverfault.com/questions/1095898/how-can-i-use-a-legacy-ssh-rsa-key-on-centos-9-stream
echo "PubkeyAcceptedKeyTypes +ssh-rsa" | sudo tee /etc/ssh/ssh_config.d/51-fix-depreciated-rsa.conf
sudo update-crypto-policies --set LEGACY
