
# Environemment du Lab utilisant Vagrant

---
## Description de l'environnement

Vagrant est un utilitaire qui permet de déployer simplement des architectures à base de VM [https://www.vagrantup.com/](https://www.vagrantup.com/).

Vagrant s'appuie sur Virtualbox et 

- Déploie 1 machine virtuelle "osaio" qui aura les roles "control-plane" et "compute"
- Configure un réseau privé entre les 2 VMs (172.16.52.0/24) 
    - Adresse IP de 'osaio' :  172.16.52.61.
- Mets en place des redirection de ports depuis la machine hôte, pour accéder aux services sur les VM :
    - ssh sur "osaio" : 2261
    - accès à Horizon : 8261

---
## Déploiment de l'environnement

Récupérer l'intégralité du dépot des labs
```bash
git clone https://gitlab.com/ebraux/openstack-labs-101.git
cd openstack-labs-101/
git checkout yoga-centos9s
cd ..
```

Vérifier la version du TP
```bash
cat openstack-labs-101/version.txt
# yoga-centos9s
```

Copier le dossier de configuration de vagrant dans un dossier local (ici le dossier "Openstack101" à la racine du HomeDir )
```bash
cp -R openstack-labs-installation/vagrant ~/Openstack101
ll ~/Openstack101
```
Lancer l'environnement de TP
```bash
cd ~/Openstack101
vagrant up
```

---
## Utilisation de l'environnement

> Les mots de passe sont toujours "stack".

### Accès à la VM "Controller"

- Connexion via ssh
    - utilisateur :  stack
    - mot de passe : stack
    - serveur : localhost
    - port : 2421 (le port 2421 est redirigé vers le port 22 de la macine controller)

Exemple de connexion en ligne de commande
``` bash
ssh -p 2261 stack@localhost
```

Exemple de copie de fichier avec scp
``` bash
scp -P 2261 demo.pem stack@localhost:/home/stack
```

- Accés à Horizon : [http://localhost:8421](http://localhost:8261)

---
## Gestion de l'environnement

Pour suspendre le LAB 
```bash
vagrant suspend
```
Pour reprendre le LAB 
```bash
vagrant resume
```

Une fois le LAB terminé, pour supprimer l'environnement
```bash
vagrant  destroy -f
```








