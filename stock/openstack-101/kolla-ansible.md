
---
## Installation

Mise à jour du système 
``` bash
sudo apt update \
  && sudo apt upgrade -y \
  && [ -f /var/run/reboot-required ] && sudo systemctl reboot
```

Installation des packages 
``` bash
sudo apt install -y git python3-dev libffi-dev python3-venv gcc libssl-dev git python3-pip
```


---
## Network- config
``` bash
sudo tee  /etc/netplan/50-br0.yaml > /dev/null <<EOF 
network:
  version: 2
  renderer: networkd

  bridges:
    br0:
      dhcp4: no
      dhcp6: no
      accept-ra: no
      interfaces: [ ]
      addresses:
        - 10.20.11.1/24
EOF
```

``` bash
sudo chmod  600 /etc/netplan/50-br0.yaml /etc/netplan/50-cloud-init.yaml
sudo netplan apply
ip a
```


Activer le forwarding d'IP

``` bash
sysctl -w net.ipv4.ip_forward=1
```

dans le fichier /etc/sysctl.conf
``` ini
net.ipv4.ip_forward=1
```

Vérifier
``` bash
sysctl net.ipv4.ip_forward
# net.ipv4.ip_forward = 1
```

---
## Prérequis Disque

Cinder en mode LVM utilise un volume dédié


---
## Installation
Création du virtualenv
``` bash
cd ~
python3 -m venv $HOME/kolla-ansible
```

Activation du virtualenv
``` bash
cd kolla-ansible
source bin/activate
```

Installation de pip
``` bash
pip install -U pip
```

Installation d'Ansible
``` bash
pip install -U 'ansible<6.0'
```

Configuration d'Ansible
```
tee  $HOME/ansible.cfg > /dev/null <<EOF 
[defaults]
host_key_checking=False
pipelining=True
forks=100
EOF
```

Récupération de kolla Ansible
``` bash
pip --default-timeout=1000 install git+https://github.com/openstack/kolla-ansible@15.4.0
```

Configuration de kolla Ansible


``` bash
sudo mkdir /etc/kolla
sudo chown $USER:$USER /etc/kolla
cp $HOME/kolla-ansible/share/kolla-ansible/etc_examples/kolla/* /etc/kolla/

cp $HOME/kolla-ansible/share/kolla-ansible/ansible/inventory/all-in-one .
```

``` bash
cp -p /etc/kolla/globals.yml /etc/kolla/globals.yml.DIST
```

``` bash
2: ens2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether de:00:00:4c:2a:09 brd ff:ff:ff:ff:ff:ff
    altname enp0s2
    inet 51.15.243.235/32 metric 100 scope global dynamic ens2
       valid_lft 891sec preferred_lft 891sec
    inet6 fe80::dc00:ff:fe4c:2a09/64 scope link 
       valid_lft forever preferred_lft forever
3: br0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 36:84:bf:b2:37:cf brd ff:ff:ff:ff:ff:ff
    inet 10.20.11.1/24 brd 10.20.11.255 scope global br0
       valid_lft forever preferred_lft forever
```


Edition de /etc/kolla/globals.yml :
- kolla_internal_vip_address
- network_interface
- neutron_external_interface
- kolla_base_distro
- openstack_release
- nova_compute_virt_type
- enable_neutron_provider_networks: "yes"

``` bash
diff /etc/kolla/globals.yml.DIST /etc/kolla/globals.yml
42c42
< #kolla_base_distro: "rocky"
---
> kolla_base_distro: "ubuntu"
45c45
< #openstack_release: "zed"
---
> openstack_release: "zed"
61c61
< #kolla_internal_vip_address: "10.10.10.254"
---
> kolla_internal_vip_address: "51.15.243.235"
130c130
< #network_interface: "eth0"
---
> network_interface: "ens2"
159c159
< #neutron_external_interface: "eth1"
---
> neutron_external_interface: "br0"
165c165
< #neutron_plugin_agent: "openvswitch"
---
> #neutron_plugin_agent: "ovn"
296,297c296,297
< #enable_hacluster: "no"
< #enable_haproxy: "yes"
---
> enable_hacluster: "no"
> enable_haproxy: "no"
318c318
< #enable_cinder_backup: "yes"
---
> enable_cinder_backup: "no"
333c333
< #enable_fluentd: "yes"
---
> enable_fluentd: "no"
385c385
< #enable_neutron_provider_networks: "no"
---
> enable_neutron_provider_networks: "yes"
574c574
< #nova_compute_virt_type: "kvm"
---
> nova_compute_virt_type: "qemu"
```

``` bash
grep -vE '^$|^#' /etc/kolla/globals.yml
```



---
``` bash
cd ~/kolla-ansible
source bin/activate

kolla-genpwd
cat all-in-one
```

``` bash
cd ~/kolla-ansible
source bin/activate

kolla-ansible install-deps
# Installing Ansible Galaxy dependencies
# Starting galaxy collection install process
# ...
```




Verifier le fichier hosts, l'adresse associée au nom du serveur ne doit pas êtr celle de localhost
``` bash
sudo getent ahostsv4  ${hostname}
# 127.0.0.1       localhost
# 127.0.0.1      kos-aio
```

Il faut donc éditer le fichier /etc/hosts indiquer l'IP, et mettre en commentaire la ligne contenant le nom du serveur.

``` bash
cd ~/kolla-ansible
source bin/activate

kolla-ansible -i all-in-one bootstrap-servers
```

Cette opération modifie  le fichier /etc/hosts, pour ajouter 
l'adressesse IP de 'kolla_internal_vip_address' mappée sur le nom de l'hôte. Bien vérifier que le nom de l'hôte est bien mappé **uniquement** sur l'adresse IP renseignée dans `kolla_internal_vip_address` (Rabbit Mq ne supporte pas que le nom soit résolu par 2 IP).
``` bash
sudo getent ahostsv4  ${hostname}
127.0.0.1       localhost
10.0.2.15       kos-aio
 ```


``` bash
kolla-ansible -i all-in-one prechecks
```


Donner les droits d'accès aux commandes docker pour l'utilisateur, pour les session futures, ou recharcher la session.
``` bash
sudo usermod -aG docker $USER
```

``` bash
kolla-ansible -i all-in-one deploy
```

Vérification que tous les conteneurs ont bien été déployés, et sont 
``` bash
sudo docker  ps |grep '(healthy)' | wc -l
# 26
```

Génération du fichier rc pour admin
``` bash
kolla-ansible -i all-in-one post-deploy
cat  /etc/kolla/admin-openrc.sh
```

---
## Installation des librairies client

``` bash
sudo pip install python-openstackclient -c https://releases.openstack.org/constraints/upper/zed
sudo pip install python-neutronclient -c https://releases.openstack.org/constraints/upper/zed
sudo pip install python-glanceclient -c https://releases.openstack.org/constraints/upper/zed
sudo pip install python-heatclient -c https://releases.openstack.org/constraints/upper/zed
```

Mise en place de l'autocompletion pour le client
``` bash
openstack complete | sudo tee /etc/bash_completion.d/osc > /dev/null
source /etc/bash_completion.d/osc
```

``` bash
source /etc/kolla/admin-openrc.sh
openstack service list



---
## annexes


Test du bon fonctionnement de docker.
``` bash
sudo docker run hello-world
sudo docker run quay.io/podman/hello
```

Ménage
``` bash
sudo docker ps -a
sudo docker container prune -f

sudo docker images
sudo docker image prune -af
```
