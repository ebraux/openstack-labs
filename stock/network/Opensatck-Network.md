


* <https://mkdev.me/en/posts/how-networks-work-what-is-a-switch-router-dns-dhcp-nat-vpn-and-a-dozen-of-other-useful-things>
* <https://mkdev.me/en/posts/how-networks-work-part-two-teaming-for-fault-tolerance-bandwidth-management-with-traffic-control-tap-interfaces-and-linux-bridge>


Bridge :
 permet de connecter des segments résau
 les paquets sont transmis enfonction de l'adresse Ethernet, au niveau de la "Layer2"
 comme le routage se fait au niveau de la layer2, c'est indépendant du protocol.

 L'objectif d'un bridge est de connecter de façon transparente des reseaux Ethernet, pour qu'il puissent être vus comme un seul réseau.
 Les paquets qui circulent d'un segment réseau à un autre le font de façon transparente.
 Avec un outil comme "traceroute" les bridge n'apparaissnet pas.
 
 C'est donc un réseau à plat.
 On n'ajoute un device à plusieurs bridge 
 On n'ajoute pas un device de bridge à un bridge (pour crér une arborescence par exmple)

rem : pour la connection de segment réseau, au niveau de l'adresse IP (layer 3) on utilise des routeurs

Une fois activé, un bridge :
* écoute (listening) : regarde les packets qui arrivent sur différentes interfaces
* apprend (learning) : analyse et mémorise la configuration du réseau : quelles adresses MAC sont derrière quels ports, ...
* forwarding : transmission des paquets.

Mise en cache des adresses mac "aging time" : 
* mémoiriser les adresses , pour les retouver rapidement
* permettre la reconfiguration du réseau en cas de changement
Quand le bridge reçoit un paquet, il mémorise l'adresse MAC.
Il concerve cette information pendat un temps limité
setting "ageing time" to zero makes all entries permanent.

Eviter les boucle, optimiser le traffic
 Pour éviter les boucle (Spanning Tree Protocol) : fonctionnalité STP
 POur optimiser les chemin :  Path priority and cost

 Un bridge peut relier des segment réseau de tout type : PPP, VPN, VLAN ...
Restriction sur les devices : 
* All devices share the same maximum packet size (MTU). The bridge doesn't fragment packets.
* Devices must look like Ethernet. i.e have 6 byte source and destination address.
* Support promiscuous operation. The bridge needs to be able to receive all network traffic, not just traffic destined for its own address.
* Allow source address spoofing. The bridge must be able to send data over network as if it came from another host.

Linux Bridge :
 implémentation logicielle d'un "bridge" dans Linux.
 développé pour travailler avec le noyau 2.2
 redéveloppé et intégré directement au kernel dans les versions 2.4 et 2.6


Exensions :

Bridge et Firewalling
also filter and shape traffic. The combination of bridging and firewalling is done with the companion project ebtables. (http://ebtables.netfilter.org/)



# UTilisation
* soit en ligne de commande
** brctl
** 
* soit dans la configuration réseau de la machine (permet à la configurationd'être appliquée à chaque redémarrage)

Lorsque vous définissez un pont, les interfaces utilisées comme ports deviennent inutilisables au niveau réseau. 
En revanche, Linux définit une interface virtuelle (ici, br0), qui peut être configurée et utilisée sur l'ordinateur comme si elle était connectée au pont réseau.

Cette interface reçoit l'adresse MAC la plus faible parmi les interfaces utilisées comme ports, qui sont lues comme des nombres hexadécimaux. Ainsi, si votre pont relie les interfaces eth0 00:0c:6e:b1:c7:95, eth1 00:08:54:3d:cf:bb et eth3 00:05:5d:a1:e1:ad, l'interface virtuelle br0 prendra l'adresse MAC 00:05:5d:a1:e1:ad (son troisième chiffre, 5 étant inférieur à celui des deux autres interfaces, c et 8).

Différents états du Bridge







# sources :
* <https://wiki.linuxfoundation.org/networking/bridge>




 ta 

ML2 plug-in with Linux bridge.
* <https://formation-debian.viarezo.fr/bridge.html>
* <https://wiki.linuxfoundation.org/networking/bridge>
* <http://ebtables.netfilter.org/br_fw_ia/br_fw_ia.html>

# Prérequis


Les Réseau de projet (Project Networks)

En mode "Self service", chaque utilisateur peut créer des réseau virtuels au sein d'un projet.
Les réseau virtuels sont lié à un projet. On les appelle donc des réseau de projet "Network Project".
Ce sont des réseau privés :
* Ils disposent de leur propre  range IP (RFC1918). 
* Ils ne sont pas accessibles depuis les réseau externes, comme les resaue d'entreprise, ou Internet.
Les adresses IP de ces réseau sont appelée adresse IP fixes ( fixed IP addresses)

Les réseau externes (External networks)
Les réseau externes sont connectés au réseaux physiques.
C'est par eux qu'il est possible d'avoir accés au réseau d'entreprise ou à Internet.
Il utilisent des range IP publiques, ou au moins routées sur les réseaux physiques.
Seuls les administrateurs peuvent les créer.

Les routeurs (router) et IP Flottantes (floating IP)
Les routeurs permettent d'interconnecter les réseaux.
Il sont principalement utilisés pour connecter les "Project Networks" aux "External networks".
Ils sont également utilisés pour connecter des "Project Networks", dans le cas d'une architecture Ntiers.
Les  "Project Networks" doivent être dans le même projet.
Un routeur entre un "Project Networks" et un "External networks" permet :
 - aux instances d'avoir un accés vers les réseaux externes (SNAT).
 - d'accéder aux instances depuis le réseau physique, en leur associant une floating IP (DNAT).
Une "floating IP" est une adresse IP d'un "External networks", qui est associée à une instance.
Le traffic réseau vers la  "floating IP" est redirigé  vers la "fixed IP" de l'instance.
Un routeur dispose d'une adresse IP dans chaque réseau qui y sont connectés.


North-south  / East-west
North-south network traffic travels between an instance and external network, typically the Internet. East-west network traffic travels between instances.


Services

DHCP : gére les adresse IP pour les instances dans un "Project Networks". Le serveur DHCP dispose d'un pool d'adresses à affecter aux instances. Quand une instance est lancée, elle demande une adresse IP, et le serveur DHCP lui en affecte une du pool. 

metadata : provides an API for instances on project networks to obtain metadata such as SSH keys.


# Techniquement :

Accès entrant sortant vers les instances :
 - SNAT: accès sortant
 - DNAT: accès entrant


* les "Project Networks"  peuvent s'appuyer sur les technologie VLAN, GRE ou Vxlan (transport methods)
* les "External Networks" peuvnet utiliser les technologies FLAT ou VLAN (transport methods)

The Linux bridge agent does not support GRE project networks

Un "FLAT Network" utilise un réseau physique, un VLAN Natif, ou un VLAN "untagged". On ne peut donc utiliser qu'un seul "FLAT Network" par "external bridge".


L'accés au réseau externes par des instances se fait en utilisant SNAT.



# Recommandation, bonnes pratiques
En production, on utilise plutôt VLAN transport for external networks, pour pouvoir utiliser plusieur exeternal network dans un "external bridge"

# sources 
* <https://docs.openstack.org/kilo/networking-guide/scenario_legacy_lb.html>