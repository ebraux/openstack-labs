

OpenStack Neutron est responsable de la gestion des réseaux, sous-réseaux, routeurs, ports, etc., dans un déploiement OpenStack.



Driver Linux Bridge

Principe :

Linux Bridge est un composant du noyau Linux.

- Création de bridge sur les différents noeuds
    - connexion des instances via une paire de veth (une pour l'instance, un pour le bridge)
    - connexion de l'agent DHCP  et metadata
    - gestion du routage

Le lien entre les bridge sur les différents noeuds et géré par un agent L2.

La sécurité est gérée au niveau port, via des iptables sur les interfaces des VM, au niveau des bridges.

Pas de fonctionnalité réseau avancée.



https://docs.openstack.org/neutron/pike/admin/


https://docs.openstack.org/neutron/pike/admin/deploy-ovs.html
https://docs.openstack.org/neutron/pike/admin/deploy-lb.html