# Modular layer 2 (ML2) networking

ML2 is the OpenStack Networking core plug-in.

The ML2 modular design enables the concurrent operation of mixed network technologies.



---
## L2 population driver

The L2 Population driver enables broadcast, multicast, and unicast traffic to scale out on large overlay networks. By default, Open vSwitch GRE and VXLAN replicate broadcasts to every agent, including those that do not host the destination network. This design requires the acceptance of significant network and processing overhead. The alternative design introduced by the L2 Population driver implements a partial mesh for ARP resolution and MAC learning traffic; it also creates tunnels for a particular network only between the nodes that host the network. This traffic is sent only to the necessary agent by encapsulating it as a targeted unicast.


--- 
## ML2 network types
Multiple network segment types can be operated concurrently. In addition, these network segments can interconnect using ML2 support for multi-segmented networks. Ports are automatically bound to the segment with connectivity; it is not necessary to bind ports to a specific segment. Depending on the mechanism driver, ML2 supports the following network segment types:

flat
GRE
local
VLAN
VXLAN
Geneve


``` bash
To install ARP reply flows, configure the arp_responder flag:

[agent]
l2_population = True
arp_responder = True
```