



nftables replaces iptables, ip6tables, arptables and ebtables, in order to provide a single API for all Netfilter operations.

``` bash
Just to unravel the possible terminology confusion, these are the three Netfilter available framework alternatives:

The legacy binaries (iptables, ip6tables, arptables and ebtables) that use the legacy API.

The new nftables binaries that use the legacy API, to help in the transition to this new framework. Those binaries replicate the same commands as the legacy one but using the new framework. The binaries have the same name ended in -nft.

The new nftables framework using the new API. All Netfilter operations are executed using this new API and one single binary, nft.

```