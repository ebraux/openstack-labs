
---
## Configuration du client ssh

Les Keypair SSH générées par Openstack utilisent RSA-SHA1, qui est déprécié sur les systèmes récents. Il faut le ré-activer

```bash
echo "PubkeyAcceptedKeyTypes +ssh-rsa" | sudo tee /etc/ssh/ssh_config.d/51-fix-depreciated-rsa.conf
sudo update-crypto-policies --set LEGACY
```

Cette configuration n'est pas recommandée en production.

- [https://serverfault.com/questions/1095898/how-can-i-use-a-legacy-ssh-rsa-key-on-centos-9-stream](https://serverfault.com/questions/1095898/how-can-i-use-a-legacy-ssh-rsa-key-on-centos-9-stream)
