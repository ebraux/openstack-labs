# Day2 : Opérer Devstack 

-  [Réactiver le réseau public après reboot](./network-reboot.md)
-  [Utiliser systemd pour gérer les services](./systemd.md)

Docs :

- [https://docs.openstack.org/devstack/latest/configuration.html](https://docs.openstack.org/devstack/latest/configuration.html)

