# Réactiver le réseau public après reboot


Well, after 5 days of research, lecture and harassment from stackoverflow reviewers, I found those lines to solve my problem:
``` bash
echo 1 > /proc/sys/net/ipv4/conf/ens160/proxy_arp
iptables -t nat -A POSTROUTING -o ens160 -j MASQUERADE
```

https://stackoverflow.com/questions/59109557/how-to-expose-the-devstack-floating-ip-to-the-external-world
https://stackoverflow.com/questions/23401425/openstack-vm-is-not-accessible-on-lan
https://barakme.wordpress.com/2013/12/23/openstack-in-a-box-setting-up-devstack-havana-on-your/



---
## iptables

Afficher les iptables
``` bash
sudo  iptables -L -v -n
```

``` bash
sudo iptables -t nat -A PREROUTING -p tcp --dport 60080 -j DNAT --to 192.168.1.236:80
```
``` bash
iptables -t nat -I POSTROUTING -o eth0 -s 172.24.0.0/24 -j MASQUERADE
iptables -I FORWARD -s 172.24.0.0/24 -j ACCEPT
```

pour donner l'accès :
``` bash
FLOATING_RANGE="172.18.161.0/24"
sudo iptables -t nat -A POSTROUTING -o $d -s $FLOATING_RANGE -j MASQUERADE
```

https://rahulait.wordpress.com/2016/06/27/manually-routing-traffic-from-br-ex-to-internet-devstack/

---
## reconfiguration complète

https://gist.github.com/charlesflynn/5576114

``` bash
sudo ip addr flush dev br-ex
sudo sysctl -w net.ipv4.ip_forward=1
sudo ip addr add 172.24.4.225/28 dev br-ex
sudo ip link set br-ex up
sudo route add -net 10.0.0.0/24 gw 172.24.4.226
sudo service rabbitmq-server start
sudo service postgresql start
sudo service apache2 start
export SERVICE_TOKEN=password
export OS_TENANT_NAME=demo
export OS_USERNAME=admin
export OS_PASSWORD=$YOUR_ADMIN_PASSWORD
export OS_AUTH_URL=http://localhost:5000/v2.0/
export SERVICE_ENDPOINT=http://localhost:35357/v2.0
./rejoin-stack.sh
```