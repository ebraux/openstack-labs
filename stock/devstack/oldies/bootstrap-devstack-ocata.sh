#!/usr/bin/env bash


    # set environement for installation and non interactive update
    export DEBIAN_FRONTEND=noninteractive
    export TERM="xterm"

    # set the system up to date
    apt-get update && apt -y dist-upgrade && apt-get clean

    # Gestion de Repositories complementaires
    apt install -y  software-properties-common

    # Outils standards
    apt install -y sudo vim vim-nox lynx zip binutils wget openssl ssl-cert ssh

    # Prerequis pour devstack
    apt install -y bridge-utils git python-pip

    # declaration du nom DNS controller pour la machine
    echo `hostname -I`'       controller'  >> /etc/hosts

    # configuration de python-pip
    #python -m pip install --user --upgrade pip==9.0.1
    #pip install --upgrade pip
    pip install  -U os-testr

    # creation de l'utilisateur stack
    groupadd stack
    useradd -g stack       -s /bin/bash -d /home/stack  -m  stack
    cd /etc/sudoers.d
    echo "stack ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_stack_sh
    chmod 440 /etc/sudoers.d/50_stack_sh

    # Installation de devstack
    cd /home/stack
    git clone https://git.openstack.org/openstack-dev/devstack
    cd devstack
    git checkout stable/ocata
    echo '[[local|localrc]]' > local.conf
    echo 'HOST_IP=172.28.128.x' >> local.conf
    echo 'ADMIN_PASSWORD=stack' >> local.conf
    echo 'DATABASE_PASSWORD=stack' >> local.conf
    echo 'RABBIT_PASSWORD=stack' >> local.conf
    echo 'SERVICE_PASSWORD=stack' >> local.conf
    echo 'GIT_BASE=https://git.openstack.org'  >> local.conf
    echo 'USE_SCREEN=FALSE'  >> local.conf
    echo 'PIP_UPGRADE=True'  >> local.conf
    echo 'RECLONE=yes'  >> local.conf
    echo '#'  >> local.conf
    echo '#DOWNLOAD_DEFAULT_IMAGES=False'  >> local.conf
    echo '#IMAGE_URLS="http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img"'  >> local.conf
    echo '#'  >> local.conf
    echo '#IP_VERSION=4'  >> local.conf
    echo '#Enable heat services'  >> local.conf
    echo 'enable_plugin heat https://git.openstack.org/openstack/heat stable/ocata'  >> local.conf
    chown -R stack.stack /home/stack/devstack
#    su -l stack -c "cd devstack; ./stack.sh"
