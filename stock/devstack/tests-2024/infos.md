


`br-int` is a bridge that the Open vSwitch mechanism driver creates,
which is used as the "integration bridge" where ports are created, and
plugged into the virtual switching fabric. `br-ex` is an OVS bridge
that is used to connect physical ports (like `eth0`), so that floating
IP traffic for project networks can be received from the physical
network infrastructure (and the internet), and routed to self service
project network ports.  `br-tun` is a tunnel bridge that is used to
connect OpenStack nodes (like `devstack-2`) together. This bridge is
used so that project network traffic, using the VXLAN tunneling
protocol, flows between each compute node where project instances run.


The second physical interface, eth1 is added to a bridge (in this case
named br-ex), which is used to forward network traffic from guest VMs.

::

        stack@compute:~$ sudo ovs-vsctl add-br br-ex
        stack@compute:~$ sudo ovs-vsctl add-port br-ex eth1
        stack@compute:~$ sudo ovs-vsctl show
        9a25c837-32ab-45f6-b9f2-1dd888abcf0f
            Bridge br-ex
                Port br-ex
                    Interface br-ex
                        type: internal
                Port phy-br-ex
                    Interface phy-br-ex
                        type: patch
                        options: {peer=int-br-ex}
                Port "eth1"
                    Interface "eth1"


dans 
``` bash
        export OVS_PHYSICAL_BRIDGE=br-ex
        export PUBLIC_INTERFACE=''
        sudo ovs-vsctl add-br $OVS_PHYSICAL_BRIDGE
        # sudo ovs-vsctl --no-wait -- --may-exist add-port $OVS_PHYSICAL_BRIDGE $PUBLIC_INTERFACE

        sudo ip link set $OVS_PHYSICAL_BRIDGE up

        sudo ip link set br-int up
        sudo ip link set $PUBLIC_INTERFACE up
```
https://docs.openvswitch.org/en/latest/faq/issues/
$ ip addr add 192.168.128.5/24 dev br0
$ ip link set br0 up

ip addr add 172.24.4.1/24 dev br-ex
ip link set br-ex up

Pour attacher une interface physique au bridge
``` bash
        ## Open vSwitch provider networking options
        PHYSICAL_NETWORK=default
        OVS_PHYSICAL_BRIDGE=br-ex
        PUBLIC_INTERFACE=eth1
        Q_USE_PROVIDER_NETWORKING=True
```


MTU
``` bash
Neutron by default uses a MTU of 1500 bytes, which is
the standard MTU for Ethernet.

A different MTU can be specified by adding the following to
the Neutron section of `local.conf`. For example,
if you have network equipment that supports jumbo frames, you could
set the MTU to 9000 bytes by adding the following

::

    [[post-config|/$Q_PLUGIN_CONF_FILE]]
    global_physnet_mtu = 9000
```

pour donner l'accès :
``` bash
FLOATING_RANGE="172.18.161.0/24"
sudo iptables -t nat -A POSTROUTING -o $d -s $FLOATING_RANGE -j MASQUERADE
```
