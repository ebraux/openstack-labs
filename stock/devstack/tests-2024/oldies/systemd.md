
# config Apache
cp -p /etc/apache2/ports.conf /etc/apache2/ports.conf.DIST
sudo sed -i 's/Listen 80/Listen 8000/' /etc/apache2/ports.conf

cp -p /etc/apache2/sites-available/horizon.conf /etc/apache2/sites-available/horizon.conf.DIST
sudo sed -i 's/<VirtualHost \*:80>/<VirtualHost \*:8000>/'  /etc/apache2/sites-available/horizon.conf
systemctl restart apache2.service

# prerequis
systemctl enable mysql.service
systemctl restart mysql.service

systemctl enable rabbitmq-server.service
systemctl restart rabbitmq-server.service

systemctl enable memcached.service 
systemctl restart memcached.service 

systemctl enable apache2.service
systemctl restart apache2.service

systemctl enable devstack@etcd.service
systemctl restart devstack@etcd.service

# Keystone
systemctl enable devstack@keystone.service
systemctl restart devstack@keystone.service

# placement 
systemctl enable devstack@placement-api.service
systemctl restart devstack@placement-api.service

# glance
systemctl enable devstack@g-api.service
systemctl restart "devstack@g-*"

# nova
systemctl enable \
  devstack@n-cpu.service \
  devstack@n-api.service \
  devstack@n-sch.service \
  devstack@n-super-cond.service \
  devstack@n-cond-cell1.service \
  devstack@n-api-meta.service \
  devstack@n-novnc-cell1.service
systemctl restart "devstack@n-*"

# neutron
systemctl enable \
  devstack@g-api.service \
  devstack@q-svc.service  \
  devstack@q-l3.service   \
  devstack@q-meta.service \
  devstack@q-dhcp.service \
  devstack@q-agt.service 
systemctl restart "devstack@q-*"

# cinder
systemctl enable \
  devstack@c-api.service \
  devstack@c-sch.service \
  devstack@c-vol.service
systemctl restart "devstack@c-*"


---


sudo systemctl status "devstack@*"

systemctl enable mysql.service
systemctl restart mysql.service
systemctl status mysql.service

systemctl enable rabbitmq-server.service
systemctl restart rabbitmq-server.service
systemctl status rabbitmq-server.service

systemctl enable memcached.service 
systemctl restart memcached.service 
systemctl status memcached.service 

systemctl enable apache2.service
systemctl restart apache2.service
systemctl status apache2.service

#devstack@etcd.service
systemctl enable devstack@etcd.service
systemctl restart devstack@etcd.service

# Keystone
#devstack@keystone.service
systemctl enable devstack@keystone.service
systemctl restart devstack@keystone.service

# placement 
#devstack@placement-api.service 
systemctl enable devstack@placement-api.service
systemctl restart devstack@placement-api.service

# glance
#devstack@g-api.service
systemctl enable devstack@g-api.service
systemctl restart "devstack@g-*"
systemctl status "devstack@g-*"

#nova
#devstack@n-api.service
#devstack@n-sch.service
#devstack@n-cpu.service
#devstack@n-super-cond.service
#devstack@n-cond-cell1.service
#devstack@n-novnc-cell1.service
#devstack@n-api-meta.service
systemctl enable \
  devstack@n-cpu.service \
  devstack@n-api.service \
  devstack@n-sch.service \
  devstack@n-super-cond.service \
  devstack@n-cond-cell1.service \
  devstack@n-api-meta.service \
  devstack@n-novnc-cell1.service
systemctl restart "devstack@n-*"
systemctl status "devstack@n-*"

# neutron
#devstack@g-api.service
#devstack@q-dhcp.service
#devstack@q-agt.service
#devstack@q-meta.service
#devstack@q-l3.service
#devstack@n-api-meta.service
#devstack@q-svc.service
systemctl enable \
  devstack@g-api.service \
  devstack@q-svc.service  \
  devstack@q-l3.service   \
  devstack@q-meta.service \
  devstack@q-dhcp.service \
  devstack@q-agt.service 
systemctl restart "devstack@q-*"

# cinder
#devstack@c-api.service
#devstack@c-sch.service
#devstack@c-vol.service
systemctl enable \
  devstack@c-api.service \
  devstack@c-sch.service \
  devstack@c-vol.service
systemctl restart "devstack@c-*"
