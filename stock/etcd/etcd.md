# Configuration de Etcd

---
## Référence:

- [https://docs.openstack.org/install-guide/environment-etcd-ubuntu.html](https://docs.openstack.org/install-guide/environment-etcd-ubuntu.html)

**Installation**

``` bash
sudo apt install -y etcd
```

**Configuration**

Fichier de configuration [etcd](conf/etcd_default_etcd.conf) à mettre en place dans le fichier : /etc/etcd/default
``` bash
sudo cp -p /etc/default/etcd /etc/default/etcd.DIST
sudo cp ~/openstack_config/etcd_default_etcd.conf /etc/default/etcd
sudo chown root.root /etc/default/etcd
cat /etc/default/etcd
```

**Relance du service**

``` bash
sudo systemctl enable etcd
sudo systemctl restart etcd
```

**Vérification**

Etat du service etcd
``` bash
systemctl status etcd
# Active: active (running) 
```