# Démo déploiement d'une infrastructure


Création d'un réseau privé, d'un sous-réseau et de groupes de sécurité et d'une instance dans un projet.


---
## Initialisation de l'environnement

Vérification de l'environnement utilisateur
``` bash
env | grep OS_
# OS_AUTH_URL=http://controller:5000/v3
# OS_PROJECT_DOMAIN_NAME=Default
# OS_USERNAME=demo
# OS_USER_DOMAIN_NAME=Default
# OS_PROJECT_NAME=demo
# OS_PASSWORD=stack
# OS_IDENTITY_API_VERSION=3
```


Configuration de l'environnement
``` bash
PROJECT_NAME=$OS_PROJECT_NAME
echo "PROJECT_NAME : $PROJECT_NAME"

EXTERNAL_NETWORK_NAME=$(openstack network list --external --share -f value -c Name
)
echo "EXTERNAL_NETWORK_NAME : $EXTERNAL_NETWORK_NAME"
# ->  defini dans la config de neutron

# configuration
NETWORK_NAME=${PROJECT_NAME}selfservice
SUBNET_NAME=${PROJECT_NAME}selfservice
ROUTER_NAME=${PROJECT_NAME}Routeur
SECURITY_GROUP_NAME=${PROJECT_NAME}BasicAdmin
TEST_INSTANCE_NAME=${PROJECT_NAME}Test
VOLUME_NAME=${PROJECT_NAME}Volume
echo "NETWORK_NAME : $NETWORK_NAME"
echo "SUBNET_NAME : $SUBNET_NAME"
echo "ROUTER_NAME : $ROUTER_NAME"
echo "SECURITY_GROUP_NAME : $SECURITY_GROUP_NAME"
echo "TEST_INSTANCE_NAME: ${TEST_INSTANCE_NAME}"
echo "VOLUME_NAME: ${VOLUME_NAME}"

```

---
## Configuration d'une infrastructure réseau 
```bash
# creation d'un reseau
echo "NETWORK_NAME : $NETWORK_NAME"
openstack network create ${NETWORK_NAME}

# Affichage du resultat
openstack network show ${NETWORK_NAME} -f json  

# creation d'un sous réseau
  echo "SUBNET_NAME : $SUBNET_NAME"
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 8.8.4.4 --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# [optionnel] configuration des DNS Open NIC
openstack subnet set --dns-nameserver 192.71.245.208 ${SUBNET_NAME}
openstack subnet set --dns-nameserver 94.247.43.254 ${SUBNET_NAME}
openstack subnet unset --dns-nameserver 8.8.4.4  ${SUBNET_NAME}

# Affichage du resultat
openstack subnet show ${SUBNET_NAME} -f json  

# creation d'un routeur
echo "ROUTER_NAME : $ROUTER_NAME"
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}


# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# Affichage du resultat
openstack router show ${ROUTER_NAME} -f json  

# Test de la bonne connexion du router
export ROUTER_GTW_IP=$(openstack router show ${ROUTER_NAME} -f json  | jq --raw-output -c '.external_gateway_info.external_fixed_ips[].ip_address')
echo $ROUTER_GTW_IP
ping -c 3 $ROUTER_GTW_IP
```

---
## Creation des groupes de sécurité
``` bash
echo "SECURITY_GROUP_NAME : $SECURITY_GROUP_NAME"
openstack security group create \
    --description 'Admin Basiques rules' \
    ${SECURITY_GROUP_NAME}

openstack security group rule create \
    --proto icmp --dst-port 0 \
    ${SECURITY_GROUP_NAME}

openstack security group rule create \
    --proto tcp --dst-port 22 \
    ${SECURITY_GROUP_NAME}
```

---
## Creation d'une instance

```bash
# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS=$(openstack flavor list | grep '| cirros ' | awk '{ print $4}')

# recherche de l'id de l'image cirros
IMAGE_CIRROS=$(openstack image list | grep ' cirros ' | awk '{ print $4 }')


# Creation d'une instance de test
echo "FLAVOR_CIRROS: ${FLAVOR_CIRROS}"
echo "IMAGE_CIRROS: ${IMAGE_CIRROS}"
echo "NETWORK_NAME : $NETWORK_NAME"
echo "SECURITY_GROUP_NAME : $SECURITY_GROUP_NAME"
echo "TEST_INSTANCE_NAME: ${TEST_INSTANCE_NAME}"

openstack server create \
  --flavor ${FLAVOR_CIRROS} \
  --image ${IMAGE_CIRROS} \
  --nic net-id=${NETWORK_NAME} \
  --security-group ${SECURITY_GROUP_NAME} \
  --wait \
  ${TEST_INSTANCE_NAME}

openstack server list
openstack server show ${TEST_INSTANCE_NAME}

```

---
## A affectation d'une IP, et ouverture des accès

``` bash
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
openstack floating ip list
FLOATING_IP=$(openstack floating ip list -f json  | jq --raw-output -c '.[0]."Floating IP Address"')
echo "FLOATING_IP: ${FLOATING_IP}"

openstack server add floating ip ${TEST_INSTANCE_NAME} ${FLOATING_IP}

# Test connexion
ping -c 4 ${FLOATING_IP}

ssh cirros@${FLOATING_IP}
# password = gocubsgo
```

---
## Ajout d'un volume

Observation des disque sur la VM : Uniquement le disque système "/dev/vda"
``` bash
ssh cirros@${FLOATING_IP} sudo fdisk -l
# cirros@xxx.xxx.xxx.xxx's password: 
# Disk /dev/vda: 1 GiB, 1073741824 bytes, 2097152 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
# Disklabel type: gpt
# Disk identifier: 83E1BD1F-EF8A-4997-8E0E-65E38FB771F4

# Device     Start     End Sectors  Size Type
# /dev/vda1  18432 2097118 2078687 1015M Linux filesystem
# /dev/vda15  2048   18431   16384    8M EFI System

# Partition table entries are not in disk order.
```


``` bash
echo "VOLUME_NAME: ${VOLUME_NAME}"
openstack volume create --size 1 --description "Demo Volume" ${VOLUME_NAME}

openstack volume  list
```

``` bash
openstack server add volume ${TEST_INSTANCE_NAME} ${VOLUME_NAME}
# +-----------------------+--------------------------------------+
# | Field                 | Value                                |
# +-----------------------+--------------------------------------+
# | ID                    | fd0b2c44-667c-493c-be8d-907d49e4403b |
# | Server ID             | 6e31072c-0101-480a-91ed-a021520ec3db |
# | Volume ID             | fd0b2c44-667c-493c-be8d-907d49e4403b |
# | Device                | /dev/vdb                             |
# | Tag                   | None                                 |
# | Delete On Termination | False                                |
# +-----------------------+--------------------------------------+
```

``` bash
openstack volume  list
# +--------+------------+-----------+------+----------------------------------+
# +        | Name       | Status    | Size | Attached to                      |
# +--------+------------+-----------+------+----------------------------------+
# | 6da... | demoVolume | in-use    |    1 | Attached to demoTest on /dev/vdb |
# +--------+------------+-----------+------+----------------------------------+ 
```

``` bash
openstack server show ${TEST_INSTANCE_NAME}
# +---------------------+-----------------+
# | Field               | Value           |
# +---------------------+-----------------+
# ...
# | volumes_attached    | id='6da...      |
# +---------------------+-----------------+
```


Observation des disque sur la VM : le disque "/dev/vdb" est désormais disponible
``` bash
ssh cirros@${FLOATING_IP} sudo fdisk -l
# cirros@xxx.xxx.xxx.xxx's password: 
# ...
# Disk /dev/vdb: 1 GiB, 1073741824 bytes, 2097152 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
```


---
## Suppression des ressources

``` bash
openstack server remove volume ${TEST_INSTANCE_NAME} ${VOLUME_NAME}

openstack server remove floating ip ${TEST_INSTANCE_NAME} ${FLOATING_IP}
openstack floating ip delete ${FLOATING_IP}

openstack server delete ${TEST_INSTANCE_NAME}

openstack security group delete ${SECURITY_GROUP_NAME}

openstack router remove  subnet ${ROUTER_NAME} ${SUBNET_NAME}
openstack router unset ${ROUTER_NAME} --external-gateway 

openstack router delete ${ROUTER_NAME}
openstack subnet delete ${SUBNET_NAME}
openstack network delete ${NETWORK_NAME}
```


Bug pour la suppression du Volume :

- Le détachement échoue n'est pas complètement pris en compte.
- C'est un bug de communication entre les API de nova et cinder

Pour pouvoir supprimer le volume, il faut d'abord modifier les information d'attachement, en tant qu'admininstateur.
``` bash
source ~/admin-openrc
openstack volume set --detached ${VOLUME_NAME}
``` 

Et ensuite en tant qu'utilisteur standard, supprimer le volume
``` bash
openstack volume delete ${VOLUME_NAME}
```

ref :
- [https://answers.launchpad.net/cinder/+question/708280](https://answers.launchpad.net/cinder/+question/708280)
- [https://serverfault.com/questions/1131856/error-detatching-cinder-volumes-in-openstack](https://serverfault.com/questions/1131856/error-detatching-cinder-volumes-in-openstack)
- [https://www.reddit.com/r/openstack/comments/16bm9as/detaching_volumes_fails_with_openstack_api_and/](https://www.reddit.com/r/openstack/comments/16bm9as/detaching_volumes_fails_with_openstack_api_and/)
