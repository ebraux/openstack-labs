title: Configure OpenStack with KVM-based Nested Virtualization

When using virtualization technologies like KVM, one can take advantage of Nested VMX (i.e. the ability to run KVM on KVM) so that the VMs in cloud (Nova guests) can run relatively faster than with plain QEMU emulation.

Check if the nested KVM Kernel parameter is enabled.

```bash
cat /sys/module/kvm_intel/parameters/nested
N
```

Add the below content in /etc/modprobe.d/kvm.conf

```bash
options kvm_intel nested=Y
```

Reboot the node and check the nested KVM kernel parameter again.

Check if the nested KVM Kernel parameter is enabled.

```bash
cat /sys/module/kvm_intel/parameters/nested
Y
```

