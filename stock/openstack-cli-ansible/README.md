title: "the Ansible way"


# Introduction

Si vous utilisez un outil d'orchestration pour déployer et configurer vos serveurs, vous avez déjà pu constater les bénéfices en terme de temps passé, de reproductibilité, de sécurité, ... 

Un des outils les plus utilisés avec openstack est Ansible. Ansible permet de décrire dans des fichiers texte (playbook) des taches à réaliser sur des serveurs pour les configurer.

Ansible propose un grand nombre de modules (installtion de paquets, ...), dont des modules de connection au différents outils de cloud, et naturellement Openstack [https://docs.ansible.com/ansible/latest/modules/list_of_cloud_modules.html#openstack](https://docs.ansible.com/ansible/latest/modules/)


# Installation d'ansible et premiers pas..

Méthode préférée : pip + vitulaenv

Par exemple : [https://clouddocs.f5.com/products/orchestration/ansible/devel/usage/virtualenv.html](https://clouddocs.f5.com/products/orchestration/ansible/devel/usage/virtualenv.html)



# Gestion de l'authentification à Openstack

En l'intégrant dans chaque tache :
```yaml
  - name: exemple de tache
    os_networks_facts:
      auth:
         auth_url: https://openstack.imt-atlantique.fr:5000/v3
         username: xxxxxx
         password: xxxxxx
         project_name: xxxxxx
```

Ou en utilisant un fichier clouds.yaml :
```yaml
clouds:
 mycloud:
   region_name: RegionOne
   domain_name: 'Default'
   auth:
     username: 'xxxxx'
     password: 'xxxxx'
     project_name: 'xxxxx'
     auth_url: 'https://openstack.imt-atlantique.fr:5000/v3'
   identity_api_version: 3
   verify: false
```

```yaml
  - name: exemple de tache
    os_networks_facts:
      cloud: mycloud
      .... 
```  

# Quelques exemples

On peut alors commencer à interragir avec Openstack, pour lister des informations:
```yaml
  - name: List of Networks
    os_networks_facts:
      cloud: mycloud
  - debug: var=openstack_networks

  - name: List of Subnets
    os_subnets_facts:
      cloud: mycloud
  - debug: var=openstack_subnets

  - name: List of Routers
    os_router_facts:
      cloud: mycloud
  - debug: var=openstack_routers
```

Ou encore créer des ressources :
```yaml
- name: Create a new instance
  os_server:
      state: present
      cloud: mycloud
      name: vm1
      image: imta-ubuntu-basic
      key_name: mykey
      flavor: m1.small
      nics:
        - net-name: private-network      
```

 ....


# Quelques pointeurs 

* [https://superuser.openstack.org/articles/using-ansible-2-0-to-launch-a-server-on-openstack/](https://superuser.openstack.org/articles/using-ansible-2-0-to-launch-a-server-on-openstack/)
* [https://www.redhat.com/en/blog/full-stack-automation-ansible-and-openstack](https://www.redhat.com/en/blog/full-stack-automation-ansible-and-openstack)
* [https://riptutorial.com/fr/ansible/example/27176/rassembler-des-informations-sur-notre-nouvelle-instance](https://riptutorial.com/fr/ansible/example/27176/rassembler-des-informations-sur-notre-nouvelle-instance)

