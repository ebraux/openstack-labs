
openstack hypervisor list
+---------------------------------------+---------------------+-----------------+----------------+-------+
| ID                                    | Hypervisor Hostname | Hypervisor Type | Host IP        | State |
+---------------------------------------+---------------------+-----------------+----------------+-------+
| 35a1114c-6dc8-45e5-8e5c-0d4ab100b11e  | lab-aio-ebraux      | QEMU            | 51.159.182.163 | up    |
+---------------------------------------+---------------------+-----------------+----------------+-------+


openstack resource provider inventory list 35a1114c-6dc8-45e5-8e5c-0d4ab100b11e
+----------------+------------------+----------+----------+----------+-----------+-------+-------+
| resource_class | allocation_ratio | min_unit | max_unit | reserved | step_size | total |  used |
+----------------+------------------+----------+----------+----------+-----------+-------+-------+
| VCPU           |              4.0 |        1 |        4 |        0 |         1 |     4 |    16 |
| MEMORY_MB      |              1.0 |        1 |    11951 |      512 |         1 | 11951 | 11200 |
| DISK_GB        |              1.0 |        1 |      108 |        0 |         1 |   108 |    78 |
+----------------+------------------+----------+----------+----------+-----------+-------+-------+


https://docs.rackspacecloud.com/openstack-cpu-allocation-ratio/