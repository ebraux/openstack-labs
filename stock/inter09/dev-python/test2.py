#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Il manque les fonctionnalités liées aux sous réseaux, donc on ne peut pas connecter les instances aux réseaux créés, mais seulement aux réseaux que l'on a créé préalablement (sous horizon par exemple).


from keystoneauth1.identity import v3
from keystoneauth1 import session
#from keystoneclient.v3 import client
from novaclient import client as clientno
from neutronclient.v2_0 import client as clientne

auth = v3.Password(auth_url="https://openstack.imt-atlantique.fr:5000/v3",
                   username="g18sanya",
                   password="changeit",
                   project_name="int272-intro_06",
                   user_domain_id="default",
                   project_domain_id="default")
sess = session.Session(auth=auth)
nova = clientno.Client('2',
                     'g18sanya',
                     'changeit',
                     '18bedfa83530444e9d47ec8565663b43',
                     'https://openstack.imt-atlantique.fr:5000/v3',
                     user_domain_name="Default")
#keystone = client.Client(session=sess)
neutron = clientne.Client(session = sess)
#p = keystone.projects.get('int272-intro_06')
#keystone.projects.list()
#
#project = keystone.projects.create(name="test", description="My new Project!", domain="default", enabled=True)
#project.delete()
#print(nova.images.list())




network = {'name':'reso','admin_state_up':True}


#{print(nova.servers.list())
#print(nova.flavors.list())
fl = nova.flavors.find(ram=512)
img = nova.glance.list()[-1]
#nova.servers.create("my-server", img, flavor=fl, nics=['auto'])
#if nova.servers.


def find_net_id(name) :
    for i in range(len(neutron.list_networks()["networks"])) :
        if (neutron.list_networks()["networks"][i][u'name'] == name) :
            return i
def delete_net(name) :
    i = find_net_id(name)
    if i==None:
        return 'Error : No network found with that name'
    else :
        neutron.delete_network(neutron.list_networks()["networks"][i][u'id'])
        return 'Network deleted.'
        
def create_net(name) :
    i = find_net_id(name)
    if i==None:
        network = {'name': name, "admin_state_up" : True}
        neutron.create_network({'network' : network})
        return 'Network succesfully created'
    else :
        return 'Error : Network name already exists'

def delete_server(name) :
    i = 0
    while i < len(nova.servers.list()) and nova.servers.list()[i].name != name :
        i+=1
    if i >= len(nova.servers.list()):
        return 'Error : no server found with that name'
    else :
        nova.servers.delete(nova.servers.list()[i])
        return 'Server deleted'

def create_server_add(name,network_name,ram) :
    nics = [{"net-id": nova.neutron.find_network(name=network_name).id }]
    fl = nova.flavors.find(ram=512)
    img = nova.glance.list()[-1]
    nova.servers.create(name,img,fl, nics=nics)
def create_server(name,network_name,ram=512) :
    """This function only works with an existing network,because we did'nt have enough time to implement a subnetwork"""
    i = 0
    while i < len(nova.servers.list()) and nova.servers.list()[i].name != name :
        i+=1
    if i < len(nova.servers.list()):
        print('Server name already exists : Override ? (yes/no)')
        answer = ""
        while answer != "yes" and answer != "y" and answer != "no" and answer != "n":
            answer = input()
        if answer == 'yes' or answer == 'y':
            print(delete_server(name))
        else :
            return 'Server not created : same instance name already exists'
    #if network_name == None :
    #    network_name = name + '-network'
    #create_net(network_name)
    if find_net_id(network_name)==None:
        return 'No network found with the name ' + network_name
    else :
        create_server_add(name,network_name,ram)
        return 'Server created'