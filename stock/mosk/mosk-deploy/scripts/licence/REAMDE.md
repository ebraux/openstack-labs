# Mise à jour de la licence
``` bash
kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < current.lic| tr -d '\n')\"}}"
```
