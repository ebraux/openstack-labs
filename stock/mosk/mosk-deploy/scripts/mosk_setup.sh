#!/bin/bash

home_dir=$PWD

export KUBECONFIG=$home_dir/kaas-bootstrap/kubeconfig

kubectl delete -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/cluster.yaml

kubectl delete -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-master.yaml
kubectl delete -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-ceph.yaml
kubectl delete -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-cmp.yaml

kubectl delete ns $CHILD_NAMESPACE
sleep 30
kubectl create ns $CHILD_NAMESPACE
kubectl get publickey -o yaml bootstrap-key | sed "s|namespace: default|namespace: $CHILD_NAMESPACE|" | kubectl apply -f -
kubectl get baremetalhostprofile -o yaml | sed "s|namespace: default|namespace: $CHILD_NAMESPACE|" | sed "s|name: region-one-default|name: default-$CHILD_NAMESPACE|" | kubectl apply -f -
sleep 10
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmhp-master.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmhp-ceph.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmhp-cmp-ceph.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmhp-cmp-lvm.yaml

sleep 10
kubectl get l2templates -A
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/subnet.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/l2template-cmp-lvm.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/l2template-cmp-ceph.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/l2template-master.yaml
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/l2template-ceph.yaml
#kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/l2template-cmp.yaml

#~/ipmi/ipmi_check.sh ~/ipmi/hosts
sleep 30
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-master.yaml
sleep 400
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-ceph.yaml
sleep 400
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/bmh-cmp.yaml
sleep 30
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/proxy.yml
ubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/cluster.yaml
sleep 30

kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/machines-master.yaml
sleep 400
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/machines-ceph.yaml
sleep 400
kubectl apply -f $home_dir/kaas-bootstrap/$CHILD_NAMESPACE/machines-cmp.yaml

