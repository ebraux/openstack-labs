
nodes="
br-mosk001-ceph-0
br-mosk001-ceph-1
br-mosk001-ceph-2
br-mosk001-cmp-0
br-mosk001-cmp-1
br-mosk001-cmp-2
br-mosk001-cmp-3
br-mosk001-master-0
br-mosk001-master-1
br-mosk001-master-2
"
#for i in $nodes; do 
#kubectl patch bmh $i -n br-mosk001 --type json     --patch='[ { "op": "remove", "path": "/metadata/finalizers" } ]'
#kubectl -n br-mosk001 delete bmh $i
#done

function clean_child(){
echo "Cleanup env ${ENV_NAME}? ENTER to skip, y to yup:"
read -n 1 cleanup
if  [ "$cleanup" == 'y' ]; then
  echo "cleanup.."
  time kubectl -n ${TARGET_NAMESPACE} delete cluster --all || true
  for bmh in $(kubectl -n ${TARGET_NAMESPACE} get bmh -o custom-columns=:.metadata.name) ; do
    kubectl -n ${TARGET_NAMESPACE} patch bmh ${bmh} --type=json -p='[{"op": "remove", "path": "/metadata/annotations"}]' || true
  done
  time kubectl -n ${TARGET_NAMESPACE} delete bmh --all || true
  time kubectl delete ns ${TARGET_NAMESPACE} || true
fi
}
