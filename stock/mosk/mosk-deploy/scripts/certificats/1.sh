#!/bin/bash

set -x


kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.ca}' | base64 -d | tee tls.ca
kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.crt}' | base64 -d | tee tls.crt
kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.key}' | base64 -d | tee tls.key

cat <<EOF > ironic_rc_mgmt_mtls
export HTTP_PROXY="http://proxy.enst-bretagne.fr:8080"
export HTTPS_PROXY="http://proxy.enst-bretagne.fr:8080"
if [[ ! -d ~/venv_os/ ]] ; then
  virtualenv --always-copy --python=/usr/bin/python3 ~/venv_os/
  . ~/venv_os/bin/activate
  pip3 install python-ironic-inspector-client python-ironicclient python-openstackclient
else
  . ~/venv_os/bin/activate
fi
unset HTTP_PROXY
unset HTTPS_PROXY
IRONIC_IP=$(kubectl --kubeconfig ${KUBECONFIG} get svc ironic-kaas-bm -n kaas -o json | jq -r .status.loadBalancer.ingress[0].ip)
export INSPECTOR_URL=https://ironic-kaas-bm:5050/
export OS_URL=https://ironic-kaas-bm:6385/
export OS_TOKEN=fake-token
export OS_ENDPOINT=${OS_URL}
EOF


openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node list
