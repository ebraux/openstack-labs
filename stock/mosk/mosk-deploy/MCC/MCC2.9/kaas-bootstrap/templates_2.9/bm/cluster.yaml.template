apiVersion: cluster.k8s.io/v1alpha1
kind: Cluster
metadata:
  name: kaas-mgmt
  labels:
    kaas.mirantis.com/provider: baremetal
spec:
  clusterNetwork:
    services:
      cidrBlocks:
        - 10.233.0.0/18
    pods:
      cidrBlocks:
        - 10.233.64.0/18
  providerSpec:
    value:
      apiVersion: baremetal.k8s.io/v1alpha1
      kind: BaremetalClusterProviderSpec
      # Set from KaaSRelease during bootstrap process
      # release:
      # Will be set during bootstrap.
      # Must be out of 10.129.172.61-10.129.172.80
      loadBalancerHost: 10.129.172.90
      nodeCidr: 10.10.10.0/24
      dnsNameservers:
        - 192.44.75.10
      # default valid for US ICM cloud
      #  - 172.18.224.6
      # use this value for EU ICM cloud
      #  - 172.18.176.6
      dedicatedControlPlane: false
      helmReleases:
        - name: metallb
          values:
            configInline:
              address-pools:
                - name: default
                  protocol: layer2
                  addresses:
                    - 10.129.172.61-10.129.172.80
        - name: ceph-controller
          values: {}
        - name: stacklight
          values:
            clusterSize: small
            alertmanagerSimpleConfig:
              email:
                enabled: false
              slack:
                enabled: false
            elasticsearch:
              logstashRetentionTime: "2"
              persistentVolumeClaimSize: 10Gi
              #cronJobSchedule: "0 * * * *"
            highAvailabilityEnabled: true
            logging:
              enabled: true
            prometheusServer:
              customAlerts: []
              persistentVolumeClaimSize: 10Gi
              retentionSize: 2GB
              retentionTime: 2d
              watchDogAlertEnabled: true
            telemetry:
              metricCollector:
                enabled: true
      kaas:
        # Set from KaaSRelease during bootstrap process
        # release:
        regional:
        - provider: baremetal
          helmReleases:
          - name: baremetal-operator
            values: {}
          - name: baremetal-provider
            values:
              config:
                ipamEnabled: true
                lcm:
                  ntp:
                    servers:
                    - ntp1.svc.enst-bretagne.fr
                    - ntp2.svc.enst-bretagne.fr
                    - ntp3.svc.enst-bretagne.fr
          - name: kaas-ipam
            values: {}
        - provider: byo
        management:
          enabled: true
          helmReleases:
            - name: kaas-ui
              values:
                keycloak:
                  realm: iam
                  clientId: kaas
            - name: iam
              requiresPersistentVolumes: true
              values:
                keycloak:
                  initUsers:
                    reader:
                      username: reader
                      passwordSalt: "ifH1Ci/Sh4mSh9XnGxjL1Q=="
                      passwordHash: "e4Y+vZUzhw8qwoWQOpem9Rocklc2ANjYUtuvPsQ2Sye/oHvH9YyaJh9o8BZky4zN3G3GRZBWZcOa1mA1BdlsyA=="
                      realmRoles:
                        - "m:kaas@reader"
                    writer:
                      username: writer
                      passwordSalt: "eRV7Mqct4IGGorsunKwjZA=="
                      passwordHash: "5jXw+YMUHOgDeJsINHQ1btwaz1ZiGixfLAaTfxFo3piu6A82+Jh/KuafShO56D87xvn4F2cqFRTRNlqpf33Vsw=="
                      realmRoles:
                        - "m:kaas@writer"
                    operator:
                      username: operator
                      passwordSalt: "8gZNMZ5WmuW7Vz9rfL2JDA=="
                      passwordHash: "asB+M425XicdXIXNj1n7JGyoY2MMRCqwmmq8Q8uzH1gJDvpTD8hfQodkSOOyaujumIGnZoKijzGtXwcqhnfq0w=="
                      realmRoles:
                        - "m:kaas@operator"
                  keycloak:
                    pvc:
                      enabled: false
