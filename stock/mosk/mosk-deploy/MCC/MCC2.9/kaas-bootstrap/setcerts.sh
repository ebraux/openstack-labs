./kaas  set certificate --cacert-file  \
	--cert-file /root/certs/wildcard_beta.imt-atlantique.fr_plus_CHAINE_CA.pem --key-file /root/certs/wildcard_beta.imt-atlantique.fr.key \
	--for keycloak  --hostname  iam.beta.imt-atlantique.fr \
	--kubeconfig kubeconfig
./kaas  set certificate --cert-file /root/certs/wildcard_beta.imt-atlantique.fr_plus_CHAINE_CA.pem \
	--key-file /root/certs/wildcard_beta.imt-atlantique.fr.key --for ui --hostname iam.beta.imt-atlantique.fr \
	--kubeconfig kubeconfig
