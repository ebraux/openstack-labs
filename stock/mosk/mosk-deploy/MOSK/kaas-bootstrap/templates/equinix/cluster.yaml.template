apiVersion: cluster.k8s.io/v1alpha1
kind: Cluster
metadata:
  name: kaas-mgmt
  labels:
    kaas.mirantis.com/provider: equinixmetal
spec:
  clusterNetwork:
    services:
      cidrBlocks:
        - 172.21.0.0/18
    pods:
      cidrBlocks:
        - 172.21.128.0/17
  providerSpec:
    value:
      apiVersion: equinix.kaas.mirantis.com/v1alpha1
      kind: EquinixMetalClusterProviderSpec
      # Set from KaaSRelease during bootstrap process
      # release:
      facility: am6
      dedicatedControlPlane: false
      credentials: cloud-config
      helmReleases:
        - name: stacklight
          values:
            telemetry:
              metricCollector:
                enabled: true
      kaas:
        # Set from KaaSRelease during bootstrap process
        # release:
        regional:
        - provider: equinixmetal
        - provider: byo
        management:
          enabled: true
          helmReleases:
            - name: kaas-ui
              values:
                keycloak:
                  realm: iam
                  clientId: kaas
            - name: iam
              requiresPersistentVolumes: true
              values:
                keycloak:
                  initUsers:
                    reader:
                      username: reader
                      passwordSalt: "ifH1Ci/Sh4mSh9XnGxjL1Q=="
                      passwordHash: "e4Y+vZUzhw8qwoWQOpem9Rocklc2ANjYUtuvPsQ2Sye/oHvH9YyaJh9o8BZky4zN3G3GRZBWZcOa1mA1BdlsyA=="
                      realmRoles:
                        - "m:kaas@reader"
                    writer:
                      username: writer
                      passwordSalt: "eRV7Mqct4IGGorsunKwjZA=="
                      passwordHash: "5jXw+YMUHOgDeJsINHQ1btwaz1ZiGixfLAaTfxFo3piu6A82+Jh/KuafShO56D87xvn4F2cqFRTRNlqpf33Vsw=="
                      realmRoles:
                        - "m:kaas@writer"
                    operator:
                      username: operator
                      passwordSalt: "8gZNMZ5WmuW7Vz9rfL2JDA=="
                      passwordHash: "asB+M425XicdXIXNj1n7JGyoY2MMRCqwmmq8Q8uzH1gJDvpTD8hfQodkSOOyaujumIGnZoKijzGtXwcqhnfq0w=="
                      realmRoles:
                        - "m:kaas@operator"
                  keycloak:
                    pvc:
                      enabled: false
