
## kaas tarball download
Dans doc : [https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html)

indiqué qu'apèrès un auto-update, il faut mettre à jour manuellement le "tarball" 

1/ c'est quoi le tarball
2/ la commande indiquée ne fonctionne pas. par exemple sur la machine mcc-seed-01, pour le management cluster MCC
``` bash
./kaas bootstrap download --management-kubeconfig kubeconfig --target-dir update_20210928
```

``` bash
Manage a Bootstrap cluster with one command

Usage:
  kaas bootstrap [command]

Available Commands:
  aws         Configure IAM policy for KaaS
  create      Create a Bootstrap cluster
  delete      Delete a Bootstrap cluster
  license     Validate KaaS License
  preflight   Preflight check
  prepare     Prepare a Bootstrap cluster
  regional    Manage Regional clusters
  user        Manage MCC users

Flags:
  -h, --help   help for bootstrap

Global Flags:
      --add-dir-header                   If true, adds the file directory to the header
      --alsologtostderr                  log to standard error as well as files
      --log-backtrace-at traceLocation   when logging hits line file:N, emit a stack trace (default :0)
      --log-dir string                   If non-empty, write log files in this directory
      --log-file string                  If non-empty, use this log file
      --log-file-max-size uint           Defines the maximum size a log file can grow to. Unit is megabytes. If the value is 0, the maximum file size is unlimited. (default 1800)
      --logtostderr                      log to standard error instead of files (default true)
      --skip-headers                     If true, avoid header prefixes in the log messages
      --skip-log-headers                 If true, avoid headers when opening log files
      --stderrthreshold severity         logs at or above this threshold go to stderr (default 2)
  -v, --v Level                          number for the log level verbosity
      --vmodule moduleSpec               comma-separated list of pattern=N settings for file-filtered logging

Use "kaas bootstrap [command] --help" for more information about a command.
```


