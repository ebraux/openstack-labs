

                                                v1.0.0-20210716222903
image: 127.0.0.1:44301/ceph/mcp/ceph-controller:v1.0.0-20210521190241





 

kmosk get  lcmclusterstate -o wide
NAME                                                              CLUSTERNAME       TYPE            ARG                                              VALUE   ACTUALVALUE   ATTEMPT   MESSAGE
cordon-drain-kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4cancjv5   kaas-br-mosk001   cordon-drain    kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   false   false         0         
  -> br-mosk001-master-1   kaas-br-mosk001   control   Ready     10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.2.0-349-g4870b7f5
cordon-drain-kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6lx4kw   kaas-br-mosk001   cordon-drain    kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   false   false         0         
  -> br-mosk001-ceph-0     kaas-br-mosk001   worker    Ready     10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.2.0-349-g4870b7f5
cordon-drain-kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8e6jwvk   kaas-br-mosk001   cordon-drain    kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   false   false         0         
  -> br-mosk001-master-2   kaas-br-mosk001   control   Ready     10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.2.0-349-g4870b7f5
cordon-drain-kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa850w6pv4   kaas-br-mosk001   cordon-drain    kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   false   false         0         
  -> br-mosk001-ceph-2     kaas-br-mosk001   worker    Ready     10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.2.0-349-g4870b7f5
cordon-drain-kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8w7vzw   kaas-br-mosk001   cordon-drain    kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   false   false         0         
  -> br-mosk001-ceph-1     kaas-br-mosk001   worker    Ready     10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.2.0-349-g4870b7f5
cordon-drain-kaas-node-b10834cf-25a4-40e9-85be-d7be198417f9dqv8   kaas-br-mosk001   cordon-drain    kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   false   false         0         
  -> br-mosk001-master-0   kaas-br-mosk001   control   Ready     10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.2.0-349-g4870b7f5
  
---
helm-deployed-kaas-br-mosk001-pzc4d                               kaas-br-mosk001   helm-deployed                                                    true    true          0         
swarm-drain-kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6fdml4v   kaas-br-mosk001   swarm-drain     kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   false   false         0         
swarm-drain-kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504jcwl9   kaas-br-mosk001   swarm-drain     kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   false   false         0         
swarm-drain-kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8cpltrz   kaas-br-mosk001   swarm-drain     kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   false   false         0         
---

cordon-drain-kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096tbknb   kaas-br-mosk001   cordon-drain    kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   true    true          0         
swarm-drain-kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c9rfqw   kaas-br-mosk001   swarm-drain     kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   true    true          0   
  -> br-mosk001-cmp-0      kaas-br-mosk001   worker    Prepare   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.2.0-349-g4870b7f5


br-mosk001-cmp-0      kaas-br-mosk001   worker    Prepare   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.2.0-349-g4870b7f5
br-mosk001-cmp-05     kaas-br-mosk001   worker    Prepare   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   v0.2.0-349-g4870b7f5
br-mosk001-cmp-06     kaas-br-mosk001   worker    Prepare   10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   v0.2.0-349-g4870b7f5
br-mosk001-cmp-07     kaas-br-mosk001   worker    Ready     10.129.172.117   kaas-node-38538401-efd4-4ddc-a328-7666724b917f   v0.2.0-349-g4870b7f5
br-mosk001-cmp-09     kaas-br-mosk001   worker    Prepare   10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   v0.2.0-349-g4870b7f5
br-mosk001-cmp-1      kaas-br-mosk001   worker    Prepare   10.129.172.110   kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302   v0.2.0-349-g4870b7f5
br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready     10.129.172.112   kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   v0.2.0-349-g4870b7f5




$ openstack compute service list
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
| ID | Binary         | Host                                           | Zone     | Status   | State | Updated At                 |
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
| 37 | nova-compute   | kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c | nova     | enabled  | up    | 2021-10-18T15:31:09.000000 |
| 40 | nova-compute   | kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302 | nova     | disabled | up    | 2021-10-18T15:31:12.000000 |
| 52 | nova-compute   | kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea | nova     | enabled  | up    | 2021-10-18T15:31:11.000000 |
| 55 | nova-compute   | kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059 | nova     | enabled  | up    | 2021-10-18T15:31:20.000000 |
| 58 | nova-compute   | kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd | nova     | enabled  | up    | 2021-10-18T15:31:12.000000 |
| 70 | nova-conductor | nova-conductor-5db55b5645-67t89                | internal | enabled  | up    | 2021-10-18T15:31:12.000000 |
| 73 | nova-scheduler | nova-scheduler-59cbf7f7fc-c67ks                | internal | enabled  | up    | 2021-10-18T15:31:18.000000 |
| 85 | nova-conductor | nova-conductor-5db55b5645-559r5                | internal | enabled  | up    | 2021-10-18T15:31:18.000000 |
| 87 | nova-scheduler | nova-scheduler-59cbf7f7fc-2tmkp                | internal | enabled  | up    | 2021-10-18T15:31:04.000000 |
| 89 | nova-conductor | nova-conductor-5db55b5645-4btnp                | internal | enabled  | up    | 2021-10-18T15:31:18.000000 |
| 91 | nova-scheduler | nova-scheduler-59cbf7f7fc-grxnj                | internal | enabled  | up    | 2021-10-18T15:31:09.000000 |
| 97 | nova-compute   | kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163 | nova     | disabled | up    | 2021-10-18T15:31:33.000000 |
| 99 | nova-compute   | kaas-node-38538401-efd4-4ddc-a328-7666724b917f | nova     | enabled  | up    | 2021-10-18T15:31:10.000000 |
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
