# Ajout d'un compute



## Préparation


A executer depuis un des 3 noeuds de gestion du cluster Openstack: 

Configuration de l'environnement
```bash
export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig
```

Vérifier l'accès IPMI
``` bash
ipmitool -I lanplus -H '10.29.20.36' -U 'root' -P 'OStack2021' chassis power status
```

Crééer le dossier avec les caractéristiques du serveur
```bash
cd ~/MCC2.9/kaas-bootstrap/br-mosk001
mkdir cmp-6
```


## Créer le fichier de "secret"

Exemple de fichier de secret : br-mosk001-cmp-6-bmc-secret.yaml
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  namespace: br-mosk001
  name: br-mosk001-cmp-6-bmc-secret
  labels:
    kaas.mirantis.com/credentials: "true"
    kaas.mirantis.com/provider: baremetal
    kaas.mirantis.com/region: region-one
type: Opaque
data:
  username: cm9vdA==
  password: T1N0YWNrMjAyMQ==
```

Create Secret Config for IMPI password
``` bash
kubectl apply -f br-mosk001/cmp-5/br-mosk001-cmp-5-bmc-secret.yaml
---
secret/br-mosk001-cmp-5-bmc-secret created
```

Vérification
```bash
kubectl -n br-mosk001 get secret
---
br-mosk001-cmp-5-bmc-secret                      Opaque                                2      7s
```

---

## Crééer le BM-Host

Exemple de fichier de BM Host :  br-mosk001-cmp-6_bmhost.yaml
```yaml
---
apiVersion: metal3.io/v1alpha1
kind: BareMetalHost
metadata:
  namespace: br-mosk001
  name: br-mosk001-cmp-6
  labels:
    baremetal: hw-br-mosk001-cmp-6
    kaas.mirantis.com/provider: baremetal
    kaas.mirantis.com/region: region-one
    kaas.mirantis.com/baremetalhost-id: br-mosk001-cmp-6
spec:
  bootMode: UEFI
  online: true
  bootMACAddress: b0:26:28:13:eb:b0
  bmc:
    address: 10.29.20.36:623
    credentialsName: br-mosk001-cmp-6-bmc-secret
```

Create "Baremetal Hosts"
```bash
kubectl -n br-mosk001 apply -f br-mosk001-cmp-6_bmhost.yaml
---
baremetalhost.metal3.io/br-mosk001-cmp-6 created
```

Vérification
```bash
kubectl -n br-mosk001 get bmh grep br-mosk001-cmp-6
---
NAME               STATE   CONSUMER           BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-cmp-4   ready   br-mosk001-cmp-4   UEFI       true             region-one
```

Attendre qu'il passe de "inspecting" à "ready". ça peut prendre quelques minutes.

Pour Suivre le déroulement de 'l"inspection", le plus simple c'ets de se connecter à la console, via la carte iDrac

En cas de pb, les logs sont visible dans le détail de la config  
```bash
kubectl -n br-mosk001 get bmh  br-mosk001-cmp-4 -o yaml |less
```

## Créer la machine


Exemple de fichier de machine : br-mosk001-cmp-6_machine.yml
```yaml
---
apiVersion: cluster.k8s.io/v1alpha1
kind: Machine
metadata:
  name: br-mosk001-cmp-6
  namespace: br-mosk001
  labels:
    kaas.mirantis.com/region: region-one
    hostlabel.bm.kaas.mirantis.com/worker: "true"
    kaas.mirantis.com/provider: baremetal
    cluster.sigs.k8s.io/cluster-name: kaas-br-mosk001
    openstack.nova.storage: lvm
spec:
  providerSpec:
    value:
      apiVersion: "baremetal.k8s.io/v1alpha1"
      kind: "BareMetalMachineProviderSpec"
      bareMetalHostProfile:
        name: mosk-compute-lvm
        namespace: br-mosk001
      l2TemplateSelector:
        name: br-mosk001-4nic-cmp-lvm
        namespace: br-mosk001
      nodeLabels:
        - key: openstack-compute-node
          value: enabled
        - key: openvswitch
          value: enabled
      hostSelector:
        matchLabels:
          baremetal: hw-br-mosk001-cmp-6

```

Vérifier la configuration reconnue du BMHost, pour adapter la configuration de la machine
```bash
kubectl -n br-mosk001 get bmh  br-mosk001-cmp-4 -o yaml |less
```

Notament :
- le nombre (4, 6 ou 8) et le nom des NICs (enoX, ...) : pour choisir la bonn L2Template
- le nombre et le nom du stockage (/dev/sda): pour savoir


Lancer la création de la machine
```bash
kubectl -n br-mosk001 create -f br-mosk001-cmp-4_machine.yml
```

Vérification
```bash
kubectl -n br-mosk001 get machine
---
br-mosk001-cmp-4  
```
Suivi
```bash
kubectl get bmh -A
---
NAMESPACE    NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001   br-mosk001-cmp-4      ready         br-mosk001-cmp-4      UEFI       true             region-one
```
- Il doit y avoir un "CONSUMER"
- Attendre qu'elle passe de "ready" à "provisioned". ça peut prendre quelques minutes.

Vérification de la validité de la configutaion l2template
```bash
kubectl get ipamhosts -A
NAMESPACE    NAME                  STATUS   AGE     REGION
br-mosk001   br-mosk001-cmp-4      OK       6m38s   region-one
```
- le status doit être à OK


```bash
kubectl get lcmmachines -A -o wide;
NAMESPACE    NAME                  CLUSTERNAME       TYPE      STATE   INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001   br-mosk001-cmp-4      kaas-br-mosk001   worker                                                                              
```
- le status doit êtr "Ready", et les information de "INTERNALIP", "HOSTNAME" et "AGENTVERSION" doivnet être renseignés

