

I have tried to deploy a new compute.
It is the frisrt time, and this is a fresh new Mosk Infra.
4 computes have been alredy deployed by youy deployment team.

I have created a BM config: br-mosk001-cmp-4_bmhost.yaml

And applied it
``` yaml
kubectl -n br-mosk001 apply -f 
``` 
Its STATE passed from "inspecting" to "ready"
``` yaml
kubectl -n br-mosk001 get bmh br-mosk001-cmp-4
NAME               STATE   CONSUMER           BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-cmp-4   ready                      UEFI       true             region-one
```

Then I have created a Machine config: br-mosk001-cmp-4_machine.yaml

And I have created  it
```bash
kubectl -n br-mosk001 create -f br-mosk001-cmp-4_machine.yml
```
and checked it
```bash
kubectl -n br-mosk001 get bmh -A
NAMESPACE    NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001   br-mosk001-cmp-4      ready         br-mosk001-cmp-4      UEFI       true             region-one
```

```bash
kubectl -n br-mosk001  get ipamhosts -A
NAMESPACE    NAME                  STATUS   AGE   REGION
br-mosk001   br-mosk001-cmp-4      OK       14h   region-one
```

But then nothing. The provisionning doesn't seems to start.


I have noticed that on the server's console, the inspecting process is looping.
- There is a message concerning a python error during inspection  :"Error getting configuration from ironic : Could not look up node info."
- And at the end, thers is a 404 error, from a request to https://ironic-kaas-bm:6385. 

I don't know wher to find more logs or informations.

Please help me.


---

depuis un compute avec une config similaire : accès Vlan 172
```bash
mcc-user@kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec:~$ ip a | grep 172
    inet 10.129.172.112/24 brd 10.129.172.255 scope global eno1
mcc-user@kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec:~$ 
```

Accès à Ironic
```bash
mcc-user@kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec:~$ curl -k https://10.129.172.64:6385
{"name": "OpenStack Ironic API", "description": "Ironic is an OpenStack project which aims to provision baremetal machines.", "default_version": {"id": "v1", "links": [{"href": "https://127.0.0.1:46385//v1/", "rel": "self"}], "status": "CURRENT", "min_version": "1.1", "version": "1.68"}, "versions": [{"id": "v1", "links": [{"href": "https://127.0.0.1:46385//v1/", "rel": "self"}], "status": "CURRENT", "min_version": "1.1", "version": "1.68"}]}
```

---
    # NOTE(alexz): Otherwise, if value in "all" state, all macs will be also added into
    # pxe_filter => which may broke buggy envs with multiply PXE-network NICs
    add_ports = pxe
    # FIXME(alexz):disable "lldp_basic,extra_hardware," temporarly

---

hello, in the configmap inspector-config, I have found this 
```bash
    # NOTE(alexz): Otherwise, if value in "all" state, all macs will be also added into
    # pxe_filter => which may broke buggy envs with multiply PXE-network NICs
    add_ports = pxe
```
I think that we still have We still have this issue    

