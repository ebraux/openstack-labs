

$ kubectl -n br-mosk001 get machine
---
NAME                  PROVIDERID   PHASE
br-mosk001-ceph-0                  
br-mosk001-ceph-1                  
br-mosk001-ceph-2                  
br-mosk001-cmp-0                   
br-mosk001-cmp-1                   
br-mosk001-cmp-2                   
br-mosk001-cmp-3                   
br-mosk001-master-0                
br-mosk001-master-1                
br-mosk001-master-2                


$ kubectl -n br-mosk001 get bmh
NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true             region-one
br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true             region-one
br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true             region-one
br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true             region-one
br-mosk001-cmp-1      provisioned   br-mosk001-cmp-1      UEFI       true             region-one
br-mosk001-cmp-2      provisioned   br-mosk001-cmp-2      UEFI       true             region-one
br-mosk001-cmp-3      provisioned   br-mosk001-cmp-3      UEFI       true             region-one
br-mosk001-cmp-4      ready                               UEFI       false            region-one
br-mosk001-cmp-5      ready                               UEFI       true             region-one
br-mosk001-cmp-9      ready                               UEFI       true             region-one
br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true             region-one
br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true             region-one
br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true             region-one


$ kubectl -n br-mosk001 delete bmh br-mosk001-cmp-9

$kubectl -n br-mosk001 get bmh
---
NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true             region-one
br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true             region-one
br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true             region-one
br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true             region-one
br-mosk001-cmp-1      provisioned   br-mosk001-cmp-1      UEFI       true             region-one
br-mosk001-cmp-2      provisioned   br-mosk001-cmp-2      UEFI       true             region-one
br-mosk001-cmp-3      provisioned   br-mosk001-cmp-3      UEFI       true             region-one
br-mosk001-cmp-4      ready                               UEFI       false            region-one
br-mosk001-cmp-5      ready                               UEFI       true             region-one
br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true             region-one
br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true             region-one
br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true             region-one



$ date
Fri Oct  1 13:20:33 UTC 2021


$ kubectl -n br-mosk001 apply -f br-mosk001/cmp-9/br-mosk001-cmp-9_bmhost.yaml
---
secret/br-mosk001-cmp-9-bmc-secret created
baremetalhost.metal3.io/br-mosk001-cmp-9 created

  --------------------------------
 (  the server boot, and loop)
   -----------------------------

$ date
Fri Oct  1 13:37:37 UTC 2021
$ kubectl -n br-mosk001 get bmh
---
NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true             region-one
br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true             region-one
br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true             region-one
br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true             region-one
br-mosk001-cmp-1      provisioned   br-mosk001-cmp-1      UEFI       true             region-one
br-mosk001-cmp-2      provisioned   br-mosk001-cmp-2      UEFI       true             region-one
br-mosk001-cmp-3      provisioned   br-mosk001-cmp-3      UEFI       true             region-one
br-mosk001-cmp-4      ready                               UEFI       false            region-one
br-mosk001-cmp-5      ready                               UEFI       true             region-one
br-mosk001-cmp-9      ready                               UEFI       true             region-one
br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true             region-one
br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true             region-one
br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true             region-one

$  kubectl -n br-mosk001 delete bmh br-mosk001-cmp-9
---
baremetalhost.metal3.io "br-mosk001-cmp-9" deleted

$ kubectl -n br-mosk001 get  bmh br-mosk001-cmp-9
---
Error from server (NotFound): baremetalhosts.metal3.io "br-mosk001-cmp-9" not found


  --------------------------------
 (  the server still  loop)
   -----------------------------

$ kubectl -n kaas logs baremetal-provider-6c74fd5447-pqjfj >  baremetal-provider-6c74fd5447-pqjfj_full.log
$ kubectl -n kaas logs ironic-56676c774b-bfcjs  ironic-api >  ironic-56676c774b-bfcjs_ironic-api_full.log



"GET /v1/lookup?addresses=18%3A66%3Ada%3Aec%3A4b%3A3e%2C18%3A66%3Ada%3Aec%3A4b%3A3c%2C18%3A66%3Ada%3Aec%3A4b%3A3f%2C18%3A66%3Ada%3Aec%3A4b%3A3d HTTP/1.1" status: 404  len: 448 time: 0.0370469^[[00m


2021-10-01 13:38:47.748 27 INFO eventlet.wsgi.server [req-ca9886f5-3ece-4b2d-a37c-e54ee90e346d - - - - -] 10.129.172.101,127.0.0.1 "GET /v1/lookup?addresses=00%3A22%3A19%3A6b%3A83%3A18%2C00%3A22%3A19%3A6b%3A83%3A14%2C00%3A22%3A19%3A6b%3A83%3A16%2C00%3A22%3A19%3A6b%3A83%3A12 HTTP/1.1" status: 404  len: 448 time: 0.0443339

00:22:19:6b:83:18,00:22:19:6b%3A83%3A14%2C00%3A22%3A19%3A6b%3A83%3A16%2C00%3A22%3A19%3A6b%3A83%3A12

e4:43:4b:83:10:2c

I gave you the ironic API logs, because I can sees wrrong request the lookup interface
```
2021-10-01 13:38:54.903 26 INFO eventlet.wsgi.server [req-d29be15c-c529-4617-b9fa-bfffd7acd065 - - - - -] 10.129.172.101,127.0.0.1 "GET /v1/lookup?addresses=e4%3A43%3A4b%3A83%3A10%3A2d%2Ce4%3A43%3A4b%3A83%3A10%3A2e%2Ce4%3A43%3A4b%3A83%3A10%3A2c%2Ce4%3A43%3A4b%3A83%3A10%3A2f HTTP/1.1" status: 404  len: 448 time: 0.0360122
--> e4:43:4b:83:10:2d,e4:43:4b:83:10:2e,e4:43:4b:83:10:2c,2C:43:4b:83:10:2f
not only the PXE, e4:43:4b:83:10:2c, but all the MAC adresses
```

```
18%3A66%3Ada%3Aec%3A4b%3A3c,... --> cmp-4
00%3A22%3A19%3A6b%3A83%3A18,... --> cmp-5
```


"GET /v1/lookup?addresses=e4%3A43%3A4b%3A83%3A10%3A2e%2Ce4%3A43%3A4b%3A83%3A10%3A2c%2Ce4%3A43%3A4b%3A83%3A10%3A2f%2Ce4%3A43%3A4b%3A83%3A10%3A2d&node_uuid=4cbbf54c-43f1-4076-acb4-1945dee67339 HTTP/1.1" status: 200  len: 1315 time: 0.1151071
2021-10-01 13:31:00.699 25 INFO eventlet.wsgi.server [req-2547dfc1-7a80-4bda-bc3f-0d8cc31bfd1a - - - - -] 10.129.172.101,127.0.0.1 "GET /v1/lookup?addresses=e4%3A43%3A4b%3A83%3A10%3A2f%2Ce4%3A43%3A4b%3A83%3A10%3A2d%2Ce4%3A43%3A4b%3A83%3A10%3A2e%2Ce4%3A43%3A4b%3A83%3A10%3A2c HTTP/1.1" status: 200  len: 1511 time: 0.1351280
