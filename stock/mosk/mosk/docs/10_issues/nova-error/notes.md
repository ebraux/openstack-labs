
-kaas-node-31823844-3925-4d13-92af-c5018ee51b1a.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d
br-mosk001-cmp-11  -  10.129.172.119   kaas-node-31823844-3925-4d13-92af-c5018ee51b1a
OK

-kaas-node-b293fd7a-21e9-463c-b5e3-e4a7648e6a02.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d
br-mosk001-cmp-10  - 10.129.172.110   kaas-node-b293fd7a-21e9-463c-b5e3-e4a7648e6a02
OK

-kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d
br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready   10.129.172.112   kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   v0.3.0-32-gee08c2b8
en cours machine

-kaas-node-d2144611-3110-417f-8d20-ac38b6ddbdbf.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d
br-mosk001-cmp-14     kaas-br-mosk001   worker    Ready   10.129.172.111   kaas-node-d2144611-3110-417f-8d20-ac38b6ddbdbf   v0.3.0-32-gee08c2b8

----
-kaas-node-a7f2d0f2-411b-423d-b4f8-af604768a55b.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d
br-mosk001-cmp-02     kaas-br-mosk001   worker    Ready   10.129.172.118   kaas-node-a7f2d0f2-411b-423d-b4f8-af604768a55b   v0.3.0-32-gee08c2b8



10.129.172.119
kaas-node-31823844-3925-4d13-92af-c5018ee51b1a

```bash
cd ~/_LOCAL/developpements/openstack/mosk-deploy/
ssh-add auth/ssh_key
ssh mcc-user@10.129.172.119
exit
```

```bash
cd ~/_LOCAL/developpements/openstack/mosk-deploy
source mosk-env.sh 
kubectl -n br-mosk001 get nodes


```
```bash
cd ~/_LOCAL/developpements/openstack/operations
source auth/admin-openrc.sh
openstack compute service list
export COMPUTE_NAME=kaas-node-31823844-3925-4d13-92af-c5018ee51b1a

openstack compute service list --host $COMPUTE_NAME
openstack compute service delete 279

openstack network agent list --host $COMPUTE_NAME
openstack network agent delete c11d6c0b-dce1-4edf-9e9a-1c8ee5038785
openstack network agent delete eceaacff-4439-4915-8c64-bd28c1f1283b
```


---
    image:
      checksum: http://httpd-http/images/stub_image.qcow2.md5sum
      url: http://httpd-http/images/stub_image.qcow2
    rootDeviceHints:
---

```bash
cd ~/_LOCAL/developpements/openstack/mosk-deploy
source mcc-env.sh
kubectl -n br-mosk001 delete  machine br-mosk001-cmp-11
kubectl -n br-mosk001 delete  bmh br-mosk001-cmp-11
```
---




```bashsudo df -h
Filesystem                       Size  Used Avail Use% Mounted on
udev                              63G     0   63G   0% /dev
tmpfs                             13G  7.2M   13G   1% /run
/dev/mapper/lvm_root-root         45G   24G   19G  57% /
tmpfs                             63G     0   63G   0% /dev/shm
tmpfs                            5.0M     0  5.0M   0% /run/lock
tmpfs                             63G     0   63G   0% /sys/fs/cgroup
/dev/sda2                        204M  152K  204M   1% /boot/efi
/dev/mapper/lvm_lvp-lvp           35G   49M   33G   1% /mnt/local-volumes
/dev/mapper/nova--vol-nova_fake  976M  2.6M  907M   1% /nova-fake
```

``` bash
$ sudo cat /etc/fstab 
UUID=26921660-fb83-45c8-bfef-3fa1e46ae54e / ext4 defaults,errors=remount-ro 0 0
UUID=2F72-90A5 /boot/efi vfat defaults,errors=remount-ro 0 0
UUID=a8a53f7b-fad5-475a-bafe-da8d8a632730 /mnt/local-volumes ext4 defaults,errors=remount-ro 0 0
UUID=8395d3e9-b840-48af-b5b2-7ddd7d08dcaa /nova-fake ext4 defaults,errors=remount-ro 0 0

/mnt/local-volumes/src/stacklight/elasticsearch-data/vol00 /mnt/local-volumes/stacklight/elasticsearch-data/vol00 none bind 0 0
/mnt/local-volumes/src/stacklight/prometheus-data/vol00 /mnt/local-volumes/stacklight/prometheus-data/vol00 none bind 0 0
/mnt/local-volumes/src/stacklight/alertmanager-data/vol00 /mnt/local-volumes/stacklight/alertmanager-data/vol00 none bind 0 0
/mnt/local-volumes/src/stacklight/postgresql-db/vol00 /mnt/local-volumes/stacklight/postgresql-db/vol00 none bind 0 0
/mnt/local-volumes/src/stacklight/postgresql-db/vol01 /mnt/local-volumes/stacklight/postgresql-db/vol01 none bind 0 0
/mnt/local-volumes/src/openstack-operator/bind-mounts/vol00 /mnt/local-volumes/openstack-operator/bind-mounts/vol00 none bind 0 0
/mnt/local-volumes/src/openstack-operator/bind-mounts/vol01 /mnt/local-volumes/openstack-operator/bind-mounts/vol01 none bind 0 0
/mnt/local-volumes/src/openstack-operator/bind-mounts/vol02 /mnt/local-volumes/openstack-operator/bind-mounts/vol02 none bind 0 0
/mnt/local-volumes/src/tungstenfabric-operator/bind-mounts/vol00 /mnt/local-volumes/tungstenfabric-operator/bind-mounts/vol00 none bind 0 0
/mnt/local-volumes/src/tungstenfabric-operator/bind-mounts/vol01 /mnt/local-volumes/tungstenfabric-operator/bind-mounts/vol01 none bind 0 0
/mnt/local-volumes/src/tungstenfabric-operator/bind-mounts/vol02 /mnt/local-volumes/tungstenfabric-operator/bind-mounts/vol02 none bind 0 0
/mnt/local-volumes/src/tungstenfabric-operator/bind-mounts/vol03 /mnt/local-volumes/tungstenfabric-operator/bind-mounts/vol03 none bind 0 0
```

```bash
sudo fdisk -l
Disk /dev/sda: 278.9 GiB, 299439751168 bytes, 584843264 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: B5F375F3-3524-477E-A09E-E4FA181F8A73

Device         Start       End   Sectors   Size Type
/dev/sda1       2048     10239      8192     4M BIOS boot
/dev/sda2      10240    428031    417792   204M EFI System
/dev/sda3     428032    559103    131072    64M Linux filesystem
/dev/sda4     559104  73959423  73400320    35G Linux filesystem
/dev/sda5   73959424 168331263  94371840    45G Linux filesystem
/dev/sda6  168331264 584841215 416509952 198.6G Linux filesystem

Disk /dev/mapper/nova--vol-nova_fake: 1 GiB, 1073741824 bytes, 2097152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/lvm_root-root: 45 GiB, 48314187776 bytes, 94363648 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/lvm_lvp-lvp: 35 GiB, 37576769536 bytes, 73392128 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```