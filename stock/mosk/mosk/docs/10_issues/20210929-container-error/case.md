Hello, I am trying to use Object storage.
With Horizon, I get errors "Error: Unable to fetch the policy details.", "Error: Unable to get the Swift container listing.".

With Opensatck Cli, I had got error :
```
Unable to establish connection to https://object-store.openstack.imt-atlantique.fr/swift/v1/AUTH_e79d3b06822f4d578ba23d4f4c210066: HTTPSConnectionPool(host='object-store.openstack.imt-atlantique.fr', port=443): Max retries exceeded with url: /swift/v1/AUTH_e79d3b06822f4d578ba23d4f4c210066?format=json (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7f28e5d961c0>: Failed to establish a new connection: [Errno -2] Nom ou service inconnu'))
```
because ther was no DNS entry for "object-store.openstack.imt-atlantique.fr"

I have ask for a DNS updtae. But now I have this message 
```
openstack -vv container list
  ...
RESP BODY: {"Code":"NoSuchBucket","BucketName":"rook-ceph-rgw-object-store.rook-ceph.svc","RequestId":"tx000000000000000002ed2-006154aaf2-29916b-object-store","HostId":"29916b-object-store-object-store"}
Request returned failure status: 404
Unrecognized schema in response body. (HTTP 404)
```
For Horizon, nothing has change.

Maybe something was not initialized because of this issue with public endpoint ?
Did you test it before?
Do I open a case for it ?