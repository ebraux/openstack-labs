

:~/MCC2.9/kaas-bootstrap/br-mosk001/cmp-04$ kmosk create -f br-mosk001-cmp-04_machine.yaml
Error from server: error when creating "br-mosk001-cmp-04_machine.yaml": admission webhook "limits.kaas.mirantis.com" denied the request: Maximum number (15) of worker machines in cluster allowed by your license is reached


kubectl  get  bmh 
NAME       STATE         CONSUMER   BOOTMODE   ONLINE   ERROR   REGION
master-0   provisioned   master-0   UEFI       true             region-one
master-1   provisioned   master-1   UEFI       true             region-one
master-2   provisioned   master-2   UEFI       true             region-one
--> 3 pour MCC2

mcc-user@kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0:~/MCC2.9/kaas-bootstrap/br-mosk001/cmp-04$ kmosk get machine
NAME                  PROVIDERID   PHASE
br-mosk001-master-0                
br-mosk001-master-1                
br-mosk001-master-2   
--> 3 pour le master MOSK

br-mosk001-ceph-0                  
br-mosk001-ceph-1                  
br-mosk001-ceph-2                  
--> 3 pour CEPH

br-mosk001-cmp-0                   
br-mosk001-cmp-03                  
br-mosk001-cmp-04                  
br-mosk001-cmp-05                  
br-mosk001-cmp-06                  
br-mosk001-cmp-07                  
br-mosk001-cmp-08                  
br-mosk001-cmp-09                  
br-mosk001-cmp-1                   
br-mosk001-cmp-11                  
br-mosk001-cmp-12                  
br-mosk001-cmp-20                  
--> 12 compute

Le message d'erreur indique 15 compute max. Ca comprendrit donc les noeuds COMPUTE, et les noeud CEPh --> 15
Et si on joute les 3 Noued de MCC et les 3 Noeud du master MOSK, on arrive à 24, donc >25 comme indiqué dans le devis.
Tu m'avais dit pas de problème pour déployer tous mes noeuds; Mais là je suis bloqué.