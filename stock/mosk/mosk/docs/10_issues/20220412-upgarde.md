
MCC
kubectl get po  -A | grep lcm-controller
kaas                     lcm-lcm-controller-6d748445df-s9p6p                               1/1     Running                 0          35d
kaas                     lcm-lcm-controller-6d748445df-vnk62                               1/1     Running                 0          35d
kaas                     lcm-lcm-controller-6d748445df-wtlrx                               1/1     Running                 0      

kubectl -n rook-ceph logs rook-ceph-osd-11-6b6b48fc48-74xqx > /tmp/rook-ceph-osd-11-6b6b48fc48-74xqx.logs

kubectl -n kaas logs lcm-lcm-controller-6d748445df-s9p6p > lcm-lcm-controller-6d748445df-s9p6p.log
kubectl -n kaas logs lcm-lcm-controller-6d748445df-vnk62 > lcm-lcm-controller-6d748445df-vnk62.log
kubectl -n kaas logs lcm-lcm-controller-6d748445df-wtlrx > lcm-lcm-controller-6d748445df-wtlrx.log

MOSK

kubectl get po  -A | grep ceph-controller
ceph-lcm-mirantis        ceph-controller-7767f5b757-4xgzs                                  1/1     Running     0          16h
ceph-lcm-mirantis        ceph-controller-7767f5b757-4z5n5                                  1/1     Running     2          5d20h
ceph-lcm-mirantis        ceph-controller-7767f5b757-jkrkz                                  1/1     Running     1          5d20h
kubectl -n  ceph-lcm-mirantis logs ceph-controller-7767f5b757-4xgzs > ceph-controller-7767f5b757-4xgzs.log
kubectl -n  ceph-lcm-mirantis logs ceph-controller-7767f5b757-4z5n5 > ceph-controller-7767f5b757-4z5n5.log
kubectl -n  ceph-lcm-mirantis logs ceph-controller-7767f5b757-jkrkz > ceph-controller-7767f5b757-jkrkz.log

kubectl get po  -A | grep openstack-controller
osh-system               openstack-controller-54f56c8996-x472w                             7/7     Running     20         19h
osh-system               openstack-controller-admission-6fd959cfc6-9wmss                   1/1     Running     0          19h
kubectl -n osh-system logs openstack-controller-54f56c8996-x472w > openstack-controller-54f56c8996-x472w.log
 osdpl
 secrets
 health
 node
 nodemaintenancerequest
 ceph-secrets
 osdplstatus
kubectl -n osh-system logs openstack-controller-54f56c8996-x472w -c osdpl 


kubectl -n osh-system logs  openstack-controller-admission-6fd959cfc6-9wmss >  openstack-controller-admission-6fd959cfc6-9wmss.log
