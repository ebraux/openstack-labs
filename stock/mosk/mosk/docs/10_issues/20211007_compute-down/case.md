



```bash
openstack compute service list --host kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea
+----+--------------+------------------------------------------------+------+---------+-------+----------------------------+
| ID | Binary       | Host                                           | Zone | Status  | State | Updated At                 |
+----+--------------+------------------------------------------------+------+---------+-------+----------------------------+
| 52 | nova-compute | kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea | nova | enabled | down  | 2021-10-08T11:38:41.000000 |
+----+--------------+------------------------------------------------+------+---------+-------+----------------------------+
```


openstack network agent list --host kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host                                           | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+
| 35fa51a2-a8f4-43a9-b1ab-5e035f83d5b5 | Open vSwitch agent | kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea | None              | XXX   | UP    | neutron-openvswitch-agent |
| 87296f4d-55bb-4c60-bbde-8a08e271f3ff | L3 agent           | kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea | nova              | XXX   | UP    | neutron-l3-agent          |
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+


@
Sorry for the delay.
I was mistaken. All the command I done were with a kubeconfig for "kaas".
I have used Lens, and I have seen the Openstack namespace. .
And so, I have configure kubcetl to interract with the mosk cluster.
Ans know, I get some results :

kubectl -n openstack get pods -o wide | grep kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea
image-precaching-0-pkmtk                                       30/30   Running     0          17h     10.233.99.207    kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
libvirt-libvirt-default-rmlx6                                  2/2     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
neutron-l3-agent-8046e21058b59d30-mzl6t                        1/1     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
neutron-netns-cleanup-cron-default-mprl8                       1/1     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
neutron-ovs-agent-default-ktdwf                                1/1     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
nova-compute-default-4vcf4                                     2/2     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
openvswitch-openvswitch-db-default-4xxlb                       1/1     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
openvswitch-openvswitch-vswitchd-default-fxjv9                 1/1     Running     0          17h     10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>



Logs :

``` log
2021-10-10 21:58:18.775 18117 WARNING oslo_config.cfg [req-79cec5c0-4419-4331-a8ed-37ebc451d793 - - - - -] Deprecated: Option "cpu_model" from group "libvirt" is deprecated. Use option "cpu_models" from group "libvirt".
2021-10-10 21:58:18.778 18117 WARNING oslo_config.cfg [req-79cec5c0-4419-4331-a8ed-37ebc451d793 - - - - -] Deprecated: Option "notify_on_state_change" from group "DEFAULT" is deprecated. Use option "notify_on_state_change" from group "notifications".
2021-10-10 21:58:18.782 18117 WARNING oslo_config.cfg [req-79cec5c0-4419-4331-a8ed-37ebc451d793 - - - - -] Deprecated: Option "vncserver_listen" from group "vnc" is deprecated. Use option "server_listen" from group "vnc".
2021-10-10 21:58:18.783 18117 WARNING oslo_config.cfg [req-79cec5c0-4419-4331-a8ed-37ebc451d793 - - - - -] Deprecated: Option "vncserver_proxyclient_address" from group "vnc" is deprecated. Use option "server_proxyclient_address" from group "vnc".
2021-10-10 21:58:18.787 18117 INFO nova.service [-] Starting compute node (version 21.2.2)
2021-10-10 21:58:18.801 18117 INFO nova.virt.libvirt.driver [-] Connection event '1' reason 'None'
2021-10-10 21:58:19.031 18117 WARNING nova.virt.libvirt.driver [req-27779863-2a03-4288-b09b-cfe1202799c4 - - - - -] An error occurred while updating compute node resource provider status to "enabled" for provider: 94587960-24ac-4e9b-8da3-96b343713890: ValueError: No such provider 94587960-24ac-4e9b-8da3-96b343713890
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver Traceback (most recent call last):
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/virt/libvirt/driver.py", line 4437, in _update_compute_provider_status
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     context, rp_uuid, enabled=not service.disabled)
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/compute/manager.py", line 557, in update_compute_provider_status
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     context, rp_uuid, new_traits)
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/scheduler/client/report.py", line 72, in wrapper
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     return f(self, *a, **k)
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/scheduler/client/report.py", line 1021, in set_traits_for_provider
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     if not self._provider_tree.have_traits_changed(rp_uuid, traits):
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/compute/provider_tree.py", line 584, in have_traits_changed
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     provider = self._find_with_lock(name_or_uuid)
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver   File "/var/lib/openstack/lib/python3.6/site-packages/nova/compute/provider_tree.py", line 440, in _find_with_lock
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver     raise ValueError(_("No such provider %s") % name_or_uuid)
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver ValueError: No such provider 94587960-24ac-4e9b-8da3-96b343713890
2021-10-10 21:58:19.031 18117 ERROR nova.virt.libvirt.driver 
2021-10-10 21:58:19.892 18117 INFO nova.virt.libvirt.host [-] Libvirt host capabilities <capabilities>

```