Business impact:
  Almost all the services running on the MOS cluster can not be used

Details of the problem:
The VM are very slow, on all the compute.
I have tried to add a new fresh compute, and run an Ubuntu instance on it.
When I try to install a new package it is very slow.

On an guest instance, With the `top` command, I can see on the instance that "wa" is  big (IOWAIT)

On the host CPU and Disk access not saturated. But the load average is between 8 and 10.


br-mosk001-cmp-02     kaas-br-mosk001   worker    Ready   10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826
New one : 
openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826 --nic net-id=82393655-ab44-423e-a79f-a709c6c80140 testnewone  

openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8 --nic net-id=82393655-ab44-423e-a79f-a709c6c80140 test-lvm



[libvirt]
connection_uri = qemu+tcp://127.0.0.1/system
cpu_mode = custom
cpu_model = kvm64
disk_cachemodes = network=writeback
hw_disk_discard = unmap
images_rbd_ceph_conf = /etc/ceph/ceph.conf
images_rbd_pool = vms-hdd
images_type = lvm
images_volume_group = nova-vol
live_migration_use_ip_to_scp_configdrive = true
rbd_secret_uuid = 457eb676-33da-42ec-9a8c-9293d545c337
rbd_user = nova
virt_type = kvm


au lancement:
```bash
$ sudo su -
root@kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8:~# du -sh  /var/lib/nova/instances/*
1.4G	/var/lib/nova/instances/_base
4.0K	/var/lib/nova/instances/compute_nodes
60K	/var/lib/nova/instances/ebb0ddfd-5a19-4dbf-adc9-1a43a3613bb3
4.0K	/var/lib/nova/instances/locks
root@kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8:~# tree /var/lib/nova/instances
/var/lib/nova/instances
├── _base
│   └── 81fe2af3c08afb240abc955c0dc99b4e0be9c475
├── compute_nodes
├── ebb0ddfd-5a19-4dbf-adc9-1a43a3613bb3
│   └── console.log
└── locks
    ├── nova-81fe2af3c08afb240abc955c0dc99b4e0be9c475
    └── nova-storage-registry-lock

3 directories, 5 files
root@kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8:~# pvs
  PV         VG       Fmt  Attr PSize    PFree   
  /dev/sda4  lvm_lvp  lvm2 a--   <35.00g       0 
  /dev/sda5  lvm_root lvm2 a--   <55.00g       0 
  /dev/sda6  nova-vol lvm2 a--  <188.61g <184.61g
```
après updates

```bash
du -sh  /var/lib/nova/instances/*
1.4G	/var/lib/nova/instances/_base
4.0K	/var/lib/nova/instances/compute_nodes
64K	/var/lib/nova/instances/ebb0ddfd-5a19-4dbf-adc9-1a43a3613bb3
4.0K	/var/lib/nova/instances/locks
root@kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8:~# tree /var/lib/nova/instances
/var/lib/nova/instances
├── _base
│   └── 81fe2af3c08afb240abc955c0dc99b4e0be9c475
├── compute_nodes
├── ebb0ddfd-5a19-4dbf-adc9-1a43a3613bb3
│   └── console.log
└── locks
    ├── nova-81fe2af3c08afb240abc955c0dc99b4e0be9c475
    └── nova-storage-registry-lock

3 directories, 5 files
root@kaas-node-72272f86-cc3b-41fc-a1a9-38ef2e756df8:~# pvs
  PV         VG       Fmt  Attr PSize    PFree   
  /dev/sda4  lvm_lvp  lvm2 a--   <35.00g       0 
  /dev/sda5  lvm_root lvm2 a--   <55.00g       0 
  /dev/sda6  nova-vol lvm2 a--  <188.61g <184.61g

```




ssh ubuntu@10.129.177.242

```
root@testnewone:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 17.1275 s, 62.7 MB/s
```

En direct sur le compute
```
root@kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 0.702882 s, 1.5 GB/s
```


```
root@testlasone:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 1.1458 s, 937 MB/s
```

```
root@testlasone:~# time apt install -y  apache2 
real	0m10.085s
user	0m4.774s
sys	0m1.525s
```



```
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 15
model		: 6
model name	: Common KVM processor
stepping	: 1
microcode	: 0x1
cpu MHz		: 2095.074
cache size	: 16384 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 syscall nx lm constant_tsc nopl xtopology cpuid tsc_known_freq pni cx16 x2apic hypervisor cpuid_fault pti
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit
bogomips	: 4190.14
clflush size	: 64
cache_alignment	: 128
address sizes	: 40 bits physical, 48 bits virtual
power management:

```


# br-mosk001-cmp-0  10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c 
cmp-0  ceph
openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c --nic net-id=82393655-ab44-423e-a79f-a709c6c80140 test-cmp-ceph 

openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-3ca03bf6-8687-4927-b841-36701c17b269	--nic net-id=82393655-ab44-423e-a79f-a709c6c80140 test-cmp-local 

ssh ubuntu@10.129.176.122

```bash
root@test-cmp-ceph:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 14.6895 s, 73.1 MB/s
```

Apache2 install :
```
real	4m54.426s
user	0m34.714s
sys	0m14.269s
```

```
root@test-cmp-ceph:~# sudo  cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 15
model		: 6
model name	: Common KVM processor
stepping	: 1
microcode	: 0x1
cpu MHz		: 2095.078
cache size	: 16384 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 syscall nx lm constant_tsc nopl xtopology cpuid tsc_known_freq pni cx16 x2apic hypervisor cpuid_fault pti
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit
bogomips	: 4190.15
clflush size	: 64
cache_alignment	: 128
address sizes	: 40 bits physical, 48 bits virtual
power management:
```

Load average of the node : about 8.



br-mosk001-cmp-05   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd  
openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd --nic net-id=82393655-ab44-423e-a79f-a709c6c80140 test-cmp-lvm-full

ssh ubuntu@10.129.176.146

```
root@test-cmp-lvm-full:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 16.6668 s, 64.4 MB/s
```

```
root@test-cmp-lvm-full:~# cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 15
model		: 6
model name	: Common KVM processor
stepping	: 1
microcode	: 0x1
cpu MHz		: 2294.606
cache size	: 16384 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 syscall nx lm constant_tsc nopl xtopology cpuid tsc_known_freq pni cx16 x2apic hypervisor cpuid_fault pti
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit
bogomips	: 4589.21
clflush size	: 64
cache_alignment	: 128
address sizes	: 40 bits physical, 48 bits virtual
power management:

```




kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826

echo 'Acquire::http::Proxy "http://apt-cacher-01.priv.enst-bretagne.fr:3142";' > /etc/apt/apt.conf.d/01proxy

dd if=/dev/zero of=./1G bs=1M count=1024

cat /proc/cpuinfo



---

for exemeple, for node  kaas-node-9b49e515-67c3-45f6-9d2c-df63a2dd804e : 

```
    kubernetes.io/hostname::kaas-node-9b49e515-67c3-45f6-9d2c-df63a2dd804e:
        services:
          compute:
            nova:
              nova_compute:
                values:
                  conf:
                    nova:
                      libvirt:
                        cpu_mode: host-passthrough
                        cpu_model: <None>
```

By the way, I have found in that section the lvm configuration for the 2 nodes that where deployed with LVM, for using volume group nova-vol.


I have tried the test with the same kind of server, in my old openstack infra :
apache2 infra = 16 seconds
```
root@test-cpu:~# time apt install -y apache2

 ...real	0m16.666s
user	0m7.434s
sys	0m1.756s
```

Disk acces x20
```
root@test-cpu:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 1.08367 s, 991 MB/s
```


```
root@test-cpu:~# cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel Xeon Processor (Skylake, IBRS)
stepping	: 4
microcode	: 0x1
cpu MHz		: 2294.608
cache size	: 16384 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq vmx ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti ssbd ibrs ibpb tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves arat pku ospke md_clear
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa itlb_multihit
bogomips	: 4589.21
clflush size	: 64
cache_alignment	: 64
address sizes	: 40 bits physical, 48 bits virtual
power management:
```

And there is 11 other instances runningon this compute, for reserach. and 58 cpu allocated for 48 available.