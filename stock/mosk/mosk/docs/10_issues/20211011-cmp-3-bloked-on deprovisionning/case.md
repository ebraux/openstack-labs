
Details of the problem:
I have tried to delete a compute from MOSK.
 - I have donne th openstack part 
 - I have deleted the "machine"
But now I can not  delete the "BareMetalHost". The delete command never end.


As mentionned, the compute has been deleted from openstack and the machine has been deleted
But is blocked on STATE  deprovisioning.
So ther is no more <compute node name> or <node ID> in Openstack
Only the bmh name "br-mosk001-cmp-3" 

$ kmosk get bmh
NAME                  STATE            CONSUMER              BOOTMODE   ONLINE   ERROR              REGION
br-mosk001-ceph-0     provisioned      br-mosk001-ceph-0     UEFI       true                        region-one
br-mosk001-ceph-1     provisioned      br-mosk001-ceph-1     UEFI       true                        region-one
br-mosk001-ceph-2     provisioned      br-mosk001-ceph-2     UEFI       true                        region-one
br-mosk001-cmp-0      provisioned      br-mosk001-cmp-0      UEFI       true                        region-one
br-mosk001-cmp-05     provisioned      br-mosk001-cmp-05     UEFI       true                        region-one
br-mosk001-cmp-06     provisioned      br-mosk001-cmp-06     UEFI       true                        region-one
br-mosk001-cmp-09     provisioned      br-mosk001-cmp-09     UEFI       true                        region-one
br-mosk001-cmp-1      provisioned      br-mosk001-cmp-1      UEFI       true                        region-one
br-mosk001-cmp-10     inspecting                             UEFI       true     inspection error   region-one
br-mosk001-cmp-11     inspecting                             UEFI       true     inspection error   region-one
br-mosk001-cmp-12     inspecting                             UEFI       true     inspection error   region-one
br-mosk001-cmp-2      provisioned      br-mosk001-cmp-2      UEFI       true                        region-one
br-mosk001-cmp-20     provisioned      br-mosk001-cmp-20     UEFI       true                        region-one
br-mosk001-cmp-3      deprovisioning                         UEFI       false                       region-one
br-mosk001-cmp-7      inspecting       br-mosk001-cmp-7      UEFI       true                        region-one
br-mosk001-cmp-8      provisioned      br-mosk001-cmp-8      UEFI       true                        region-one
br-mosk001-master-0   provisioned      br-mosk001-master-0   UEFI       true                        region-one
br-mosk001-master-1   provisioned      br-mosk001-master-1   UEFI       true                        region-one
br-mosk001-master-2   provisioned      br-mosk001-master-2   UEFI       true                        region-one



kmosk get machine
NAME                  PROVIDERID   PHASE
br-mosk001-ceph-0                  
br-mosk001-ceph-1                  
br-mosk001-ceph-2                  
br-mosk001-cmp-0                   
br-mosk001-cmp-05                  
br-mosk001-cmp-06                  
br-mosk001-cmp-09                  
br-mosk001-cmp-1                   
br-mosk001-cmp-11                  
br-mosk001-cmp-12                  
br-mosk001-cmp-2                   
br-mosk001-cmp-20                  
br-mosk001-cmp-7                   
br-mosk001-cmp-8                   
br-mosk001-master-0                
br-mosk001-master-1                
br-mosk001-master-2                


kmosk get lcmmachine -o wide
NAME                  CLUSTERNAME       TYPE      STATE     INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001-ceph-0     kaas-br-mosk001   worker    Prepare   10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.2.0-349-g4870b7f5
br-mosk001-ceph-1     kaas-br-mosk001   worker    Prepare   10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.2.0-349-g4870b7f5
br-mosk001-ceph-2     kaas-br-mosk001   worker    Prepare   10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.2.0-349-g4870b7f5
br-mosk001-cmp-0      kaas-br-mosk001   worker    Prepare   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.2.0-349-g4870b7f5
br-mosk001-cmp-05     kaas-br-mosk001   worker    Prepare   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   v0.2.0-349-g4870b7f5
br-mosk001-cmp-06     kaas-br-mosk001   worker    Prepare   10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   v0.2.0-349-g4870b7f5
br-mosk001-cmp-09     kaas-br-mosk001   worker    Prepare   10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   v0.2.0-349-g4870b7f5
br-mosk001-cmp-1      kaas-br-mosk001   worker    Prepare   10.129.172.110   kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302   v0.2.0-349-g4870b7f5
br-mosk001-cmp-2      kaas-br-mosk001   worker    Prepare   10.129.172.111   kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577   v0.2.0-349-g4870b7f5
br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready     10.129.172.112   kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   v0.2.0-349-g4870b7f5
br-mosk001-cmp-8      kaas-br-mosk001   worker                                                                                
br-mosk001-master-0   kaas-br-mosk001   control   Prepare   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.2.0-349-g4870b7f5
br-mosk001-master-1   kaas-br-mosk001   control   Ready     10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.2.0-349-g4870b7f5
br-mosk001-master-2   kaas-br-mosk001   control   Ready     10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.2.0-349-g4870b7f5




$ openstack hypervisor list
+----+-------------------------------------------------------------------------------------------------+-----------------+----------------+-------+
| ID | Hypervisor Hostname                                                                             | Hypervisor Type | Host IP        | State |
+----+-------------------------------------------------------------------------------------------------+-----------------+----------------+-------+
|  1 | kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.16 | up    |
|  4 | kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.17 | up    |
|  7 | kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.18 | up    |
| 16 | kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.22 | up    |
| 19 | kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.20 | up    |
| 22 | kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.23 | up    |
| 23 | kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163.kaas-kubernetes-51fa68d14d894cae9f38f55f7d67229d | QEMU            | 192.168.179.24 | up    |
+----+-------------------------------------------------------------------------------------------------+-----------------+----------------+-------+


---
kubectl -n kaas logs --follow=true baremetal-operator-75cc95855b-86tjj | grep cmp-3
```bash
{"level":"info","ts":1634072447.7370071,"logger":"controllers.BareMetalHost","msg":"start","baremetalhost":"br-mosk001/br-mosk001-cmp-3"}
{"level":"info","ts":1634072447.9513342,"logger":"controllers.BareMetalHost","msg":"registering and validating access to management controller","baremetalhost":"br-mosk001/br-mosk001-cmp-3","provisioningState":"deprovisioning","credentials":{"credentials":{"name":"br-mosk001-cmp-3-bmc-secret","namespace":"br-mosk001"},"credentialsVersion":"95822890"}}
{"level":"info","ts":1634072448.029032,"logger":"provisioner.ironic","msg":"updating option data","host":"br-mosk001~br-mosk001-cmp-3","option":"instance_uuid","value":"1f5686c4-5a17-4630-8999-fdf53e01630b","old_value":""}
{"level":"info","ts":1634072448.0290818,"logger":"provisioner.ironic","msg":"adding option data","host":"br-mosk001~br-mosk001-cmp-3","option":"capabilities","section":"instance_info","value":{}}
{"level":"info","ts":1634072448.030106,"logger":"provisioner.ironic","msg":"adding option data","host":"br-mosk001~br-mosk001-cmp-3","option":"image_source","section":"instance_info","value":"http://httpd-http/images/stub_image.qcow2"}
{"level":"info","ts":1634072448.0301402,"logger":"provisioner.ironic","msg":"adding option data","host":"br-mosk001~br-mosk001-cmp-3","option":"image_os_hash_algo","section":"instance_info","value":"md5"}
{"level":"info","ts":1634072448.0301528,"logger":"provisioner.ironic","msg":"adding option data","host":"br-mosk001~br-mosk001-cmp-3","option":"image_os_hash_value","section":"instance_info","value":"http://httpd-http/images/stub_image.qcow2.md5sum"}
{"level":"info","ts":1634072448.0301664,"logger":"provisioner.ironic","msg":"adding option data","host":"br-mosk001~br-mosk001-cmp-3","option":"image_checksum","section":"instance_info","value":"http://httpd-http/images/stub_image.qcow2.md5sum"}
{"level":"info","ts":1634072448.0301857,"logger":"provisioner.ironic","msg":"updating deploy interface","host":"br-mosk001~br-mosk001-cmp-3","current":"ansible","new":"ansible"}
{"level":"info","ts":1634072448.0302129,"logger":"provisioner.ironic","msg":"updating node settings in ironic","host":"br-mosk001~br-mosk001-cmp-3"}
{"level":"info","ts":1634072448.0576365,"logger":"provisioner.ironic","msg":"could not update node settings in ironic, busy","host":"br-mosk001~br-mosk001-cmp-3"}
{"level":"info","ts":1634072448.05768,"logger":"controllers.BareMetalHost","msg":"host not ready","baremetalhost":"br-mosk001/br-mosk001-cmp-3","provisioningState":"deprovisioning","wait":10}
{"level":"info","ts":1634072448.0576952,"logger":"controllers.BareMetalHost","msg":"done","baremetalhost":"br-mosk001/br-mosk001-cmp-3","provisioningState":"deprovisioning","requeue":true,"after":10}
```
$ kubectl -n kaas logs ironic-56676c774b-bfcjs syslog  | grep 1f5686c4-5a17-4630-8999-fdf53e01630b
```bash
{"source":"ironic_conductor","program":"ironic-conductor","priority":"info","pid":"","message":"2021-10-12 15:33:44.515 1 INFO ironic.conductor.manager [req-9378f398-aed2-4284-9ea1-c8e8ade15f59 - - - - -] Successfully unprovisioned node 7ee85f27-32da-4419-bf89-51012c357811 with instance 1f5686c4-5a17-4630-8999-fdf53e01630b.","host":"ironic-56676c774b-bfcjs","facility":"user","date":"2021-10-12T15:33:44+00:00"}
{"source":"ironic_conductor","program":"ironic-conductor","priority":"info","pid":"","message":"2021-10-12 16:04:44.615 1 INFO ironic.conductor.manager [req-1897d869-9b52-42e8-a23f-e5e218ced9a5 - - - - -] Successfully unprovisioned node 7ee85f27-32da-4419-bf89-51012c357811 with instance 1f5686c4-5a17-4630-8999-fdf53e01630b.","host":"ironic-56676c774b-bfcjs","facility":"user","date":"2021-10-12T16:04:44+00:00"}

```
