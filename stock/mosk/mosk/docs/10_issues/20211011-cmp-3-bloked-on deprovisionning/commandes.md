


+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name                           | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| 76a2c8f2-1fee-49ba-9454-ec90faf993a4 | master-0                       | 9c081499-eed8-4cf7-aa9b-292e25b917c7 | power on    | active             | False       |
| 48ec3e60-fb9b-4e7e-99a5-68a5c4469bed | master-1                       | 47b3dccd-f540-4f4e-bb04-d170c59ac948 | power on    | active             | False       |
| 65628469-8845-4098-9d11-a56d5f363610 | master-2                       | 38b24c3b-eb62-4034-8507-9136b6e1e1ff | power on    | active             | False       |
| e28f47b9-3eb4-42e6-9da9-351d94021503 | br-mosk001~br-mosk001-master-0 | 02a1b0ca-0519-478a-864b-8c3f83d1b6b9 | power on    | active             | False       |
| 765f30ea-d859-4f6c-a397-dcccf7782a58 | br-mosk001~br-mosk001-master-1 | 12c12bd4-a2bf-47de-88c0-0ba66c6b10fd | power on    | active             | False       |
| be39e92f-cec5-4e10-af6e-6bbbc6075b9d | br-mosk001~br-mosk001-master-2 | e9cf7c22-f323-4fa1-9a84-0ed053e44b71 | power on    | active             | False       |

| 13d9f60a-f2cc-4d53-b9b9-bd4767e8c318 | br-mosk001~br-mosk001-ceph-0   | 270911cc-4af7-4bd3-a291-c4a94377c2aa | power on    | active             | False       |
| ce8f1b0e-11fb-4ca4-bcfe-d4b0fe57c8d4 | br-mosk001~br-mosk001-ceph-1   | 8898d06f-8758-497e-92bf-055e8d4170d9 | power on    | active             | False       |
| 23becd07-a95e-4bae-97f0-f6d3d13b4dc2 | br-mosk001~br-mosk001-ceph-2   | 1cb2c52a-5853-46b7-b9a2-d06ff065cbb6 | power on    | active             | False       |

| f18ae5f6-f849-4ed3-899f-86da2e27a292 | br-mosk001~br-mosk001-cmp-0    | 744e435a-d3b8-4d73-a90f-a3ef3c5f6d06 | power on    | active             | False       |
| 99dee5cc-ef7d-4b09-887a-77ef4a829c07 | br-mosk001~br-mosk001-cmp-05   | e7ee8051-577e-4ddd-b215-1a1e850eb487 | power on    | active             | False       |
| ed873dc5-ba91-41a4-be70-aff576474746 | br-mosk001~br-mosk001-cmp-06   | 3432b2c2-6539-4194-944a-093becf90691 | power on    | active             | False       |
| b4a63a97-1eb6-4b63-8bef-3711ee53d6d7 | br-mosk001~br-mosk001-cmp-07   | 55375a0d-6627-4c2f-942f-230b8a1b7465 | power on    | active             | False       |
| 69349686-65a5-4286-8554-858e9cc0e8ec | br-mosk001~br-mosk001-cmp-09   | 77d905a3-8322-44c9-91c4-4ffcd759098f | power on    | active             | False       |
| dce9ea7a-7aaf-4161-934c-a0faf7242da1 | br-mosk001~br-mosk001-cmp-14   | 597e762c-30da-462a-ac09-0a64b5636f27 | power on    | active             | False       |

| 13dc8a9a-9bf0-4907-ab34-f6aaea0fbff1 | br-mosk001~br-mosk001-cmp-1    | 27e8080d-2ce8-4dd2-90da-e04e9d0f9f75 | power off   | available          | False       |
| d7c3c2c5-2dda-48c3-be02-dab6740c15c1 | br-mosk001~br-mosk001-cmp-2    | 230ecd85-a1f8-4254-a004-e88292a2d19a | power on    | active             | False       |
| 7ee85f27-32da-4419-bf89-51012c357811 | br-mosk001~br-mosk001-cmp-3    | None                                 | power off   | clean failed       | False       |
| e69f379d-f265-4d97-a5f4-75ab38495b2d | br-mosk001~br-mosk001-cmp-12   | None                                 | power on    | inspect failed     | False       |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+


The compute are not in the lcmmachine list.
```$ kmosk get lcmmachines -o  wide
NAME                  CLUSTERNAME       TYPE      STATE   INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001-master-0   kaas-br-mosk001   control   Ready   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.2.0-349-g4870b7f5
br-mosk001-master-1   kaas-br-mosk001   control   Ready   10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.2.0-349-g4870b7f5
br-mosk001-master-2   kaas-br-mosk001   control   Ready   10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.2.0-349-g4870b7f5
br-mosk001-ceph-0     kaas-br-mosk001   worker    Ready   10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.2.0-349-g4870b7f5
br-mosk001-ceph-1     kaas-br-mosk001   worker    Ready   10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.2.0-349-g4870b7f5
br-mosk001-ceph-2     kaas-br-mosk001   worker    Ready   10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.2.0-349-g4870b7f5
br-mosk001-cmp-0      kaas-br-mosk001   worker    Ready   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.2.0-349-g4870b7f5
br-mosk001-cmp-05     kaas-br-mosk001   worker    Ready   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   v0.2.0-349-g4870b7f5
br-mosk001-cmp-06     kaas-br-mosk001   worker    Ready   10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   v0.2.0-349-g4870b7f5
br-mosk001-cmp-07     kaas-br-mosk001   worker    Ready   10.129.172.117   kaas-node-38538401-efd4-4ddc-a328-7666724b917f   v0.2.0-349-g4870b7f5
br-mosk001-cmp-09     kaas-br-mosk001   worker    Ready   10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   v0.2.0-349-g4870b7f5
br-mosk001-cmp-14     kaas-br-mosk001   worker    Ready   10.129.172.111   kaas-node-d2144611-3110-417f-8d20-ac38b6ddbdbf   v0.2.0-349-g4870b7f5
br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready   10.129.172.112   kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   v0.2.0-349-g4870b7f5
```

Not in the managed cluster nodes list :

```$ kmosk get nodes
NAME                                             STATUS   ROLES    AGE    VERSION
kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   Ready    <none>   40d    v1.18.14-mirantis-1
kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   Ready    <none>   40d    v1.18.14-mirantis-1
kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   Ready    <none>   40d    v1.18.14-mirantis-1
kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   Ready    <none>   40d    v1.18.14-mirantis-1
kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   Ready    <none>   17d    v1.18.14-mirantis-1
kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   Ready    <none>   17d    v1.18.14-mirantis-1
kaas-node-38538401-efd4-4ddc-a328-7666724b917f   Ready    <none>   12d    v1.18.14-mirantis-1
kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   Ready    <none>   17d    v1.18.14-mirantis-1
kaas-node-d2144611-3110-417f-8d20-ac38b6ddbdbf   Ready    <none>   3d5h   v1.18.14-mirantis-1
kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   Ready    <none>   13d    v1.18.14-mirantis-1
kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   Ready    master   40d    v1.18.14-mirantis-1
kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   Ready    master   40d    v1.18.14-mirantis-1
kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   Ready    master   40d    v1.18.14-mirantis-1
```

The 2 nodes "br-mosk001-cmp-2" and "br-mosk001-cmp-3" was not using CEPH
I have also have removed an other node, that was using Ceph : "br-mosk001-cmp-1"



```kmosk get bmh
NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR                REGION
br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true                          region-one
br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true                          region-one
br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true                          region-one
br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true                          region-one
br-mosk001-cmp-02     registering                         UEFI       true     registration error   region-one
br-mosk001-cmp-05     provisioned   br-mosk001-cmp-05     UEFI       true                          region-one
br-mosk001-cmp-06     provisioned   br-mosk001-cmp-06     UEFI       true                          region-one
br-mosk001-cmp-07     provisioned   br-mosk001-cmp-07     UEFI       true                          region-one
br-mosk001-cmp-09     provisioned   br-mosk001-cmp-09     UEFI       true                          region-one
br-mosk001-cmp-1      ready                               UEFI       false                         region-one
br-mosk001-cmp-14     provisioned   br-mosk001-cmp-14     UEFI       true                          region-one
br-mosk001-cmp-20     provisioned   br-mosk001-cmp-20     UEFI       true                          region-one
br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true                          region-one
br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true                          region-one
br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true                          region-one
```

```kmosk get ipamhosts
NAME                  STATUS   AGE     REGION
br-mosk001-ceph-0     OK       40d     region-one
br-mosk001-ceph-1     OK       40d     region-one
br-mosk001-ceph-2     OK       40d     region-one
br-mosk001-cmp-0      OK       40d     region-one
br-mosk001-cmp-05     OK       18d     region-one
br-mosk001-cmp-06     OK       18d     region-one
br-mosk001-cmp-07     OK       13d     region-one
br-mosk001-cmp-09     OK       18d     region-one
br-mosk001-cmp-14     OK       5d17h   region-one
br-mosk001-cmp-20     OK       13d     region-one
br-mosk001-master-0   OK       40d     region-one
br-mosk001-master-1   OK       40d     region-one
br-mosk001-master-2   OK       40d     region-one
```

```kmosk  get ipaddrs
NAME                                                  MAC                                                 CIDR                STATUS   AGE
auto-00-22-19-6b-83-12                                00:22:19:6B:83:12                                   10.129.172.114/24   Active   21d
auto-18-66-da-ec-4b-3c                                18:66:DA:EC:4B:3C                                   10.129.172.117/24   Active   13d
auto-18-66-da-ed-2c-3c                                18:66:DA:ED:2C:3C                                   10.129.172.111/24   Active   5d17h
auto-78-ac-44-08-c0-10                                78:AC:44:08:C0:10                                   10.129.172.116/24   Active   18d
auto-78-ac-44-08-c0-18                                78:AC:44:08:C0:18                                   10.129.172.115/24   Active   18d
auto-br-tenant-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   VI:BR-TENANT:17EF5D4D-F896-4F84-BC84-F02E63ED76CD   192.168.179.23/24   Active   18d
auto-br-tenant-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   VI:BR-TENANT:1E389E22-2A10-4B03-9C2D-37255D4F4CA4   192.168.179.11/24   Active   40d
auto-br-tenant-38538401-efd4-4ddc-a328-7666724b917f   VI:BR-TENANT:38538401-EFD4-4DDC-A328-7666724B917F   192.168.179.25/24   Active   13d
auto-br-tenant-5d555e1d-0636-45a5-868c-b0a3350efe6f   VI:BR-TENANT:5D555E1D-0636-45A5-868C-B0A3350EFE6F   192.168.179.13/24   Active   40d
auto-br-tenant-719fafd5-4d68-4c88-8c0e-8364cba48059   VI:BR-TENANT:719FAFD5-4D68-4C88-8C0E-8364CBA48059   192.168.179.20/24   Active   18d
auto-br-tenant-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   VI:BR-TENANT:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB   192.168.179.12/24   Active   40d
auto-br-tenant-8a7273c8-f2da-4e34-a088-e76f90fa8504   VI:BR-TENANT:8A7273C8-F2DA-4E34-A088-E76F90FA8504   192.168.179.15/24   Active   40d
auto-br-tenant-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0   VI:BR-TENANT:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0   192.168.179.21/24   Active   21d
auto-br-tenant-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   VI:BR-TENANT:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C   192.168.179.14/24   Active   40d
auto-br-tenant-a15c808c-d1cd-4347-8ee8-b5d553ca096c   VI:BR-TENANT:A15C808C-D1CD-4347-8EE8-B5D553CA096C   192.168.179.16/24   Active   40d
auto-br-tenant-b10834cf-25a4-40e9-85be-d7be198417fa   VI:BR-TENANT:B10834CF-25A4-40E9-85BE-D7BE198417FA   192.168.179.10/24   Active   40d
auto-br-tenant-d2144611-3110-417f-8d20-ac38b6ddbdbf   VI:BR-TENANT:D2144611-3110-417F-8D20-AC38B6DDBDBF   192.168.179.18/24   Active   5d17h
auto-br-tenant-d29a52d3-5bed-4027-992a-5f94b1089163   VI:BR-TENANT:D29A52D3-5BED-4027-992A-5F94B1089163   192.168.179.24/24   Active   13d
auto-br-tenant-d646e156-80a1-4c0c-a102-11669fd37cea   VI:BR-TENANT:D646E156-80A1-4C0C-A102-11669FD37CEA   192.168.179.22/24   Active   18d
auto-ceph-cl-17ef5d4d-f896-4f84-bc84-f02e63ed76cd     VI:CEPH-CL:17EF5D4D-F896-4F84-BC84-F02E63ED76CD     192.168.176.23/24   Active   18d
auto-ceph-cl-1e389e22-2a10-4b03-9c2d-37255d4f4ca4     VI:CEPH-CL:1E389E22-2A10-4B03-9C2D-37255D4F4CA4     192.168.176.11/24   Active   40d
auto-ceph-cl-38538401-efd4-4ddc-a328-7666724b917f     VI:CEPH-CL:38538401-EFD4-4DDC-A328-7666724B917F     192.168.176.25/24   Active   13d
auto-ceph-cl-5d555e1d-0636-45a5-868c-b0a3350efe6f     VI:CEPH-CL:5D555E1D-0636-45A5-868C-B0A3350EFE6F     192.168.176.13/24   Active   40d
auto-ceph-cl-719fafd5-4d68-4c88-8c0e-8364cba48059     VI:CEPH-CL:719FAFD5-4D68-4C88-8C0E-8364CBA48059     192.168.176.20/24   Active   18d
auto-ceph-cl-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb     VI:CEPH-CL:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB     192.168.176.12/24   Active   40d
auto-ceph-cl-8a7273c8-f2da-4e34-a088-e76f90fa8504     VI:CEPH-CL:8A7273C8-F2DA-4E34-A088-E76F90FA8504     192.168.176.15/24   Active   40d
auto-ceph-cl-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0     VI:CEPH-CL:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0     192.168.176.21/24   Active   21d
auto-ceph-cl-92e4e163-04ba-4890-be3a-ad81f4f8ad8c     VI:CEPH-CL:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C     192.168.176.14/24   Active   40d
auto-ceph-cl-a15c808c-d1cd-4347-8ee8-b5d553ca096c     VI:CEPH-CL:A15C808C-D1CD-4347-8EE8-B5D553CA096C     192.168.176.16/24   Active   40d
auto-ceph-cl-b10834cf-25a4-40e9-85be-d7be198417fa     VI:CEPH-CL:B10834CF-25A4-40E9-85BE-D7BE198417FA     192.168.176.10/24   Active   40d
auto-ceph-cl-d2144611-3110-417f-8d20-ac38b6ddbdbf     VI:CEPH-CL:D2144611-3110-417F-8D20-AC38B6DDBDBF     192.168.176.18/24   Active   5d17h
auto-ceph-cl-d29a52d3-5bed-4027-992a-5f94b1089163     VI:CEPH-CL:D29A52D3-5BED-4027-992A-5F94B1089163     192.168.176.24/24   Active   13d
auto-ceph-cl-d646e156-80a1-4c0c-a102-11669fd37cea     VI:CEPH-CL:D646E156-80A1-4C0C-A102-11669FD37CEA     192.168.176.22/24   Active   18d
auto-ceph-rep-17ef5d4d-f896-4f84-bc84-f02e63ed76cd    VI:CEPH-REP:17EF5D4D-F896-4F84-BC84-F02E63ED76CD    192.168.177.23/24   Active   18d
auto-ceph-rep-1e389e22-2a10-4b03-9c2d-37255d4f4ca4    VI:CEPH-REP:1E389E22-2A10-4B03-9C2D-37255D4F4CA4    192.168.177.11/24   Active   40d
auto-ceph-rep-38538401-efd4-4ddc-a328-7666724b917f    VI:CEPH-REP:38538401-EFD4-4DDC-A328-7666724B917F    192.168.177.25/24   Active   13d
auto-ceph-rep-5d555e1d-0636-45a5-868c-b0a3350efe6f    VI:CEPH-REP:5D555E1D-0636-45A5-868C-B0A3350EFE6F    192.168.177.13/24   Active   40d
auto-ceph-rep-719fafd5-4d68-4c88-8c0e-8364cba48059    VI:CEPH-REP:719FAFD5-4D68-4C88-8C0E-8364CBA48059    192.168.177.20/24   Active   18d
auto-ceph-rep-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb    VI:CEPH-REP:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB    192.168.177.12/24   Active   40d
auto-ceph-rep-8a7273c8-f2da-4e34-a088-e76f90fa8504    VI:CEPH-REP:8A7273C8-F2DA-4E34-A088-E76F90FA8504    192.168.177.15/24   Active   40d
auto-ceph-rep-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0    VI:CEPH-REP:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0    192.168.177.21/24   Active   21d
auto-ceph-rep-92e4e163-04ba-4890-be3a-ad81f4f8ad8c    VI:CEPH-REP:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C    192.168.177.14/24   Active   40d
auto-ceph-rep-a15c808c-d1cd-4347-8ee8-b5d553ca096c    VI:CEPH-REP:A15C808C-D1CD-4347-8EE8-B5D553CA096C    192.168.177.16/24   Active   40d
auto-ceph-rep-b10834cf-25a4-40e9-85be-d7be198417fa    VI:CEPH-REP:B10834CF-25A4-40E9-85BE-D7BE198417FA    192.168.177.10/24   Active   40d
auto-ceph-rep-d2144611-3110-417f-8d20-ac38b6ddbdbf    VI:CEPH-REP:D2144611-3110-417F-8D20-AC38B6DDBDBF    192.168.177.18/24   Active   5d17h
auto-ceph-rep-d29a52d3-5bed-4027-992a-5f94b1089163    VI:CEPH-REP:D29A52D3-5BED-4027-992A-5F94B1089163    192.168.177.24/24   Active   13d
auto-ceph-rep-d646e156-80a1-4c0c-a102-11669fd37cea    VI:CEPH-REP:D646E156-80A1-4C0C-A102-11669FD37CEA    192.168.177.22/24   Active   18d
auto-e4-43-4b-83-10-2c                                E4:43:4B:83:10:2C                                   10.129.172.113/24   Active   18d
auto-e4-43-4b-e9-37-58                                E4:43:4B:E9:37:58                                   10.129.172.112/24   Active   13d
auto-e4-43-4b-ea-67-50                                E4:43:4B:EA:67:50                                   10.129.172.105/24   Active   40d
auto-e4-43-4b-ea-67-70                                E4:43:4B:EA:67:70                                   10.129.172.106/24   Active   40d
auto-e4-43-4b-ea-67-a0                                E4:43:4B:EA:67:A0                                   10.129.172.104/24   Active   40d
auto-e4-43-4b-ea-73-b0                                E4:43:4B:EA:73:B0                                   10.129.172.109/24   Active   40d
auto-e4-43-4b-ea-b2-b0                                E4:43:4B:EA:B2:B0                                   10.129.172.103/24   Active   40d
auto-e4-43-4b-ea-be-80                                E4:43:4B:EA:BE:80                                   10.129.172.107/24   Active   40d
auto-e4-43-4b-ea-c2-b0                                E4:43:4B:EA:C2:B0                                   10.129.172.108/24   Active   40d
auto-k8s-ext-17ef5d4d-f896-4f84-bc84-f02e63ed76cd     VI:K8S-EXT:17EF5D4D-F896-4F84-BC84-F02E63ED76CD     10.129.175.23/24    Active   18d
auto-k8s-ext-1e389e22-2a10-4b03-9c2d-37255d4f4ca4     VI:K8S-EXT:1E389E22-2A10-4B03-9C2D-37255D4F4CA4     10.129.175.11/24    Active   40d
auto-k8s-ext-38538401-efd4-4ddc-a328-7666724b917f     VI:K8S-EXT:38538401-EFD4-4DDC-A328-7666724B917F     10.129.175.25/24    Active   13d
auto-k8s-ext-5d555e1d-0636-45a5-868c-b0a3350efe6f     VI:K8S-EXT:5D555E1D-0636-45A5-868C-B0A3350EFE6F     10.129.175.13/24    Active   40d
auto-k8s-ext-719fafd5-4d68-4c88-8c0e-8364cba48059     VI:K8S-EXT:719FAFD5-4D68-4C88-8C0E-8364CBA48059     10.129.175.20/24    Active   18d
auto-k8s-ext-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb     VI:K8S-EXT:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB     10.129.175.12/24    Active   40d
auto-k8s-ext-8a7273c8-f2da-4e34-a088-e76f90fa8504     VI:K8S-EXT:8A7273C8-F2DA-4E34-A088-E76F90FA8504     10.129.175.15/24    Active   40d
auto-k8s-ext-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0     VI:K8S-EXT:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0     10.129.175.21/24    Active   21d
auto-k8s-ext-92e4e163-04ba-4890-be3a-ad81f4f8ad8c     VI:K8S-EXT:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C     10.129.175.14/24    Active   40d
auto-k8s-ext-a15c808c-d1cd-4347-8ee8-b5d553ca096c     VI:K8S-EXT:A15C808C-D1CD-4347-8EE8-B5D553CA096C     10.129.175.16/24    Active   40d
auto-k8s-ext-b10834cf-25a4-40e9-85be-d7be198417fa     VI:K8S-EXT:B10834CF-25A4-40E9-85BE-D7BE198417FA     10.129.175.10/24    Active   40d
auto-k8s-ext-d2144611-3110-417f-8d20-ac38b6ddbdbf     VI:K8S-EXT:D2144611-3110-417F-8D20-AC38B6DDBDBF     10.129.175.18/24    Active   5d17h
auto-k8s-ext-d29a52d3-5bed-4027-992a-5f94b1089163     VI:K8S-EXT:D29A52D3-5BED-4027-992A-5F94B1089163     10.129.175.24/24    Active   13d
auto-k8s-ext-d646e156-80a1-4c0c-a102-11669fd37cea     VI:K8S-EXT:D646E156-80A1-4C0C-A102-11669FD37CEA     10.129.175.22/24    Active   18d
auto-k8s-pods-17ef5d4d-f896-4f84-bc84-f02e63ed76cd    VI:K8S-PODS:17EF5D4D-F896-4F84-BC84-F02E63ED76CD    10.129.174.23/24    Active   18d
auto-k8s-pods-1e389e22-2a10-4b03-9c2d-37255d4f4ca4    VI:K8S-PODS:1E389E22-2A10-4B03-9C2D-37255D4F4CA4    10.129.174.11/24    Active   40d
auto-k8s-pods-38538401-efd4-4ddc-a328-7666724b917f    VI:K8S-PODS:38538401-EFD4-4DDC-A328-7666724B917F    10.129.174.25/24    Active   13d
auto-k8s-pods-5d555e1d-0636-45a5-868c-b0a3350efe6f    VI:K8S-PODS:5D555E1D-0636-45A5-868C-B0A3350EFE6F    10.129.174.13/24    Active   40d
auto-k8s-pods-719fafd5-4d68-4c88-8c0e-8364cba48059    VI:K8S-PODS:719FAFD5-4D68-4C88-8C0E-8364CBA48059    10.129.174.20/24    Active   18d
auto-k8s-pods-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb    VI:K8S-PODS:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB    10.129.174.12/24    Active   40d
auto-k8s-pods-8a7273c8-f2da-4e34-a088-e76f90fa8504    VI:K8S-PODS:8A7273C8-F2DA-4E34-A088-E76F90FA8504    10.129.174.15/24    Active   40d
auto-k8s-pods-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0    VI:K8S-PODS:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0    10.129.174.21/24    Active   21d
auto-k8s-pods-92e4e163-04ba-4890-be3a-ad81f4f8ad8c    VI:K8S-PODS:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C    10.129.174.14/24    Active   40d
auto-k8s-pods-a15c808c-d1cd-4347-8ee8-b5d553ca096c    VI:K8S-PODS:A15C808C-D1CD-4347-8EE8-B5D553CA096C    10.129.174.16/24    Active   40d
auto-k8s-pods-b10834cf-25a4-40e9-85be-d7be198417fa    VI:K8S-PODS:B10834CF-25A4-40E9-85BE-D7BE198417FA    10.129.174.10/24    Active   40d
auto-k8s-pods-d2144611-3110-417f-8d20-ac38b6ddbdbf    VI:K8S-PODS:D2144611-3110-417F-8D20-AC38B6DDBDBF    10.129.174.18/24    Active   5d17h
auto-k8s-pods-d29a52d3-5bed-4027-992a-5f94b1089163    VI:K8S-PODS:D29A52D3-5BED-4027-992A-5F94B1089163    10.129.174.24/24    Active   13d
auto-k8s-pods-d646e156-80a1-4c0c-a102-11669fd37cea    VI:K8S-PODS:D646E156-80A1-4C0C-A102-11669FD37CEA    10.129.174.22/24    Active   18d
```

```kmosk get secrets
NAME                                             TYPE                                  DATA   AGE
br-mosk001-ceph-0-bmc-secret                     Opaque                                2      40d
br-mosk001-ceph-0-user-data                      Opaque                                1      40d
br-mosk001-ceph-1-bmc-secret                     Opaque                                2      40d
br-mosk001-ceph-1-user-data                      Opaque                                1      40d
br-mosk001-ceph-2-bmc-secret                     Opaque                                2      40d
br-mosk001-ceph-2-user-data                      Opaque                                1      40d
br-mosk001-cmp-0-bmc-secret                      Opaque                                2      40d
br-mosk001-cmp-0-user-data                       Opaque                                1      40d
br-mosk001-cmp-02-bmc-secret                     Opaque                                2      3d18h
br-mosk001-cmp-05-bmc-secret                     Opaque                                2      18d
br-mosk001-cmp-05-user-data                      Opaque                                1      18d
br-mosk001-cmp-06-bmc-secret                     Opaque                                2      18d
br-mosk001-cmp-06-user-data                      Opaque                                1      18d
br-mosk001-cmp-07-bmc-secret                     Opaque                                2      13d
br-mosk001-cmp-07-user-data                      Opaque                                1      13d
br-mosk001-cmp-09-bmc-secret                     Opaque                                2      18d
br-mosk001-cmp-09-user-data                      Opaque                                1      18d
br-mosk001-cmp-1-bmc-secret                      Opaque                                2      40d
br-mosk001-cmp-14-bmc-secret                     Opaque                                2      5d17h
br-mosk001-cmp-14-user-data                      Opaque                                1      5d17h
br-mosk001-cmp-20-bmc-secret                     Opaque                                2      13d
br-mosk001-cmp-20-user-data                      Opaque                                1      13d
br-mosk001-master-0-bmc-secret                   Opaque                                2      40d
br-mosk001-master-0-user-data                    Opaque                                1      40d
br-mosk001-master-1-bmc-secret                   Opaque                                2      40d
br-mosk001-master-1-user-data                    Opaque                                1      40d
br-mosk001-master-2-bmc-secret                   Opaque                                2      40d
br-mosk001-master-2-user-data                    Opaque                                1      40d
default-token-8p6j6                              kubernetes.io/service-account-token   3      40d
helm-controller-sa-kaas-br-mosk001-token-wb2zj   kubernetes.io/service-account-token   3      40d
kaas-br-mosk001-deployment                       Opaque                                3      40d
kaas-br-mosk001-kubeconfig                       Opaque                                8      40d
machine-sa-br-mosk001-ceph-0-token-sc85j         kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-ceph-1-token-d9s5d         kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-ceph-2-token-tpz56         kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-cmp-0-token-ttqtj          kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-cmp-05-token-46rjb         kubernetes.io/service-account-token   3      18d
machine-sa-br-mosk001-cmp-06-token-85wfs         kubernetes.io/service-account-token   3      18d
machine-sa-br-mosk001-cmp-07-token-n5kjz         kubernetes.io/service-account-token   3      13d
machine-sa-br-mosk001-cmp-09-token-ghjw4         kubernetes.io/service-account-token   3      18d
machine-sa-br-mosk001-cmp-14-token-wnwkz         kubernetes.io/service-account-token   3      5d17h
machine-sa-br-mosk001-cmp-20-token-tr9jq         kubernetes.io/service-account-token   3      13d
machine-sa-br-mosk001-master-0-token-9jlqx       kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-master-1-token-m287g       kubernetes.io/service-account-token   3      40d
machine-sa-br-mosk001-master-2-token-6lgsm       kubernetes.io/service-account-token   3      40d
telemeter-sa-kaas-br-mosk001-token-gxdfq         kubernetes.io/service-account-token   3      40d
ucp-admin-password-kaas-br-mosk001               Opaque                                1      40d
```


```
$ kmosk get  bmh br-mosk001-cmp-02 -o yaml
...
status:
  errorCount: 33
  errorMessage: MAC address e4:43:4b:ea:bd:c0 conflicts with existing node br-mosk001~br-mosk001-cmp-1
  errorType: registration error
  goodCredentials: {}
  hardwareProfile: ""
  lastUpdated: "2021-10-26T09:23:12Z"
  operationHistory:
    deprovision:
      end: null
      start: null
    inspect:
      end: null
      start: null
    provision:
      end: null
      start: null
    register:
      end: null
      start: "2021-10-22T15:20:27Z"
  operationalStatus: error
...
```

```
kmosk get kaasreleases
NAME          AGE
kaas-2-10-0   96d
kaas-2-11-0   42d
kaas-2-9-0    124d
```

Hello,
I am talking about this issue on slack with Serhii Ivanov and Alexey Zvyagintsev.

For deletion, I have folowed the docs : 
- https://docs.mirantis.com/mos/latest/ops/openstack-operations/delete-compute.html
- https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/delete-machine.html

- deleted the service on openstack
- deleted the machine with `kubectl delete machine <machine_name>` `kubectl delete bmh <bmh_name>`.
The BMH was not deleting. I have open the case "4831282", and followed the instruction from Vitaly Edrenkin :
- edit the bmh ressources
- remove the finalizers and consumerRef information
It seems that removing the finalizers was a very bad advice ...
Alexey Zvyagintsev is now looking for a way to finalize the deletion manually.

For the creation, I have create yaml file form bmh and apply it. And another yaml file for machine, create it. As I did 4 time without any problem.




