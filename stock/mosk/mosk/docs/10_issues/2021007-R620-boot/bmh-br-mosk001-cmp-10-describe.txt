Name:         br-mosk001-cmp-10
Namespace:    br-mosk001
Labels:       baremetal=hw-br-mosk001-cmp-10
              kaas.mirantis.com/provider=baremetal
              kaas.mirantis.com/region=region-one
Annotations:  <none>
API Version:  metal3.io/v1alpha1
Kind:         BareMetalHost
Metadata:
  Creation Timestamp:  2021-10-07T12:25:12Z
  Finalizers:
    baremetalhost.metal3.io
  Generation:  1
  Managed Fields:
    API Version:  metal3.io/v1alpha1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:kubectl.kubernetes.io/last-applied-configuration:
        f:labels:
          .:
          f:baremetal:
          f:kaas.mirantis.com/provider:
          f:kaas.mirantis.com/region:
      f:spec:
        .:
        f:automatedCleaningMode:
        f:bmc:
          .:
          f:address:
          f:credentialsName:
        f:bootMACAddress:
        f:bootMode:
        f:image:
          .:
          f:checksum:
          f:url:
        f:online:
    Manager:      kubectl-client-side-apply
    Operation:    Update
    Time:         2021-10-07T12:25:12Z
    API Version:  metal3.io/v1alpha1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:finalizers:
          .:
          v:"baremetalhost.metal3.io":
      f:status:
        .:
        f:errorCount:
        f:errorMessage:
        f:errorType:
        f:goodCredentials:
          .:
          f:credentials:
            .:
            f:name:
            f:namespace:
          f:credentialsVersion:
        f:hardwareProfile:
        f:lastUpdated:
        f:operationHistory:
          .:
          f:deprovision:
            .:
            f:end:
            f:start:
          f:inspect:
            .:
            f:end:
            f:start:
          f:provision:
            .:
            f:end:
            f:start:
          f:register:
            .:
            f:end:
            f:start:
        f:operationalStatus:
        f:poweredOn:
        f:provisioning:
          .:
          f:ID:
          f:bootMode:
          f:image:
            .:
            f:url:
          f:state:
        f:triedCredentials:
          .:
          f:credentials:
            .:
            f:name:
            f:namespace:
          f:credentialsVersion:
    Manager:         baremetal-operator
    Operation:       Update
    Time:            2021-10-07T12:46:27Z
  Resource Version:  122311666
  UID:               6b43f89d-9ef6-414e-8287-9a0ee45a094f
Spec:
  Automated Cleaning Mode:  metadata
  Bmc:
    Address:           10.29.20.40:623
    Credentials Name:  br-mosk001-cmp-10-bmc-secret
  Boot MAC Address:    c8:1f:66:bd:d8:11
  Boot Mode:           UEFI
  Image:
    Checksum:  http://10.129.172.62/images/stub_image.qcow2.md5sum
    URL:       http://10.129.172.62/images/stub_image.qcow2
  Online:      true
Status:
  Error Count:    12
  Error Message:  Introspection timeout
  Error Type:     inspection error
  Good Credentials:
    Credentials:
      Name:               br-mosk001-cmp-10-bmc-secret
      Namespace:          br-mosk001
    Credentials Version:  121392651
  Hardware Profile:       
  Last Updated:           2021-10-08T07:26:37Z
  Operation History:
    Deprovision:
      End:    <nil>
      Start:  <nil>
    Inspect:
      End:    <nil>
      Start:  2021-10-07T12:25:23Z
    Provision:
      End:    <nil>
      Start:  <nil>
    Register:
      End:             2021-10-07T12:25:23Z
      Start:           2021-10-07T12:25:12Z
  Operational Status:  error
  Powered On:          false
  Provisioning:
    ID:         e49128a8-962f-4c77-807c-c61c9b3dfe94
    Boot Mode:  UEFI
    Image:
      URL:  
    State:  inspecting
  Tried Credentials:
    Credentials:
      Name:               br-mosk001-cmp-10-bmc-secret
      Namespace:          br-mosk001
    Credentials Version:  121392651
Events:                   <none>
mcc-user@kaas-node-88c118e2-9ce8-4