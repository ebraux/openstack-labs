

I am trying to deploy au compute with a Dell poweredge R620 Hardware.
With a PERC H710 Mini card

The UEFI boot has been configured in bios.

The server :
 - Start PXE over IP,
 - get an IP adresse
 - download NBP files

But stuck on the "iPxe Initialising device ..."

3 servers of this model has been succesfully deployed for MCC by your deployement team.
 
In the log bmh logs, I can see a a timeout error.

kubectl -n br-mosk001 get bmh br-mosk001-cmp-10 -o yaml
...
status:
  errorCount: 7
  errorMessage: Introspection timeout
  errorType: inspection error
...

---
mcc-user@kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0:~$ kubectl -n kaas  logs baremetal-operator-75cc95855b-86tjj | grep cmp-10


{"level":"info","ts":1633620767.0521312,"logger":"controllers.BareMetalHost","msg":"registering and validating access to management controller","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting","credentials":{"credentials":{"name":"br-mosk001-cmp-10-bmc-secret","namespace":"br-mosk001"},"credentialsVersion":"121392651"}}
{"level":"info","ts":1633620767.119036,"logger":"provisioner.ironic","msg":"current provision state","host":"br-mosk001~br-mosk001-cmp-10","lastError":"ironic-inspector inspection failed: Introspection timeout","current":"inspect failed","target":"manageable"}
{"level":"info","ts":1633620767.1190765,"logger":"controllers.BareMetalHost","msg":"verified access to the BMC","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting"}
{"level":"info","ts":1633620767.1190934,"logger":"controllers.BareMetalHost","msg":"inspecting hardware","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting"}
{"level":"info","ts":1633620767.1191597,"logger":"controllers.BareMetalHost","msg":"inspecting hardware","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting"}
{"level":"info","ts":1633620767.1191657,"logger":"provisioner.ironic","msg":"inspecting hardware","host":"br-mosk001~br-mosk001-cmp-10"}
{"level":"info","ts":1633620767.1533387,"logger":"provisioner.ironic","msg":"inspection failed","host":"br-mosk001~br-mosk001-cmp-10","error":"Introspection timeout"}
{"level":"info","ts":1633620767.1534276,"logger":"controllers.BareMetalHost","msg":"saving host status","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting","operational status":"error","provisioning state":"inspecting"}
{"level":"info","ts":1633620767.1689112,"logger":"controllers.BareMetalHost","msg":"publishing event","baremetalhost":"br-mosk001/br-mosk001-cmp-10","reason":"InspectionError","message":"Introspection timeout"}
{"level":"info","ts":1633620767.1761906,"logger":"controllers.BareMetalHost","msg":"done","baremetalhost":"br-mosk001/br-mosk001-cmp-10","provisioningState":"inspecting","requeue":false,"after":8025.512774133}



---
kubectl -n br-mosk001 logs -f bmh br-mosk001-cmp-10
Error from server (NotFound): pods "bmh" not found

kubectl -n br-mosk001 describe bmh br-mosk001-cmp-10 

kubectl get no -n br-mosk001
NAME                                             STATUS   ROLES    AGE    VERSION
kaas-node-34412b23-0e4b-42c1-949c-4f20d79fc858   Ready    master   106d   v1.20.1-mirantis-1-3-g71afa0ba032bfd
kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0   Ready    master   106d   v1.20.1-mirantis-1-3-g71afa0ba032bfd
kaas-node-d09c78aa-00c3-40bc-b44d-957e015a8e51   Ready    master   106d   v1.20.1-mirantis-1-3-g71afa0ba032bfd

---
```bash
kubectl get events -n br-mosk001 
LAST SEEN   TYPE     REASON               OBJECT                            MESSAGE
42m         Normal   BMCAccessValidated   baremetalhost/br-mosk001-cmp-11   Verified access to BMC
16m         Normal   InspectionError      baremetalhost/br-mosk001-cmp-11   Introspection timeout
30m         Normal   InspectionError      baremetalhost/br-mosk001-cmp-10   Introspection timeout
19m         Normal   InspectionError      baremetalhost/br-mosk001-cmp-11   Introspection timeout
21m         Normal   InspectionError      baremetalhost/br-mosk001-cmp-11   Introspection timeout
8s          Normal   InspectionError      baremetalhost/br-mosk001-cmp-11   Introspection timeout
11m         Normal   InspectionError      baremetalhost/br-mosk001-cmp-11   Introspection timeout
42m         Normal   InspectionStarted    baremetalhost/br-mosk001-cmp-11   Hardware inspection started
42m         Normal   Registered           baremetalhost/br-mosk001-cmp-11   Registered new host

```


610 : Broadcom NetXtreme II Gigabit Ethernet 

Broadcom Gigabit Ethernet BCM5720
  Paramètres de cible de stokage et initiateur


BRCM GbE 4P 5720-t rNDC
