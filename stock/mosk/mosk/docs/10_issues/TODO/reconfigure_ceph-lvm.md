

Config :
- [https://docs.mirantis.com/mos/latest/deploy/deploy-openstack/advanced-config/enable-lvm-ephemeral-storage.html](https://docs.mirantis.com/mos/latest/deploy/deploy-openstack/advanced-config/enable-lvm-ephemeral-storage.html)
- [https://docs.mirantis.com/mos/latest/deploy/deploy-managed/create-bm-hostprofile/create-host-profile.html](https://docs.mirantis.com/mos/latest/deploy/deploy-managed/create-bm-hostprofile/create-host-profile.html)

Modification : 
- [https://docs.mirantis.com/mos/latest/ref-arch/openstack/openstack-operator/osdpl-cr/standard-conf.html](https://docs.mirantis.com/mos/latest/ref-arch/openstack/openstack-operator/osdpl-cr/standard-conf.html)


Dans la config du déployement Opensatck, actuellement on a 

```yaml
spec:
  features:
    nova:
      images:
        backend: ceph
```

Et pour les noeuds en LVM, on ajoute le label dans les spec de la machine
```yaml
---
apiVersion: cluster.k8s.io/v1alpha1
kind: Machine
metadata:
  name: br-mosk001-cmp-11
  namespace: br-mosk001
  labels:
    cluster.sigs.k8s.io/cluster-name: kaas-br-mosk001
    hostlabel.bm.kaas.mirantis.com/worker: "true"
    kaas.mirantis.com/provider: baremetal
    kaas.mirantis.com/region: region-one
    openstack.nova.storage: lvm
```

La doc indique de replacer par
```yaml
spec:
  features:
    nova:
      images:
        backend: lvm
        lvm:
          volume_group: "nova-vol"
```
Mais du coup c'est global.
 Quel impact sur l'infra si on modifie le deployemnt ?
 comment on fait si on veut un neud CEPH ensuite ?


Fir BMHP, I have set config :
``` yaml
- name: lvm_nova_part
  sizeGiB: 5
  wipe: true
- name: lvm_root_part
  sizeGiB: 0
  wipe: true
``` 
The volume with  `sizeGiB: 0`  must be the last one.

Vasyl Saienko:maison_avec_jardin:  18 h 41 06/11/2021
@BRAUX Emmanuel could you please show your osdpl, the nova.conf from compute nodes where you want to configure lvm and kubectl get nodes <node-name> --show-labels for compute nodes with lvm


---
 Dans le déploimeent actuel, les node lvm sont configurés au niveau de osdpl
 ```bash
 kubectl -n openstack get osdpl -oyaml
 ```

 ```bash
 spec: 
   nodes:
     kubernetes.io/hostname::kaas-node-61fa4916-d21f-4c8b-b39b-c38cd05ab8c8:
      features:
        nova:
          images:
            backend: lvm
            lvm:
              volume_group: nova-vol
          live_migration_interface: br-tenant

 
 ```


 If remove the compute with CEPH storage, and I configure all my compute with dedicated host configuration in the "osdpl", all the compute will be using LVM config. could I then configure LVM as defaut config, and remove dedicated config for all the nodes ?
Nouveau

Vasyl Saienko:maison_avec_jardin:  19 h 14
yes, you can do that. Or you can add some label to nodes with lvm for example nova-compute-images-backend:lvm. Apply it via kubectl label node <node-name> nova-compute-images-backend=lvm and use in osdpl override as selector. As soon you redeploy node and add lvm label nova will be reconfigured automatically. And you will not need to specify exact hostname in osdpl after adding new node



I have redeployed the conpute, and configured osdpl, as soon as I got hostname with `get lcmmachine -o wide`

```
kubectl -n openstack edit  osdpl
spec:
  nova:
    images:
      backend: ceph
    live_migration_interface: br-tenant
  nodes:
    kubernetes.io/hostname::kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:
      features:
        nova:
          images:
            backend: lvm
            lvm:
              volume_group: nova-vol
          live_migration_interface: br-tenant
```

It is better for apache2 install,  but still not good :
```
real	4m42.843s
user	0m5.174s
sys	0m1.694s
```

And disk perf are still bad
```
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 17.1275 s, 62.7 MB/s
```

For information, the same test in the compute : 
```
root@kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 0.702882 s, 1.5 GB/s
```

I don't know wher Nova stores instances 
```
mcc-user@kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:/var/lib/nova/instances$ tree
.
├── _base
│   └── 81fe2af3c08afb240abc955c0dc99b4e0be9c475
├── d45a0b58-dcad-45b7-b262-201d0c37e50f
│   └── console.log
└── locks
    └── nova-81fe2af3c08afb240abc955c0dc99b4e0be9c475
```



The labels associated to the compute are :
```
kubectl get node kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826  --show-labels
NAME                                             STATUS   ROLES    AGE   VERSION               LABELS
kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   Ready    <none>   28m   v1.18.19-mirantis-1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,ceph-daemonset-available-node=true,com.docker.ucp.collection.root=true,com.docker.ucp.collection.shared=true,com.docker.ucp.collection.swarm=true,com.docker.ucp.collection=shared,kaas.mirantis.com/machine-name=br-mosk001-cmp-02,kubernetes.io/arch=amd64,kubernetes.io/hostname=kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826,kubernetes.io/os=linux,openstack-compute-node=enabled,openvswitch=enabled
```

d45a0b58-dcad-45b7-b262-201d0c37e50f
d45a0b58-dcad-45b7-b262-201d0c37e50f
