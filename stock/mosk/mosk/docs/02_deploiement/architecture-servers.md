# Architecture matérielle serveur


## Détails de l'architecture matérielle nécessaire

Pour le gestionnaire de cluster (MCC), il faut 3 machines.

rem : 

- ce gestionnaire sert à gérer les clusters déployés.
- pour l'instant, il y a uniquement le cluster Openstack.
- Mais si on part sur du Mirantis pour l'infra Swram/K8S de contrats, ça gèrerait aussi ce cluster.

Pour le Cluster Openstack, normalement il faut :
 - 3 machines pour le master K8s
 - 3 machines pour le Controlplane Openstack
 - 5 machines pour le cluster CEPH
 - des computes
Un déploiement en mode "limité" est en cours de validation, c'est ce qu'on a mis en place.
  - toutes les fonctionnalités ne sont pas disponibles, mais on a celle dont on a besoin pour l'instant
  - la mise à jour auto, n'est pas encore officiellement supportée, mais elle est en release candidate, donc c'est bon.
Ce qui donne
 - 3 machines pour le master k8s, et le controlPlane Openstack en collocation
 - 3 machine pour CEPH

---
## Détails de l'architecture matérielle

### pour le gestionnaire de Clusters (MCC):

Ce qu'on a mis en place :    3 machines de type R620
   -  hors garantie,
   - avec 64 Go de RAM,
   - disque :
       système : 1x 145Go HDD
       données locales :   1x 145Go HDD
       données partagées :   1x 145Go HDD

Recommandé 
   - 128 Go de Ram
   - disque
       système : 960Go SDD
       données locales :   2To SSD/HDD
       données partagées :   2To SSD/HDD

Limitation et évolution
 - la taille des disque limite la rétention des stats de performance/logs. Actuellement on a que 3 jours. mais au final ça peut être suffisant
 - pas de redondance des disques
 - pourrait être hébergée sur une infra VSphere
 - Possibilité d'évolution à court terme :
     - 3x (3 disques de 2To, 2.5''  HDD 15k ) 
     - 2  disques de 2To, 2.5''  HDD 15k en spare
     - éventuellement 3x (2 disques de 480 ou 960Go, 2.5", en HDD 15k, ou SSD)
 - Dans un contexte "contrats", il vaudrait mieux remplacer les machines, ou voir comment les placer sur Vsphere


### Pour Openstack (MOSK):
Ce qu'on a mis en place :    3 machines de type R640
   -  fin de garantie : 05 OCT. 2027
   - avec 192 Go de RAM,
   - disque :
       système : 1x 480Go SSD
       données locales :   1x 480Go SSD
       données partagées :   1x 2To HDD

Recommandé 
   - 256 Go de Ram
   - disque
       système : 960Go SDD
       données locales :   2To SSD/HDD
       données partagées :   2To SSD/HDD

Limitation et évolution
 - La taille des disque limite la rétention des stats de performance/logs. Actuellement on a que 10 jours. C'est trop limité en cas de pb récurent
 - Pas de redondance des disques
 - Possibilité d'évolution à court terme :
     - 3x ( 2 disques 960Go SSD,  ou récupération des disques 480Go SSD  libérés pour les données)
     - 3x (1 disque de 2To, 2.5", en HDD  15k)
     - 3x (1 disque de 2To, 2.5", en SSD)
     - 2  disques de 2To, 2.5''  HDD 15k en spare 
     - 1  disques de 2To, 2.5''  SSD en spare : important !!


---
## Contexte Contrat

Pour "contrats", possibilité de déployer un ou plusieurs clusters Swarm ou k8s.
Normalement un cluster c'est au minimum 3 master + 3 slave, mais le mode "collocation" est supporté maintenant.

### Contraintes si scénario VSphere : 
   - nécessite d'avoir un utilisateur de type "API", avec des droits de création/gestion de VM
   - nécessité soit :
        - soit d’amener des vlan de la "bulle" openstack sur Vsphere :
             - le Vlan 420 : vlan OOB/IPMI de getsion des cartes idrac, et management des composants bas niveau
             - le Vlan 172 (le Vlan des IP des machines physiques)
             - pas besoin des autres Vlan, spécifiques Openstack, donc risque limité
        - soit d'installer un MCC dédié aux cluster Swram/k8s
    - besoin de valider la notion d e"stockage partagé", qui est indiqué dans la documentation Mirantis.

L'idée serait donc de faire un POC Vsphere avec une licence de démo de 60 jours ou de mettre en place une infra vsphere dédiée "contrats", le temps de voir ce que ça donne.
A confirmer avec Claude


### Contraintes si scénario BareMetal

- acheter 3 machines pour remplacer le MCC qui a été mis en place pour Openstack
- acheter 3 machines pour le cluster Swarm/Kubernetes
- et prévoir tous les a-côté en terme de connexion réseau.
 
 