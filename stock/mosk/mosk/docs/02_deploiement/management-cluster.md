


## le "tarball"

To update the bootstrap tarball after an automatic cluster upgrade:

Select from the following options:

For clusters deployed using Container Cloud 2.11.0 or later:
```bash
./kaas bootstrap download --management-kubeconfig <pathToMgmtKubeconfig> \
--target-dir <pathToBootstrapDirectory>
```

[https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html)


## Mise àjour
A management cluster upgrade to a newer version is performed automatically once a new Container Cloud version is released

[https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html)


## Informations de déploiment d'Management Cluster

[https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt.html)

* Adding more than 3 nodes or deleting nodes from a management or regional cluster is not supported.
* Removing a management or regional cluster using the Container Cloud web UI is not supported. Use the dedicated cleanup script instead. For details, see Remove a management cluster and Remove a regional cluster.
* Before removing a regional cluster, delete the credentials of the deleted managed clusters associated with the region.