


liste au 12/11/2021

```bash
$ kub get lcmmachine -o wide
NAME                  STATE   INTERNALIP       HOSTNAME                               
br-mosk001-ceph-0     10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f
br-mosk001-ceph-1     10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c
br-mosk001-ceph-2     10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504
br-mosk001-cmp-01     10.129.172.111   kaas-node-37e14019-2ff6-447d-8000-a015a52dafc6
br-mosk001-cmp-02     10.129.172.109   kaas-node-3ca03bf6-8687-4927-b841-36701c17b269
br-mosk001-cmp-03     -
br-mosk001-cmp-04     -
br-mosk001-cmp-05     10.129.172.115   kaas-node-a4b68b33-a5a9-44a7-9e30-8bac007869e3
br-mosk001-cmp-06     10.129.172.116   kaas-node-8eaa6a0a-0da4-46e3-bb9b-9e1ffa10e995
br-mosk001-cmp-07     10.129.172.117   kaas-node-15d69619-7253-4155-b6a9-e8f2cd46d8d3
br-mosk001-cmp-08     -
br-mosk001-cmp-09     10.129.172.118   kaas-node-96087b45-86de-49d2-91bc-35b3d7ab08cc
br-mosk001-cmp-10     10.129.172.119   kaas-node-b3acb17c-b213-46a1-9779-7588e5592f2e
br-mosk001-cmp-11     10.129.172.120   kaas-node-7817b125-1b8d-4544-8728-5d2cfac3abf5
br-mosk001-cmp-12     -
br-mosk001-cmp-14     10.129.172.122   kaas-node-3d62a6db-993b-4c32-8bbe-52d3125eebbc
br-mosk001-cmp-20     10.129.172.113   kaas-node-77c79309-7f7d-4c57-8a22-89963ba1d015
br-mosk001-master-0   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa
br-mosk001-master-1   10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4
br-mosk001-master-2   10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb
```

br-mosk001-cmp-0  -> [prod] config CEPH, a réinitiliaser ()
  br-mosk001-cmp-0  10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c 

br-mosk001-cmp-1  -> [prod]config CEPH, , en cours de ré-initialisation
br-mosk001-cmp-2  -> [prod] config disque indépendants, a réinitiliaser ()
br-mosk001-cmp-3  -> config disque indépendants, en cours de ré-initialisation
br-mosk001-cmp-05 -> [prod] ok
br-mosk001-cmp-06 -> [prod] ok
br-mosk001-cmp-06 -> [prod] ok
br-mosk001-cmp-07 -> [prod] ok
  -> ipa-5b28276a boucle sur check-inspection
br-mosk001-cmp-09 -> [prod] ok
br-mosk001-cmp-10  -> PB ipxe
br-mosk001-cmp-11  -> PB ipxe
br-mosk001-cmp-12  -> PB ipxe

br-mosk001-cmp-0  10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c
br-mosk001-cmp-1  10.129.172.110   kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302
br-mosk001-cmp-2  10.129.172.111   kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577
br-mosk001-cmp-3  10.129.172.112   kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec

br-mosk001-cmp-05  10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd
br-mosk001-cmp-06  10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea
br-mosk001-cmp-09  10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059

ex machine Director : 10.29.20.20
R630
18:66:da:ed:2c:3c
300Go en RAID 1
boot Bios

Idrac/BMC
10.29.20.39

10.29.20.40 esxi-06 
  baie 22
  pxe: C8:1F:66:BD:D8:11
  model 620
  carte réseau : 2 carte 4 ports
  disks, 2 disques de 300G, en RAID1

10.29.20.41 esxi-03
  pxe :  c8:1f:66:bd:ee:2d
  baie13
  model 620
  carte réseau : 2 carte 4 ports
  disks, 2 disques de 300G, en RAID1

10.29.20.42 esxi-04
  pxe:  C8:1F:66:BD:EB:49
  baie 23
  model 620
  carte réseau : 2 carte 4 ports
  disks, 2 disques de 300G, en RAID1


Les autres ESXi sont dispos :
-   esxi-01, esxi-02, esxi-07  sont au K02
-   esxi-03, esxi-04, esxi-06  au D01


Congig idrac
Masque de sous-réseau 	255.255.255.0
Passerelle 10.29.20.1
Utiliser DHCP pour obtenir les adresses de serveurs DNS	Désactivé
Serveur DNS préféré actuel 192.44.75.10
Autre serveur DNS actuel 192.108.115.2