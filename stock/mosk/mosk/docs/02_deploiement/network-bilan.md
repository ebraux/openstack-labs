
# Configuration réseau

Copie du contenu du PAD: [https://semestriel.framapad.org/p/r0edqftbsp-9otp](https://semestriel.framapad.org/p/r0edqftbsp-9otp)


## ***Besoins en Vlans  / subnets***



   * toutes les cartes Idrac et certaines interfaces de management des switchs sont connectés à un Vlan dédié "Out of Band", le Vlan 420. Il doit être accessible depuis le Vlan DISI
   * la nouvelle infra Openstack à déployer utilise les Vlan 172 à 179, et les subnets correspondants
   * le vlan 172 est utilisé pour la machine d'admin, et pour déployer les machines physiques, et y accéder. Il doit être accessible depuis le Vlan DISI. Il doit pouvoir accéder au Vlan OOB
   * le Vlan 178 est utilisé pour les IP qui seront attribuées aux VM déployées dans Openstack. Il doit être accessible sur le réseau interne d'IMT atlantique
   * le vlan 175 est utilisé pour accéder à Openstack (interface web et API), ainsi que pour des outil annexes (gestion de logs, ...). Il doit être accessible depuis le Vlan DISI.
   * les Vlans 173, 174, et 175 doivent pouvoir être utilisé en multisite, si un jour une infra multi-site devait être mise en place
   * les Vlans 176, 177, et 179 sont purement internes au cluster Openstack, et n'ont pas besoin d'être routés en dehors du cluster, local à Brest.
   * L'IP utilisée pour  accéder à Openstack doit être accessible au moins sur le réseau interne d'IMT atlantique, et si possible avec une IP publique.
     * une IP publique a été affectée à ce service
     * les nom DNS *.openstack.imt-atlantique.fr ont éét défini vers cette IP
     * un mécanisme de Reverse proxy basé sur HAProxy a été mis en place entre l'IP publique, et l'IP Opensatck interfec WEB et API du VLAn 175
   * l'IP publique utilisée pour Opensatck doit être accessible depuis le VLAN 172.  

Les subnets utilisés dans les Vlans sont : 

   * 420 : 10.29.20.0/24
   * 172 : 10.129.172.0/24
   * 173 : 10.129.173.0/24
   * 174 : 10.129.174.0/24
   * 175 : 10.129.175.0/24
   * 176 : 192.168.176.0/24
   * 177 : 192.168.177.0/24
   * 178 : 10.129.176.0/22  et 192.108.116.208/29
   * 179 : 192.168.179.0/24




## ***Cablage des switches***



L'infra réseau est composées de :

   * 2 switchs DELL SFP+ 10G avec LACP
   * 6 switchs cisco RJ45 1G
A l'origine, seuls les switchs DELL devaient être reliés au VSS pour les accès vers l'extérieur. Les switch Cisco devaient être reliés en cascade aux switchs DELL.

Mais les switch Cisco n'ont pour l'instant pas pu être relié aux switchs DELL, et sont donc reliés directement au VSS.

Il faut donc que tous les VLAN 172 à 179 puissent circuler entre les switchs DELL et Cisco via le VSS



On a donc :

    VSS 

       |\_ switch DELL stack10g-01,

       |     || [LACP]

       |\_ switch DELL stack10g-02, 

       |         -  utilisent les vlan 172 à 179

       |         - prises "MOSK NIC0", configurée en  mode access sur le Vlan 172

       |         - prises "MOSK BOND", configurée en mode trunk pour Vlan 173-179, sans Vlan natif

       |

       |\_ stack Cisco stack-d1-02

       |         - utilise les vlans 173 à 179

       |         - prises configurées en mode trunk pour Vlan 173-179, native Vlan = 173 

       |

       |\_ switch Cisco sw-d1-04 : 

              |        - utilise les vlans pour l'infra Openstack actuellement en production (50 à 56)

              |       - prises configurées en mode trunk pour Vlan 50.-56, native Vlan = 50 

              |       - master pour des switchs en "cascade"  

          |

              |\_  switch Cisco sw-d1-03 : 

              |               - en cascade,

              |               - utilise le  Vlan 172

              |               - prise configurée en mode access sur le Vlan 172

          |\_  stack Cisco stack-d1-01 : 

                              - en cascade,

                              - utilise le Vlan 420

                              - prises configurées en mode access sur le Vlan 420



## *Configuration des serveurs*

2 configurations différentes de machines ont été mises en place



   *  machines avec connexion SFP+/10G (connectées aux 2 switchs DELL)
       * les machines les plus récentes
       * principalement des serveurs R640 et R740
       * correspond au controlplane Openstack, aux serveurs CEPH, et à 2 compute (ceux prévu pour un stockage des disques des VM sur CEPH)
   * machines avec connexion RJ45/1G (connectées aux switchs CISCO)
       * tout type de machine :  R610, R710, R620, R630, R640, R740, ...
       * les machines des "gestion" du cluster, et ules compute Openstack




Sur les machines avec connexion SFP+/10G, avec 2 cartes, connectées aux 2 switchs DELL :

   * NIC1
       * interface dédiée au VLAN 172
       * au niveau OS : pas de Vlan configuré
       * au niveau switch : connecté uniquement au switch DELL#1, les prises sont configurée en mode access
       * PXE activé
   * NIC2 et NIC3, et carte2 NIC1 et NIC2
       * interfaces utilisées pour les VLANs 173 à 179
       * au niveau OS :
           * interface en Bond   (avec netplan "bonds")
               * Bond0 = NIC2 et carte2 NIC1,
               * Bond1 = NIC3 et carte2 NIC2
           * utilisation de bridges et d'interfaces virtuelle pour gestion des vlans  (avec netplan "vlans"  et "bridges")
       *  au niveau switch:
           * connecté aux 2 switches DELL, avec correspondance des prises. Par ex pour le bond0, si la NIC2 est sur l'interface 4 du switch#1, la carte2 NIC1 est sur l'interface 4 du switch#2
           * prises configurée en mode Trunk
           * LACP activé
       * PXE désactivé


Sur les machines avec connexion RJ45/1G, avec 1 carte, connectées aux switchs CISCO :

   * NIC1 :
       * interface dédiée au VLAN 172
       * au niveau OS : pas de Vlan configuré
       * au niveau switch : connecté au switch sw-d1-03, dédié à ce Vlan. Les prises sont configurée en mode access
       * PXE activé
   * NIC2 et NIC3
       * interfaces utilisées pour les VLANs 173 à 179
       * au niveau OS :  
           * Interface en bond   (avec netplan "bonds") :
               * Bond0 = NIC2 et NIC3
           * utilisation de bridges et d'interfaces virtuelle pour gestion des vlan  (avec netplan "vlans"  et "bridges")
       * au niveau switch : 
           * connecté au stack stack-d1-02. un switch du stack est dédié aux NIC2, l'autre aux NIC3
           * prises configurée en mode Trunk
       * PXE désactivé




## Accès au service



L'accès aux différents composants d'Openstack se fait via les API. Un Nom DNS spécifique est attribué à chaque service :

   * une seule IP est utilisés pur tous les services
   * seuls les ports standards 80/443 sont utilisés


L'ip PUBLIQUE attribuée est "192.108.117.56", dans le Vlan  Bastion 670.

L'adresse IP pour accéder à Openstack (interface web et API) est "10.129.175.205"



Les noms des services sont en *.openstack.imt-atlantique.fr.  La liste est  : barbican, cinder, cloudformation, designate, glance, heat, horizon, keystone, neutron, nova, novncproxy, octavia, placement

Par exemple pour le service horizon (le dashboard), l'accès se fera donc via [https://horizon.openstack.imt-atlantique.fr](https://horizon.openstack.imt-atlantique.fr). (accès limité donc nécessite le VPN)



Les déclarations DNS ont été effectuées. 

Un serveur HAProxy a été mis en place pour gérer le reverse-proxy entre l'IP Publique, et l'infra interne





# **Situation actuelle**



## Informations pour les tests



Machines/IP pour tests : 

 accès idrac : 10.29.20.24,  10.29.20.25, ...

   * machine admin :  mcc-seed-01 [10.129.172.14], connecté à sw-d1-03. + une IP finissant par 2 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.2)
   * machines pour tests connexion stack-d1-02
       *  #1 : ceph-poc-01 [10.129.172.231] + une IP finissant par 231 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.231)
       * #2 : ceph-poc-02 [10.129.172.232] + une IP finissant par 232 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.232)
   * machines pour tests connexion switch DELL
       * #1 :  OS-CTRL-01 [10.129.172.106] + une IP finissant par 13 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.13)
       * #2 : OS-CTRL-02 [10.129.172.107] + une IP finissant par 14 dans chaque Vlan  (ex pour vlan 175 : 10.129.175.14)
       * attention,  le Vlan 173 n'est pas utilisé sur les machines connectées aux swiths DELL


## Détail procédure de test

Signification des tests effectués: 

   * accessible depuis le vlan DISI  : depuis ma machine dans le Vlan DISI (VPN)
   * accessible directement depuis le VLAN 172 : depuis la machine admin, avec Vlan autre que 172 désactivés.
   * accessible dans le même switch Cisco  : depuis 2 machines connectées au même switch (utilisé pour Vlan 172)
   * accessible stack Cisco : depuis ceph-poc-01 vers ceph-poc-02
   * accessible entre les switchs/stack Cisco : depuis machine admin, vers ceph-poc-01 et/ou ceph-poc-02
   * accessible entre les switchs Del : depuis OS-CTRL-01 vers OS-CTRL-02
   * accessible entre les switchs Cisco et DELL : depuis admin et ceph-poc-01 et/ou ceph-poc-02, vers OS-CTRL-01 et/ou OS-CTRL-02




## Bilan



**Vlan 420**

   * accessible depuis le Vlan DISI -> OK
   * accessible depuis campus distant : pratique, mais très très sécurisé ...


**Vlan 172**

   * accessible depuis le vlan DISI  -> OK
   * accès au Vlan 420  -> OK
   * accessible dans le même switch Cisco -> OK
   * accessible entre les switchs Dell  -> OK
   * accessible entre les switches Cisco et DELL  -> OK
-> tout va bien pour ce Vlan



**Vlan 175**

   * accessible au sein du même switch Cisco   -> OK
   * accessible entre les switch Cisco -> OK
   * accessible entre les switchs Dell *-> OK*
   * accessible entre les switchs Cisco et Dell*-> OK*
   * **accessible depuis le Vlan DISI -> NON**
   * **accessible directement depuis le VLAN 172 -> NON**
 -> satisfaisant, car alternative mise en place : affecter une IP dans ce Vlan à la Machine d'admin



**Vlan 178**

   * accessible au sein du même switch Cisco   -> OK
   * accessible entre les switch Cisco -> OK
   * *accessible entre les switchs Dell -> NON TESTÉ POUR L'INSTANT*
   * *accessible entre les switchs Cisco et Dell -> NON TESTÉ POUR L'INSTANT*
-> tout va bien pour ce Vlan



**Vlan 173**

   * accessible au sein du même switch Cisco -> OK
   * *accessible entre les switch Cisco -> OK*
   * *accessible entre les switchs Dell -> NON TESTÉ POUR L'INSTANT*
   * *accessible entre les switchs Cisco et Dell -> NON TESTÉ POUR L'INSTANT*
-> tout va bien pour ce Vlan



**Vlan 174, 176, 177, 179 **

   * accessible au sein du même switch Cisco   -> OK
   * accessible entre les switch Cisco -> OK
   * *accessible entre les switchs Dell -> OK*
   * *accessible entre les switchs Cisco et Dell -> OK*
-> tout va bien pour ces Vlan





# **Actions à mener**



## Courant septembre



***Configuration/cablage des switchs***

   * connecter le switch "Cisco stack-d1-02" en cascade sur le "switch Cisco sw-d1-04"
   * connecter le stack "Cisco stack-d1-01" utiliser pour les acces OOB, soit dirctement sur le VSS, soit sur un switch Hos de la config "Openstack", pour garantir un accés de management en cas de Pb sur le "réseau Openstack"
   * connecter l'interface de management de chaque switch Cisco au switch OOB "Cisco stack-d1-01", et affecter un IP de management sur le réseau 420 à chaque équippement
   * relier les switchs Cisco "switch Cisco sw-d1-03" et "stack Cisco stack-d1-02" en cascade sur les switchs DELL.
   * Evaluer si il faut augmenter le nombre de connexion RJ45/1G, pour éventuellement ajouter un switch au stack, et/ou convertir le switch en stack


***Sécurisation***

   * désactiver les VLAN qui n'on pas à être routé hors infra Openstack  du VSS (173, 174, 176, 177, et 179)
   * limiter l'accès au VLAN 420 au VLAN DISI, et éventuellement aux serveurs d'admin DISI. En tout cas, supprimer l'accès depuis campus distant. A faire après le 23 août, car ça m'a bien servi pour accéder aux carte idrac et aux switches en urgence pendant mes congés


***Autre***:

   * sur stack-d01-01 : Rétablir VTP. VTP client actif mais il ne connait pas tous les Vlans. Il semble que la communication VTP soit stoppée depuis le 8-6-20 13:07:05..  Réactiver le VTP, ou passer sur un mode de définition manuelle des VLANs.


***Accès aux interfaces d'Openstack  et outils de monitoring (Vlan 175) pour DISI et Prestataire***

   * rendre le Vlan 175 accessible depuis le Vlan DISI, pour l'accès aux différents outils d'admin et supervision
   * si possible rendre le VLAN 175 accessible depuis le Vlan 172. pour permettre l'administration depuis une machine passerelle hors réseau DISI, pour le prestatire
   * en attenant, un RP Haproxy a été mis en place, et des IP dans le VLAN 172 sont utilisées. Le RP a donc un accès directe au Vlan 172, ce qui n'est pas très sécurisant.




# Actions réalisées

   * ~~ rendre les vlans 174, 176, 177, 179 accessibles entre les switch DELL et les switchs CISCO~~
   * ~~définir une IP publique~~
   * définir le modèle à mettre en place : NAT ou Proxy  -> Modèle Proxy avec HA Proxy Retenu
       * ~~mettre en place un serveur HAproxy, avec une pâte dans réseau public (Vlan Bastion 670), et une pâte dans le Vlan 175 (ou un accès au Vlan 175)~~
       * ~~mettre en place une déclaration DNS *.openstack.imt-atlantique.fr" vers l'IP externe "192.108.117.56"~~
   * *Config du "Vlan 173"*
       * ~~investiguer pour comprendre le pb de ping qui ne passe pas sur le Vlan 173~~
       * ~~valider vlan 173  accessible entre les switch Cisco, et entre les switchs DELL et les switchs CISCO~~
   * 
