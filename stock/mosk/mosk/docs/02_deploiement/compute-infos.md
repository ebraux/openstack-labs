


| Name | IP  |bareMetalHostProfile | l2TemplateSelector |
| --- | --------------- | ----------- |
| br-mosk001-cmp-0 | 10.129.172.109 | mosk-compute-ceph | br-mosk001-6nic-cmp-ceph |
| br-mosk001-cmp-1 | 10.129.172.110 | mosk-compute-ceph | br-mosk001-6nic-cmp-ceph |
| br-mosk001-cmp-2 | 10.129.172.111 | mosk-compute-lvm | br-mosk001-8nic-cmp-lvm |
| br-mosk001-cmp-3 | 10.129.172.112 | mosk-compute-lvm | br-mosk001-8nic-cmp-lvm |



## default-br-mosk001
- disque #1
  - partition bios_grub 4M
  - partition uefi 204M
  - partition "config" 64M
- disque #2
  - 1 partitions pour VG : 
    - lvm_lvp : 1 LV /dev/lvm_lvp/lvp 130G
- disque #3
    - lvm_root : 1 LV /dev/lvm_root/root mini 30Go


## mosk-compute-ceph
- 1 disque
  - partition bios_grub 4M
  - partition uefi 204M
  - partition "config" 64M
  - 2 partitions pour VG : 
    - lvm_lvp : 1 LV /dev/lvm_lvp/lvp 35G
    - lvm_root : 1 LV /dev/lvm_root/root le reste

```bash
sudo fdisk -l
Disk /dev/sda: 446.6 GiB, 479559942144 bytes, 936640512 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: AA5837D2-8BFD-4715-9A4F-553AF6783AEC

Device        Start       End   Sectors   Size Type
/dev/sda1      2048     10239      8192     4M BIOS boot
/dev/sda2     10240    428031    417792   204M EFI System
/dev/sda3    428032    559103    131072    64M Linux filesystem
/dev/sda4    559104  73959423  73400320    35G Linux filesystem
/dev/sda5  73959424 936638463 862679040 411.4G Linux filesystem


Disk /dev/mapper/lvm_root-root: 411.4 GiB, 441689571328 bytes, 862674944 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/lvm_lvp-lvp: 35 GiB, 37576769536 bytes, 73392128 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```


## mosk-compute-lvm 
- disque #1
  - partition bios_grub 4M
  - partition uefi 204M
  - partition "config" 64M
  - 2 partitions pour VG : 
    - lvm_lvp : 1 LV /dev/lvm_lvp/lvp 35G
    - lvm_root : 1 LV /dev/lvm_root/root le reste

- disque #2
  - 1 partitions pour VG : 
    - nova-vol : 1 LV nova_fake
    
```bash
sudo fdisk -l
Disk /dev/sda: 136.1 GiB, 146163105792 bytes, 285474816 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 0A6819B5-71EC-4E53-A197-16C768360D1A

Device        Start       End   Sectors   Size Type
/dev/sda1      2048     10239      8192     4M BIOS boot
/dev/sda2     10240    428031    417792   204M EFI System
/dev/sda3    428032    559103    131072    64M Linux filesystem
/dev/sda4    559104  73959423  73400320    35G Linux filesystem
/dev/sda5  73959424 285472767 211513344 100.9G Linux filesystem


Disk /dev/sdb: 136.1 GiB, 146163105792 bytes, 285474816 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 68870DDA-A66D-4856-969A-DCE7B4F136F5

Device     Start       End   Sectors   Size Type
/dev/sdb1   2048 285472767 285470720 136.1G Linux filesystem


Disk /dev/mapper/nova--vol-nova_fake: 1 GiB, 1073741824 bytes, 2097152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/lvm_root-root: 100.9 GiB, 108292734976 bytes, 211509248 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/lvm_lvp-lvp: 35 GiB, 37576769536 bytes, 73392128 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```


```bash
sudo vgdisplay
  --- Volume group ---
  VG Name               nova-vol
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               136.12 GiB
  PE Size               4.00 MiB
  Total PE              34847
  Alloc PE / Size       256 / 1.00 GiB
  Free  PE / Size       34591 / 135.12 GiB
  VG UUID               QLW8XV-sgxl-yhZe-Yd1U-XriH-tkbs-G1RJI4
   
  --- Volume group ---
  VG Name               lvm_root
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <100.86 GiB
  PE Size               4.00 MiB
  Total PE              25819
  Alloc PE / Size       25819 / <100.86 GiB
  Free  PE / Size       0 / 0   
  VG UUID               Z9ooIV-8S82-lymj-ix0e-h5ze-tVbG-IWzDon
   
  --- Volume group ---
  VG Name               lvm_lvp
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <35.00 GiB
  PE Size               4.00 MiB
  Total PE              8959
  Alloc PE / Size       8959 / <35.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               Ljl02e-CiTB-rtOi-PUJ6-AmaU-Cc99-zG3ZnL
```


```bash
sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/nova-vol/nova_fake
  LV Name                nova_fake
  VG Name                nova-vol
  LV UUID                pYN1Rf-jLCw-fxfY-LCPb-S1m9-eI2J-swaLoI
  LV Write Access        read/write
  LV Creation host, time ipa-5b28276a-c5be-4fb9-8ca6-bdd344955327, 2021-09-15 13:49:32 +0000
  LV Status              available
  # open                 1
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/lvm_root/root
  LV Name                root
  VG Name                lvm_root
  LV UUID                0v5IIh-eFSt-rG3p-1noZ-dZvC-O926-S1IITV
  LV Write Access        read/write
  LV Creation host, time ipa-5b28276a-c5be-4fb9-8ca6-bdd344955327, 2021-09-15 13:49:19 +0000
  LV Status              available
  # open                 1
  LV Size                <100.86 GiB
  Current LE             25819
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/lvm_lvp/lvp
  LV Name                lvp
  VG Name                lvm_lvp
  LV UUID                zUkqrS-0oWd-dXvA-w319-mjh1-bd0V-jhyprG
  LV Write Access        read/write
  LV Creation host, time ipa-5b28276a-c5be-4fb9-8ca6-bdd344955327, 2021-09-15 13:49:26 +0000
  LV Status              available
  # open                 1
  LV Size                <35.00 GiB
  Current LE             8959
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
```