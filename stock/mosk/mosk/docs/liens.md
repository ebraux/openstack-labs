

## Cluster
https://docs.mirantis.com/container-cloud/latest/deployment-guide/deploy-bm-mgmt.html
https://docs.mirantis.com/container-cloud/latest/deployment-guide/deploy-bm-mgmt/bm-mgmt-deploy.html
https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/upgrade-mgmt.html

# operations
https://docs.mirantis.com/container-cloud/latest/operations-guide.html
https://docs.mirantis.com/mos/latest/ops/openstack-operations.html


## BM Host
https://docs.mirantis.com/container-cloud/latest/api/bm/baremetalhost.html#baremetalhost
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/create-bm-hostprofile.html#create-bm-hostprofile
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/create-bm-hostprofile/create-host-profile.html

## Machine
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-os/add-machine-os.html
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/match-machine-to-bmh.html
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/delete-machine.html
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/nic-mapping-override.html#nic-mapping-override

## L2template
https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/adv-nw-config/create-l2.html#create-l2

## autres config
https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/conf-ntp.html


## Openstack

https://docs.mirantis.com/mos/latest/deploy/deploy-openstack.html


## Support 
https://docs.mirantis.com/mcp/q4-18/mcp-operations-guide/tshooting/generate-sosreport.html