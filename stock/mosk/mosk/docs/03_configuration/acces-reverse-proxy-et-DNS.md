# Endpoint publiques  : reverse proxy et DNS

## Principe

Les endpoints publics et services accessibles aux utilisateurs (comme le Dashboard Horizon) sont déployés en tant que service kubernetes, et dont accessible à travers une IP dans le Vlan 175 (k8s Metal_LB).

L'ip utilisée est la 10.129.175.206. Cette IP n'est pas accessible depuis les réseaux de l'école. 

Une Ip publique a donc été dédiée aux accès Openstack : 192.108.117.56.

Une instance de HA proxy : 

- assure reverse proxy vers l'IP dans le Vlan 175,
- gère des droits d'accès par restriction d'IP aux API.

Les déclaration DNS correspondants aux service et Endpoint publiques pointent vers l'IP publique.

--- 

## Déclarations DNS

Liste des endpoint publics des API :
```bash
openstack endpoint list -f value -c URL  --interface public
---
https://octavia.openstack.imt-atlantique.fr
https://placement.openstack.imt-atlantique.fr/
https://glance.openstack.imt-atlantique.fr
https://cloudformation.openstack.imt-atlantique.fr/v1
https://designate.openstack.imt-atlantique.fr/
https://nova.openstack.imt-atlantique.fr/v2.1/%(tenant_id)s
https://cinder.openstack.imt-atlantique.fr/v2/%(tenant_id)s
https://cinder.openstack.imt-atlantique.fr/v3/%(tenant_id)s
https://barbican.openstack.imt-atlantique.fr/
https://heat.openstack.imt-atlantique.fr/v1/%(project_id)s
https://keystone.openstack.imt-atlantique.fr/v3
https://neutron.openstack.imt-atlantique.fr
https://object-store.openstack.imt-atlantique.fr/swift/v1/AUTH_$(project_id)s
```

Liste des service WEB :
- horizon.openstack.imt-atlantique.fr
- novncproxy.openstack.imt-atlantique.fr

Ce qui donne la liste : 
```bash
barbican.openstack.imt-atlantique.fr
cinder.openstack.imt-atlantique.fr
cloudformation.openstack.imt-atlantique.fr
designate.openstack.imt-atlantique.fr
glance.openstack.imt-atlantique.fr
heat.openstack.imt-atlantique.fr
horizon.openstack.imt-atlantique.fr
keystone.openstack.imt-atlantique.fr
neutron.openstack.imt-atlantique.fr
nova.openstack.imt-atlantique.fr
novncproxy.openstack.imt-atlantique.fr
object-store.openstack.imt-atlantique.fr
octavia.openstack.imt-atlantique.fr
placement.openstack.imt-atlantique.fr
```

---

## Accès par reverse proxy

### Configuration des adresses en reverse-proxy
Reverse proxy mis en place, lien entre le réseau MetalLB-K8s de MOSK (subnet 10.129.175.0/24, Vlan 175, ) et le reste du monde :

| Fonction | IP publiée | IP cible MetaLB | Scope|
| -------- | ---------- | --------------- | ---- |
| api openstack | [http://10.129.175.202/](http://10.129.175.202/) redirige vers 10.129.175.206 | Interne IMT |
| Grafana (Graphes perf) | [http://10.129.172.15/](http://10.129.172.15/). redirige vers 10.129.175.202 | DISI |
| ELK (logs) | [http://10.129.172.16/](http://10.129.172.16/). redirige vers 10.129.175.203 | DISI |
| Prometheus (metrics) | [http://10.129.172.17/](http://10.129.172.17/). redirige vers 10.129.175.204 | DISI |

### Architecture mise en place

Géré par un serveur physique.

- nom : esxi-free-04 
- IP principale : 10.29.10.28/24 sur eno1
- IPs pour accès "externes"
  - 192.108.117.56/26, vlan 670, sur en02
  - 10.129.172.3/24, vlan 172, sur eno3
- IPs vers Vlan internes
  - 10.129.175.3/24, vlan 175, sur eno3


NIC1 : IP principale - D1-8, port 3
```bash
interface GigabitEthernet0/3
 description WEB-RP-01 NIC1
 switchport access vlan 410
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast trunk
```

NIC2 : Vlan acces IMT -  D1-8, port 4
```bash
interface GigabitEthernet0/4
 description WEB-RP-01 NIC2
 switchport trunk allowed vlan 1,670
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast trunk
```


* NIC3 : VLAn MOSK, dont 175 - stack-d01-02.net.enst-bretagne.fr port 2/19
```bash
interface GigabitEthernet2/0/19
 description "Openstack MOSK-BOND"
 switchport trunk allowed vlan 173-179
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
```

* NIC4 : VLAN Common/PXE - sw-d01-04.net.enst-bretagne.fr 18
```bash
interface GigabitEthernet1/0/18
 description "Openstack MOSK-NIC0"
 switchport access vlan 172
 switchport trunk allowed vlan 172
 switchport mode trunk
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast edge trunk
```



backup 

RP02 :
Nic1  D1-31 port 7
Nic2  D1-31 port 8


```bash
interface GigabitEthernet1/0/7
 switchport access vlan 666
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast
!         
interface GigabitEthernet1/0/8
 switchport access vlan 666
 switchport mode access
 storm-control broadcast level 5.00
 storm-control multicast level 5.00
 spanning-tree portfast
```