# Configuration Réseau des Hosts : L2Template


- [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/nic-mapping-override.html#nic-mapping-override](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/nic-mapping-override.html#nic-mapping-override)
- [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/adv-nw-config/create-l2.html#create-l2](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/adv-nw-config/create-l2.html#create-l2)


Configuration de l'environnement
```bash
cd ~/MCC2.9/kaas-bootstrap
export KUBECONFIG=kubeconfig.yml
```

```bash
kubectl -n br-mosk001 get l2template
---
NAME                                          AGE   STATUS
br-mosk001-6nic-cmp-ceph                      12d   Ready
br-mosk001-6nic-master                        12d   Ready
br-mosk001-8nic-ceph                          12d   Ready
br-mosk001-8nic-cmp-lvm                       12d   Ready
region-one-preinstalled-2-nic                 12d   Ready
region-one-preinstalled-4-nic                 12d   Ready
region-one-preinstalled-4-nic-fixed-mapping   12d   Ready
region-one-preinstalled-6-nic                 12d   Ready
region-one-preinstalled-single-1              12d   Ready
```

Afficher une template
```bash
kubectl -n br-mosk001 get l2template  br-mosk001-8nic-cmp-lvm -o yaml |less
```

Q : où est sont défini les network, notament le "lcm-nw" 

## br-mosk001-8nic-cmp-lvm
- 8 NIC
- NIC0 dédiée à l'accées PXE/Management
   - IPV4, pas de DHCP
   - sur réseau "lcm-nw"
   - paserelle sur réseau "lcm-nw"}}
   - nameservers_from_subnet "lcm-nw"}}
   - mtu: 1500
- toutes les autres NIC (de 1 à 7), pas de config spéciqes, car géré vie des bond. MTU 1500
- 1 Bond Créée : BOND0
  - NIC1 et NIC2
  - mode: active-backup
  - mtu: 1500
  - Vlans 
    - pr-floating: 178
    - k8s-ext-v: 175
    - k8s-pods-v: 174
    - ceph-cl-v: 176
    - ceph-rep-v: 177
    - tenant: 179




