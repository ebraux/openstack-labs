
## memory et CPU limites pour prometheusEsExporter

Modification d ela config du cluster

```bash
kubectl -n br-mosk001  edit cluster kaas-br-mosk001
```


Ajouter les "requests" et "limits" pour "prometheusEsExporter"
```yaml
          resources:
            prometheusEsExporter:
              requests:
                cpu: "50m"
                memory: "100Mi"
              limits:
                cpu: "60m"
                memory: "200Mi"
```

Vérifier
Les limites sont visibles dans le replicaset associé au pod.

kubectl -n stacklight   get Deployment prometheus-es-exporter -o yaml | grep ReplicaSet


Docs :
- https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-sl/config/sl-config.html
- https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-sl/config/sl-config-params.html#resource-limits