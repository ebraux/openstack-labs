# Principe de configuration


## Gestion de la configuraion

C'est avant tout du Kubernetes, donc toute la configuration se fait sous forme de déclaration dans un fichier yaml, qu'il faut importer.
K_s se charge ensuite de l'application de la configuratipon dns le clster Openstack.


## Configurer l'environnement

La configuration se fait depuis un des "Master" du cluster MOSK : 10.129.172.100,  10.129.172.101 ou 10.129.172.102

Par défaut, on utilise la 10.129.172.100.

On s'y connecte depui la macine `mcc-seed-01`

Connexion à mcc-seed-01
```bash
ssh root@10.129.172.14
```

Connexion au Master K8s
```bash
cd ~/MCC2.9/kaas-bootstrap
ssh -v -o "IdentitiesOnly=yes"  -i ssh_key mcc-user@10.129.172.100
```

Rem : le nombre de tentatives de connexion est limité à 4 . si plusieurs clés sont chargé dans votre l'agent SSH, la connexion échoue.
pour forcer l'utilisation de la clé, sans passer par les clés stockées dans l'agent on utilise l'option `-o "IdentitiesOnly=yes"`
```

---


## Comment idenfifier les machines "Master" du cluster MOSK

Après installation de MCC, il fait récupérer sur la machine qui a permi de déployer MCC ()
- le fichier  kubeconfig
- la clé ssh.
  
Docs : 
* [https://docs.mirantis.com/container-cloud/latest/operations-guide/connect-cluster.html#connect-cluster] (https://docs.mirantis.com/container-cloud/latest/operations-guide/connect-cluster.html#connect-cluster)

Installer kubectl
```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```

Configurer l'accés au cluster MCC
```bash
export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig-MCC.yml
```

Lister les machines disponibles
```bash
kubectl get nodes -o wide
```
```bash
kubectl get nodes -o wide
NAME                                             STATUS   ROLES    AGE   VERSION                                INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
kaas-node-34412b23-0e4b-42c1-949c-4f20d79fc858   Ready    master   95d   v1.20.1-mirantis-1-3-g71afa0ba032bfd   10.129.172.102   <none>        Ubuntu 18.04.4 LTS   4.15.0-112-generic   docker://20.10.5
kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0   Ready    master   95d   v1.20.1-mirantis-1-3-g71afa0ba032bfd   10.129.172.100   <none>        Ubuntu 18.04.4 LTS   4.15.0-112-generic   docker://20.10.5
kaas-node-d09c78aa-00c3-40bc-b44d-957e015a8e51   Ready    master   95d   v1.20.1-mirantis-1-3-g71afa0ba032bfd   10.129.172.101   <none>        Ubuntu 18.04.4 LTS   4.15.0-112-generic   docker://20.10.5
```

