 Adapter le fonctionnement de Nova via  osdpl
 
```bash
kubectl -n openstack get osdpl -oyaml
```

Modfification du service `compute`
``` yaml
  services:
    compute:
      nova:
        values:
          conf:
            nova:
              filter_scheduler:
                host_subset_size: 3
``` 

Modification du host_subset_size, par défaut à 30, passé à 3.

"Once adjusted nova-scheduler-* pods should restart and you can verify the new value by looking at /etc/nova/nova.conf in one of the restarted pods"

``` bash
kubectl -n openstack get pods | grep nova-scheduler
nova-scheduler-b44cb5b4-7nc2b                                  1/1     Running     0          7m18s
nova-scheduler-b44cb5b4-bmsqp                                  1/1     Running     0          8m
nova-scheduler-b44cb5b4-lghnq                                  1/1     Running     0          6m41s
```
kubectl -n openstack exec -t nova-scheduler-b44cb5b4-7nc2b  -- cat /etc/nova/nova.conf | grep host_subset_size

- [https://docs.openstack.org/ocata/config-reference/compute/config-options.html](https://docs.openstack.org/ocata/config-reference/compute/config-options.html)
- [https://thesaitech.wordpress.com/2017/10/02/nova-scheduler-tuning/](https://thesaitech.wordpress.com/2017/10/02/nova-scheduler-tuning/)