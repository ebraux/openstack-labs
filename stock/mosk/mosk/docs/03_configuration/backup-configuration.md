


Vérifier l'état du backup

kubectl -n openstack get cronjob mariadb-phy-backup
NAME                 SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
mariadb-phy-backup   0 1 * * *   True      0        <none>          62d


- [https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database.html](https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database.html)



# Enable the backup in the OpenStackDeployment object:

- [https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database/configure-backup.html](https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database/configure-backup.html)

Dans le Child Cluster (MOSK)
```bash
kubectl -n openstack get  openstackdeployment 
kubectl -n openstack get  openstackdeployment  -o yaml 

kubectl -n openstack edit  openstackdeployment 
```


kubectl -n openstack get openstackdeployment $(kubectl -n openstack get openstackdeployment | tail -1 | awk '{print $1}') -o yaml | grep openstack_version |tail -1

```bash
spec:
  features:
    database:
      backup:
        enabled: true
```

