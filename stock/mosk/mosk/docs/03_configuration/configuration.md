

## Principe

La configuration du cluster se fait en modifiant via kubconfig
```bash
kubectl --kubeconfig <kubeconfigPath> edit -n <projectName> cluster <managementClusterName>
```

```bash
kubectl -n br-mosk001 edit bmh 
kubectl -n br-mosk001 edit machine
```

Les machines sont déployées en fonction de "profils" (baremetalhostprofile.)

Avant de pouvoir êtr déployée, il faut qu'elle soient enregistrée en tant que "BareMetalHost", et qu'elles soient ensuite inspéctée et enregistrée dans le systémes

On déploie enfin une "Machine", en associant un "BareMetalHost" avec un "baremetalhostprofile".

- Création d'un 'baremetalhostprofile' : [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/create-bm-hostprofile.html#create-bm-hostprofile]
- Création d'un 'BareMetalHost' :
  - avec l'UI  
  - en ligne de commande : [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-bm-host-to-managed/add-bm-host-cli.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-bm-host-to-managed/add-bm-host-cli.html)


## Commande

A executer depuis un des 3 noueud de getsion du cluster Openstack: 

Configuration de l'environnement
```bash
cd ~/MCC2.9/kaas-bootstrap
export KUBECONFIG=kubeconfig.yml
```




## Gestion des clusters

```bash
kubectl -n br-mosk001 delete cluster --all
```


- a way to modify srd to include
  - ldap
  - nova conf (example from the deployment)
- documentation links: 
  - https://docs.mirantis.com/mos/latest/deploy/deploy-openstack/deploy-cluster.html
  - cdr osdpl - https://docs.mirantis.com/mos/latest/ref-arch/openstack/openstack-operator/osdpl-cr/standard-conf.html#general-osdpl-cr
  - [https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/conf-ntp.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/manage-mgmt/conf-ntp.html)


Les config d'initilisation sont dans #configurations#, du namespace "Openstack"

On retouve par exemple la configMap "neutron-bin";, qui contient la config des subnets créés

```bash
#!/bin/bash
set -ex
openstack network show public || openstack network create \
  --provider-network-type flat \
  --provider-physical-network physnet1 \
  --external \
  public
# set default gateway via any non-master/worker node to get into ingress controller
openstack subnet show public-subnet || openstack subnet create  public-subnet \
  --no-dhcp --subnet-range  10.129.176.0/22 \
  --allocation-pool start=10.129.176.4,end=10.129.179.254 \
  --gateway 10.129.176.1 \
  --network public
openstack router show r1 || openstack router create r1
openstack router set --external-gateway public r1
openstack network set public --external  --default 
```

ou "nova-bin" por les gabarits par défaut


--- 



