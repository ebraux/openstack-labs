# Configuration Réseau des Hosts : L2Template


- [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/nic-mapping-override.html#nic-mapping-override](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/add-machine-to-managed/add-machine-cli/nic-mapping-override.html#nic-mapping-override)
- [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/adv-nw-config/create-l2.html#create-l2](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/operate-managed-bm/adv-nw-config/create-l2.html#create-l2)


Configuration de l'environnement
```bash
export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig.yml
```

```bash
kubectl -n br-mosk001 get l2template
---
NAME                                          AGE   STATUS
br-mosk001-6nic-cmp-ceph                      12d   Ready
br-mosk001-6nic-master                        12d   Ready
br-mosk001-8nic-ceph                          12d   Ready
br-mosk001-8nic-cmp-lvm                       12d   Ready
region-one-preinstalled-2-nic                 12d   Ready
region-one-preinstalled-4-nic                 12d   Ready
region-one-preinstalled-4-nic-fixed-mapping   12d   Ready
region-one-preinstalled-6-nic                 12d   Ready
region-one-preinstalled-single-1              12d   Ready
```

Afficher une template
```bash
kubectl -n br-mosk001 get l2template  br-mosk001-8nic-cmp-lvm -o yaml |less
```

Importer une nouvelle template
```bash
cd ~/MCC2.9/kaas-bootstrap/br-mosk-01/l2template
kubectl -n br-mosk001 apply -f l2template-4nic-cmp-lvm.yaml
```
Q : où est sont défini les network, notament le "lcm-nw" 

## Choix de la L2template

## Pour machine de type MASTER/CTRLPlane, avec 6 NICs

- 1 carte réseau fille SFP+ 4 ports
- 1 carte réseau additioennelle SFP+ 2ports

br-mosk001-6nic-master :
- NIC0 
  - indépendante
  - mtu: 1500
  - VLANS 172
- Bond0
  - NIC1 et NIC4, 
  - mode: active-backup
  - mtu: 1500
  - VLANs : 174 (k8s-pods-v), 175 (k8s-ext-v), 176 (ceph-cl-v)
- Bond1
  - NIC2 et NIC5, 
  - mode: active-backup
  - mtu: 1500
  - Vlan: 177(ceph-rep-v), 178(pr-floating), 179 (tenant)
  
kaas-br-mosk001-br-mosk001-8nic-cmp-lvm


## Pour machine de type COMPUTE , avec 8 NICs
- 1 carte réseau fille RJ45 4 ports
- 1 carte réseau RJ45 4ports

br-mosk001-8nic-cmp-lvm
- NIC0 
  - indépendante
  - mtu: 1500
  - VLANS 172
- Bond0
  - NIC1 et NIC2, 
  - mode: active-backup
  - mtu: 1500
  - VLANs : 174 (k8s-pods-v), 175 (k8s-ext-v), 176 (ceph-cl-v), 177(ceph-rep-v), 178(pr-floating), 179 (tenant)

## Pour machine de type COMPUTE , avec 4 NICs





