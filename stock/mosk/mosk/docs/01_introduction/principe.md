

# architecture

Openstack est déployé sur un cluster Kubernetes, qui est chargé de gérer le bon fonctionnement es composants

- chaque service est déployé sous forme d'image Docker
- Kubernetes Ochestre le fonctionnement
 
Mirantis propose un outil de getsion de cluster Kubernetes : MCC

A partir de MCC, un cluster Kubernetes a été déployé pour gérer l'infra Openstack.

L'architecture standard recommandée est :

- 3 noeuds pour MCC
- 3 noueds "Master K8S" pour le cluster MOSK
- 3 noeuds "Worker Control Plane Openstack"
- 5 Noeuds "Worker CEPH"
- des noeuds "Worker Compute"
Les noeuds doivnet disposer de capacité disque importante, et rapide de type SSD

Dans notre cas, nous ne disposions pas d'un nombre de machine suffisnats. Ni de disques SSD.

Nous avons mis en place une infrastructure spécifique avec ressources limités:

- 3 noeuds pour MCC
- 3 noueds "Master K8S / Worker Control Plane Openstack" pour le cluster MOSK
- 5 Noeuds "Worker CEPH"
- des noeuds "Worker Compute"


Impact :
- Le déploiement et les actions de modifcation peuvent êtr lentes.
- La rétention delogs est limité à quelques jours.

Le cluster a été optimisé par les équipes de Mirantis, pour êter pleinement fonctionnel, avec des resource slmitées.

Nous ne somme pas un cas isolé.  Ils continuent à travailler sur des déploiement de petits cluster comme le notre.

