

source set-mosk.sh 

## Modifification de la config

Dans le Child Cluster (MOSK)
```bash
kubectl -n openstack edit deployment horizon
```

Ajouter des variables, par exemple le proxy :
```bash
     env:
        - name: MY_POD_IP
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: status.podIP
        - name: HTTPS_PROXY
          value: http://proxy.enst-bretagne.fr:8080
        - name: HTTP_PROXY
          value: http://proxy.enst-bretagne.fr:8080
        - name: NO_PROXY
          value: .svc,.svc.cluster.local,10.233.128.0/18,10.233.64.0/18,127.0.0.1,cluster.local,docker,localhost,openstack.imt-atlantique.fr,registry.internal.lan
        - name: http_proxy
          value: http://proxy.enst-bretagne.fr:8080
        - name: https_proxy
          value: http://proxy.enst-bretagne.fr:8080
        - name: no_proxy
          value: .svc,.svc.cluster.local,10.233.128.0/18,10.233.64.0/18,127.0.0.1,cluster.local,docker,localhost,openstack.imt-atlantique.fr,registry.internal.lan
        image: 127.0.0.1:44301/openstack/horizon:ussuri-bionic-20210910182329
```