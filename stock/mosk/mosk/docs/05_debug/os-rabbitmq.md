

```bash
Problème initial : gros pic de traffic réseau sur l'interface
vxlan_sys_4789 de certains compute et répercussion sur les interface
réseau des os-ctrl (surtout le 4 à cause de la centralisation des log !?)

Intervention de reboot hard sur les 3 noeud avec un temps entre chaque
reboot

Problème constaté ensuite : nova et neutron indique des timeout au
niveau des messages (files rabbitmq)

arret de 2 rabbitmq sur 3  => pas d'amélioration
voir même une perte des agents neutron (XXX dans colonne Alive lors d'un
openstack network agent list)

Pour fixer temporairement le problème, il faut :

Se connecter sur les lxc rabbitmq et arrêter l'un après l'autre les
rabbitmq via la commande :
$ rabbitmqctl stop_app

il faut le faire pour les 3 puis il faut redémarrer les rabbitmq en
commençant par le dernier arrêté via la commande :
$ rabbitmqctl start_app

on peut faire un rabbitmqctl cluster_status pour voir l'état du cluster

on peut également vérifier si les neutron agent sont de nouveau
accessible via openstack neutron agent list

Normalement, le lancement d'une stack heat devrait fonctionner.
```

---

``` bash
Voici la liste en vracs des commandes utilisées pour rabbitmq :

sur le rqbbitmq@os-ctrl-05

# rabbitmqctl status
# rabbitmqctl cluster_status
# rabbitmqctl list_connections
# rabbitmqctl list_vhosts
# rabbitmqctl list_queues -p neutron

# for i in `rabbitmqctl list_vhosts`; do rabbitmqctl list_queues -p $i
|awk '{print $2" "$1}'; done | sort -n

# rabbitmqctl stop_app
# rabbitmqctl reset
# rabbitmqctl join_cluster rabbit@os-ctrl-04-rabbit-mq-container-891b027d
# rabbitmqctl start_app

si le noeud distant rale parce qu'il connait déjà le nouveau noeud,
alors il faut se connecter sur le noeud déjà dans le cluster et lancer
la commande forget_cluster_node
sur rabbit@os-ctrl-04-rabbit-mq-container-891b027d
# rabbitmqctl -n rabbit@os-ctrl-04-rabbit-mq-container-891b027d
forget_cluster_node rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb


pour voir le nombre de connexion sur tous les serveurs rabbitmq :
# ansible rabbit_mq_container  -m shell -a "ss -taupn | grep 5671 |wc -l "

pour avoir l'état du cluster galera
# ansible galera_container -m shell -a "mysql -h localhost -e 'show
status like \"%wsrep_cluster_%\";'"

Voir l'état des connexions/requêtes sur mysql
en se connectant sur un des conteneurs galera, lancer la commande
# mysql -e "show processlist;"
# mysql -e "show full processlist;"


```

```