

```bash
kubectl -n br-mosk001 get BareMetalHostprofile
---
NAME                 REGION       DEFAULT
default                           true
default-br-mosk001   region-one   true
mosk-ceph            region-one   
mosk-compute-ceph    region-one   
mosk-compute-lvm     region-one   
mosk-master          region-one   
```


 Important, la partie avec la définition de devices
 
 - 1 disque, découpé en 5 partitions
 - la dernière partition est en mode "auto-extend" (sizeGiB: 0)
 ```yaml
 spec:
  devices:
  - device:
      minSizeGiB: 60
      wipe: true
    partitions:
    - name: bios_grub
      partflags:
      - bios_grub
      sizeGiB: 0.00390625
    - name: uefi
      partflags:
      - esp
      sizeGiB: 0.2
    - name: config-2
      sizeGiB: 0.0625
    - name: lvm_lvp_part
      sizeGiB: 35
      wipe: true
    - name: lvm_root_part
      sizeGiB: 0
      wipe: true
 ```
création de 2 VG
 
```yaml
volumeGroups:
  - devices:
    - partition: lvm_lvp_part
    name: lvm_lvp
  - devices:
    - partition: lvm_root_part
    name: lvm_root
```

Et sur chaque VG, un LV
```yaml
   logicalVolumes:
  - name: root
    sizeGiB: 0
    type: linear
    vg: lvm_root
  - name: lvp
    sizeGiB: 0
    type: linear
    vg: lvm_lvp
 ```

Puis le découpage

 ```yaml
   fileSystems:
  - fileSystem: vfat
    partition: config-2
  - fileSystem: vfat
    mountPoint: /boot/efi
    partition: uefi
  - fileSystem: ext4
    logicalVolume: root
    mountPoint: /
  - fileSystem: ext4
    logicalVolume: lvp
    mountPoint: /mnt/local-volumes/
 ```
