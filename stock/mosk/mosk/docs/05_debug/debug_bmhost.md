# Gestion des "Baremetal Hosts"


impossible de supprimer un bmhost : supprimer annotation pause si existe
```yaml
metadata:
  annotations:
    baremetalhost.metal3.io/paused: baremetal-provider-lock
    kubectl.kubernetes.io/last-applied-configuration: |

```
Différents status d'un bmh
```bash
registering / inspecting / preparing / provisioning / provisioned /deprovisioning
```


```bash
kubectl -n br-mosk001 get bmh 
NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true             region-one
br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true             region-one
br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true             region-one
br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true             region-one
br-mosk001-cmp-1      provisioned   br-mosk001-cmp-1      UEFI       true             region-one
br-mosk001-cmp-2      provisioned   br-mosk001-cmp-2      UEFI       true             region-one
br-mosk001-cmp-3      provisioned   br-mosk001-cmp-3      UEFI       true             region-one
br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true             region-one
br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true             region-one
br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true             region-one
```


```bash
kubectl -n br-mosk001 get bmh 
kubectl -n br-mosk001 get bmh  br-mosk001-cmp-1 -o yaml |less
kubectl -n br-mosk001 edit bmh 
kubectl -n br-mosk001 delete bmh br-mosk001-cmp-4
```


---

## Gestion des "Baremetal Profils"

List de profils
```bash
kubectl -n br-mosk001 get baremetalhostprofile
---
NAME                 REGION       DEFAULT
default                           true
default-br-mosk001   region-one   true
mosk-ceph            region-one   
mosk-compute-ceph    region-one   
mosk-compute-lvm     region-one   
mosk-master          region-one   
```

```bash
kubectl -n br-mosk001 get baremetalhostprofile
kubectl -n br-mosk001 get baremetalhostprofile default-br-mosk001 -o yaml |less
kubectl -n br-mosk001 get baremetalhostprofile mosk-master mosk-compute-lvm mosk-compute-ceph mosk-ceph -o yaml |less
```

---


## exemples de config

### baremetalhost config
`kubectl.kubernetes.io/last-applied-configuration
```yaml 
{
  "apiVersion":"metal3.io/v1alpha1",
  "kind":"BareMetalHost",
  "metadata":
  {
      "annotations":{},
      "labels":
      {
          "baremetal":"hw-br-mosk001-cmp-0",
          "kaas.mirantis.com/provider":"baremetal",
          "kaas.mirantis.com/region":"region-one"
      },
      "name":"br-mosk001-cmp-0",
      "namespace":"br-mosk001"
  },
  "spec":
  {
      "bmc":
      {
          "address":"10.29.20.31:623",
          "credentialsName":"br-mosk001-cmp-0-bmc-secret"
      },
      "bootMACAddress":"e4:43:4b:ea:73:b0",
      "bootMode":"UEFI","online":true
  }
}
```

### baremetalhostprofile config
`kubectl.kubernetes.io/last-applied-configuration`
```yaml
{
  "apiVersion":"metal3.io/v1alpha1",
  "kind":"BareMetalHostProfile",
  "metadata":
  {
    "annotations":{},
    "creationTimestamp":"2021-06-24T01:55:13Z",
    "generation":1,
    "labels":
    {
      "kaas.mirantis.com/defaultBMHProfile":"true",
      "kaas.mirantis.com/region":"region-one"
    },
    "managedFields":
    [
      {
        "apiVersion":"metal3.io/v1alpha1",
        "fieldsType":"FieldsV1",
        "fieldsV1":
        {
          "f:metadata":
          {
            "f:labels":
            {
              ".":{},
              "f:kaas.mirantis.com/defaultBMHProfile":{},
              "f:kaas.mirantis.com/region":{}
            }
          },
          "f:spec":
          {
            ".":{},
            "f:devices":{},
            "f:fileSystems":{},
            "f:grubConfig":
            {
              ".":{},
              "f:defaultGrubOptions":{}
            },
            "f:kernelParameters":
            {
              ".":{},
              "f:sysctl":
              {
                ".":{},
                "f:fs.aio-max-nr":{},
                "f:fs.file-max":{},
                "f:fs.inotify.max_user_instances":{},
                "f:kernel.core_uses_pid":{},
                "f:kernel.dmesg_restrict":{},
                "f:kernel.panic":{},
                "f:vm.max_map_count":{}
              }
            },
            "f:logicalVolumes":{},
            "f:postDeployScript":{},
            "f:preDeployScript":{},
            "f:volumeGroups":{}
          },
          "f:status":{}
        },
        "manager":"kaas",
        "operation":"Update",
        "time":"2021-06-24T00:30:36Z"}
      ],
      "name":"default-br-mosk001",
      "namespace":"br-mosk001",
      "resourceVersion":"28235",
      "uid":"9f1a9453-f08d-4b77-a382-25d0a9a00622"
    },
    "spec":
    {
      "devices":
      [
        {
          "device":
          {
            "minSizeGiB":60,
            "wipe":true
          },
          "partitions":
          [
            {
              "name":"bios_grub",
              "partflags": ["bios_grub"],
              "sizeGiB":0.00390625
            },
            {
              "name":"uefi",
              "partflags": ["esp"],
              "sizeGiB":0.20000000298023224
            },
            {
              "name":"config-2",
              "sizeGiB":0.0625
            },
            {
              "name":"lvm_root_part"}
          ]
        },
        {
          "device":
          {
            "minSizeGiB":30,
            "wipe":true
          },
          "partitions":
          [
            {
              "name":"lvm_lvp_part",
              "sizeGiB":130
            }
          ]
        },
        {
          "device":
          {
            "minSizeGiB":30,
            "wipe":true
          }
        }
      ],
      "fileSystems":
      [
        {
          "fileSystem":"vfat",
          "partition":"config-2"
        },
        {
          "fileSystem":"vfat",
          "mountPoint":"/boot/efi",
          "partition":"uefi"
        },
        {
          "fileSystem":"ext4",
          "logicalVolume":"root","mountPoint":"/"
        },
        {
          "fileSystem":"ext4",
          "logicalVolume":"lvp",
          "mountPoint":"/mnt/local-volumes/"
        }
      ],
      "grubConfig":
      {
        "defaultGrubOptions":["GRUB_DISABLE_RECOVERY=\"true\"","GRUB_PRELOAD_MODULES=lvm","GRUB_TIMEOUT=20"]
      },
      "kernelParameters":
      {
        "sysctl":
        {
          "fs.aio-max-nr":"1048576",
          "fs.file-max":"9223372036854775807",
          "fs.inotify.max_user_instances":"4096",
          "kernel.core_uses_pid":"1",
          "kernel.dmesg_restrict":"1",
          "kernel.panic":"900",
          "vm.max_map_count":"262144"}
        },
        "logicalVolumes":
        [
          {
            "name":"root",
            "sizeGiB":0,
            "type":"linear",
            "vg":"lvm_root"
          },
          {
            "name":"lvp",
            "sizeGiB":0,
            "type":"linear",
            "vg":"lvm_lvp"
          }
        ],
        "postDeployScript":"#!/bin/bash -ex\necho $(date) 'post_deploy_script done' \u003e\u003e /root/post_deploy_done\n",
        "preDeployScript":"#!/bin/bash -ex\necho $(date) 'pre_deploy_script done' \u003e\u003e /root/pre_deploy_done\n",
        "volumeGroups":
        [
          {
            "devices":
            [
              {
                "partition":"lvm_root_part"
              }
            ],
            "name":"lvm_root"
          },
          {
            "devices":
            [
              {
                "partition":"lvm_lvp_part"
              }
            ],
            "name":"lvm_lvp"
          }
        ]
      }
    }
  }