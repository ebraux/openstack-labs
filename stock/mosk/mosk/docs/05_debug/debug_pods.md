


Afficher les containers associés (dont)
``` bash
kubectl -n rook-ceph get pod -o wide -l app=rook-ceph-osd | grep osd-1
NAME                              READY  STATUS           RESTARTS  AGE  IP              NODE          NOMINATED NODE   READINESS GATES
rook-ceph-osd-1-5f8569d467-jsspq   1/1  Running           224       15h  10.129.172.108  kaas-node-xx  <none>           <none>
rook-ceph-osd-10-6b65d6fc84-6bk6m  0/1  CrashLoopBackOff  230       16h  10.129.172.106  kaas-node-xx  <none>           <none>
rook-ceph-osd-11-6b6b48fc48-74xqx  0/1  CrashLoopBackOff  231       16h  10.129.172.107  kaas-node-xx  <none>           <none>
rook-ceph-osd-12-686f986849-d648t  1/1  Running             0       16h  10.129.172.106  kaas-node-xx  <none>           <none>
rook-ceph-osd-13-86949b57b9-7jkkv  1/1  Running             0       16h  10.129.172.107  kaas-node-xx  <none>           <none>
rook-ceph-osd-14-5f8f8b68f7-jzxk2  1/1  Running             0       16h  10.129.172.106  kaas-node-xx  <none>           <none>
rook-ceph-osd-15-7f898f8d79-q2zps  1/1  Running             0       16h  10.129.172.107  kaas-node-xx  <none>           <none>
rook-ceph-osd-16-9b4f9f967-rzcnv   1/1  Running             0       16h  10.129.172.107  kaas-node-xx  <none>           <none>
rook-ceph-osd-17-7dc6bd766c-kvtlj  1/1  Running             0       16h  10.129.172.107  kaas-node-xx  <none>           <none>
```

Récupérer les logs
``` bash
kubectl -n rook-ceph logs rook-ceph-osd-11-6b6b48fc48-74xqx > /tmp/rook-ceph-osd-11-6b6b48fc48-74xqx.logs
```