

# 16 -> 18

- Stacklight Bug : [https://docs.mirantis.com/mos/latest/release-notes/21.4/known-issues/update.html](https://docs.mirantis.com/mos/latest/release-notes/21.4/known-issues/update.html)
 
- issues fixed for this version : [https://docs.mirantis.com/mos/latest/release-notes/21.5/known-issues/update.html](https://docs.mirantis.com/mos/latest/release-notes/21.5/known-issues/update.html)

- Other fixed issues :
      -  [https://docs.mirantis.com/container-cloud/latest/release-notes/releases/2-11-0/known-2-11-0.html](https://docs.mirantis.com/container-cloud/latest/release-notes/releases/2-11-0/known-2-11-0.html)
      -  [https://docs.mirantis.com/container-cloud/latest/release-notes/releases/2-11-0/managed-upgrade-2-11-0.html](https://docs.mirantis.com/container-cloud/latest/release-notes/releases/2-11-0/managed-upgrade-2-11-0.html)

---


please remove any sensitive information/creds from the output if any
```bash
kubectl --kubeconfig ~/configs/MCC/kubeconfig  get cluster kaas-br-mosk001 -n br-mosk001 -oyaml
kubectl --kubeconfig ~/configs/MCC/kubeconfig  get helmbundle kaas-br-mosk001 -n br-mosk001 -oyaml
```

from your MOS cluster also will help
```bash
kubectl get all -n stacklight
kubectl get pvc -n stacklight
```

```bash
kubectl get po -n stacklight -owide
kubectl get pvc -n stacklight -ojson
kubectl get pv -ojson
```


```bash
kubectl --kubeconfig ~/configs/br-mosk001/kubeconfig-br-mosk-01_operator get persistentvolumes -o=json | jq '.items[]|select(.spec.claimRef.namespace=="stacklight")|.spec.nodeAffinity.required.nodeSelectorTerms[].matchExpressions[].values[]| sub("^kaas-node-"; "")' | sort -u | xargs -I {}  kubectl --kubeconfig ~/configs/MCC/kubeconfig get machines --all-namespaces -o=jsonpath='{.items[?(@.metadata.annotations.kaas\.mirantis\.com/uid=="{}")].metadata.name}{"\n"}'
br-mosk001-master-1
br-mosk001-ceph-0
br-mosk001-master-2
br-mosk001-ceph-2
br-mosk001-ceph-1
br-mosk001-master-0
```




```bash
kubectl --kubeconfig ~/configs/br-mosk001/kubeconfig-br-mosk-01_operator get persistentvolumes -o=json | \
> jq '.items[]|select(.spec.claimRef.namespace=="stacklight")|.spec.nodeAffinity.required.nodeSelectorTerms[].matchExpressions[].values[]| sub("^kaas-node-"; "")' | \
> sort -u | xargs -I {} kubectl --kubeconfig ~/configs/MCC/kubeconfig -n br-mosk001 get machines -o=jsonpath='{.items[?(@.metadata.annotations.kaas\.mirantis\.com/uid=="{}")].metadata.name}{"\n"}'
br-mosk001-master-1
br-mosk001-master-2
br-mosk001-ceph-2
br-mosk001-ceph-1
br-mosk001-master-0
```
