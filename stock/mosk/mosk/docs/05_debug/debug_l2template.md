# getsion des L2Template

```bash
kubectl get ipamhosts -A
NAMESPACE    NAME                  STATUS                                          AGE   REGION
br-mosk001   br-mosk001-ceph-0     OK                                             12d   region-one
br-mosk001   br-mosk001-ceph-1     OK                                             12d   region-one
br-mosk001   br-mosk001-ceph-2     OK                                             12d   region-one
br-mosk001   br-mosk001-cmp-0      OK                                             12d   region-one
br-mosk001   br-mosk001-cmp-1                                                     12d   region-one
br-mosk001   br-mosk001-cmp-2      OK                                             12d   region-one
br-mosk001   br-mosk001-cmp-3                                                     12d   region-one
br-mosk001   br-mosk001-cmp-5     ERR: template: L2Template:37:4: 
                                    executing "L2Template" at <nic 4>:
                                    error calling nic: runtime error: 
                                     index out of range [4] with length 4        27m   region-one
br-mosk001   br-mosk001-master-0                                                 12d   region-one
br-mosk001   br-mosk001-master-1                                                 12d   region-one
br-mosk001   br-mosk001-master-2                                                 12d   region-one
default      master-0                                                            96d   region-one
default      master-1                                                            96d   region-one
default      master-2                                                            96d   region-one

```
