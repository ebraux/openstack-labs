
---
## Utilisation de la ligne de commande

## Gérer le déploiement de CEPH : Utiliser kubctl, et le namespace 'ceph-lcm-mirantis'

Ouvrir une session K8s

``` bash
source mosk-env.sh 
```

## Gérer CEP : ouvrir une session "ceph-tools"

Initialiser l'environnement pour le cluster CEP utilisé par Openstack :

``` bash
source mosk-env.sh
```

Puis, se connecter au pod "ceph-tools", soit en 2 étapes, soit avec la commande magique :

En 2 étapes : 
- Rechercher le nom du pod "ceph-tools"
```bash
kubectl get po  -n rook-ceph | grep tools
rook-ceph-tools-7958b4c6fd-4t59k                                  1/1     Running     0          12d
```
- puis s'y connecter
``` bash
kubectl exec -i -t -n rook-ceph rook-ceph-tools-7958b4c6fd-4t59k  -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
```

Avec la commande magique
``` bash
kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
```


---
## Diag du déploiement

Voir l'état des pod CEPH
```bash
kubectl get po  -n rook-ceph 
```

---

## Configuration de déploiement


kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml 
kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml | less

---


## Diagnostique global de CEPH

``` bash
ceph -s

ceph health

ceph health detail

```


---
## Gestion des Configuration des OSD

```bash
ceph osd tree

```
---
## Gestion des Configuration des Pools

```bash
# ceph osd lspools
1 other-hdd
2 object-store.rgw.control
3 kubernetes-hdd
4 object-store.rgw.meta
5 object-store.rgw.log
6 volumes-hdd
7 object-store.rgw.buckets.index
8 vms-hdd
9 object-store.rgw.buckets.non-ec
10 backup-hdd
11 .rgw.root
12 object-store.rgw.buckets.data
13 images-hdd
14 device_health_metrics


```



## Commande de diag standard



---
## details de la config 




---
## manipulation de données



---
# voir les données

# rbd ls vms-hdd
0e64ad88-62f4-42eb-ae3b-0929e115beaf_disk
1c9ffc70-b16b-4f73-997f-7a1e201f0432_disk
4db422db-637d-40ce-824d-2d0cbaaacbb8_disk
d19751c8-fb53-4689-a11b-ec4c433cd597_disk
d45a0b58-dcad-45b7-b262-201d0c37e50f_disk


# rbd info vms-hdd/0e64ad88-62f4-42eb-ae3b-0929e115beaf_disk

# rbd rm vms-hdd/0e64ad88-62f4-42eb-ae3b-0929e115beaf_disk


``` bash
# sudo ceph-conf -D | grep mon_pg_warn_max_object_skew mon_pg_warn_max_object_skew = 10.000000

# ceph daemon mon.dao-wkr-04 config show | grep mon_pg_warn_max_object_skew
```

``` bash
# ceph -s
  cluster:
    id:     28f391e0-0c00-44d4-9691-ecfce0daaa4c
    health: HEALTH_WARN
            1 pools have many more objects per pg than average
 
  services:
    mon: 3 daemons, quorum d,e,f (age 11d)
    mgr: a(active, since 12d)
    osd: 18 osds: 18 up (since 12d), 18 in (since 4w)
    rgw: 3 daemons active (object.store.a, object.store.b, object.store.c)
 
  task status:
 
  data:
    pools:   14 pools, 1265 pgs
    objects: 38.20k objects, 150 GiB
    usage:   494 GiB used, 131 TiB / 131 TiB avail
    pgs:     1265 active+clean
 
  io:
    client:   880 KiB/s rd, 880 op/s rd, 0 op/s wr
 


# ceph health detail
HEALTH_WARN 1 pools have many more objects per pg than average
[WRN] MANY_OBJECTS_PER_PG: 1 pools have many more objects per pg than average
    pool images-hdd objects per pg (868) is more than 28.9333 times cluster average (30)

---

#ceph osd pool get  images-hdd  pg_num
pg_num: 32
# ceph osd pool get  images-hdd  pgp_num
pgp_num: 32


---
# ceph df
--- RAW STORAGE ---
CLASS  SIZE     AVAIL    USED     RAW USED  %RAW USED
hdd    131 TiB  131 TiB  472 GiB   494 GiB       0.37
TOTAL  131 TiB  131 TiB  472 GiB   494 GiB       0.37
 
--- POOLS ---
POOL                             ID  PGS   STORED   OBJECTS  USED     %USED  MAX AVAIL
other-hdd                         1    32      0 B        0      0 B      0     41 TiB
object-store.rgw.control          2     8      0 B        8      0 B      0     41 TiB
kubernetes-hdd                    3    32     35 B        6  384 KiB      0     41 TiB
object-store.rgw.meta             4     8  4.6 KiB       17  2.8 MiB      0     41 TiB
object-store.rgw.log              5     8  1.2 MiB      217   10 MiB      0     41 TiB
volumes-hdd                       6    32   15 GiB    4.29k   46 GiB   0.04     41 TiB
object-store.rgw.buckets.index    7     8  5.3 KiB       33   16 KiB      0     41 TiB
vms-hdd                           8    32   21 GiB    5.82k   63 GiB   0.05     41 TiB
object-store.rgw.buckets.non-ec   9     8  2.7 KiB        0  8.0 KiB      0     41 TiB
backup-hdd                       10    32      0 B        0      0 B      0     41 TiB
.rgw.root                        11     8  4.0 KiB       16  2.8 MiB      0     41 TiB
object-store.rgw.buckets.data    12  1024   11 KiB        3  384 KiB      0     41 TiB
images-hdd                       13    32  112 GiB   27.78k  336 GiB   0.26     41 TiB
device_health_metrics            14     1  2.6 MiB       18  7.8 MiB      0     41 TiB

``` 


---
## commande debug Mirantis


``` bash
# ceph osd pool autoscale-status
POOL                               SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO  BIAS  PG_NUM  NEW PG_NUM  AUTOSCALE  
other-hdd                            0                 3.0        130.9T  0.0000                                  1.0      32              on         
object-store.rgw.control             0                 3.0        130.9T  0.0000                                  1.0       8              on         
kubernetes-hdd                   15564k                3.0        130.9T  0.0000                                  1.0      32              on         
object-store.rgw.meta             4720                 3.0        130.9T  0.0000                                  1.0       8              on         
object-store.rgw.log              1213k                3.0        130.9T  0.0000                                  1.0       8              on         
volumes-hdd                      15655M                3.0        130.9T  0.0003        0.2000           0.0185   1.0      32              on         
object-store.rgw.buckets.index    5388                 3.0        130.9T  0.0000                                  1.0       8              on         
vms-hdd                           1028M                3.0        130.9T  0.0000        0.4000           0.0370   1.0      32              on         
object-store.rgw.buckets.non-ec   2715                 3.0        130.9T  0.0000                                  1.0       8              on         
backup-hdd                           0                 3.0        130.9T  0.0000        0.1000           0.0093   1.0      32              on         
.rgw.root                         4107                 3.0        130.9T  0.0000                                  1.0       8              on         
object-store.rgw.buckets.data    11445                 3.0        130.9T  0.0000       10.0000           0.9259   1.0    1024              on         
images-hdd                       67046M                3.0        130.9T  0.0015        0.1000           0.0093   1.0      32              on         
device_health_metrics             2949k                3.0        130.9T  0.0000                                  1.0       1              on        
```

Copier le fichier
```bash
kubectl cp  ceph_collect.sh rook-ceph-tools-5896485948-m22k7:tmp/ceph_collect.sh  -n rook-ceph 
```

Récupérer le fichier :
```bash
kubectl cp  rook-ceph-tools-5896485948-m22k7:tmp/CephCollectData.IMT-atlantique.br-mosk001.2021-10-26.tgz CephCollectData.IMT-atlantique.br-mosk001.2021-10-26.tgz  -n rook-ceph
```
kubectl -n rook-ceph  cp $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';):

Script de collecte :
```bash
#!/bin/sh
echo "Collecting Ceph cluster data."
echo "This script must be run on an admin node.\n"

if [ $# -lt 2 ]; then echo "Usage: $0 <CUSTOMER> <CLUSTERNAME>"; exit 2; fi
DATE=`date "+%Y-%m-%d"`
DIRNAME="CephCollectData.$1.$2.$DATE"
ARCHNAME=$DIRNAME".tgz"
mkdir $DIRNAME
cd $DIRNAME

echo "Collecting CRUSH map"
ceph osd getcrushmap -o crush.bin
crushtool -d crush.bin -o crushmap.txt
rm crush.bin

echo "Collecting cluster status"
ceph -s -f json -o ceph_s.json
echo "Collecting monmap"
ceph mon dump -f json -o monmap.json
echo "Collecting ceph df"
ceph df -f json -o ceph_df.json
echo "Collecting ceph osd df"
ceph osd df -f json -o ceph_osd_df.json
echo "Collecting ceph osd dump"
ceph osd dump -f json -o ceph_osd_dump.json
echo "Collecting rados df"
rados df -f json >rados_df.json
echo "Collecting ceph report"
ceph report -o ceph_report.json
echo "Collecting auth data anonymized"
ceph auth list -f json |sed 's/AQ[^=]*==/KEY/g' > ceph_auth_ls.json

cd ..
tar czvf $ARCHNAME $DIRNAME
```

---
## OSD
ceph osd pool autoscale-status


---
## PG


ceph -s

ceph pg dump_stuck


---
## configuration

 to fix the Ceph warning, please can you run the following within the ceph tools pod
```bash
# ceph osd pool get  object-store.rgw.buckets.data target_size_ratio    
target_size_ratio: 10

# ceph osd pool set object-store.rgw.buckets.data target_size_ratio 0.1
set pool 12 target_size_ratio to 0.1

# ceph osd pool get  object-store.rgw.buckets.data target_size_ratio 
target_size_ratio: 0.1

```
- [https://avengermojo.medium.com/ceph-placement-groups-autoscale-9981aeccbc21](https://avengermojo.medium.com/ceph-placement-groups-autoscale-9981aeccbc21)


- [https://tracker.ceph.com/projects/ceph/wiki/10_Commands_Every_Ceph_Administrator_Should_Know](https://tracker.ceph.com/projects/ceph/wiki/10_Commands_Every_Ceph_Administrator_Should_Know)



---

# could you also please show objectStore.Rgw section from your miraCeph spec? or kaascephcluster
``` bash
source mcc-env.sh 
$kub  get KaaSCephCluster
NAME              AGE
br-mosk001-ceph   64d

$kub -n br-mosk001  get KaaSCephCluster
NAME              AGE
br-mosk001-ceph   64d

$kub -n br-mosk001  get KaaSCephCluster br-mosk001-ceph
NAME              AGE
br-mosk001-ceph   64d

$ kub -n br-mosk001  get KaaSCephCluster br-mosk001-ceph -o yaml | less
```



```
source mosk-env.sh 
kubectl -n ceph-lcm-mirantis get miraceph
kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml 
kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml | less

```

kubectl get po  -n rook-ceph | grep osd


kubectl -n rook-ceph logs rook-ceph-osd-1-84ff49bbc5-mjx2p > /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p_2.logs

kubectl -n rook-ceph logs rook-ceph-osd-10-58f54b554d-qqn6w > /tmp/rook-ceph-osd-10-58f54b554d-qqn6w

kubectl -n rook-ceph logs rook-ceph-osd-11-65c69bd48d-hv2fj > /tmp/rook-ceph-osd-11-65c69bd48d-hv2fj


kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph -s"


kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg dump_stuck"


kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 10"


  cluster:
    id:     28f391e0-0c00-44d4-9691-ecfce0daaa4c
    health: HEALTH_WARN
            2 pools have many more objects per pg than average
            Reduced data availability: 15 pgs inactive, 7 pgs stale
            Degraded data redundancy: 38/63288 objects degraded (0.060%), 1 pg degraded, 7 pgs undersized
            2 slow ops, oldest one blocked for 62268 sec, daemons [osd.14,osd.16] have slow ops.
 
  services:
    mon: 3 daemons, quorum d,e,f (age 18h)
    mgr: a(active, since 18h)
    osd: 18 osds: 15 up (since 14m), 15 in (since 17h)
    rgw: 3 daemons active (object.store.a, object.store.b, object.store.c)
 
  task status:
 
  data:
    pools:   14 pools, 1731 pgs
    objects: 21.10k objects, 83 GiB
    usage:   267 GiB used, 109 TiB / 109 TiB avail
    pgs:     0.867% pgs unknown
             38/63288 objects degraded (0.060%)
             1709 active+clean
             15   unknown
             6    stale+active+undersized
             1    stale+active+undersized+degraded
 
  io:
    client:   951 KiB/s rd, 951 op/s rd, 0 op/s wr
 
  progress:
    PG autoscaler decreasing pool 8 PGs from 512 to 32 (81m)
      [............................] (remaining: 20h)
 



PG_STAT  STATE                             UP      UP_PRIMARY  ACTING  ACTING_PRIMARY
12.372            stale+active+undersized  [1,10]           1  [1,10]               1
12.2eb            stale+active+undersized  [10,1]          10  [10,1]              10
12.28c            stale+active+undersized  [10,1]          10  [10,1]              10
8.1f1                             unknown      []          -1      []              -1
8.71                              unknown      []          -1      []              -1
8.51                              unknown      []          -1      []              -1
8.151                             unknown      []          -1      []              -1
12.168            stale+active+undersized  [10,1]          10  [10,1]              10
8.1b1                             unknown      []          -1      []              -1
8.171                             unknown      []          -1      []              -1
8.1d1                             unknown      []          -1      []              -1
8.191                             unknown      []          -1      []              -1
12.143            stale+active+undersized  [10,1]          10  [10,1]              10
12.107            stale+active+undersized  [1,10]           1  [1,10]               1
8.131                             unknown      []          -1      []              -1
8.111                             unknown      []          -1      []              -1
8.f1                              unknown      []          -1      []              -1
8.d1                              unknown      []          -1      []              -1
8.b1                              unknown      []          -1      []              -1
8.91                              unknown      []          -1      []              -1
8.11     stale+active+undersized+degraded  [10,1]          10  [10,1]              10
8.31                              unknown      []          -1      []              -1



```
mcc-user@kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c:~$ lsblk
NAME                                                                                                  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                                                                                                     8:0    0   7.3T  0 disk 
└─ceph--c8b18951--3779--4e9e--9c0a--6feca1bd92c4-osd--block--0bd5c4cf--c786--4254--9c0a--8e7967b35396 253:2    0   7.3T  0 lvm  
sdb                                                                                                     8:16   0   7.3T  0 disk 
└─ceph--33cc7da5--3ee9--48b9--b971--6159e1378f45-osd--block--7dcccc1a--c109--481f--9447--c9596a09fe06 253:5    0   7.3T  0 lvm  
sdc                                                                                                     8:32   0   7.3T  0 disk 
└─ceph--37eb0e66--24c3--4441--b61f--d8b8b0b20a19-osd--block--a66ff108--6585--415e--8727--199925c996fe 253:7    0   7.3T  0 lvm  
sdd                                                                                                     8:48   0   7.3T  0 disk 
└─ceph--de6de31f--54b9--4445--b2c6--bd177b7cd169-osd--block--80e5da8c--b28e--4e8c--8e40--2e645a69ccc7 253:4    0   7.3T  0 lvm  
sde                                                                                                     8:64   0   7.3T  0 disk 
└─ceph--a33ad295--74b9--4265--b890--773961cf4588-osd--block--47dd05e7--1d44--41d2--8815--b7e1b64c044f 253:6    0   7.3T  0 lvm  
sdf                                                                                                     8:80   0   7.3T  0 disk 
└─ceph--08609658--eff2--4503--ab50--ecd802a1a0bb-osd--block--758ff52b--0e82--4a13--b75b--deb7a139ba6a 253:3    0   7.3T  0 lvm  
sdg                                                                                                     8:96   0 447.1G  0 disk 
├─sdg1                                                                                                  8:97   0     4M  0 part 
├─sdg2                                                                                                  8:98   0   204M  0 part /boot/efi
├─sdg3                                                                                                  8:99   0    64M  0 part 
└─sdg4                                                                                                  8:100  0 446.9G  0 part 
  └─lvm_root-root                                                                                     253:1    0 446.9G  0 lvm  /
sdh                                                                                                     8:112  0 558.9G  0 disk 
sdi                                                                                                     8:128  0 447.1G  0 disk 
└─sdi1                                                                                                  8:129  0 447.1G  0 part 
  └─lvm_lvp-lvp                                                                                       253:0    0 447.1G  0 lvm  /mnt/local-volumes
sdj                                                                                                     8:144  0   1.8T  0 disk 
sdk                                                                                                     8:160  0   1.8T  0 disk 
sdl                                                                                                     8:176  0 278.9G  0 disk 
mcc-user@kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c:~$ blkid
/dev/sdg2: SEC_TYPE="msdos" UUID="7902-A99F" TYPE="vfat" PARTLABEL="uefi" PARTUUID="58b0c45b-9a45-4f9c-9845-719157f7cf5a"
/dev/sdg3: UUID="2021-09-15-13-34-59-00" LABEL="config-2" TYPE="iso9660" PARTLABEL="config-2" PARTUUID="3663fe32-d775-40f4-b564-50074f3da15f"
/dev/sdg4: UUID="OHfdyi-cFyT-W10i-GkYb-UVT4-lJlc-YoVP4m" TYPE="LVM2_member" PARTLABEL="lvm_root_part" PARTUUID="58285710-d1aa-4e5c-ac89-17c498e49c92"
/dev/sdi1: UUID="kaftlc-sEHH-g6Gb-d5k1-69Lz-Li3k-HtxZ25" TYPE="LVM2_member" PARTLABEL="lvm_part" PARTUUID="285d2639-e5bd-46cd-8328-8a0620e5dcb0"
/dev/mapper/lvm_lvp-lvp: UUID="13b44ded-4680-4fb4-b563-0bb7feb0deec" TYPE="ext4"
/dev/mapper/lvm_root-root: UUID="d7f87dbc-10c7-48ad-bc53-3111140779f4" TYPE="ext4"

```
```
mcc-user@kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f:~$ blkid
/dev/sdg2: SEC_TYPE="msdos" UUID="779C-D975" TYPE="vfat" PARTLABEL="uefi" PARTUUID="8311e9f9-49c1-4133-b7d8-f1796d4b184c"
/dev/sdg3: UUID="2021-09-15-13-34-48-00" LABEL="config-2" TYPE="iso9660" PARTLABEL="config-2" PARTUUID="96a54684-bcf9-4cc1-889b-0a3055381b20"
/dev/sdg4: UUID="dgaJYV-q0L8-ViF3-hrcc-OcxC-Y4KF-661bgs" TYPE="LVM2_member" PARTLABEL="lvm_root_part" PARTUUID="8b8ed61b-d6ec-4246-a3d6-0cfdded0973e"
/dev/sdi1: UUID="KdUl3r-2sHY-R3hv-WMEV-eRyj-BcBf-BqBBhd" TYPE="LVM2_member" PARTLABEL="lvm_part" PARTUUID="9dbe7b47-aca6-43b9-b056-744e5f964474"
/dev/sdb: UUID="cgGc3C-2LyY-uT1M-0xqK-BQtP-UdU3-YOyHip" TYPE="LVM2_member"
/dev/sda: UUID="ZTHavR-SJYu-B4uA-opbF-0QVP-3E1q-GhfHAr" TYPE="LVM2_member"
/dev/sdd: UUID="7lX7wY-NZ0v-avUd-BoBw-HWXE-CyJx-28wr3v" TYPE="LVM2_member"
/dev/sdf: UUID="Qcb3mT-7Y8H-DeWN-ix4W-8mMS-yp4X-h3vzZp" TYPE="LVM2_member"
/dev/sde: UUID="VGr0k0-S0ck-GCk2-KhnA-WGtX-YZ8w-ZzEo0o" TYPE="LVM2_member"
/dev/mapper/lvm_lvp-lvp: UUID="234a5144-33be-4b81-981a-e148809fd391" TYPE="ext4"
/dev/sdc: UUID="qGEeqd-AuNb-rg5t-aUlj-TWno-rK9q-r4De9X" TYPE="LVM2_member"
/dev/mapper/lvm_root-root: UUID="884eedda-e8ad-4e0b-9816-c24e83a5b606" TYPE="ext4"
```
---
## Ceph config

1 volume RAID pour le système :
	sys	En ligne	RAID-1	278.88 Go	HDD	Lecture anticipée	Écriture différée	64K	Non	1

13 volumes disque pour CEPH  
		Physical Disk 0:1:0	En ligne	0	278.88 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:1	En ligne	1	278.88 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:2	Non RAID	2	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:3	Non RAID	3	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:4	Non RAID	4	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:5	Non RAID	5	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:6	Non RAID	6	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Physical Disk 0:1:7	Non RAID	7	7452.04 Go	Non pris en charge	SAS	HDD	Non	Non applicable
		Solid State Disk 0:1:12	Non RAID	12	447.13 Go	Non pris en charge	SATA	SSD	Non	99%
		Physical Disk 0:1:13	Non RAID	13	558.91 Go	Non pris en charge	SAS	HDD	Non	Non applicable


---
# Modifier le déploiement de CEPH

---


---
## Infos sur les OSD

https://documentation.suse.com/ses/6/html/ses-all/ceph-pools.html


Positionnements des OSD sur les nodes
```
# ceph osd tree
ID  CLASS  WEIGHT     TYPE NAME                                                STATUS  REWEIGHT  PRI-AFF
-1         130.99301  root default                                                                      
-5          43.66434      host kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f                           
 3    hdd    7.27739          osd.3                                                up   1.00000  1.00000
 5    hdd    7.27739          osd.5                                                up   1.00000  1.00000
 7    hdd    7.27739          osd.7                                                up   1.00000  1.00000
10    hdd    7.27739          osd.10                                             down         0  1.00000
12    hdd    7.27739          osd.12                                               up   1.00000  1.00000
14    hdd    7.27739          osd.14                                               up   1.00000  1.00000
-3          43.66434      host kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504                           
 0    hdd    7.27739          osd.0                                                up   1.00000  1.00000
 1    hdd    7.27739          osd.1                                              down         0  1.00000
 2    hdd    7.27739          osd.2                                                up   1.00000  1.00000
 4    hdd    7.27739          osd.4                                                up   1.00000  1.00000
 6    hdd    7.27739          osd.6                                                up   1.00000  1.00000
 9    hdd    7.27739          osd.9                                                up   1.00000  1.00000
-7          43.66434      host kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c                           
 8    hdd    7.27739          osd.8                                                up   1.00000  1.00000
11    hdd    7.27739          osd.11                                             down         0  1.00000
13    hdd    7.27739          osd.13                                               up   1.00000  1.00000
15    hdd    7.27739          osd.15                                               up   1.00000  1.00000
16    hdd    7.27739          osd.16                                               up   1.00000  1.00000
17    hdd    7.27739          osd.17                                               up   1.00000  1.00000
```

Utilisation des OSD par les pool

```
# ceph osd dump

```

```
# ceph osd dump
```

---
## Gestion des pools

```bash
# ceph osd lspools 
1 other-hdd
2 object-store.rgw.control
3 kubernetes-hdd
4 object-store.rgw.meta
5 object-store.rgw.log
6 volumes-hdd
7 object-store.rgw.buckets.index
8 vms-hdd
9 object-store.rgw.buckets.non-ec
10 backup-hdd
11 .rgw.root
12 object-store.rgw.buckets.data
13 images-hdd
14 device_health_metrics

```

```
# ceph osd dump | grep pool
pool 1 'other-hdd' replicated size 3 min_size 2 crush_rule 1 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 57217 flags hashpspool stripe_width 0 target_size_ratio 0.1 application rbd
pool 2 'object-store.rgw.control' replicated size 3 min_size 2 crush_rule 2 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 49609 flags hashpspool stripe_width 0 pg_num_min 8 application rook-ceph-rgw
pool 3 'kubernetes-hdd' replicated size 3 min_size 2 crush_rule 3 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 49623 flags hashpspool,selfmanaged_snaps stripe_width 0 application rbd
pool 4 'object-store.rgw.meta' replicated size 3 min_size 2 crush_rule 4 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 49609 flags hashpspool stripe_width 0 pg_num_min 8 application rook-ceph-rgw
pool 5 'object-store.rgw.log' replicated size 3 min_size 2 crush_rule 6 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 49609 flags hashpspool stripe_width 0 pg_num_min 8 application rook-ceph-rgw
pool 6 'volumes-hdd' replicated size 3 min_size 2 crush_rule 5 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 52080 lfor 0/52080/52078 flags hashpspool,selfmanaged_snaps stripe_width 0 target_size_ratio 0.2 application rbd
pool 7 'object-store.rgw.buckets.index' replicated size 3 min_size 2 crush_rule 7 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 49609 flags hashpspool stripe_width 0 pg_num_min 8 application rook-ceph-rgw
pool 8 'vms-hdd' replicated size 3 min_size 2 crush_rule 8 object_hash rjenkins pg_num 498 pgp_num 32 pg_num_target 512 pgp_num_target 512 autoscale_mode off last_change 57840 lfor 0/51224/51222 flags hashpspool,selfmanaged_snaps stripe_width 0 target_size_ratio 0.4 application rbd
pool 9 'object-store.rgw.buckets.non-ec' replicated size 3 min_size 2 crush_rule 9 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 49609 flags hashpspool stripe_width 0 pg_num_min 8 application rook-ceph-rgw
pool 10 'backup-hdd' replicated size 3 min_size 2 crush_rule 10 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 57232 lfor 0/51539/51537 flags hashpspool stripe_width 0 target_size_ratio 0.1 application rbd
pool 11 '.rgw.root' replicated size 3 min_size 2 crush_rule 11 object_hash rjenkins pg_num 8 pgp_num 8 autoscale_mode on last_change 57234 flags hashpspool stripe_width 0 pg_num_min 8 target_size_ratio 0.1 application rook-ceph-rgw
pool 12 'object-store.rgw.buckets.data' replicated size 3 min_size 2 crush_rule 12 object_hash rjenkins pg_num 1024 pgp_num 1024 pg_num_target 512 pgp_num_target 512 autoscale_mode off last_change 57843 lfor 0/0/15149 flags hashpspool stripe_width 0 target_size_ratio 10 application rook-ceph-rgw
pool 13 'images-hdd' replicated size 3 min_size 2 crush_rule 13 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 53605 lfor 0/51653/51651 flags hashpspool,selfmanaged_snaps stripe_width 0 target_size_ratio 0.1 application rbd
pool 14 'device_health_metrics' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 1 pgp_num 1 autoscale_mode on last_change 49570 flags hashpspool stripe_width 0 pg_num_min 1 application mgr_devicehealth

```

 # Maintenance sur les OSD   

Infos :
- [https://avengermojo.medium.com/ceph-placement-groups-autoscale-9981aeccbc21](https://avengermojo.medium.com/ceph-placement-groups-autoscale-9981aeccbc21)

Ouvrir une session "ceph-tools"
``` bash
source mosk-env.sh
kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
```

Ouvrir une session K8s
``` bash
source mosk-env.sh 
kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml 
kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml | less
```

Diag CEPH before actions

```
$ kubectl get po  -n rook-ceph | grep osd
NAME                                                              READY   STATUS      RESTARTS   AGE
rook-ceph-osd-0-7857d658ff-5nqrh                                  1/1     Running     0          117m
rook-ceph-osd-1-5f8569d467-7jmjm                                  1/1     Running     30         116m
rook-ceph-osd-10-6b65d6fc84-nfg58                                 1/1     Running     30         115m
rook-ceph-osd-11-6b6b48fc48-l5z5t                                 1/1     Running     30         114m
rook-ceph-osd-12-686f986849-d648t                                 1/1     Running     0          5d
rook-ceph-osd-13-86949b57b9-7jkkv                                 1/1     Running     0          5d
rook-ceph-osd-14-5f8f8b68f7-jzxk2                                 1/1     Running     0          5d
rook-ceph-osd-15-7f898f8d79-q2zps                                 1/1     Running     0          5d
rook-ceph-osd-16-9b4f9f967-rzcnv                                  1/1     Running     0          5d
rook-ceph-osd-17-7dc6bd766c-kvtlj                                 1/1     Running     0          5d
rook-ceph-osd-2-6cc79fb5c5-gcrbg                                  1/1     Running     0          117m
rook-ceph-osd-3-77c4847b4b-dr8gx                                  1/1     Running     0          5d
rook-ceph-osd-4-b5f6d7bb4-wqdqz                                   1/1     Running     0          117m
rook-ceph-osd-5-6974d8dc6c-4pwb7                                  1/1     Running     0          5d
rook-ceph-osd-6-79c58969-5nkzd                                    1/1     Running     0          117m
rook-ceph-osd-7-6f9ff7c78f-jbrtp                                  1/1     Running     0          5d
rook-ceph-osd-8-854f59584d-szz28                                  1/1     Running     0          5d
rook-ceph-osd-9-544fbdc878-mh7gn                                  1/1     Running     0          117m
```

```
 ceph -s                                
  cluster:
    id:     28f391e0-0c00-44d4-9691-ecfce0daaa4c
    health: HEALTH_WARN
            Reduced data availability: 15 pgs inactive, 7 pgs stale
            Degraded data redundancy: 38/9792 objects degraded (0.388%), 1 pg degraded, 7 pgs undersized
            26 daemons have recently crashed
            4 slow ops, oldest one blocked for 358890 sec, daemons [osd.14,osd.16] have slow ops.
 
  services:
    mon: 3 daemons, quorum d,e,f (age 5d)
    mgr: a(active, since 5d)
    osd: 18 osds: 15 up (since 33m), 15 in (since 4d)
    rgw: 3 daemons active (object.store.a, object.store.b, object.store.c)
 
  task status:
 
  data:
    pools:   14 pools, 1731 pgs
    objects: 3.26k objects, 12 GiB
    usage:   65 GiB used, 109 TiB / 109 TiB avail
    pgs:     0.867% pgs unknown
             38/9792 objects degraded (0.388%)
             1709 active+clean
             15   unknown
             6    stale+active+undersized
             1    stale+active+undersized+degraded
 
  io:
    client:   1.0 MiB/s rd, 1.06k op/s rd, 0 op/s wr
 
  progress:
    PG autoscaler decreasing pool 8 PGs from 512 to 32 (4d)
      [............................] (remaining: 4M)
```

```
# ceph df
--- RAW STORAGE ---
CLASS  SIZE     AVAIL    USED    RAW USED  %RAW USED
hdd    109 TiB  109 TiB  50 GiB    65 GiB       0.06
TOTAL  109 TiB  109 TiB  50 GiB    65 GiB       0.06
 
--- POOLS ---
POOL                             ID  PGS   STORED   OBJECTS  USED     %USED  MAX AVAIL
other-hdd                         1    32  1.3 KiB        0  3.9 KiB      0     41 TiB
object-store.rgw.control          2     8      0 B        8      0 B      0     41 TiB
kubernetes-hdd                    3    32   44 MiB       43  139 MiB      0     41 TiB
object-store.rgw.meta             4     8  5.5 KiB       17  3.4 MiB      0     41 TiB
object-store.rgw.log              5     8  1.2 MiB      217   11 MiB      0     41 TiB
volumes-hdd                       6    32  6.3 GiB    1.02k   19 GiB   0.01     41 TiB
object-store.rgw.buckets.index    7     8  6.6 KiB       33   20 KiB      0     41 TiB
vms-hdd                           8   498  1.2 GiB      578  3.6 GiB      0     42 TiB
object-store.rgw.buckets.non-ec   9     8      0 B        0      0 B      0     41 TiB
backup-hdd                       10    32      0 B        0      0 B      0     41 TiB
.rgw.root                        11     8  4.6 KiB       16  3.4 MiB      0     41 TiB
object-store.rgw.buckets.data    12  1024   11 KiB        3  512 KiB      0     41 TiB
images-hdd                       13    32   19 GiB    1.31k   56 GiB   0.04     41 TiB
device_health_metrics            14     1  5.9 MiB       18   18 MiB      0     41 TiB
```




```
# ceph osd pool stats
pool other-hdd id 1
  nothing is going on

pool object-store.rgw.control id 2
  nothing is going on

pool kubernetes-hdd id 3
  nothing is going on

pool object-store.rgw.meta id 4
  client io 1.0 MiB/s rd, 1.06k op/s rd, 0 op/s wr

pool object-store.rgw.log id 5
  nothing is going on

pool volumes-hdd id 6
  nothing is going on

pool object-store.rgw.buckets.index id 7
  nothing is going on

pool vms-hdd id 8
  38/1734 objects degraded (2.191%)

pool object-store.rgw.buckets.non-ec id 9
  nothing is going on

pool backup-hdd id 10
  nothing is going on

pool .rgw.root id 11
  nothing is going on

pool object-store.rgw.buckets.data id 12
  nothing is going on

pool images-hdd id 13
  nothing is going on

pool device_health_metrics id 14
  nothing is going on
```

OSD List: 
- 1 : other-hdd
- 8 : vms-hdd
- 10 : backup-hdd
- 11 : .rgw.root

OSD-1, 10 and 11 are crashing
```
    PG autoscaler decreasing pool 8 PGs from 512 to 32 (4d)
      [............................] (remaining: 4M)
```

  

1. Disable autoscaler for affected pools
``` 
# ceph osd pool autoscale-status
POOL                               SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO  BIAS  PG_NUM  NEW PG_NUM  AUTOSCALE  
other-hdd                         1316                 3.0        109.1T  0.0000                                  1.0      32              on         
object-store.rgw.control             0                 3.0        109.1T  0.0000                                  1.0       8              on         
kubernetes-hdd                   44744k                3.0        109.1T  0.0000                                  1.0      32              on         
object-store.rgw.meta             5624                 3.0        109.1T  0.0000                                  1.0       8              on         
object-store.rgw.log              1213k                3.0        109.1T  0.0000                                  1.0       8              on         
volumes-hdd                       6453M                3.0        109.1T  0.0002        0.2000           0.0185   1.0      32              on         
object-store.rgw.buckets.index    6749                 3.0        109.1T  0.0000                                  1.0       8              on         
vms-hdd                           1244M                3.0        109.1T  0.0000        0.4000           0.0370   1.0      32              on         
object-store.rgw.buckets.non-ec      0                 3.0        109.1T  0.0000                                  1.0       8              on         
backup-hdd                           0                 3.0        109.1T  0.0000        0.1000           0.0093   1.0      32              on         
.rgw.root                         4693                 3.0        109.1T  0.0000                                  1.0       8              on         
object-store.rgw.buckets.data    11459                 3.0        109.1T  0.0000       10.0000           0.9259   1.0    1024              on         
images-hdd                       19081M                3.0        109.1T  0.0005        0.1000           0.0093   1.0      32              on         
device_health_metrics             6042k                3.0        109.1T  0.0000                                  1.0       1              on       
```

```
ceph osd pool set other-hdd pg_autoscale_mode off
ceph osd pool set vms-hdd pg_autoscale_mode off
ceph osd pool set backup-hdd pg_autoscale_mode off
ceph osd pool set .rgw.root pg_autoscale_mode off
```

```
# ceph osd pool autoscale-status          
POOL                               SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO  BIAS  PG_NUM  NEW PG_NUM  AUTOSCALE  
other-hdd                         1316                 3.0        109.1T  0.0000                                  1.0      32              off        
object-store.rgw.control             0                 3.0        109.1T  0.0000                                  1.0       8              on         
kubernetes-hdd                   44744k                3.0        109.1T  0.0000                                  1.0      32              on         
object-store.rgw.meta             5624                 3.0        109.1T  0.0000                                  1.0       8              on         
object-store.rgw.log              1213k                3.0        109.1T  0.0000                                  1.0       8              on         
volumes-hdd                       6453M                3.0        109.1T  0.0002        0.2000           0.0185   1.0      32              on         
object-store.rgw.buckets.index    6749                 3.0        109.1T  0.0000                                  1.0       8              on         
vms-hdd                           1244M                3.0        109.1T  0.0000        0.4000           0.0370   1.0      32              off        
object-store.rgw.buckets.non-ec      0                 3.0        109.1T  0.0000                                  1.0       8              on         
backup-hdd                           0                 3.0        109.1T  0.0000        0.1000           0.0093   1.0      32              off        
.rgw.root                         4693                 3.0        109.1T  0.0000                                  1.0       8              off        
object-store.rgw.buckets.data    11459                 3.0        109.1T  0.0000       10.0000           0.9259   1.0    1024              on         
images-hdd                       19081M                3.0        109.1T  0.0005        0.1000           0.0093   1.0      32              on         
device_health_metrics             6042k                3.0        109.1T  0.0000                                  1.0       1              on         
```



2. Restart affected osds (simply kill pods and wait till it respawned). Since autoscaler disabled now it should start new operation for autoscaling and should prevent osd kill.
``` 
$ kubectl get po  -n rook-ceph | grep osd
rook-ceph-osd-0-7857d658ff-5nqrh                                  1/1     Running     0          126m
rook-ceph-osd-1-5f8569d467-7jmjm                                  1/1     Running     32         125m
rook-ceph-osd-10-6b65d6fc84-nfg58                                 1/1     Running     33         124m
rook-ceph-osd-11-6b6b48fc48-l5z5t                                 1/1     Running     32         124m
rook-ceph-osd-12-686f986849-d648t                                 1/1     Running     0          5d
rook-ceph-osd-13-86949b57b9-7jkkv                                 1/1     Running     0          5d
rook-ceph-osd-14-5f8f8b68f7-jzxk2                                 1/1     Running     0          5d
rook-ceph-osd-15-7f898f8d79-q2zps                                 1/1     Running     0          5d
rook-ceph-osd-16-9b4f9f967-rzcnv                                  1/1     Running     0          5d
rook-ceph-osd-17-7dc6bd766c-kvtlj                                 1/1     Running     0          5d
rook-ceph-osd-2-6cc79fb5c5-gcrbg                                  1/1     Running     0          126m
rook-ceph-osd-3-77c4847b4b-dr8gx                                  1/1     Running     0          5d
rook-ceph-osd-4-b5f6d7bb4-wqdqz                                   1/1     Running     0          126m
rook-ceph-osd-5-6974d8dc6c-4pwb7                                  1/1     Running     0          5d
rook-ceph-osd-6-79c58969-5nkzd                                    1/1     Running     0          126m
rook-ceph-osd-7-6f9ff7c78f-jbrtp                                  1/1     Running     0          5d
rook-ceph-osd-8-854f59584d-szz28                                  1/1     Running     0          5d
rook-ceph-osd-9-544fbdc878-mh7gn                                  1/1     Running     0          126m
```


``` 
$ kubectl delete po rook-ceph-osd-1-5f8569d467-7jmjm   -n rook-ceph 
pod "rook-ceph-osd-1-5f8569d467-7jmjm" deleted

$ kubectl delete po rook-ceph-osd-8-854f59584d-szz28  -n rook-ceph 
pod "rook-ceph-osd-8-854f59584d-szz28" deleted

$ kubectl delete po rook-ceph-osd-10-6b65d6fc84-nfg58   -n rook-ceph 
pod "rook-ceph-osd-10-6b65d6fc84-nfg58" deleted

$ kubectl delete po rook-ceph-osd-11-6b6b48fc48-l5z5t   -n rook-ceph 
pod "rook-ceph-osd-11-6b6b48fc48-l5z5t" deleted

```

``` 
$ kubectl get po  -n rook-ceph | grep osd
rook-ceph-osd-0-7857d658ff-5nqrh                                  1/1     Running     0          130m
rook-ceph-osd-1-5f8569d467-rlfk7                                  1/1     Running     3          3m10s
rook-ceph-osd-10-6b65d6fc84-nl5l5                                 1/1     Running     1          119s
rook-ceph-osd-11-6b6b48fc48-skqtg                                 1/1     Running     0          20s
rook-ceph-osd-12-686f986849-d648t                                 1/1     Running     0          5d
rook-ceph-osd-13-86949b57b9-7jkkv                                 1/1     Running     0          5d
rook-ceph-osd-14-5f8f8b68f7-jzxk2                                 1/1     Running     0          5d
rook-ceph-osd-15-7f898f8d79-q2zps                                 1/1     Running     0          5d
rook-ceph-osd-16-9b4f9f967-rzcnv                                  1/1     Running     0          5d
rook-ceph-osd-17-7dc6bd766c-kvtlj                                 1/1     Running     0          5d
rook-ceph-osd-2-6cc79fb5c5-gcrbg                                  1/1     Running     0          130m
rook-ceph-osd-3-77c4847b4b-dr8gx                                  1/1     Running     0          5d
rook-ceph-osd-4-b5f6d7bb4-wqdqz                                   1/1     Running     0          130m
rook-ceph-osd-5-6974d8dc6c-4pwb7                                  1/1     Running     0          5d
rook-ceph-osd-6-79c58969-5nkzd                                    1/1     Running     0          130m
rook-ceph-osd-7-6f9ff7c78f-jbrtp                                  1/1     Running     0          5d
rook-ceph-osd-8-854f59584d-smdrp                                  1/1     Running     0          2m12s
rook-ceph-osd-9-544fbdc878-mh7gn                                  1/1     Running     0          130m
```


3. Check ratio and pg nums on affected pools
``` 

$ ceph osd pool get other-hdd  pg_num
pg_num: 32

$ ceph osd pool get vms-hdd  pg_num
pg_num: 498

$ ceph osd pool get backup-hdd  pg_num
pg_num: 32

$ ceph osd pool get .rgw.root  pg_num
pg_num: 8
```

4. Step-by-step decreasing pg num till 32. On every step decreasing it by 2 times.


Only OSD 8 - `vms-hdd`  has pg_num > 32
And ceph -s returns that `the PG autoscaler decreasing pool 8 PGs from 512 to 32 (4d)` is still running `(remaining: 4M)`

I fhave tried to set pg_num = 512

``` 
ceph osd pool set vms-hdd pg_num 512
```

But pool 8 is still autoscalling. 

``` 
# ceph -s
  cluster:
    id:     28f391e0-0c00-44d4-9691-ecfce0daaa4c
    health: HEALTH_WARN
            Reduced data availability: 15 pgs inactive, 7 pgs stale
            Degraded data redundancy: 38/9792 objects degraded (0.388%), 1 pg degraded, 7 pgs undersized
            26 daemons have recently crashed
            4 slow ops, oldest one blocked for 359899 sec, daemons [osd.14,osd.16] have slow ops.
 
  services:
    mon: 3 daemons, quorum d,e,f (age 5d)
    mgr: a(active, since 5d)
    osd: 18 osds: 15 up (since 9m), 15 in (since 4d)
    rgw: 3 daemons active (object.store.a, object.store.b, object.store.c)
 
  task status:
 
  data:
    pools:   14 pools, 1731 pgs
    objects: 3.26k objects, 12 GiB
    usage:   65 GiB used, 109 TiB / 109 TiB avail
    pgs:     0.867% pgs unknown
             38/9792 objects degraded (0.388%)
             1709 active+clean
             15   unknown
             6    stale+active+undersized
             1    stale+active+undersized+degraded
 
  io:
    client:   767 KiB/s rd, 766 op/s rd, 0 op/s wr
 
  progress:
    PG autoscaler decreasing pool 8 PGs from 512 to 32 (4d)
      [............................] (remaining: 4M)
``` 


I have no more datas in vm-hdd pool, the nova storage is configured as 'local' on every computes.


5. Change ratio to 0.1 for affected pools.
6. Enable autoscaler for affected pools

Try on pool 1, 10 and 11
``` 
# ceph osd pool set other-hdd target_size_ratio 0.1
# ceph osd pool set other-hdd pg_autoscale_mode on

#  ceph osd pool autoscale-status
POOL                               SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO  BIAS  PG_NUM  NEW PG_NUM  AUTOSCALE  
other-hdd                         1316                 3.0        109.1T  0.0000        0.1000           0.0092   1.0      32              on         
object-store.rgw.control             0                 3.0        109.1T  0.0000                                  1.0       8              on         
kubernetes-hdd                   44744k                3.0        109.1T  0.0000                                  1.0      32              on         
object-store.rgw.meta             5624                 3.0        109.1T  0.0000                                  1.0       8              on         
object-store.rgw.log              1213k                3.0        109.1T  0.0000                                  1.0       8              on         
volumes-hdd                       6453M                3.0        109.1T  0.0002        0.2000           0.0183   1.0      32              on         
object-store.rgw.buckets.index    6749                 3.0        109.1T  0.0000                                  1.0       8              on         
vms-hdd                           1244M                3.0        109.1T  0.0000        0.4000           0.0367   1.0     512          32  off        
object-store.rgw.buckets.non-ec      0                 3.0        109.1T  0.0000                                  1.0       8              on         
backup-hdd                           0                 3.0        109.1T  0.0000        0.1000           0.0092   1.0      32              off        
.rgw.root                         4693                 3.0        109.1T  0.0000                                  1.0       8              off        
object-store.rgw.buckets.data    11459                 3.0        109.1T  0.0000       10.0000           0.9174   1.0    1024              on         
images-hdd                       19082M                3.0        109.1T  0.0005        0.1000           0.0092   1.0      32              on         
device_health_metrics             6042k                3.0        109.1T  0.0000                                  1.0       1              on  
```


Pool 1 no more crash, try on 10 and 11
```
# ceph osd pool set backup-hdd target_size_ratio 0.1
# ceph osd pool set backup-hdd pg_autoscale_mode on

# ceph osd pool set .rgw.root target_size_ratio 0.1
# ceph osd pool set .rgw.root pg_autoscale_mode on
```

```
$ kubectl get po  -n rook-ceph | grep osd
rook-ceph-osd-0-7857d658ff-5nqrh                                  1/1     Running            0          158m
rook-ceph-osd-1-5f8569d467-rlfk7                                  1/1     Running            12         31m
rook-ceph-osd-10-6b65d6fc84-nl5l5                                 0/1     CrashLoopBackOff   9          29m
rook-ceph-osd-11-6b6b48fc48-skqtg                                 0/1     CrashLoopBackOff   9          28m
rook-ceph-osd-12-686f986849-d648t                                 1/1     Running            0          5d1h
rook-ceph-osd-13-86949b57b9-7jkkv                                 1/1     Running            0          5d
rook-ceph-osd-14-5f8f8b68f7-jzxk2                                 1/1     Running            0          5d1h
rook-ceph-osd-15-7f898f8d79-q2zps                                 1/1     Running            0          5d
rook-ceph-osd-16-9b4f9f967-rzcnv                                  1/1     Running            0          5d
rook-ceph-osd-17-7dc6bd766c-kvtlj                                 1/1     Running            0          5d
rook-ceph-osd-2-6cc79fb5c5-gcrbg                                  1/1     Running            0          158m
rook-ceph-osd-3-77c4847b4b-dr8gx                                  1/1     Running            0          5d1h
rook-ceph-osd-4-b5f6d7bb4-wqdqz                                   1/1     Running            0          158m
rook-ceph-osd-5-6974d8dc6c-4pwb7                                  1/1     Running            0          5d1h
rook-ceph-osd-6-79c58969-5nkzd                                    1/1     Running            0          158m
rook-ceph-osd-7-6f9ff7c78f-jbrtp                                  1/1     Running            0          5d1h
rook-ceph-osd-8-854f59584d-smdrp                                  1/1     Running            0          30m
rook-ceph-osd-9-544fbdc878-mh7gn                                  1/1     Running            0          158m
```

```
#  ceph osd pool autoscale-status
POOL                               SIZE  TARGET SIZE  RATE  RAW CAPACITY   RATIO  TARGET RATIO  EFFECTIVE RATIO  BIAS  PG_NUM  NEW PG_NUM  AUTOSCALE  
other-hdd                         1316                 3.0        109.1T  0.0000        0.1000           0.0091   1.0      32              on         
object-store.rgw.control             0                 3.0        109.1T  0.0000                                  1.0       8              on         
kubernetes-hdd                   44744k                3.0        109.1T  0.0000                                  1.0      32              on         
object-store.rgw.meta             5624                 3.0        109.1T  0.0000                                  1.0       8              on         
object-store.rgw.log              1213k                3.0        109.1T  0.0000                                  1.0       8              on         
volumes-hdd                       6453M                3.0        109.1T  0.0002        0.2000           0.0182   1.0      32              on         
object-store.rgw.buckets.index    6749                 3.0        109.1T  0.0000                                  1.0       8              on         
vms-hdd                           1244M                3.0        109.1T  0.0000        0.4000           0.0364   1.0     512          32  off        
object-store.rgw.buckets.non-ec      0                 3.0        109.1T  0.0000                                  1.0       8              on         
backup-hdd                           0                 3.0        109.1T  0.0000        0.1000           0.0091   1.0      32              on         
.rgw.root                         4693                 3.0        109.1T  0.0000        0.1000           0.0091   1.0       8              on         
object-store.rgw.buckets.data    11459                 3.0        109.1T  0.0000       10.0000           0.9091   1.0    1024              on         
images-hdd                       19082M                3.0        109.1T  0.0005        0.1000           0.0091   1.0      32              on         
device_health_metrics             6042k                3.0        109.1T  0.0000                                  1.0       1              on         
```


I have noticed that vms-hdd is empty, but has has 578 OBJECTS.

```
# rbd ls vms-hdd
# 
```

```
#  ceph df
--- RAW STORAGE ---
CLASS  SIZE     AVAIL    USED    RAW USED  %RAW USED
hdd    109 TiB  109 TiB  51 GiB    66 GiB       0.06
TOTAL  109 TiB  109 TiB  51 GiB    66 GiB       0.06
 
--- POOLS ---
POOL                             ID  PGS   STORED   OBJECTS  USED     %USED  MAX AVAIL
other-hdd                         1    32  1.3 KiB        0  3.9 KiB      0     41 TiB
object-store.rgw.control          2     8      0 B        8      0 B      0     41 TiB
kubernetes-hdd                    3    32   53 MiB       45  166 MiB      0     41 TiB
object-store.rgw.meta             4     8  5.5 KiB       17  3.4 MiB      0     41 TiB
object-store.rgw.log              5     8  1.2 MiB      217   11 MiB      0     41 TiB
volumes-hdd                       6    32  6.3 GiB    1.02k   19 GiB   0.01     41 TiB
object-store.rgw.buckets.index    7     8  6.6 KiB       33   20 KiB      0     41 TiB
vms-hdd                           8   498  1.2 GiB      578  3.6 GiB      0     42 TiB
object-store.rgw.buckets.non-ec   9     8      0 B        0      0 B      0     41 TiB
backup-hdd                       10    32      0 B        0      0 B      0     41 TiB
.rgw.root                        11     8  4.6 KiB       16  3.4 MiB      0     41 TiB
object-store.rgw.buckets.data    12  1024   11 KiB        3  512 KiB      0     41 TiB
images-hdd                       13    32   19 GiB    1.31k   56 GiB   0.04     41 TiB
device_health_metrics            14     1  6.2 MiB       18   19 MiB      0     41 TiB
```

---
Commandes incident ceph

```bash
 1980  cd _LOCAL/developpements/openstack/mosk-deploy/
 1981  ll
 1982  source mcc-env.sh
 1983  kubectl get KaaSCephCluster
 1984  kubectl get ns
 1985  kubectl -n br-mosk001 get KaaSCephCluster
 1986  kubectl -n br-mosk001 edit  KaaSCephCluster br-mosk001-ceph
 1987  cd ..
 1988  ll
 1989  source mosk-env.sh
 1990  kubectl -n rook-ceph get pod
 1991  kubectl -n rook-ceph exec -it rook-ceph-osd-1-5f8569d467-zpds9 -bash
 1992  kubectl -n rook-ceph exec -it rook-ceph-osd-1-5f8569d467-zpds9 -- bash
 1993  kubectl -n rook-ceph get pod
 1994  kubectl -n rook-ceph exec -it rook-ceph-osd-10-6b65d6fc84-jwd9z  -- bash
 1995  cd ..
 1996  ll
 1997  source mcc-env.sh
 1998  kubectl -n br-mosk001 edit  KaaSCephCluster br-mosk001-ceph
 1999  cd ..
 2000  ll
 2001  source mosk-env.sh
 2002  kubectl -n rook-ceph get pods
 2003  kubectl -n rook-ceph exec -it rook-ceph-osd-0-6cf85f88b5-85lzr -- bash
 2004  kubectl -n rook-ceph get pods
 2005  kubectl -n rook-ceph exec -it rook-ceph-osd-3-74f8f54d4-ghcq5  -- bash
 2006  kubectl -n rook-ceph delete deploy rook-ceph-osd-10
 2007  kubectl -n rook-ceph get pods
 2008  kubectl -n rook-ceph get deploy
 2009  kubectl -n rook-ceph scale deploy rook-ceph-operator --replicas 1
 2010  kubectl -n rook-ceph get deploy
 2011  kubectl -n rook-ceph get pods
 2012  kubectl -n rook-ceph get pods | grep ope
 2013  kubectl -n rook-ceph logs rook-ceph-operator-6dff7f9897-4v4rj -f
 2014  kubectl -n rook-ceph get deploy
 2015  kubectl -n rook-ceph scale deploy rook-ceph-operator --replicas 0
 2016  kubectl -n rook-ceph get pods
 2017  kubectl -n rook-ceph exec -it rook-ceph-osd-0-6cf85f88b5-85lzr -- bash
 2018  kubectl -n rook-ceph scale deploy rook-ceph-operator --replicas 0
 2019  kubectl -n rook-ceph scale deploy rook-ceph-operator --replicas 1
 2020  kubectl -n rook-ceph get pods
 2021  kubectl -n rook-ceph logs rook-ceph-operator-6dff7f9897-v78zn -f
 2022  kubectl -n ceph-lcm-mirantis get pods
 2023  kubectl -n ceph-lcm-mirantis logs ceph-controller-b5db445c4-ssrdd
 2024  kubectl -n ceph-lcm-mirantis logs ceph-controller-b5db445c4-ssrdd -f
 2025  vim ~/.ssh/id_ed25519.pub 
 2026  history

 ---
  1454  cd developpements/openstack/mosk-deploy/
 1455  source mosk-env.sh
 1456  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1457  cd _LOCAL/developpements/openstack/mosk-deploy/
 1458  source mosk-env.sh
 1459  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1460  cd _LOCAL/
 1461  ls
 1462  cd developpements/openstack/mosk-deploy/
 1463  source mosk-env.sh
 1464  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml 
 1465  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml  | grep scale
 1466  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml  | grep -i scale
 1467  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml  | grep -i auto
 1468  kubectl get po  -n rook-ceph | grep osd
 1469  kubectl get po  -n rook-ceph 
 1470  kubectl get po  -n rook-ceph | grep osd
 1471  kubectl get po  -n rook-ceph | grep osd grep -v Completed
 1472  kubectl get po  -n rook-ceph | grep osd | grep -v Completed
 1473  kubectl get po  -n rook-ceph -o wide  | grep osd | grep -v Completed
 1474  kubectl get po  -n rook-ceph | grep osd
 1475  kubectl delete po rook-ceph-osd-1-5f8569d467-7jmjm   -n rook-ceph 
 1476  kubectl delete po rook-ceph-osd-8-854f59584d-szz28  -n rook-ceph 
 1477  kubectl delete po rook-ceph-osd-10-6b65d6fc84-nfg58   -n rook-ceph 
 1478  kubectl delete po rook-ceph-osd-11-6b6b48fc48-l5z5t   -n rook-ceph 
 1479  kubectl get po  -n rook-ceph | grep osd

---
 1149  cd _LOCAL/developpements/openstack/mosk-deploy/
 1150  ll
 1151  source mosk-env.sh 
 1152  kubectl -n openstack get  openstackdeployment
 1153  kubectl -n openstack edit  openstackdeployment
 1154  kubectl -n openstack get osdplst
 1155  kubectl -n openstack get openstackdeployment $(kubectl -n openstack get openstackdeployment | tail -1 | awk '{print $1}') -o yaml | grep openstack_version |tail -1
 1156  kubectl -n openstack get osdplst
 1157  cd ../..
 1158  cd ope
 1159  cd operations/
 1160  source auth/admin-openrc.sh 
 1161  openstack server list --all
 1162  openstack server list --all | grep 35a2727980ae6066
 1163  openstack server list --all | grep e28300da06e03684
 1164  openstack node list 
 1165  openstack hypervisor list
 1166  openstack hypervisor list | grep 35a2727980ae6066
 1167  openstack service compute  list | grep 35a2727980ae6066
 1168  openstack  compute  list 
 1169  openstack compute service  list | grep 35a2727980ae6066
 1170  openstack compute service  list 
 1171  openstack compute service  kaas-node-3ca03bf6-8687-4927-b841-36701c17b269
 1172  openstack compute service list  kaas-node-3ca03bf6-8687-4927-b841-36701c17b269
 1173  openstack compute service list --host kaas-node-3ca03bf6-8687-4927-b841-36701c17b269
 1174  ssh os-admin
 1175  kubectl -n openstack get ds |grep nova
 1176  kubectl -n openstack get ds 
 1177  kubectl -n openstack get daemonsets 
 1178  kubectl -n openstack get daemonset
 1179  kubectl -n openstack get daemonset -o yaml
 1180  kubectl -n openstack get daemonset
 1181  kubectl -n openstack get daemonset nova-compute-default
 1182  kubectl -n openstack get daemonset nova-compute-default -o yaml
 1183  kubectl -n openstack get daemonset nova-compute-default -o yaml | grep 35a2727980ae6066
 1184  kubectl -n openstack get osdpl -o yaml
 1185  kubectl -n openstack get osdpl -o yaml | less
 1186  kubectl -n openstack get osdplst -o yaml | less
 1187  kubectl -n openstack edit osdplst 
 1188  kubectl -n openstack get osdplst -o yaml | less
 1189  kubectl -n rook-ceph get cephobjectstore -o yaml
 1190  kubectl -n openstack get osdplst
 1191  telnet stack-d01-02.net.enst-bretagne.fr
 1192  cd ..
 1193  cd mosk-deploy/
 1194  ll
 1195  source mcc-env.sh
 1196  cd nodes/
 1197  ll
 1198  cd cmp-13/
 1199  ll
 1200  kub apply -f br-mosk001-cmp-13_bmhost.yaml
 1201  kub create -f br-mosk001-cmp-13_machine.yaml 
 1202  vim br-mosk001-cmp-13_bmhost.yaml 
 1203  kub get lcmmachine -o wide
 1204  cd ../..
 1205  ll
 1206  cd mosk-deploy/
 1207  source mosk-env.sh 
 1208  kubectl get po  -n rook-ceph | grep tools
 1209  kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';
 1210  ${kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';}
 1211  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1212  ssh root@10.29.224.23
 1213  for i in $(kubectl get lcmmachines -n br-mosk001 | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines -n br-mosk001 $i -o yaml | grep release | tail -1; done
 1214  cd ..
 1215  source mcc-env.sh 
 1216  for i in $(kubectl get lcmmachines -n br-mosk001 | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines -n br-mosk001 $i -o yaml | grep release | tail -1; done
 1217  cd ..
 1218  source mosk-env.sh 
 1219  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1220  kubectl logs 
 1221  rook-ceph-osd-1-5f8569d467-jsspq
 1222  kubectl logs rook-ceph-osd-1-5f8569d467-jsspq
 1223  kubectl get po  -n rook-ceph 
 1224  kubectl get po  -n rook-ceph | grep '-osd-1'
 1225  kubectl get po  -n rook-ceph | grep "-osd-1"
 1226  kubectl get po  -n rook-ceph | grep osd
 1227  kubectl logs rook-ceph-osd-1-5f8569d467-jsspq
 1228  kubectl logs rook-ceph-osd-12-686f986849-d648t
 1229  kubectl get po  rook-ceph-osd-12-686f986849-d648t
 1230  kubectl -n rook-ceph logs rook-ceph-osd-1-5f8569d467-jsspq
 1231  kubectl -n rook-ceph logs rook-ceph-osd-1-5f8569d467-jsspq > /tmp/rook-ceph-osd-1-5f8569d467-jsspq.logs
 1232  cat /tmp/rook-ceph-osd-1-5f8569d467-jsspq.logs
 1233  kubectl get po  -n rook-ceph | grep osd
 1234  kubectl -n rook-ceph logs rook-ceph-osd-10-6b65d6fc84-6bk6m > /tmp/rook-ceph-osd-10-6b65d6fc84-6bk6m.logs
 1235  kubectl -n rook-ceph logs rook-ceph-osd-11-6b6b48fc48-74xqx > /tmp/rook-ceph-osd-11-6b6b48fc48-74xqx.logs
 1236  kubectl -n rook-ceph get pod -o wide -l app=rook-ceph-osd | grep osd-1
 1237  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1238  kubectl -n rook-ceph get pod -o wide -l app=rook-ceph-osd
 1239  ceph -s
 1240  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1241  kubectl -n rook-ceph get pod -o wide -l app=rook-ceph-osd | grep osd
 1242  kubectl -n rook-ceph get pod 
 1243  kubectl -n rook-ceph get pod  | grep osd
 1244  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "(bash || ash || sh)"
 1245  cd ..
 1246  grep -r kaascephcluster * 
 1247  source mosk-env.sh 
 1248  kub get kaascephcluster
 1249  vim MOSK/kaas-bootstrap/bootstrap.sh
 1250  cd ..
 1251  vim MOSK/kaas-bootstrap/bootstrap.sh
 1252  find . -name kaascephcluster.yaml
 1253  vim ./br-mosk001/cluster/kaascephcluster.yaml
 1254  kub get br-mosk001-ceph
 1255  find . -name kaascephcluster.yaml
 1256  vim ./MOSK/kaas-bootstrap/br-mosk001/kaascephcluster.yaml
 1257  kub -n br-mosk001   get  kaascephcluster br-mosk001-ceph
 1258  vim ./MOSK/kaas-bootstrap/br-mosk001/kaascephcluster.yaml
 1259  kub -n br-mosk001  get KaaSCephCluster
 1260  source mcc-env.sh 
 1261  kub  get KaaSCephCluster
 1262  kub -n br-mosk001  get KaaSCephCluster
 1263  kub -n br-mosk001  get KaaSCephCluster br-mosk001-ceph
 1264  kub -n br-mosk001  get KaaSCephCluster br-mosk001-ceph -o yaml | less
 1265  kubectl -n ceph-lcm-mirantis get miraceph br-mosk001-ceph -o yaml | less
 1266  kubectl -n ceph-lcm-mirantis get miraceph 
 1267  cd ..
 1268  source mosk-env.sh 
 1269  kubectl -n ceph-lcm-mirantis get miraceph
 1270  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml | less
 1271  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml | grep -i objectStore
 1272  kubectl -n ceph-lcm-mirantis get miraceph rook-ceph -o yaml 
 1273  history
 1274  kubectl -n rook-ceph edit deploy rook-ceph-osd-1
 1275  kubectl get po  -n rook-ceph | grep osd
 1276  kubectl get po  -n rook-ceph | grep osd-1
 1277  kubectl -n rook-ceph logs rook-ceph-osd-1-84ff49bbc5-mjx2p > /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p.logs
 1278  kubectl get po  -n rook-ceph | grep osd-1
 1279  cat /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p.logs
 1280  kubectl get po  -n rook-ceph | grep osd-1
 1281  kubectl get po  -n rook-ceph | grep osd-1-
 1282  kubectl get po  -n rook-ceph | grep osd-1
 1283  kubectl get po  -n rook-ceph | grep osd-1-
 1284  kubectl get po  -n rook-ceph | grep osd-1
 1285  kubectl -n rook-ceph edit deploy rook-ceph-osd-1
 1286  kubectl -n rook-ceph edit deploy rook-ceph-osd-10
 1287  kubectl get po  -n rook-ceph | grep osd-1
 1288  kubectl -n rook-ceph edit deploy rook-ceph-osd-11
 1289  kubectl get po  -n rook-ceph | grep osd-1
 1290  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph -s"
 1291  kubectl get po  -n rook-ceph | grep osd-1
 1292  kubectl -n rook-ceph logs rook-ceph-osd-1-84ff49bbc5-mjx2p > /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p.logs
 1293  kubectl get po  -n rook-ceph | grep osd-1
 1294  ceph osd pool autoscale-status
 1295  kubectl get po  -n rook-ceph | grep osd-1
 1296  kubectl -n rook-ceph logs rook-ceph-osd-1-84ff49bbc5-mjx2p > /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p_2.logs
 1297  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg dump_stuck"
 1298  kubectl get po  -n rook-ceph | grep osd-1
 1299  ceph pg ls-by-osd 11
 1300  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 11"
 1301  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 12"
 1302  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 10"
 1303  ceph pg ls-by-osd 11
 1304  kubectl get po  -n rook-ceph | grep osd-1
 1305  kubectl -n rook-ceph logs rook-ceph-osd-10-58f54b554d-qqn6w > /tmp/rook-ceph-osd-10-58f54b554d-qqn6w
 1306  kubectl -n rook-ceph logs rook-ceph-osd-11-65c69bd48d-hv2fj > /tmp/rook-ceph-osd-11-65c69bd48d-hv2fj
 1307  kubectl get po  -n rook-ceph | grep osd-1
 1308  kubectl -n rook-ceph logs rook-ceph-osd-1-84ff49bbc5-mjx2p > /tmp/rook-ceph-osd-1-84ff49bbc5-mjx2p_2.logs
 1309  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 1"
 1310  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 10"
 1311  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph pg ls-by-osd 11"
 1312  kubectl -n rook-ceph edit deploy rook-ceph-osd-11
 1313  kubectl -n rook-ceph edit deploy rook-ceph-osd-10
 1314  kubectl -n rook-ceph edit deploy rook-ceph-osd-1
 1315  kubectl get po  -n rook-ceph | grep osd
 1316  kubectl get po  -n rook-ceph | grep osd-1
 1317  kubectl -n rook-ceph logs rook-ceph-osd-10-ddbc7bf9b-4q5h9 > /tmp/rook-ceph-osd-10-ddbc7bf9b-4q5h9.log
 1318  kubectl -n rook-ceph logs rook-ceph-osd-11-7f499cd596-g8brc > /tmp/rook-ceph-osd-11-7f499cd596-g8brc.log
 1319  kubectl get po  -n rook-ceph | grep osd-1
 1320  kubectl -n rook-ceph logs rook-ceph-osd-1-7f947d6d67-wf9fr > /tmp/rook-ceph-osd-1-7f947d6d67-wf9fr.log
 1321  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph osd dump"
 1322  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "ceph osd dump" | wc
 1323  ll
 1324  rm CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tgz 
 1325  ll
 1326  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';) -c rook-ceph-tools "--" sh -c "bash"
 1327  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';):CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz
 1328  kubectl exec -i -t -n rook-ceph  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';):./CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz
 1329  kubectl -n rook-ceph cp  $(kubectl get po  -n rook-ceph | grep tools | awk '{print $1}';):CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz CephCollectData.IMT-Atlantique.br-mosk-01.2021-11-19.tar.gz



```