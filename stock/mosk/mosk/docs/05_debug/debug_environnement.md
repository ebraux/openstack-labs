# Debug - Infos sur l'environnement

Liste des Namespace
```bash
kubectl get ns
---
NAME                    STATUS   AGE
br-mosk001              Active   12d
ceph                    Active   96d
ceph-lcm-mirantis       Active   96d
default                 Active   96d
istio-system            Active   96d
kaas                    Active   96d
kube-node-lease         Active   96d
kube-public             Active   96d
kube-system             Active   96d
local-path-storage      Active   96d
metallb-system          Active   96d
openstack-ceph-shared   Active   96d
rook-ceph               Active   96d
stacklight              Active   96d
```