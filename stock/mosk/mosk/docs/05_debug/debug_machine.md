# Debug des "Machine"

```bash
1) you have wrong place for node label in machine:
metadata:
  name: br-mosk001-cmp-4
  namespace: br-mosk001
  labels:
    kaas.mirantis.com/region: region-one
    hostlabel.bm.kaas.mirantis.com/worker: "true"
    kaas.mirantis.com/provider: baremetal
    cluster.sigs.k8s.io/cluster-name: kaas-br-mosk001
    openstack.nova.storage: lvm <<< 
as far i know, that wrong label place for extra labels, might be it will be ignored  => those label will not be passed to target k8s nodes. just information generally, not an issue
```

## Gestion des "Machines"

```bash
kubectl -n br-mosk001 get machines
---
NAME                  PROVIDERID   PHASE
br-mosk001-ceph-0                  
br-mosk001-ceph-1                  
br-mosk001-ceph-2                  
br-mosk001-cmp-0                   
br-mosk001-cmp-1                   
br-mosk001-cmp-2                   
br-mosk001-cmp-3                   
br-mosk001-master-0                
br-mosk001-master-1                
br-mosk001-master-2                
```

```bash
kubectl -n br-mosk001 get machine 
kubectl -n br-mosk001 get machine  br-mosk001-cmp-1 -o yaml |less
kubectl -n br-mosk001 edit machine 
kubectl -n br-mosk001 delete machine br-mosk001-master-2
kubectl -n br-mosk001 delete machine --all
```

## Bilan de quel BMH est affecté à quel

```bash
kubectl get bmh -A
NAMESPACE    NAME                  STATE         CONSUMER              BOOTMODE   ONLINE   ERROR   REGION
br-mosk001   br-mosk001-ceph-0     provisioned   br-mosk001-ceph-0     UEFI       true             region-one
br-mosk001   br-mosk001-ceph-1     provisioned   br-mosk001-ceph-1     UEFI       true             region-one
br-mosk001   br-mosk001-ceph-2     provisioned   br-mosk001-ceph-2     UEFI       true             region-one
br-mosk001   br-mosk001-cmp-0      provisioned   br-mosk001-cmp-0      UEFI       true             region-one
br-mosk001   br-mosk001-cmp-1      provisioned   br-mosk001-cmp-1      UEFI       true             region-one
br-mosk001   br-mosk001-cmp-2      provisioned   br-mosk001-cmp-2      UEFI       true             region-one
br-mosk001   br-mosk001-cmp-3      provisioned   br-mosk001-cmp-3      UEFI       true             region-one
br-mosk001   br-mosk001-cmp-4      ready                               UEFI       true             region-one
br-mosk001   br-mosk001-cmp-5      ready         br-mosk001-cmp-5      UEFI       true             region-one
br-mosk001   br-mosk001-master-0   provisioned   br-mosk001-master-0   UEFI       true             region-one
br-mosk001   br-mosk001-master-1   provisioned   br-mosk001-master-1   UEFI       true             region-one
br-mosk001   br-mosk001-master-2   provisioned   br-mosk001-master-2   UEFI       true             region-one
default      master-0              provisioned   master-0              UEFI       true             region-one
default      master-1              provisioned   master-1              UEFI       true             region-one
default      master-2              provisioned   master-2              UEFI       true             region-one

```


```bash
kubectl get lcmmachines -A -o wide;
NAMESPACE    NAME                  CLUSTERNAME       TYPE      STATE   INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001   br-mosk001-ceph-0     kaas-br-mosk001   worker    Ready   10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-ceph-1     kaas-br-mosk001   worker    Ready   10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-ceph-2     kaas-br-mosk001   worker    Ready   10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-cmp-0      kaas-br-mosk001   worker    Ready   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-cmp-1      kaas-br-mosk001   worker    Ready   10.129.172.110   kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-cmp-2      kaas-br-mosk001   worker    Ready   10.129.172.111   kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-cmp-3      kaas-br-mosk001   worker    Ready   10.129.172.112   kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-cmp-5      kaas-br-mosk001   worker                                                                              
br-mosk001   br-mosk001-master-0   kaas-br-mosk001   control   Ready   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-master-1   kaas-br-mosk001   control   Ready   10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.2.0-349-g4870b7f5
br-mosk001   br-mosk001-master-2   kaas-br-mosk001   control   Ready   10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.2.0-349-g4870b7f5
default      master-0              kaas-mgmt         control   Ready   10.129.172.101   kaas-node-d09c78aa-00c3-40bc-b44d-957e015a8e51   v0.2.0-399-g85be100f
default      master-1              kaas-mgmt         control   Ready   10.129.172.102   kaas-node-34412b23-0e4b-42c1-949c-4f20d79fc858   v0.2.0-399-g85be100f
default      master-2              kaas-mgmt         control   Ready   10.129.172.100   kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0   v0.2.0-399-g85be100f

```

---
### exemple de Machine

`kubectl.kubernetes.io/last-applied-configuration`
```yaml
"apiVersion":"cluster.k8s.io/v1alpha1",
"kind":"Machine",
"metadata":
{
    "annotations":{},
    "labels":
    {
        "cluster.sigs.k8s.io/cluster-name":"kaas-br-mosk001",
        "hostlabel.bm.kaas.mirantis.com/worker":"true",
        "kaas.mirantis.com/provider":"baremetal",
        "kaas.mirantis.com/region":"region-one",
        "openstack.nova.storage":"ceph"
    },
    "name":"br-mosk001-cmp-1",
    "namespace":"br-mosk001"
},
"spec":
{
    "providerSpec":
     {
        "value":
        {
            "apiVersion":"baremetal.k8s.io/v1alpha1",
            "bareMetalHostProfile":
            {
                "name":"mosk-compute-ceph",
                "namespace":"br-mosk001"
            },
            "hostSelector":
            {
                "matchLabels":
                {
                    "baremetal":"hw-br-mosk001-cmp-1"
                }
            },
            "kind":"BareMetalMachineProviderSpec",
            "l2TemplateSelector":
            {
                "name":"br-mosk001-6nic-cmp-ceph","namespace":"br-mosk001"
            },
            "nodeLabels":
            [
              {
                  "key":"openstack-compute-node",
                  "value":"enabled"
              },
              {
                  "key":"openvswitch",
                  "value":"enabled"
              }
            ]
        }
    }
}
```
