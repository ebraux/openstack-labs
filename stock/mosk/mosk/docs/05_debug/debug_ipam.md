

```kmosk get ipamhosts
NAME                  STATUS   AGE     REGION
br-mosk001-ceph-0     OK       40d     region-one
br-mosk001-ceph-1     OK       40d     region-one
br-mosk001-ceph-2     OK       40d     region-one
br-mosk001-cmp-0      OK       40d     region-one
br-mosk001-cmp-05     OK       18d     region-one
br-mosk001-cmp-06     OK       18d     region-one
br-mosk001-cmp-07     OK       13d     region-one
br-mosk001-cmp-09     OK       18d     region-one
br-mosk001-cmp-14     OK       5d17h   region-one
br-mosk001-cmp-20     OK       13d     region-one
br-mosk001-master-0   OK       40d     region-one
br-mosk001-master-1   OK       40d     region-one
br-mosk001-master-2   OK       40d     region-one
```

```kmosk  get ipaddrs
NAME                                                  MAC                                                 CIDR                STATUS   AGE
auto-00-22-19-6b-83-12                                00:22:19:6B:83:12                                   10.129.172.114/24   Active   21d
auto-18-66-da-ec-4b-3c                                18:66:DA:EC:4B:3C                                   10.129.172.117/24   Active   13d
auto-18-66-da-ed-2c-3c                                18:66:DA:ED:2C:3C                                   10.129.172.111/24   Active   5d17h
auto-78-ac-44-08-c0-10                                78:AC:44:08:C0:10                                   10.129.172.116/24   Active   18d
auto-78-ac-44-08-c0-18                                78:AC:44:08:C0:18                                   10.129.172.115/24   Active   18d
auto-br-tenant-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   VI:BR-TENANT:17EF5D4D-F896-4F84-BC84-F02E63ED76CD   192.168.179.23/24   Active   18d
auto-br-tenant-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   VI:BR-TENANT:1E389E22-2A10-4B03-9C2D-37255D4F4CA4   192.168.179.11/24   Active   40d
auto-br-tenant-38538401-efd4-4ddc-a328-7666724b917f   VI:BR-TENANT:38538401-EFD4-4DDC-A328-7666724B917F   192.168.179.25/24   Active   13d
auto-br-tenant-5d555e1d-0636-45a5-868c-b0a3350efe6f   VI:BR-TENANT:5D555E1D-0636-45A5-868C-B0A3350EFE6F   192.168.179.13/24   Active   40d
auto-br-tenant-719fafd5-4d68-4c88-8c0e-8364cba48059   VI:BR-TENANT:719FAFD5-4D68-4C88-8C0E-8364CBA48059   192.168.179.20/24   Active   18d
auto-br-tenant-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   VI:BR-TENANT:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB   192.168.179.12/24   Active   40d
auto-br-tenant-8a7273c8-f2da-4e34-a088-e76f90fa8504   VI:BR-TENANT:8A7273C8-F2DA-4E34-A088-E76F90FA8504   192.168.179.15/24   Active   40d
auto-br-tenant-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0   VI:BR-TENANT:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0   192.168.179.21/24   Active   21d
auto-br-tenant-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   VI:BR-TENANT:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C   192.168.179.14/24   Active   40d
auto-br-tenant-a15c808c-d1cd-4347-8ee8-b5d553ca096c   VI:BR-TENANT:A15C808C-D1CD-4347-8EE8-B5D553CA096C   192.168.179.16/24   Active   40d
auto-br-tenant-b10834cf-25a4-40e9-85be-d7be198417fa   VI:BR-TENANT:B10834CF-25A4-40E9-85BE-D7BE198417FA   192.168.179.10/24   Active   40d
auto-br-tenant-d2144611-3110-417f-8d20-ac38b6ddbdbf   VI:BR-TENANT:D2144611-3110-417F-8D20-AC38B6DDBDBF   192.168.179.18/24   Active   5d17h
auto-br-tenant-d29a52d3-5bed-4027-992a-5f94b1089163   VI:BR-TENANT:D29A52D3-5BED-4027-992A-5F94B1089163   192.168.179.24/24   Active   13d
auto-br-tenant-d646e156-80a1-4c0c-a102-11669fd37cea   VI:BR-TENANT:D646E156-80A1-4C0C-A102-11669FD37CEA   192.168.179.22/24   Active   18d
auto-ceph-cl-17ef5d4d-f896-4f84-bc84-f02e63ed76cd     VI:CEPH-CL:17EF5D4D-F896-4F84-BC84-F02E63ED76CD     192.168.176.23/24   Active   18d
auto-ceph-cl-1e389e22-2a10-4b03-9c2d-37255d4f4ca4     VI:CEPH-CL:1E389E22-2A10-4B03-9C2D-37255D4F4CA4     192.168.176.11/24   Active   40d
auto-ceph-cl-38538401-efd4-4ddc-a328-7666724b917f     VI:CEPH-CL:38538401-EFD4-4DDC-A328-7666724B917F     192.168.176.25/24   Active   13d
auto-ceph-cl-5d555e1d-0636-45a5-868c-b0a3350efe6f     VI:CEPH-CL:5D555E1D-0636-45A5-868C-B0A3350EFE6F     192.168.176.13/24   Active   40d
auto-ceph-cl-719fafd5-4d68-4c88-8c0e-8364cba48059     VI:CEPH-CL:719FAFD5-4D68-4C88-8C0E-8364CBA48059     192.168.176.20/24   Active   18d
auto-ceph-cl-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb     VI:CEPH-CL:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB     192.168.176.12/24   Active   40d
auto-ceph-cl-8a7273c8-f2da-4e34-a088-e76f90fa8504     VI:CEPH-CL:8A7273C8-F2DA-4E34-A088-E76F90FA8504     192.168.176.15/24   Active   40d
auto-ceph-cl-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0     VI:CEPH-CL:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0     192.168.176.21/24   Active   21d
auto-ceph-cl-92e4e163-04ba-4890-be3a-ad81f4f8ad8c     VI:CEPH-CL:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C     192.168.176.14/24   Active   40d
auto-ceph-cl-a15c808c-d1cd-4347-8ee8-b5d553ca096c     VI:CEPH-CL:A15C808C-D1CD-4347-8EE8-B5D553CA096C     192.168.176.16/24   Active   40d
auto-ceph-cl-b10834cf-25a4-40e9-85be-d7be198417fa     VI:CEPH-CL:B10834CF-25A4-40E9-85BE-D7BE198417FA     192.168.176.10/24   Active   40d
auto-ceph-cl-d2144611-3110-417f-8d20-ac38b6ddbdbf     VI:CEPH-CL:D2144611-3110-417F-8D20-AC38B6DDBDBF     192.168.176.18/24   Active   5d17h
auto-ceph-cl-d29a52d3-5bed-4027-992a-5f94b1089163     VI:CEPH-CL:D29A52D3-5BED-4027-992A-5F94B1089163     192.168.176.24/24   Active   13d
auto-ceph-cl-d646e156-80a1-4c0c-a102-11669fd37cea     VI:CEPH-CL:D646E156-80A1-4C0C-A102-11669FD37CEA     192.168.176.22/24   Active   18d
auto-ceph-rep-17ef5d4d-f896-4f84-bc84-f02e63ed76cd    VI:CEPH-REP:17EF5D4D-F896-4F84-BC84-F02E63ED76CD    192.168.177.23/24   Active   18d
auto-ceph-rep-1e389e22-2a10-4b03-9c2d-37255d4f4ca4    VI:CEPH-REP:1E389E22-2A10-4B03-9C2D-37255D4F4CA4    192.168.177.11/24   Active   40d
auto-ceph-rep-38538401-efd4-4ddc-a328-7666724b917f    VI:CEPH-REP:38538401-EFD4-4DDC-A328-7666724B917F    192.168.177.25/24   Active   13d
auto-ceph-rep-5d555e1d-0636-45a5-868c-b0a3350efe6f    VI:CEPH-REP:5D555E1D-0636-45A5-868C-B0A3350EFE6F    192.168.177.13/24   Active   40d
auto-ceph-rep-719fafd5-4d68-4c88-8c0e-8364cba48059    VI:CEPH-REP:719FAFD5-4D68-4C88-8C0E-8364CBA48059    192.168.177.20/24   Active   18d
auto-ceph-rep-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb    VI:CEPH-REP:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB    192.168.177.12/24   Active   40d
auto-ceph-rep-8a7273c8-f2da-4e34-a088-e76f90fa8504    VI:CEPH-REP:8A7273C8-F2DA-4E34-A088-E76F90FA8504    192.168.177.15/24   Active   40d
auto-ceph-rep-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0    VI:CEPH-REP:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0    192.168.177.21/24   Active   21d
auto-ceph-rep-92e4e163-04ba-4890-be3a-ad81f4f8ad8c    VI:CEPH-REP:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C    192.168.177.14/24   Active   40d
auto-ceph-rep-a15c808c-d1cd-4347-8ee8-b5d553ca096c    VI:CEPH-REP:A15C808C-D1CD-4347-8EE8-B5D553CA096C    192.168.177.16/24   Active   40d
auto-ceph-rep-b10834cf-25a4-40e9-85be-d7be198417fa    VI:CEPH-REP:B10834CF-25A4-40E9-85BE-D7BE198417FA    192.168.177.10/24   Active   40d
auto-ceph-rep-d2144611-3110-417f-8d20-ac38b6ddbdbf    VI:CEPH-REP:D2144611-3110-417F-8D20-AC38B6DDBDBF    192.168.177.18/24   Active   5d17h
auto-ceph-rep-d29a52d3-5bed-4027-992a-5f94b1089163    VI:CEPH-REP:D29A52D3-5BED-4027-992A-5F94B1089163    192.168.177.24/24   Active   13d
auto-ceph-rep-d646e156-80a1-4c0c-a102-11669fd37cea    VI:CEPH-REP:D646E156-80A1-4C0C-A102-11669FD37CEA    192.168.177.22/24   Active   18d
auto-e4-43-4b-83-10-2c                                E4:43:4B:83:10:2C                                   10.129.172.113/24   Active   18d
auto-e4-43-4b-e9-37-58                                E4:43:4B:E9:37:58                                   10.129.172.112/24   Active   13d
auto-e4-43-4b-ea-67-50                                E4:43:4B:EA:67:50                                   10.129.172.105/24   Active   40d
auto-e4-43-4b-ea-67-70                                E4:43:4B:EA:67:70                                   10.129.172.106/24   Active   40d
auto-e4-43-4b-ea-67-a0                                E4:43:4B:EA:67:A0                                   10.129.172.104/24   Active   40d
auto-e4-43-4b-ea-73-b0                                E4:43:4B:EA:73:B0                                   10.129.172.109/24   Active   40d
auto-e4-43-4b-ea-b2-b0                                E4:43:4B:EA:B2:B0                                   10.129.172.103/24   Active   40d
auto-e4-43-4b-ea-be-80                                E4:43:4B:EA:BE:80                                   10.129.172.107/24   Active   40d
auto-e4-43-4b-ea-c2-b0                                E4:43:4B:EA:C2:B0                                   10.129.172.108/24   Active   40d
auto-k8s-ext-17ef5d4d-f896-4f84-bc84-f02e63ed76cd     VI:K8S-EXT:17EF5D4D-F896-4F84-BC84-F02E63ED76CD     10.129.175.23/24    Active   18d
auto-k8s-ext-1e389e22-2a10-4b03-9c2d-37255d4f4ca4     VI:K8S-EXT:1E389E22-2A10-4B03-9C2D-37255D4F4CA4     10.129.175.11/24    Active   40d
auto-k8s-ext-38538401-efd4-4ddc-a328-7666724b917f     VI:K8S-EXT:38538401-EFD4-4DDC-A328-7666724B917F     10.129.175.25/24    Active   13d
auto-k8s-ext-5d555e1d-0636-45a5-868c-b0a3350efe6f     VI:K8S-EXT:5D555E1D-0636-45A5-868C-B0A3350EFE6F     10.129.175.13/24    Active   40d
auto-k8s-ext-719fafd5-4d68-4c88-8c0e-8364cba48059     VI:K8S-EXT:719FAFD5-4D68-4C88-8C0E-8364CBA48059     10.129.175.20/24    Active   18d
auto-k8s-ext-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb     VI:K8S-EXT:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB     10.129.175.12/24    Active   40d
auto-k8s-ext-8a7273c8-f2da-4e34-a088-e76f90fa8504     VI:K8S-EXT:8A7273C8-F2DA-4E34-A088-E76F90FA8504     10.129.175.15/24    Active   40d
auto-k8s-ext-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0     VI:K8S-EXT:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0     10.129.175.21/24    Active   21d
auto-k8s-ext-92e4e163-04ba-4890-be3a-ad81f4f8ad8c     VI:K8S-EXT:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C     10.129.175.14/24    Active   40d
auto-k8s-ext-a15c808c-d1cd-4347-8ee8-b5d553ca096c     VI:K8S-EXT:A15C808C-D1CD-4347-8EE8-B5D553CA096C     10.129.175.16/24    Active   40d
auto-k8s-ext-b10834cf-25a4-40e9-85be-d7be198417fa     VI:K8S-EXT:B10834CF-25A4-40E9-85BE-D7BE198417FA     10.129.175.10/24    Active   40d
auto-k8s-ext-d2144611-3110-417f-8d20-ac38b6ddbdbf     VI:K8S-EXT:D2144611-3110-417F-8D20-AC38B6DDBDBF     10.129.175.18/24    Active   5d17h
auto-k8s-ext-d29a52d3-5bed-4027-992a-5f94b1089163     VI:K8S-EXT:D29A52D3-5BED-4027-992A-5F94B1089163     10.129.175.24/24    Active   13d
auto-k8s-ext-d646e156-80a1-4c0c-a102-11669fd37cea     VI:K8S-EXT:D646E156-80A1-4C0C-A102-11669FD37CEA     10.129.175.22/24    Active   18d
auto-k8s-pods-17ef5d4d-f896-4f84-bc84-f02e63ed76cd    VI:K8S-PODS:17EF5D4D-F896-4F84-BC84-F02E63ED76CD    10.129.174.23/24    Active   18d
auto-k8s-pods-1e389e22-2a10-4b03-9c2d-37255d4f4ca4    VI:K8S-PODS:1E389E22-2A10-4B03-9C2D-37255D4F4CA4    10.129.174.11/24    Active   40d
auto-k8s-pods-38538401-efd4-4ddc-a328-7666724b917f    VI:K8S-PODS:38538401-EFD4-4DDC-A328-7666724B917F    10.129.174.25/24    Active   13d
auto-k8s-pods-5d555e1d-0636-45a5-868c-b0a3350efe6f    VI:K8S-PODS:5D555E1D-0636-45A5-868C-B0A3350EFE6F    10.129.174.13/24    Active   40d
auto-k8s-pods-719fafd5-4d68-4c88-8c0e-8364cba48059    VI:K8S-PODS:719FAFD5-4D68-4C88-8C0E-8364CBA48059    10.129.174.20/24    Active   18d
auto-k8s-pods-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb    VI:K8S-PODS:71AA87C1-4248-4A6B-9F96-7B50EAF8A8EB    10.129.174.12/24    Active   40d
auto-k8s-pods-8a7273c8-f2da-4e34-a088-e76f90fa8504    VI:K8S-PODS:8A7273C8-F2DA-4E34-A088-E76F90FA8504    10.129.174.15/24    Active   40d
auto-k8s-pods-8bc3091e-c32d-4855-ab9a-e1df2b8a4ac0    VI:K8S-PODS:8BC3091E-C32D-4855-AB9A-E1DF2B8A4AC0    10.129.174.21/24    Active   21d
auto-k8s-pods-92e4e163-04ba-4890-be3a-ad81f4f8ad8c    VI:K8S-PODS:92E4E163-04BA-4890-BE3A-AD81F4F8AD8C    10.129.174.14/24    Active   40d
auto-k8s-pods-a15c808c-d1cd-4347-8ee8-b5d553ca096c    VI:K8S-PODS:A15C808C-D1CD-4347-8EE8-B5D553CA096C    10.129.174.16/24    Active   40d
auto-k8s-pods-b10834cf-25a4-40e9-85be-d7be198417fa    VI:K8S-PODS:B10834CF-25A4-40E9-85BE-D7BE198417FA    10.129.174.10/24    Active   40d
auto-k8s-pods-d2144611-3110-417f-8d20-ac38b6ddbdbf    VI:K8S-PODS:D2144611-3110-417F-8D20-AC38B6DDBDBF    10.129.174.18/24    Active   5d17h
auto-k8s-pods-d29a52d3-5bed-4027-992a-5f94b1089163    VI:K8S-PODS:D29A52D3-5BED-4027-992A-5F94B1089163    10.129.174.24/24    Active   13d
auto-k8s-pods-d646e156-80a1-4c0c-a102-11669fd37cea    VI:K8S-PODS:D646E156-80A1-4C0C-A102-11669FD37CEA    10.129.174.22/24    Active   18d
```
