
## Diagnostic

Vérifier l'état de Stacklight :
```bash
 kubectl --kubeconfig ~/configs/MCC/kubeconfig  get cluster kaas-br-mosk001 -n br-mosk001  -o jsonpath='{.status.providerStatus.conditions[?(@.type=="StackLight")]}' | jq
{
  "message": "StackLight is fully up.",
  "ready": true,
  "type": "StackLight"
}
```

Vérifier les composants déployés :
```python
$ kubectl get helmbundle stacklight-bundle -n stacklight -o=template='{{range $k, $v := .status.releaseStatuses}}{{printf "%s:\n  %s\n  %v\n" $k $v.status $v.success}}{{end}}'
```

Rechercher les nodes qui hébergent des services "Stacklight"
```bash
kubectl get nodes --show-labels | grep stacklight
```

Afficher les pods tournant sur ces nodes
```bash
kubectl get po -n stacklight -o wide | grep -v -E "kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa|kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb|kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4"
```

---
## modifier la configuration par défaut

source mcc-env.sh

kubectl -n br-mosk001 get cluster 
NAME              AGE
kaas-br-mosk001   61d

kubectl get cluster kaas-br-mosk001  -o yaml
---

## Stacklight


Add `forcedRole: stacklight` to the 3 master nodes
```bash
  labels:
    beta.kubernetes.io/arch: amd64
    beta.kubernetes.io/os: linux
    ...
    forcedRole: stacklight
    stacklight: enabled
```


pour le deployment :
```yaml
      nodeSelector:
        forcedRole: stacklight
        stacklight: enabled

```


```bash
# list de noeuds
source setenv.sh 
kmosk get  lcmmachine -o wide

# edition des noeuds Compute CEPH
cd 
source setenv-mosk.sh 
kmosk edit node kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f
kmosk edit node kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c
kmosk edit node kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504

# modification du deployment de stacklight
kmosk get deployments -n stacklight
kmosk edit  deployments  stacklight-helm-controller -n stacklight
```

# edition des noeuds Master
cd 
source setenv-mosk.sh 
kmosk edit node kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa
kmosk edit node kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4
kmosk edit node kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb

---

## Prometheus and Alertmanager

ok, good. In that case let’s start from Prometheus and Alertmanager:
```bash
kubectl scale --replicas=0 statefulset.apps/prometheus-alertmanager statefulset.apps/prometheus-server -n stacklight
statefulset.apps/prometheus-alertmanager scaled
statefulset.apps/prometheus-server scaled
```

wait till all corresponding pods terminated, 
```bash
kubectl get po  -n stacklight | grep prometheus-alertmanager
kubectl get po  -n stacklight | grep prometheus-server
```

then:

Check PVC
```bash
get pvc -n stacklight
NAME                                          STATUS   VOLUME              CAPACITY   ACCESS MODES   STORAGECLASS                    AGE
elasticsearch-master-elasticsearch-master-0   Bound    local-pv-7458216d   2088Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-1   Bound    local-pv-ed8a2d3    2271Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-2   Bound    local-pv-3ef9a6e1   2088Gi     RWO            stacklight-elasticsearch-data   44d
storage-volume-patroni-12-0                   Bound    local-pv-b5b24c75   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-patroni-12-1                   Bound    local-pv-d1de366d   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-12-2                   Bound    local-pv-f4bfd9ac   439Gi      RWO            stacklight-postgresql-db        16d
storage-volume-patroni-13-0                   Bound    local-pv-600beef6   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-1                   Bound    local-pv-b54d122b   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-2                   Bound    local-pv-e28ad7e2   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-prometheus-alertmanager-0      Bound    local-pv-da5c6f63   439Gi      RWO            stacklight-alertmanager-data    44d
storage-volume-prometheus-alertmanager-1      Bound    local-pv-ae1fc0a9   2088Gi     RWO            stacklight-alertmanager-data    16d
storage-volume-prometheus-server-0            Bound    local-pv-576234be   2088Gi     RWO            stacklight-prometheus-data      44d
storage-volume-prometheus-server-1            Bound    local-pv-41e174e3   439Gi      RWO            stacklight-prometheus-data      44d
```

And delete  "prometheus-alertmanager-0" and "prometheus-server-1"
```bash
kubectl delete pvc storage-volume-prometheus-alertmanager-0 storage-volume-prometheus-server-1 -n stacklight
```
make sure that these PVCs deleted (kubectl get pvc -n stacklight), 

then:
```bash
kubectl scale --replicas=2 statefulset.apps/prometheus-alertmanager statefulset.apps/prometheus-server -n stacklight
```
after that wait some time and monitor the pods list, I expect the prometheus-server and the prometheus-alertmanager to be 2/2


## Postgreql - Patroni

```bash
kubectl get pvc -n stacklight
NAME                                          STATUS   VOLUME              CAPACITY   ACCESS MODES   STORAGECLASS                    AGE
elasticsearch-master-elasticsearch-master-0   Bound    local-pv-7458216d   2088Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-1   Bound    local-pv-ed8a2d3    2271Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-2   Bound    local-pv-3ef9a6e1   2088Gi     RWO            stacklight-elasticsearch-data   44d
storage-volume-patroni-12-0                   Bound    local-pv-b5b24c75   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-patroni-12-1                   Bound    local-pv-d1de366d   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-12-2                   Bound    local-pv-f4bfd9ac   439Gi      RWO            stacklight-postgresql-db        16d
storage-volume-patroni-13-0                   Bound    local-pv-600beef6   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-1                   Bound    local-pv-b54d122b   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-2                   Bound    local-pv-e28ad7e2   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-prometheus-alertmanager-0      Bound    local-pv-479571a7   2271Gi     RWO            stacklight-alertmanager-data    54m
storage-volume-prometheus-alertmanager-1      Bound    local-pv-ae1fc0a9   2088Gi     RWO            stacklight-alertmanager-data    16d
storage-volume-prometheus-server-0            Bound    local-pv-576234be   2088Gi     RWO            stacklight-prometheus-data      44d
storage-volume-prometheus-server-1            Bound    local-pv-322fb072   2088Gi     RWO            stacklight-prometheus-data      52m
```

```bash
kubectl delete pvc storage-volume-patroni-13-1 -n stacklight
persistentvolumeclaim "storage-volume-patroni-13-1" deleted
```

wait some time and make sure that these PVCs deleted
```bash
kubectl get pvc -n stacklight
NAME                                          STATUS   VOLUME              CAPACITY   ACCESS MODES   STORAGECLASS                    AGE
elasticsearch-master-elasticsearch-master-0   Bound    local-pv-7458216d   2088Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-1   Bound    local-pv-ed8a2d3    2271Gi     RWO            stacklight-elasticsearch-data   44d
elasticsearch-master-elasticsearch-master-2   Bound    local-pv-3ef9a6e1   2088Gi     RWO            stacklight-elasticsearch-data   44d
storage-volume-patroni-12-0                   Bound    local-pv-b5b24c75   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-patroni-12-1                   Bound    local-pv-d1de366d   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-0                   Bound    local-pv-600beef6   439Gi      RWO            stacklight-postgresql-db        44d
storage-volume-patroni-13-2                   Bound    local-pv-e28ad7e2   2088Gi     RWO            stacklight-postgresql-db        44d
storage-volume-prometheus-alertmanager-0      Bound    local-pv-479571a7   2271Gi     RWO            stacklight-alertmanager-data    55m
storage-volume-prometheus-alertmanager-1      Bound    local-pv-ae1fc0a9   2088Gi     RWO            stacklight-alertmanager-data    16d
storage-volume-prometheus-server-0            Bound    local-pv-576234be   2088Gi     RWO            stacklight-prometheus-data      44d
storage-volume-prometheus-server-1            Bound    local-pv-322fb072   2088Gi     RWO            stacklight-prometheus-data      53m
```

```bash
kubectl delete po patroni-13-1 -n stacklight
pod "patroni-13-1" deleted
```

```bash
$ kubectl get po -n stacklight -o wide | grep patroni
patroni-12-0                                     3/3     Running     0          41s     10.233.92.114    kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   <none>           <none>
patroni-12-1                                     3/3     Running     0          2m37s   10.233.76.201    kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   <none>           <none>
patroni-12-2                                     3/3     Running     0          48m     10.233.116.210   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   <none>           <none>
patroni-12-endpoint-cleanup-ztt74                0/1     Completed   0          2d2h    10.233.92.212    kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   <none>           <none>
patroni-13-0                                     3/3     Running     3          7d3h    10.233.92.247    kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   <none>           <none>
patroni-13-1                                     3/3     Running     0          48m     10.233.65.230    kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   <none>           <none>
patroni-13-2                                     3/3     Running     0          47m     10.233.92.90     kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   <none>           <none>
```

```bash
$ kubectl exec -it  stacklight-helm-controller-5c4f5f887c-s6nj4  -n stacklight -c tiller -- ./helm --host=localhost:44134 history patroni-13
REVISION	UPDATED                 	STATUS         	CHART                	APP VERSION	DESCRIPTION      
1       	Wed Sep 15 14:20:24 2021	SUPERSEDED     	patroni-0.15.1-mcp-24	12-1.6p2   	Install complete 
2       	Wed Oct 27 14:29:03 2021	SUPERSEDED     	patroni-0.15.1-mcp-24	12-1.6p2   	Upgrade complete 
3       	Wed Oct 27 14:44:42 2021	DEPLOYED       	patroni-0.15.1-mcp-24	12-1.6p2   	Upgrade complete 
4       	Wed Oct 27 15:04:24 2021	PENDING_UPGRADE	patroni-0.15.1-mcp-24	12-1.6p2   	Preparing upgrade
```
