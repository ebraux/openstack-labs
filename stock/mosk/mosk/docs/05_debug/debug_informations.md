

 for i in $(kubectl get lcmmachines -n br-mosk001 | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines -n br-mosk001 $i -o yaml | grep release | tail -1; done
br-mosk001-ceph-0
  release: 6.18.0+21.4
br-mosk001-ceph-1
  release: 6.18.0+21.4
br-mosk001-ceph-2
  release: 6.18.0+21.4
br-mosk001-cmp-0
  release: 6.18.0+21.4
br-mosk001-cmp-02
  release: 6.18.0+21.4
br-mosk001-cmp-05
  release: 6.18.0+21.4
br-mosk001-cmp-06
  release: 6.18.0+21.4
br-mosk001-cmp-07
  release: 6.18.0+21.4
br-mosk001-cmp-09
  release: 6.18.0+21.4
br-mosk001-cmp-14
  release: 6.18.0+21.4
br-mosk001-cmp-20
  release: 6.18.0+21.4
br-mosk001-master-0
  release: 6.18.0+21.4
br-mosk001-master-1
  release: 6.18.0+21.4
br-mosk001-master-2
  release: 6.18.0+21.4

---

for i in $(kubectl get lcmmachines | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines $i -o yaml | grep release | tail -1; done
master-0
  release: 7.2.0+3.4.5
master-1
  release: 7.2.0+3.4.5
master-2
  release: 7.2.0+3.4.5

---
kubectl -n openstack get openstackdeployment $(kubectl -n openstack get openstackdeployment | tail -1 | awk '{print $1}') -o yaml | grep openstack_version |tail -1
  openstack_version: ussuri
maintenant :  
  openstack_version: victoria


---
those ds might be leftovers in health metadata after you've played with node overrides
```bash
kubectl -n openstack get ds 
NAME                                       DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                    AGE
image-precaching-0                         13        13        13      13           13          <none>                           6h11m
libvirt-libvirt-default                    10        10        10      10           10          openstack-compute-node=enabled   63d
neutron-dhcp-agent-default                 3         3         3       3            3           openstack-gateway=enabled        63d
neutron-l3-agent-8046e21058b59d30          10        10        10      10           10          openstack-compute-node=enabled   63d
neutron-l3-agent-default                   3         3         3       3            3           openstack-gateway=enabled        63d
neutron-metadata-agent-default             3         3         3       3            3           openstack-gateway=enabled        63d
neutron-netns-cleanup-cron-default         13        13        13      13           13          openvswitch=enabled              63d
neutron-ovs-agent-default                  13        13        13      13           13          openvswitch=enabled              63d
nova-compute-default                       10        10        10      10           10          openstack-compute-node=enabled   63d
octavia-health-manager-default             3         3         3       3            3           <none>                           63d
openvswitch-openvswitch-db-default         13        13        13      13           13          openvswitch=enabled              63d
openvswitch-openvswitch-vswitchd-default   13        13        13      13           13          openvswitch=enabled              63d
```

```
$ kubectl -n openstack get osdpl -o yaml | less
```

```
$ kubectl -n openstack get osdplst -o yaml | less
```
In the status:health:nova you should see only computes that corresponds to daemonsets that you see with kubectl -n openstack get ds |grep nova.
As you can see in osdpl object you have only nova-compute-default which is ok as you don't have any nodes overrides applies for compute service according to https://miracloud.slack.com/archives/C02GQNKQ87K/p1637240185037100.
But due to bug in openstack-controller the halth section for osdplst  was not updated when daemonset removed. Even if you drop service accidentally from status, it will be automatically added by periodic task (default timeout is 60sec AFAIR).
BRAUX Emmanuel
kubectl -n openstack get ds |grep nova
nova-compute-default                       10        10        10      10           10          openstack-compute-node=enabled   63d
