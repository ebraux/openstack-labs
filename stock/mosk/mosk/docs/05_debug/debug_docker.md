

pb de chatgemen timage/kubelet


```bash
root@kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059:~# ls -lt /root/lcm-ansible-*/inventory
/root/lcm-ansible-v0.10.0-12-g7cd13b6/inventory:
total 36
-rw------- 1 root root 9817 Nov  3 19:16 inventory-054261863
-rw------- 1 root root 9515 Nov  3 17:12 inventory-369408679
drwxr-xr-x 2 root root 4096 Nov  3 17:11 group_vars
-rw-r--r-- 1 root root 1136 Nov  3 17:11 inventory-docker.tmpl
-rw-r--r-- 1 root root  750 Nov  3 17:11 inventory-k0s.tmpl

/root/lcm-ansible-v0.9.0-17-g28bc9ce/inventory:
total 32
-rw------- 1 root root 9777 Oct 29 18:44 inventory-047954865
-rw------- 1 root root 9504 Oct 29 18:03 inventory-380253466
drwxr-xr-x 2 root root 4096 Oct 29 18:03 group_vars
-rw-r--r-- 1 root root 1136 Oct 29 18:03 inventory-docker.tmpl

/root/lcm-ansible-v0.7.0-9-g30acaae/inventory:
total 68
-rw------- 1 root root 9814 Oct 22 13:36 inventory-424699907
-rw------- 1 root root 9358 Oct 20 16:35 inventory-346330164
-rw------- 1 root root 9158 Oct 12 16:04 inventory-773692905
-rw------- 1 root root 9466 Oct  8 08:59 inventory-775646770
-rw------- 1 root root 9158 Oct  8 08:56 inventory-595432031
-rw------- 1 root root    0 Oct  8 08:56 inventory-871753472
drwxr-xr-x 2 root root 4096 Oct  8 08:55 group_vars
-rw-r--r-- 1 root root 1136 Oct  8 08:55 inventory-docker.tmpl
```

``` bash
cd /root/lcm-ansible-v0.10.0-12-g7cd13b6
ansible-playbook -i inventory/inventory-369408679 download_ucp.yml --limit=$HOSTNAME
```