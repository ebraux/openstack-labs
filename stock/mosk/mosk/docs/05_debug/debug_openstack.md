

# configuration de Kubectcl pour MOSK

```bash
export KUBECONFIG="kubeconfig-br-mosk-01"
```

List des pods "openstack"
```bash
kubectl -n openstack get pods
kubectl -n openstack get pods -o wide
kubectl -n openstack get pods -o wide | grep kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea
```  

Affichage des logs 
```bash
kubectl  -n openstack logs nova-compute-default-4vcf4 nova-compute
```


Lancer une instance sur un noeud spécifique
```bash
openstack server create --image IMAGE_ID --flavor FLAVOR_NAME --key-name KEY --availability-zone nova:kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea --nic net-id=NET_UUID Test_Server_Oct1
```

openstack server create --image 4e3565bb-b5de-4aa4-911c-c41248798e6b --flavor 09da749a-e540-46c8-86fc-937aa9a6f034 --key-name  admin --availability-zone nova:kaas-node-9b49e515-67c3-45f6-9d2c-df63a2dd804e --nic net-id=82393655-ab44-423e-a79f-a709c6c80140 testeb   

kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c
9 compute are Waiting to be integrated to MOSK for the moment : 
 3xR620
 2xR610
 1xR630
 3xR640
9 more computes are used by the the old prod, and will be integrated.
6 more compute are used for calcule with



## dans Openstack

```bash
openstack compute service list

openstack compute service list --host  <cmp_host_name>

openstack network agent list
openstack network agent list --host <cmp_host_name>

```