
```bash
  - w
  - nproc
  - dmesg
  - top
  - iostat -xm 2
  - ps auxf
```

```bash
root@test-cmp-ceph:~# dd if=/dev/zero of=./1G bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 14.6895 s, 73.1 MB/s
```
