

Se connecter au container
```bash
kubectl -n kaas exec -it $(kubectl -n kaas get pods|grep dnsmasq| awk '{print $1}') -- bash
Defaulting container name to dhcpd.

Use 'kubectl describe pod/dnsmasq-7f8969d698-t59k7 -n kaas' to see all of the containers in this pod.
root@kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0:/# 
```


Si message tfp :
```bash

```

alor pb d'espace disqu.
```bash
root@kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0:/# df -h
Filesystem                 Size  Used Avail Use% Mounted on
overlay                    133G   67G   60G  54% /
tmpfs                       64M     0   64M   0% /dev
tmpfs                       32G     0   32G   0% /sys/fs/cgroup
/dev/rbd0                  4.9G  4.7G  189M  97% /volume
/dev/mapper/lvm_root-root  133G   67G   60G  54% /etc/hosts
shm                         64M     0   64M   0% /dev/shm
tmpfs                       32G   12K   32G   1% /run/secrets/kubernetes.io/serviceaccount
tmpfs                       32G     0   32G   0% /proc/acpi
tmpfs                       32G     0   32G   0% /proc/scsi
tmpfs                       32G     0   32G   0% /sys/firmware
```

ménage dans les logs

cat /dev/null > /volume/log/ironic/ironic-api.log   (no free space)
cat /dev/null > /volume/log/ambassador/access.log   (2,3G)
cat /dev/null > /volume/log/dnsmasq/dnsmasq-dhcpd.log  (822M)
cat /dev/null > /volume/log/dnsmasq/dnsmasq-tftpd.log  (1.2M)