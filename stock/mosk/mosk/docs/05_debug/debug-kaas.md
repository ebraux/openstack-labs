

Vérification des versions du cluster d emanagement
``` bash
for i in $(kubectl get lcmmachines | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines $i -o yaml | grep release | tail -1; done
master-0
  release: 7.2.0+3.4.5
master-1
  release: 7.2.0+3.4.5
master-2
  release: 7.2.0+3.4.5

```
```
for i in $(kubectl get lcmmachines -n br-mosk001 | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines -n br-mosk001 $i -o yaml | grep release | tail -1; done

```
```bash
kubectl get cluster
NAME        AGE
kaas-mgmt   127d
```

```bash
 kubectl cluster-info
Kubernetes control plane is running at https://10.129.172.90:443
CoreDNS is running at https://10.129.172.90:443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```




```bash

1923  cd kaas
 1924  ls
 1925  cd ..
 1926  ls
 1927  cd kaas-bootstrap/
 1928  ls
 1929  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --cluster-name ucp_10.129.172.90:5443_admin --cluster-namespace mcc_br-mosk001
 1930  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key 
 1931  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --cluster-name default
 1932  ./kaas 
 1933  ./kaas cluster 
 1934  ./kaas cluster healthcheck
 1935  ./kaas cluster healthcheck --management-kubeconfig kubeconfig
 1936  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --cluster-name default
 1937  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --help
 1938  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --cluster-name ucp_10.129.172.90:5443_admin
 1939  less kubeconfig
 1940  ./kaas collect logs --management-kubeconfig kubeconfig --key-file ssh_key --cluster-name ucp_10.129.172.90\:5443_admin
 1941  kubectl cluster get
 1942  kubectl cluster-info
 1943  kubectl cluster-info dump
 1944  export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig
 1945  kubectl cluster-info
 1946  kubectl cluster-info dump
 1947  kubectl cluster-info dump | less
 1948  kubectl cluster get
 1949  kubectl cluster 
 1950  kubectl get cluster
```

# getsion du cluster

```bash
kmosk -n kaas get pods

kmosk -n kaas logs lcm-lcm-controller-5c44d8dc8b-7mjtc 
kmosk -n kaas logs lcm-lcm-controller-5c44d8dc8b-7mjtc | grep  ClusterMaintenanceReques

kubectl edit deployment -n kaas lcm-lcm-controller

kmosk get lcmclusterstate -o wide
kmosk edit lcmclusterstate swarm-drain-kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c9rfqw

```
remplacement de la licence
```bash
1889  ls
 1890  cd MCC2.9
 1891  \ls
 1892  ls
 1893  cd kaas-bootstrap/
 1894  ls
 1895  mv mirantis.lic mirantis.lic.old
 1896  vi mirantis.lic
 1897  ls
 1898  export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig
 1899  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic)\"}}"
 1900  vi mirantis.lic
 1901  tr -d '\n' mirantis.lic
 1902  tr -d '\n' < mirantis.lic
 1903  tr -d '\n' < mirantis.lic >mirantis.lic2
 1904  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1905  kubectl -n kaas patch secret license -p '{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}'
 1906  kubectl -n kaas patch secret license -p "{"data\":{"license":"$(base64 < mirantis.lic2)"}}"
 1907  "
 1908  base64 < mirantis.lic2
 1909  base64 < mirantis.lic2 | tr -d '\n'
 1910  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1911  abash kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1912  bash kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
```



# 
```bash
ssh  -o "IdentitiesOnly=yes"  -i ~/MCC2.9/kaas-bootstrap/ssh_key mcc-user@10.129.172.103
sudo su -
1  journalctl -u lcm-agent
    2  journalctl -u *
    3  journalctl -u lcm-agent*
    4  journalctl -u lcm-agent* | grep "new ansible run"
    5  journalctl -u lcm-agent* | grep -i "ansible"
    6  journalctl -u lcm-agent* | grep  "Ansible"
    7  journalctl -u lcm-agent* | grep  "New ansible run"
    8  diff /root/lcm-ansible-v0.7.0-9-g30acaae/inventory/inventory-367785915 /root/lcm-ansible-v0.7.0-9-g30acaae/inventory/inventory-140244389
```


