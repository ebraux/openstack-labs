

swarm tourne sur chaque worker node

pour interragir.
 - se connecter au node
 - utiliser la commande `ctr`en tant que root

Récupérer l'ip des worker : dans un environnement cluster de management, tapper
```bash

kubectl -n br-mosk01 get lcmmachine -o wide
NAME                  CLUSTERNAME       TYPE      STATE   INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001-ceph-0     kaas-br-mosk001   worker    Ready   10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.3.0-32-gee08c2b8
br-mosk001-ceph-1     kaas-br-mosk001   worker    Ready   10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.3.0-32-gee08c2b8
br-mosk001-ceph-2     kaas-br-mosk001   worker    Ready   10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.3.0-32-gee08c2b8
br-mosk001-cmp-0      kaas-br-mosk001   worker    Ready   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   v0.3.0-32-gee08c2b8
br-mosk001-cmp-02     kaas-br-mosk001   worker    Ready   10.129.172.118   kaas-node-a7f2d0f2-411b-423d-b4f8-af604768a55b   v0.3.0-32-gee08c2b8
br-mosk001-cmp-05     kaas-br-mosk001   worker    Ready   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   v0.3.0-32-gee08c2b8
br-mosk001-cmp-06     kaas-br-mosk001   worker    Ready   10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   v0.3.0-32-gee08c2b8
br-mosk001-cmp-07     kaas-br-mosk001   worker    Ready   10.129.172.117   kaas-node-38538401-efd4-4ddc-a328-7666724b917f   v0.3.0-32-gee08c2b8
br-mosk001-cmp-09     kaas-br-mosk001   worker    Ready   10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   v0.3.0-32-gee08c2b8
br-mosk001-cmp-14     kaas-br-mosk001   worker    Ready   10.129.172.111   kaas-node-d2144611-3110-417f-8d20-ac38b6ddbdbf   v0.3.0-32-gee08c2b8
br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready   10.129.172.112   kaas-node-d29a52d3-5bed-4027-992a-5f94b1089163   v0.3.0-32-gee08c2b8
br-mosk001-master-0   kaas-br-mosk001   control   Ready   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.3.0-32-gee08c2b8
br-mosk001-master-1   kaas-br-mosk001   control   Ready   10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.3.0-32-gee08c2b8
br-mosk001-master-2   kaas-br-mosk001   control   Ready   10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.3.0-32-gee08c2b8
```

Se connecter à la machine
```bash
root@mcc-seed-01:~# ssh  -o "IdentitiesOnly=yes"  -i ~/MCC2.9/kaas-bootstrap/ssh_key mcc-user@10.129.172.113
```

Passer root
```bash
sudo su -
```

Lancer les commande `ctr` et `docker`. Par exemple

```bash
ctr -n com.docker.ucp snapshot rm ucp-kubelet
docker rm -f ucp-kubelet
ctr --help
ctr c ls
```
