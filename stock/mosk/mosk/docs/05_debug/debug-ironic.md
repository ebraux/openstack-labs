# Interraction directe avec Ironic


le pod :
- ironic-operator-xxxx : gère le déplimeent d'Ironic
- ironic-xxxx : reçoit les requetes des API (les client sur les poste à inspecter)
- dnsmaq-xxx : utilisé pour donner des IP aux IPA
- baremetal-provider ???
- baremetal operator ???
- 
récupérer l'IP d'ironic
```bash
/usr/local/bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}
```

Déploiment d'Ironic
```bash
kubectl -n kaas exec -ti deployment/ironic -c ironic-api bash
kubectl api-resources --verbs=list --namespaced -o name | paste -sd, - | xargs -n 1 kubectl get --show-kind --ignore-not-found  -n br-mosk001
kubectl -n kaas exec -ti deployment/ironic -c ironic-api bash

```

## Test de l'API

Test de réponse de l'API
```bash
curl -k https://ironic-kaas-bm:6385
{"name": "OpenStack Ironic API", "description": "Ironic is an OpenStack project which aims to provision baremetal machines.", "default_version": {"id": "v1", "links": [{"href": "https://127.0.0.1:46385//v1/", "rel": "self"}], "status": "CURRENT", "min_version": "1.1", "version": "1.68"}, "versions": [{"id": "v1", "links": [{"href": "https://127.0.0.1:46385//v1/", "rel": "self"}], "status": "CURRENT", "min_version": "1.1", "version": "1.68"}]}
```

Utiliser l'API : [https://docs.openstack.org/api-ref/baremetal/](https://docs.openstack.org/api-ref/baremetal/)

Afficher les BHHosts
```bash
kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
curl -k https://10.129.172.64:6385

curl -k https://10.129.172.64:6385/v1/nodes  | python -m json.tool
curl -k https://10.129.172.64:6385/v1/nodes/detail  | python -m json.tool
curl -k https://10.129.172.64:6385/v1/nodes/7ee85f27-32da-4419-bf89-51012c357811 | python -m json.tool

```

Afficher les ports
```bash
curl -k https://10.129.172.64:6385/v1/nodes/7ee85f27-32da-4419-bf89-51012c357811/ports | python -m json.tool
curl -k https://10.129.172.64:6385/v1/ports  | python -m json.tool
```

## Tets de ???

```bash
curl -v 10.129.172.62:80
```

##  Afficher les logs

```bash
kubectl -n kaas logs  -l app.kubernetes.io/name=baremetal-provider --tail 5000
```

```bash
kubectl -n kaas logs deployment/ironic -c syslog | jq -rM 'select(.source == "ironic_conductor" and .program != "ironic-conductor") | .message'
```

```bash
kubectl -n kaas get pods | grep ironic
ironic-56676c774b-bfcjs                                  6/6     Running   0          14d
ironic-operator-7b66b978d4-4v4qr                         1/1     Running   0          14d

kubectl logs ironic-56676c774b-bfcjs  -n kaas  ironic-inspector  |grep d71cad45-e3a2-422e-9a98-6496189bf583
kubectl logs ironic-56676c774b-bfcjs  -n kaas  ironic-conductor   

# on peut faire un grep avec un id de ???
kubectl logs ironic-7d4576755c-889bs  -n kaas  ironic-inspector  |grep d71cad45-e3a2-422e-9a98-6496189bf583
kubectl logs ironic-7d4576755c-889bs  -n kaas  ironic-conductor   |grep d71cad45-e3a2-422e-9a98-6496189bf583
```

## Commandes Ironic

```bash
. ~/venv_os/bin/activate
export OS_URL=http://$(/usr/local/bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):6385/
#export INSPECTOR_URL=http://$(/usr/local/bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):5050/echo $OS_URL
export OS_TOKEN=fake-token
export OS_ENDPOINT=${OS_URL}
openstack --insecure baremetal node list
---
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name                           | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| 48ec3e60-fb9b-4e7e-99a5-68a5c4469bed | master-1                       | 47b3dccd-f540-4f4e-bb04-d170c59ac948 | power on    | active             | False       |
| 76a2c8f2-1fee-49ba-9454-ec90faf993a4 | master-0                       | 9c081499-eed8-4cf7-aa9b-292e25b917c7 | power on    | active             | False       |
| 65628469-8845-4098-9d11-a56d5f363610 | master-2                       | 38b24c3b-eb62-4034-8507-9136b6e1e1ff | power on    | active             | False       |
| e28f47b9-3eb4-42e6-9da9-351d94021503 | br-mosk001~br-mosk001-master-0 | 02a1b0ca-0519-478a-864b-8c3f83d1b6b9 | power on    | active             | False       |
| 765f30ea-d859-4f6c-a397-dcccf7782a58 | br-mosk001~br-mosk001-master-1 | 12c12bd4-a2bf-47de-88c0-0ba66c6b10fd | power on    | active             | False       |
| be39e92f-cec5-4e10-af6e-6bbbc6075b9d | br-mosk001~br-mosk001-master-2 | e9cf7c22-f323-4fa1-9a84-0ed053e44b71 | power on    | active             | False       |
| 13d9f60a-f2cc-4d53-b9b9-bd4767e8c318 | br-mosk001~br-mosk001-ceph-0   | 270911cc-4af7-4bd3-a291-c4a94377c2aa | power on    | active             | False       |
| ce8f1b0e-11fb-4ca4-bcfe-d4b0fe57c8d4 | br-mosk001~br-mosk001-ceph-1   | 8898d06f-8758-497e-92bf-055e8d4170d9 | power on    | active             | False       |
| 23becd07-a95e-4bae-97f0-f6d3d13b4dc2 | br-mosk001~br-mosk001-ceph-2   | 1cb2c52a-5853-46b7-b9a2-d06ff065cbb6 | power on    | active             | False       |
| f18ae5f6-f849-4ed3-899f-86da2e27a292 | br-mosk001~br-mosk001-cmp-0    | 744e435a-d3b8-4d73-a90f-a3ef3c5f6d06 | power on    | active             | False       |
| 13dc8a9a-9bf0-4907-ab34-f6aaea0fbff1 | br-mosk001~br-mosk001-cmp-1    | 27e8080d-2ce8-4dd2-90da-e04e9d0f9f75 | power on    | active             | False       |
| d7c3c2c5-2dda-48c3-be02-dab6740c15c1 | br-mosk001~br-mosk001-cmp-2    | 230ecd85-a1f8-4254-a004-e88292a2d19a | power on    | active             | False       |
| 7ee85f27-32da-4419-bf89-51012c357811 | br-mosk001~br-mosk001-cmp-3    | 1f5686c4-5a17-4630-8999-fdf53e01630b | power on    | active             | False       |
| 8abb6fd0-20e2-4b41-9550-dd2f8a421bb3 | br-mosk001~br-mosk001-cmp-4    | None                                 | power on    | available          | False       |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
```

```bash
openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node list
openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node maintenance set br-mosk001~br-mosk001-master-0
openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node node delete br-mosk001~br-mosk001-master-0
```

## Autres

```bash
 1578  cat 1.sh 
 1579  curl -X PUT --upload-file somefile.txt http://172.18.194.16:8000
 1580  curl -X PUT --upload-file 1.sh http://37.57.27.211:8000
 1581  cat ironic_rc_mgmt_mtls 
 1582  export HTTP_PROXY="http://proxy.enst-bretagne.fr:8080"                                                                                                                                                  │··········
 1583  export HTTP_PROXY="http://proxy.enst-bretagne.fr:8080"
 1584  export HTTPS_PROXY="http://proxy.enst-bretagne.fr:8080"
 1585  curl -X PUT --upload-file 1.sh http://37.57.27.211:8000
 1586  readlink  -e 1.sh 
 1587  less /home/mcc-user/MCC2.9/kaas-bootstrap/alexz/1.sh
```



# Commandes de connexion Mirantis

```bash
export KUBECONFIG=management_kubeconfig

kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.ca}' | base64 -d | tee tls.ca
kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.crt}' | base64 -d | tee tls.crt
kubectl -n kaas get secrets bm-mtls-client -o jsonpath='{.data.tls\.key}' | base64 -d | tee tls.key

export OS_URL=http://$(kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):6385/
export IRONIC_IP=$(kubectl --kubeconfig ${KUBECONFIG} get svc ironic-kaas-bm -n kaas -o json | jq -r .status.loadBalancer.ingress[0].ip)


docker run --entrypoint=/bin/bash -e IRONIC_IP=$IRONIC_IP -e OS_URL=$OS_URL -v $(readlink -e .)/:/test1/:ro --hostname=docker-quick -ti ubuntu:focal
# everything below - inside container
# proxy IMT atlantique
echo 'Acquire::http::Proxy "http://apt-cacher-01.priv.enst-bretagne.fr:3142";' > /etc/apt/apt.conf.d/01proxy
export https_proxy=proxy.enst-bretagne.fr:8080
export http_proxy=proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,ironic-kaas-bm,${OS_URL}

apt-get update && apt-get install -y python3-pip
pip3 install python-ironic-inspector-client python-ironicclient python-openstackclient setuptools-rust
export INSPECTOR_URL=https://ironic-kaas-bm:5050/
export OS_URL=https://ironic-kaas-bm:6385/
export OS_TOKEN=fake-token
export OS_ENDPOINT=${OS_URL}


echo -e "${IRONIC_IP} ironic-kaas-bm\n" | tee -a /etc/hosts
cd /test1
openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node list
```

Ok, let's go 
```
baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca node list
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| UUID                                 | Name                           | Instance UUID                        | Power State | Provisioning State | Maintenance |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
| 76a2c8f2-1fee-49ba-9454-ec90faf993a4 | master-0                       | 9c081499-eed8-4cf7-aa9b-292e25b917c7 | power on    | active             | False       |
| 48ec3e60-fb9b-4e7e-99a5-68a5c4469bed | master-1                       | 47b3dccd-f540-4f4e-bb04-d170c59ac948 | power on    | active             | False       |
| 65628469-8845-4098-9d11-a56d5f363610 | master-2                       | 38b24c3b-eb62-4034-8507-9136b6e1e1ff | power on    | active             | False       |

| e28f47b9-3eb4-42e6-9da9-351d94021503 | br-mosk001~br-mosk001-master-0 | 02a1b0ca-0519-478a-864b-8c3f83d1b6b9 | power on    | active             | False       |
| 765f30ea-d859-4f6c-a397-dcccf7782a58 | br-mosk001~br-mosk001-master-1 | 12c12bd4-a2bf-47de-88c0-0ba66c6b10fd | power on    | active             | False       |
| be39e92f-cec5-4e10-af6e-6bbbc6075b9d | br-mosk001~br-mosk001-master-2 | e9cf7c22-f323-4fa1-9a84-0ed053e44b71 | power on    | active             | False       |

| 13d9f60a-f2cc-4d53-b9b9-bd4767e8c318 | br-mosk001~br-mosk001-ceph-0   | 270911cc-4af7-4bd3-a291-c4a94377c2aa | power on    | active             | False       |
| ce8f1b0e-11fb-4ca4-bcfe-d4b0fe57c8d4 | br-mosk001~br-mosk001-ceph-1   | 8898d06f-8758-497e-92bf-055e8d4170d9 | power on    | active             | False       |
| 23becd07-a95e-4bae-97f0-f6d3d13b4dc2 | br-mosk001~br-mosk001-ceph-2   | 1cb2c52a-5853-46b7-b9a2-d06ff065cbb6 | power on    | active             | False       |

| f18ae5f6-f849-4ed3-899f-86da2e27a292 | br-mosk001~br-mosk001-cmp-0    | 744e435a-d3b8-4d73-a90f-a3ef3c5f6d06 | power on    | active             | False       |
| 99dee5cc-ef7d-4b09-887a-77ef4a829c07 | br-mosk001~br-mosk001-cmp-05   | e7ee8051-577e-4ddd-b215-1a1e850eb487 | power on    | active             | False       |
| ed873dc5-ba91-41a4-be70-aff576474746 | br-mosk001~br-mosk001-cmp-06   | 3432b2c2-6539-4194-944a-093becf90691 | power on    | active             | False       |
| b4a63a97-1eb6-4b63-8bef-3711ee53d6d7 | br-mosk001~br-mosk001-cmp-07   | 55375a0d-6627-4c2f-942f-230b8a1b7465 | power on    | active             | False       |
| 69349686-65a5-4286-8554-858e9cc0e8ec | br-mosk001~br-mosk001-cmp-09   | 77d905a3-8322-44c9-91c4-4ffcd759098f | power on    | active             | False       |
| dce9ea7a-7aaf-4161-934c-a0faf7242da1 | br-mosk001~br-mosk001-cmp-14   | 597e762c-30da-462a-ac09-0a64b5636f27 | power on    | active             | False       |
| 09be21a4-4eee-4b09-88a7-c706fc2fc636 | br-mosk001~br-mosk001-cmp-20   | 8dd74d3a-227d-4128-ad9b-ae815d4e0c06 | power on    | active             | False       |


| 13dc8a9a-9bf0-4907-ab34-f6aaea0fbff1 | br-mosk001~br-mosk001-cmp-1    | 27e8080d-2ce8-4dd2-90da-e04e9d0f9f75 | power off   | available          | False       |
| d7c3c2c5-2dda-48c3-be02-dab6740c15c1 | br-mosk001~br-mosk001-cmp-2    | 230ecd85-a1f8-4254-a004-e88292a2d19a | power on    | active             | False       |
| 7ee85f27-32da-4419-bf89-51012c357811 | br-mosk001~br-mosk001-cmp-3    | None                                 | power off   | clean failed       | False       |
| e69f379d-f265-4d97-a5f4-75ab38495b2d | br-mosk001~br-mosk001-cmp-12   | None                                 | power on    | inspect failed     | False       |
+--------------------------------------+--------------------------------+--------------------------------------+-------------+--------------------+-------------+
```

```bash
 openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca port list


Ok, I have checked the ports
Theses are good :
```
+--------------------------------------+-------------------+
| UUID                                 | Address           |
+--------------------------------------+-------------------+
| a1532ad4-6f92-477c-8bdb-07d9038d6500 | b8:2a:72:d2:e5:9c | master-0
| b4c426c1-133f-45f5-bbe2-c8c67d116667 | c8:1f:66:ce:5b:98 | master-1
| 9aaf74dd-3b92-4ab7-9dbd-d1affcdd3f6f | b8:2a:72:d2:f0:cd | master-2
| 7c336fae-724a-4dac-af1f-42403b19881e | e4:43:4b:ea:b2:b0 | br-mosk001~br-mosk001-master 0
| b05b3fb2-9f89-4083-844f-73fdb5b3089a | e4:43:4b:ea:67:a0 | br-mosk001~br-mosk001-master 1
| cbad8220-e7c3-4e67-84b6-20a2431b10fd | e4:43:4b:ea:67:50 | br-mosk001~br-mosk001-master 2
| ddaa6ea3-3fbb-484c-9dc2-9ae72c785f57 | e4:43:4b:ea:67:70 | br-mosk001~br-mosk001-ceph 0
| b8710c57-be4a-4606-9ee4-f2ea72e8b8c1 | e4:43:4b:ea:be:80 | br-mosk001~br-mosk001-ceph 1
| 76baf526-a7a2-4516-8460-429b8523a469 | e4:43:4b:ea:c2:b0 | br-mosk001~br-mosk001-ceph 2
| 4e6e8805-fbf1-4e90-a1fc-313e4c4fdc6a | e4:43:4b:ea:73:b0 | br-mosk001~br-mosk001-br-mosk001-cmp-0
| f19fa5ed-a633-40eb-9582-a69b16938b32 | 78:ac:44:08:c0:10 | br-mosk001~br-mosk001-br-mosk001-cmp-05
| 86476db6-083e-44ac-9a11-10e80c7e37e8 | 78:ac:44:08:c0:18 | br-mosk001~br-mosk001-br-mosk001-cmp-06
| b4535746-e08f-4a79-87f4-3d6aa08b7a2e | 18:66:da:ec:4b:3c | br-mosk001~br-mosk001-br-mosk001-cmp-07
| 91f0a327-e7b7-438b-bccb-50e6b3b8f8bd | e4:43:4b:83:10:2c | br-mosk001~br-mosk001-br-mosk001-cmp-09
| bbcf04a9-0062-4f65-b78d-e861fa4a04a0 | 18:66:da:ed:2c:3c | br-mosk001~br-mosk001-br-mosk001-cmp-14
| e449502d-b453-4a54-b773-9ac82d4164e9 | e4:43:4b:e9:37:58 |  br-mosk001~br-mosk001-br-mosk001-cmp-20
```

These are not used anymore
```
+--------------------------------------+-------------------+
| UUID                                 | Address           |
+--------------------------------------+-------------------+
| f77904d2-0a70-4fed-bda6-409cda17b2fb | e4:43:4b:ea:bd:c0 | br-mosk001-cmp-02
| d7358f7c-8df4-4fa5-8523-b0b0fd2a1e27 | d4:be:d9:af:c2:cd | br-mosk001-cmp-03
| e27183b0-c78c-4550-bc87-8ea0fd463248 | d4:be:d9:af:c3:9c | br-mosk001-cmp-04
| 7f90a73c-44dd-4de1-a1f1-7277497b47ae | c8:1f:66:bd:eb:49 | br-mosk001-cmp-10
```

+--------------------------------------+-------------------+
```


To delete a port
```bash
openstack baremetal --os-cert tls.crt --os-key tls.key --os-cacert tls.ca port delete 7f90a73c-44dd-4de1-a1f1-7277497b47ae
```
