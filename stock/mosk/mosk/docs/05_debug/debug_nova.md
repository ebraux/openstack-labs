


# pod "compute" sur un node

Nova est géré sous forme de pods sur les computes:
- image-precaching
- libvirt-libvirt
- neutron-l3-agent
- neutron-netns-cleanup-cron
- neutron-ovs-agent
- nova-compute
- openvswitch-openvswitch-db
- openvswitch-openvswitch-vswitchd

``` bash
 kubectl -n openstack get pods -o wide |grep kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826
image-precaching-0-qpdz8                                       30/30   Running     0          17h     10.233.125.3     kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
libvirt-libvirt-default-gjrqt                                  2/2     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
neutron-l3-agent-8046e21058b59d30-9ptm6                        1/1     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
neutron-netns-cleanup-cron-default-kgrjh                       1/1     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
neutron-ovs-agent-default-pnrf2                                1/1     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
nova-compute-53e07fd295d6b15d-v2pgj                            2/2     Running     0          7h50m   10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
openvswitch-openvswitch-db-default-42c5d                       1/1     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
openvswitch-openvswitch-vswitchd-default-rs7rk                 1/1     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>

```


Pour afficher les pods "nova-compute" : 

``` bash
kubectl -n openstack get pods -o wide |grep  nova-compute
nova-compute-53e07fd295d6b15d-v2pgj                            2/2     Running     0          7h41m
nova-compute-default-944hd                                     2/2     Running     0          17h
nova-compute-default-b964z                                     2/2     Running     0          17h
nova-compute-default-frhr5                                     2/2     Running     0          17h
nova-compute-default-jx9g9                                     2/2     Running     0          17h
nova-compute-default-k4lnm                                     2/2     Running     0          17h
nova-compute-default-lr6qj                                     2/2     Running     0          17h
nova-compute-default-pffn7                                     2/2     Running     0          17h
nova-compute-default-rfg74                                     2/2     Running     0          17h
nova-compute-default-v2rm2                                     2/2     Running     0          17h
```

Pour afficher les pods libvirt :
``` bash
kubectl -n openstack get pods -o wide |grep libvirt
libvirt-libvirt-default-2f9gn  2/2   Running  0  3d2h  10.129.172.119  kaas-node-2a6424c9-9504-4639-b93d-119c85a5620c   <none>           <none>
libvirt-libvirt-default-5svlk  2/2  Running   0  5d21h   10.129.172.109   kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c   <none>           <none>
libvirt-libvirt-default-fbk5b                                  2/2     Running     0          3d      10.129.172.112   kaas-node-40989eff-5bfa-4fd6-8441-4e0818e358b0   <none>           <none>
libvirt-libvirt-default-fp22s                                  2/2     Running     0          5d21h   10.129.172.115   kaas-node-d646e156-80a1-4c0c-a102-11669fd37cea   <none>           <none>
libvirt-libvirt-default-gjrqt                                  2/2     Running     0          17h     10.129.172.118   kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826   <none>           <none>
libvirt-libvirt-default-gv9lf                                  2/2     Running     0          2d23h   10.129.172.111   kaas-node-adef27fc-1824-4c85-8428-007a6086757f   <none>           <none>
libvirt-libvirt-default-lqxv4                                  2/2     Running     0          5d21h   10.129.172.117   kaas-node-38538401-efd4-4ddc-a328-7666724b917f   <none>           <none>
libvirt-libvirt-default-slg6j                                  2/2     Running     0          5d21h   10.129.172.116   kaas-node-17ef5d4d-f896-4f84-bc84-f02e63ed76cd   <none>           <none>
libvirt-libvirt-default-wq8gg                                  2/2     Running     0          5d1h    10.129.172.113   kaas-node-719fafd5-4d68-4c88-8c0e-8364cba48059   <none>           <none>
libvirt-libvirt-default-z6mnm                                  2/2     Running     0          3d1h    10.129.172.110   kaas-node-15e11438-4629-405f-8332-13d7dad435f0   <none>           <none>



Lié à un compute , par exemple sur le compute `kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826`

``` bash
kubectl -n openstack get pods |grep 53e07fd295d6b15d
nova-compute-53e07fd295d6b15d-v2pgj                            2/2     Running     0          7h26m
```

Afficher la config de nova :
``` bash
kubectl -n opentack exec -it <pod name from previous command>  cat /etc/nova/nova.conf
kubectl -n openstack exec -it nova-compute-53e07fd295d6b15d-v2pgj cat /etc/nova/nova.conf

```

# vérifier les instances sur un compute

avec la commande Openstack, identifier les instances
```bash
openstack server list --all --host kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826
+--------------------------------------+------------+--------+------------------------+--------------+------------+
| ID                                   | Name       | Status | Networks               | Image        | Flavor     |
+--------------------------------------+------------+--------+------------------------+--------------+------------+
| 1091a34f-0bc6-4757-8f43-5b8c34990083 | testnewone | ACTIVE | myNetwork=172.16.1.152 | ubuntu-20.04 | m1.small   |
| 56ee16d5-3584-453f-8d95-123fec832885 | testlasone | ACTIVE | myNetwork=172.16.1.209 | ubuntu-20.04 | s20.xlarge |
+--------------------------------------+------------+--------+------------------------+--------------+------------+
```

Vérification directement sur le compute, via la commande lvs

```bash
ssh mcc-user@10.129.172.118

root@kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:~# lvs
  LV                                        VG       Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lvp                                       lvm_lvp  -wi-ao---- <35.00g                                                    
  root                                      lvm_root -wi-ao---- <55.00g                                                    
  1091a34f-0bc6-4757-8f43-5b8c34990083_disk nova-vol -wi-ao----   3.00g                                                    
  56ee16d5-3584-453f-8d95-123fec832885_disk nova-vol -wi-ao----  20.00g                                                    
  nova_fake                                 nova-vol -wi-ao----   1.00g   
```

Affichier la config de l'instance 

kubectl -n openstack exec -it <pod name from previous command> — cat /etc/nova/nova.conf
kubectl -n openstack exec -it libvirt-libvirt-default-gjrqt virsh dumpxml 56ee16d5-3584-453f-8d95-123fec832885

Vérification sur le pod lié au compute


Ensuite, sur le compute
Nova creates volumes (instance drives) on lvm directly, you can check if instances are using lvm by running virsh dumpxml <instance-name> from libvirt pod, or by running lvs on compute node


# personnaliser nova pour un node

Dans la doc 
```bash
  nodes:
    kubernetes.io/hostname::kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:
      features:
        nova:
          images:
            backend: lvm
            lvm:
              volume_group: nova-vol
          live_migration_interface: br-tenant
```

Mais dans la config, pour que ça fonctionne réélement ( this is a temporary workaround to solve your issue, the proper fix will be delivered in the next release 22.1 )
```bash
  nodes:
    kubernetes.io/hostname::kaas-node-ddf0f96e-8138-4af3-b44e-53f1b8135826:
      services:
        compute:
          nova:
            nova_compute:
              values:
                conf:
                  nova:
                    libvirt:
                      images_type: lvm
                      images_volume_group: nova-vol
```

---
## Autres

```bash
kubectl get nodes <lvm compute hostname> --show-labels
```

```bash
kubectl -n openstack get daemonsets |grep nova
nova-compute-53e07fd295d6b15d              1         1         1       1            1           openstack-compute-node=enabled   7h15m
nova-compute-default                       9         9         9       9            9           openstack-compute-node=enabled   54d

```


---
nodes:
    nova-compute-images-backend::lvm:
      services:
        ...
8 h 49
the syntax is: <label-name>::<label-value>




-----------
## bas image

remove_unused_base_images = True
	(Boolean) Should unused base images be removed?

image_cache_subdirectory_name = _base	
(String) Location of cached images.
This is NOT the full path - just a folder name relative to ‘$instances_path’. For per-compute-host cached images, set to ‘_base_$my_ip’

remove_unused_resized_minimum_age_seconds = 3600
	(Integer) Unused resized base images younger than this will not be removed