

```bash
kubectl get events -n br-mosk001 
```


## Licence

```bash
1895  mv mirantis.lic mirantis.lic.old
 1896  vi mirantis.lic
 1897  ls
 1898  export KUBECONFIG=~/MCC2.9/kaas-bootstrap/kubeconfig
 1899  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic)\"}}"
 1900  vi mirantis.lic
 1901  tr -d '\n' mirantis.lic
 1902  tr -d '\n' < mirantis.lic
 1903  tr -d '\n' < mirantis.lic >mirantis.lic2
 1904  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1905  kubectl -n kaas patch secret license -p '{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}'
 1906  kubectl -n kaas patch secret license -p "{"data\":{"license":"$(base64 < mirantis.lic2)"}}"
 1907  "
 1908  base64 < mirantis.lic2

 1909  base64 < mirantis.lic2 | tr -d '\n'
 1910  kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1911  abash kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 1912  bash kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic2)\"}}"
 ```

 kubectl -n kaas patch secret license -p "{\"data\":{\"license\":\"$(base64 < mirantis.lic3| tr -d '\n')\"}}"


 ## check versions
 for i in $(kubectl get lcmmachines -n br-mosk001 | awk '{print $1}' | sed '1d'); do echo $i; kubectl get lcmmachines -n br-mosk001 $i -o yaml | grep release | tail -1; done
br-mosk001-ceph-0
  release: 6.16.0+21.3
br-mosk001-ceph-1
  release: 6.16.0+21.3
br-mosk001-ceph-2
  release: 6.16.0+21.3
br-mosk001-cmp-0
  release: 6.16.0+21.3
br-mosk001-cmp-05
  release: 6.16.0+21.3
br-mosk001-cmp-06
  release: 6.16.0+21.3
br-mosk001-cmp-07
  release: 6.16.0+21.3
br-mosk001-cmp-08
  release: 6.16.0+21.3
br-mosk001-cmp-09
  release: 6.16.0+21.3
br-mosk001-cmp-1
  release: 6.16.0+21.3
br-mosk001-cmp-2
  release: 6.16.0+21.3
br-mosk001-cmp-20
  release: 6.16.0+21.3
br-mosk001-master-0
  release: 6.16.0+21.3
br-mosk001-master-1
  release: 6.16.0+21.3
br-mosk001-master-2
  release: 6.16.0+21.3
