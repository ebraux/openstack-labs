# Suppression d'un Compute

Opération en plusieurs étapes :
- suppression au niveau d'openstack d'Openstack
- suppression au niveau du cluster

Docs :
- [https://docs.mirantis.com/mos/latest/ops/openstack-operations/delete-compute.html](https://docs.mirantis.com/mos/latest/ops/openstack-operations/delete-compute.html)
- [https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/delete-machine.html](https://docs.mirantis.com/container-cloud/latest/operations-guide/operate-managed/delete-machine.html)


La suppression d'un compute Node comporte 2 phase :

 - la suppression du compute dans Openstack
 - la suppression de la machine dans le cluster MOSK


```bash
. ~/venv_os/bin/activate
export OS_URL=http://$(/usr/local/bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):6385/
#export INSPECTOR_URL=http://$(/usr/local/bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):5050/echo $OS_URL
export OS_TOKEN=fake-token
export OS_ENDPOINT=${OS_URL}
openstack --insecure baremetal node list


. ~/venv_os/bin/activate
 export OS_URL=http://$(./bin/kubectl -n kaas get svc ironic-kaas-bm -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):6385/
```

openstack compute service

identififier le compute à supprimer :
```bash
openstack compute service list
---
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
| ID | Binary         | Host                                           | Zone     | Status   | State | Updated At                 |
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
| 19 | nova-conductor | nova-conductor-5db55b5645-srmv6                | internal | enabled  | up    | 2021-10-08T07:25:31.000000 |
| 22 | nova-conductor | nova-conductor-5db55b5645-44v6m                | internal | enabled  | up    | 2021-10-08T07:25:18.000000 |
| 25 | nova-conductor | nova-conductor-5db55b5645-bnpsv                | internal | enabled  | up    | 2021-10-08T07:25:25.000000 |
| 28 | nova-scheduler | nova-scheduler-59cbf7f7fc-tr2wb                | internal | enabled  | up    | 2021-10-08T07:25:28.000000 |
| 31 | nova-scheduler | nova-scheduler-59cbf7f7fc-g9629                | internal | enabled  | up    | 2021-10-08T07:25:22.000000 |
| 34 | nova-scheduler | nova-scheduler-59cbf7f7fc-fs682                | internal | enabled  | up    | 2021-10-08T07:25:12.000000 |
| 37 | nova-compute   | kaas-node-a15c808c-d1cd-4347-8ee8-b5d553ca096c | nova     | enabled  | up    | 2021-10-08T07:25:23.000000 |
| 40 | nova-compute   | kaas-node-f0097dfa-a82e-4f28-b958-c6907f62c302 | nova     | enabled  | up    | 2021-10-08T07:25:29.000000 |
| 43 | nova-compute   | kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577 | nova     | enabled  | up    | 2021-10-08T07:25:35.000000 |
| 46 | nova-compute   | kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec | nova     | enabled  | up    | 2021-10-08T07:25:38.000000 |
| 49 | nova-compute   | kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735 | nova     | enabled  | up    | 2021-10-08T07:25:22.000000 |
+----+----------------+------------------------------------------------+----------+----------+-------+----------------------------+
```

Désactiver le service


```bash
openstack compute service set --disable kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735 nova-compute --disable-reason "Compute is going to be removed."
```
openstack compute service set --disable kaas-node-477bfeb7-bd22-406a-a053-d38b7d23f1ec  nova-compute --disable-reason "Compute is going to be removed."

Le "status" du compute passe alors en "disabled"


Ensure that there are no pods running on the node to delete by draining the node as instructed in the Kubernetes official documentation: Safely drain node : [https://kubernetes.io/docs/tasks/administer-cluster/safely-drain-node/](https://kubernetes.io/docs/tasks/administer-cluster/safely-drain-node/)

```bash
export KUBECONFIG="/home/mcc-user/MCC2.9/kaas-bootstrap/br-mosk001/kubeconfig-br-mosk-01"
kubectl -n br-mosk001 get nodes
kubectl drain  <node name>
kubectl uncordon <node name>
```
``` bash
 kubectl drain kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577
node/kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577 cordoned
error: unable to drain node "kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577", aborting command...

There are pending nodes to be drained:
 kaas-node-606e3abb-6e6c-4da2-a829-3a3b325ed577
error: cannot delete DaemonSet-managed Pods (use --ignore-daemonsets to ignore): kube-system/calico-node-z98sd, kube-system/local-volume-provisioner-8jcc7, kube-system/ucp-nvidia-device-plugin-l6vkc, metallb-system/metallb-speaker-2dng6, openstack/image-precaching-0-9ltd8, openstack/libvirt-libvirt-default-xdsms, openstack/neutron-l3-agent-8046e21058b59d30-wmb9f, openstack/neutron-netns-cleanup-cron-default-577td, openstack/neutron-ovs-agent-default-jk9s2, openstack/nova-compute-default-mm9s2, openstack/openvswitch-openvswitch-db-default-65tfs, openstack/openvswitch-openvswitch-vswitchd-default-262sv, stacklight/fluentd-elasticsearch-w6gqx, stacklight/netchecker-agent-fhr8c, stacklight/netchecker-agent-hostnet-fw77q, stacklight/prometheus-libvirt-exporter-q8qgk, stacklight/prometheus-node-exporter-ln48x, stacklight/telegraf-ds-smart-6zsnp
```

### Suppression de l'agent Nova

récupération d el'ID du compute
```bash
openstack compute service list --host  <cmp_host_name>
openstack compute service list --host kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735
---
+----+--------------+------------------------------------------------+------+----------+-------+----------------------------+
| ID | Binary       | Host                                           | Zone | Status   | State | Updated At                 |
+----+--------------+------------------------------------------------+------+----------+-------+----------------------------+
| 49 | nova-compute | kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735 | nova | disabled | up    | 2021-10-08T07:36:52.000000 |
+----+--------------+------------------------------------------------+------+----------+-------+----------------------------+

```

Suppression du compute
```bash
openstack compute service delete <service_id>
openstack compute service delete 49
```

### suppression de l'agent Neutron

```bash
openstack network agent list --host <cmp_host_name>
openstack network agent list --host kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host                                           | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+
| 07aa85f6-ecde-4d09-95c9-c7cd1809a261 | Open vSwitch agent | kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735 | None              | :-)   | UP    | neutron-openvswitch-agent |
| b1e72853-e2c4-42b9-ad4e-6b88b183d65a | L3 agent           | kaas-node-7a7d6103-3959-4e70-a146-0c47d9a27735 | nova              | :-)   | UP    | neutron-l3-agent          |
+--------------------------------------+--------------------+------------------------------------------------+-------------------+-------+-------+---------------------------+
```
```bash
openstack network agent delete <agent_id>
openstack network agent delete
 pour les 2 
```

## suppression du noed MOSK

```bash
kubectl -n br-mosk001 delete  machine br-mosk001-cmp-9
kubectl -n br-mosk001 delete  bmh br-mosk001-cmp-9
```