

# vérification du backup

- [https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database/verify-backup-job.html](https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database/verify-backup-job.html)

## Vérification des jobs

```bash
kubectl -n openstack get pods -l application=mariadb-phy-backup
NAME                                  READY   STATUS      RESTARTS   AGE
mariadb-phy-backup-1637024400-dtfg8   0/1     Completed   0          26m
```

## Lancement d'un container pour accéder au backup

Récupération de l'image à utiliser

```bash
kubectl -n openstack get pods mariadb-server-0 -o jsonpath='{.spec.containers[0].image}'
127.0.0.1:44301/general/mariadb:10.4.17-bionic-20210617085111
```

Création du fichier pour lancer le pod
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: check-backup-helper
  namespace: openstack
---
apiVersion: v1
kind: Pod
metadata:
  name: check-backup-helper
  namespace: openstack
  labels:
    application: check-backup-helper
spec:
  nodeSelector:
    openstack-control-plane: enabled
  containers:
    - name: helper
      securityContext:
        allowPrivilegeEscalation: false
        runAsUser: 0
        readOnlyRootFilesystem: true
      command:
        - sleep
        - infinity
      image: 127.0.0.1:44301/general/mariadb:10.4.17-bionic-20210617085111
      imagePullPolicy: IfNotPresent
      volumeMounts:
        - name: pod-tmp
          mountPath: /tmp
        - mountPath: /var/backup
          name: mysql-backup
  restartPolicy: Never
  serviceAccount: check-backup-helper
  serviceAccountName: check-backup-helper
  volumes:
    - name: pod-tmp
      emptyDir: {}
    - name: mariadb-secrets
      secret:
        secretName: mariadb-secrets
        defaultMode: 0444
    - name: mariadb-bin
      configMap:
        name: mariadb-bin
        defaultMode: 0555
    - name: mysql-backup
      persistentVolumeClaim:
        claimName: mariadb-phy-backup-data
```

Lancement du pod
```bash
kubectl -n openstack apply -f  backup_check_pod.yaml
kubectl -n openstack get pods -l application=check-backup-helper
```


Vérification de la sauvegarde
```bash
kubectl -n openstack exec -t check-backup-helper -- tree /var/backup
/var/backup
|-- base
|   `-- 2021-11-16_18-46-25
|       |-- backup.stream.gz
|       |-- backup.successful
|       |-- grastate.dat
|       |-- xtrabackup_checkpoints
|       `-- xtrabackup_info
`-- lost+found
```

```bash
kubectl -n openstack exec -i -t check-backup-helper bash
```

Suppression du pod
```bash
kubectl delete -f backup_check_pod.yaml
```
