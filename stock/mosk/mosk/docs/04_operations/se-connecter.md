


POur gérer les 2 clusters
Connection au cluster


## Renouvellement des fichier kubconfig
Pour récupérer le fichier kubeconfig, il faut se connecter à la console d'admin (voir les login/mdp dans credentials), puis choisir le cluster "default" ou "br-mosk-01"
ensuite cliquer sur le trois petit point en haut à droite, et choisir "Download Kubeconfig"



## Cluster MCC

Se connecter au cluster opensatck :

```bash
cd ~/_LOCAL/developpements/openstack/mosk-deploy
source mcc-env.sh
```


Lister le neouds du cluster MCC
```bash
$ kubectl get nodes -A -o wide
NAME                                             STATUS   ROLES    AGE    VERSION               INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
kaas-node-34412b23-0e4b-42c1-949c-4f20d79fc858   Ready    master   279d   v1.20.11-mirantis-1   10.129.172.102   <none>        Ubuntu 18.04.6 LTS   5.4.0-90-generic   docker://20.10.8
kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0   Ready    master   279d   v1.20.11-mirantis-1   10.129.172.100   <none>        Ubuntu 18.04.6 LTS   5.4.0-90-generic   docker://20.10.8
kaas-node-d09c78aa-00c3-40bc-b44d-957e015a8e51   Ready    master   279d   v1.20.11-mirantis-1   10.129.172.101   <none>        Ubuntu 18.04.6 LTS   5.4.0-90-generic   docker://20.10.8
```

Lister les baremetal Host attaché à MCC
```bash
kubectl get bmh
NAME       STATE         CONSUMER   BOOTMODE   ONLINE   ERROR   REGION
master-0   provisioned   master-0   UEFI       true             region-one
master-1   provisioned   master-1   UEFI       true             region-one
master-2   provisioned   master-2   UEFI       true             region-one
```

Lister les machine déployée pour  MCC
```bash
$ kubectl get lcmmachines
NAME       CLUSTERNAME   TYPE      STATE
master-0   kaas-mgmt     control   Ready
master-1   kaas-mgmt     control   Ready
master-2   kaas-mgmt     control   Ready
```


Lister l'ensemble des BareMetal Hosts gérés pour MCC  + Openstack
```bash
kubectl get bmh -A -o wide
NAMESPACE    NAME                  STATUS   STATE         CONSUMER              BMC               BOOTMODE   ONLINE   ERROR   REGION
br-mosk001   br-mosk001-ceph-0     OK       provisioned   br-mosk001-ceph-0     10.29.20.24:623   UEFI       true             region-one
br-mosk001   br-mosk001-ceph-1     OK       provisioned   br-mosk001-ceph-1     10.29.20.25:623   UEFI       true             region-one
br-mosk001   br-mosk001-ceph-2     OK       provisioned   br-mosk001-ceph-2     10.29.20.26:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-01     OK       provisioned   br-mosk001-cmp-01     10.29.20.31:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-02     OK       provisioned   br-mosk001-cmp-02     10.29.20.32:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-06     OK       provisioned   br-mosk001-cmp-06     10.29.20.36:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-07     OK       provisioned   br-mosk001-cmp-07     10.29.20.37:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-09     OK       provisioned   br-mosk001-cmp-09     10.29.20.39:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-10     OK       provisioned   br-mosk001-cmp-10     10.29.20.40:623   legacy     true             region-one
br-mosk001   br-mosk001-cmp-11     OK       provisioned   br-mosk001-cmp-11     10.29.20.41:623   legacy     true             region-one
br-mosk001   br-mosk001-cmp-13     OK       provisioned   br-mosk001-cmp-13     10.29.20.43:623   legacy     true             region-one
br-mosk001   br-mosk001-cmp-14     OK       provisioned   br-mosk001-cmp-14     10.29.20.44:623   UEFI       true             region-one
br-mosk001   br-mosk001-cmp-20     OK       provisioned   br-mosk001-cmp-20     10.29.20.50:623   UEFI       true             region-one
br-mosk001   br-mosk001-master-0   OK       provisioned   br-mosk001-master-0   10.29.20.21:623   UEFI       true             region-one
br-mosk001   br-mosk001-master-1   OK       provisioned   br-mosk001-master-1   10.29.20.22:623   UEFI       true             region-one
br-mosk001   br-mosk001-master-2   OK       provisioned   br-mosk001-master-2   10.29.20.23:623   UEFI       true             region-one
default      master-0              OK       provisioned   master-0              10.29.20.13       UEFI       true             region-one
default      master-1              OK       provisioned   master-1              10.29.20.14       UEFI       true             region-one
default      master-2              OK       provisioned   master-2              10.29.20.15       UEFI       true             region-one


Lister l'ensemble des machines gérés pour MCC  + Openstack
```bash
$ kubectl get lcmmachines -A -o wide
NAMESPACE    NAME                  CLUSTERNAME       TYPE      STATE   INTERNALIP       HOSTNAME                                         AGENTVERSION
br-mosk001   br-mosk001-ceph-0     kaas-br-mosk001   worker    Ready   10.129.172.106   kaas-node-5d555e1d-0636-45a5-868c-b0a3350efe6f   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-ceph-1     kaas-br-mosk001   worker    Ready   10.129.172.107   kaas-node-92e4e163-04ba-4890-be3a-ad81f4f8ad8c   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-ceph-2     kaas-br-mosk001   worker    Ready   10.129.172.108   kaas-node-8a7273c8-f2da-4e34-a088-e76f90fa8504   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-01     kaas-br-mosk001   worker    Ready   10.129.172.111   kaas-node-37e14019-2ff6-447d-8000-a015a52dafc6   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-02     kaas-br-mosk001   worker    Ready   10.129.172.109   kaas-node-3ca03bf6-8687-4927-b841-36701c17b269   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-06     kaas-br-mosk001   worker    Ready   10.129.172.116   kaas-node-8eaa6a0a-0da4-46e3-bb9b-9e1ffa10e995   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-07     kaas-br-mosk001   worker    Ready   10.129.172.117   kaas-node-15d69619-7253-4155-b6a9-e8f2cd46d8d3   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-09     kaas-br-mosk001   worker    Ready   10.129.172.118   kaas-node-96087b45-86de-49d2-91bc-35b3d7ab08cc   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-10     kaas-br-mosk001   worker    Ready   10.129.172.119   kaas-node-b3acb17c-b213-46a1-9779-7588e5592f2e   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-11     kaas-br-mosk001   worker    Ready   10.129.172.120   kaas-node-7817b125-1b8d-4544-8728-5d2cfac3abf5   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-13     kaas-br-mosk001   worker    Ready   10.129.172.110   kaas-node-045c69c7-8b0a-4939-a0e9-118cade51c65   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-14     kaas-br-mosk001   worker    Ready   10.129.172.122   kaas-node-3d62a6db-993b-4c32-8bbe-52d3125eebbc   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-cmp-20     kaas-br-mosk001   worker    Ready   10.129.172.113   kaas-node-77c79309-7f7d-4c57-8a22-89963ba1d015   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-master-0   kaas-br-mosk001   control   Ready   10.129.172.103   kaas-node-b10834cf-25a4-40e9-85be-d7be198417fa   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-master-1   kaas-br-mosk001   control   Ready   10.129.172.104   kaas-node-1e389e22-2a10-4b03-9c2d-37255d4f4ca4   v0.3.0-132-g83a348fa
br-mosk001   br-mosk001-master-2   kaas-br-mosk001   control   Ready   10.129.172.105   kaas-node-71aa87c1-4248-4a6b-9f96-7b50eaf8a8eb   v0.3.0-132-g83a348fa
default      master-0              kaas-mgmt         control   Ready   10.129.172.101   kaas-node-d09c78aa-00c3-40bc-b44d-957e015a8e51   v0.3.0-132-g83a348fa
default      master-1              kaas-mgmt         control   Ready   10.129.172.102   kaas-node-34412b23-0e4b-42c1-949c-4f20d79fc858   v0.3.0-132-g83a348fa
default      master-2              kaas-mgmt         control   Ready   10.129.172.100   kaas-node-88c118e2-9ce8-4509-aeb5-f21a88e985e0   v0.3.0-132-g83a348fa
```

Se connecter à un noeud, par exmeple  kaas-node-77c79309-7f7d-4c57-8a22-89963ba1d015
- avec la liste des lcmmachines, on a 
  - le nom : br-mosk001-cmp-20
  - l'adresse IP : 10.129.172.113
- ave la liste des bmh, on a 
  - l'adresse de la carte idrac correpsondnat au nom : 10.29.20.50
``` bash
ssh -i ssh_key mcc-user@10.129.172.113
```

Pour se conneter aux carte idrec : 
10.29.20.40

## Cluster Openstack

Se conneceter au cluster opensatck :

```bash
cd ~/_LOCAL/developpements/openstack/mosk-deploy
source mosk-env.sh
```

Lister les noeuds
```bash
kubectl get nodes -A -o wide
```




Pour la gestion des architecture, le plus simple est d'utiliser "Lens", et donc d'exporter des fichier kubeconfig avec un token "illimité"

