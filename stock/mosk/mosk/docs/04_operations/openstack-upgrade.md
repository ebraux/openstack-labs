


## backup de la base de données


## check

$ kubectl -n openstack get osdplst
NAME                   OPENSTACK VERSION   CONTROLLER VERSION   STATE
br-mosk001-openstack   ussuri              0.5.7                APPLIED


## modification de la version

Dans le Child Cluster (MOSK)
```bash
kubectl -n openstack get  openstackdeployment 
kubectl -n openstack get  openstackdeployment  -o yaml 

kubectl -n openstack edit  openstackdeployment 
```

change the value of the spec: openstack_version 

## suivi

``` bash
kubectl -n openstack get osdplst
NAME                   OPENSTACK VERSION   CONTROLLER VERSION   STATE
br-mosk001-openstack   victoria            0.5.7                APPLYING
```

STATE : APPLYING
STATE : APPLIED

Déroulement :
- relance des pods "precaching" -> chargement des nouvelles images sur les nodes
- migrate de keystone, puis update de l'api
- ensuite placement, glance, neutron, 




[https://docs.mirantis.com/mos/latest/ops/openstack-operations/upgrade-os.html](https://docs.mirantis.com/mos/latest/ops/openstack-operations/upgrade-os.html)


https://docs.mirantis.com/mos/latest/ref-arch/openstack/openstack-operator/osdpl-cr/standard-conf.html?highlight=customize%20horizon

https://docs.mirantis.com/mos/latest/ops/openstack-operations/run-tempest.html#run-tempest


Hi,
I am following the Openstack upgarde doc : 
https://docs.mirantis.com/mos/latest/ops/openstack-operations/upgrade-os.html

I am trying to backup mariadb database, https://docs.mirantis.com/mos/latest/ops/openstack-operations/backup-restore-os-database/backup-workflow.html, but I don't find the "mariadb-phy-backup job".
