


## DaemonSets

```bash
$ kubectl get daemonset pod1
$ kubectl get daemonsets pod1
$ kubectl get ds pod1
```


## Pods

```bash
$ kubectl get pod pod1
$ kubectl get pods pod1
$ kubectl get po pod1
```

