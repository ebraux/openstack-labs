#!/bin/bash

echo " - build du site"
mkdocs build

echo " - synchronisation"
rsync -av --delete site/ root@b2-80:/var/www/web2/web-as-disi1/www/edisi-archi.disi1.web.telecom-bretagne.eu/web/architectures/mosk

echo " - mise à jour des droits"
ssh root@b2-80 "chown -R 65513.65513 /var/www/web2/web-as-disi1/www/edisi-archi.disi1.web.telecom-bretagne.eu/web/architectures/mosk"

echo " - menage "
rm -rf site
