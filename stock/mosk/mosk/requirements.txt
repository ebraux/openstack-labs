# Documentation static site generator & deployment tool
mkdocs==1.1.2

# Add your custom theme if not inside a theme_dir
# (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
mkdocs-bootswatch==1.1
mkdocs-material==5.4.0
mkdocs-git-revision-date-plugin
mkdocs-git-revision-date-localized-plugin

#mkdocs-awesome-pages-plugin
#mkdocs-mk2pdf-plugin
#mkdocs-pdf-export-plugin
