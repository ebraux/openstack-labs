

http://kimizhang.com/openstack-zoning-regionavailability-zonehost-aggregate/

---

Availability zone is primarily a Nova (read: compute) construct similar to VMware HA cluster. AZ awareness was added to Neutron not so long ago, but it seems to be just that: awareness (or as the documentation says: hints).

Regions are distinct API entry points similar to VMware vCenter instances – just the thing to use if you want the data centers to be independent.
It’s my understanding that you cannot span a tenant network across multiple regions… at least I haven’t found any multi-region confederation capability in OpenStack documentation (apart the ability to select regions in client-facing dashboards).
--> You can configure the same set of provider networks in multiple OpenStack regions and connect them outside of OpenStack using VLANs or VRFs.

---


Regions, cells, and availability zones
As we mentioned before, OpenStack is designed to be scalable, but not infinitely scalable.

There are three different techniques architects can use to segregate an OpenStack cloud: regions, cells, and availability zones.
In this section, we'll walk through how each of these concepts maps to hypervisor topologies.

Regions
From an end user's perspective, OpenStack regions are equivalent to regions in Amazon Web Services.
Regions live in separate data centers and are often named after their geographical location.
If your organization has a data center in Phoenix and one in Raleigh you'll have at least a PHX and a RDU region.
Users who want to geographically disperse their workloads will place some of them in PHX and some of them in RDU.
Regions have separate API endpoints, and although the Horizon UI has some support for multiple regions, they are essentially entirely separate deployments.

From an architectural standpoint, there are two main design choices for implementing regions, which are as follows:
- The first is around authorization. Users will want to have the same credentials for accessing each of the OpenStack regions.
There are a few ways to accomplish this. The simplest way is to use a common backing store (usually LDAP) for the Keystone service in each region.
In this scenario, the user has to authenticate separately to each region to get a token, but the credentials are the same.
Note
In Juno and later, Keystone also supports federation across regions. In this scenario, a Keystone token granted by one region can be presented to another region to authenticate a user. While this currently isn't widely used, it is a major focus area for the OpenStack Foundation and will probably see broader adoption in the future.

The second major consideration for regional architectures is whether or not to present a single set of Glance images to each region.
While work is currently being done to replicate Glance images across federated clouds, most organizations are manually ensuring that the shared images are consistent.
This typically involves building a workflow around image publishing and deprecation which is mindful of the regional layout.

Another option for ensuring consistent images across regions is to implement a central image repository using Swift. This also requires shared Keystone and Glance services which span multiple data centers. Details on how to design multiple regions with shared services are in the OpenStack Architecture Design Guide.

Cells
The Nova compute service has the concept of cells, which can be used to segregate large pools of hypervisors within a single region. This technique is primarily used to mitigate the scalability limits of the OpenStack message bus. The deployment at CERN makes wide use of cells to achieve massive scalability within single regions.

Support for cells varies from service to service and as such cells are infrequently used outside a few very large cloud deployments. The CERN deployment is well-documented and should be used as a reference for these types of deployments.

In our experience, it's much simpler to deploy multiple regions within a single data center than to implement cells to achieve large scale. The added inconvenience of presenting your users with multiple API endpoints within a geographic location is typically outweighed by the benefits of having a more robust platform. If multiple control planes are available in a geographic region, the failure of a single control plane becomes less dramatic.

Tip
The cell architecture has its own set of challenges with regard to networking and scheduling of instance placement. Some very large companies that support the OpenStack effort have been working for years to overcome these hurdles. However, many different OpenStack distributions are currently working on a new control plane design. These new designs would begin to split the OpenStack control plane into containers running the OpenStack services in a microservice type architecture. This way the services themselves can be placed anywhere and be scaled horizontally based on the load. One architecture that has garnered a lot of attention lately is the Kolla project, which promotes Docker containers and Ansible playbooks to provide production-ready containers and deployment tools for operating OpenStack clouds. To see more, go to https://wiki.openstack.org/wiki/Kolla.

Availability zones
Availability zones are used to group hypervisors within a single OpenStack region. Availability zones are exposed to the end user and should be used to provide the user with an indication of the underlying topology of the cloud. The most common use case for availability zones is to expose failure zones to the user.

To ensure the H/A of a service deployed on OpenStack, a user will typically want to deploy the various components of their service onto hypervisors within different racks. This way, the failure of a top of rack switch or a PDU will only bring down a portion of the instances which provide the service. Racks form a natural boundary for availability zones for this reason. There are a few other interesting uses of availability zones apart from exposing failure zones to the end user. One financial services customer we work with had a requirement for the instances of each line of business to run on dedicated hardware. A combination of availability zones and the AggregateMultiTenancyIsolation Nova Scheduler filter was used to ensure that each tenant had access to dedicated compute nodes.

Availability zones can also be used to expose hardware classes to end users. For example, hosts with faster processors might be placed in one availability zone and hosts with slower processors might be placed in different availability zones. This allows end users to decide where to place their workloads based upon compute requirements.

---
## Cells

https://docs.openstack.org/kolla-ansible/zed/reference/compute/nova-cells-guide.html
https://docs.openstack.org/nova/zed/admin/cells.html

---
## Aggregats
Les agrégats (aggregates) dans OpenStack sont utilisés pour regrouper des ressources matérielles, telles que des nœuds hyperviseurs (compute nodes), en fonction de caractéristiques communes. Ces caractéristiques peuvent inclure des éléments tels que le matériel sous-jacent, la localisation physique, la performance, ou d'autres attributs pertinents. L'utilisation des agrégats permet une gestion plus fine des ressources et offre un contrôle accru sur le placement des instances de machines virtuelles (VM) dans un environnement OpenStack.

Voici quelques utilisations courantes des agrégats dans OpenStack :

1. **Contrôle du Placement :** En associant des nœuds hyperviseurs à des agrégats, vous pouvez spécifier que certaines instances doivent être déployées uniquement sur les nœuds inclus dans un agrégat particulier. Cela permet un contrôle précis du placement des VM en fonction des besoins de l'application.

2. **Gestion des Ressources :** Les agrégats peuvent être utilisés pour gérer les ressources de manière plus granulaire. Par exemple, si certains nœuds hyperviseurs ont des caractéristiques matérielles spécifiques (CPU, mémoire, stockage) ou sont situés dans une région géographique particulière, vous pouvez les regrouper dans des agrégats correspondants.

3. **Isolation ou Groupement :** Les agrégats peuvent être utilisés pour isoler certaines ressources ou, au contraire, pour regrouper des ressources similaires. Cela peut être utile pour des raisons de performance, de sécurité, ou pour répondre à des exigences spécifiques de l'entreprise.

4. **Planification et Déploiement :** Lors de la planification de déploiements d'instances, les agrégats peuvent être pris en compte pour garantir que les instances sont déployées sur des ressources appropriées en fonction de critères spécifiques.

Un exemple d'utilisation pratique pourrait être la création d'un agrégat pour regrouper des nœuds hyperviseurs situés dans le même centre de données physique. Ensuite, vous pouvez spécifier que certaines instances doivent être déployées uniquement dans cet agrégat, garantissant ainsi qu'elles sont exécutées sur des ressources situées dans le même emplacement physique.

En résumé, les agrégats offrent une manière flexible et puissante de gérer les ressources et de contrôler le placement des instances dans un environnement OpenStack, ce qui peut être essentiel pour répondre aux exigences spécifiques des charges de travail et des politiques de déploiement.

---
## server groups
Dans OpenStack, les "server groups" (groupes de serveurs) sont des fonctionnalités qui permettent de regrouper plusieurs instances de machines virtuelles (VM) afin de contrôler leur déploiement et leur placement au sein du cloud. Ces groupes sont conçus pour faciliter la gestion des ressources et améliorer la disponibilité et la résilience des applications déployées.

Voici quelques utilisations courantes des server groups dans OpenStack :

1. **Affinité ou anti-affinité :**
   - **Affinité :** Les server groups permettent de spécifier que certaines instances doivent être déployées sur le même hyperviseur physique (serveur hôte). Cela peut être utile lorsque des instances ont besoin de communiquer fréquemment entre elles et peuvent bénéficier d'une faible latence.
   - **Anti-affinité :** À l'inverse, les server groups peuvent également spécifier que certaines instances ne doivent pas être déployées sur le même hyperviseur. Cela peut améliorer la résilience en répartissant les instances sur des hôtes distincts pour éviter une panne complète si un hôte échoue.

2. **Répartition de charge :**
   - Les server groups peuvent être utilisés pour créer des groupes de serveurs destinés à la répartition de charge. Dans ce cas, plusieurs instances du même service ou de la même application peuvent être déployées et réparties sur différents nœuds pour équilibrer la charge du réseau.

3. **Migration de VM :**
   - Lorsque vous devez effectuer la migration d'instances d'une machine physique à une autre (live migration), les server groups peuvent être utilisés pour spécifier des politiques de migration, telles que l'affinité ou l'anti-affinité, pour contrôler le comportement de migration.

4. **Haute disponibilité :**
   - En utilisant des server groups avec des politiques d'anti-affinité, vous pouvez améliorer la haute disponibilité de vos applications en vous assurant que les instances ne sont pas toutes déployées sur le même hôte, réduisant ainsi les risques liés à une défaillance matérielle.

En résumé, les server groups dans OpenStack offrent une flexibilité significative pour gérer le déploiement des instances de VM, améliorer la performance, la résilience et la disponibilité des applications dans un environnement cloud.


---
## aggregats vs server groups
Les server groups et les aggregates (aggrégats) sont deux concepts distincts dans OpenStack, et bien qu'ils puissent sembler similaires à première vue, ils servent des objectifs différents.

1. **Server Groups :**
   - **Objectif :** Les server groups permettent de regrouper plusieurs instances de machines virtuelles (VM) pour influencer leur déploiement et leur placement sur les hyperviseurs.
   - **Utilisations typiques :** Affinité ou anti-affinité, répartition de charge, migration de VM, et amélioration de la haute disponibilité.
   - **Exemple d'utilisation :** Vous pouvez créer un server group avec une politique d'anti-affinité pour garantir que certaines instances ne sont pas déployées sur le même hyperviseur afin d'assurer une meilleure résilience.

2. **Aggregates (Aggrégats) :**
   - **Objectif :** Les aggregates sont utilisés pour regrouper des ressources matérielles (hyperviseurs) en fonction de caractéristiques communes, comme le matériel sous-jacent, la localisation, etc.
   - **Utilisations typiques :** Contrôle du placement des instances sur des ressources spécifiques, gestion des ressources, et amélioration de la performance.
   - **Exemple d'utilisation :** Vous pouvez créer un agrégat pour regrouper des nœuds hyperviseurs situés dans un même centre de données physique, puis spécifier que certaines instances doivent être déployées uniquement dans cet agrégat.

En résumé, les server groups sont axés sur le regroupement d'instances pour influencer leur placement, tandis que les aggregates sont utilisés pour regrouper des ressources matérielles (hyperviseurs) pour des raisons de gestion des ressources et de contrôle du placement des instances. Ces deux concepts peuvent être utilisés conjointement pour permettre une gestion plus fine des ressources et des déploiements dans un environnement OpenStack.

cette doc date un peu, mais on voit bien le concept. Et je pense que ça fonctionne toujours comme ça.
https://raymii.org/s/articles/Openstack_Affinity_Groups-make-sure-instances-are-on-the-same-or-a-different-hypervisor-host.html

On créer un groupe, avec une régle d'affinité, de non affinité, ...
Et quand on lance une instance, on ajoute une règle au scheduler, pour la placer dans un groupe `--hint group=$affinity-group-uuid`.
C'est les instance qu'on ajoute dans un groupe.

Alors que pour les agrégats, c'est les machines hôte

l'affinity ca permet d'éviter le traffic  réseau entre les noeud pour rien dans les achitecture Ntier
L'anti affinity, pour sécuriser un cluster

---
## les cells

Les "cells" (cellules) dans OpenStack font référence à une fonctionnalité spécifique du service Nova, qui est le composant responsable de la gestion des instances de machines virtuelles (VM). L'idée derrière les cellules est de permettre à OpenStack de s'échelonner horizontalement, en gérant plusieurs groupes de nœuds hyperviseurs (compute nodes) comme s'ils étaient des entités distinctes.

Voici quelques-unes des raisons pour lesquelles les cellules sont utilisées dans OpenStack :

1. **Évolutivité Horizontale :** Les cellules permettent à OpenStack de s'échelonner horizontalement en divisant l'infrastructure en cellules autonomes. Chaque cellule peut avoir son propre ensemble de nœuds hyperviseurs, sa propre base de données, et son propre contrôleur Nova.

2. **Répartition de Charge :** En utilisant des cellules, il est possible de répartir la charge de manière plus efficace en répartissant les instances de VM entre différentes cellules. Cela peut contribuer à améliorer la performance et la gestion des ressources.

3. **Isolation et Échelle :** Les cellules permettent d'isoler différentes parties de l'infrastructure, ce qui peut être utile pour des raisons de gestion, de performances ou de sécurité. Chaque cellule peut évoluer indépendamment en fonction des besoins.

4. **Gestion de la Latence :** Dans des environnements distribués sur plusieurs sites géographiques, les cellules peuvent être utilisées pour réduire la latence en plaçant les instances de VM plus près des utilisateurs finaux ou des services.

5. **Gestion des Erreurs :** En cas de problème dans une cellule spécifique, les autres cellules peuvent continuer à fonctionner normalement. Cela contribue à rendre l'ensemble du déploiement plus résilient aux pannes locales.

Il est important de noter que l'utilisation des cellules dans OpenStack peut ajouter de la complexité à la configuration et à la gestion de l'infrastructure. Les cellules sont souvent mises en œuvre dans des déploiements OpenStack à grande échelle pour gérer des centaines ou des milliers de nœuds hyperviseurs. Pour les déploiements plus petits ou moins complexes, l'utilisation de cellules peut ne pas être nécessaire.

En résumé, les cellules dans OpenStack permettent d'organiser l'infrastructure en entités autonomes, offrant une meilleure évolutivité, une gestion plus fine des ressources, et une résilience accrue face aux pannes.


https://www.guruadvisor.net/en/cloud/121-openstack-regions-and-availability-zones

https://www.rackspace.com/sites/default/files/white-papers/scaling-openstack-nova-cells.pdf

https://www.redhat.com/en/blog/running-red-hat-openstack-platform-16-multiple-cells


https://www.redhat.com/en/blog/scaling-red-hat-openstack-platform-more-500-overcloud-nodes



keystone federation :
- https://www.alibabacloud.com/tech-news/a/keystone/gw178027dk-keystone-federation-for-newbies
- https://diurnal.st/2021/07/17/openstack-keystone-federation-part-1.html
- https://beyondtheclouds.github.io/blog/openstack/cockroachdb/2018/06/04/evaluation-of-openstack-multi-region-keystone-deployments.html
- 