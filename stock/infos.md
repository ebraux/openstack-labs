
Dans les fichiers de configuration :

- les 2 machines sont reliées au même réseau
- Le controler a pour nom 'controller'. Il est accessible directement par une  IP publique.
- Le compute a pour nom 'compute'. Il n'est pas accessible directement. Il faut passer par un rebond depuis le controller.
- Tous les ports doivent être ouverts entre les 2 machines. Les accès au controller sont limités aux ports 22, 80 et 5000.
- La plage d'IP utilisée pour les IP flottantes dans l'instance Openstack déployée n'est pas une plage routable, mais une plage locale au controller. L'accès aux instances déployées dans Openstack se ne sera donc possible que depuis le controller.


Une template heat correspondant a cette infrastructure est disponible a cette adresse : [openstack_lab.yml]  (./01_lab/openstack_lab.yml). Elle reprend :


