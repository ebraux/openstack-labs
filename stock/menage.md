dnf clean packages
dnf clean metadata
dnf clean dbcache
dnf clean all
rm -rf /var/cache/dnf/*

sudo systemctl stop libvirtd.service openstack-nova-compute.service

sudo systemctl stop httpd.service memcached.service 