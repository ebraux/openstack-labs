

https://docs.openstack.org/nova/2023.2/install/compute-install-ubuntu.html


sudo apt install -y nova-compute

 /etc/nova/nova-compute.conf

[DEFAULT]
# Bug Fix
compute_driver=libvirt.LibvirtDriver

[libvirt]
virt_type = qemu


sudo systemctl restart  nova-compute 


openstack compute service list --service nova-compute
+--------------------------------------+--------------+------------+------+---------+-------+----------------------------+
| ID                                   | Binary       | Host       | Zone | Status  | State | Updated At                 |
+--------------------------------------+--------------+------------+------+---------+-------+----------------------------+
| 64bb3372-114e-4c52-8749-db4c28fb1e8e | nova-compute | controller | nova | enabled | up    | 2024-04-18T13:07:46.000000 |
+--------------------------------------+--------------+------------+------+---------+-------+----------------------------+


sudo su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova


openstack hypervisor list
+--------------------------------------+---------------------+-----------------+-----------+-------+
| ID                                   | Hypervisor Hostname | Hypervisor Type | Host IP   | State |
+--------------------------------------+---------------------+-----------------+-----------+-------+
| bde922b4-f754-4df9-be78-41f095414b7d | ubuntu              | QEMU            | 10.0.2.15 | up    |
+--------------------------------------+---------------------+-----------------+-----------+-------+

---
## réseau


https://docs.openstack.org/neutron/2023.2/install/compute-install-ubuntu.html

 sudo systemctl restart  neutron-linuxbridge-agent.service