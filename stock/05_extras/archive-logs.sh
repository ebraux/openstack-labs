## archive deploy logs
sudo systemctl stop  neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent
sudo systemctl stop nova-api nova-scheduler nova-conductor nova-novncproxy
sudo systemctl stop glance-api
sudo systemctl stop apache2

export LOG_PHASE='deploy'

for i in $(sudo ls /var/log/neutron/*.log ); do sudo mv $i $i.$LOG_PHASE; done
for i in $(sudo ls /var/log/nova/*.log ); do sudo mv $i $i.$LOG_PHASE; done
for i in $(sudo ls /var/log/glance/*.log ); do sudo mv $i $i.$LOG_PHASE; done
for i in $(sudo ls /var/log/apache2/*.log ); do sudo mv $i $i.$; done

sudo systemctl start apache2
sudo sleep 10
sudo systemctl start glance-api
sudo systemctl start nova-api nova-scheduler nova-conductor nova-novncproxy
sudo systemctl start  neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent
