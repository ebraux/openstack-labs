
PROJECTID=$1
USERID=$2


curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -X GET \
 http://$OS_HOST/identity/v3/projects/$PROJECTID/users/$USERID/roles | python -mjson.tool

