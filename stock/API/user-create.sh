
USERNAME=$1

curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -H "Content-Type: application/json" \
 -X POST \
 -d '
{ "user" : {
    "name": "'$USERNAME'",
    "password":"'$USERNAME'",
    "description": "utilisateur '$USERNAME'",
    "email": "'$USERNAME'@test.fr",
    "domain_id": "default" }
}' \
 http://$OS_HOST/identity/v3/users | python -mjson.tool

