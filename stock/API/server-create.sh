
SERVERNAME=$1
IMAGE="93d6dfd2-b5a7-4c6c-a046-476c1d5d2115"

curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -H "Content-Type: application/json" \
 -d '
{ "server": {
        "name": "'${SERVERNAME}'",
        "imageRef": "'${IMAGE}'",
        "flavorRef": "1",
        "metadata": {
            "My Server Name": "Test API'${SERVERNAME}'"
        }
    }
}' \
 http://$OS_HOST:8774/v2.1/servers | python -mjson.tool

# a0992460-5b41-43bf-8503-0ab050c498ca
#       "min_count": "2",
#        "max_count": "3"

