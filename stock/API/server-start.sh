curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -H "Content-Type: application/json" \
 -d '
{
    "os-start" : null
}' \
 http://$OS_HOST:8774/v2.1/servers/$1/action 
#| python -mjson.tool

# a0992460-5b41-43bf-8503-0ab050c498ca
#       "min_count": "2",
#        "max_count": "3"

