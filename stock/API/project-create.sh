
PROJECTNAME=$1

curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -H "Content-Type: application/json" \
 -X POST \
 -d '
{ "project" : {
      "name": "'$PROJECTNAME'",
      "description": "projet '$PROJECTNAME'",
      "domain_id": "default"
  }
}' \
 http://$OS_HOST/identity/v3/projects | python -mjson.tool

#{
#    "project": {
#        "description": "My new project",
#        "domain_id": "default",
#        "enabled": true,
#        "is_domain": true,
#        "name": "myNewProject"
#    }
#}
