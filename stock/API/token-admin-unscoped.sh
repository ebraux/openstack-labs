curl -si \
  -H "Content-Type: application/json" \
  -d '
{ "auth": { 
    "identity": {
      "methods": ["password"],
      "password": {
        "user": {"name": "admin",
          "domain": { "id": "default" },
          "password": "stack"
        }
      }
    }
  }
}' \
http://$OS_HOST/identity/v3/auth/tokens  | grep X-Subject-Token
