curl -si \
  -H "Content-Type: application/json" \
  -d '
{ "auth": { 
    "identity": {
      "methods": ["password"],
      "password": {
        "user": {"name": "admin",
          "domain": { "id": "default" },
          "password": "stack"
        }
      }
    },
    "scope": {
      "project": {
        "name": "admin",
        "domain": { "id": "default" }
      }
    }
  }
}' \
http://$OS_HOST/identity/v3/auth/tokens  | python -mjson.tool

#    "scope": {
#      "domain": {
#        "id": "default"
#      }
#    }


#    "scope": {
#      "project": {
#        "name": "demo",
#        "domain": { "id": "default" }
#      }
#    }

# | grep X-Subject-Token
#  http://$OS_HOST:5000/v3/auth/tokens  | grep X-Subject-Token

#http://$OS_HOST:5000/v2.0 

# | grep X-Subject-Token
#http://$OS_HOST:5000/v3/auth/tokens  | grep X-Subject-Token
#  | python -mjson.tool


#    "scope": {
#      "project": {
#        "name": "admin",
#        "domain": { "id": "default" }
#      }
#    }
