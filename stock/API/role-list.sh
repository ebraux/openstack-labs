curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -H "Content-Type: application/json" \
 -X GET \
 http://$OS_HOST/identity/v3/roles | python -mjson.tool

