curl -is \
  -H "Content-Type: application/json" \
  -d '
{ "auth": { 
    "identity": {
      "methods": ["password"],
      "password": {
        "user": {"name": "demo",
          "domain": { "id": "default" },
          "password": "stack"
        }
      }
    },
    "scope": {
      "project": {
        "name": "demo",
        "domain": { "id": "default" }
      }
    }
  }
}' \
http://192.168.113.129:5000/v3/auth/tokens  | grep X-Subject-Token

