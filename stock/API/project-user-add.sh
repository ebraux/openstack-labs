
PROJECTID=$1
USERID=$2
ROLEID=$3


curl -s \
 -H "X-Auth-Token: $OS_TOKEN" \
 -X PUT \
 http://$OS_HOST/identity/v3/projects/$PROJECTID/users/$USERID/roles/$ROLEID
