



A tester :
Vu avec NFS, il faut installer les client NFS sur le compute, et tester l'accès.
La commande du client est dans les logs du compute.
Et message d'erreur pas clair du tout.

--> la création du volume se fait bien sur  LVM
--> le problème d'accès est peut ^tre juste entre le compute, et le iscsi du controlleur
il manque peut être jsute le client



### Identification du disque à utiliser


Vérification des Périphériques de stockage disponibles 
``` bash
sudo fdisk -l 
# ...
# Disk /dev/loopxx
# ...
#
# Disk /dev/vda: 111,76 GiB, 120000000000 bytes, 234375000 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
# Disklabel type: gpt
# Disk identifier: 01BBFCE3-32D6-4286-8BD9-9F9D18A081D3

# Device      Start       End   Sectors   Size Type
# /dev/vda1  227328 234374966 234147639 111,7G Linux filesystem
# /dev/vda14   2048     10239      8192     4M BIOS boot
# /dev/vda15  10240    227327    217088   106M EFI System

# Partition table entries are not in disk order.


# Disk /dev/sda: 953,67 MiB, 1000000000 bytes, 1953125 sectors
# Disk model: b_ssd           
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 4096 bytes
# I/O size (minimum/optimal): 4096 bytes / 4194304 bytes

```

- Ignorer les "Disk /dev/loopxx"
- "Disk /dev/vda" est le disque système
- Il reste donc "/dev/sda", un disque de 1GO qui n'est pas utilisé


> Dans le cas où aucun périphérique de stockage ne serait disponible il est possible de créer un "périphérique virtuel" : voir doc 04-storage.md 


### Gestion du  volume LVM

Configuration de LVM : les volumes Cinder seront créés dans le LVM Volume Group "cinder-volumes".

``` bash
sudo apt install -y lvm2 thin-provisioning-tools
```

- Création du "LVM physical volume" avec /dev/sda:
``` bash
sudo pvcreate /dev/sda
#   Physical volume "/dev/sda" successfully created.

```

- Création du "LVM volume group cinder-volumes"
``` bash
sudo vgcreate cinder-volumes /dev/sda
#  Volume group "cinder-volumes" successfully created
```

à voir
``` bash
  /etc/lvm/lvm.conf 
In the devices section, add a filter that accepts the /dev/sdb device and rejects all other devices:

devices {
...
filter = [ "a/sdb/", "r/.*/"]
```






Information sur les Logical Volumes
``` bash

sudo lvs -a cinder-volumes
  # LV                                          VG             Attr       LSize   Pool                Origin Data%  Meta%  Move Log Cpy%Sync Convert
  # cinder-volumes-pool                         cinder-volumes twi-aotz-- 908,00m                            0,00   10,94                           
  # [cinder-volumes-pool_tdata]                 cinder-volumes Twi-ao---- 908,00m                                                                   
  # [cinder-volumes-pool_tmeta]                 cinder-volumes ewi-ao----   4,00m                                                                   
  # [lvol0_pmspare]                             cinder-volumes ewi-------   4,00m                                                                   
  # volume-1ce4cb40-159e-4b67-bae0-dda8083e12bf cinder-volumes Vwi-a-tz--   1,00g cinder-volumes-pool        0,00        
```

Affichage des partitions Linux
``` bash
sudo lsblk /dev/sda
# NAME                                                                   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
# sda                                                                      8:0    0 953,7M  0 disk 
# ├─cinder--volumes-cinder--volumes--pool_tmeta                          253:0    0     4M  0 lvm  
# │ └─cinder--volumes-cinder--volumes--pool-tpool                        253:2    0   908M  0 lvm  
# │   ├─cinder--volumes-cinder--volumes--pool                            253:3    0   908M  1 lvm  
# │   └─cinder--volumes-volume--1ce4cb40--159e--4b67--bae0--dda8083e12bf 253:4    0     1G  0 lvm  
# └─cinder--volumes-cinder--volumes--pool_tdata                          253:1    0   908M  0 lvm  
#   └─cinder--volumes-cinder--volumes--pool-tpool                        253:2    0   908M  0 lvm  
#     ├─cinder--volumes-cinder--volumes--pool                            253:3    0   908M  1 lvm  
#     └─cinder--volumes-volume--1ce4cb40--159e--4b67--bae0--dda8083e12bf 253:4    0     1G  0 lvm  
```

> t_meta = thin provisioning metadatas, t_data = thin provisionng datas






---
## infos
Vérifier les fichiers de conf
-  /etc/tgt/conf.d
-  

Normalement cindr ajouter son fichier :
``` bash
echo 'include /var/lib/cinder/volumes/ *' >> /etc/tgt/conf.d/cinder_tgt.conf
```

vérification du service :
``` bash
sudo ss -ltunp | grep 3260
tcp   LISTEN 0      4096                     0.0.0.0:3260       0.0.0.0:*    users:(("tgtd",pid=889,fd=6))                                                                               
```                                                                                                                                                                                                                                                                                                                                                   
tcp   LISTEN 0      4096                        [::]:3260          [::]:*    users:(("tgtd",pid=889,fd=7))  
 --> OK tgtd écoute sur le port 3260

iscsiadm -m discovery -t st -p172.16.10.3:3260

Run iscsiadm in debug mode:
iscsiadm -m discovery -t st -d8 -p 172.16.10.3




nova sur compute
cinderclient.exceptions.ClientException: Unable to update attachment.(Invalid input received: Connector doesn't have required information: initiator). (HTTP 500) (Request-ID: req-40336fb4-4a46-4ebe-9d12-e7fdfee3cc63)


cinder-volume
2024-04-28 11:51:01.234 26026 ERROR cinder.volume.targets.iscsi [req-85538edf-8e4c-4f2c-9306-7ec27b2aaf7c req-7d5c7ea0-0891-473b-9d34-4424dccec4fc ce0e0fac2cab47799a7ffb71c1443586 1de11093869349ed86afcdf646c64bbc - - - -] The volume driver requires the iSCSI initiator name in the connector.
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server [req-85538edf-8e4c-4f2c-9306-7ec27b2aaf7c req-7d5c7ea0-0891-473b-9d34-4424dccec4fc ce0e0fac2cab47799a7ffb71c1443586 1de11093869349ed86afcdf646c64bbc - - - -] Exception during message handling: cinder.exception.InvalidInput: Invalid input received: Connector doesn't have required information: initiator
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server Traceback (most recent call last):
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/cinder/volume/manager.py", line 4795, in _connection_create
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     self.driver.validate_connector(connector)
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/cinder/volume/drivers/lvm.py", line 847, in validate_connector
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     return self.target_driver.validate_connector(connector)
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/cinder/volume/targets/iscsi.py", line 289, in validate_connector
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     raise exception.InvalidConnectorException(missing='initiator')
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server cinder.exception.InvalidConnectorException: Connector doesn't have required information: initiator
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server 
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server During handling of the above exception, another exception occurred:
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server 
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server Traceback (most recent call last):
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/oslo_messaging/rpc/server.py", line 165, in _process_incoming
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     res = self.dispatcher.dispatch(message)
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/oslo_messaging/rpc/dispatcher.py", line 309, in dispatch
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     return self._do_dispatch(endpoint, method, ctxt, args)
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/oslo_messaging/rpc/dispatcher.py", line 229, in _do_dispatch
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     result = func(ctxt, **new_args)
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/cinder/volume/manager.py", line 4878, in attachment_update
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     connection_info = self._connection_create(context,
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server   File "/usr/lib/python3/dist-packages/cinder/volume/manager.py", line 4797, in _connection_create
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server     raise exception.InvalidInput(reason=str(err))
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server cinder.exception.InvalidInput: Invalid input received: Connector doesn't have required information: initiator
2024-04-28 11:51:01.235 26026 ERROR oslo_messaging.rpc.server 




 tgtadm --lld iscsi --mode target --op show
--> rien
 --lld <driver> --mode target --op show
	show all the targets.


ll /etc/tgt/conf.d
--> rien





ficiher /etc/tgt/conf.d/iscsi.conf 

tgtadm --mode target --op show




https://guide.ubuntu-fr.org/server/iscsi-initiator.html
sudo iscsiadm -m discovery -t st -p  172.16.10.3
iscsiadm: Connection to Discovery Address 172.16.10.3 failed
iscsiadm: Login I/O error, failed to receive a PDU


iscsiadm  --mode discovery --type sendtargets --portal  172.16.10.3


pour etre sur que le daemon iscsi tourne : 
iscsiadm -m session
#iscsiadm: No active sessions.


Il faut "découvrir" 
sudo iscsiadm -m discoverydb -t st -p 172.16.10.3 --discover
sudo iscsiadm -m discovery -t sendtargets -p 172.16.10.3 

et ensuite se connecter
sudo iscsiadm -m node -l



----------
sed -i 's/udev_rules = 1/udev_rules = 0/' /etc/lvm/lvm.conf
sed -i 's/use_lvmetad = 1/use_lvmetad = 0/' /etc/lvm/lvm.conf

#Adding LVM filter
sed -i 's:filter = \[ "a/.\*/" \]:filter = \[ "a/sda/", "r/.\*/"\]:g' /etc/lvm/lvm.conf

#Adding cinder volumes to tgtd config
echo "include /etc/cinder/volumes/*" >> /etc/tgt/tgtd.conf



---
iscsi

# Must have some pre-defined targets to login to
ConditionDirectoryNotEmpty=|/etc/iscsi/nodes
# or have a session to use via iscsid
ConditionDirectoryNotEmpty=|/sys/class/iscsi_session

ExecStart=/sbin/iscsiadm -m node --loginall=automatic
ExecStart=/lib/open-iscsi/activate-storage.sh

ExecStop=/lib/open-iscsi/umountiscsi.sh
ExecStop=/bin/sync
ExecStop=/lib/open-iscsi/logout-all.sh



/etc/default/open-iscsi


ExecStart=/usr/sbin/tgtd -f
ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v offline
ExecStartPost=/usr/sbin/tgt-admin -e -c /etc/tgt/targets.conf
ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v ready


sudo modprobe iscsi_tcp
sudo mkdir -p /etc/iscsi/nodes


---
## autres
 tgtadm --lld iscsi --mode target --op show
--> rien
 --lld <driver> --mode target --op show
	show all the targets.


ll /etc/tgt/conf.d
--> rien


echo 'include /var/lib/cinder/volumes/ *' >> /etc/tgt/conf.d/cinder_tgt.conf


ficiher /etc/tgt/conf.d/iscsi.conf 

tgtadm --mode target --op show




https://guide.ubuntu-fr.org/server/iscsi-initiator.html
sudo iscsiadm -m discovery -t st -p  172.16.10.3
iscsiadm: Connection to Discovery Address 172.16.10.3 failed
iscsiadm: Login I/O error, failed to receive a PDU


iscsiadm  --mode discovery --type sendtargets --portal  172.16.10.3


pour etre sur que le daemon iscsi tourne : 
iscsiadm -m session
#iscsiadm: No active sessions.


Il faut "découvrir" 
sudo iscsiadm -m discoverydb -t st -p 172.16.10.3 --discover
sudo iscsiadm -m discovery -t sendtargets -p 172.16.10.3 

et ensuite se connecter
sudo iscsiadm -m node -l



----------
sed -i 's/udev_rules = 1/udev_rules = 0/' /etc/lvm/lvm.conf
sed -i 's/use_lvmetad = 1/use_lvmetad = 0/' /etc/lvm/lvm.conf

#Adding LVM filter
sed -i 's:filter = \[ "a/.\*/" \]:filter = \[ "a/sda/", "r/.\*/"\]:g' /etc/lvm/lvm.conf

#Adding cinder volumes to tgtd config
echo "include /etc/cinder/volumes/*" >> /etc/tgt/tgtd.conf



---
iscsi

# Must have some pre-defined targets to login to
ConditionDirectoryNotEmpty=|/etc/iscsi/nodes
# or have a session to use via iscsid
ConditionDirectoryNotEmpty=|/sys/class/iscsi_session

ExecStart=/sbin/iscsiadm -m node --loginall=automatic
ExecStart=/lib/open-iscsi/activate-storage.sh

ExecStop=/lib/open-iscsi/umountiscsi.sh
ExecStop=/bin/sync
ExecStop=/lib/open-iscsi/logout-all.sh



/etc/default/open-iscsi


ExecStart=/usr/sbin/tgtd -f
ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v offline
ExecStartPost=/usr/sbin/tgt-admin -e -c /etc/tgt/targets.conf
ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v ready


sudo modprobe iscsi_tcp
sudo mkdir -p /etc/iscsi/nodes


vérification du service :
sudo ss -ltunp | grep 3260
tcp   LISTEN 0      4096                     0.0.0.0:3260       0.0.0.0:*    users:(("tgtd",pid=889,fd=6))                                                                                                                                                                                                                                                                                                                                                                                                                                    
tcp   LISTEN 0      4096                        [::]:3260          [::]:*    users:(("tgtd",pid=889,fd=7))  
 --> OK tgtd écoute sur le port 3260

iscsiadm -m discovery -t st -p172.16.10.3:3260

Run iscsiadm in debug mode:
iscsiadm -m discovery -t st -d8 -p 172.16.10.3


---
## Config




[DEFAULT]

default_volume_type = lvmdriver-1
enabled_backends = lvmdriver-1
state_path = /opt/stack/data/cinder

osapi_volume_listen = 0.0.0.0
osapi_volume_extension = cinder.api.contrib.standard_extensions
rootwrap_config = /etc/cinder/rootwrap.conf
api_paste_config = /etc/cinder/api-paste.ini

target_helper = lioadm

debug = True


[lvmdriver-1]
image_volume_cache_enabled = True
volume_clear = zero
lvm_type = auto
target_prefix = iqn.2010-10.org.openstack:
target_port = 3260
target_protocol = iscsi
target_helper = lioadm
volume_group = stack-volumes-lvmdriver-1
volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver
volume_backend_name = lvmdriver-1



---
 sudo vgdisplay
  --- Volume group ---
  VG Name               stack-volumes-lvmdriver-1
  System ID             


--
open-iscsi/jammy,now 2.1.5-1ubuntu1 amd64 [installed]


systemctl  status iscsid.service 
● iscsid.service - iSCSI initiator daemon (iscsid)
     Loaded: loaded (/lib/systemd/system/iscsid.service; disabled; vendor preset: enabled)
     Active: active (running) since Sun 2024-04-28 15:25:02 CEST; 10min ago
TriggeredBy: ● iscsid.socket
       Docs: man:iscsid(8)
   Main PID: 374228 (iscsid)
      Tasks: 2 (limit: 14302)
     Memory: 2.6M
        CPU: 24ms
     CGroup: /system.slice/iscsid.service
             ├─374227 /sbin/iscsid
             └─374228 /sbin/iscsid


systemctl  status iscsi.service 
○ open-iscsi.service - Login to default iSCSI targets
     Loaded: loaded (/lib/systemd/system/open-iscsi.service; enabled; vendor preset: enabled)
     Active: inactive (dead)
  Condition: start condition failed at Fri 2024-04-19 05:48:42 CEST; 1 week 2 days ago



NAMEFILE=/etc/iscsi/initiatorname.iscsi
CONFIGFILE=/etc/iscsi/iscsid.conf


cat /etc/iscsi/initiatorname.iscsi
InitiatorName=iqn.2016-04.com.open-iscsi:65c9b213a54b
