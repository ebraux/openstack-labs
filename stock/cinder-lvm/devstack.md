
sudo sed -i 's/^.*# from devstack$//' /etc/lvm/lvm.conf
``` bash
        # Configuration option devices/global_filter.
        # Limit the block devices that are used by LVM system components.
        # Because devices/filter may be overridden from the command line, it is
        # not suitable for system-wide device filtering, e.g. udev.
        # Use global_filter to hide devices from these LVM system components.
        # The syntax is the same as devices/filter. Devices rejected by
        # global_filter are not opened by LVM.
        # This configuration option has an automatic default value.
        # global_filter = [ "a|.*|" ]
        global_filter = [ "a|loop3|", "r|.*|" ]  # from devstack
```