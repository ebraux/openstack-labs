

# Principe d'openstack-ansible


### Opensatck-ansible

## Fichier de configuration


## Déploiement

* [https://subscription.packtpub.com/book/virtualization_and_cloud/9781788398763/1]

## faire prendre en compte une modification de la configuration

executed the openstack-ansible playbook command, 
* specifying the playbook that corresponded to the service we wanted to change.
*  As we were making configuration changes, we notified Ansible of this through the --tag parameter.


# Configuration de nova

1. Editer le fichier `/etc/openstack_deploy/user_variables.yml`
2. Appliquer la modif
```console
openstack-ansible os-nova-install.yml --tags 'nova-config'
```
## Modification simple
Exemple de variables :
* Ratio de cpu : `nova_cpu_allocation_ratio: 8.0`

## Modification complexe

Ajouter ou modifier la section  `nova_nova_conf_overrides` dans le fichier `/etc/openstack_deploy/user_variables.yml`

Exemple :
```console
nova_nova_conf_overrides:  
  DEFAULT:
   quota_fixed_ips = -1
   quota_floating_ips = 20
   quota_instances = 20
```

# Configuration de neutron

1. Editer le fichier `/etc/openstack_deploy/user_variables.yml`
2. Appliquer la modif
```console
openstack-ansible os-neutron-install.yml --tag 'neutron-config'
```
## Modification simple
Exemple de variables :
*  : ``

## Modification complexe

Ajouter ou modifier la section  `neutron_neutron_conf_overrides` dans le fichier `/etc/openstack_deploy/user_variables.yml`

Exemple, pour modifier le fichier neutron.conf :
```console
neutron_neutron_conf_overrides:  
  DEFAULT:
   dhcp_lease_duration = -1
```
