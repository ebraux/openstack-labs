
* * *

*   configurer des propriétés sur un gabarit

```
openstack flavor set m1.large.2xK80 --property pci_passthrough:alias='K80_Tesla:2'
openstack flavor set vgpu_1                 --property "resources:VGPU=1"
```