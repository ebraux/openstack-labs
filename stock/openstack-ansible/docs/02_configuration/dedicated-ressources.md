# Limitation d el'accès à un computeRéservation de compute


## objectif

Dédier des ressouces (compute, ...) à un service/projet/utilisateur ...

## Principe 

Cas d'usage, utilisation d'instances avec GPU : 

- il faut que les instances soient déployées sur des compute avec des GPU.
- il ne faut pas que ces compute soient monopolisés par des instances qui n'utilisent pas de GPU.

Pour traiter ce cas d'usage, on va travailler avec le "scheduler" de Nova, qui est en charge de choisir les compute sur lesquels déployer les instance.

Plutôt que de travailler sur un compte, il est préférable de travailler avec un aggregat, ce qui permet ensuite d'ajouter/supprimer des compute en fonction des besoins, sans avoir à modifier leur configuration.


Pour limiter le déploiement d'une instance à un aggrégat, on utilise un gabarit, configuré de telle sorte qu'il ne puisse s'executer que sur cet aggrégat. 

Openstack ne propose pas d'option pour empécher les instances lancées avec un autres gabarit d'être déployées sur l'aggrégat. Il faut travailler au niveau "Project", et limiter l'utilisation d'un aggrégat à un ou plusieurs projets spécifiques. Il suffit de créer un gabarit privé, et de l'autoriser uniquement pour les projets concernés.

Avec cette configuration : 

- le projet sera le seul à pouvoir utiliser le gabarit, et donc l'aggrégat,
- le projet pourra déployer des instances sur tous les compute en utilisant les autres gabarits.



## Configuration du Nova Scheduler pour gérer les aggregats

Pour pouvoir lancer des instance sur un aggrégat spécifique, il faut définir une property spécifique au niveau du gabarit et de l'aggregat.  Pour que cette property soit prise en compte par le Nova scheduler,il faut que le filtre `AggregateInstanceExtraSpecsFilter` soit actif.

Pour pouvoir limiter un aggrégat à un projet (ou plusieurs), il faut définir une property `filter_tenant_id` avec l'iD du projet au niveau de l'aggregat. Pour que cette property soit prise en compte par le Nova scheduler,il faut que le filtre `AggregateMultiTenancyIsolation` soit actif.


Modification de la configuration du "filter", au niveau du scheduler de Nova : ajout de `AggregateMultiTenancyIsolation`  et `AggregateInstanceExtraSpecsFilter`
``` yaml
nova_scheduler_default_filters: "RetryFilter,AvailabilityZoneFilter,RamFilter,ComputeFilter,ComputeCapabilitiesFilter,ImagePropertiesFilter,ServerGroupAntiAffinityFilter,ServerGroupAffinityFilter,AggregateCoreFilter,AggregateDiskFilter,AggregateInstanceExtraSpecsFilter,AggregateMultiTenancyIsolation,PciPassthroughFilter"
```

Sources :

- [http://egonzalez.org/openstack-segregation-with-availability-zones-and-host-aggregates/](http://egonzalez.org/openstack-segregation-with-availability-zones-and-host-aggregates/)
-  AggregateMultiTenancyIsolation [https://docs.openstack.org/newton/config-reference/compute/schedulers.html]([https://docs.openstack.org/newton/config-reference/compute/schedulers.html) : 
      -  Ensures that the tenant (or list of tenants) creates all instances only on specific Host aggregates and availability zones.
      -  If a host is in an aggregate that has the filter_tenant_id metadata key, the host creates instances from only that tenant or list of tenants.
      -  A host can be in different aggregates.
      -  If a host does not belong to an aggregate with the metadata key, the host can create instances from all tenants.
      -  This setting does not isolate the aggregate from other tenants. Any other tenant can continue to build instances on the specified aggregate.
 

## Configuration de l'aggregat

- Create a host aggregate:
``` bash
openstack aggregate create toto-dedicated
openstack aggregate add host toto-dedicated os-compute-xx
```
* Add a key=value pair to the host aggregate metadata – we’ll use this to match against later.
``` bash

openstack aggregate set toto-dedicated --property filter_tenant_id=d999ea47a245432eaf09542e808099f0
openstack aggregate show  toto-dedicated

+-------------------+------------------------------------------------------------------------------+
| Field             | Value                                                                        |
+-------------------+------------------------------------------------------------------------------+
| availability_zone | None                                                                         |
| created_at        | 2019-05-31T09:43:16.000000                                                   |
| deleted           | False                                                                        |
| deleted_at        | None                                                                         |
| hosts             | [u'os-compute-xx']                                                           |
| id                | 4                                                                            |
| name              | decide-dedicated                                                             |
| properties        | filter_tenant_id='d999ea47a245432eaf09542e808099f0' |
| updated_at        | None                                                                         |
+-------------------+------------------------------------------------------------------------------+
```

Autre option, pas sur que ça marche et que ça serve :  Dédier un aggregat à un projet
* create a dedicated aggregate for this tenant: 
``` bash
nova aggregate-set-metadata <aggregate_id> project_id=<tenant_id>
```

## Création d'un gabarit spécifique

On utilise la possibilité d'ajouter des propriétes à un gabarit, associé au mécanisme de filtre du scheduler de nova.

POur le projet 'toto'

- création du gabarit `toto-gabarit`, et affectation au projet `toto` :
``` bash
openstack flavor create --private  --ram 15360 --disk 15 --vcpus 20 toto-gabarit
openstack flavor set --project toto toto-gabarit
```
- récupérer l'id du projet
``` bash
openstack project show toto -f value -c id
---
d999ea47a245432eaf09542e808099f0
```
- configurer des propriétés sur le gabarit
``` bash
openstack flavor set toto-gabarit --property "aggregate_instance_extra_specs:filter_tenant_id=d999ea47a245432eaf09542e808099f0" 
```

```console
openstack flavor show toto-gabarit
+----------------------------+---------------------------------------------------------------------------------------+
| Field                      | Value                                                                                 |
+----------------------------+---------------------------------------------------------------------------------------+
| OS-FLV-DISABLED:disabled   | False                                                                                 |
| OS-FLV-EXT-DATA:ephemeral  | 0                                                                                     |
| access_project_ids         | d999ea47a245432eaf09542e808099f0                                                      |
| disk                       | 15                                                                                    |
| id                         | b0b4f490-6474-4725-8c02-da0560938242                                                  |
| name                       | decide-dedicated                                                                      |
| os-flavor-access:is_public | False                                                                                 |
| properties                 |  aggregate_instance_extra_specs:filter_tenant_id=''d999ea47a245432b9f09542e808099f0'' |
| ram                        | 20480                                                                                 |
| rxtx_factor                | 1.0                                                                                   |
| swap                       |                                                                                       |
| vcpus                      | 20                                                                                    |
+----------------------------+---------------------------------------------------------------------------------------+
```



