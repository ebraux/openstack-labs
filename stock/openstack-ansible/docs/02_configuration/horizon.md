

* [https://docs.openstack.org/releasenotes/openstack-ansible/rocky.html#relnotes-18-1-15-stable-rocky](https://docs.openstack.org/releasenotes/openstack-ansible/rocky.html#relnotes-18-1-15-stable-rocky)

```bash
Add the possibility to disable openrc v2 download in the dashboard. new var horizon_show_keystone_v2_rc can be set to False to remove the entry for the openrc v2 download.

```