title: Préparation de noeuds

Objectifs

L'installation physique minimume des machine sous Ubuntu est effectuée via les mécanismes habituels (boot pxe + configuration kickstart)

Il est ensuite necessaire de les préparer pour intégrer l'infrastructure Opensatck : installation des prérequis, configuration réseau

On utilise des scripts ansible.

Configuration réseau

* Remplacement de netplan par ifup-ifdown
* Mise en place des bridge, et des accès au différents Vlan