title: Mise à jour



# gestion de version

par dfaut, avec le git clone en stable/Rocky
git branch
/opt/ansible-runtime/bin/python setup.py --version


Si ensuite in fait un
```bash
git checkout 18.1.18
git branch
* (HEAD detached at 18.1.18)
  stable/rocky

/opt/ansible-runtime/bin/python setup.py --version
18.1.18
```

```bash
git checkout master
git branch
* master
  stable/rocky
/opt/ansible-runtime/bin/python setup.py --version
18.1.19.dev2
```


A creuser !!!



## Mise à jour Mineure


verifier la version :
```bash
 cat /etc/openstack-release
```

au 27/02/2020 : version 18.1.19.dev2, version la plus récente stable de Rocky : 18.1.18

* [https://github.com/openstack/openstack-ansible/releases/tag/18.1.18](https://github.com/openstack/openstack-ansible/releases/tag/18.1.18)


Change directory to the cloned repository’s root directory:
```bash
cd /opt/openstack-ansible
```
Verifier s Vresion
```bash
/opt/ansible-runtime/bin/python setup.py --version
```

Ensure that your OpenStack-Ansible code is on the latest Rocky tagged release:
```bash
git checkout 18.1.18
```

Update all the dependent roles to the latest version:
```bash
./scripts/bootstrap-ansible.sh
```

Change to the playbooks directory:
```bash
cd playbooks
```

Update the hosts
```bash
openstack-ansible setup-hosts.yml
```

Update the infrastructure
```bash
openstack-ansible -e rabbitmq_upgrade=true setup-infrastructure.yml
```

Update all OpenStack services:
```bash
openstack-ansible setup-openstack.yml
```


source :
* [https://docs.openstack.org/openstack-ansible/rocky/admin/upgrades/minor-updates.html](https://docs.openstack.org/openstack-ansible/rocky/admin/upgrades/minor-updates.html)