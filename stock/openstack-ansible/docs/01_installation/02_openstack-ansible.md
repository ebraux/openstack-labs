title: installation d'opensatck Ansible

# Installation


```bash
git clone -b stable/rocky  https://git.openstack.org/openstack/openstack-ansible /opt/openstack-ansible

cd /opt/openstack-ansible
git branch
  * stable/rocky



sources :
* [https://docs.openstack.org/project-deploy-guide/openstack-ansible/ocata/deploymenthost.html](https://docs.openstack.org/project-deploy-guide/openstack-ansible/ocata/deploymenthost.html)

cd /opt/openstack-ansible

scripts/bootstrap-ansible.sh
```

```bash
/opt/ansible-runtime/bin/python setup.py --version
18.1.19.dev2

```

Configuration et déploiement

    Copier la configuration par défaut et générer les mots de passe

```bash
        cp -a etc/openstack_deploy/ /etc/
        configure everything ...
        cd /opt/openstack-ansible/scripts
        python pw-token-gen.py --file /etc/openstack_deploy/user_secrets.yml

```
    Configurer les fichiers `openstack_user_config.yml` et `user_variables.yml`
    Lancer le déploiement

```bash

        cd /opt/openstack-ansible/playbooks
        openstack-ansible setup-everything.yml

```


# Customisation

## Customisation du role gnocchi

Dans la version Rocky/stable, Gnocchi fonctionne avec apache + uwsgi. Mais le playbook ne prend pas en charge l'installation de uwsgi, c'ets un bug non corrigé. Dans les versions ultérieures, apache n'est plus necessaire, et uwsgi est directement installé dans le venv, et les script systemd sont adaptés.

la version d'uwsgi librée par défaut avec Ubuntu18 n'est pas compatible avec Gnocchi. Il faut l'installe via PIP.
```bash
apt install python-pip
pip install uwsgi
uwsgi --version
  2.0.18
```

Le role gnocchi doit être modifié pour prendre en charge cette installation. chemin du rôke : `:/etc/ansible/roles/os_gnocchi`

Ajout de python-pip à la liste des packages à installer dans le `vars/ubuntu.yml` :
```yaml
#: Necessary packages
gnocchi_distro_packages:
  - apache2
  - apache2-utils
  - build-essential
  - git
  - libapache2-mod-wsgi
  - libpq-dev
  - librados-dev
  - python-dev
  - python-pip
```

Ajout d'une tache d'installation de uwsgi via pip dans `tasks/gnocchi_install.yml`, à placer après la tache "Install distro packages" pour que pip soit disponible :
```yaml
- name: Install system pip packages
  pip:
    name:
      - uwsgi
    state: "{{ gnocchi_package_state }}"
    extra_args: >-
      {{ pip_install_options | default('') }}
    register: install_packages
    until: install_packages is success
    retries: 5
    delay: 2
```


