title: Ansible-ops



# Ansible-ops

Ensemble de playbooks Ansible apportant des fonctionnalités de gestion d'une plateforme Openstack installée avec opensack-ansible: 

Installation :
```bash
git clone https://git.openstack.org/openstack/openstack-ansible-ops   /opt/openstack-ansible-ops
cd /opt/openstack-ansible-ops/ansible_tools/playbooks
```
