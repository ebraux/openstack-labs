
# --- Galera ---

root@os-ctrl-04:/opt/openstack-ansible/playbooks

ansible galera_container -m shell -a "mysql -h localhost -e 'show status like \"%wsrep_cluster_%\";'"

ansible galera_container -m shell -a "mysql -h localhost -e 'show status like \"wsrep_local_recv_queue_avg\";'"

ansible galera_container -m shell -a "mysql -h localhost -e 'flush status;'"
