

## Changer le status "in-use" d'un volume

en acs de plantage d'une instance, un volume peut rester "in-use", bien que l'instance ai été détruite.

Pour changer le stataus d'un volume

Avec la commande Openstack

```bash
openstack volume set --state available <UUID>
cinder reset-state --attach-status detached <UUID>
```

La même chose avec la commande "cinder" :

```bash
cinder reset-state --state available <UUID>
cinder reset-state --attach-status detached <UUID>
```

La même chose directement dans la base de données :

```sql
mysql> use cinder;

mysql>update volumes set attach_status='detached',status='available' where id ='$volume_uuid';

#ou meme

mysql>update volumes set attach_status='detached',instance_uuid=NULL,attach_time=NULL,status="available" WHERE id='volume_uuid';
```

Sources :

* [https://ask.openstack.org/en/question/66918/how-to-delete-volume-with-available-status-and-attached-to/](https://ask.openstack.org/en/question/66918/how-to-delete-volume-with-available-status-and-attached-to/)


Si le volume reste attaché au niveau de l'instance, il faut également agir sur nova

```bash
delete from block_device_mapping where volume_id='8329bf61-61d2-4f39-bb7a-7995e96c4b04';
Query OK, 1 row affected (0.00 sec)
```

Attention, avant de supprimer le volume, vérifier qu'il n'est plus dan sla configuartion d el'instance.
* se connecte au compute
* editer le fihier de config de l'instance
```bash
virsh edit instance-0000079b
```
* regardre le contenu de la partie  "device" : dans l'exemple ci dessous, un volume CEPH, monté sur /dev/vdb est toujours présent
```bash
 <devices>
   ...
    <disk type='file' device='disk'>
      <driver name='qemu' type='qcow2' cache='none' discard='ignore'/>
      <source file='/var/lib/nova/instances/adead1df-041b-4e0a-823a-48a2ab664f33/disk'/>
      <target dev='vda' bus='virtio'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x05' function='0x0'/>
    </disk>
    <disk type='network' device='disk'>
      <driver name='qemu' type='raw' cache='none' discard='unmap'/>
      <auth username='cinder'>
        <secret type='ceph' uuid='4fb657a5-9634-4f19-bc47-a71c326f30ae'/>
      </auth>
      <source protocol='rbd' name='volumes/volume-8329bf61-61d2-4f39-bb7a-7995e96c4b04'>
        <host name='192.168.74.24' port='6789'/>
        <host name='192.168.74.25' port='6789'/>
        <host name='192.168.74.26' port='6789'/>
      </source>
      <target dev='vdb' bus='virtio'/>
      <serial>8329bf61-61d2-4f39-bb7a-7995e96c4b04</serial>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x0'/>
    </disk>
   ...
```

Ne rebooetr que s tout est bon, sinon l'instance sera ensuite inulilisable.

Docs : 
* https://raymii.org/s/articles/Fix_inconsistent_Openstack_volumes_and_instances_from_Cinder_and_Nova_via_the_database.html#toc_2