title: rabbitMQ


Config Timezone

```
echo "Europe/Paris" > /etc/timezone
ls -l /etc/timezone 
dpkg-reconfigure tzdata
apt-get update 
apt-get install tzdata
date 
```

# architecture
Rabbit MQ tourne sur chaque controlleur.
Dans des containers LXC, avec une IP dédiée
* os-ctrl-04_rabbit_mq_container-891b027d : 192.168.73.232
* os-ctrl-05_rabbit_mq_container-e94d2dfb : 192.168.73.65
* os-ctrl-06_rabbit_mq_container-209a4ef0 : 192.168.73.74

# Utilisation du dashboard  /manager

Le manager est activé sur chacun des neoud du cluster rabbit MQ. il ermet de gérer l'ensemeble du cluster
il est accessible sur l'IP du container LXC, il faut donc mettre en place un tunnel ssh pour y accèder.
exemeple
```
ssh os-admin -L 15674:localhost:15674
ssh os-ctrl-04.priv -L 15674:192.168.73.232:15672
```
L'interface de management est désormai accessible via http://localhost:15674
manager[hyterfafcFR56]





# redémarrage

il faut le faire pour les 3 puis il faut redémarrer les rabbitmq en
commençant par le dernier arrêté 

* arrêter : finir par le 04
```
ansible os-ctrl-06_rabbit_mq_container-209a4ef0 -m shell -a "rabbitmqctl stop_app"
ansible os-ctrl-05_rabbit_mq_container-e94d2dfb -m shell -a "rabbitmqctl stop_app"
ansible os-ctrl-04_rabbit_mq_container-891b027d -m shell -a "rabbitmqctl stop_app"
```
* relancer : commencer par le 04
```
ansible os-ctrl-04_rabbit_mq_container-891b027d -m shell -a "rabbitmqctl start_app"
ansible os-ctrl-05_rabbit_mq_container-e94d2dfb -m shell -a "rabbitmqctl start_app"
ansible os-ctrl-06_rabbit_mq_container-209a4ef0 -m shell -a "rabbitmqctl start_app"
```


on peut faire un rabbitmqctl cluster_status pour voir l'état du cluster

on peut également vérifier si les neutron agent sont de nouveau
accessible via openstack neutron agent list

# Connexion en ligne de commande
```
ssh os-ctrl-04.priv.enst-bretagne.fr
latt rabb
# (ou lxc-attach os-ctrl-04-rabbit-mq-container-891b027d)
```


# Inspection

* voir les logs : sur le serveur de centralisation des logs
```
tail -f /openstack/os-ctrl-04_rsyslog_container-c36e9c3f/log-storage/*/*log | grep ERR
```
* sur le container Rabbimq
```
tail -f /var/log/rabbitmq/*log
```

* infos sur l'instance : version, les process, utilisation ressources, mémoire, disque ... 
```
rabbitmqctl status
...
{running_applications,
     [{rabbitmq_management,"RabbitMQ Management Console","3.7.7"},
      {rabbitmq_management_agent,"RabbitMQ Management Agent","3.7.7"},
...
 {memory,
     [{connection_readers,2291976},
      {connection_writers,104832},
      {connection_channels,2817384},
      {connection_other,7440688},
      {queue_procs,620640},
...
```
* info sur l'état du cluster : nom du cluster, noeuds, ....
```
rabbitmqctl cluster_status
Cluster status of node rabbit@os-ctrl-04-rabbit-mq-container-891b027d ...
[{nodes,[{disc,['rabbit@os-ctrl-04-rabbit-mq-container-891b027d',
                'rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb',
                'rabbit@os-ctrl-06-rabbit-mq-container-209a4ef0']}]},
 {running_nodes,['rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb',
                 'rabbit@os-ctrl-06-rabbit-mq-container-209a4ef0',
                 'rabbit@os-ctrl-04-rabbit-mq-container-891b027d']},
 {cluster_name,<<"openstack">>},
 {partitions,[]},
 {alarms,[{'rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb',[]},
          {'rabbit@os-ctrl-06-rabbit-mq-container-209a4ef0',[]},
          {'rabbit@os-ctrl-04-rabbit-mq-container-891b027d',[]}]}]
```

* lister les vhosts
```
rabbitmqctl list_vhosts
Listing vhosts ...
/neutron
/designate
/heat
/magnum
/keystone
/aodh
/
/cinder
/nova
/glance
/ceilometer
```

* lister les queues
```
rabbitmqctl list_queues  -p /keystone
Timeout: 60.0 seconds ...
Listing queues for vhost /keystone ...
notifications.info	6
```
* lister les consumers des queues
```
rabbitmqctl list_queues consumers name -p /neutron
```
* liste des messages
rabbitmqctl list_queues messages consumers name -p /keystone
Timeout: 60.0 seconds ...
Listing queues for vhost /keystone ...
6	0	notifications.info

* lister les queues par vhosts
```
for TOTO in `rabbitmqctl list_vhosts -q `; do echo -n "$TOTO : "; rabbitmqctl list_queues  -q -p $TOTO | wc -l; done
/neutron : 686
/designate : 0
/heat : 102
/magnum : 0
/keystone : 1
/aodh : 0
/ : 0
/cinder : 24
/nova : 178
/glance : 1
/ceilometer : 7
```
* afficher les messages en attente
```
rabbitmqctl list_queues messages consumers name -p /neutron | sort -n
...
rem :
Le premier element est le nombre de messages en attente, le deuxième le nombre de consumers.
If a queue has consumers, but also messages are accumulating there, it means that the corresponding service can not process mesages in time or got stuck in a deadlock or cluster is partitioned.
Pour vérifier le partitionnement ou nom du cluster `rabbitmqctl cluster_status`
```

* lister les queues par host
On se connecte sur le os-ctrl-04, et on utilise l'option "--local"
```
root@os-ctrl-04:/opt/openstack-ansible#ansible rabbitmq_all  -m shell -a "rabbitmqctl --local  list_queues name -q -p /nova | wc -l"

os-ctrl-06_rabbit_mq_container-209a4ef0 | SUCCESS | rc=0 >>
16

os-ctrl-05_rabbit_mq_container-e94d2dfb | SUCCESS | rc=0 >>
194

os-ctrl-04_rabbit_mq_container-891b027d | SUCCESS | rc=0 >>
15


```

## expiration des queues
géré via les policies
par défaut 'amqp_auto_delete=true', mais c'est mal. il faut fixer des "queues expiration policies" avec un TTL d'au moins 1 minute.
[TODO] : comment on a cette info
[TODO] : comment on modifie ce parametre

* consommation mémoire des queues
```
rabbitmqctl list_queues memory name -p /neutron | sort -n
202808	neutron-vo-Port-1.4_fanout_becf5f64b2d441f6bbcc3b9d188d7c0f
204776	q-reports-plugin
728040	notifications.critical
728040	notifications.debug
728040	notifications.sample
728040	notifications.warn
739488	notifications.info
1153608	notifications.audit
1153608	notifications.error  (1.1MB)
...

```

# Vérifier les connections

```
rabbitmqctl list_connections | wc -l
1885
```

```
   48  ss -tanpu
   49  ss -tanpu |wc -l 
ansible rabbitmq_all  -m shell -a "ss -tanpu |wc -l"
Variable files: "-e @/etc/openstack_deploy/user_secrets.yml -e @/etc/openstack_deploy/user_variables.yml "
 [WARNING]: Unable to parse /etc/openstack_deploy/inventory.ini as an inventory source

os-ctrl-06_rabbit_mq_container-209a4ef0 | SUCCESS | rc=0 >>
260

os-ctrl-05_rabbit_mq_container-e94d2dfb | SUCCESS | rc=0 >>
1884

os-ctrl-04_rabbit_mq_container-891b027d | SUCCESS | rc=0 >>
242

```
# Vérifier les channels

```
rabbitmqctl list_channels | wc -l
1777
```


# Inspect processes
```
# rabbitmqctl eval "rabbit_diagnostics:maybe_stuck()."
2019-11-07 20:27:49 There are 3032 processes.
2019-11-07 20:27:49 Investigated 1 processes this round, 5000ms to go.
2019-11-07 20:27:50 Investigated 1 processes this round, 4500ms to go.
2019-11-07 20:27:50 Investigated 1 processes this round, 4000ms to go.
2019-11-07 20:27:51 Investigated 1 processes this round, 3500ms to go.
2019-11-07 20:27:51 Investigated 1 processes this round, 3000ms to go.
2019-11-07 20:27:52 Investigated 1 processes this round, 2500ms to go.
2019-11-07 20:27:52 Investigated 1 processes this round, 2000ms to go.
2019-11-07 20:27:53 Investigated 1 processes this round, 1500ms to go.
2019-11-07 20:27:53 Investigated 1 processes this round, 1000ms to go.
2019-11-07 20:27:54 Investigated 1 processes this round, 500ms to go.
2019-11-07 20:27:54 Found 1 suspicious processes.
2019-11-07 20:27:54 [{pid,<9336.1.0>},
                     {registered_name,erts_code_purger},
                     {current_stacktrace,
                         [{erts_code_purger,wait_for_request,0,[]}]},
                     {initial_call,{erts_code_purger,start,0}},
                     {message_queue_len,0},
                     {links,[]},
                     {monitors,[]},
                     {monitored_by,[]},
                     {heap_size,987}]

```
```
from the output of previous command identify lines like:

[{pid,<5490.58.0>},
you see the word pid followed by three numbers

forget about the first number and execute (note that dots have been replaced by commas):

rabbitmqctl eval 'erlang:exit(c:pid(0,58,0),kill).'

```
* [https://cloud.garr.it/support/kb/openstack/rabbitmq_debug/]


# Info sur l'état de la base de statistiques
* consomation mémoire
```
rabbitmqctl status | grep mgmt_db
      {mgmt_db,159574424},
```
* reset
```
# par node
rabbitmqctl eval 'rabbit_mgmt_storage:reset().'
# pour le cluster 
rabbitmqctl eval 'rabbit_mgmt_storage:reset_all().
```

# tests  /évaluation
* https://www.cloudamqp.com/blog/2016-06-21-how-to-delete-queues-in-rabbitmq.html
* https://cloud.garr.it/support/kb/openstack/rabbitmq/
* https://www.cloudamqp.com/plans.html




* https://docs.openstack.org/nova/latest/reference/notifications.html


    list_bindings [-p <vhost>] [<bindinginfoitem> ...] [-t <timeout>]
    list_consumers [-p vhost] [<consumerinfoitem> ...] [-t <timeout>]
    list_exchanges [-p <vhost>] [<exchangeinfoitem> ...] [-t <timeout>]
    list_policies [-p <vhost>] [-t <timeout>]


# pistes
* Reduce TCP buffer size (Rabbit Networking Guide)
* force per-connection channel limit  sue rabbit.channel_max 

# Getsion des users et des droits

```
list_permissions [-p <vhost>] [-t <timeout>]
```


# ---

```
rabbitmqctl stop_app 
rabbitmqctl start_app



for i in `rabbitmqctl list_vhosts`; do rabbitmqctl list_queues -p $i |awk '{print $2" "$i}'; done
systemctl stop rabbitmq-server
systemctl start  rabbitmq-server

ss -taupn 

rabbitmqctl -n rabbit@os-ctrl-04-rabbit-mq-container-891b027d forget-cluster-node rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb
 
rabbitmqctl -n rabbit@os-ctrl-04-rabbit-mq-container-891b027d forget_cluster_node rabbit@os-ctrl-05-rabbit-mq-container-e94d2dfb

rabbitmqctl -n rabbit@os-ctrl-04-rabbit-mq-container-891b027d forget_cluster_node rabbit@os-ctrl-06-rabbit-mq-container-209a4ef0


   22  cd /var/log
   23  cd rabbitmq/
   24  ll
   25  ls -ltr
   26  vim rabbit\@os-ctrl-04-rabbit-mq-container-891b027d.log
   27  cat rabbit\@os-ctrl-04-rabbit-mq-container-891b027d.log
   28  rabbitmqctl cluster_status
   29  rabbitmqctl list_connections
   30  exit
   31  rabbitmqctl list_queues consumers name
   32  rabbitmqctl list_queues 
   33  rabbitmqctl help | grep consumer
   34  rabbitmqctl list-consumers
   35  rabbitmqctl list_consumers
   36  rabbitmqctl list_consumers -p /neutron
   37  rabbitmqctl list_queues consumers -p neutron
   38  rabbitmqctl list_queues consumers -p /neutron
   39  rabbitmqctl list_queues consumers name -p /neutron
   40  rabbitmqctl list_queues messages consumers name -p /neutron
   41  rabbitmqctl cluster_status
   42  rabbitmqctl status
   43  rabbitmqctl list_queues name messages memory consummers -p /neutron
   44  rabbitmqctl list_queues name messages memory consumers -p /neutron
   45  rabbitmqctl list_connections | wc -l 
   46  rabbitmq_top
   47  rabbitmqctl list_channels | wc -l 

   50  rabbitmqctl trace_on -p /neutron
   51  rabbitmqctl trace_off -p /neutron
   52  apt install tcpdump 
   53  tcpdump -ni any port 5671
   54  tcpdump -nnvvXSs 1514 -i any port 5671
   55  tcpdump -i any port 5671
   56  exit
   57  systemctl restart rabbi*
   58  rabbitmqctl list_channels | wc -l 
   59  rabbitmqctl status
   60  rabbitmqctl cluster_status
   61  ps -ef
   62  cd /etc/systemd/system
   63  ll
   64  cd /var/log
   65  ll
   66  cd rabbitmq/
   67  ll
   68  ls -ltr
   69  vim rabbit\@os-ctrl-04-rabbit-mq-container-891b027d.log
   70  apt install vim
   71  vim rabbit\@os-ctrl-04-rabbit-mq-container-891b027d.log
   72  vim /etc/rabbitmq/rabbitmq.config 

   77  reboot 
   78  rabbitmqctl status
   79  rabbitmqctl list_queues
 
   88  exit 
   89  cd /var/log
   90  cd rabbitmq/
   91  ll
   92  tail -f rabbit\@os-ctrl-04-rabbit-mq-container-891b027d.log
   93  rabbitmqctl status
   94  vim /etc/rabbitmq/rabbitmq.config 
   95  ss -natlp
   96  exit
   97  rabbitmqctl add_user telegraf telegraf
   98  rabbitmqctl set_permission telegraf ".*" ".*" ".*"
   99  rabbitmqctl set_permissions telegraf ".*" ".*" ".*"
  100  exit
  101  rabbitmqctl --help 
  102  rabbitmqctl list_users
  103  rabbitmqctl list_user_permissions telegraf 
  104  rabbitmqctl list_user_permissions datadog
  105  history | grep user
  106  exit
  107  rabbitmqctl --help 
  108  rabbitmqctl delete_user telegraf 
  109  exit
  110  history 
  111  rabbitmqctl cluster_status
  112  rabbitmqctl list_channels | wc -l
  113  rabbitmqctl list_queues consumers -p neutron
  114  rabbitmqctl list_queues consumers -p nova
  115  rabbitmqctl list_consumers
  116  rabbitmqctl list_queues consumers name
  117  rabbitmqctl list_queues name messages memory consumers -p /neutron
  118  df -h 
  119  rabbitmqctl list_queues consumers -p neutron
  120  rabbitmqctl list_queues name messages memory consumers -p /neutron
  121  history | grep cluster
  122  rabbitmqctl status
  123  rabbitmqctl list_queues
  124  exit
## gestion des plugins
```
rabbitmq-plugins enable rabbitmq_management 


```
  128  netstat -ltunp
  129  apt install netstat
  130  apt install net-utils
  131  apt install netutils
  132  apt install net-tools
  133  netstat -ltunp
  134  history


```
## Gestion des utilisateurs

nova:06b2a6a6232a6b099a554c6f0238e11b6daba244490ed120e76
```
rabbitmqctl add_user datadog RTgHYUYT45yt67HT
rabbitmqctl set_permissions  -p / datadog "^aliveness-test$" "^amq\.default$" ".*"
rabbitmqctl set_user_tags datadog monitoring


rabbitmqctl add_user manager hyterfafcFR56
rabbitmqctl set_user_tags manager administrator
rabbitmqctl set_permissions -p / manager ".*" ".*" ".*"
```

# sources :
* [https://fr.slideshare.net/michaelklishin/troubleshooting-common-oslomessaging-and-rabbitmq-issues] slide 33, c'est moi à 3h du matin
* [https://www.rabbitmq.com/management.html]