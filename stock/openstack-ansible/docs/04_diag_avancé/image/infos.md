

## Docs

* [http://people.csail.mit.edu/jon/openstack-ops/content/snapshots.html](http://people.csail.mit.edu/jon/openstack-ops/content/snapshots.html)

## Infos techniques, cohérence des données

* [https://www.ovh.com/blog/create-and-use-openstack-snapshots/#:~:text=Once%20you're%20logged%20in,when%20creating%20a%20new%20instance](https://www.ovh.com/blog/create-and-use-openstack-snapshots/#:~:text=Once%20you're%20logged%20in,when%20creating%20a%20new%20instance.)


## import/export

* [https://docs.openstack.org/mitaka/user-guide/cli_use_snapshots_to_migrate_instances.html](https://docs.openstack.org/mitaka/user-guide/cli_use_snapshots_to_migrate_instances.html)



## snapshot vs images

* [https://serverfault.com/questions/527449/why-does-openstack-distinguish-images-from-snapshots](https://serverfault.com/questions/527449/why-does-openstack-distinguish-images-from-snapshots)