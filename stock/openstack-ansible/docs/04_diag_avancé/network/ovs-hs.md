


# Pb de timeout OVS sur un des agents 

Ménage dans la base OVS, neutron recréara tout au prochain démarrage de l'agent. Le timeout est de 600secondes, il faut dans certains cas rela,cer la commande plusieurs fois avanat que la commande ne retourne pas d'erreur; Il vaut mieux égalemnt arrêter les services
```bash
latt neutron_a

systemctl stop neutron-dhcp-agent.service neutron-metadata-agent.service neutron-l3-agent.service neutron-openvswitch-agent.service

source /openstack/venvs/neutron-18.1.19.dev2/bin/activate
neutron-ovs-cleanup
  2020-02-27 16:16:40.006 43224 INFO neutron.common.config [-] Logging enabled!
  2020-02-27 16:16:40.007 43224 INFO neutron.common.config [-] /openstack/venvs/neutron-18.1.19.dev2/bin/neutron-ovs-cleanup version 13.0.7.dev2
  2020-02-27 16:16:40.507 43224 INFO neutron.cmd.ovs_cleanup [-] Cleaning bridge: br-int
  ...

systemctl start   neutron-openvswitch-agent.service  neutron-l3-agent.service neutron-metadata-agent.service neutron-dhcp-agent.service

```
