title: Ceph




Config :
serveurs CEPH :
https://os-ctrl-04.priv:8443
https://os-ctrl-05.priv:8443
https://os-ctrl-06.priv:8443


https://os-ctrl-04.priv.enst-bretagne.fr:8443/
https://os-ctrl-05.priv:8443
https://os-ctrl-06.priv:8443



# Interface de tableau de bord 

Aller c'est cadeau je t'ai activé la dashboard de ceph :
https://os-ctrl-04.priv:8443
https://os-ctrl-05.priv:8443
https://os-ctrl-06.priv:8443
login: imtuser
password: ve150219

Les commandes pour activer la dashboard (sur os-ctrl-04):
ceph mgr module enable dashboard
ceph dashboard create-self-signed-cert
ceph dashboard set-login-credentials <username> <password>



# openstack/ CEPH
Accès en RDB (mode Bloc)

Mimic en mode bluestore, mais pas LVM mais mode disque
 -> ceph ansible ne savait pas gérer le LVM

non colocated : les fichiers journaux sont sur des SSD séparés.

Si on utilisait pour les compute, il faudrait que libvirt sache discuter avce CEPH, et donc utiliser RDB

# vérification de la configuartion des noeuds

```console
ceph -s
ceph -w

ceph osd pool ls
rdb -p images ls
rdb -p volumes ls
```


## vérifcation de la configuration réseau

* les interfaces réseau (après re-cablage)
```console
for i in eno1np0 eno3 eno2np1 eno4; do echo "===$i==="; ethtool $i | grep Speed;done
```

* l'état des interface bond
```console
cat /proc/net/bonding/bond0
=> tu dois avoir deux interfaces dont une en active slave
```


## vérification le bon fonctionnement de CEPH

Sur les serveur CEPH

* vérifier que tous les daemons CEPH tournent
```console
systemctl status ceph-osd*
  (est en mode failure pour l'instan, sur des ancienne info à nettoyer)
```

* vérifier l'état des OSD
Depuis un "client" CEPH, os-ctrl-04 par exemple 

```console
ceph osd tree
  -> liste les OSD sur les diffrents neouds, et leur état
```
exemple de résultat au 12/04/2019

```console
ceph osd tree
 ID CLASS WEIGHT    TYPE NAME           STATUS REWEIGHT PRI-AFF
-1       133.24370 root default
-3        44.41457     host os-ceph-01
 0   hdd   7.30659         osd.0           up  1.00000 1.00000
 3   hdd   7.30659         osd.3           up  1.00000 1.00000
 6   hdd   7.30659         osd.6           up  1.00000 1.00000
11   hdd   7.30659         osd.11          up  1.00000 1.00000
14   hdd   7.30659         osd.14          up  1.00000 1.00000
17   hdd   0.57500         osd.17          up  1.00000 1.00000
20   hdd   7.30659         osd.20          up  1.00000 1.00000
-5        44.41457     host os-ceph-02
 1   hdd   7.30659         osd.1           up  1.00000 1.00000
 4   hdd   7.30659         osd.4           up  1.00000 1.00000
 7   hdd   7.30659         osd.7           up  1.00000 1.00000
 9   hdd   7.30659         osd.9           up  1.00000 1.00000
12   hdd   7.30659         osd.12          up  1.00000 1.00000
15   hdd   7.30659         osd.15          up  1.00000 1.00000
18   hdd   0.57500         osd.18          up  1.00000 1.00000
-7        44.41457     host os-ceph-03
 2   hdd   7.30659         osd.2           up  1.00000 1.00000
 5   hdd   7.30659         osd.5           up  1.00000 1.00000
 8   hdd   7.30659         osd.8           up  1.00000 1.00000
10   hdd   7.30659         osd.10          up  1.00000 1.00000
13   hdd   7.30659         osd.13          up  1.00000 1.00000
16   hdd   7.30659         osd.16          up  1.00000 1.00000
19   hdd   0.57500         osd.19          up  1.00000 1.00000
```

* vérifier l'état du cluster

Depuis un "client" CEPH, os-ctrl-04 par exemple 

```console
ceph -s
  -> envoie l'état du cluster
```
  * l'objectif est d'avoir : `pgs:     832 active+clean`

exemple de résultat au 12/04/2019

```console
ceph -s
  cluster:
    id:     b98b9a3a-c3a1-4e9f-8ed4-377fd4ff5792
    health: HEALTH_OK

  services:
    mon:        3 daemons, quorum os-ctrl-04,os-ctrl-05,os-ctrl-06
    mgr:        os-ctrl-04(active), standbys: os-ctrl-06, os-ctrl-05
    osd:        21 osds: 21 up, 21 in
    rbd-mirror: 3 daemons active

  data:
    pools:   4 pools, 832 pgs
    objects: 19.86 k objects, 153 GiB
    usage:   1.1 TiB used, 132 TiB / 133 TiB avail
    pgs:     832 active+clean

  io:
    client:   29 KiB/s rd, 33 op/s rd, 0 op/s wr
```

* Suivre une reconstruction 
Depuis un "client" CEPH, os-ctrl-04 par exemple 
Utiliser `ceph -w`, qui renvoie les même infos que `ceph -w`, mais en dynamique


* lister les volumes

Depuis un "client" CEPH, os-ctrl-04 par exemple 

```console
ceph osd lspools
ceph osd pool ls

```
exemple de résultat au 12/04/2019
```console
1 images
2 volumes
3 backups
4 metrics

```

* Infos détaillées sur les pool 

```console
rbd -p <POOL_NAME> ls -l
```
exemple de résultat au 12/04/2019

```console
 images ls -l
NAME                                         SIZE PARENT FMT PROT LOCK
0af615bb-5552-464f-a54a-f0f943aea8d6      9.4 GiB          2
0af615bb-5552-464f-a54a-f0f943aea8d6@snap 9.4 GiB          2 yes
...
```

# Getsion des Objets Openstack

## vérification des images

Stockées dans le Pool "images"

```console
ceph osd lspools
```

* lister les images

```console
 images ls -l
```
exemple de résultat au 12/04/2019

```console
NAME                                         SIZE PARENT FMT PROT LOCK
0af615bb-5552-464f-a54a-f0f943aea8d6      9.4 GiB          2
0af615bb-5552-464f-a54a-f0f943aea8d6@snap 9.4 GiB          2 yes
0b8b6ed5-bf99-42e0-8547-2fc9f3dab0d3      283 MiB          2
0b8b6ed5-bf99-42e0-8547-2fc9f3dab0d3@snap 283 MiB          2 yes
14e58e36-f724-4593-be47-8cb4c5a878f8      7.8 GiB          2
...
```

* optenir de sinfos sur une image
```console
qemu-img info rbd:images/<IMAGE_ID>
```

exemple de résultat au 12/04/2019

``` bash
qemu-img info rbd:images/0af615bb-5552-464f-a54a-f0f943aea8d6

image: json:{"driver": "qcow2", "file": {"pool": "images", "image": "0af615bb-5552-464f-a54a-f0f943aea8d6", "driver": "rbd"}}
file format: qcow2
virtual size: 10G (10737418240 bytes)
disk size: unavailable
cluster_size: 65536
Format specific information:
    compat: 1.1
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
```

Récupérer une image (c'est au format qcow2)

``` bash
rbd export images/0af615bb-5552-464f-a54a-f0f943aea8d6 0af615bb-5552-464f-a54a-f0f943aea8d6.qcow2
```

## vérification des volumes


Stockées dans le Pool "volumes"

```console
ceph osd lspools
```

* lister les volumes

```console
rbd -p volumes ls -l
```
exemple de résultat au 12/04/2019

```console
NAME                                         SIZE PARENT FMT PROT LOCK
0af615bb-5552-464f-a54a-f0f943aea8d6      9.4 GiB          2
0af615bb-5552-464f-a54a-f0f943aea8d6@snap 9.4 GiB          2 yes
0b8b6ed5-bf99-42e0-8547-2fc9f3dab0d3      283 MiB          2
0b8b6ed5-bf99-42e0-8547-2fc9f3dab0d3@snap 283 MiB          2 yes
14e58e36-f724-4593-be47-8cb4c5a878f8      7.8 GiB          2
...
```

## Getsion des volumes

Lister les volumes dans le pool de volume et le pool de backup 
```bash
rbd -p volumes  ls -l
rbd -p backup  ls -l
```

Supprimer un volume
```bash
rbd rm volumes/volume-f787c33e-97da-4ae8-9f02-487d4cdd84f2
```

rbd status POOLNAME/VOLUMEid
rbd snap purge POOLNAME/VOLUMEid

Lister les snapshots associé à un volume
```bash
rbd snap ls volumes/volume-f787c33e-97da-4ae8-9f02-487d4cdd84f2
```

Supprimer les snapshot
```bash
rbd snap rm volumes/volume-f787c33e-97da-4ae8-9f02-487d4cdd84f2@snapshot-67b9cc99-5b9f-4351-a8e6-3bd9782e033b
```

Forcer la suppression d'un snapshot
```bash
rbd snap unprotect volumes/volume-f787c33e-97da-4ae8-9f02-487d4cdd84f2@snapshot-67b9cc99-5b9f-4351-a8e6-3bd9782e033b
rbd snap rm volumes/volume-f787c33e-97da-4ae8-9f02-487d4cdd84f2@snapshot-67b9cc99-5b9f-4351-a8e6-3bd9782e033b
```

Suppression d'un lock

```bash
rbd lock ls --pool volumes volume-22f0227e-e38e-4fe3-b487-2f9bba88bffe
There is 1 exclusive lock on this image.
Locker          ID                   Address                     
client.33257242 auto 140681184022464 192.168.74.101:0/3592938473 

rbd lock remove --pool  volumes volume-22f0227e-e38e-4fe3-b487-2f9bba88bffe "auto 140681184022464" client.33257242
```


docs : 
* https://mohamede.com/2017/07/04/busy-ceph-volumes-and-cinder/


