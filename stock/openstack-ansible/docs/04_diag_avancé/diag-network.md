title: Diag pb réseau


# Shell Admin Openstack

Ouvrir un accès admin en ligne de commande, par exemepldepuis os-admin-01
```bash
workon ansible-os
cd os-imta
source openrc/openrc_admin-admin 
```

```bsah
 ---- Neutron ---
# lister les routeurs
ansible neutron_agents_container  -m shell -a " ip -o netns list | grep qrouter"
ansible neutron_agents_container  -m shell -a " ip -o netns list | grep qdhcp"


# --- ntp ---

ansible compute-infra_hosts -m shell -a "timedatectl"
ansible compute-infra_hosts -m shell -a "ntpq -p"
ansible compute_hosts       -m shell -a "timedatectl"
ansible compute_hosts       -m shell -a "ntpq -p"

ansible compute-infra_hosts -m shell -a "systemctl restart ntp"

ansible compute_hosts       -m shell -a "systemctl stop ntp; ntpd -gq; systemctl start ntp;"
ansible compute-infra_hosts -m shell -a "systemctl stop ntp; ntpd -gq; systemctl start ntp;"

# --- neutron ---

ansible neutron_server_container  -m shell -a "systemctl restart neutron-server"
ansible neutron_dhcp_agent        -m shell -a "systemctl restart neutron-dhcp-agent"
ansible neutron_l3_agent          -m shell -a "systemctl restart neutron-l3-agent"
ansible neutron_lbaas_agent       -m shell -a "systemctl restart neutron-lbaasv2-agent"
ansible neutron_linuxbridge_agent -m shell -a "systemctl restart neutron-linuxbridge-agent"
ansible neutron_metadata_agent    -m shell -a "systemctl restart neutron-metadata-agent"
ansible neutron_metering_agent    -m shell -a "systemctl restart neutron-metering-agent"

# lister les routeurs
ansible neutron_agents_container  -m shell -a "ip netns list"

# restart compute_host services
ansible compute_hosts       -m shell -a "systemctl restart nova-compute"
ansible compute_hosts       -m shell -a "systemctl restart neutron-linuxbridge-agent"


```

# Vérifier l'état des agents

```bash
openstack network agent list
```
le "State" doit être à UP, et Alive à ??? 

```bash

+--------------------------------------+--------------------+----------------------------------------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host                                         | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+----------------------------------------------+-------------------+-------+-------+---------------------------+
| 016c6ec1-5fdc-4f3e-a11e-bcf99273532d | Metering agent     | os-ctrl-04-neutron-agents-container-7a8afb9c | None | False | UP |neutron-metering-agent    |
| 15664cd0-61a1-4f6a-ba08-13d1380998eb | Open vSwitch agent | os-ctrl-05-neutron-agents-container-4c34c2fc | None              | False | UP    | neutron-openvswitch-agent |
| 16fccf93-89f0-4fd4-8101-532d8f16c216 | Open vSwitch agent | os-gpu-01                                    | None              | False | UP    | neutron-openvswitch-agent |
| 1afe7ca7-c47b-4f45-81e0-a5cdb68e7a94 | Open vSwitch agent | os-compute-18                                | None              | False | UP    | neutron-openvswitch-agent |
| 20122d9d-6253-404e-98d6-c2742c394bf4 | Open vSwitch agent | os-ctrl-04-neutron-agents-container-7a8afb9c | None              | False | UP    | neutron-openvswitch-agent |
| 26b59246-e7a7-4166-9ef9-b99b30aa57e7 | Open vSwitch agent | os-compute-12                                | None              | False | UP    | neutron-openvswitch-agent |
| 3a8ce649-0c60-4f4c-9aeb-ef617c1f27b7 | L3 agent           | os-ctrl-05-neutron-agents-container-4c34c2fc | nova              | False | UP    | neutron-l3-agent          |
| 3edba35d-601a-4205-98b2-48928a1cf0d4 | Metering agent     | os-ctrl-05-neutron-agents-container-4c34c2fc | None              | False | UP    | neutron-metering-agent    |
| 4fe53917-11fe-471f-b8d4-9d81255aa6c8 | Open vSwitch agent | os-compute-16                                | None              | False | UP    | neutron-openvswitch-agent |
| 501ae487-7de3-4c91-8c42-1ca1c00f67bd | DHCP agent         | os-ctrl-04-neutron-agents-container-7a8afb9c | nova              | False | UP    | neutron-dhcp-agent        |
| 5443dacb-c65d-4df7-924b-7380c2dcfadd | Open vSwitch agent | os-compute-17                                | None              | False | UP    | neutron-openvswitch-agent |
| 5499dd42-6910-417e-83e1-5cc05a20453b | DHCP agent         | os-ctrl-05-neutron-agents-container-4c34c2fc | nova              | False | UP    | neutron-dhcp-agent        |
| 61b1c4ef-adad-46fe-8aad-be386308690d | Open vSwitch agent | os-compute-05                                | None              | False | UP    | neutron-openvswitch-agent |
| 6deaf242-75ac-452a-9971-8392788ffc31 | L3 agent           | os-ctrl-06-neutron-agents-container-f7a1c01b | nova              | False | UP    | neutron-l3-agent          |
| 7113ca57-dd47-439b-a8c7-4bdda83a4727 | DHCP agent         | os-ctrl-06-neutron-agents-container-f7a1c01b | nova              | False | UP    | neutron-dhcp-agent        |
| 71daae90-d09b-4680-bd52-2550d1816e55 | Open vSwitch agent | os-compute-03                                | None              | False | UP    | neutron-openvswitch-agent |
| 7ba8ead8-a4ea-44d9-9bc5-eeaf9c581a62 | Open vSwitch agent | os-compute-15                                | None              | False | UP    | neutron-openvswitch-agent |
| 8eb1de2b-db72-4760-b980-02526a8c1397 | Open vSwitch agent | os-compute-06                                | None              | False | UP    | neutron-openvswitch-agent |
| 9ceb2793-6105-4685-ae92-c2dd3401331b | Open vSwitch agent | os-compute-20                                | None              | False | UP    | neutron-openvswitch-agent |
| a42302ca-6cd1-4b1b-a565-de09480b3187 | Open vSwitch agent | os-compute-14                                | None              | False | UP    | neutron-openvswitch-agent |
| a4f6ff76-823f-486a-8769-37bbf6b1ac26 | Metadata agent     | os-ctrl-06-neutron-agents-container-f7a1c01b | None              | False | UP    | neutron-metadata-agent    |
| a80a8db9-55b7-4509-9008-29ccd983353c | Open vSwitch agent | os-ctrl-06-neutron-agents-container-f7a1c01b | None              | False | UP    | neutron-openvswitch-agent |
| af057eb4-e5fa-4248-b343-b4ac54754162 | Open vSwitch agent | os-compute-08                                | None              | False | UP    | neutron-openvswitch-agent |
| afe02893-e2f2-4f5c-8e75-5f6e0bbc0a7f | Open vSwitch agent | os-compute-09                                | None              | False | UP    | neutron-openvswitch-agent |
| b26268e5-b608-4a5b-b74b-3e3b7d084025 | Open vSwitch agent | os-compute-04                                | None              | False | UP    | neutron-openvswitch-agent |
| b96cae62-df3e-4378-adf4-f13656274203 | L3 agent           | os-ctrl-04-neutron-agents-container-7a8afb9c | nova              | False | UP    | neutron-l3-agent          |
| b9e68a43-bf8c-4a16-ae9b-ed777d457089 | Open vSwitch agent | os-compute-01                                | None              | False | UP    | neutron-openvswitch-agent |
| cc9cdb81-2de8-4337-85f5-95df313b13f0 | Metadata agent     | os-ctrl-05-neutron-agents-container-4c34c2fc | None              | False | UP    | neutron-metadata-agent    |
| cf8d55d7-aedb-425b-94fa-faf3605054cd | Metering agent     | os-ctrl-06-neutron-agents-container-f7a1c01b | None              | False | UP    | neutron-metering-agent    |
| d5fccb4d-eef9-4e0a-b1a4-e8e9e7a1c68b | Metadata agent     | os-ctrl-04-neutron-agents-container-7a8afb9c | None              | False | UP    | neutron-metadata-agent    |
| e580fcdc-65ee-4c0c-a5f6-c155f3c45c54 | Open vSwitch agent | os-compute-13                                | None              | False | UP    | neutron-openvswitch-agent |
| eed47627-aaa1-42f0-9204-16d2da9616cb | Open vSwitch agent | os-compute-07                                | None              | False | UP    | neutron-openvswitch-agent |
| f2122b25-4dfe-419f-be9c-914cc77964f6 | Open vSwitch agent | os-compute-19                                | None              | False | UP    | neutron-openvswitch-agent |
| f6383d7d-8c0a-4b28-83c2-26f6d7161cc2 | Open vSwitch agent | os-compute-02                                | None              | False | UP    | neutron-openvswitch-agent |
| f87ae7a5-5537-4554-9d3d-6ef19a59a0da | Open vSwitch agent | os-compute-10                                | None              | False | UP    | neutron-openvswitch-agent |
+--------------------------------------+--------------------+----------------------------------------------+-------------------+-------+-------+---------------------------+
```

```bash
openstack network agent list --agent-type  'l3'
+--------------------------------------+----------+----------------------------------------------+------+-------+------+------------------+
| ID                                   | Agent Type | Host                                       | AZ  | Alive | State | Binary           |
+--------------------------------------+------------+--------------------------------------------+-----+-------+-------+------------------+
| b96cae62-df3e-4378-adf4-f13656274203 | L3 agent | os-ctrl-04-neutron-agents-container-7a8afb9c | nova | False | UP   | neutron-l3-agent |
| 3a8ce649-0c60-4f4c-9aeb-ef617c1f27b7 | L3 agent | os-ctrl-05-neutron-agents-container-4c34c2fc | nova | False | UP   | neutron-l3-agent |
| 6deaf242-75ac-452a-9971-8392788ffc31 | L3 agent | os-ctrl-06-neutron-agents-container-f7a1c01b | nova | False | UP   | neutron-l3-agent |
+--------------------------------------+----------+----------------------------------------------+------+-------+------+------------------+
```


```
openstack network agent show b96cae62-df3e-4378-adf4-f13656274203
+-------------------------------------+
| Field             | Value               |
+------------------------------------+
| admin_state_up    | UP                    |
| agent_type        | L3 agent             |
| alive             | False                 |
| availability_zone | nova                 |
| binary            | neutron-l3-agent     |
| configuration     | {
	u'agent_mode': u'legacy',
	u'gateway_external_network_id': u'',
	u'handle_internal_only_routers': True,
	u'routers': 21,
	u'interfaces': 20,
	u'floating_ips': 46,
	u'interface_driver': u'openvswitch',
	u'log_agent_heartbeats': False,
	u'external_network_bridge': u'',
	u'ex_gw_ports': 21
}                                              |
| created_at        | 2019-01-28 09:31:44  |
| description       | None                 |
| host              | os-ctrl-04-neutron-agents-container-7a8afb9c    |
| id                | b96cae62-df3e-4378-adf4-f13656274203 |
| last_heartbeat_at | 2019-06-03 11:57:19       |
| name              | None                 |
| started_at        | 2019-06-03 10:49:19   |
| topic             | l3_agent             |
+-----------------------------------+



``` bash
ansible neutron_agents_container  -m shell -a "ovs-vsctl show"
```


# Vérification des connexions

## Vérifier l'état de connactivité d'un routeur

Lister les routeurs
```bash
openstack router list
+--------------------------------------+-------------------------+--------+-------+-------------+------+----------------------------------+
| ID                                   | Name                    | Status | State | Distributed | HA   | Project                          |
+--------------------------------------+-------------------------+--------+-------+-------------+------+----------------------------------+
  ...
| 1f4a32d5-1345-4198-b40c-149023f5b3b2 | gitlab-router           | ACTIVE | UP    | False       | True | f022bc2d868046cc895ac13a0c315f90 |
  ...
```

```bash
ansible neutron_agents_container  -m shell -a "ip netns list" | grep qrouter
os-ctrl-06_neutron_agents_container-f7a1c01b | SUCCESS | rc=0 >>
qrouter-f9d33054-c2f1-47c4-8bb5-2b56b23a37b2 (id: 116)
qrouter-f66fa35c-49b3-4413-baea-2d0a244aa3c8 (id: 115)
qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 114)
...
os-ctrl-05_neutron_agents_container-4c34c2fc | SUCCESS | rc=0 >>
qrouter-f9d33054-c2f1-47c4-8bb5-2b56b23a37b2 (id: 116)
qrouter-f66fa35c-49b3-4413-baea-2d0a244aa3c8 (id: 115)
qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 114)
...
os-ctrl-04_neutron_agents_container-7a8afb9c | SUCCESS | rc=0 >>
qrouter-f9d33054-c2f1-47c4-8bb5-2b56b23a37b2 (id: 116)
qrouter-f66fa35c-49b3-4413-baea-2d0a244aa3c8 (id: 115)
qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 114)
qrouter-f3e21681-a6bf-4545-8411-c361532eff83 (id: 113)
```


Obtenir des infos sur un routeur
``` bash
openstack router show 1f4a32d5-1345-4198-b40c-149023f5b3b2

+----------------------------------------------------------------------+
| Field                   | Value                                      |
+----------------------------------------------------------------------+
| admin_state_up          | UP                                         |
| availability_zone_hints |                                            |
| availability_zones      | nova                                       |
| created_at              |2019-03-18T14:33:00Z                        |
| description             |                                            |
| distributed             | False                                      |
| external_gateway_info   | {
                             "network_id": "aedefe24-5a76-4c29-9ac0-7810f9c20d91",
                             "enable_snat": true,
                             "external_fixed_ips": [{
                                "subnet_id": "4a5a8cb0-79ce-4f67-9d36-197ee30d7334",
                                "ip_address": "10.29.244.59"}]}        |
| flavor_id               | None                                       |
| ha                      | True                                       |
| id                      | 1f4a32d5-1345-4198-b40c-149023f5b3b2       |
| name                    | gitlab-router                              |
| project_id              | f022bc2d868046cc895ac13a0c315f90           |
| revision_number         | None                                       |
| routes                  |                                            |
| status                  | ACTIVE                                     |
| updated_at              | 2019-03-18T14:33:11Z                       |
+----------------------------------------------------------------------+
```

```bash
ansible neutron_agents_container  -m shell -a "ip netns list | grep 1f4a32d5-1345-4198-b40c-149023f5b3b2 "

os-ctrl-06_neutron_agents_container-f7a1c01b | SUCCESS | rc=0 >>
qrouter-1f4a32d5-1345-4198-b40c-149023f5b3b2 (id: 66)

os-ctrl-05_neutron_agents_container-4c34c2fc | SUCCESS | rc=0 >>
qrouter-1f4a32d5-1345-4198-b40c-149023f5b3b2 (id: 53)

os-ctrl-04_neutron_agents_container-7a8afb9c | SUCCESS | rc=0 >>
qrouter-1f4a32d5-1345-4198-b40c-149023f5b3b2 (id: 50)

```


Nom du projet
```bash
openstack project show f022bc2d868046cc895ac13a0c315f90
+-------------+----------------------------------+
| Field       | Value                            |
+-------------+----------------------------------+
| description | Projet pour docker-security-ci   |
| domain_id   | default                          |
| enabled     | True                             |
| id          | f022bc2d868046cc895ac13a0c315f90 |
| is_domain   | False                            |
| name        | docker-security-ci               |
| parent_id   | default                          |
| tags        | []                               |
+-------------+----------------------------------+
```

Vérifier l'état de la "High Avalability" 2 agents doivent être en standby, et un en UP :
```bash
neutron l3-agent-list-hosting-router 1f4a32d5-1345-4198-b40c-149023f5b3b2
+--------------------------------------+----------------------------------------------+----------------+-------+----------+
| id                                   | host                                         | admin_state_up | alive | ha_state |
+--------------------------------------+----------------------------------------------+----------------+-------+----------+
| 3a8ce649-0c60-4f4c-9aeb-ef617c1f27b7 | os-ctrl-05-neutron-agents-container-4c34c2fc | True           | :-)   | standby  |
| 6deaf242-75ac-452a-9971-8392788ffc31 | os-ctrl-06-neutron-agents-container-f7a1c01b | True           | :-)   | standby  |
| b96cae62-df3e-4378-adf4-f13656274203 | os-ctrl-04-neutron-agents-container-7a8afb9c | True           | :-)   | standby  |
+--------------------------------------+----------------------------------------------+----------------+-------+----------+
 - "alive" doit etre à ":-)". si c'est à "xxx" l'agent est HS
 - "ha_state" : il doit y avoir 1 en "active", et 2 en "standby"
```

Controler, via un ping vers sune IP qui doit être accessible (par exempl os-admin-01 : 10.29.241.5 )
```console
ansible neutron_agents_container  -m shell -a "ip netns exec qrouter-f656d350-43fc-4683-993a-4ddf25825f06  ping -c 3 10.29.241.5"

os-ctrl-05_neutron_agents_container-4c34c2fc | FAILED | rc=2 >>
  connect: Network is unreachablenon-zero return code
os-ctrl-04_neutron_agents_container-7a8afb9c | FAILED | rc=2 >>
  connect: Network is unreachablenon-zero return code
os-ctrl-06_neutron_agents_container-f7a1c01b | SUCCESS | rc=0 >>
  PING 10.29.241.5 (10.29.241.5) 56(84) bytes of data.
  64 bytes from 10.29.241.5: icmp_seq=1 ttl=62 time=0.650 ms
  64 bytes from 10.29.241.5: icmp_seq=2 ttl=62 time=0.470 ms
  64 bytes from 10.29.241.5: icmp_seq=3 ttl=62 time=0.435 ms
  --- 10.29.241.5 ping statistics ---
  3 packets transmitted, 3 received, 0% packet loss, time 4078ms
  rtt min/avg/max/mdev = 0.435/0.504/0.650/0.076 ms

 - le ping doit fonctionner sur un des nodes,
 - les autres indiquesnt  "connect: Network is unreachable"
```

TODO : controller via ping vers le subnnet, et les IP du réseau de HA, accessible dans le détail du routeur en tant qu'admin dans Horizon.




## Autres infos 

### infos sur la "External Gateway"

```console
 openstack network show aedefe24-5a76-4c29-9ac0-7810f9c20d91
+---------------------------+--------------------------------------+
| Field                     | Value                                |
+---------------------------+--------------------------------------+
| admin_state_up            | UP                                   |
| availability_zone_hints   |                                      |
| availability_zones        | nova                                 |
| created_at                | 2019-01-29T23:12:51Z                 |
| description               |                                      |
| dns_domain                | None                                 |
| id                        | aedefe24-5a76-4c29-9ac0-7810f9c20d91 |
| ipv4_address_scope        | None                                 |
| ipv6_address_scope        | None                                 |
| is_default                | False                                |
| mtu                       | 1500                                 |
| name                      | external                             |
| port_security_enabled     | True                                 |
| project_id                | dbdddc0e1d1f4fa4a54eae49b2e577c9     |
| provider:network_type     | vlan                                 |
| provider:physical_network | provider                             |
| provider:segmentation_id  | 53                                   |
| qos_policy_id             | None                                 |
| revision_number           | 3                                    |
| router:external           | External                             |
| segments                  | None                                 |
| shared                    | False                                |
| status                    | ACTIVE                               |
| subnets                   | 4a5a8cb0-79ce-4f67-9d36-197ee30d7334 |
| updated_at                | 2019-03-04T21:37:51Z                 |
+---------------------------+--------------------------------------+


```

# vérification de la connectivité

## Environnement

Lancement des commande `openstack-ansible`
se connecter sur un controlleur, et configurer l'environnement
```console
ssh os-ctrl-04.priv
cd /opt/openstack-ansible/playbooks/
```
## données necesaires
* IP Flottante :
* routeur-ID : f656d350-43fc-4683-993a-4ddf25825f06

## vérifier les interfaces
liste les bridges et les ports associés

```console
    Bridge br-provider
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port phy-br-provider
            Interface phy-br-provider
                type: patch
                options: {peer=int-br-provider}
        Port "eth11"
            Interface "eth11"
        Port br-provider
            Interface br-provider
                type: internal

    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port "qg-9fd10656-df"
            tag: 4095
            Interface "qg-9fd10656-df"
                type: internal
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port "vxlan-ac10006b"
            Interface "vxlan-ac10006b"
                type: vxlan
                options: {df_default="true", in_key=flow, local_ip="172.17.149.98", out_key=flow, remote_ip="172.16.0.107"}
        Port "vxlan-ac159d13"
            Interface "vxlan-ac159d13"
                type: vxlan
                options: {df_default="true", in_key=flow, local_ip="172.17.149.98", out_key=flow, remote_ip="172.21.157.19"}
        Port br-tun
            Interface br-tun
                type: internal

```

* les types de ports
```console
        Port "ha-193658e5-1e"
            tag: 4095
            Interface "ha-193658e5-1e"
                type: internal

        Port "qr-e844cfba-3c"
            tag: 9
            Interface "qr-e844cfba-3c"
                type: internal

        Port "tap00cf34df-ee"
            tag: 11
            Interface "tap00cf34df-ee"
                type: internal

        Port "qg-4e0655e3-f8"
            tag: 4095
            Interface "qg-4e0655e3-f8"
                type: internal

        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}

        Port int-br-provider
            Interface int-br-provider
                type: patch
                options: {peer=phy-br-provider}

        Port "vxlan-ac100068"
            Interface "vxlan-ac100068"
                type: vxlan
                options: {df_default="true", in_key=flow, local_ip="172.17.149.98", out_key=flow, remote_ip="172.16.0.104"}

``` 
* Bridge
```console


```



## vérifier que l'IP publique ets bien active

ovs-vsctl show depuis le lxc neutron agent

brctshow depuis le controlleur actif


ping sur une ip flottante, tout en faisant un tcpdump -ni eth11 icmp dans le lxc du neutron agent
si besoin installet tcpdump `apt install tcpdump`.


sur ctrl-06 il faudrait faire un

tcpdump -ni eno2np1 icmp

pour voir si le ping sur l'ip flottante arrive bien par ici

si le tcpdump montre des ping, tu devrais en voir aussi lors d'un

tcpdump -ni f7a1c01b_eth11 icmp

ce qui indiquerait que le ping arrive dans ton conteneur lxc neutron-agent


## vérifier les "Routeur Namespace"

commande : `ip netns`

Format de nom des namespace :  `qrouter-<ROUTER-ID>` 

* vérifier que les Namespace sont bien présents
```console
ansible neutron_agents_container  -m shell -a "ip netns list | grep f656d350-43fc-4683-993a-4ddf25825f06"

os-ctrl-06_neutron_agents_container-f7a1c01b | SUCCESS | rc=0 >>
  qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 42)
os-ctrl-05_neutron_agents_container-4c34c2fc | SUCCESS | rc=0 >>
  qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 42)
os-ctrl-04_neutron_agents_container-7a8afb9c | SUCCESS | rc=0 >>
  qrouter-f656d350-43fc-4683-993a-4ddf25825f06 (id: 42)
```


## --




enfin le log indique qu'il y a déjà un champ dans la table manager. ce
champ est (utilise la commande ovsdb-client dump Open_vSwitch)


ovs-ofctl dump-flows br-int
affiche les netflow ...

# sources
* [https://docs.openstack.org/openstack-ansible/latest/admin/troubleshooting.html]