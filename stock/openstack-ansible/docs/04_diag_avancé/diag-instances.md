title: Diag Instances


# Les instances

## manipulation en dircte sur le scompute via le scommandes virsh

* récupérer les infos sur une instance 
```console
# lister toutes les instances
openstack server list --all

# récupérer l'ID de l'instance
openstack server list --all -f value -c Name -c ID  | grep <INSTANCE_NAME>
openstack server list --all -f value -c Name -c ID  | grep <INSTANCE_NAME> |  awk '{print $1}'

# récupérer les infos sur l'instance
openstack server show <INSTANCE_ID>
```

* identifier l'instance sur le compute
```console
# lister les "domaines"
virsh list --all

# récupérer les infos des domaines
#virsh dumpxml instance-xxxxx > ~/dump-instance/instance-xxxx.xml
mkdir  ~/dump-instances
for TOTO in ` virsh list --all --name`; do echo $TOTO ; virsh dumpxml ${TOTO} > ~/dump-instances/${TOTO}.xml; done


# rechercher à quel domaine correspond le nom de l'instance
grep <INSTANCE_ID> ~/dump-instances/*

# interragir avec l'instance
virsh reboot <DOMAIN_NAME>
virsh start  <DOMAIN_NAME>
virsh stop   <DOMAIN_NAME>
pause/resume
...
``` 

## Modifier la configuration, ou editer le disque d'une instance

Les fichier libvirt.xml sont dans `/etc/libvirt/qemu`, et sont au format "instance-xxxxxxxx.xml", ce qui correspond au nom du domaine libvirt (la VM). par exemple "/etc/libvirt/qemu/instance-00003fb9.xml"

Pour retrouver une instance, il faut recherceher son id dans les fichiers, pour savoir a quel domain ça correspond. Exemple pour l'instance dont l'id est "8a406b2a-c68b-48f9-bc98-43d56178fe24"
```bash
grep 8a406b2a-c68b-48f9-bc98-43d56178fe24/etc/libvirt/qemu/*.xml
``` 

Les disques des instances (fichiers image) sont stockés dans `/var/lib/nova/instances`. Là, on retrouve un dossier par instance, organisé par id d'insance et pas nom de domain. Par exemple "/var/lib/nova/instances/8a406b2a-c68b-48f9-bc98-43d56178fe24/disk"

Pour pouvoir accèder au disque :
```bash
apt install libguestfs-tools
cd /var/lib/nova/instances/8a406b2a-c68b-48f9-bc98-43d56178fe24
guestmount -a disk -i --rw /mnt
```
Il y a également des données dans /var/lib/libvirt

Ref :
* [https://docs.openstack.org/image-guide/modify-images.html(https://docs.openstack.org/image-guide/modify-images.html)




## modif des infos concernant l'instance dirctement dans la base de données

```console
# se connecter sur le container mairaiDB d'un un des controller
ssh os-ctrl-04.priv
latt galera
mysql
> use nova;
> SELECT * FROM instances WHERE uuid = <INSTANCE_ID>;
```
Ex de correction du status d'une instance : `task_state` à passer de `rebooting`  à `NULL`
```language
SELECT display_name,vm_state,task_state FROM instances WHERE uuid = <INSTANCE_ID>;

UPDATE instances SET task_state = NULL WHERE uuid = '<INSTANCE_ID>';

SELECT display_name,vm_state,task_state FROM instances WHERE uuid = <INSTANCE_ID>;
```

https://docs.openstack.org/nova/pike/admin/node-down.html



## convertir une Instance en image


Le principe :
Une VM est constituée d'une image de base (backing file), et d'une image contenant les différences avec cette image de base. La manip consiste a reporter toutes les modif dans l'image de base. On va donc :

* faire une copie de l'image de base
* modifier l'image courante pour pointer vers cette nouvell image de base
* reporter les modif de l'imag courante vers la nouvell image de base.

C'est cette nouvelle image de base qui sera l'image à utiliser.

Les infos sur le fichier image sont dan le dump xml de l'instance. Les infos du backing file également, mais on peut les retrouver avec la commande 'qemu-img info --backing-chain <fichier_image>'



Exemple si dans un fichier de dump on retrouve les infos 
```bash

<source file='/var/lib/nova/instances/d988cdca-919b-4254-a690-4060df048356/disk'/>

_base
   <source file='/var/lib/nova/instances/_base/b0b5130f2e44d96257dde9360a809030966238e6'/>
```

Vérification de la "backing file"
```bash
qemu-img info --backing-chain /var/lib/nova/instances/d988cdca-919b-4254-a690-4060df048356/disk

file format: qcow2
virtual size: 20G (21474836480 bytes)
disk size: 17G
cluster_size: 65536
backing file: /root/convert/gitlab
Format specific information:
    compat: 1.1
    lazy refcounts: false
    refcount bits: 16
    corrupt: false

image: /root/convert/gitlab
file format: raw
virtual size: 20G (21474836480 bytes)
disk size: 19G

```

Préparation d'un environnement de travail / backup 
```bash

mkdir ~/convert
cp -p  /var/lib/nova/instances/d988cdca-919b-4254-a690-4060df048356/disk ~/convert/current-gitlab
cp /var/lib/nova/instances/_base/b0b5130f2e44d96257dde9360a809030966238e6 ~/convert/new-gitlab-base

qemu-img info --backing-chain ~/convert/current-gitlab
# resulat identique à l'image d'origine
```

Modification de l'image courante, et application des modifs
```bash
cd ~/convert
qemu-img rebase -b new-gitlab-base  current-gitlab
# si besoin de "forcer" :  qemu-img rebase -b new-gitlab-base -u current-gitlab

qemu-img info --backing-chain ~/convert/current-gitlab
# le "backing file" doit maintenant correspondre à "new-gitlab-base"
```

Application des modifs de l'image courante sur le "backing file"
```bash
qemu-img commit  current-gitlab

qemu-img info new-gitlab-base
# il ne doit pas y avoir de "backing file"
```

Importer l'image
```bash

# si le format est RAW, convertir en cqow2
#qemu-img convert -f raw -O qcow2 ~/convert/gitlab ~/convert/gitlab.qcow2

openstack image create \
  --disk-format qcow2 \
  --container-format bare \
  --private \
  --project beta-gitlab \
  --file gitlab.qcow2 \
  gitlab_Resto_20200930 
```

source :
 * https://medium.com/@kumar_pravin/qemu-merge-snapshot-and-backing-file-into-standalone-disk-c8d3a2b17c0e