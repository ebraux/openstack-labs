



## Contexte

IMT Atalntique exploite depuis plusieurs années une infrastructure Openstack pour ses besoins d'enseignement et de recheche.
Une première instance a été déployée manuellement en interne, en version Rocky.
Une deuxiéme instance a ensuite été déployée par la société Ozone, sur la base de la solution Openstack Ansible, en version Ocata.
Une troisiéme instance a été déployée par la société Unyonsys, également sur la base de la solution Openstack Ansible, en version Rocky. Cette instance s'appuie sur un Backend CEPH, et est actuellement l'instance de production.

Cette infrastructure est utilisée en enseignement, pour fournir des environnements de TP à la demande, mais également en recherche, ou pour des projets.

La gestion au quotidien, et le maintien en conditions opérationelle sont  assurés par IMT Atlantique.

La plateforme Openstack est désormais entrée dans une phase de production, et a atteints une taille limite par rapport aux capacités d'exploitation d'IMT Atlantique.
Par ailleurs, plusieurs projets sont en cours d'étude, qui immpliqueront une montée en puissance de cette infrastrsucture.

IMT Atlantique est donc à la recherche d'un partenaire pour l'aider aider à assurer le maintien opérationnel, et l'évolution de cloud privée Opanstack.


## Détail sur la plateforme en place

La platefortme actuellement en production a été déployé avec la solution openstack-ansible. Elle est en version Rocky, sur une base Ubuntu 18.04.

Elle comporte :
* un control-plane de 3 noeuds en HA. Chaque noeud imlémente l'ensemble des composants du control plane. Il n'y a pas de noeud dédié pour les composants d'infrastructure  (mysql, RabbitMq, ...),  ou pour neutron.
* 20 noeuds de compute, assez hétérogènes (serveur DELL Poweredge de divers génération alant du R610 au R740)
* un cluster CEPH de 30To utiles, utilisé comme backend pour glance et cinder, mais pas pour Nova pour l'instant (interconnexion réseau en 1G, pas assez performante l'instant).

Les composants installés sont : keystone, neutron (vxlan-openvswtich), nova, glance, cinder, horizon, heat, ceilometer, gnocchi.
D'autres composants comme Magnum et Designate ont été installés mais ne sont pas utilisés.

Le cluster CEPH a été installé de façon séparé (via ceph-ansible), mais n'est utilisé que par Openstack.
 
Heat est utilisé pour provisionner des infrastrucutres. Terrafome commence également a être utilisé.

Un projet de déploiement d'une suite de suivi et monitoring de l'infrastrucutre est en cours, basé sur Grafana, Chronograf, Kibana, ...

Il n'y a pas d'applications "legacy" en production sur cette infrastructure. Ce type d'application est hébergé sur une autre plateforme dédiée à la production.

Les principaux problèmes rencontrés sont liés à ansible-openstack, et au fonctionnement de RabbitMQ :
* complexité pour modifier précisément les configurations,
* gestion de la base Mysql/galera
* timeout  RabbitMQ.


## Prestations attendues

IMT Atlantique souhaite un redéploiement de son Infrastrucutre Openstack/Ceph dans les dernière versions stables, ainsi qu'un contrat de support couvrant cette infrastructure.

La nouvelle infrastructure doit être opérationelle dès que possible, et au plus tard pour la prochaine rentrée scolaire, soit le 14 septembre 2020.

Le candidat assurera la gestion de projet. Il devra préciser les grandes étapes de sa démarche.

Le candidat devra indiquer le mode de fontionnement du support (portail utilisateur, accès distant, tickets, ... ), et fournir un exemplaire du contrat de support.
Le contrat de support devra couvrir au minimum la période entre la validation du redéploiement et mars 2021.
Il ne sera pas reconductible.
Le niveau attendu est un support en heures ouvrées : 8/5
Il doit couvrir les mises à jour de la plateforme pour suivre les versions stables d'Opensatck.


Le candidat devra indiquer la métholdologie qui sera mise en place pour le redéploiement de l'infrastructure, et la migration des données.
Il devra indiquer comment se répartiront les interventions entre ses équipes et IMT Atlantique.
Il devra préciser quelles données penvent être migrées. 


Le redéploiement doit comporter au minimum les briques Openstack de base suivantes: Keystone, Neutron (vxlan-openvswtich), Nova, Glance, Cinder, Horizon et Heat. Et intéger le cluster CEPH pour le stockage des images par Glance, et la gestion des volumes par Cinder.
Le cluster CEPH peut être intégré à Opensatck, ou installé séparément, mais il doit être couvert par le maintient opérationel.

Une sauvegarde régulière des données de configuration et de gestion de l'infrastrucutre doit etre mise en place. 

La fonctionnalité de Haute Disponibilité actuellement en place n'est pas un prérequis. IMT Atlantique souhaite qu'en cas de panne ou d'incident, le role de controller puisse être retabli en 4 heures maximum, sans perte de donnée significative.

Il n'est pas possible de mettre en place une nouvelle infrastrucutre en parallèle de celle existante, le redéploiement devra donc être effectué sur le matériel actuellement en production.

Une interruption de service de 2 jours peut-être envisagée, 5 jours maximum.

Les données devront être migrées de l'infrastructure actuelle vers la nouvelle infrastructure.  
Les données à migrer sont au minimum les instances et les volumes, mais doivent si possible concerner également les réseaux, sous réseaux, VIP, groupe de sécurité, keypair, utilisateurs et projets.


IMT Atlantique souhaitent pouvoir être autonome pour la gestion de l'évolution de l'infrastructure:
* ajout suppression de noeuds,
* adaptation de la configuration de façon spécifique à chaque noeud,

IMT Atlantique souhaite également être en mesure de mettre en place un prototype pour tester de nouvelles fonctionnalités.

Le candidat devra donc prévoir un transfert de compétence des technologies utilisées pour le déploiement.


Le candidats devra indiquer si l'ajout d'un noeud de compute est compris dans sa propostion en terme de licence et support. Si ce n'est pas le cas, il devra fournir une estimation des éléments financiers.

Le candidat devra indiquer les outils de suivi, gestion de logs et montoring disponibles dans sa solution.
Si ce type d'outil peut être mis en place mais necessite une prestation complémentaire, le candidat fournir une estimation des éléments financiers.

Bien que non compris dans les composant indispensables, IMT Atlantique souhaiterais pouvoir utiliser à court terme le composant de gestion de LBaas Octavia. Le candidat devra indiquer si ce composant est disponible dans sa solution, si sa mise en place mais necessite une prestation complémentaire, et le cas échéant fournir une estimation des éléments financiers.

## Conditions

Cette demande est effectuée dans le cadre d'un marché à procédure adaptée (MAPA), à critère prix uniquement.
Elle n'a pas été diffusée sur les plateformes de marchés publiques, mais plusieurs sociétés ont été consultées.
Toute proposition dépassant le plafond maximum de 25 000€ HT ne pourra pas être traitée.
Le délai de réponse a été fixé à 2 semaines. La date limte de réponse est donc le lundi 27 avril, 10h00.




...

# ---------------------------------------------

base
octavia : 2j  2k€
monitoting : 
support 5k€ + 10 2,8k€  -> 7,8k€



# ----------------------------------------------------------
 
Prévision de fonctionnalités à ajouter :  
 - "GPU as a service"  (via  PCI Passtrough et vGPU)
 - traçabilité des accès réseau vers l'extérieur (en tant que fournisseur internet)
 - ajout de stockage objet stockage objet (via swift, ou en direct)
 - backend CEPH pour nova sur certain computes (quand réseau mis à jour à jour)
 - "FPGA as a service"
 - monitoring/supervision
 - Authentification via fédération Renater 


Nous souhaitons a court terme proposer du stockage Objet, et du GPU as a service.
Ainsi que des fonctionnalités de métrologie, d'auto-scaling, et de mesure de consommation des ressources
A moyen terme, nous auront également besoin d'une infrastructure d'orchestration de container. Nous avons fait des tests non convaincants avec magnum + kubernetes.


 

Nous somme prêt à revoir notre déploiement. Une piste serait l'abandon d'openstack-ansible, au profit de Kolla-ansible, et l'abandon du HA, au profit d'une infra avec 1 control plane, 1 noeud dédié à neutron, et un à rabbitMQ.
Avec une redondance des service mais sans HA.

Nous avons une première phase qui concerne le maintien opérationnel de l'infrastructure actuelle, a horizon 2020.

Ainsi qu'une deuxième phase qui correspond à un appel à projet, et qui impliquerait :
 - une augmentation significative du nombre de compute + GPU (une 30 aine de compute)
 - la gestion de plusieurs infratructure réparties sur différentes site de la région Brestoise.
Si le projet est retenu, cette deuxième phase serait à horizon 2022.

Ci dessous quelques détail technique complémentaires.



Information sur l'infrastructure d'IMT Atlantique
 

a ## options

 # Evaluation de solution de gestion des accès sortants Openstack

Description
Actuellement chaque instance déployée doit être configurée pour utiliser un proxy sans authentification, mais avec accès limité.
L'utilisation de ce type de proxy pose des problèmes de configuration pour la plupart des outils utilisés sur la plateforme, et les demandes d'ajout dec sites dans la liste blanche est régulier.
Plusieurs actions ont été menées, sans succés :
* Mise en place du traçage des actions réseau au niveau de mysql : incomplet.
* remplacement du backend réseau de lunixbridge vers openvswitch pour pouvoir almenter le serveur Opensflow, non finalisé.



Par rapport à vote devis, dans la demande finale, j'aurais besoin d'ajouter le Lbaas (avec Octavia).
Et je voudrais également savoir si des outils de monitoring sont intégrés (type elasticsearch, Grafana, ...) ou si il faut prévoir les prévoir en supplément.
J'aurais également souhaité échanger sur les contraintes de mis en place de la nouvelle infrastructure, notamment par rapport à l'utilisation de CEPH, pour pouvoir anticiper des achats de matériel éventuels.


L'option déploiement avec Kolla ansible semble être l'option la plus utilisée pour les nouveaux déploiements dans la communauté enseignement/recherche. Si vous la déployez et assurez du support dessus, nous partirions certainement sur cette option.
 
Nous partirions sur un scénario sans migration d'instance, car les instances servent principalement pour du calcul. et dans tous les cas, les utilisateurs savent qu'il s'agit d'une infrastructure jetable.

## Options
- "GPU as a service"  (via  PCI Passtrough et vGPU) - 10 à 15k€
 - traçabilité des accès réseau vers l'extérieur (en tant que fournisseur internet) - Pouvez-nous donner plus de détails sur ce point ? En l'état nous ne pouvons pas chiffrer.
 - ajout de stockage objet stockage objet (via swift, ou en direct) - 5 à 7k€
 - backend CEPH pour nova sur certain computes (quand réseau mis à jour à jour) - 10 à 15k€
 - "FPGA as a service" - à ce jour nous n'avons pas les compétences mais nous pourrons probablement prévoir des jours de R&D sur le sujet pour une montée en compétences si vous êtes intéressés.
 - monitoring/supervision - 10 à 15k€
 - Authentification via fédération Renater - Quel type de protocole utlisez-vous pour le SSO ? (Kerberos, SAML, OIDC)


Pour l'option "traçabilité des accès réseau vers l'extérieur en tant que fournisseur internet", consiste pour les connexions sortantes vers internet, a pouvoir déterminer quelle instance est  à l'origine de la requête, et donc quel projet, et donc le responsable de ce projet.
C'est par rapport aux demandes que nous avons parfois de la justice, d'identifier la personne se trouvant derrière une adresse IP.
Sur les postes administrés c'ets simple, puisque chaque IP est associée à une adresse mac. le flux réseau est ensuite anlysé (netflow).
Pour le traffic sortant de la plateforme Openstack c’est compliqué, puisque les instances sont créés/supprimées à la demande.
Actuellement, les accès sortant sont limités à une whitelist de sites.
 
L'authentification  via fédération Renater  se fait via Shibboleth c'est du SAML (https://services.renater.fr/federation/index)
 
 

 - préparation plateforme OS et tests:

ceilometer
cinder
glance
gnocchi
heat
keystone
neutron
nova
horizon


- livrable : compte-rendu d'installation + doc d'exploitation

- transfert de compétences Playbook Ansible Objectif Libre et configuration

- preparation/formation trouble shooting

- Transfert de compétences trouble shooting

- rédaction doc utilisation des playbooks OpenStack Objectif Libre

- recette- Rédaction cahier de recette OpenStack et Ceph
- tests sur site
- correction à distance 

- gestion de projet

Pour la partie support nous fonctionnons en heures ouvrées de 9h à 18h avec un système de carnet de tickets dont les tarifs sont les suivants :

- 10 tickets - 20H00 - 2800 € HT

- 20 tickets - 40h00 - 4800 € HT 

- 30 tickets - 60h00 - 6500 € HT

- 50 tickets - 100H00 - 10 000 € HT

- 100 ticket - 200h00 à 19 000 € HT

En complément d'un carnet il y a une redevance annuelle de 10K€ pour accéder aux supports (peu importe le nombre de plateformes) 



##
contacts :
### RedHat

* lserot@redhat.com
* tbotte@redhat.com

``` Bash
Laurent SEROT
Architecte Avant-vente / Account Solution Architect
Red Hat France
23-25 rue Delarivière Lefoullon - 92800 Puteaux
Mobile: +33 (0) 6 43 51 85 52
Email: lserot@redhat.com
LinkedIn: https://fr.linkedin.com/in/serot
Twitter: @lserot
``` 

### Canonical

"Reg Deraed" <reg.deraed@canonical.com>


### Mirantis

"Daniel Virassamy" <dvirassamy@mirantis.com>
"David Chorbadzhiev" <dchorbadzhiev@mirantis.com>