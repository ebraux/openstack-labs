## Configuration du réseau

### Configuration du support des bridge sur le système

Installation des package
```console
apt install -y bridge-utils
```

Activation du support des bridge, et du 802.1q
```console
# Activation des module du noyau
modprobe bridge
modprobe br_netfilter
modprobe 8021q
echo '8021q' >> /etc/modules

# Enable network bridge filters
sysctl -w net.bridge.bridge-nf-call-iptables="1"
echo 'net.bridge.bridge-nf-call-iptables = 1' >> /etc/sysctl.conf
sysctl -w net.bridge.bridge-nf-call-ip6tables="1"
echo 'net.bridge.bridge-nf-call-ip6tables = 1' >> /etc/sysctl.conf
```

---

## Création du bridge pour le reseau externe

### Mise en place de la configuration

Fichier de configuration [99-br-ex.yaml](conf/netplan_99-br-ex.yaml), à mettre en place le fichier : /etc/netplan/99-br-ex.yaml
```bash
cp ~/openstack_config/netplan_99-br-ex.yaml /etc/netplan/99-br-ex.yaml
cat /etc/netplan/99-br-ex.yaml
```
### Détails du fichier de configuration

Configuration d'un Bridge pour IP 203.0.113.2/24, sans interface
```yaml
    bridges:
        br-ex:
            interfaces: []
            addresses: [203.0.113.2/24]
            dhcp4: false
              parameters:
              stp: false
              forward-delay: 0
```

###  Appliquer la configuration
```bash
netplan apply
```

Vérification
```bash
brctl show
ip a
ip route
```

```bash
$ brctl show
bridge name	bridge id		STP enabled	interfaces
br-ex		8000.6eb1d52a4824	no		tap1b6ffceb-3e
```


```bash
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1400 qdisc fq_codel state UP group default qlen 1000
    link/ether fa:16:3e:42:9e:a7 brd ff:ff:ff:ff:ff:ff
    inet 172.16.50.21/24 brd 172.16.50.255 scope global dynamic ens3
       valid_lft 50509sec preferred_lft 50509sec
    inet6 fe80::f816:3eff:fe42:9ea7/64 scope link 
       valid_lft forever preferred_lft forever
3: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 6e:b1:d5:2a:48:24 brd ff:ff:ff:ff:ff:ff
    inet 203.0.113.2/24 brd 203.0.113.255 scope global br-ex
       valid_lft forever preferred_lft forever
    inet6 fe80::6cb1:d5ff:fe2a:4824/64 scope link 
       valid_lft forever preferred_lft forever
```

``` bash
$ ip route
default via 172.16.50.1 dev ens3 proto dhcp src 172.16.50.21 metric 100 
169.254.169.254 via 172.16.50.3 dev ens3 proto dhcp src 172.16.50.21 metric 100 
172.16.50.0/24 dev ens3 proto kernel scope link src 172.16.50.21 
203.0.113.0/24 dev br-ex proto kernel scope link src 203.0.113.2 
```

