

- OpenStack Security: A Practical Guide : [https://superuser.openinfra.dev/articles/openstack-security-a-practical-guide/](https://superuser.openinfra.dev/articles/openstack-security-a-practical-guide/)

- Firewalls and default ports : [https://docs.openstack.org/install-guide/firewalls-default-ports.html](https://docs.openstack.org/install-guide/firewalls-default-ports.html)