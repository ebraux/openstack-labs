



---
## vérifier la connectivité d'un routeur

``` bash
ip netns list" | grep qrouter

```

``` bash
openstack router show <ID>
ip netns list | grep <ID>
neutron l3-agent-list-hosting-router 9253e3a5-5432-4a3d-a8a1-b01b86470564
openstack network agent list --router 9253e3a5-5432-4a3d-a8a1-b01b86470564
```



---
réseau 'public"
2ab8e95e-a0d2-4f76-9f37-c39491750d80

Réseau fournisseur
  - Type de Réseau : flat
  - Réseau Physique : physnet1
  - CIDR : 10.0.2.0/24
  - Démarrer 10.0.2.150 - Fin 10.0.2.199
  - Adresse IP de la passerelle 10.0.2.1
  
router 9253e3a5-5432-4a3d-a8a1-b01b86470564
ip 10.0.2.195
sbbnet : 10.0.0.0/24 



openstack subnet list
+--------------------------------------+----------------+--------------------------------------+-------------+
| ID                                   | Name           | Network                              | Subnet      |
+--------------------------------------+----------------+--------------------------------------+-------------+
| 5ae2df9f-be8a-4d9d-915d-3217ac911299 | public1-subnet | 2ab8e95e-a0d2-4f76-9f37-c39491750d80 | 10.0.2.0/24 |
| b1d755e4-ef0f-4e5a-b636-55c31a8882a2 | demo-subnet    | 18c5183e-829a-4bf2-ae7d-e5d0b5374d4a | 10.0.0.0/24 |
+--------------------------------------+----------------+--------------------------------------+-------------+

Info sur les network namespace crées
``` bash
sudo ip netns list
qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 (id: 1)
qdhcp-18c5183e-829a-4bf2-ae7d-e5d0b5374d4a (id: 0)
```
On a bien un namespace pour le réseau "demo-subnet", et un pour le routeur qui lui est asscosié.

Infos sur le routeur :
``` bash
openstack router show 9253e3a5-5432-4a3d-a8a1-b01b86470564 -f json -c external_gateway_info -c interfaces_info
# {
#   "external_gateway_info": {
#     "network_id": "2ab8e95e-a0d2-4f76-9f37-c39491750d80",
#     "external_fixed_ips": [
#       {
#         "subnet_id": "5ae2df9f-be8a-4d9d-915d-3217ac911299",
#         "ip_address": "10.0.2.195"
#       }
#     ],
#     "enable_snat": true
#   },
#   "interfaces_info": [
#     {
#       "port_id": "d73c56b2-05cf-4534-8775-4b9184fcfcf7",
#       "ip_address": "10.0.0.1",
#       "subnet_id": "b1d755e4-ef0f-4e5a-b636-55c31a8882a2"
#     }
#   ]
# }
```
On a bien une pate sur le réseau "public", et un pate sur le réseau `demo-subnet` : 

- Ip sur le réseau externe : 10.0.2.195
- IP sur le réseau interne : 10.0.0.1

Vérification de la configuration
``` bash
sudo ip netns exec qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 ip a
10: qr-d73c56b2-05: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 10.0.0.1/24 brd 10.0.0.255 scope global qr-d73c56b2-05
11: qg-069cd643-94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 10.0.2.195/24 brd 10.0.2.255 scope global qg-069cd643-94
```


Test de connectivité du routeur
Tests de ping 
``` bash
sudo ip netns exec qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 ping -c 3 10.0.2.195
# --> OK

sudo ip netns exec qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 ping -c 3 10.0.0.1
# --> OK

sudo ip netns exec qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 ping -c 3 10.0.2.1
# --> KO. pas de passerelle active
```

docker exec openvswitch_vswitchd ovs-vsctl 
sudo ip addr add 10.0.2.2/24 brd + dev br-ex

docker exec openvswitch_vswitchd ip addr add 10.0.2.1/24 dev br-ex

Ping de la passerelle depuis l'hôte :
``` bash
openstack subnet show public1-subnet -f value  -c gateway_ip
# 10.0.2.1

ping 10.0.2.1
```


Ping d'une instance
``` bash
openstack server list -c Name -c Status -c Networks
# +-------+--------+--------------------+
# | Name  | Status | Networks           |
# +-------+--------+--------------------+
# | demo1 | ACTIVE | demo-net=10.0.0.66 |
# +-------+--------+--------------------+
    ```

``` bash
sudo ip netns exec qrouter-9253e3a5-5432-4a3d-a8a1-b01b86470564 ping -c 3 10.0.0.66
```


Assoxier une IP flottante
``` bash
openstack floating ip create public1
openstack floating ip list
+--------------------------------------+---------------------+------------------+------+--------------------------------------+----------------------------------+
| ID                                   | Floating IP Address | Fixed IP Address | Port | Floating Network                     | Project                          |
+--------------------------------------+---------------------+------------------+------+--------------------------------------+----------------------------------+
| 8046bbb0-77b0-41b5-a066-4a3b850c2240 | 10.0.2.167          | None             | None | 2ab8e95e-a0d2-4f76-9f37-c39491750d80 | df8641b708e24ab3b6e5e0876070060d |
+--------------------------------------+---------------------+------------------+------+--------------------------------------+----------------------------------+
```

9106e739df26   quay.io/openstack.kolla/openvswitch-vswitchd:2023.1-ubuntu-jammy            "dumb-init --single-…"   11 hours ago   Up 11 hours (healthy)               openvswitch_vswitchd
aa0a333e1706   quay.io/openstack.kolla/openvswitch-db-server:2023.1-ubuntu-jammy           "dumb-init --single-…"   11 hours ago   Up 11 hours (healthy)               openvswitch_db


Voir les interfaces sur le système :
``` bash
# ip link list
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
# 2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
#     link/ether fa:16:3e:af:9c:ec brd ff:ff:ff:ff:ff:ff
#     altname enp0s3
#     altname ens3
# 3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
#     link/ether fa:16:3e:af:a5:81 brd ff:ff:ff:ff:ff:ff
#     altname enp4s4
# 4: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master ovs-system state UP mode DEFAULT group default qlen 1000
#     link/ether fa:16:3e:af:a6:25 brd ff:ff:ff:ff:ff:ff
#     altname enp4s5

# 5: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
#     link/ether 52:f7:38:54:72:67 brd ff:ff:ff:ff:ff:ff

# 6: br-ex: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
#     link/ether 7e:81:86:c0:2a:44 brd ff:ff:ff:ff:ff:ff

# 7: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
#     link/ether 76:61:1d:a3:4c:47 brd ff:ff:ff:ff:ff:ff

# 8: br-tun: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
#     link/ether e2:4e:67:11:f7:48 brd ff:ff:ff:ff:ff:ff
```
- tapxxxx -> interface virtuelle de l'instance (configurée par neutron pour nova)




Lisiter les bridge dans openvswitch
``` bash
docker exec openvswitch_vswitchd ovs-vsctl list-br
br-ex
br-int
br-tun
```

``` bash
docker exec openvswitch_vswitchd ovs-vsctl list-ports br-ex
eth2
phy-br-ex
```

``` bash
docker exec openvswitch_vswitchd ovs-vsctl list-ports br-int
int-br-ex
patch-tun
qg-069cd643-94
qr-d73c56b2-05
qvoa1cb205b-2c
tapab571d98-e5
```
``` bash
docker exec openvswitch_vswitchd ovs-vsctl list-ports br-tun
patch-int
```

Voir la config d'openvswitch
``` bash
docker exec openvswitch_vswitchd ovs-vsctl show

0da53324-be40-4a97-b9e8-c30a99881812
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-tun
            Interface br-tun
                type: internal
        Port patch-int
            Interface patch-int
                type: patch
                options: {peer=patch-tun}
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-int
            Interface br-int
                type: internal
        Port qr-d73c56b2-05
            tag: 1
            Interface qr-d73c56b2-05
                type: internal
        Port qg-069cd643-94
            tag: 2
            Interface qg-069cd643-94
                type: internal
        Port tapab571d98-e5
            tag: 1
            Interface tapab571d98-e5
                type: internal
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
    Bridge br-ex
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-ex
            Interface br-ex
                type: internal
        Port phy-br-ex
            Interface phy-br-ex
                type: patch
                options: {peer=int-br-ex}
        Port eth2
            Interface eth2
```

On retrouve br-ex, attaché à eth2
un patch entre br-ex et br-int
un patch entre br-tun et br-int



---
## Ressources

- [https://wiki.openstack.org/wiki/OpsGuide/Network_Troubleshooting](https://wiki.openstack.org/wiki/OpsGuide/Network_Troubleshooting)
- [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/10/html/networking_guide/sec-connect-instance](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/10/html/networking_guide/sec-connect-instance)
- [https://docs.openstack.org/devstack/pike/guides/neutron.html](https://docs.openstack.org/devstack/pike/guides/neutron.html)

- [https://kifarunix.com/deploy-multinode-openstack-using-kolla-ansible/#google_vignette](https://kifarunix.com/deploy-multinode-openstack-using-kolla-ansible/#google_vignette)