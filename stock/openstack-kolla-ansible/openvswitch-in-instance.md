


In current Neutron implementation MAC spoofing protection rules are added implicitly to each VM port, these rules are not part of the security groups feature.

Without this protection a VM in a network can spoof and answer to ARP requests that don’t actually belong to it, or fake response for other VM.

- [https://docs.openstack.org/developer/dragonflow/specs/mac_spoofing.html](https://docs.openstack.org/developer/dragonflow/specs/mac_spoofing.html)



https://superuser.openinfra.dev/articles/openvswitch-openstack-sdn/

les flow, et tcpdumsp :

- https://fosshelp.blogspot.com/2016/07/openstack-openvswitch-vm-instance-not_64.html

https://superuser.openinfra.dev/articles/managing-port-level-security-openstack/


https://serverascode.com/2015/01/21/bridge-not-replying-arp.html

https://networkop.co.uk/blog/2016/05/06/neutron-l2pop/