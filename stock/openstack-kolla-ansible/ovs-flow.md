
Si vous souhaitez voir les règles de translation d'adresses réseau (NAT) mises en place par OpenvSwitch, vous pouvez utiliser la commande suivante :
lua
Copy code
ovs-ofctl dump-flows <bridge_name>



https://stackoverflow.com/questions/60583652/cannot-respond-to-arp-requests-in-ovs-system

``` bash
Then, I added the flow entry:

$ ovs-ofctl -O openflow13 br0 priority=1,arp,in_port=eth2,action=output:NORMAL
```

sudo iptables -L -n -v
Chain INPUT (policy ACCEPT 489K packets, 1522M bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain OUTPUT (policy ACCEPT 414K packets, 86M bytes)
 pkts bytes target     prot opt in     out     source               destination         




ip route show
default via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.81 metric 100 
10.20.1.0/24 dev eth0 proto kernel scope link src 10.20.1.81 metric 100 
10.20.1.1 dev eth0 proto dhcp scope link src 10.20.1.81 metric 100 
10.20.1.254 dev eth0 proto dhcp scope link src 10.20.1.81 metric 100 
10.20.10.0/24 dev eth1 proto kernel scope link src 10.20.10.200 metric 100 

10.20.11.0/24 dev eth2 proto kernel scope link src 10.20.11.170 metric 100 

100.125.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.81 metric 100 
100.126.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.81 metric 100 
169.254.169.254 via 10.20.1.254 dev eth0 proto dhcp src 10.20.1.81 metric 100 



kolla-ansible) cloud@kos-aio:~/kolla-ansible$ docker exec openvswitch_vswitchd ovs-ofctl dump-flows  br-int
NXST_FLOW reply (xid=0x4):
 cookie=0x4b5b909e1d93c4ae, duration=3350.615s, table=0, n_packets=0, n_bytes=0, idle_age=3350, priority=65535,dl_vlan=4095 actions=drop
 cookie=0x4b5b909e1d93c4ae, duration=3344.681s, table=0, n_packets=0, n_bytes=0, idle_age=3349, priority=2,in_port=1 actions=drop
 cookie=0x4b5b909e1d93c4ae, duration=3350.618s, table=0, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=resubmit(,59)
 cookie=0x4b5b909e1d93c4ae, duration=3350.619s, table=23, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=drop
 cookie=0x4b5b909e1d93c4ae, duration=3350.615s, table=24, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=drop
 cookie=0x4b5b909e1d93c4ae, duration=3350.613s, table=30, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=resubmit(,59)
 cookie=0x4b5b909e1d93c4ae, duration=3350.612s, table=31, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=resubmit(,59)
 cookie=0x4b5b909e1d93c4ae, duration=3350.617s, table=59, n_packets=0, n_bytes=0, idle_age=3350, priority=0 actions=resubmit(,60)
 cookie=0x4b5b909e1d93c4ae, duration=3350.616s, table=60, n_packets=0, n_bytes=0, idle_age=3350, priority=3 actions=NORMAL
 cookie=0x4b5b909e1d93c4ae, duration=3350.614s, table=62, n_packets=0, n_bytes=0, idle_age=3350, priority=3 actions=NORMAL
(kolla-ansible) cloud@kos-aio:~/kolla-ansible$ docker exec openvswitch_vswitchd ovs-ofctl dump-flows  br-tun
NXST_FLOW reply (xid=0x4):
 cookie=0xca847fcae08dae52, duration=3356.584s, table=0, n_packets=0, n_bytes=0, idle_age=3356, priority=1,in_port=1 actions=resubmit(,2)
 cookie=0xca847fcae08dae52, duration=3356.583s, table=0, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=drop
 cookie=0xca847fcae08dae52, duration=3356.583s, table=2, n_packets=0, n_bytes=0, idle_age=3356, priority=1,arp,dl_dst=ff:ff:ff:ff:ff:ff actions=resubmit(,21)
 cookie=0xca847fcae08dae52, duration=3356.581s, table=2, n_packets=0, n_bytes=0, idle_age=3356, priority=0,dl_dst=00:00:00:00:00:00/01:00:00:00:00:00 actions=resubmit(,20)
 cookie=0xca847fcae08dae52, duration=3356.580s, table=2, n_packets=0, n_bytes=0, idle_age=3356, priority=0,dl_dst=01:00:00:00:00:00/01:00:00:00:00:00 actions=resubmit(,22)
 cookie=0xca847fcae08dae52, duration=3356.580s, table=3, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=drop
 cookie=0xca847fcae08dae52, duration=3356.579s, table=4, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=drop
 cookie=0xca847fcae08dae52, duration=3356.579s, table=6, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=drop
 cookie=0xca847fcae08dae52, duration=3356.578s, table=10, n_packets=0, n_bytes=0, idle_age=3356, priority=1 actions=learn(table=20,hard_timeout=300,priority=1,cookie=0xca847fcae08dae52,NXM_OF_VLAN_TCI[0..11],NXM_OF_ETH_DST[]=NXM_OF_ETH_SRC[],load:0->NXM_OF_VLAN_TCI[],load:NXM_NX_TUN_ID[]->NXM_NX_TUN_ID[],output:OXM_OF_IN_PORT[]),output:1
 cookie=0xca847fcae08dae52, duration=3356.577s, table=20, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=resubmit(,22)
 cookie=0xca847fcae08dae52, duration=3356.577s, table=21, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=resubmit(,22)
 cookie=0xca847fcae08dae52, duration=3356.576s, table=22, n_packets=0, n_bytes=0, idle_age=3356, priority=0 actions=drop
(kolla-ansible) cloud@kos-aio:~/kolla-ansible$ docker exec openvswitch_vswitchd ovs-ofctl dump-flows  br-ex
NXST_FLOW reply (xid=0x4):
 cookie=0x71658c366e3667de, duration=3358.478s, table=0, n_packets=0, n_bytes=0, idle_age=3363, priority=2,in_port=2 actions=drop
 cookie=0x71658c366e3667de, duration=3358.481s, table=0, n_packets=0, n_bytes=0, idle_age=3363, priority=0 actions=NORMAL
