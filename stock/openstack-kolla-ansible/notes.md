





## config standard sur la machine "edge" :

``` bash
ip a
1: lo: inet 127.0.0.1/8
2: eth0: inet 10.20.1.230/24
3: eth1: inet 10.20.10.18/24
4: eth2: inet 10.20.11.181/24
```

``` bash
ip route
default via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.230 metric 100 
100.125.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.230 metric 100 
100.126.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.230 metric 100 
169.254.169.254 via 10.20.1.254 dev eth0 proto dhcp src 10.20.1.230 metric 100 

10.20.1.0/24 dev eth0 proto kernel scope link src 10.20.1.230 metric 100 
10.20.1.1 dev eth0 proto dhcp scope link src 10.20.1.230 metric 100 
10.20.1.254 dev eth0 proto dhcp scope link src 10.20.1.230 metric 100 

10.20.10.0/24 dev eth1 proto kernel scope link src 10.20.10.18 metric 100 

10.20.11.0/24 dev eth2 proto kernel scope link src 10.20.11.181 metric 100 
```

Test de ping des passerelles :
``` bash
ping 10.20.11.1
# --> OK

ping 10.20.11.1
# --> OK
```


https://stackoverflow.com/questions/19817506/openstack-quantum-vm-able-to-ping-br-ex-but-not-outside-network

``` bash
sudo -i 

echo 1 > /proc/sys/net/ipv4/ip_forward

tee -a /etc/sysctl.conf <<EOF
#net.ipv4.conf.eth0.proxy_arp = 1
net.ipv4.ip_forward = 1
EOF
```

If I run tcpdump on br-ex I getting ARP request only:

``` bash
ip route
192.168.200.0/24 dev br-ex  scope link
192.168.200.0/24 dev eth1  scope link
169.254.0.0/16 dev eth0  scope link  metric 1002
169.254.0.0/16 dev eth1  scope link  metric 1003
172.16.0.0/16 dev eth0  proto kernel  scope link  src 172.16.100.3
default via 172.16.100.1 dev eth0
```

Configured eth1 with following options:

DEVICE=eth1
ONBOOT=yes
HWADDR=08:00:27:3E:9F:A5
TYPE=OVSPort
DEVICETYPE=ov
OVS_BRIDGE=br-ex
NM_CONTROLLED=no
IPV6INIT=no

Reboot this box, and after reboot

manually set ip on br-ex
``` bash
ip addr add 192.168.200.10/24 dev br-ex
```