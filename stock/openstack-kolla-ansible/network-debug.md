


Ajout d'une interface vers le réseau externe
``` bash
openstack router set --external-gateway ${EXTERNAL_NETWORK_NAME}  ${ROUTER_NAME} 
# ...
# | id                      | 932a82c7-bdc1-4d35-bfd8-844f63ea75e3 |
# ...
```
 Relever l'adresse IP externe du router :
``` bash
openstack router  show -f json ${ROUTER_NAME} 
```

Dans la configuration d'openvswitch, un port a été créé sur `br-int` :
``` bash
docker exec openvswitch_vswitchd ovs-vsctl show
# ...
#     Bridge br-int
# ...
#         Port qg-7afe1b46-e1
#             tag: 1
#             Interface qg-7afe1b46-e1
#                 type: internal
# ...
```

Un namespace a également été créé au niveau Linux, au format `qrouter-<ROUTER_ID>`
``` bash
sudo ip netns
# qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 (id: 0)
```

Dans les iptables, on voit  :

- des règles mise en place pour neutron et openvswitch
- des règles associées à une tap, dont l'id correspond à l'interface dans le namespace :
    - tap7afe1b46-e1
    - qg-7afe1b46-e1

``` bash
Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out tap7afe1b46-e1 --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tap7afe1b46-e1 --physdev-is-bridged /* Accept all packets when port is trusted. */
```

Mais aucune trace de la tap au niveau système ...


Observer sa configuration
``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 ip a
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#     inet 127.0.0.1/8 scope host lo
#        valid_lft forever preferred_lft forever
#     inet6 ::1/128 scope host 
#        valid_lft forever preferred_lft forever
# 13: qg-7afe1b46-e1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/ether fa:16:3e:4b:00:fc brd ff:ff:ff:ff:ff:ff
#     inet 10.20.11.184/24 brd 10.20.11.255 scope global qg-7afe1b46-e1
#        valid_lft forever preferred_lft forever
#     inet6 fe80::f816:3eff:fe4b:fc/64 scope link 
#        valid_lft forever preferred_lft forever
```


``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 ip route
# default via 10.20.11.1 dev qg-7afe1b46-e1 proto static 
# 10.20.11.0/24 dev qg-7afe1b46-e1 proto kernel scope link src 10.20.11.184 
```

La route par défaut est bien sur l'interface `13: qg-7afe1b46-e1:`


``` bash
ip a
```
Pas de trace de l'interface. 

Tests de ping :

- Terminal 1 : lancer les pings :
``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 ping 10.20.11.184
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 ping 10.20.11.1
```
- Terminal 2 : observer avec tcpDump, sur la boucle locale `lo` :
``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 tcpdump -i lo -l -n  icmp
# ...
# 13:51:40.873506 IP 10.20.11.184 > 10.20.11.184: ICMP echo request, id 56335, seq 31, length 64
# 13:51:40.873536 IP 10.20.11.184 > 10.20.11.184: ICMP echo reply, id 56335, seq 31, length 64
# 13:51:41.893527 IP 10.20.11.184 > 10.20.11.184: ICMP echo request, id 56335, seq 32, length 64
# 13:51:41.893557 IP 10.20.11.184 > 10.20.11.184: ICMP echo reply, id 56335, seq 32, length 64
# ...
```
Ca passe pour l'IP du router, pas pour la passerelle

Si on lance le tcpdump sur l'interface elle même :
``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 tcpdump -i qg-7afe1b46-e1 -l -n  icmp
```
On n'obtient rien. ni sur l'IP du router `10.20.11.184`, ni sur la passerelle `10.20.11.1`


---
## Test de connectivité avec un 2eme routeur


``` bash
openstack router create ${ROUTER_2_NAME}
# | id                      | 402c5616-45b0-443d-b752-1d27da5fb0c9 |
```

Ajoute d'un interface vers le réseau externe
``` bash
openstack router set --external-gateway ${EXTERNAL_NETWORK_NAME}  ${ROUTER_2_NAME} 
```


openvswitch / br-int
``` yaml
        Port qg-737f8352-ae
            tag: 1
            Interface qg-737f8352-ae
                type: internal
```
Système
``` bash
 ip netns
qrouter-402c5616-45b0-443d-b752-1d27da5fb0c9 (id: 1)
qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 (id: 0)
```

Dans iptables
``` bash
Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out tap7afe1b46-e1 --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tap7afe1b46-e1 --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out tap737f8352-ae --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tap737f8352-ae --physdev-is-bridged /* Accept all packets when port is trusted. */
```


Test de ping entre les routeurs :

- Terminal 1 : lancer les pings depuis le deuxième routeur, vers le premier
``` bash
sudo ip netns exec qrouter-402c5616-45b0-443d-b752-1d27da5fb0c9 ping 10.20.11.184
```
- Terminal 2 : observer avec tcpDump, l'interface kdu premier router 
``` bash
sudo ip netns exec qrouter-932a82c7-bdc1-4d35-bfd8-844f63ea75e3 tcpdump  -l -n  icmp
# ...
# 14:16:43.653544 IP 10.20.11.135 > 10.20.11.184: ICMP echo request, id 38676, seq 25, length 64
# 14:16:43.653595 IP 10.20.11.184 > 10.20.11.135: ICMP echo reply, id 38676, seq 25, length 64
# 14:16:44.677528 IP 10.20.11.135 > 10.20.11.184: ICMP echo request, id 38676, seq 26, length 64
# 14:16:44.677577 IP 10.20.11.184 > 10.20.11.135: ICMP echo reply, id 38676, seq 26, length 64
# ...
```

Le ping fonctionne bien, à traver la couche réseau.
- Terminal 1 : lancer les pings depuis le deuxième routeur, vers le premier
``` bash
sudo ip netns exec qrouter-402c5616-45b0-443d-b752-1d27da5fb0c9 ping 10.20.11.1
# From 10.20.11.135 icmp_seq=1 Destination Host Unreachable
# From 10.20.11.135 icmp_seq=2 Destination Host Unreachable
# From 10.20.11.135 icmp_seq=3 Destination Host Unreachable
```

Test de tcpdump sur br-int:
``` bash
sudo tcpdump  -i br-int -l -n  icmp
# tcpdump: br-int: That device is not up
```

``` bash
sudo ip netns exec qrouter-402c5616-45b0-443d-b752-1d27da5fb0c9 ip route get 10.20.11.1
# 10.20.11.1 dev qg-737f8352-ae src 10.20.11.135 uid 0 
#     cache 
```


---
## recherche au niveau d'Openstack
``` bash
openstack port list -c 'MAC Address' -c 'Fixed IP Addresses' -c 'Status'
+-------------------+-----------------------------------------------------------------------------+--------+
| MAC Address       | Fixed IP Addresses                                                          | Status |
+-------------------+-----------------------------------------------------------------------------+--------+
| fa:16:3e:70:55:3e | ip_address='10.20.11.156', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | N/A    |
| fa:16:3e:b5:b6:3f | ip_address='10.20.11.135', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | ACTIVE |
| fa:16:3e:4b:00:fc | ip_address='10.20.11.184', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | ACTIVE |
+-------------------+-----------------------------------------------------------------------------+--------+
```
C'est quoi la `ip_address='10.20.11.156'``.
Elle est down, et on ne la ping pas ni depuis le système, ni depuis les routeurs.



---
## création d'un réseau et d'un sous réeau

# creation d'un reseau
openstack network create ${NETWORK_NAME}


rien au niveau linux, openvswitch ou iptables

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 \
  --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 


Openvswitch  / brint
``` bash
        Port tap7de69c40-b8
            tag: 2
            Interface tap7de69c40-b8
                type: internal
```  

iptables et , rien
``` bash
ip netns
qdhcp-ae3b39ca-d2d2-4ae8-8b83-e8cab8608982 (id: 2)
```

passerelle avec router
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}  
``` bash
        Port qg-7afe1b46-e1
            tag: 1
            Interface qg-7afe1b46-e1
                type: internal
```
toujours rien sur système
``` bash
sudo ip netns exec qdhcp-ae3b39ca-d2d2-4ae8-8b83-e8cab8608982 ping 10.20.11.1
PING 10.20.11.1 (10.20.11.1) 56(84) bytes of data.
--> KO
```
---
## 
### Lancement d'une instance


Ajout d'une image cirros.
wget http://download.cirros-cloud.net/0.5.2/cirros-0.5.2-x86_64-disk.img
openstack image create "cirros" \
  --file cirros-0.5.2-x86_64-disk.img  \
  --disk-format qcow2 \
  --container-format bare \
  --public
openstack flavor create --ram 256 --disk 1 --vcpus 1 cirros

``` bash
# mémorisation de l'ID du gabarit cirros
FLAVOR_CIRROS_ID=$(openstack flavor list -f value -c Name | grep  cirros)
echo ${FLAVOR_CIRROS_ID}

# recherche de l'id de l'image cirros
IMAGE_CIRROS_ID=$(openstack image list -f value -c Name | grep 'cirros')
echo ${IMAGE_CIRROS_ID}


# lancement d'une instance
# creation d'un instance de test
export INSTANCE_NAME=srv-1
echo ${INSTANCE_NAME}
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --nic net-id=${NETWORK_NAME} \
   --wait \
   ${INSTANCE_NAME}

# verification que l'instance est active
openstack server show ${INSTANCE_NAME} -c status -f value


création de toutes les interfaces au niveau du système
``` bash
19: qbrd3fc8879-37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
    link/ether ae:49:c1:e5:aa:98 brd ff:ff:ff:ff:ff:ff

20: qvod3fc8879-37@qvbd3fc8879-37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master ovs-system state UP group default qlen 1000
    link/ether e2:cf:5d:d3:09:49 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::e0cf:5dff:fed3:949/64 scope link 
       valid_lft forever preferred_lft forever

21: qvbd3fc8879-37@qvod3fc8879-37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master qbrd3fc8879-37 state UP group default qlen 1000
    link/ether 82:5d:1b:11:93:69 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::805d:1bff:fe11:9369/64 scope link 
       valid_lft forever preferred_lft forever

22: tapd3fc8879-37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master qbrd3fc8879-37 state UNKNOWN group default qlen 1000
    link/ether fe:16:3e:cb:dd:14 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::fc16:3eff:fecb:dd14/64 scope link 
       valid_lft forever preferred_lft forever
```
Openvswitch
``` bash
        Port tap7de69c40-b8
            tag: 2
            Interface tap7de69c40-b8
                type: internal
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
        Port qg-737f8352-ae
            tag: 1
            Interface qg-737f8352-ae
                type: internal
        Port qr-3c6dd069-65
            tag: 2
            Interface qr-3c6dd069-65
                type: internal
        Port qg-7afe1b46-e1
            tag: 1
            Interface qg-7afe1b46-e1
                type: internal
        Port qvod3fc8879-37
            tag: 2
            Interface qvod3fc8879-37
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
```

routeur : tap7afe1b46-e1

ne fonctionne tuojours pas :
``` bash
 sudo ip netns exec qdhcp-ae3b39ca-d2d2-4ae8-8b83-e8cab8608982 ping 10.20.11.1
PING 10.20.11.1 (10.20.11.1) 56(84) bytes of data.
```

---
``` bash
openstack port list -c 'MAC Address' -c 'Fixed IP Addresses' -c 'Status'
+-------------------+-----------------------------------------------------------------------------+--------+
| MAC Address       | Fixed IP Addresses                                                          | Status |
+-------------------+-----------------------------------------------------------------------------+--------+
| fa:16:3e:70:55:3e | ip_address='10.20.11.156', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | N/A    |
| fa:16:3e:6b:b0:e0 | ip_address='172.16.1.1', subnet_id='23438dd8-4116-47cb-b187-69d3b699c022'   | ACTIVE |
| fa:16:3e:b5:b6:3f | ip_address='10.20.11.135', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | ACTIVE |
| fa:16:3e:4b:00:fc | ip_address='10.20.11.184', subnet_id='42f9b579-0cb5-463e-8298-9e484e19e655' | ACTIVE |
| fa:16:3e:ee:8b:59 | ip_address='172.16.1.2', subnet_id='23438dd8-4116-47cb-b187-69d3b699c022'   | ACTIVE |
| fa:16:3e:cb:dd:14 | ip_address='172.16.1.58', subnet_id='23438dd8-4116-47cb-b187-69d3b699c022'  | ACTIVE |
+-------------------+-----------------------------------------------------------------------------+--------+

```


---
## création d'une IP flottante de le réseau external

``` bash
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
# ...
# | name                | 10.20.11.187  
# ...
```
On ne voit rien de créé au niveau système : iptables, ovs-flow, route


---
## ajout de groupe de sécu et IP flottante

export SECGROUP_NAME=sg-1
echo ${SECGROUP_NAME}
openstack security group create --description 'Security Group' ${SECGROUP_NAME}
openstack security group rule create --proto icmp --dst-port 0 ${SECGROUP_NAME}
openstack security group rule create --proto tcp --dst-port 22 ${SECGROUP_NAME}

openstack server add security group srv-1 sg-1

openstack server list
+--------------------------------------+-------+--------+-------------------+--------+--------+
| ID                                   | Name  | Status | Networks          | Image  | Flavor |
+--------------------------------------+-------+--------+-------------------+--------+--------+
| 423fd117-b112-4627-996c-cabd08de5674 | srv-1 | ACTIVE | net-1=172.16.1.84 | cirros | cirros |
+--------------------------------------+-------+--------+-------------------+--------+--------+
sudo ip netns exec qdhcp-e5a0fc0d-680a-45f8-9f14-3f70bd9b95f4  ping 172.16.1.84
--> OK VM
--> OK interface router
--> KO gateway router : C'est normal

sudo ip netns exec qrouter-532a14f0-a2d8-4f64-95a1-060cdb333b57 ping 10.20.11.84
--> KO VM : Normal ??
--> KO interface router :  Normal ??
--> OK gateway router 


---
## Reste
```
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=$(openstack floating ip list -f value -c 'Floating IP Address' -c 'Fixed IP Address' | grep None | head -1 | awk '{ print $1 }')
echo ${FREE_FLOATING_IP}
openstack server add floating ip ${INSTANCE_NAME} ${FREE_FLOATING_IP}
```

```bash
ping -c 3 ${FREE_FLOATING_IP}

ssh cirros@${FREE_FLOATING_IP}
# gocubsgo





Test de ping vers la passerelle


``` bash
        Port qg-8ac2b980-b4
            tag: 1
            Interface qg-8ac2b980-b4
                type: internal
        Port qg-07179438-f6
            tag: 1
            Interface qg-07179438-f6
                type: internal
```


```
# ajout d'uune interface vers le réseau externe
# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}   
---
## Création d'un network



# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}



---
## commandes

``` bash
export OS_PROJECT_NAME=validation
export OS_USERNAME=validation
export OS_PASSWORD=stack
```


``` bash
PROJECT_NAME=validation
NETWORK_NAME=net-validation
SUBNET_NAME=subnet-validation
ROUTER_NAME=router-validation
SECGROUP_NAME=sg-validation
KEYPAIR_NAME=keypair-validation
VOLUME_NAME=vol-validation
INSTANCE_NAME=server-validation
LB_NAME=lb-validation

EXTERNAL_NETWORK_NAME=external


# creation d'un reseau
openstack network create ${NETWORK_NAME}

# creation d'un sous réseau
openstack subnet create --network ${NETWORK_NAME} \
  --dns-nameserver 192.44.75.10 --dns-nameserver 192.108.115.2 \
  --gateway 172.16.1.1 \
  --subnet-range 172.16.1.0/24 \
  ${SUBNET_NAME} 

# creation d'un routeur
openstack router create ${ROUTER_NAME}

# ajout d'une interface vers le reseau "selfservice"
openstack router add subnet ${ROUTER_NAME} ${SUBNET_NAME}

# ajout d'uune interface vers le réseau externe
openstack router set ${ROUTER_NAME} --external-gateway ${EXTERNAL_NETWORK_NAME}

openstack router show  ${ROUTER_NAME}

# test du router
# openstack router show  ${ROUTER_NAME} -c external_gateway_info -f value |
#ping -c 3 $ROUTER_EXTERNEL_IP
```


``` bash
# creation des groupes de sécurité
echo ${SECGROUP_NAME}
openstack security group create --description 'Security Group' ${SECGROUP_NAME}
openstack security group rule create --proto icmp --dst-port 0 ${SECGROUP_NAME}
openstack security group rule create --proto tcp --dst-port 22 ${SECGROUP_NAME}
```

``` bash
# creation d'une keypair
openstack keypair create ${KEYPAIR_NAME} > tmp/${KEYPAIR_NAME}.pem
cat tmp/${KEYPAIR_NAME}.pem
chmod 400  tmp/${KEYPAIR_NAME}.pem 

```






# affectation d'une IP, et ouverture des acces
openstack floating ip create ${EXTERNAL_NETWORK_NAME}
FREE_FLOATING_IP=$(openstack floating ip list -f value -c 'Floating IP Address' -c 'Fixed IP Address' | grep None | head -1 | awk '{ print $1 }')
echo ${FREE_FLOATING_IP}
openstack server add floating ip ${INSTANCE_NAME} ${FREE_FLOATING_IP}
```

```bash
ping -c 3 ${FREE_FLOATING_IP}

ssh cirros@${FREE_FLOATING_IP}
# gocubsgo


ssh -i tmp/${KEYPAIR_NAME}.pem cirros@${FREE_FLOATING_IP}

```


on a un bridge, mais pour quelle tap ???
``` bash
sudo brctl show
bridge name	bridge id		STP enabled	interfaces
qbrd8456558-02		8000.7e71840f4362	no		qvbd8456558-02
							tapd8456558-02
```



---
## tests de ping depuis un routeur vers la passerelle

lancer un ping depuis le namesapce du routeur, vers la passerelle
``` bash
sudo ip netns exec qrouter-de643622-4501-418b-99fc-de518b290555 ping 10.20.11.1
```
et dans une autre fenêtre  un tcpdump sur l'interface
``` bash
sudo tcpdump -i eth2 --immediate-mode -e -n 
```

``` bash
08:32:43.251458 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 1, length 64
08:32:44.256931 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 2, length 64
08:32:45.280937 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 3, length 64
08:32:46.304909 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 4, length 64
08:32:47.328943 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 5, length 64
08:32:48.293000 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype ARP (0x0806), length 42: Request who-has 10.20.11.1 tell 10.20.11.116, length 28
08:32:48.352917 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 6, length 64
08:32:49.312852 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype ARP (0x0806), length 42: Request who-has 10.20.11.1 tell 10.20.11.116, length 28
08:32:49.376916 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 7, length 64
08:32:50.340845 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype ARP (0x0806), length 42: Request who-has 10.20.11.1 tell 10.20.11.116, length 28
08:32:50.400915 fa:16:3e:d4:86:1c > fa:16:3e:74:8f:33, ethertype IPv4 (0x0800), length 98: 10.20.11.116 > 10.20.11.1: ICMP echo request, id 63808, seq 8, length 64
08:32:51.425114 fa:16:3e:d4:86:1c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 42: Request who-has 10.20.11.1 tell 10.20.11.116, length 28
08:32:52.448864 fa:16:3e:d4:86:1c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 42: Request who-has 10.20.11.1 tell 10.20.11.116, length 28

```


``` bash
sudo ip netns exec qrouter-de643622-4501-418b-99fc-de518b290555 arping 10.20.11.181
```


sudo tcpdump -n -e -ttt -i br100 arp

Afficher les tables arp  des bridge OVS.

 docker exec openvswitch_vswitchd  ovs-appctl fdb/show br-ex
 port  VLAN  MAC                Age
    1     0  fa:16:3e:74:8f:33   17



docker exec openvswitch_vswitchd  ovs-appctl fdb/show br-ex
 port  VLAN  MAC                Age
    2     0  fa:16:3e:2f:38:20  114
    1     0  0a:00:27:00:00:03   56  --> c'est la MAC qui correspond à la passerelle





sudo tcpdump -i eth3 --immediate-mode -e -n 

sudo ip netns exec qrouter-de643622-4501-418b-99fc-de518b290555 arping 10.20.11.1


https://sureshkvl.gitbooks.io/devstack-for-beginners/content/neutron_networking_explained.html  
https://docs.openstack.org/devstack/queens/guides/neutron.html

---
## iptable pour ping en local

``` bash
sudo ip route add 10.20.11.0/24 dev br0 proto kernel scope link src 10.20.11.1
```

Ne marche pas, à virer.
``` bash
sudo -i
iptables --table nat -A POSTROUTING -s 10.20.11.0/24 -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward
```

echo 1 > /proc/sys/net/ipv4/conf/ens160/proxy_arp
iptables -t nat -A POSTROUTING -o ens160 -j MASQUERADE

sudo bash
echo 1 > /proc/sys/net/ipv4/ip_forward
echo 1 > /proc/sys/net/ipv4/conf/eth0/proxy_arp
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

Adding a route to client machine to openstack VM, helped me.
