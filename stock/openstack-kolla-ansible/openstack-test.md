

ssh-add ~/.ssh/openstack/os-ebraux.pem


edge :
 ssh ubuntu@10.129.177.77

kos-aio
 ssh ubuntu@10.129.179.70


# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
  ethernets:
    eno1:
      dhcp4: false
      dhcp6: false
      addresses:
          - 10.20.11.12/24
      gateway4: 10.20.11.1
      nameservers:
        addresses:
          - 192.44.75.10
          - 192.108.115.2