

Différents agents :

- Open vSwitch agent 
    - managing virtual switches, connectivity among them,
    - and interaction via virtual ports with other network components such as namespaces, Linux bridges, and underlying interfaces.
  
- DHCP agent 
    - managing the qdhcp namespaces.
    - The qdhcp namespaces provide DHCP services for instances using project networks.

- L3 agent : 
    - managing the qrouter namespaces.
    - The qrouter namespaces provide routing between project and external networks and among project networks.
    - They also route metadata traffic between instances and the metadata agent. Metadata agent handling metadata operations for instances.


Linux bridges handling security groups. Due to limitations with Open vSwitch and iptables, the Networking service uses a Linux bridge to manage security groups for instances.

---
## Architecture
OVS créé 3 bridges :

- br-ex : Interconnexion avec réseau publics
- br-int : "internal bridge" Permet de connecter les instances
- br-tun : tunnel d'interconnexion entre les hosts (dans le cas d'un cluster), permet d'interconnecter les instances dans un même réseau privé sur plusieurs noeuds

Liens entre bridges (Patchs)

 - "patch-tun" : entre br-int (Interface int-br-ex) et br-tun (Interface patch-int )
 - "int-br-ex" : entre br-int (Interface patch-tun) et br-ex (Interface phy-br-ex)
 
---
## Router

- Sont gérés par le L3 agent
- Namespace Linux,
    - au format "qrouter-<ROUTER_ID>"
    - contient uniquement l'interafce `lo`
- Interagi avec le Metadata-agent

A l'ajout d'une gateway vers le réseau externe :
- Création d'une interfaces dans br-int, au format `qg-xxxxxxxx-xx`.
- Création d'entrée dans la Chain `neutron-openvswi-FORWARD`, pour le traffic IN et OUT

Le namespace contient une interface pour la gateway: 

- nom "qg-xxxxxxxx-xx" (aucun lien avec le nom du routeur)
- IP: adresse IP du routeur, dans le subnet external


 OU est créée la tap `tap6c2c2fef-e2` ???$


 
- 
Commande :
``` bash
ip netns
# 

ip netns exex ${ROUTER_NAMESPACE_ID} ${COMMAND}
```
Commandes utiliées : 

- ip a : 
- ip route
- tcpdump  


---
## Network et subnet

DHCP
- géré par le "DHCP agent"
- "qdhcp" namespaces. un par subnet
- Par défaut, 1 interface créé dans br-int
  - `tap`

---
## passerelle



---
## Groupes de sécurité


---
## IP flottantes

---
## Instances

- Interface de la VM, non visible depuis le système
- Attachée à br-int, à travers un bridge "linux-bridge"
- Sur br-int, une interface :
  - `qvo`
- Sur le bridge LinuxBridge, 2 interfaces
  - `tap` : associée à l'interface réseau de l'instance
  - `qvb` : associée à br-int

La gestion de la communication entre instances se fait ensuite via le patchs entre br-int et br-tun, qui permet de propager les réseaux virtuels entre les noeuds du cluster. Aucune interface supplémentaire  n'est créée sur br-tun.



-  

---
## Ressources 

- [https://docs.openstack.org/liberty/networking-guide/scenario-classic-ovs.html](https://docs.openstack.org/liberty/networking-guide/scenario-classic-ovs.html)
- [https://docs.openstack.org/ocata/networking-guide/deploy-ovs-selfservice.html](https://docs.openstack.org/ocata/networking-guide/deploy-ovs-selfservice.html)



---
## Annexes

### Commandes

- Pour afficher les iptables : `sudo iptables -L -n -v`


### Régle sIptables par défaut
```bash
Chain INPUT (policy ACCEPT 1580K packets, 1628M bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain OUTPUT (policy ACCEPT 997K packets, 171M bytes)
 pkts bytes target     prot opt in     out     source               destination         
```
```
### Règles iptables créée pour Neutron

``` bash
Chain INPUT (policy ACCEPT 345K packets, 1512M bytes)
 pkts bytes target     prot opt in     out     source               destination         
32950 7266K neutron-openvswi-INPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 neutron-filter-top  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
    0     0 neutron-openvswi-FORWARD  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain OUTPUT (policy ACCEPT 290K packets, 52M bytes)
 pkts bytes target     prot opt in     out     source               destination         
32748 7269K neutron-filter-top  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
32748 7269K neutron-openvswi-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-filter-top (2 references)
 pkts bytes target     prot opt in     out     source               destination         
32748 7269K neutron-openvswi-local  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-INPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-OUTPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-local (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-sg-chain (0 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-openvswi-sg-fallback (0 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      *       0.0.0.0/0            0.0.0.0/0            /* Default drop rule for unmatched traffic. */
```

### Règles Iptable pour une Gateway de router
``` bash
Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out  --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tap6c2c2fef-e2 --physdev-is-bridged /* Accept all packets when port is trusted. */
```

### Règles Iptable pour une passerelle de router vers un subnet

``` bash
Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out tap33729761-08 --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tap33729761-08 --physdev-is-bridged /* Accept all packets when port is trusted. */
```