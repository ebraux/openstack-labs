network:
  version: 2
  renderer: networkd
  ethernets:
    eth2:
      dhcp4: no
  bridges:
    br0:
      interfaces: [eth2, tap-local, tap-br-ex]
      dhcp4: no
      parameters:
        stp: false  # Spanning Tree Protocol désactivé
      dhcp4: false  # Désactiver le DHCP par défaut pour toutes les interfaces dans le bridge
      dhcp4-overrides:
        tap0:
          dhcp4: true  # Activer le DHCP uniquement pour l'interface tap0        


sudo netplan apply


https://serverfault.com/questions/906810/tap-interface-using-netplan

for i in `seq 0 5`; do
    ip tuntap add name tap$i mode tap;
    ip link set up dev tap$i;
    ip link set tap$i master br0;
done;


Effectivement, vous avez raison. Netplan ne prend pas en charge directement l'ajout d'interfaces de type `tap` ou `veth` à un bridge. Netplan est principalement conçu pour la configuration des interfaces réseau physiques et les bridges.

Pour ajouter des interfaces `tap` ou `veth` à un bridge, vous devrez généralement le faire à l'aide d'autres outils système, tels que `ip` ou `brctl`. Voici comment vous pouvez procéder :


Créez le bridge et ajoutez l'interface physique à celui-ci :
``` bash
sudo ip link add br0 type bridge
sudo ip link set dev br0 up
sudo ip link set dev eth2 master br0
```

1. Créez les interfaces `tap` ou `veth` :
   
   ```bash
   sudo ip tuntap add tap-local mode tap
   sudo ip tuntap add tap-brex mode tap
   ```

2. Activez les interfaces :

   ```bash
   sudo ip link set tap-local up
   sudo ip link set tap-brex up
   ```

3. Ajoutez les interfaces au bridge :

   ```bash
   sudo brctl addif br0 tap-local
   sudo brctl addif br0 tap-brex
   ```

Mettre en place IP / route
sudo ip addr add 10.20.11.2/24 dev tap-local
sudo ip route add 10.20.11.0/24 via 10.20.11.1


Avec cette méthode, vous devriez pouvoir ajouter les interfaces `tap0` et `tap1` au bridge `br0`. Assurez-vous également de configurer les adresses IP et d'autres paramètres réseau nécessaires pour les interfaces `tap` si vous en avez besoin.

Merci de votre patience, et désolé pour la confusion précédente.

---
## pour redre permanent
Pour rendre la configuration du bridge et des interfaces virtuelles permanente, vous devrez généralement ajouter ces commandes de configuration dans des scripts d'initialisation ou des fichiers de configuration système qui sont exécutés au démarrage du système.

Pour cela, vous pouvez créer un script d'initialisation qui contient les commandes de configuration nécessaires, puis le rendre exécutable et l'ajouter au démarrage du système.

Voici comment vous pouvez procéder sur un système utilisant systemd :

1. Créez un script d'initialisation, par exemple `/etc/network-config.sh`, et ajoutez les commandes de configuration nécessaires à l'intérieur :

```bash
#!/bin/bash

# Création du bridge
ip link add br0 type bridge
ip link set dev br0 up
ip link set dev eth0 master br0

# Création des interfaces virtuelles
ip tuntap add tap0 mode tap
ip tuntap add tap1 mode tap

# Activation des interfaces virtuelles
ip link set dev tap0 up
ip link set dev tap1 up

# Ajout des interfaces virtuelles au bridge
ip link set dev tap0 master br0
ip link set dev tap1 master br0

# Configuration de l'adresse IP statique sur tap0
ip addr add 192.168.1.100/24 dev tap0

# Ajout de la route par défaut si nécessaire
ip route add default via 192.168.1.1
```

2. Rendez le script exécutable :

```bash
sudo chmod +x /etc/network-config.sh
```

3. Ensuite, créez un service systemd pour exécuter ce script au démarrage :

Créez un fichier de service `/etc/systemd/system/network-config.service` avec le contenu suivant :

```
[Unit]
Description=Network Configuration
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
ExecStart=/etc/network-config.sh

[Install]
WantedBy=multi-user.target
```

4. Activez le service :

```bash
sudo systemctl enable network-config.service
```

Une fois que vous avez suivi ces étapes, la configuration du bridge et des interfaces virtuelles, y compris l'adresse IP statique, sera appliquée au démarrage du système. Assurez-vous de personnaliser les adresses IP, les routes et les autres paramètres selon vos besoins spécifiques.


---
# avec des veth
https://kifarunix.com/deploy-multinode-openstack-using-kolla-ansible/

Les interfaces `tap` et `veth` sont deux types d'interfaces virtuelles utilisées dans les environnements réseau Linux, mais elles ont des fonctionnalités légèrement différentes :

1. **TAP (Network Tap)** :
   - Une interface TAP est généralement utilisée pour émuler une interface réseau complète.
   - Elle est souvent utilisée dans les environnements de virtualisation, tels que les machines virtuelles, pour permettre à la machine virtuelle de communiquer avec le réseau de l'hôte.
   - Les paquets envoyés à une interface TAP peuvent être interceptés par un logiciel de capture réseau (comme Wireshark) ou être analysés par un autre logiciel avant d'être transmis sur le réseau.
   - Les interfaces TAP sont souvent utilisées pour l'émulation de périphériques réseau, permettant à un logiciel ou à une machine virtuelle de communiquer comme s'il était connecté directement à un réseau physique.

2. **VETH (Virtual Ethernet)** :
   - Une paire d'interfaces `veth` est utilisée pour créer une connexion de pontage virtuelle entre deux points du réseau.
   - Une interface `veth` est généralement utilisée par paire : chaque paire comporte deux interfaces, l'une à chaque extrémité de la connexion virtuelle.
   - Les paires d'interfaces `veth` sont souvent utilisées pour connecter des conteneurs Docker, des espaces de noms réseau Linux ou d'autres environnements de virtualisation.
   - Les paquets envoyés à une interface `veth` sont transmis directement à l'autre interface de la paire, créant ainsi une connexion de pontage virtuelle.

En résumé, les interfaces TAP sont généralement utilisées pour émuler des interfaces réseau complètes, tandis que les paires d'interfaces `veth` sont utilisées pour créer des connexions de pontage virtuelles entre deux points du réseau.

---
## avec openvswitch

https://blog.debugo.fr/gestion-reseau-complexe-kvm-openvswitch/
le pb, c'est que ça ferait une base en local, et un ebase dans Kolla ...


---
## route traffic
https://rahulait.wordpress.com/2016/06/27/manually-routing-traffic-from-br-ex-to-internet-devstack/