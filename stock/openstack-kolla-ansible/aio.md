# Installation de Kolla-ansible, mode AIO


---
## Définir les versions de kolla et d'ansible

(lien vers la correspondance kolla-ansible / ansible)

La version du module Python kolla-ansible est fonction de la version d'OpenStack qu'il permet de déployer.

17.* : Bobcat series (exemple 17.1.0)
16.* : Antelop (exemple 16.3.0)
15.* : Zed series (exemple 15.4.0)
14.* : Yoga series

Voir l'ensemble des tags pour kolla-ansible sur Github : [https://github.com/openstack/kolla-ansible/tags](https://github.com/openstack/kolla-ansible/tags)

Le module Python kolla-ansible est disponible :

- via opendev.org https://opendev.org/openstack/kolla-ansible
- via GitHub https://github.com/openstack/kolla-ansible
- via Pypi, le dépôt de module Python https://pypi.org/project/kolla-ansible/

``` 
pip install 'ansible>=6,<8'
pip install git+https://opendev.org/openstack/kolla-ansible@stable/2023.1
```
```
pip install -U 'ansible<6.0'
pip install git+https://github.com/openstack/kolla-ansible@15.4.0
```  
---
## Prérequis Réseau


2 interfaces sont nécessaires, dont une sans adresse IP.

### Interface physique

Désactiver le DHCP
``` bash
ip a
cd /etc/netplan/
sudo vim 50-cloud-init.yaml 
# desactiver dhcp pour la deuxieme interafce
sudo netplan apply 
```

## Interafce "virtuelle"


``` bash
Create Dummy Interface
sudo ip tuntap add mode tap br_ex_port
sudo ip link set dev br_ex_port up
```

Avec netplan, créer un "Dummy Bridge" : 
``` bash
tee  /etc/netplan/50-br0.yaml > /dev/null <<EOF 
network:
  version: 2
  renderer: networkd

  bridges:
    br0:
      dhcp4: no
      dhcp6: no
      accept-ra: no
      interfaces: [ ]
      addresses:
        - 10.20.11.1/24
EOF        
```

``` bash
sudo netplan apply
ip a
```


### Interfaces dans Openstack

``` bash
for x in \
    3f664e5e-dd67-4fe5-b2cc-86c2918b6a2f \
    c44da3d5-00e7-4e41-af39-7010d70ac6a5; do
  echo $x
  openstack port set --no-security-group $x
  openstack port set --disable-port-security $x
done
```

- [https://kyle.pericak.com/openstack-aio-ka-vm.html](https://kyle.pericak.com/openstack-aio-ka-vm.html)

---
## Prérequis Disque

Cinder en mode LVM utilise un volume dédié
``` bash
sudo fdisk -l
sudo pvcreate /dev/vdb
sudo vgcreate cinder-volumes /dev/vdb
```

``` bash
sudo vgs
#   VG             #PV #LV #SN Attr   VSize   VFree  
#   cinder-volumes   1   0   0 wz--n- <50.00g <50.00g
```
---
## Prérequis système

Mise à jour du système 
``` bash
sudo apt update \
  && sudo apt upgrade -y \
  && [ -f /var/run/reboot-required ] && sudo systemctl reboot
```

Installation des packages 
``` bash
sudo apt install -y git python3-dev libffi-dev python3-venv gcc libssl-dev git python3-pip
```


---
## Préparation de l'environnement

Création du virtualenv
``` bash
cd ~
python3 -m venv $HOME/kolla-ansible
```

Activation du virtualenv
``` bash
cd kolla-ansible
source bin/activate
```

Update de pip
``` bash
pip install -U pip
```

---
## configuration de Ansible

Installation d'Ansible
``` bash
pip install -U 'ansible<6.0'
```

Configuration
```
tee  $HOME/ansible.cfg > /dev/null <<EOF 
[defaults]
host_key_checking=False
pipelining=True
forks=100
EOF
```

``` bash
cat  $HOME/ansible.cfg 
```

---
## Installation de Kolla-ansible

### Depuis de dépôt git

``` bash
pip --default-timeout=1000 install git+https://github.com/openstack/kolla-ansible@15.4.0
```
> Le timeout de pip et de 15s par défaut, le dépôt est assez gros.

Si ça bloque, télécharger le dépôt manuellement.


### Depuis un dépôt en local

En téléchargeant le dépôt manuellement avec git clone :
``` bash
git clone  https://github.com/openstack/kolla-ansible /tmp/pip-kolla-ansible
pip  install /tmp/pip-kolla-ansible
rm -rf /tmp/pip-kolla-ansible
```
Soit depuis le fichier zip téléchargé depuis [https://github.com/openstack/kolla-ansible/tree/15.4.0](https://github.com/openstack/kolla-ansible/tree/15.4.0), et copié sur la VM dans /tmp :
``` bash
curl -k https://codeload.github.com/openstack/kolla-ansible/zip/refs/tags/15.4.0  --output kolla-ansible-15.4.0.zip
sudo apt install -y unzip
unzip  /tmp/kolla-ansible-15.4.0.zip
export PBR_VERSION=15.4.0
pip  install /tmp/kolla-ansible-15.4.0/
rm -rf pip-kolla-ansible-15.4.0
```


---
## Configuration


``` bash
sudo mkdir /etc/kolla
sudo chown $USER:$USER /etc/kolla
cp $HOME/kolla-ansible/share/kolla-ansible/etc_examples/kolla/* /etc/kolla/

cp $HOME/kolla-ansible/share/kolla-ansible/ansible/inventory/all-in-one .
```


``` bash
cp -p /etc/kolla/globals.yml /etc/kolla/globals.yml.DIST
```

Editer /etc/kolla/globals.yml, pour obtenir le résultat ci dessous. Les  points les plus importants sont :

- kolla_internal_vip_address
- network_interface
- neutron_external_interface
- kolla_base_distro
- openstack_release
- nova_compute_virt_type
- enable_neutron_provider_networks: "yes"


``` bash
grep -vE '^$|^#' /etc/kolla/globals.yml
```
``` ini
---
workaround_ansible_issue_8743: yes
kolla_base_distro: "ubuntu"
kolla_internal_vip_address: "172.16.50.11"
network_interface: "enp0s8"
neutron_external_interface: "enp0s9"
neutron_plugin_agent: "openvswitch"
enable_openstack_core: "yes"
enable_hacluster: "no"
enable_haproxy: "no"
enable_cinder: "no"
nova_compute_virt_type: "qemu"
```
-> enlever heat
enable_central_logging
enable_fluentd


``` ini
---
workaround_ansible_issue_8743: yes
config_strategy: "COPY_ALWAYS"
kolla_base_distro: "ubuntu"
openstack_release: "zed"
kolla_internal_vip_address: "10.0.2.15"
kolla_internal_fqdn: "{{ kolla_internal_vip_address }}"
kolla_external_vip_address: "{{ kolla_internal_vip_address }}"
kolla_external_fqdn: "{{ kolla_external_vip_address }}"
kolla_container_engine: docker
network_interface: "eth1"
neutron_external_interface: "eth2"
neutron_plugin_agent: "openvswitch"
enable_openstack_core: "yes"
enable_hacluster: "no"
enable_haproxy: "no"
enable_mariadb: "yes"
enable_memcached: "yes"
enable_aodh: "no"
enable_barbican: "no"
enable_blazar: "no"
enable_ceilometer: "no"
enable_ceilometer_ipmi: "no"
enable_cells: "no"
enable_central_logging: "no"
enable_ceph_rgw: "no"
enable_cinder: "no"
enable_cinder_backup: "no"
enable_cinder_backend_hnas_nfs: "no"
enable_cinder_backend_lvm: "yes"
enable_cinder_backend_nfs: "no"
enable_cinder_backend_quobyte: "no"
enable_cinder_backend_pure_iscsi: "no"
enable_cinder_backend_pure_fc: "no"
enable_cinder_backend_pure_roce: "no"
enable_cloudkitty: "no"
enable_collectd: "no"
enable_cyborg: "no"
enable_designate: "no"
enable_destroy_images: "no"
enable_etcd: "no"
enable_fluentd: "no"
enable_freezer: "no"
enable_gnocchi: "no"
enable_gnocchi_statsd: "no"
enable_grafana: "no"
enable_ironic: "no"
enable_kuryr: "no"
enable_magnum: "no"
enable_manila: "no"
enable_manila_backend_generic: "no"
enable_manila_backend_hnas: "no"
enable_manila_backend_cephfs_native: "no"
enable_manila_backend_cephfs_nfs: "no"
enable_manila_backend_glusterfs_nfs: "no"
enable_mariabackup: "no"
enable_masakari: "no"
enable_mistral: "no"
enable_multipathd: "no"
enable_murano: "no"
enable_neutron_vpnaas: "no"
enable_neutron_sriov: "no"
enable_neutron_dvr: "no"
enable_neutron_qos: "no"
enable_neutron_agent_ha: "no"
nable_neutron_bgp_dragent: "no"
enable_neutron_provider_networks: "yes"
enable_neutron_segments: "no"
enable_neutron_sfc: "no"
enable_neutron_trunk: "no"
enable_neutron_metering: "no"
enable_neutron_infoblox_ipam_agent: "no"
enable_neutron_port_forwarding: "no"
enable_nova_serialconsole_proxy: "no"
enable_nova_ssh: "yes"
enable_octavia: "no"
enable_ovs_dpdk: "no"
enable_osprofiler: "no"
enable_prometheus: "no"
enable_proxysql: "no"
enable_redis: "no"
enable_sahara: "no"
enable_senlin: "no"
enable_skyline: "no"
enable_solum: "no"
enable_swift: "no"
enable_swift_s3api: "no"
enable_tacker: "no"
enable_telegraf: "no"
enable_trove: "no"
enable_trove_singletenant: "no"
enable_venus: "no"
enable_vitrage: "no"
enable_watcher: "no"
enable_zun: "no"
cinder_volume_group: "cinder-volumes"
nova_compute_virt_type: "qemu"

```



``` bash
cd ~/kolla-ansible
source bin/activate

kolla-genpwd
cat all-in-one
```

``` bash
cd ~/kolla-ansible
source bin/activate

kolla-ansible install-deps
# Installing Ansible Galaxy dependencies
# Starting galaxy collection install process
# ...
```




Verifier le fichier hosts, l'adresse assosiée au nom du serveur ne doit pas êtr celle de localhost
``` bash
sudo getent ahostsv4  ${hostname}
# 127.0.0.1       localhost
# 127.0.0.1      kos-aio
```

Il faut donc éditer le fichier /etc/hosts indiquer l'IP, et mettre en commentaire la ligne contenant le nom du serveur.

``` bash
cd ~/kolla-ansible
source bin/activate

kolla-ansible -i all-in-one bootstrap-servers
```

Cett opération modifie  le fichier /etc/hosts, pour ajouter 
l'adressesse IP de 'kolla_internal_vip_address' mappée sur le nom de l'hôte. Bien vérifier que le nom de l'hôte est bien mappé **uniquement** sur l'adresse IP renseignée dans `kolla_internal_vip_address` (Rabbit Mq ne supporte pas que le nom soit résolu par 2 IP).
``` bash
sudo getent ahostsv4  ${hostname}
127.0.0.1       localhost
10.0.2.15       kos-aio
 ```


``` bash
kolla-ansible -i all-in-one prechecks
```

Test du bon fonctionnement de docker.
``` bash
sudo docker run hello-world
sudo docker run quay.io/podman/hello
```

Si derrière un proxy, il faut configurer le proxy pour Docker
test ;

``` bash
# creation du dossier de configuration du service docker
sudo mkdir -p /etc/systemd/system/docker.service.d

# creation du fichier de configuration du proxy
sudo tee /etc/systemd/system/docker.service.d/proxy.conf << 'EOF'
[Service]
Environment='HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080' 'HTTP_PROXY=http://proxy.enst-bretagne.fr:8080' 'NO_PROXY=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr'
EOF

cat /etc/systemd/system/docker.service.d/proxy.conf

# relancer Docker
sudo systemctl daemon-reload
sudo systemctl restart docker
```

Ménage
``` bash
sudo docker ps -a
sudo docker container prune -f

sudo docker images
sudo docker image prune -af
```

Donner les droits d'accès aux commandes docker pour l'utilisateur, pour les session futures, ou recharcher la session.
``` bash
sudo usermod -aG docker $USER
```


``` bash
kolla-ansible -i all-in-one deploy
```

Génération du fichier rc pour admin
``` bash
kolla-ansible -i all-in-one post-deploy
cat  /etc/kolla/admin-openrc.sh
```

Liste des conteneurs
``` bash
sudo docker ps
# CONTAINER ID   IMAGE                                                             COMMAND                  CREATED          STATUS                    PORTS     NAMES
# 27d4c1a8cf50   quay.io/openstack.kolla/horizon:zed-ubuntu-jammy                  "dumb-init --single-…"   15 minutes ago   Up 15 minutes (healthy)             horizon
# 11d536640d63   quay.io/openstack.kolla/heat-engine:zed-ubuntu-jammy              "dumb-init --single-…"   16 minutes ago   Up 16 minutes (healthy)             heat_engine
# c5f99006d693   quay.io/openstack.kolla/heat-api-cfn:zed-ubuntu-jammy             "dumb-init --single-…"   16 minutes ago   Up 16 minutes (healthy)             heat_api_cfn
# 0034a79a3c2e   quay.io/openstack.kolla/heat-api:zed-ubuntu-jammy                 "dumb-init --single-…"   16 minutes ago   Up 16 minutes (healthy)             heat_api
# 3f090d0e1056   quay.io/openstack.kolla/neutron-metadata-agent:zed-ubuntu-jammy   "dumb-init --single-…"   17 minutes ago   Up 17 minutes (healthy)             neutron_ovn_metadata_agent
# 0e72f916c3b2   quay.io/openstack.kolla/neutron-server:zed-ubuntu-jammy           "dumb-init --single-…"   17 minutes ago   Up 17 minutes (healthy)             neutron_server
# 7e69884a521c   quay.io/openstack.kolla/ovn-northd:zed-ubuntu-jammy               "dumb-init --single-…"   18 minutes ago   Up 18 minutes                       ovn_northd
# 5b7636985086   quay.io/openstack.kolla/ovn-sb-db-server:zed-ubuntu-jammy         "dumb-init --single-…"   19 minutes ago   Up 19 minutes                       ovn_sb_db
# 554675a6bd9f   quay.io/openstack.kolla/ovn-nb-db-server:zed-ubuntu-jammy         "dumb-init --single-…"   19 minutes ago   Up 19 minutes                       ovn_nb_db
# cddfa8d6ea64   quay.io/openstack.kolla/ovn-controller:zed-ubuntu-jammy           "dumb-init --single-…"   19 minutes ago   Up 19 minutes                       ovn_controller
# 0bf670e8dcd2   quay.io/openstack.kolla/openvswitch-vswitchd:zed-ubuntu-jammy     "dumb-init --single-…"   19 minutes ago   Up 19 minutes (healthy)             openvswitch_vswitchd
# 93001c8a65de   quay.io/openstack.kolla/openvswitch-db-server:zed-ubuntu-jammy    "dumb-init --single-…"   20 minutes ago   Up 20 minutes (healthy)             openvswitch_db
# 9ccff4945d64   quay.io/openstack.kolla/nova-compute:zed-ubuntu-jammy             "dumb-init --single-…"   21 minutes ago   Up 20 minutes (healthy)             nova_compute
# 703128c888a3   quay.io/openstack.kolla/nova-libvirt:zed-ubuntu-jammy             "dumb-init --single-…"   22 minutes ago   Up 21 minutes (healthy)             nova_libvirt
# 491a52d9962e   quay.io/openstack.kolla/nova-ssh:zed-ubuntu-jammy                 "dumb-init --single-…"   23 minutes ago   Up 23 minutes (healthy)             nova_ssh
# 6da895510fb3   quay.io/openstack.kolla/nova-novncproxy:zed-ubuntu-jammy          "dumb-init --single-…"   23 minutes ago   Up 23 minutes (healthy)             nova_novncproxy
# 1da60406fb8b   quay.io/openstack.kolla/nova-conductor:zed-ubuntu-jammy           "dumb-init --single-…"   23 minutes ago   Up 23 minutes (healthy)             nova_conductor
# 7885142a4f21   quay.io/openstack.kolla/nova-api:zed-ubuntu-jammy                 "dumb-init --single-…"   24 minutes ago   Up 24 minutes (healthy)             nova_api
# fae59f7ba077   quay.io/openstack.kolla/nova-scheduler:zed-ubuntu-jammy           "dumb-init --single-…"   24 minutes ago   Up 24 minutes (healthy)             nova_scheduler
# 6ec0f97e8934   quay.io/openstack.kolla/placement-api:zed-ubuntu-jammy            "dumb-init --single-…"   25 minutes ago   Up 25 minutes (healthy)             placement_api
# 974ed9a3b70f   quay.io/openstack.kolla/glance-api:zed-ubuntu-jammy               "dumb-init --single-…"   26 minutes ago   Up 26 minutes (healthy)             glance_api
# 747a5c475ff4   quay.io/openstack.kolla/keystone:zed-ubuntu-jammy                 "dumb-init --single-…"   27 minutes ago   Up 27 minutes (healthy)             keystone
# 2b5946449b82   quay.io/openstack.kolla/keystone-fernet:zed-ubuntu-jammy          "dumb-init --single-…"   27 minutes ago   Up 27 minutes (healthy)             keystone_fernet
# 5696494c0825   quay.io/openstack.kolla/keystone-ssh:zed-ubuntu-jammy             "dumb-init --single-…"   27 minutes ago   Up 27 minutes (healthy)             keystone_ssh
# 4c311961f14c   quay.io/openstack.kolla/rabbitmq:zed-ubuntu-jammy                 "dumb-init --single-…"   29 minutes ago   Up 29 minutes (healthy)             rabbitmq
# 7b971c00d8df   quay.io/openstack.kolla/memcached:zed-ubuntu-jammy                "dumb-init --single-…"   29 minutes ago   Up 29 minutes (healthy)             memcached
# b0cb25b281ec   quay.io/openstack.kolla/mariadb-server:zed-ubuntu-jammy           "dumb-init -- kolla_…"   29 minutes ago   Up 29 minutes (healthy)             mariadb
# 1b8e1f023f4b   quay.io/openstack.kolla/cron:zed-ubuntu-jammy                     "dumb-init --single-…"   30 minutes ago   Up 30 minutes                       cron
# 583cab48a129   quay.io/openstack.kolla/kolla-toolbox:zed-ubuntu-jammy            "dumb-init --single-…"   31 minutes ago   Up 31 minutes                       kolla_toolbox

```

---
## Installation des librairies client

``` bash
pip install python-openstackclient -c https://releases.openstack.org/constraints/upper/zed
pip install python-neutronclient -c https://releases.openstack.org/constraints/upper/zed
pip install python-glanceclient -c https://releases.openstack.org/constraints/upper/zed
pip install python-heatclient -c https://releases.openstack.org/constraints/upper/zed
```

Mise en place de l'autocompletion pour le client
``` bash
#openstack complete > /etc/bash_completion.d/osc
cp -p ~/.bashrc ~/.bashrc.ORI
openstack complete >> ~/.bashrc
source ~/.bashrc
```

``` bash
source /etc/kolla/admin-openrc.sh
openstack service list
# +----------------------------------+-----------+----------------+
# | ID                               | Name      | Type           |
# +----------------------------------+-----------+----------------+
# | 1fe1ef7bf36b4e3f98235768b479cc24 | neutron   | network        |
# | 405c76c42a3e4dff92d4b61b912eee7c | glance    | image          |
# | 845caa59897d41c4982ffff4e7e641e8 | nova      | compute        |
# | 95268de53aec453bb07eb430c9598a2c | heat      | orchestration  |
# | 9863d484cca24fb482d1332b936c3180 | keystone  | identity       |
# | dcab91ba09bc4a0d9b951b68ef4de2b7 | heat-cfn  | cloudformation |
# | e94620b76d0a4146897003af90ff8e9f | placement | placement      |
# +----------------------------------+-----------+----------------+
```

---
## connexion à horizon
http://90.84.47.132/

Pour le mot de passe admin :
``` bash
utilisateur = admin
mot de passe : cat /etc/kolla/admin-openrc.sh | grep OS_PASSWORD
export OS_PASSWORD='pAb7q3scPxpFZmebrzMtshZaV4CvWRmOL3wQ5pgc'
```









---
##
``` bash
kolla-ansible -i all-in-one reconfigure
```


---
## suppression

kolla-ansible -i all-in-one destroy --yes-i-really-really-mean-it

Suppression des containers et volumes
``` bash
# Suppression
docker container prune 
docker system prune --volumes -f
# Vérification : 
docker ps -a
docker volume list
docker image list
```

rm -rf /var/log/kolla

---
## Ressources

- [https://kifarunix.com/deploy-all-in-one-openstack-with-kolla-ansible-on-ubuntu-22-04/?expand_article=1](https://kifarunix.com/deploy-all-in-one-openstack-with-kolla-ansible-on-ubuntu-22-04/?expand_article=1)
- [https://www.golinuxcloud.com/deploy-openstack-using-kolla-ansible/#Step-5Configure_Ansible](https://www.golinuxcloud.com/deploy-openstack-using-kolla-ansible/#Step-5Configure_Ansible)
- [https://gist.github.com/gilangvperdana/e74b3536c0c8786c68cb3ed51e4acbd2](https://gist.github.com/gilangvperdana/e74b3536c0c8786c68cb3ed51e4acbd2)


- [https://hackmd.io/@yujungcheng/ByXxq-_Vj](https://hackmd.io/@yujungcheng/ByXxq-_Vj)

- [https://people.umr-lops.fr/~tletou/kolla-training/](https://people.umr-lops.fr/~tletou/kolla-training/)
- [https://docs.openstack.org/kolla-ansible/latest/user/quickstart.html](https://docs.openstack.org/kolla-ansible/latest/user/quickstart.html)
- [https://achchusnulchikam.medium.com/deploy-production-ready-openstack-using-kolla-ansible-9cd1d1f210f1](https://achchusnulchikam.medium.com/deploy-production-ready-openstack-using-kolla-ansible-9cd1d1f210f1)

- Avec Magnum
    - [https://kdinesh.in/openstack/](https://kdinesh.in/openstack/)
        - [ht(tps://github.com/Dineshk1205/openstacksinglenode/blob/main/Openstack%20single.sh](https://github.com/Dineshk1205/openstacksinglenode/blob/main/Openstack%20single.sh)