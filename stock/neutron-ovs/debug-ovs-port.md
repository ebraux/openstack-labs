    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system


        Port qr-c80e11df-d3
            tag: 1
            Interface qr-c80e11df-d3
                type: internal
--> ???


        Port qr-bfa4c71d-4e
            tag: 3
            Interface qr-bfa4c71d-4e
                type: internal
        Port qg-2309e411-8a
            tag: 2
            Interface qg-2309e411-8a
                type: internal
qrouter-fb74cbf9-5413-4edc-8b1f-a44bf87661a7 (id: 3)
- qr-bfa4c71d-4e:  inet 172.16.1.1
- qg-2309e411-8a : inet 172.24.4.204/24


        Port qg-2022428a-22
            tag: 2
            Interface qg-2022428a-22
                type: internal
        Port qr-53e74237-30
            tag: 1
            Interface qr-53e74237-30
                type: internal
qrouter-d9595ead-c7a5-4afa-bd21-5dfa65129e1f (id: 1)
- qr-53e74237-30: 10.0.0.1/26
- qg-2022428a-22: 172.24.4.243/24
- qr-c80e11df-d3: IPV6 only




        Port tapf42f92da-7d
            tag: 1
            Interface tapf42f92da-7d
                type: internal
| f42f92da-7d70-462a-a248-50363d17e602 |          | fa:16:3e:11:31:fb | ip_address='10.0.0.2', subnet_id='d79315da-93ab-4aa3-a2be-1993e4d662f5'                             | ACTIVE |
--> DHCP pour subnet  private-subnet
qdhcp-90bf1816-1027-4d79-8ff7-8b4c71bbc57e (id: 0        
- tapf42f92da-7d: 10.0.0.2/26




        Port tap568000e6-b4
            tag: 3
            Interface tap568000e6-b4
                type: internal
| 568000e6-b4ed-498c-9994-afc63857cffe |          | fa:16:3e:e3:9a:cf | ip_address='172.16.1.2', subnet_id='caafbaa3-6f6d-4e25-bcfd-e94bc2e2ddf5'                           | ACTIVE |
--> DHCP pour subnet  demoselfservice
qdhcp-364433c3-bad5-40bb-b3db-fc9ee49d46d2 (id: 2)
- tap568000e6-b4 : inet 172.16.1.2/24


        Port tapd6e65a16-8f
            tag: 3
            Interface tapd6e65a16-8f
| d6e65a16-8f54-4336-8afd-0b82f0923f3e |          | fa:16:3e:db:90:cc | ip_address='172.16.1.123', subnet_id='caafbaa3-6f6d-4e25-bcfd-e94bc2e2ddf5'                         | ACTIVE |
--> instance demoTest


        Port tap8de2b6f2-ce
            tag: 3
            Interface tap8de2b6f2-ce

| 8de2b6f2-ce3f-4717-bc5c-6bd67c29e50e | testDemo | fa:16:3e:11:ff:14 | ip_address='172.16.1.215', subnet_id='caafbaa3-6f6d-4e25-bcfd-e94bc2e2ddf5'                         | ACTIVE |

--> instance :
openstack port create --network 364433c3-bad5-40bb-b3db-fc9ee49d46d2 testDemo
openstack server create    --flavor ${FLAVOR_CIRROS_ID}    --image ${IMAGE_CIRROS_ID}    --nic port-id=8de2b6f2-ce3f-4717-bc5c-6bd67c29e50e --security-group ${PROJECT_NAME}BasicAdmin    --wait    ${TEST_INSTANCE_NAME}_2 



+--------------------------------------+----------+-------------------+-----------------------------------------------------------------------------------------------------+--------+
| ID                                   | Name     | MAC Address       | Fixed IP Addresses                                                                                  | Status |
+--------------------------------------+----------+-------------------+-----------------------------------------------------------------------------------------------------+--------+
router1 :
  - external
| 2022428a-22ca-4930-9c11-cf502dcace8a |          | fa:16:3e:2e:56:86 | ip_address='172.24.4.243', subnet_id='70dfaaca-b2e9-4527-98ea-b502ce8e57ea'                         | ACTIVE |
  - subnet interface
| 53e74237-3020-4c79-8b38-8ff397f15569 |          | fa:16:3e:b2:93:ae | ip_address='10.0.0.1', subnet_id='d79315da-93ab-4aa3-a2be-1993e4d662f5'                             | ACTIVE |

DemoRouter
  - external
| 2309e411-8a4b-4a26-9787-31aee1878335 |          | fa:16:3e:50:c5:0c | ip_address='172.24.4.204', subnet_id='70dfaaca-b2e9-4527-98ea-b502ce8e57ea'                         | ACTIVE |
  - subnet interface
| bfa4c71d-4e8a-4ce9-980f-b92fc66d66fc |          | fa:16:3e:f2:07:53 | ip_address='172.16.1.1', subnet_id='caafbaa3-6f6d-4e25-bcfd-e94bc2e2ddf5'                           | ACTIVE |




| 8de2b6f2-ce3f-4717-bc5c-6bd67c29e50e | testDemo | fa:16:3e:11:ff:14 | ip_address='172.16.1.215', subnet_id='caafbaa3-6f6d-4e25-bcfd-e94bc2e2ddf5'                         | DOWN   |


+--------------------------------------+----------+-------------------+-----------------------------------------------------------------------------------------------------+--------+

