---
## docs

- [https://wiki.openstack.org/wiki/OpsGuide-Network-Troubleshooting](https://wiki.openstack.org/wiki/OpsGuide-Network-Troubleshooting)

---
##  configuration système


``` bash
sudo lsmod | grep  filter
# Module                  Size  Used by
# br_netfilter           32768  0
# bridge                307200  1 br_netfilter
```

``` bash
sudo modprobe bridge
sudo modprobe br_netfilter 
```


dans fichier /etc/sysctl.conf

 
``` bash
# Disable Spoof protection (reverse-path filter) 
# Turn OFF Source Address Verification in all interfaces to
net.ipv4.conf.default.rp_filter=0
net.ipv4.conf.all.rp_filter=0

# enable packet forwarding for IPv4
net.ipv4.ip_forward=1

# nable packet forwarding for IPv6
#net.ipv6.conf.all.forwarding=1

#  Accept ICMP redirects
net.ipv4.conf.all.accept_redirects = 1
net.ipv6.conf.all.accept_redirects = 1
# _or_
# Accept ICMP redirects only for gateways listed in our default
# gateway list (enabled by default)
# net.ipv4.conf.all.secure_redirects = 1
#
# Send ICMP redirects
net.ipv4.conf.all.send_redirects = 1 
#
# Accept IP source route packets 
net.ipv4.conf.all.accept_source_route = 1
net.ipv6.conf.all.accept_source_route = 1
#
#
# net.bridge.bridge-nf-call-iptables = 1
# net.bridge.bridge-nf-call-ip6tables=1
```

puis appliquer
``` bash
sysctl --system
```
afficher
``` bash
cat /proc/sys/net/ipv4/ip_forward
cat /proc/sys/net/ipv4/conf/default/rp_filter
```


``` bash
lib/host:function configure_sysctl_mem_parmaters {
lib/host:        sudo sysctl -w vm.dirty_ratio=60
lib/host:        sudo sysctl -w vm.dirty_background_ratio=10
lib/host:        sudo sysctl -w vm.vfs_cache_pressure=50
lib/host:        sudo sysctl -w vm.swappiness=100
lib/host:    configure_sysctl_mem_parmaters
lib/host:function configure_sysctl_net_parmaters {
lib/host:        sudo sysctl -w net.ipv4.tcp_keepalive_time=60
lib/host:        sudo sysctl -w net.ipv4.tcp_keepalive_intvl=10
lib/host:        sudo sysctl -w net.ipv4.tcp_keepalive_probes=6
lib/host:        sudo sysctl -w net.ipv4.tcp_fastopen=3
lib/host:        sudo sysctl -w net.core.default_qdisc=pfifo_fast
lib/host:    configure_sysctl_net_parmaters
```

---
## création de l'interface

- https://docs.openstack.org/neutron/pike/admin/deploy-ovs-selfservice.html

Create the OVS provider bridge br-provider:

``` bash
sudo ovs-vsctl add-br br-ex
sudo ip addr add 172.24.4.1/24 dev br-ex
sudo ip link set dev br-ex promisc on
sudo ip link set dev br-ex up
```

``` bash
# root@ost-ctrl:~# ip link set dev br-ex up
# root@ost-ctrl:~# ip link set dev br-tun up
# root@ost-ctrl:~# ip link set dev br-int up
# root@ost-ctrl:~# ip link set dev br-ex state UP
# root@ost-ctrl:~# ip link set dev br-tun state UP
# root@ost-ctrl:~# ip link set dev br-int state UP
# root@ost-ctrl:~#
# root@ost-ctrl:~# ovs-vsctl add-port br-ex eno2
# root@ost-ctrl:~#
# root@ost-ctrl:~# ip address add 10.10.10.103/24 dev br-ex
# root@ost-ctrl:~#
# root@ost-ctrl:~# ip link set br-ex up
# root@ost-ctrl:~#
# root@ost-ctrl:~# ip address flush dev eno2
# root@ost-ctrl:~#
# root@ost-ctrl:~# route
# root@ost-ctrl:~#
# root@ost-ctrl:~# ip route replace default via 10.10.10.1
# root@ost-ctrl:~#
# root@ost-ctrl:~# route
```


pour faire une "dummy" interface : https://digv.io/blog/creating-a-dummy-network-interface-on-ubuntu-22
---
## Iptables

``` bash
sudo iptables -t nat --list
# Chain PREROUTING (policy ACCEPT)
# target     prot opt source               destination         

# Chain INPUT (policy ACCEPT)
# target     prot opt source               destination         

# Chain OUTPUT (policy ACCEPT)
# target     prot opt source               destination         

# Chain POSTROUTING (policy ACCEPT)
# target     prot opt source               destination         
# MASQUERADE  all  --  172.24.4.0/24        anywhere            
```
Il faut une règle de POSTROUTING. Pour la créer :
``` bash
sudo iptables -t nat -A POSTROUTING -o all -s 172.24.4.0/24 -j MASQUERADE
```

https://gist.github.com/dpino/6c0dca1742093346461e11aa8f608a99

---
## routes

Dans devstack
``` bash
function _configure_public_network_connectivity {
    # If we've given a PUBLIC_INTERFACE to take over, then we assume
    # that we can own the whole thing, and privot it into the OVS
    # bridge. If we are not, we're probably on a single interface
    # machine, and we just setup NAT so that fixed guests can get out.
    if [[ -n "$PUBLIC_INTERFACE" ]]; then
        _move_neutron_addresses_route "$PUBLIC_INTERFACE" "$OVS_PHYSICAL_BRIDGE" True False "inet"

        if [[ $(ip -f inet6 a s dev "$PUBLIC_INTERFACE" | grep -c 'global') != 0 ]]; then
            _move_neutron_addresses_route "$PUBLIC_INTERFACE" "$OVS_PHYSICAL_BRIDGE" False False "inet6"
        fi
    else
```

---
## Promiscuous mode
   
Vérifier une interface
``` bash
sudo ip -d l sh | grep -B 1 promiscuity
```

http://shaarli.guiguishow.info/?uTSqHg

Voir si l'une des interfaces réseau du système est en mode promiscuous :
``` bash
 ip -d l sh | grep -B 1 promiscuity.
 ```
Si le nombre affiché est supérieur à 1, alors plusieurs applications (+ le noyau voir ligne suivante) ont activé le mode promiscuous.

Activer le mode promiscuous sans passer par libpcap :
`sudo ip l set promisc on dev <INTERFACE>.`
Pour le désactiver :
`sudo ip l set promisc off dev <INTERFACE>`

```
#promiscuous mode required for routing
      /sbin/ifconfig {{ vpn_nic }} up
      /sbin/ifconfig {{ vpn_nic }} promisc
``` 

libpcap ??

POur le conserver au redémarrage :
- https://stackoverflow.com/questions/76670981/how-to-set-the-promiscusing-netplan-in-ubuntu-20-04-and-22-04-and-make-the-flag
- https://askubuntu.com/questions/1355974/how-to-enable-promiscuous-mode-permanently-on-a-nic-managed-by-networkmanager
- https://superuser.com/questions/1804774/persistent-promiscuous-mode-in-debian-12  

---
## Nova et secgroup en mode sans promiscuous

``` bash
#firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver
firewall_driver = neutron.agent.firewall.NoopFirewallDriver
``` 

[https://gist.github.com/djoreilly/db9c2d32a473c6643551](https://gist.github.com/djoreilly/db9c2d32a473c6643551).

désactiver le driver, 
``` bash
People usually want to do this because the anti-spoofing rules are dropping packets transmitted by Nova instances that do not have the source MAC or IP address that was allocated to the instance. Note: allowed-addresses-pairs or port-security extension can fix that. Also there is a performance drop using the hybrid plugging strategy (veth+linuxbridge+iptables).

But Nova needs a security groups API or it will refuse to start instances. It needs to be configured to use its own or Neutron's. Here we configure it to use the Nova security groups API, but disable nova-compute (and the Neutron L2 agent - just to be sure) from applying any iptables rules.
```

---
## Utilisation de TCP dump


br-ex-tcpdump "$TCPDUMP -i $PUBLIC_BRIDGE arp or rarp or icmp or icmp6 -enlX" "$STACK_GROUP" root


tcpdump -n -i any arp -l


---
## Interraction avec ovs

commandes :

br-int-flows "/bin/sh -c \"set +e; while true; do echo ovs-ofctl dump-flows br-int; ovs-ofctl dump-flows br-int ; sleep 30; done; \"" "$STACK_GROUP" root




Voir les flow


ex quand ça marche :
``` bash
sudo ovs-ofctl dump-flows br-ex
 cookie=0x4890ff32abcace24, duration=101564.010s, table=0, n_packets=370, n_bytes=32699, priority=4,in_port="phy-br-ex",dl_vlan=2 actions=strip_vlan,NORMAL
 cookie=0x4890ff32abcace24, duration=101565.106s, table=0, n_packets=2106, n_bytes=283608, priority=2,in_port="phy-br-ex" actions=drop
 cookie=0x4890ff32abcace24, duration=101565.108s, table=0, n_packets=341, n_bytes=31487, priority=0 actions=NORMAL
```
``` bash
sudo ovs-ofctl dump-flows br-int
 cookie=0x318434e926094d04, duration=101705.940s, table=0, n_packets=0, n_bytes=0, priority=65535,dl_vlan=4095 actions=drop
 cookie=0x318434e926094d04, duration=101704.836s, table=0, n_packets=335, n_bytes=30971, priority=3,in_port="int-br-ex",vlan_tci=0x0000/0x1fff actions=mod_vlan_vid:2,resubmit(,58)
 cookie=0x318434e926094d04, duration=101705.932s, table=0, n_packets=6, n_bytes=516, priority=2,in_port="int-br-ex" actions=drop
 cookie=0x318434e926094d04, duration=101705.942s, table=0, n_packets=2824, n_bytes=352884, priority=0 actions=resubmit(,58)
 cookie=0x318434e926094d04, duration=101705.943s, table=23, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0x318434e926094d04, duration=101705.941s, table=24, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0x318434e926094d04, duration=101705.939s, table=30, n_packets=0, n_bytes=0, priority=0 actions=resubmit(,58)
 cookie=0x318434e926094d04, duration=101705.939s, table=31, n_packets=0, n_bytes=0, priority=0 actions=resubmit(,58)
 cookie=0x318434e926094d04, duration=101705.942s, table=58, n_packets=3159, n_bytes=383855, priority=0 actions=resubmit(,60)
 cookie=0x318434e926094d04, duration=24238.317s, table=60, n_packets=122, n_bytes=10716, priority=100,in_port="tap8de2b6f2-ce" actions=load:0xd->NXM_NX_REG5[],load:0x3->NXM_NX_REG6[],resubmit(,71)
 cookie=0x318434e926094d04, duration=24238.232s, table=60, n_packets=127, n_bytes=11226, priority=100,in_port="tapd6e65a16-8f" actions=load:0xc->NXM_NX_REG5[],load:0x3->NXM_NX_REG6[],resubmit(,71)
 ...
``` 

ex quand ça marche pas :
``` bash
sudo ovs-ofctl dump-flows br-ex
 cookie=0x0, duration=4601.607s, table=0, n_packets=432, n_bytes=18660, priority=0 actions=NORMAL
```
``` bash
sudo ovs-ofctl dump-flows br-int
 cookie=0xceb8141bbfe9190, duration=4712.607s, table=0, n_packets=0, n_bytes=0, priority=65535,dl_vlan=4095 actions=drop
 cookie=0xceb8141bbfe9190, duration=4712.610s, table=0, n_packets=198, n_bytes=11464, priority=0 actions=resubmit(,58)
 cookie=0xceb8141bbfe9190, duration=4712.611s, table=23, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xceb8141bbfe9190, duration=4712.608s, table=24, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xceb8141bbfe9190, duration=4712.605s, table=30, n_packets=0, n_bytes=0, priority=0 actions=resubmit(,58)
 cookie=0xceb8141bbfe9190, duration=4712.605s, table=31, n_packets=0, n_bytes=0, priority=0 actions=resubmit(,58)
 cookie=0xceb8141bbfe9190, duration=4712.609s, table=58, n_packets=198, n_bytes=11464, priority=0 actions=resubmit(,60)
 cookie=0xceb8141bbfe9190, duration=4712.608s, table=60, n_packets=198, n_bytes=11464, priority=3 actions=NORMAL
 cookie=0xceb8141bbfe9190, duration=4712.606s, table=62, n_packets=0, n_bytes=0, priority=3 actions=NORMAL
```

explication des flow :
https://aptira.com/openstack-rules-how-openvswitch-works-inside-openstack/

Diag de la base de données
``` bash
sudo ovs-appctl -t ovsdb-server ovsdb-server/list-remotes
```

https://answers.launchpad.net/neutron/+question/216123

Is this possibly caused by the fact that I did not specify "--provider:network_type gre --provider:segmentation_id <tunnel-id>" when creating the internal network?

Débug des flows OVS : https://wiki.openstack.org/wiki/OpsGuide-Network-Troubleshooting
Super doc sur OVS : http://www.yet.org/2014/09/openvswitch-troubleshooting/


ovs-vsctl list-br
br-int
br-provider


ovs-vsctl list-ports br-int
int-br-provider
qg-444ef49c-8d
qg-7008deb7-09
tap4ff725a0-08


ovs-vsctl list-ifaces  br-int
int-br-provider
qg-444ef49c-8d   --> gateway de test-ping
qg-7008deb7-09  --> gateway de rt-test
tap4ff725a0-08




openstack router list
+--------------------------------------+-----------+--------+-------+----------------------------------+-------------+-------+
| ID                                   | Name      | Status | State | Project                          | Distributed | HA    |
+--------------------------------------+-----------+--------+-------+----------------------------------+-------------+-------+
| 1150dec3-fe78-4fc7-9a45-e0afa29702bb | test-ping | ACTIVE | UP    | 8109f2d71df74f2cb0ed8bac9db1ce9f | False       | False |
| 2ef7b82c-73e1-42dd-9987-fa269545c605 | rt-test   | ACTIVE | UP    | 8109f2d71df74f2cb0ed8bac9db1ce9f | False       | False |
+--------------------------------------+-----------+--------+-------+----------------------------------+-------------+-------+


ip netns
qrouter-1150dec3-fe78-4fc7-9a45-e0afa29702bb (id: 0) -> test-ping
qrouter-2ef7b82c-73e1-42dd-9987-fa269545c605 (id: 3) -> rt-test
qdhcp-ba7e0f42-d023-4dba-afb7-f5a7f4e767fa (id: 1)

# test-ping
ip netns exec qrouter-1150dec3-fe78-4fc7-9a45-e0afa29702bb ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
16: qg-444ef49c-8d: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether fa:16:3e:82:47:88 brd ff:ff:ff:ff:ff:ff
    inet 203.0.113.228/24 brd 203.0.113.255 scope global qg-444ef49c-8d
       valid_lft forever preferred_lft forever
    inet6 fe80::f816:3eff:fe82:4788/64 scope link 
       valid_lft forever preferred_lft forever

# rt-test
ip netns exec qrouter-2ef7b82c-73e1-42dd-9987-fa269545c605 ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
13: qg-7008deb7-09: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether fa:16:3e:8e:f9:31 brd ff:ff:ff:ff:ff:ff
    inet 203.0.113.232/24 brd 203.0.113.255 scope global qg-7008deb7-09
       valid_lft forever preferred_lft forever
    inet6 fe80::f816:3eff:fe8e:f931/64 scope link 
       valid_lft forever preferred_lft forever

 Test de ping entre les routers :
 ip netns exec qrouter-2ef7b82c-73e1-42dd-9987-fa269545c605 ping -c 4 203.0.113.228

 ip netns exec qrouter-1150dec3-fe78-4fc7-9a45-e0afa29702bb  ping -c 4 203.0.113.232

 Ok, ça ping ...


 ---
 ## Création d'un port

openstack port create  --network demoselfservice test

openstack port list -f json
[
  {
    "ID": "41ffe376-f2a1-406b-abe3-ee1f95400013",
    "Name": "test",
    "MAC Address": "fa:16:3e:51:5c:69",
    "Fixed IP Addresses": [
      {
        "subnet_id": "3b3c887d-bfb7-4e08-bb57-340f76fcf495",
        "ip_address": "172.16.1.184"
      }
    ],
    "Status": "DOWN"
  },

openstack port show test -f json

