#!/bin/bash

# Disable Firewald 
sudo systemctl stop firewalld
sudo systemctl disable firewalld

# Disable SE Linux
sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config
sudo sed -i 's/enforcing/disabled/g' /etc/sysconfig/selinux


# Update packages
sudo dnf update -y

# Puppet version management : disable epel repo
sudo yum autoremove -y epel-release
sudo yum clean all

# Install Opensatck Repo
sudo dnf config-manager --enable crb
sudo dnf install -y centos-release-openstack-yoga
sudo dnf update -y
sudo dnf upgrade -y

# Install Tools and utilities
sudo dnf install -y libcurl-devel  openssl-devel
sudo dnf install -y git vim tree nano jq


# Enable password Authentification
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd.service


# Create stack user
groupadd stack
useradd -g stack       -s /bin/bash -d /home/stack  -m  stack
cd /etc/sudoers.d
echo "stack ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_stack_sh
chmod 440 /etc/sudoers.d/50_stack_sh
echo stack:stack | chpasswd


#  Manage DNS resolution with /etc/hosts config
cp -p /etc/hosts /etc/hosts.DIST
sed -i 1i$'172.16.50.31    compute' /etc/hosts
sed -i 1i$'172.16.50.21    controller' /etc/hosts


