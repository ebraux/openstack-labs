#!/bin/bash

#
#
#yum install -y --downloadonly chrony
#yum install -y --downloadonly openstack-nova-compute @virt libvirt-devel virt-top libguestfs-tools
#yum install -y --downloadonly openstack-neutron-linuxbridge ebtables ipset

#
cd /home/stack
git clone https://gitlab.com/ebraux/openstack-labs-installation.git
cd openstack-labs-installation/
git checkout yoga-centos9s
cp -R docs/03_compute/conf /home/stack/openstack_config
chown -R stack.stack /home/stack/openstack-labs-installation/
chown -R stack.stack /home/stack/openstack_config
