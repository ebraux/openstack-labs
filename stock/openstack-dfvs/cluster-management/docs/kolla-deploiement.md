# Déploiement de OpenStack avec Kolla


## Configurer l'accès réseau

Nécessaire d'avoir accès au réseau "internal" depuis la machine qui assure le déploiement. Si ce n'ets apas le cas, créer un tunnel avec `sshoot`


créer un profil
``` bash
sshoot create -r root@os-ctrl-01.priv.enst-bretagne.fr openstack-dfvs 192.168.73.0/24
```

Afficher les profils
``` bash
$ sshoot list
   Name            Remote host                            Subnets          
---------------------------------------------------------------------------
   openstack-dfvs  root@os-ctrl-01.priv.enst-bretagne.fr  192.168.73.0/24  
```

Lancer le  
``` bash
sshoot start openstack-dfvs
```

test
``` bash
ssh root@192.168.73.21
```

---
## configurer kolla-ansible

Vérifier l'alias d ela commande kolla-ansible
```
alias kolla-ansible
```

Et si besoin l'initialiser
```
alias kolla-ansible='kolla-ansible --vault-password-file vault_secret --configdir $PWD/configdir -i inventory'
```

---
## Effectuer le déploiement

```
kolla-ansible bootstrap-servers
```
time
```
kolla-ansible prechecks
# (error sur localhost)
```

```
kolla-ansible deploy
```

```
kolla-ansible post-deploy
# création du fichier configdir/admin-openrc.sh
```


---

# creation du dossier de configuration du service docker
sudo mkdir -p /etc/systemd/system/docker.service.d

# creation du fichier de configuration du proxy
sudo tee /etc/systemd/system/docker.service.d/proxy.conf << 'EOF'
[Service]
Environment='HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080' 'HTTP_PROXY=http://proxy.enst-bretagne.fr:8080' 'NO_PROXY=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr'
EOF

cat /etc/systemd/system/docker.service.d/proxy.conf

# relancer Docker
sudo systemctl daemon-reload
sudo systemctl restart docker


