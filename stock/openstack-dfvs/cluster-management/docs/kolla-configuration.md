
# Gen Kolla passwords file

génération des mots de passe pour l'ensemble des services.

Création des mots de passe
```
kolla-genpwd -p configdir/passwords.yml 
```

Chiffrement du fichier avec le fichier de mot de passe (`$ANSIBLE_VAULT_PASSWORD_FILE`)
```
ansible-vault encrypt configdir/passwords.yml
```

---
## Certificats
``` bash
tar -xzvf openstack-dfvs.imt-atlantique.fr.tgz
ll openstack-dfvs.imt-atlantique.fr/
mkdir -p  configdir/certificates/
cat openstack-dfvs.imt-atlantique.fr/cert.pem > configdir/certificates/openstack-dfvs.imt-atlantique.fr.pem
cat openstack-dfvs.imt-atlantique.fr/privkey.pem >> configdir/certificates/openstack-dfvs.imt-atlantique.fr.pem
cat openstack-dfvs.imt-atlantique.fr/fullchain.pem >> configdir/certificates/openstack-dfvs.imt-atlantique.fr.pem

```

Chiffrement du fichier avec le fichier de mot de passe (`$ANSIBLE_VAULT_PASSWORD_FILE`)
``` 
ansible-vault encrypt configdir/certificates/openstack-dfvs.imt-atlantique.fr.pem
```


---
## Configuration du déploiement OpenStack-Kolla

Cela se passe dans `configdir/globals.yaml`

`configdir` 
- correspond àaiu paramètre --configdir
- transformé en variable {{ node_config }} dans ansible



Configuration du type d'installation

 * distribution des container : ubuntu
 * kolla_install_type : faisons confiance aux mainteneur des images 
 * openstack_release : yoga

```
#kolla_base_distro: "centos"
kolla_base_distro: "ubuntu"

#openstack_release: "yoga"
openstack_release: "yoga"

#kolla_install_type: "source"
kolla_install_type: "binary"
```


###  Config TLS
```bash
#############
# TLS options
#############
# To provide encryption and authentication on the kolla_external_vip_interface,
# TLS can be enabled.  When TLS is enabled, certificates must be provided to
# allow clients to perform authentication.
#kolla_enable_tls_internal: "no"
#kolla_enable_tls_external: "{{ kolla_enable_tls_internal if kolla_same_external_internal_vip | bool else 'no' }}"
#kolla_certificates_dir: "{{ node_config }}/certificates"
#kolla_external_fqdn_cert: "{{ kolla_certificates_dir }}/haproxy.pem"
#kolla_internal_fqdn_cert: "{{ kolla_certificates_dir }}/haproxy-internal.pem"
#kolla_admin_openrc_cacert: ""
#kolla_copy_ca_into_containers: "no"
#haproxy_backend_cacert: "{{ 'ca-certificates.crt' if kolla_base_distro in ['debian', 'ubuntu'] else 'ca-bundle.trust.crt' }}"
#haproxy_backend_cacert_dir: "/etc/ssl/certs"
kolla_enable_tls_external: "yes"
kolla_certificates_dir: "{{ node_config }}/certificates"
kolla_external_fqdn_cert: "{{ kolla_certificates_dir }}/openstack-dfvs.imt-atlantique.fr.pem"
```


### Customisation des services

```bash
#node_custom_config: "{{ node_config }}/config"
node_custom_config: "{{ node_config }}/node_custom_config"
```

- Config de horizon :
- Config de Nova : `configdir/node_custom_config/nova/nova.conf`


## Gestion des interfces réseau


#network_interface: "eth0"
--> gérée dans l'inventory
#tunnel_interface: "{{ network_interface }}"
--> gérée dans l'inventory


#kolla_internal_fqdn: "{{ kolla_internal_vip_address }}"
kolla_internal_fqdn: "{{ kolla_internal_vip_address }}"


#kolla_external_vip_interface: "{{ network_interface }}"
kolla_external_vip_interface: "{{ network_interface }}"

#kolla_internal_vip_address: "10.10.10.254"
kolla_internal_vip_address: "192.168.72.8"

#kolla_external_vip_address: "{{ kolla_internal_vip_address }}"
kolla_external_vip_address: "10.29.241.8"

#kolla_external_fqdn: "{{ kolla_external_vip_address }}"
kolla_external_fqdn: "openstack-dfvs.imt-atlantique.fr"


#api_interface: "{{ network_interface }}"
api_interface: "vlan52"

#tunnel_interface: "{{ network_interface }}"
tunnel_interface: "vlan53"



### Configuration de neutron

Configuration des providers networks neutron
```
#neutron_plugin_agent: "openvswitch"
neutron_plugin_agent: "ovn"

#enable_neutron_provider_networks: "no"
enable_neutron_provider_networks: "yes"

#neutron_external_interface: "eth1"
neutron_external_interface: "vlan51"


#neutron_bridge_name: "br-vlan33,br-vlan300"
--> pas dans la doc. mais utilise si plusieurs bridges

```

### déclaration des service

```
#enable_haproxy: "yes"
enable_haproxy: "yes"

#enable_central_logging: "no"
enable_central_logging: "yes"

#enable_grafana: "{{ enable_monasca | bool }}"
enable_grafana: "yes"

#enable_prometheus: "no"
enable_prometheus: "yes"
```