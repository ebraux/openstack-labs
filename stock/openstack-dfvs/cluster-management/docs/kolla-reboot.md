# Redémarrage de la plateforme

Le cluster de base de donnée Galera n'est pas prévu pour que tous les noeuds soient arrêtés. Il lui faut toujours un noeuds maitre actif.

Dans le cas d'un arrêt complet des controlplane, l'infra ne redémarre donc pas correctement.

On retrouve :

- Dans les logs des container des problème d'accès là la base de donnée
- Dans les logs de galera/mariadb, des message concernant des problème de synchronisation `WSREP`.
  - `[Warning] WSREP: no nodes coming from prim view, prim not possible`
  - ...

POur relancer un cluster Galera, il faut inspacter chaque neoud, définir celui qui a effectué la dernière opération, et remonter le cluster à partir de ce noeud.

Kolla-ansible propose un playbook qui réalise toutes les opérations nécéssaires
``` bash
kolla-ansible mariadb_recovery
```

Mais sinon les opération sont décrites dans ce thread, et il ets possible de les raliser manuellement : [https://bugs.launchpad.net/kolla-ansible/+bug/1712087](https://bugs.launchpad.net/kolla-ansible/+bug/1712087)

Exemple pour afficher le fichier `grastate.dat` des instances mariadb : 
``` bash
ansible control -a "cat /var/lib/docker/volumes/mariadb/_data/grastate.dat"
```