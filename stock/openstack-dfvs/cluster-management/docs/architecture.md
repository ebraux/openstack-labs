 # Architecture

Informations concernant le déploiement
- Infrastructure
  - Mysql (cluster Galera)
  - Rabbit MQ (HA)
  - Haproxy
- Services déployés : 
  - keystone
  - glance
  - placement
  - nova
  - neutron
  - heat
  - cinder
- Driver réseau pour Neutron : OVN 


Control-plane :
- 3 serveurs pour le controle-plane
- os-ctrl-[01-03].priv.enst-bretagne.fr

Compute:
- os-ctrl-[01-06].priv.enst-bretagne.fr
  - R620
  - 2 x CPU Intel Xeon E5-2650 v2, 16 coeurs = 32 coeurs
  - 128Go RAM
- Compute:
- os-ctrl-[07-09].priv.enst-bretagne.fr
  - R630
  - 2 x CPU Intel Xeon E5-2650 v4, 24 coeurs= 48 coeurs
  - 128Go RAM
