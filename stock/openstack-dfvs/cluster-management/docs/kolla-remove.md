#  Désinstallation de Kolla

Arrêt de Kolla
``` bash
kolla-ansible stop --yes-i-really-really-mean-it
```

Suppression des containers et volumes
``` bash
# Suppression
ansible all  -a "docker container prune  -f" 
ansible all  -a "docker container ps -a" 
ansible all  -a "docker system prune --volumes -f"
# Vérification : 
ansible all  -a "docker ps -a" 
ansible all  -a "docker volume list"
ansible all  -a "docker image list"
```

Suppression de la configuration Kolla
``` bash
ansible all  -a "rm -rf /etc/kolla" 
ansible all  -a "rm -rf /var/log/kolla" 
```

Désactivation des configurations réseau
``` bash
ansible all  -a "ip link delete vlan52" 
ansible all  -a "ip link delete vlan53" 
ansible all  -a "ip link delete vlan54" 
ansible all  -a "ip link delete vlan55"

ansible all  -a "ip a" 
ansible all  -a "ip r" 

ansible all  -a "rm /etc/netplan/52-openstack.yaml"
```

ansible control  -a "reboot"
ansible compute  -a "reboot"
```
