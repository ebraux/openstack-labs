

---
# Initaliser Ansible Vault

 fichier.


---
# Mettre en place le config-dir qui est dans la distribution
- Récupérer les dépots (juste pour récupérer les infos)
- Récupérer les fichiers "configdir"
- Installer les modules python via pip, à partir des dépôt
- Faire le ménage des dépôts


---
## Initialisation des environnement Kolla et Kolla ansible

Si utilisation de direnv, directement éxecuté. sinon reprendre le code du fichier .envrc
``` bash
export OS_VERSION=yoga

echo '# =========================='
echo '#  Kolla Ansble Init'
echo '# =========================='
if [[ ! $(pip list|grep 'kolla-ansible') ]]; then
   echo "===> Installing Kolla Ansible:"
   git clone https://github.com/openstack/kolla-ansible
   cd kolla-ansible
   git checkout stable/${OS_VERSION}
   pip install .
   cd ..
   rm -rf kolla-ansible
fi


echo '# =========================='
echo '#  Kolla Init'
echo '# =========================='
if [[ ! $(pip list|grep 'kolla ') ]]; then
   echo "===> Installing Kolla:"
   git clone https://github.com/openstack/kolla
   cd kolla
   git checkout stable/${OS_VERSION}
   pip install .
   cd ../
   rm -rf kolla
fi


echo '# =========================='
echo '#  Config Kolla-Ansible Env'
echo '# =========================='
INVENTORY="inventory"
VERBOSITY=
EXTRA_OPTS="--vault-password-file vault_secret"
CONFIG_DIR="${PWD}/configdir"

export ANSIBLE_VAULT_PASSWORD_FILE=$PWD/vault_secret
```


---
## Récupération des fichiers de démarrage (config)

```
git clone https://github.com/openstack/kolla
cd kolla
git checkout stable/yoga

mkdir configdir 
cp -r kolla-ansible/etc/kolla/* configdir/
rm -Rf kolla 
```

``` bash
git clone https://github.com/openstack/kolla-ansible
cd kolla-ansible
git checkout stable/yoga

cp kolla-ansible/ansible/inventory/multinode ./inventory/hosts
rm -Rf kolla-ansible
```

Suppression de kolla-ansible et kolla
```
```


---
## Personnalisation des service

Emplacement des configurations complémentaires pour les services
```
node_custom_config: "configdir/node_custom_config"
```
(par exemple `configdir/node_custom_config/nova/nova.conf` pour surcharger le fichier de configuration de nova)

Surtout désactiver ça, cette variable viendra de l'inventaire. Il s'agit de l'interface publique de la machine.
```
#network_interface: "eth0"
```

### Nova


### Horizon




