






Dans les varaibles : 

network_interface: ens18
  -> dans la config de Kolla
  kolla_external_vip_interface
  IP pour le API endpoint
kolla_external_vip_address: "194.167.223.154"
kolla_external_fqdn: "noocloud.univ-brest.fr"
kolla_external_vip_interface: "{{ network_interface }}"

trunk_iface: ens19
  -> dans la getsion des réseau via netplan
  pour les Vlan 33, 42, 300 et 99


Pour les VLAN :

Le Vlan 42, c'est pour le résau admin/Internal 
 kolla_internal_vip_address: "10.0.42.154"
 kolla_internal_fqdn: "{{ kolla_internal_vip_address }}"
Qui crée une interface virtuell / Bridge, et affecte l'IP ?

C'ets pas un vlan à par pour le VXlan ?
Réseau admin/internal et aussi pour le tunneling/vxlan
api_interface: "vlan42"


2 vlan 33 et 300 pour des réseaux externes 
  * **physnet1** : iface `vlan33` avec un bridge nommé `br-vlan33` 
  * **physnet2** : iface `vlan300` avec un bridge nommé `br-vlan300`
 neutron_external_interface: "vlan33, vlan300"
  -> qui gérer les bridge correspondants ?
  -> où est-ce qu'on défini les IP 
neutron_bridge_name: "br-vlan33, br-vlan300"


Le Vlan 99 ???


#kolla_external_vip_interface: "{{ network_interface }}"
#api_interface: "{{ network_interface }}"
#tunnel_interface: "{{ network_interface }}"


```
neutron_external_interface: "vlan33,vlan300"
neutron_bridge_name: "br-vlan33,br-vlan300"


neutron_plugin_agent: "openvswitch".
--> OVN ?

        vlan42:
            accept-ra: no
            id: 42
            addresses:
            - {{  internal_ip }}/24
            link: {{ trunk_iface }}
        vlan99:
            accept-ra: no
            id: 99
            link: {{ trunk_iface }}

        vlan33:
            accept-ra: no
            id: 33
            link: {{ trunk_iface }}
        vlan300:
            accept-ra: no
            id: 300
            link: {{ trunk_iface }}

Interfaces 
en01
en02

# ---

Réseau des IP réélles : 10.29.241.0/24
- 10.29.241.2 à 9 -> API + horizon, avec droits suplémentaires pour ouverture extérieur (l'infra utilise la 10.29.241.8)
- 10.29.241.2x : controleurs (os-ctrl)
- 10.29.241.1xx : noeuds de compute (os-compute)


Configuration Vlans
50 - 10.29.241.0/24 - Vlan Natif
51 - 10.29.244.0/22 - External Network ! Flaoting IPS
52 - 192.168.72.0/24 - Internal API and services
53 - 192.168.73.0/24 - Internal Network VxLAN

Non utilisés
54 - 192.168.74.0/24 - Storage Netork
55 - 192.168.75.0/24 - Ceph Storage replication


Reséau des IP flottantes : 10.29.244.0/22


Par convention un noeud ayant plusieurs interfaces réseaux avec des adresses IP, ces dernières auront un suffix identiques.
Prenons l’exemple de os-ctrl-04 :
- IP provisionning 10.29.241.24
- IP stockage192.168.74.24
- IP mgmt192.168.73.24

Configuration à appliquer aux niveau des switch :
``` bash
#  --- Openstack DFVS ---
description Openstack V1 vlan 50-56
switchport trunk native vlan 50
switchport trunk allowed vlan 50-56
switchport mode trunk
storm-control broadcast level 5.00
storm-control multicast level 5.00
spanning-tree portfast edge trunk
```

vlan 50
 name vl_StackAdmin

vlan 51
 name vl_Stack_non_route_1

vlan 52
 name vl_Stack_non_route_2

vlan 53
 name vl_StackPublic

vlan 54
 name vl_Stack_vert

vlan 55
 name vl_Stack_noir

vlan 56
 name vl_Stack_non_route_3

vlan 57
 name vl_Stack_non_route_4



 Le Vlan 50 n'est pas autorisé sur l'Uplink venant du switch n°4

sw-d01-03#
interface GigabitEthernet1/0/24
 description vers COEUR
 switchport trunk allowed vlan 1,148,170-179,420
 switchport mode trunk
!

==> J'ai lancé :

sw-d01-03#conf terminal
Enter configuration commands, one per line.  End with CNTL/Z.
sw-d01-03(config)#interface GigabitEthernet1/0/24
sw-d01-03(config-if)#switchport trunk allowed vlan add 50

+ sw-d01-03#write memory



---
## tests
Interconnexions : 
51, 54 : KO
52, 53, 55 :  OK

53 : externel -IP PUB)  -> OK
55 : tunnel (vxlan)  -> OK
52 : internal  (et pas le 54)