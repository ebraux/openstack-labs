# Validation d ela configuration réseau

**Avant le déploiement**

---
## Configuration réseau pour les tests

- Avec des IPS sur les Vlan 53,54 et 55 sur les compute 02 et 03.
- Test d'accès entre compute et depuis mon poste

Config 
``` yaml
network:
    ethernets:
        eno2:
            dhcp4: off
            dhcp6: off
            mtu: 1500
    vlans:
        vlan54:
            accept-ra: no
            id: 54
            addresses:
            - 192.168.73.103/24
            link: eno2
        vlan55:
            accept-ra: no
            id: 55
            addresses:            
            - 172.16.0.103/24
            link: eno2
        vlan53:
            accept-ra: no
            id: 53
            addresses:            
            - 10.29.244.103/24
            link: eno2
```

Sur compute-03
``` bash
ip r
default via 10.29.241.1 dev eno1 proto dhcp src 10.29.241.103 metric 100 
10.29.241.0/24 dev eno1 proto kernel scope link src 10.29.241.103 
10.29.241.1 dev eno1 proto dhcp scope link src 10.29.241.103 metric 100 
10.29.244.0/24 dev vlan53 proto kernel scope link src 10.29.244.103 
172.16.0.0/24 dev vlan55 proto kernel scope link src 172.16.0.103 
192.168.73.0/24 dev vlan52 proto kernel scope link src 192.168.73.103 
```

---
## Test : Vlan External  (vlan53)
- accessible depuis mon poste
- accessible entre les noeuds
--> fonctionne bien

depuis le 2  ou le 3, ou mon poste
  ping 10.29.244.102 OK
  ping 10.29.244.103 OK
  ping 10.29.244.1 OK

---
## Test : Vlan internal/admin (vlan 54)
- Non accessible accessible depuis mon poste
- accessible entre les noeuds
--> fonctionne bien

depuis le 2  ou le 3
  ping 192.168.73.102  OK
  ping 192.168.73.103  OK
  ping 192.168.73.1    KO : normal car non routé
   
Depuis mon poste   
  ping 192.168.73.102  KO
  ping 192.168.73.103  KO
  ping 192.168.73.1    KO : normal car non routé

---
## Test : Vlan Tunnel VxLAN (vlan 55)
- Non accessible accessible depuis mon poste
- accessible entre les noeuds
--> fonctionne bien

depuis le 2  ou le 3
  ping 172.16.0.102  OK
  ping 172.16.0.103  OK
  ping 172.16.0.1  KO : normal car non routé
   
Depuis mon poste   
  ping 172.16.0.102  KO : normal car non routé
  ping 172.16.0.103  KO : normal car non routé
  ping 172.16.0.1  KO : normal car non routé

