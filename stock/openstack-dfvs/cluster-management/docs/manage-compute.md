# Gestion des serveurs de compute

---
## Ajouter un noeud de compute

Ajouter le noeud du compute au fichier inventory/hosts, dans la catégorie [compute]

Utiliser une variable pour le nom du compute à ajouter
``` bash
export NODE_NAME=os-compute-xx.priv.enst-bretagne.fr
```

Préparer le server
``` bash
kolla-ansible  bootstrap-servers --limit $NODE_NAME
```

Charger les image sur le noeud
``` bash
kolla-ansible  pull  --limit $NODE_NAME
```

Déploiement
``` bash
kolla-ansible deploy--limit $NODE_NAME
```

---
## Supprimer un noeud de compute


Utiliser une variable pour le nom du compute à ajouter
``` bash
export NODE_NAME=os-compute-xx
```

Vérifier si des instances tournent sur le noeud
``` bash
openstack compute service set ${NODE_NAME} nova-compute --disable
```

Désactiver le compute au niveau  d'Openstack
``` bash
openstack compute service set ${NODE_NAME} nova-compute --disable
```

Stopper tous les service sur le noeud
``` bash
kolla-ansible stop --yes-i-really-really-mean-it --limit ${NODE_NAME}.priv.enst-bretagne.fr
```

Faire le ménage des agents au niveua Openstack
``` bash
openstack network agent list --host  ${NODE_NAME} -f value -c ID | while read id; do
  openstack network agent delete $id
done

openstack compute service list --os-compute-api-version 2.53 --host  ${NODE_NAME} -f value -c ID | while read id; do
  openstack compute service delete --os-compute-api-version 2.53 $id
done
```


---
## Source / références

- [https://docs.openstack.org/kolla-ansible/latest/user/adding-and-removing-hosts.html](https://docs.openstack.org/kolla-ansible/latest/user/adding-and-removing-hosts.html)
