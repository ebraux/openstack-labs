# Tests connexion réseau

**En configuration "production"**
---
## Test : Vlan External  (vlan53)
ansible all  -a "ping -c 2 10.29.244.1"
--> OK pour tous

## Test : Vlan internal/admin (vlan 54)

ansible all  -a "ping -c 2 192.168.73.101"
--> OK pour tous sauf localhost

ansible control   -a "ping -c 2 192.168.73.21"
ansible control   -a "ping -c 2 192.168.73.22"
ansible control   -a "ping -c 2 192.168.73.23"

## Test : Vlan Tunnel VxLAN (vlan 55)
ansible all  -a "ping -c 2 172.16.0.101"
--> OK pour tous sauf localhost

ansible control   -a "ping -c 2 172.16.0.21"
ansible control   -a "ping -c 2 172.16.0.22"
ansible control   -a "ping -c 2 172.16.0.23"
