#enable_cinder: "no"
enable_cinder: "yes"

#enable_cinder_backend_iscsi: "{{ enable_cinder_backend_lvm | bool }}"
#enable_cinder_backend_lvm: "no"
enable_cinder_backend_lvm: "yes"



## 
For Ubuntu and LVM2/iSCSI¶
iscsd process uses configfs which is normally mounted at /sys/kernel/config to store discovered targets information, on centos/rhel type of systems this special file system gets mounted automatically, which is not the case on debian/ubuntu. Since iscsid container runs on every nova compute node, the following steps must be completed on every Ubuntu server targeted for nova compute role.

Add configfs module to /etc/modules

Rebuild initramfs using: update-initramfs -u command

Stop open-iscsi system service due to its conflicts with iscsid container.

Ubuntu 16.04 (systemd): systemctl stop open-iscsi; systemctl stop iscsid

Make sure configfs gets mounted during a server boot up process. There are multiple ways to accomplish it, one example:

mount -t configfs /etc/rc.local /sys/kernel/config
 Note

There is currently an issue with the folder /sys/kernel/config as it is either empty or does not exist in several operating systems, see _bug 1631072 for more info


[https://docs.openstack.org/kolla-ansible/latest/reference/storage/cinder-guide.html](https://docs.openstack.org/kolla-ansible/latest/reference/storage/cinder-guide.html)
