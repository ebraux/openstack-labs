# Déploiement openstack-dfvs.imt-atlantique.fr

---
## Objectif : 

- Déploiement d'une instance d'Openstack, dédiée DFVS, pour les enseignements SRCD, et MS-ICD.

---
## Principe :

- Utilisation de Kolla-Ansible [https://docs.openstack.org/kolla-ansible/latest/](https://docs.openstack.org/kolla-ansible/latest)
- Déploiment sur Base Ubuntu 20.04
- Version Openstack : Yoga
- Utilisation des images Kolla standard

---
## Documentation

- [Architecture](./docs/architecture.md) mise en place 
- Configuration de l'[environnement](./docs/environment.md) 
- Préparation du réseau et des machines
    - [Network-configuration](./docs/Network-configuration.md)
    - [network-tests](./docs/network-tests.md)
- Déploiement de Kolla-ansible
    - [kolla-installation](./docs/kolla-installation.md)
    - [kolla-configuration](./docs/kolla-configuration.md)
    - [kolla-deploiement](./docs/kolla-deploiement.md)
    - [kolla-cinder](./docs/kolla-cinder.md)
- Operation du cluster
    - [kolla-operations](./docs/kolla-operations.md)
    - [Vérification du réseau](./docs/network-check.md)
    - [Gestion des compute](./docs/manage-compute.md)
