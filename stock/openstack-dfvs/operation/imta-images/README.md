#  Getsion des images imta

---
## Principe

1. Utilisation de packer pour générer les image en mode privé pour "admin"
2. download, compression et chargement des images dans openstack en mode "public"


---
## Création d'une image

### Configuration de l'environnement

- Image imta-ubuntu20
```
export IMAGE_NAME=imta-ubuntu20
export IMAGE_ID=00000000-0000-0000-0001-000000000103
```


- Image imta-ubuntu22
```
export IMAGE_NAME=imta-ubuntu22
export IMAGE_ID=00000000-0000-0000-0001-000000000100
```

- Image imta-docker
```
export IMAGE_NAME=imta-docker
export IMAGE_ID=00000000-0000-0000-0001-000000000102
```

- Image imta-centos9
```
export IMAGE_NAME=imta-centos9
export IMAGE_ID=00000000-0000-0000-0001-000000000104
```

- Image imta-centos8
```
export IMAGE_NAME=imta-centos8
export IMAGE_ID=00000000-0000-0000-0001-000000000106
```


###  Création de l'image  

``` bash
export IMAGE_MODEL=${IMAGE_NAME}
echo  ${IMAGE_MODEL}


packer validate  $IMAGE_MODEL.json
packer build  $IMAGE_MODEL.json
```

---
## Post traitement et cration d e l'image définitive

### POST Traitement de l'image

- Récupération de l'ID de l'image
```
export IMAGE_OS_ID=`openstack  image show -f value -c id ${IMAGE_NAME}_model`
echo $IMAGE_OS_ID
```
- Téléchargement de l'image en local
```
openstack image save --file tmp/$IMAGE_MODEL.raw $IMAGE_OS_ID
```
- Optimisation de l'image (compression, ...)
```
qemu-img convert -O qcow2 -c tmp/$IMAGE_MODEL.raw tmp/$IMAGE_MODEL.qcow
```

### Chargement de l'image définitive dans Openstack

- Import dans openstack
``` bash
echo $IMAGE_MODEL
echo $IMAGE_NAME
echo $IMAGE_ID
openstack image create \
    --id $IMAGE_ID \
    --public \
    --disk-format qcow2 \
    --container-format bare \
    --file tmp/$IMAGE_MODEL.qcow $IMAGE_NAME
```

---
##  Ménage

- Image "Model" dans Openstack
```
openstack image delete $IMAGE_OS_ID
```
- fichiers en local
```
rm tmp/$IMAGE_MODEL.raw tmp/$IMAGE_MODEL.qcow
```

