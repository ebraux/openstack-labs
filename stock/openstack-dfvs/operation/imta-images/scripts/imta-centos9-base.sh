#!/bin/bash



# proxy 

## configuration pour l'installation
export http_proxy='http://proxy.enst-bretagne.fr:8080'
export https_proxy='http://proxy.enst-bretagne.fr:8080'
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr

## pour le systeme
tee -a /etc/environment > /dev/null <<EOF
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
EOF


## proxy configuration for dnf
cp -p /etc/dnf/dnf.conf /etc/dnf/dnf.conf.DIST
sudo tee -a /etc/dnf/dnf.conf > /dev/null <<EOF
proxy=http://apt-cacher-01.priv.enst-bretagne.fr:3142
EOF


# system update
dnf -y update
dnf install -y man curl unzip  git vim tree nano jq


# System timezone / locale
#timedatectl set-timezone Europe/Paris
#apt install -y language-pack-fr language-pack-fr-base
#cp -p /etc/default/keyboard /etc/default/keyboard.DIST
#sed -i 's/XKBLAYOUT=.*/XKBLAYOUT="fr"/g' /etc/default/keyboard


# swap config
/bin/cp /etc/sysctl.conf /etc/sysctl.conf.DIST
cat >> /etc/sysctl.conf <<FIN
vm.swappiness = 0
FIN


# cleaning

