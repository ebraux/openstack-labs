#!/bin/bash

#  Ubuntu User
#cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config.DIST
#sed -i 's/PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#service sshd restart
#echo "ubuntu:ubuntu" | chpasswd

# avoid  the sudo unable to resolve
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts

# Configuration du proxy
## pour apt-get
echo 'Acquire::http::Proxy "http://apt-cacher-01.priv.enst-bretagne.fr:3142";' > /etc/apt/apt.conf.d/01proxy

## pour le systeme
cat >> /etc/environment <<FIN
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
FIN
#cat >> /etc/bash.bashrc <<FIN
#proxy_on() {
#    export http_proxy=http://proxy.enst-bretagne.fr:8080
#    export https_proxy=http://proxy.enst-bretagne.fr:8080
#FIN
#
#echo '    export no_proxy=localhost,127.0.0.1,'`hostname -I`  >> /etc/bash.bashrc
#
#cat >> /etc/bash.bashrc <<FIN
#}
#
#proxy_off() {
#    unset http_proxy
#    unset https_proxy
#    unset no_proxy
#}
#
#cat >> /etc/bash.bashrc <<FIN
#proxy_status() {
#    env | grep http_proxy
#    env | grep https_proxy
#    env | grep no_proxy
#}
#FIN

# proxy configuration pour l'installation
export http_proxy='http://proxy.enst-bretagne.fr:8080'
export https_proxy='http://proxy.enst-bretagne.fr:8080'
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr


# system update
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"
apt-get update

apt install -f man ssh curl unzip git vim tmux
apt install -f software-properties-common python-simplejson


# System timezone / locale
timedatectl set-timezone Europe/Paris
apt install -f language-pack-fr language-pack-fr-base
cp -p /etc/default/keyboard /etc/default/keyboard.DIST
sed -i 's/XKBLAYOUT=.*/XKBLAYOUT="fr"/g' /etc/default/keyboard


# swap config
/bin/cp /etc/sysctl.conf /etc/sysctl.conf.DIST
cat >> /etc/sysctl.conf <<FIN
vm.swappiness = 0
FIN


# cleaning
apt -y autoremove
apt -y autoclean
apt -y clean

