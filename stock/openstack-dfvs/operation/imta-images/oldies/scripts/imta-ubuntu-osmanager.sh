#!/bin/bash


# installation des clients openstack
apt install software-properties-common
add-apt-repository -y cloud-archive:rocky
apt update && sudo apt -y dist-upgrade

apt install -y python-openstackclient python-heatclient

echo "echo '--- Environnement pour : utilisateur  __VOTRE_LOGIN__, projet __VOTRE_PROJET__ ---' " > /home/ubuntu/openrc_sample
echo "export OS_AUTH_URL=https://openstack.imt-atlantique.fr:5000/v3"  >> /home/ubuntu/openrc_sample
echo "export OS_IDENTITY_API_VERSION=3" >> /home/ubuntu/openrc_sample
echo "export OS_PROJECT_DOMAIN_NAME=Default" >> /home/ubuntu/openrc_sample
echo "export OS_PROJECT_NAME=__VOTRE_PROJET__"  >> /home/ubuntu/openrc_sample
echo "export OS_USER_DOMAIN_NAME=Default"  >> /home/ubuntu/openrc_sample
echo "export OS_USERNAME=__VOTRE_LOGIN__"  >> /home/ubuntu/openrc_sample
echo "export OS_PASSWORD=__VOTRE_MOT_DE_PASSE__"  >> /home/ubuntu/openrc_sample
chown ubuntu.ubuntu /home/ubuntu/openrc_sample

