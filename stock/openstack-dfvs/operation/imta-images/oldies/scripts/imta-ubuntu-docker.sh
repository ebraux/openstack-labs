#!/bin/bash


# proxy configuration
export http_proxy='http://proxy.enst-bretagne.fr:8080'
export https_proxy='http://proxy.enst-bretagne.fr:8080'
proxy_on

# silent install
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"


#
# ADD Docker
#
apt update
apt install -y apt-transport-https ca-certificates curl software-properties-common

# Add the Docker repository to APT sources:
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /tmp/download.docker.co.gpg
#apt-key add /tmp/download.docker.co.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt-cache policy docker-ce

# Install Docker
# apt-cache policy docker-ce
apt install -y docker-ce
#systemctl status docker
# ubuntu user can Executing the Docker Command Without Sudo
usermod -a -G docker ubuntu

# config proxy
export HTTP_PROXY=$http_proxy
mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" > /etc/systemd/system/docker.service.d/proxy.conf
echo "Environment='HTTPS_PROXY=$HTTP_PROXY' 'HTTP_PROXY=$HTTP_PROXY'" >> /etc/systemd/system/docker.service.d/proxy.conf
systemctl daemon-reload
systemctl restart docker

# config Docker :
#  - DNS
echo '{' > /etc/docker/daemon.json
echo '    "dns": ["192.44.75.10", "192.108.115.2"]' >> /etc/docker/daemon.json
echo '}' >> /etc/docker/daemon.json


# Add Docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
