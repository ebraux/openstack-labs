SOURCE_ID=`openstack image list -f json | jq -r '.[] | select(.Name == "ubuntu_18.0.4_current") | .ID'`
FLAVOR_ID=`openstack flavor list -f json | jq -r '.[] | select(.Name == "m1.medium") | .ID'`
NETWORK_ID=`openstack network list -f json | jq -r '.[] | select(.Name == "private") | .ID'`
