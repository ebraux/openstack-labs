
IMAGE_NAME=imta-ubuntu-osmanager

export PACKER_LOG_PATH=tmp/packer-${IMAGE_NAME}.log
export PACKER_LOG=1
packer build ${IMAGE_NAME}.json 
MODEL_IMAGE_ID=`cat ${PACKER_LOG_PATH} | grep  'openstack: An image was created:' | awk '{print $10}'`

echo "Image ID ${MODEL_IMAGE_ID}"

glance image-download --file tmp/${IMAGE_NAME}.raw  ${MODEL_IMAGE_ID}
qemu-img convert -O qcow2 -c tmp/${IMAGE_NAME}.raw tmp/${IMAGE_NAME}.qcow
glance image-create  --name ${IMAGE_NAME}  --visibility public --disk-format qcow2  --container-format bare  --file tmp/${IMAGE_NAME}.qcow

glance image-delete ${MODEL_IMAGE_ID}
rm tmp/${IMAGE_NAME}.raw tmp/${IMAGE_NAME}.qcow
