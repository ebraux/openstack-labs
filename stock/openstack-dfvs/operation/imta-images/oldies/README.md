# Gestion des images "imta*"

Principe
1. Utilisation de packer pour générer les image en mode privé pour "admin"
2. download, compression et chargement des images dans openstack en mode "public"

1. chargement de la toolbox
```
cd
source 
```
2. création de l'image (ex imta-ubuntu-base )
```
packer validate  imta-ubuntu-base.json
packer build  imta-ubuntu-base.json
```
3. optimisatin de l'image
  * récupérer l'id de l'image dans la sortie de la commande précédente
  ```
    ==> Builds finished. The artifacts of successful builds are:
    --> openstack: An image was created: 45d66eca-bacb-406d-8727-1c7c8615f9d4
   ```
  * Télécharger l'image en local
  ```
  glance image-download --file tmp/img.raw 45d66eca-bacb-406d-8727-1c7c8615f9d4
  ```
  * optimisation del'image (compression, ...)
  ```
  qemu-img convert -O qcow2 -c tmp/img.raw tmp/img.qcow
% ```
  * import dans openstack
  ```
  glance image-create  --name imta-ubuntu-basic  --visibility public --disk-format qcow2  --container-format bare  --file tmp/img.qcow
   ```
  * ménage
  ```
  glance image-delete 45d66eca-bacb-406d-8727-1c7c8615f9d4
  rm tmp/img.raw tmp/img.qcow
  ```

