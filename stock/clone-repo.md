Récupérer l’intégralité du dépôt :
```bash
cd ~
git clone https://gitlab.com/ebraux/openstack-labs-installation.git
cd openstack-labs-installation
git checkout zed-u22.04
```

Vérifier la version du Lab
```bash
cat version.txt 
# zed-ubuntu22.04
```


- Pour le noeud "controller", depuis le dossier "02_controller" : 
```bash
cp -R docs/02_controller/conf ~/openstack_config
```
- Pour le noeud "compute, depuis le dossier "03_compute" :
```bash
cp -R docs/03_compute/conf ~/openstack_config
```