
https://docs.openstack.org/nova/rocky/admin/configuration/hypervisor-kvm.html

``` bash
 virsh list --all
 virsh dominfo <vm name>
 virsh console <vm name>

```


nova-compute-kvm
-> rien d'installé à propose de kvm ni grep qemu ou libvirt

au niveau noyau  OK
lsmod | grep kvm
kvm_amd               155648  0
ccp                   106496  1 kvm_amd
kvm                  1032192  1 kvm_amd



``` bash
 virsh list
 Id   Name   State
--------------------
```



lsmod | grep kvm

modprobe kvm
modprobe kvm-intel

 lsmod | grep kvm
modprobe -a kvm

/etc/modprobe.d/kvm.conf 
options kvm_intel nested=1
options kvm_amd nested=1



``` bash
sudo apt install -y qemu-kvm 
# virt-manager libvirt-daemon-system virtinst libvirt-clients
```

- qemu-kvm  – An opensource emulator and virtualization package that provides hardware emulation.
- libvirt-daemon-system – A package that provides configuration files required to run the libvirt daemon.

``` bash
sudo systemctl enable libvirtd
sudo systemctl restart libvirtd
sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER
```


Vérifier si KVM fonctionne correctement

``` bash
sudo apt install -y cpu-checker
kvm-ok
# kvm-ok
```

>  Il est également possible d'installer  virt-manager A Qt-based graphical interface for managing virtual machines via the libvirt daemon.


``` bash
sudo dnf install -y  openstack-nova-compute \
  libvirt python3-libvirt qemu-kvm virt-install \
  libguestfs-tools  libvirt-devel  virt-top
```