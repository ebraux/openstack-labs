 title: manage hostname

Set the hostname of your director’s host.

```bash
hostnamectl set-hostname director.example
hostnamectl set-hostname --transient director.example
```
