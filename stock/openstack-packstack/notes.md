#!/bin/bash


systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

NM_CONTROLLED=no

sudo dnf update -y
sudo dnf config-manager --enable crb
sudo dnf install -y centos-release-openstack-yoga
sudo setenforce 0
#setenforce 0 ; sed -i 's/=enforcing/=disabled/g' /etc/selinux/config
sudo dnf update -y
sudo dnf install -y openstack-packstack
sudo packstack --allinone
--keystone-admin-passwd=stack \
--keystone-demo-passwd=stack \
--os-heat-install=y \
--gen-answer-file=/root/train-answer.txt

# /var/tmp/packstack/20221025-121315-f_3k6kuy/openstack-setup.log

Additional information:
 * Parameter CONFIG_NEUTRON_L2_AGENT: You have chosen OVN Neutron backend. Note that this backend does not support the VPNaaS plugin. Geneve will be used as the encapsulation method for tenant networks
 * A new answerfile was created in: /root/packstack-answers-20221025-121316.txt
 * Time synchronization installation was skipped. Please note that unsynchronized time on server instances might be problem for some OpenStack components.
 * Warning: NetworkManager is active on 10.0.2.15. OpenStack networking currently does not work on systems that have the Network Manager service enabled.
 * File /root/keystonerc_admin has been created on OpenStack client host 10.0.2.15. To use the command line tools you need to source the file.
 * To access the OpenStack Dashboard browse to http://10.0.2.15/dashboard .
Please, find your login credentials stored in the keystonerc_admin in your home directory.
 * Because of the kernel update the host 10.0.2.15 requires reboot.
 * The installation log file is available at: /var/tmp/packstack/20221025-121315-f_3k6kuy/openstack-setup.log
 * The generated manifests are available at: /var/tmp/packstack/20221025-121315-f_3k6kuy/manifests

