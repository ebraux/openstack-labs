#!/bin/bash


# desactivation de firewald et  selinux
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX==disabled/g' /etc/selinux/config

sudo tee -a /etc/environment > /dev/null <<EOF
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

# -- puppet version management - desable epel repo
#sudo yum autoremove -y epel-release
#sudo yum clean all


sudo dnf config-manager --enable crb
sudo dnf install -y centos-release-openstack-yoga
sudo dnf update -y
sudo dnf install -y openstack-packstack

#nmcli conn add type bridge con-name br-ex ifname br-ex
#nmcli conn modify br-ex ipv4.addresses '172.24.4.1/24'
#nmcli conn modify br-ex ipv4.method manual
#sudo nmcli conn up br-ex

# --- Install Packstack
sudo packstack --allinone \
  --default-password=stack \
  --os-client-install=y \
  --os-glance-install=y \
  --os-nova-install=y \
  --os-neutron-install=y \
  --os-cinder-install=y \
  --os-swift-install=y \
  --os-horizon-install=y \
  --os-heat-install=y \
  --os-ceilometer-install=n \
  --os-aodh-install=n \
  --os-manila-install=n \
  --os-sahara-install=n \
  --os-magnum-install=n \
  --os-trove-install=n \
  --os-ironic-install=n \
  --gen-answer-file=/root/yoga-answer.txt

#  --os-panko-install=n \
#  --ntp-servers=1.fr.pool.ntp.org \
# https://www.server-world.info/en/note?os=CentOS_Stream_8&p=ntp&f=1
#1.fr.pool.ntp.org

# -d


#    --keystone-admin-passwd=stack \
#    --keystone-demo-passwd=stack \
#    --gen-answer-file=/root/victoria-answer.txt

packstack --answer-file=/root/yoga-answer.txt


# https://www.server-world.info/en/note?os=CentOS_Stream_8&p=ntp&f=1
#1.fr.pool.ntp.org


