#!/bin/bash


# desactivation de firewald et  selinux
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX==disabled/g' /etc/selinux/config


# desactivation de firewald et  selinux
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX==disabled/g' /etc/selinux/config

# Modification de la config réseau pour desactiver NetworkManager
#  - Backup de la config
mkdir /root/old-network-config
cp -p /etc/resolv.conf /root/old-network-config
ip a > /root/old-network-config/network-ip-a.txt
ip r > /root/old-network-config/network-ip-r.txt
cp -p /etc/sysconfig/network-scripts/ifcfg-eth0 /root/old-network-config/ifcfg-eth0.txt
nmcli device sho eth0 > /root/old-network-config/network-nmcli-eth0.txt

# - Passage de l'interface eth0 en statique (pb avec vagrant+DHCP+NetworkScripts)
UUID=`nmcli connection show | grep eth0 | awk '{print $3}'`
echo $UUID
ETH0_IP=`nmcli device show eth0 | grep IP4.ADDRESS | awk '{print $2}'`
echo $ETH0_IP
ETH0_DNS=`nmcli device show eth0 | grep IP4.DNS | awk '{print $2}'`
echo $ETH0_DNS
ETH0_GATEWAY=`nmcli device show eth0 | grep IP4.GATEWAY | awk '{print $2}'`
echo $ETH0_GATEWAY
nmcli connection modify $UUID IPv4.address $ETH0_IP
nmcli connection modify $UUID IPv4.gateway $ETH0_GATEWAY
nmcli connection modify $UUID IPv4.DNS $ETH0_DNS
nmcli connection modify $UUID IPv4.method manual
nmcli connection up $UUID
# - desactivation de la gestion des interface via NetworkManager
echo 'NM_CONTROLLED=no' | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-eth0
echo 'NM_CONTROLLED=no' | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-eth1
## - Installation de networks-scripts et desactivation de network-manager
sudo systemctl disable NetworkManager
sudo systemctl stop NetworkManager
systemctl disable NetworkManager-wait-online.service
systemctl disable NetworkManager-dispatcher.service
sudo dnf install network-scripts -y
sudo systemctl enable network
sudo systemctl start network


# --- install Apache HTTPD
sudo dnf update -y
sudo dnf install -y httpd


