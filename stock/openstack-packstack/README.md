

- [https://keithtenzer.com/openstack/openstack-15-stein-lab-installation-and-configuration-guide-for-hetzner-root-servers/](https://keithtenzer.com/openstack/openstack-15-stein-lab-installation-and-configuration-guide-for-hetzner-root-servers/)
- [https://aptira.com/installing-openstack-on-openstack-with-external-network/](https://aptira.com/installing-openstack-on-openstack-with-external-network/)

203.0.113.2/24

https://bugzilla.redhat.com/show_bug.cgi?id=1430181

packstack --allinone --default-password=xyz -d


https://github.com/robertluwang/cloud-hands-on-guide/blob/master/one%20NAT%20Network%20NIC%20manually%20packstack%20setup%20guide.md

https://www.linkedin.com/pulse/how-deploy-openstack-16-train-platform-using-packstack-goran-jumi%C4%87/

https://www.admin-magazine.com/Archive/2018/46/OpenStack-installation-with-the-Packstack-installer/(offset)/3


https://bugzilla.redhat.com/show_bug.cgi?id=1014774

5: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether aa:81:61:3c:28:43 brd ff:ff:ff:ff:ff:ff
    inet 172.24.4.1/24 scope global br-ex
       valid_lft forever preferred_lft forever




``` bash
[root@centos7 ~]$ cd /etc/sysconfig/network-scripts
[root@centos7 network-scripts]$ sudo cp ifcfg-eth0 ifcfg-br-ex
[root@centos7 network-scripts]$ cat ifcfg-eth0
DEVICE=eth0
ONBOOT=yes
TYPE=OVSPort
DEVICETYPE=ovs
OVS_BRIDGE=br-ex

[root@centos7 network-scripts]$ cat ifcfg-br-ex
DEVICE=br-ex
BOOTPROTO=static
ONBOOT=yes
TYPE=OVSBridge
DEVICETYPE=ovs
USERCTL=yes
PEERDNS=yes
IPV6INIT=no
IPADDR=172.24.4.1
NETMASK=255.255.255.0
GATEWAY=172.25.250.1
DNS1=172.25.250.1
DNS2=8.8.8.8
```


ERROR : Error appeared during Puppet run: 10.0.2.15_controller.pp
Error: Function lookup() did not find a value for the name 'DEFAULT_EXEC_TIMEOUT'
You will find full trace in log /var/tmp/packstack/20221025-222209-fy93zovl/manifests/10.0.2.15_controller.pp.log
Please check log file /var/tmp/packstack/20221025-222209-fy93zovl/openstack-setup.log for more information
Additional information:
 * Parameter CONFIG_NEUTRON_L2_AGENT: You have chosen OVN Neutron backend. Note that this backend does not support the VPNaaS plugin. Geneve will be used as the encapsulation method for tenant networks
 * Time synchronization installation was skipped. Please note that unsynchronized time on server instances might be problem for some OpenStack components.
 * File /root/keystonerc_admin has been created on OpenStack client host 10.0.2.15. To use the command line tools you need to source the file.
 * To access the OpenStack Dashboard browse to http://10.0.2.15/dashboard .
Please, find your login credentials stored in the keystonerc_admin in your home directory.


Additional information:
 * Parameter CONFIG_NEUTRON_L2_AGENT: You have chosen OVN Neutron backend. Note that this backend does not support the VPNaaS plugin. Geneve will be used as the encapsulation method for tenant networks
 * A new answerfile was created in: /root/packstack-answers-20221027-082750.txt
 * Time synchronization installation was skipped. Please note that unsynchronized time on server instances might be problem for some OpenStack components.
 * File /root/keystonerc_admin has been created on OpenStack client host 10.0.2.15. To use the command line tools you need to source the file.
 * To access the OpenStack Dashboard browse to http://10.0.2.15/dashboard .
Please, find your login credentials stored in the keystonerc_admin in your home directory.
 * The installation log file is available at: /var/tmp/packstack/20221027-082750-7r9uqd2m/openstack-setup.log
 * The generated manifests are available at: /var/tmp/packstack/20221027-082750-7r9uqd2m/manifests




cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-eth2
DEVICE=eth2
NAME="eth2"
ONBOOT=yes
TYPE=OVSPort
DEVICETYPE=ovs
OVS_BRIDGE=br-ex
BOOTPROTO="static"
IPV6INIT="no"
EOF

DEFROUTE="yes"
UUID="e20b64ec-fc48-4b21-b60f-e110f5380fc3"
DEVICE="eth0"



cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-br-ex
DEVICE=br-ex
NAME="br-ex"
BOOTPROTO="static"
TYPE=OVSBridge
DEVICETYPE=ovs
ONBOOT=yes
NM_CONTROLLED=no
BOOTPROTO=none
IPADDR="172.24.4.1"
PREFIX="24"
EOF


DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="no"
DEVICE="br-ex"
GATEWAY="192.168.122.1"
DNS1="10.43.138.12"


ovs-vsctl add-port br-ex eno


CONFIG_NEUTRON_ML2_VLAN_RANGES=physnet1:1100:1199,physnet0
CONFIG_NEUTRON_OVS_BRIDGE_MAPPINGS=physnet1:br-eth1,physnet0:br-ex
CONFIG_NEUTRON_OVS_BRIDGE_IFACES=br-eth1:eth1,br-ex:eth0
CONFIG_NEUTRON_OVS_BRIDGES_COMPUTE=br-eth1


 case hiera('CONFIG_NEUTRON_L2_AGENT') {
    'openvswitch': { include 'packstack::neutron::ovs_agent' }
    'linuxbridge': { include 'packstack::neutron::lb_agent' }
    'ovn':         { include 'packstack::neutron::ovn_agent' }
    default:       { include 'packstack::neutron::ovs_agent' }
  }

ip addr add 172.24.4.1/24 dev br-ex
sudo chmod +x /etc/rc.local
sudo systemctl start rc-local
$ sudo systemctl status rc-local

https://www.programmersought.com/article/42587785194/

---

https://www.golinuxhub.com/2018/07/step-guide-to-install-openstack-packstack-rhel7-controller-compute/

https://www.rdoproject.org/install/packstack/
https://www.admin-magazine.com/Archive/2018/46/OpenStack-installation-with-the-Packstack-installer/(offset)/3
https://www.linkedin.com/pulse/how-deploy-openstack-16-train-platform-using-packstack-goran-jumi%C4%87/


network :
- https://www.rdoproject.org/documentation/packstack-all-in-one-diy-configuration/#quantum-vs-neutron
- https://allthingsopen.com/2013/08/23/openstack-packstack-installation-with-external-connectivity/
- https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/14/html/networking_guide/bridge-mappings
- https://www.itzgeek.com/how-tos/linux/centos-how-tos/configure-openstack-networking-enable-access-vm-instances.html
- https://www.golinuxhub.com/2018/07/configure-openstack-ovsbridge-network-neutron-router/
- https://computingforgeeks.com/how-to-install-open-vswitch-on-centos-rhel/
- https://github.com/robertluwang/cloud-hands-on-guide/blob/master/one%20NAT%20Network%20NIC%20manually%20packstack%20setup%20guide.md
- https://kevin-wang-xin.medium.com/openstack-rdo-single-node-installation-and-configuration-on-vm-b4e6991556e4
- 















 --os-controller-host=''
  --os-neutron-ml2-mechanism-drivers='linuxbridge,l2population'
  --os-neutron-l3-ext-bridge='' \
  --os-neutron-ml2-type-drivers=flat,vlan,vxlan \
  --os-neutron-ml2-tenant-network-types=vxlan
  --os-neutron-ml2-flat-networks=physnet0 \
  --os-neutron-l2-agent=linuxbridge \
  --os-neutron-ovs-bridge-interfaces='physnet0:eth1' \
  --os-neutron-ovs-external-physnet='physnet0:br-ex' \
  --os-neutron-ovn-bridge-interfaces='physnet0:eth1' \
  --os-neutron-ovn-external-physnet='physnet0:br-ex' \


---
## Vérification de slogs

``` bash
cd /var/log
grep -ri error *
grep -ri error chrony/* 
grep -ri error cinder/* 
grep -ri error glance/* 
grep -ri error heat/* 
grep -ri error horizon/* 
grep -ri error httpd/* 
grep -ri error keystone/* 
grep -ri error libvirt/* 
grep -ri error mariadb/* 
grep -ri error neutron/* 
grep -ri error nova/* 
grep -ri error openvswitch/* 
grep -ri error placement/* 
grep -ri error rabbitmq/* 

grep -ri error messages/* 
grep -ri error puppet/* 
```

---
## Nova

### error  
- nova-compute.log
``` bash
nova-compute.log:2022-11-04 10:14:53.976 116087 ERROR nova.db.main.api [req-82bb50d6-5f30-4f5e-9725-3a2fa9645aca - - - - -] No DB access allowed in nova-compute:   File "/usr/lib/python3.9/site-packages/eventlet/greenthread.py", line 221, in main
```
- httpd.conf
``` bash
nova_api_wsgi_error.log:[Fri Nov 04 10:12:11.176285 2022] [wsgi:error] [pid 104553:tid 104620] Modules with known eventlet monkey patching issues were imported prior to eventlet monkey patching: urllib3. This warning can usually be ignored if the caller is only importing and not executing nova code.
```

### nova.conf

grep ^[^\#] /etc/nova/nova.conf > /etc/nova/nova.conf-nocomments

# Correction de la configuration de nova
sudo sed -i 's%enabled_apis=%enabled_apis = osapi_compute,metadata%' /etc/nova/nova.conf
sudo sed -i 's%transport_url=rabbit://guest:guest@10.0.2.15:5672/%transport_url=rabbit://guest:guest@172.16.51.61:5672/%' /etc/nova/nova.conf

en plus dans le fichier généré
``` ini
[neutron]
ovs_bridge=br-int
default_floating_pool=public
extension_sync_interval=600
timeout=30
```

### nova-compute.conf

grep ^[^\#] /etc/nova/nova-compute.conf > /etc/nova/nova-compute.conf-nocomments
--> vide

---
## Neutron

### error  
- metadata-agent.log
``` bash
 linuxbridge-agent.log:2022-11-04 11:17:17.219 142893 ERROR neutron.plugins.ml2.drivers.linuxbridge.agent.linuxbridge_neutron_agent [-] Tunneling cannot be enabled without the local_ip bound to an interface on the host. Please configure local_ip 0.0.0.0 on the host interface to be used for tunneling and restart the agent.
```

### /etc/neutron/plugins/ml2/

grep ^[^\#] /etc/nova/nova-compute.conf > /etc/nova/nova-compute.conf-nocomments

# - correction de la configuration de LinuxBridge
sudo sed -i 's/physical_interface_mappings=/physical_interface_mappings=physnet0:eth1/' /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo sed -i 's/local_ip=0.0.0.0/local_ip=172.16.50.51/' /etc/neutron/plugins/ml2/linuxbridge_agent.ini

### /etc/neutron/neutron.conf

grep ^[^\#] /etc/neutron/neutron.conf > /etc/neutron/neutron.conf-nocomments

Modifications
``` bash               
sudo sed -i 's%transport_url=rabbit://guest:guest@10.0.2.15:5672/%transport_url=rabbit://guest:guest@172.16.51.61:5672/%' /etc/neutron/neutron.conf
sudo sed -i 's%service_plugins=qos,trunk,router%service_plugins=router%' /etc/neutron/neutron.conf
```


Pas présent dans le fichier de config :
``` bash
# Configure Networking to notify Compute of network topology changes
notify_nova_on_port_status_changes = true
notify_nova_on_port_data_changes = true
```


### /etc/neutron/l3_agent.ini

grep ^[^\#] /etc/neutron/l3_agent.ini > /etc/neutron/l3_agent.ini-nocomments

Pas de modifications.


### /etc/neutron/dhcp_agent.ini

``` bash
grep ^[^\#] /etc/neutron/dhcp_agent.ini > /etc/neutron/dhcp_agent.ini-nocomments
vim dhcp_agent.ini-nocomments
```

Modifications.

``` bash               
sudo sed -i 's%enable_isolated_metadata=False%enable_isolated_metadata=True%' /etc/neutron/dhcp_agent.ini
```


### /etc/neutron/metadata_agent.ini

``` bash
grep ^[^\#] /etc/neutron/metadata_agent.ini > /etc/neutron/metadata_agent.ini-nocomments
vim metadata_agent.ini-nocomments
```

Modifications.

``` bash               
sudo sed -i 's%enable_isolated_metadata=False%enable_isolated_metadata=True%' /etc/neutron/dhcp_agent.ini
```




### /etc/neutron/plugins/ml2/linuxbridge_agent.ini

``` bash
grep ^[^\#] /etc/neutron/plugins/ml2/linuxbridge_agent.ini > /etc/neutron/plugins/ml2/linuxbridge_agent.ini-nocomments
vim /etc/neutron/plugins/ml2/linuxbridge_agent.ini-nocomments
```


Modifications:
``` bash               
sudo sed -i 's%local_ip=0.0.0.0%local_ip=172.16.51.61%' /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo sed -i 's%physical_interface_mappings=%physical_interface_mappings=provider:eth1%' /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo sed -i 's%firewall_driver=iptables%firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver% ' /etc/neutron/plugins/ml2/linuxbridge_agent.ini

Pas présents dans le fichier de config : 
``` bash
bridge_mappings = provider:br-ex
enable_security_group = true
enable_vxlan = true
l2_population = true
```

### /etc/neutron/plugins/ml2/ml2_conf.ini

``` bash
grep ^[^\#] /etc/neutron/plugins/ml2/ml2_conf.ini > /etc/neutron/plugins/ml2/ml2_conf.ini-nocomments
vim /etc/neutron/plugins/ml2/ml2_conf.ini-nocomments
```

Modifications
``` bash               

sudo sed -i 's%path_mtu=0%path_mtu=1500%' /etc/neutron/plugins/ml2/ml2_conf.ini
sudo sed -i 's%extension_drivers=port_security,qos%extension_drivers=port_security%' /etc/neutron/plugins/ml2/ml2_conf.ini
sudo sed -i 's%flat_networks=physnet0%flat_networks=provider%' /etc/neutron/plugins/ml2/ml2_conf.ini
```

---
## prise en compte des modifs

sudo  systemctl stop \
  openstack-nova-scheduler.service \
  openstack-nova-conductor.service \
  openstack-nova-novncproxy.service \
  openstack-nova-compute.service 
sudo  systemctl stop httpd

rm -f /var/log/nova/*.log

sudo  systemctl start httpd

sudo  systemctl start \
  openstack-nova-scheduler.service \
  openstack-nova-conductor.service \
  openstack-nova-novncproxy.service \
  openstack-nova-compute.service 


grep -ri error /var/log/nova/* 


sudo  systemctl stop \
   neutron-server.service \
   neutron-linuxbridge-agent.service \
   neutron-metadata-agent.service \
   neutron-l3-agent.service \
   neutron-dhcp-agent.service \

rm -f /var/log/neutron/*.log
sudo  systemctl start \
   neutron-server.service \
   neutron-linuxbridge-agent.service \
   neutron-metadata-agent.service \
   neutron-l3-agent.service \
   neutron-dhcp-agent.service

grep -ri error /var/log/neutron/* 
    
#neutron-metadata-agent.service
#neutron-ovs-cleanup.service
#neutron-linuxbridge-cleanup.service
# neutron-netns-cleanup.service



keystone_authtoken.service_token_roles_required set to False
0.0.0.0:8774


CONFIG_NEUTRON_OVS_BRIDGE_MAPPINGS=physnet0:br-ex
CONFIG_NEUTRON_OVN_BRIDGE_MAPPINGS=extnet:br-ex

neutron.conf :
service_plugins=qos,trunk,router
-> service_plugins=router

IP controller : 
10.0.2.15
-->

Ml2

#mechanism_drivers = linuxbridge
#--> mechanism_drivers = linuxbridge,l2population

path_mtu = 0
--> path_mtu = 1500

extension_drivers=port_security,qos
-- > extension_drivers=port_security

[securitygroup]
enable_security_group=True
[securitygroup]
enable_ipset = true


dans TP :
flat_networks = provider
--> flat_networks=physnet0


linuxbridge.ini

local_ip = 0.0.0.0

[vxlan]
enable_vxlan = true
local_ip = 172.16.50.21
l2_population = true


[linux_bridge]
physical_interface_mappings=physnet0:eth1
--> flat_networks=physnet0
bridge_mappings = physnet0:br-ex

---
L3 agent : 
[DEFAULT]
interface_driver = linuxbridge


---
DHCP
[DEFAULT]
interface_driver = linuxbridge
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true

enable_isolated_metadata=False
enable_metadata_network=False
state_path=/var/lib/neutron
resync_interval=30
interface_driver=neutron.agent.linux.interface.BridgeInterfaceDriver
root_helper=sudo neutron-rootwrap /etc/neutron/rootwrap.conf


---
metadata
nova_metadata_host=10.0.2.15


---
## heat

### error  
- heat-engine.log
``` bash
heat-engine.log:2022-11-04 10:09:16.489 104083 WARNING heat.common.pluginutils [-] Encountered exception while loading heat.engine.clients.os.blazar: "No module named 'blazarclient'". Not using blazar.reservation.: ModuleNotFoundError: No module named 'blazarclient'
```


---
## Opération post-installation

### - Creation du réseau external
``` bash
source /root/keystonerc_admin
openstack network create  --share --external \
#  --provider-physical-network provider \
  --provider-network-type flat   external
openstack subnet create --network   external \
  --allocation-pool start=203.0.113.101,end=203.0.113.250 \
  --dns-nameserver 8.8.4.4 --gateway 203.0.113.1 \
  --subnet-range 203.0.113.0/24 \
    external
```

### Creation de l'environnement de démo

- Utilisateur/group demo
``` bash
openstack project create \
  --domain default \
  --description "Demo Project" demo

openstack user create \
  --domain default \
  --password stack \
  demo

openstack role add --project demo --user demo member  
openstack role add --project demo --user admin member

sudo cp -p /root/keystonerc_admin /root/keystonerc_demo
sudo sed -i 's/admin/demo/g' /root/keystonerc_demo
```

### Image et Gabarit Cirros

``` bash
openstack flavor create --ram 256 --disk 1 --vcpus 1 cirros
wget http://download.cirros-cloud.net/0.5.2/cirros-0.5.2-x86_64-disk.img
```

``` bash
openstack image create "cirros" \
  --file cirros-0.5.2-x86_64-disk.img  \
  --disk-format qcow2 \
  --container-format bare \
  --public
```
``` bash
rm -f cirros-0.5.2-x86_64-disk.img
```


---
## pb lancement nova-api
``` bash
netstat -ltunp
tcp        0      0 0.0.0.0:8774            0.0.0.0:*               LISTEN      104550/httpd        
tcp        0      0 0.0.0.0:8775            0.0.0.0:*               LISTEN      104550/httpd        
tcp        0      0 0.0.0.0:8778            0.0.0.0:*               LISTEN      104550/httpd        
tcp        0      0 0.0.0.0:8776            0.0.0.0:*               LISTEN      103565/python3      
```
``` bash
ps -ef | grep nova
nova      104250       1  0 10:09 ?        00:00:02 /usr/bin/python3 /usr/bin/nova-novncproxy --web /usr/share/novnc/
nova      104553  104550  0 10:09 ?        00:00:01 nova_api_wsgi   -DFOREGROUND
nova      104554  104550  0 10:09 ?        00:00:01 nova_api_wsgi   -DFOREGROUND
nova      104555  104550  0 10:09 ?        00:00:00 nova_metadata_w -DFOREGROUND
nova      104556  104550  0 10:09 ?        00:00:00 nova_metadata_w -DFOREGROUND
nova      106544       1  0 10:12 ?        00:00:27 /usr/bin/python3 /usr/bin/nova-conductor
nova      106584  106544  0 10:12 ?        00:00:07 /usr/bin/python3 /usr/bin/nova-conductor
nova      106585  106544  0 10:12 ?        00:00:07 /usr/bin/python3 /usr/bin/nova-conductor
nova      106663       1  0 10:12 ?        00:00:26 /usr/bin/python3 /usr/bin/nova-scheduler
nova      106732  106663  0 10:12 ?        00:00:03 /usr/bin/python3 /usr/bin/nova-scheduler
nova      106733  106663  0 10:12 ?        00:00:03 /usr/bin/python3 /usr/bin/nova-scheduler
nova      116087       1  0 10:14 ?        00:00:11 /usr/bin/python3 /usr/bin/nova-compute
root      146932  142799  0 11:26 pts/1    00:00:00 grep --color=auto nova
```
``` bash
systemctl status openstack-nova-api.service 
○ openstack-nova-api.service - OpenStack Nova API Server
     Loaded: loaded (/usr/lib/systemd/system/openstack-nova-api.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

 systemctl status openstack-nova-metadata-api.service
○ openstack-nova-metadata-api.service - OpenStack Nova Metadata API Server
     Loaded: loaded (/usr/lib/systemd/system/openstack-nova-metadata-api.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

systemctl status openstack-nova-os-compute-api.service
○ openstack-nova-os-compute-api.service - OpenStack Nova Compute API Server
     Loaded: loaded (/usr/lib/systemd/system/openstack-nova-os-compute-api.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

```
### fichier config httpd

- 10-nova_api_wsgi.conf
``` bash
# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
# 
<VirtualHost 0.0.0.0:8774>
  ServerName packstackyoga

  ## Vhost docroot
  DocumentRoot "/var/www/cgi-bin/nova"

  ## Directories, there should at least be a declaration for /var/www/cgi-bin/nova

  <Directory "/var/www/cgi-bin/nova">
    Options -Indexes +FollowSymLinks +MultiViews
    AllowOverride None
    Require all granted
  </Directory>

  ## Logging
  ErrorLog "/var/log/httpd/nova_api_wsgi_error.log"
  ServerSignature Off
  CustomLog "/var/log/httpd/nova_api_wsgi_access.log" combined
  SetEnvIf X-Forwarded-Proto https HTTPS=1

  ## WSGI configuration
  WSGIApplicationGroup %{GLOBAL}
  WSGIDaemonProcess nova-api display-name=nova_api_wsgi group=nova processes=2 threads=1 user=nova
  WSGIProcessGroup nova-api
  WSGIScriptAlias / "/var/www/cgi-bin/nova/nova-api"
</VirtualHost>
~                 
```

- 10-nova_metadata_wsgi.conf
``` bash
# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
# 
<VirtualHost 0.0.0.0:8775>
  ServerName packstackyoga

  ## Vhost docroot
  DocumentRoot "/var/www/cgi-bin/nova"

  ## Directories, there should at least be a declaration for /var/www/cgi-bin/nova

  <Directory "/var/www/cgi-bin/nova">
    Options -Indexes +FollowSymLinks +MultiViews
    AllowOverride None
    Require all granted
  </Directory>

  ## Logging
  ErrorLog "/var/log/httpd/nova_metadata_wsgi_error.log"
  ServerSignature Off
  CustomLog "/var/log/httpd/nova_metadata_wsgi_access.log" combined
  SetEnvIf X-Forwarded-Proto https HTTPS=1

  ## WSGI configuration
  WSGIApplicationGroup %{GLOBAL}
  WSGIDaemonProcess nova-metadata display-name=nova_metadata_wsgi group=nova processes=2 threads=1 user=nova
  WSGIProcessGroup nova-metadata
  WSGIScriptAlias / "/var/www/cgi-bin/nova/nova-metadata-api"
</VirtualHost>
```

