#!/bin/bash

# Disable Firewald 
sudo systemctl stop firewalld
sudo systemctl disable firewalld

# Disable SE Linux
sudo setenforce 0
sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config
sudo sed -i 's/enforcing/disabled/g' /etc/sysconfig/selinux

# Update packages
sudo dnf update -y

# Puppet version management : disable epel repo
sudo yum autoremove -y epel-release
sudo yum clean all

# Install Openstack Repo
sudo dnf config-manager --enable crb
sudo dnf install -y centos-release-openstack-yoga
sudo dnf update -y

# Install Tools and utilities
sudo dnf install -y libcurl-devel  openssl-devel
sudo dnf install -y git vim tree

# Enable password Authentification
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd.service


# Create stack user
groupadd stack
useradd -g stack       -s /bin/bash -d /home/stack  -m  stack
cd /etc/sudoers.d
echo "stack ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_stack_sh
chmod 440 /etc/sudoers.d/50_stack_sh
echo stack:stack | chpasswd

# Archivage de la clé insedure de vagrant pour pouvoir faire une box si besoin
sudo cp -p /root/.ssh/authorized_keys /root/.ssh/authorized_keys.vagrant-insecure

# ----------------------------------------------------------
#      Packstack specific
# ----------------------------------------------------------

sudo tee -a /etc/environment > /dev/null <<EOF
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

sudo dnf install -y openstack-packstack

# Création du bridge pour br-ex
sudo nmcli conn add type bridge con-name br-ex ifname br-ex
sudo nmcli conn modify br-ex ipv4.addresses '203.0.113.2/24'
sudo nmcli conn modify br-ex ipv4.method manual
sudo nmcli conn up br-ex


# Modification de la config réseau pour desactiver NetworkManager
#  - Backup de la config
mkdir /root/old-network-config
cp -p /etc/resolv.conf /root/old-network-config
ip a > /root/old-network-config/network-ip-a.txt
ip r > /root/old-network-config/network-ip-r.txt
cp -p /etc/sysconfig/network-scripts/ifcfg-eth0 /root/old-network-config/ifcfg-eth0.txt
cp -p /etc/sysconfig/network-scripts/ifcfg-eth0 /root/old-network-config/ifcfg-eth1.txt
cp -p /etc/sysconfig/network-scripts/ifcfg-br-ex /root/old-network-config/ifcfg-br-ex.txt
nmcli device sho eth0 > /root/old-network-config/network-nmcli-eth0.txt
nmcli device sho eth1 > /root/old-network-config/network-nmcli-eth1.txt
nmcli device sho br-ex > /root/old-network-config/network-nmcli-br-ex.txt

# - Passage de l'interface eth0 en statique (pb avec vagrant+DHCP+NetworkScripts)
UUID=`nmcli connection show | grep eth0 | awk '{print $3}'`
echo $UUID
ETH0_IP=`nmcli device show eth0 | grep IP4.ADDRESS | awk '{print $2}'`
echo $ETH0_IP
ETH0_DNS=`nmcli device show eth0 | grep IP4.DNS | awk '{print $2}'`
echo $ETH0_DNS
ETH0_GATEWAY=`nmcli device show eth0 | grep IP4.GATEWAY | awk '{print $2}'`
echo $ETH0_GATEWAY
nmcli connection modify $UUID IPv4.address $ETH0_IP
nmcli connection modify $UUID IPv4.gateway $ETH0_GATEWAY
nmcli connection modify $UUID IPv4.DNS $ETH0_DNS
nmcli connection modify $UUID IPv4.method manual
nmcli connection up $UUID
# - desactivation de la gestion des interface via NetworkManager
echo 'NM_CONTROLLED=no' | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-eth0
echo 'NM_CONTROLLED=no' | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-eth1
echo 'NM_CONTROLLED=no' | sudo tee -a /etc/sysconfig/network-scripts/ifcfg-br-ex
## - Installation de networks-scripts et desactivation de network-manager
sudo systemctl disable NetworkManager
sudo systemctl stop NetworkManager
systemctl disable NetworkManager-wait-online.service
systemctl disable NetworkManager-dispatcher.service
sudo dnf install network-scripts -y
sudo systemctl enable network
sudo systemctl start network

# connexion ssh as root for packstack, IP 172.16.50.51
sudo mkdir /root/.ssh
sudo chmod 700 /root/.ssh
sudo ssh-keygen -q -t rsa -b 2048 -N '' -f /root/.ssh/id_rsa <<< y
sudo ls -la /root
sudo ls -la /root/.ssh
sudo touch /root/.ssh/authorized_keys
sudo cat /root/.ssh/id_rsa.pub | sudo tee -a /root/.ssh/authorized_keys



# --- Install Packstack
sudo packstack --allinone \
  --os-controller-host='172.16.51.61' \
  --default-password=stack \
  --provision-demo=n \
  --os-client-install=y \
  --os-glance-install=y \
  --os-nova-install=y \
  --os-neutron-install=y \
  --os-neutron-metering-agent-install=n \
  --os-cinder-install=y \
  --os-swift-install=n \
  --os-horizon-install=y \
  --os-heat-install=y \
  --os-ceilometer-install=n \
  --os-aodh-install=n \
  --os-manila-install=n \
  --os-sahara-install=n \
  --os-magnum-install=n \
  --os-trove-install=n \
  --os-ironic-install=n \
  --os-neutron-l2-agent=linuxbridge \
  --os-neutron-ml2-mechanism-drivers='linuxbridge,l2population' \
  --os-neutron-ml2-type-drivers='flat,vlan,vxlan' \
  --os-neutron-ml2-tenant-network-types=vxlan \
  --os-neutron-ml2-flat-networks=physnet0 \
  --os-neutron-l3-ext-bridge='' \
  --gen-answer-file=/root/yoga-answer.txt

#  --os-neutron-ovs-bridge-interfaces='physnet0:eth1' \
#  --os-neutron-ovs-external-physnet='physnet0:br-ex' \
#  --os-neutron-ovn-bridge-interfaces='physnet0:eth1' \
#  --os-neutron-ovn-external-physnet='physnet0:br-ex' \
#  --provision-demo-floatrange='203.0.113.2/24' \

#  --os-panko-install=n 

# --os-neutron-lb-interface-mappings=physnet0:ens192
#  --ntp-servers=1.fr.pool.ntp.org \
# https://www.server-world.info/en/note?os=CentOS_Stream_8&p=ntp&f=1
#1.fr.pool.ntp.org

# -d

packstack --answer-file=/root/yoga-answer.txt

