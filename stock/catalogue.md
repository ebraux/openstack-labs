``` bash
openstack catalog list
# +-----------+-----------+-----------------------------------------+
# | Name      | Type      | Endpoints                               |
# +-----------+-----------+-----------------------------------------+
# | glance    | image     | RegionOne                               |
# |           |           |   internal: http://controller:9292      |
# |           |           | RegionOne                               |
# |           |           |   admin: http://controller:9292         |
# |           |           | RegionOne                               |
# |           |           |   public: http://controller:9292        |
# |           |           |                                         |
# | placement | placement | RegionOne                               |
# |           |           |   internal: http://controller:8778      |
# |           |           | RegionOne                               |
# |           |           |   admin: http://controller:8778         |
# |           |           | RegionOne                               |
# |           |           |   public: http://controller:8778        |
# |           |           |                                         |
# | keystone  | identity  | RegionOne                               |
# |           |           |   admin: http://controller:5000/v3/     |
# |           |           | RegionOne                               |
# |           |           |   internal: http://controller:5000/v3/  |
# |           |           | RegionOne                               |
# |           |           |   public: http://controller:5000/v3/    |
# |           |           |                                         |
# | nova      | compute   | RegionOne                               |
# |           |           |   internal: http://controller:8774/v2.1 |
# |           |           | RegionOne                               |
# |           |           |   public: http://controller:8774/v2.1   |
# |           |           | RegionOne                               |
# |           |           |   admin: http://controller:8774/v2.1    |
# |           |           |                                         |
# +-----------+-----------+-----------------------------------------+
```