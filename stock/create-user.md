# Création de l'utilisateur démo

---
## Initialisation de l'environnement

``` bash
source ~/admin-openrc
env | grep OS_
# OS_AUTH_URL=http://controller:5000/v3
# OS_PROJECT_DOMAIN_NAME=Default
# OS_USERNAME=admin
# OS_USER_DOMAIN_NAME=Default
# OS_PROJECT_NAME=admin
# OS_PASSWORD=stack
# OS_IDENTITY_API_VERSION=3
```

---
## Création de l'utilisateur démo

Initialisation
``` bash
NEW_USER=demo
NEW_PROJECT=demo
```

Création du projet
``` bash
openstack project create \
  --domain default \
  --description "${NEW_PROJECT} Project" \
  ${NEW_PROJECT}
```

Création de l'utilisateur
``` bash
openstack user create \
  --domain default \
  --password stack \
  ${NEW_USER}
```

Affectation des roles
``` bash
openstack role add --project ${NEW_PROJECT} --user ${NEW_USER} member
openstack role add --project ${NEW_PROJECT} --user ${NEW_USER} member
```

Création du fichier d'environnement :
``` bash
tee ~/${NEW_USER}-openrc  <<EOF
OS_AUTH_URL=http://controller:5000/v3
OS_PROJECT_DOMAIN_NAME=Default
OS_USERNAME=${NEW_USER}
OS_USER_DOMAIN_NAME=Default
OS_PROJECT_NAME=${NEW_PROJECT}
OS_PASSWORD=stack
OS_IDENTITY_API_VERSION=3
EOF
```

```bash
cat ~/${NEW_USER}-openrc
```

