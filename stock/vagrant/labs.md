# Description des LABS


---
# Utilisation des LABS

Lancement des machines virtuelles avec l'outil Vagrant :
- Ouvrir un terminal
- Se déplacer dans le dossier du TP
- Taper la commande `vagrant up`
- Attendre un peu...


---
Pour l'accès aux machines, vagrant s'appuie sur le mécanisme de redirection de port : 
- l'IP est locahost (127.0.0.1)
- des ports sont afféctés à chaque service, et le traffic est redirigé vers la VM (NAT)

---
# 2 environnements de LAB


Sur chaque poste, 2 environnement de LAB sont disponibles :
- Openstack101 : une instance Openstack fonctionnelle, utilisée pour les labs de prise en main.
- InsideOpenstack : 2 VMs, sur lesquelles déployer une infrastructure Openstack.

> Un fichier aide-mémoire, reprend les informations d'accès.

