
vagrant up

vagrant halt

vagrant package --base osbasenodeyoga --output os-base-yoga.box

vagrant box add --name ebraux/os-base-yoga os-base-yoga.box

---

Obtenir le checksum
sha1sum  os-aio-yoga.box


## pb de ssh
liée à la clé que Vagrant va pousser dans l'image.
il faut rétablir la clé par défaut (insecure).
ou utiliser l'option
https://opensharing.fr/vagrant-authentication-failure-fr


# Déploiement en local

vagrant box add --name ebraux/os-aio-yoga os-aio-yoga.box
vagrant box list
