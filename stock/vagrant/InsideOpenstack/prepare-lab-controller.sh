#!/bin/bash

# Pre-download packages
#dnf install -y --downloadonly python3-openstackclient
#dnf install -y --downloadonly vim jq nano
#dnf install -y --downloadonly chrony
#dnf install -y --downloadonly mariadb mariadb-server python2-PyMySQL
#dnf install -y --downloadonly rabbitmq-server
#dnf install -y --downloadonly memcached python3-memcached
#dnf install -y --downloadonly etcd
#dnf install -y --downloadonly httpd python3-mod_wsgi
#
#dnf install -y --downloadonly openstack-keystone
#dnf install -y --downloadonly openstack-glance
#dnf install -y --downloadonly openstack-placement-api python3-osc-placement
#dnf install -y --downloadonly openstack-nova-api openstack-nova-conductor openstack-nova-novncproxy openstack-nova-scheduler
#dnf install -y --downloadonly openstack-neutron openstack-neutron-ml2 openstack-neutron-linuxbridge ebtables
#dnf install -y --downloadonly openstack-cinder lvm2 device-mapper-persistent-data targetcli python3-keystone
#dnf install -y --downloadonly openstack-dashboard

#
cd /home/stack
git clone https://gitlab.com/ebraux/openstack-labs-installation.git
cd openstack-labs-installation/
git checkout yoga-centos9s
cp -R docs/02_controller/conf /home/stack/openstack_config
chown -R stack.stack /home/stack/openstack-labs-installation/
chown -R stack.stack /home/stack/openstack_config



