
# Environemment du Lab utilisant Vagrant

---
## Description de l'environnement

Vagrant est un utilitaire qui permet de déployer simplement des architectures à base de VM [https://www.vagrantup.com/](https://www.vagrantup.com/).

Vagrant s'appuie sur Virtualbox et :

- Déploie 2 machines virtuelles
    - une VM "controller", pour le control-plane Openstack
    - une VM "compute", pour un noeud de compute
- Configure un réseau privé entre les 2 VMs (172.16.50.0/24) 
    - Adresse IP du 'controller' :  172.16.50.21.
    - Adresse IP du 'controller' :  172.16.50.31.
- Mets en place des redirection de ports depuis la machine hôte, pour accéder aux services sur les VM :
    - ssh sur le controller : 2421
    - ssh sur le comute : 2431
    - accès à Horizon : 8421

---
## Déploiment de l'environnement

Récupérer l'intégralité du dépot des labs
```bash
git clone https://gitlab.com/ebraux/openstack-labs-installation.git
cd openstack-labs-installation/
git checkout yoga-centos9s
cd ..
```

Vérifier la version du TP
```bash
cat openstack-labs-installation/version.txt
# yoga-centos9s
```

Copier le dossier de configuration de vagrant dans un dossier local (ici le dossier "InsideOpenstack" à la racine du HomeDir )
```bash
cp -R openstack-labs-installation/vagrant ~/InsideOpenstack
ll ~/InsideOpenstack 
```
Lancer l'environnement de TP
```bash
cd ~/InsideOpenstack
vagrant up
```

---
## Utilisation de l'environnement

> Les mots de passe sont toujours "stack".

### Accès à la VM "Controller"

- Connexion via ssh
    - utilisateur :  stack
    - mot de passe : stack
    - serveur : localhost
    - port : 2421 (le port 2421 est redirigé vers le port 22 de la macine controller)

Exemple de connexion en ligne de commande
``` bash
ssh -p 2421 stack@localhost
```

Exemple de copie de fichier avec scp
``` bash
scp -P 2421 demo.pem stack@localhost:/home/stack
```

- Accés à Horizon une fois installé : [http://localhost:8421](http://localhost:8421)


### Accès à la VM "Compute"

- Connexion via ssh
    - utilisateur :  stack
    - mot de passe : stack
    - serveur : localhost
    - port : 2431 (le port 2431 est redirigé vers le port 22 de la macine compute)

Exemple de connexion en ligne de commande
``` bash
ssh -p 2431 stack@localhost
```

---
## Gestion de l'environnement

Pour suspendre le LAB 
```bash
vagrant suspend
```
Pour reprendre le LAB 
```bash
vagrant resume
```

Une fois le LAB terminé, pour supprimer l'environnement
```bash
vagrant  destroy -f
```








