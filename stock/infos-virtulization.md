
### Informations sur le(s) cpu

- Nombre de processeurs : exemple pour 4 processeurs
``` bash
egrep -c '(processor)'  /proc/cpuinfo
# 4
```
- Type de processeur :  AMD oi Intel
``` bash
cat /proc/cpuinfo  | grep vendor_id
# vendor_id	: AuthenticAMD
```
Vérification du support ou non de la virtualisation par le processeur : recherche des flag vmx ou svm
``` bash
egrep -c '(vmx|svm)' /proc/cpuinfo
# 0
```
- AMD flag : svm (Secure virtual machine)
- Intel Flag : vmx (Hardware virtualization)
- '0' signifie que le processeur ne propose pas d’accélération matérielle. C'est souvent le cas des VM. Il faut donc utiliser l'émulateur QEMU au lieu de KVM.
- '1' signifie que le processeur propose de l'accélération matérielle. C'est soit une  machine physique, soit une VM supportant le mode "nested"
- plus d'informations [https://rtfmp.wordpress.com/2016/03/21/what-does-vmx-svm-cpu-flags-mean/](https://rtfmp.wordpress.com/2016/03/21/what-does-vmx-svm-cpu-flags-mean/)


### Informations sur le noyau

Vérification des modules chargés
``` bash
lsmod | grep kvm
# kvm_amd               155648  0
# ccp                   106496  1 kvm_amd
# kvm                  1032192  1 kvm_amd
```

> Dans le cas d'une VM fonctionnant sur un systéme KVM, il est possible d'activer le mode "Nested KVM", au lieu de l’émulateur QEMU.
