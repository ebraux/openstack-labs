title: "the Pythonic way"
description: Gestion des ressources dans un cloud OpenStack ("the Pythonic way")

# Introduction 

De la même manière que l'on créé des ressources (instance, réseau, router, ip flottante...) dans un cloud OpenStack en ligne de commande ou via Horizon, nous pouvons réaliser ces opérations dans des scripts Python.

Liens ressources documentaire : 

* [https://github.com/openstack/openstacksdk](https://github.com/openstack/openstacksdk)
* [API Documentation](https://docs.openstack.org/openstacksdk/latest/user/index.html#api-documentation)

# Quelques exemples

Ici les variables d'environnement OS_* seront utilisées.

Connexion au controleur, affichage du token en cours.
```python
import openstack
openstack.enable_logging(debug=False)
conn = openstack.connect()
print(conn.auth_token)
'gAAAAABeMrfERnTV-NPy2ss01MqR9Tlqh1LRsEZ_WYNJpHq1_7StoyG0H3VGxOMJrQt-3V-APqOmJLyD74eQv6W2gEZB5PoSsWPVsrUQPAxPr_v1P4SSQ2a-kZtLWHOTi0GpyMoYTctEqC-Oj9qwxl-mVdUaSx6TiNuHvZ0P6mjfW-cWORkWSeO'
```

Lister les instances du projet en cours
```python
for s in conn.compute.servers():
	print(s.name)
```

Lister les images accessibles
```python
for i in conn.image.images():
	print(i.name)
```

Créer un gabarit (demande les droits admins)

Pour cet exemple, il a été nécessaire de consulter la documentation de l'API OpenStack [https://docs.openstack.org/openstacksdk/latest/user/resources/compute/v2/flavor.html#openstack.compute.v2.flavor.Flavor](https://docs.openstack.org/openstacksdk/latest/user/resources/compute/v2/flavor.html#openstack.compute.v2.flavor.Flavor)

```python
custflavor = conn.compute.find_flavor('1c.lowmem.custom',
                                      ignore_missing=True)
if not custflavor:
    conn.compute.create_flavor(name='1c.lowmem.custom',
                               disk=1,
                               ram=1,
                               vcpus=1,
                               is_public=False)
```

# A vous de jouer

Le but de cet atelier est d'utiliser le langage Python pour :

* trouver un réseau
* trouver l'image nommée 'cirros'
* créer une instance, si celle-ci n'existe pas déjà
* appliquer un groupe de sécurité sur l'instance

# On va plus loin ?

Vous avez déployé 100 instances dans OpenStack dans lesquelles tourne votre application web, vous avez utilisé des instances de gabarit m1.tiny, mais c'est trop juste et votre application manque de mémoire.

Nous voulons migrer toute les instances 'm1.tiny' sur un gabarit plus grand 'm1.small'. La seule méthode pour cela, faire un snapshot de l'instance, la détruire, recréer une instance avec les mêmes paramètres (net, name... sauf gabarit).

Cela devrait ressembler à cela :
```
pour chaques instances
	si le gabarit est m1.tiny
		récupération des paramètres de l'instance
		création du snapshot
		destruction de l'instance
		re-création de l'instance
```
