
# Portail d'information sur la plateforme Openstack IMT Atlantique

La documenation générée est disponible sur le site [https://openstack-community.gitlab-pages.imt-atlantique.fr](https://openstack-community.gitlab-pages.imt-atlantique.fr)


## Tests en local

Build de l'image
```bash
docker build -t mkdocs_openstack-community .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-community mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-community mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_openstack-community rm -rf /work/site
```


