# Environnments de TP dédiés, et préconfigurés - Telecom Evolution

## Description

Mise à disposition pour chaque élève d'une infrastrusture dédié et préconfigurée pour effecteur des TP de sécurité.


## Détails techniques

Création de plusieurs "projet" Openstack dédiés, et déploiement des resources (reseau, VM, déploiement d'application) via des template Heat (outil Heat intégré à Opensatck) et/ou Ansible

Pas de connaissance technique Opensatck nécessaire pour utiliser le TP, chaque élève à une adresse IP sur laquelle se connecter.


## Mots clés

infrastructure, TP


