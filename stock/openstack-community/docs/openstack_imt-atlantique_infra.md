# Description de l'infrastructure

Elle est composée :

- d'un "control plane" constitué de 3 noeuds en haute disponibilité (déploiement openstack-ansible)
- d'une base fixe de 6 computes
- d'une part variable de computes complémentaires.

L'infra Openstack s'appuie sur un cluster de stockage partagé CEPH, utilisé pour stocker les images, et fournir des volumes.
Le cluster est constitué de 3 nœuds. Sa capacité est de 90To bruts, uniquement du stockage capacitif, peu performant.
CEPH n'est pas utilisé pour le stockage des disques système des VM, qui restent en local sur les compute.

Le nombre de compute complémentaire varie en fonction de la disponibilité des serveurs, et de la charge de travail à supporter par la plateforme. A ce jour le nombre minimum a été de 4 compute complémentaires (soit 10 en tout), et au maximum de 22 (soit 28 en tout).
Des compute sont ajoutés/supprimés assez régulièrement (de l'ordre de 2 fois par mois en moyenne).

Les compute "enseignement" et "recherche" ne sont pas différenciés.
Parfois des compute sont dédiées sur certaines périodes à des projets de recherche. Mais cette pratique reste exceptionnelle.

Tout les nœuds de compute sont de type DELL PowerEdge, de générations 11, 12, 13 et 14.
Certains en modèle 1U (R6xx) d'autres en modèle 2U (R7xx).
Ils ont tous 2x CPU de famille Intel.

Liste des hosts : 

- les 6 nœuds "Host fixes" sont relativement identiques : 48 cœurs 196Go RAM
- un nœud dédié « recherche » est un peu plus puissant : 64 cœurs, 256Go de RAM
- le reste des configurations varie : 
    - 16 cœurs / 64 Go de RAM
    - 32 cœurs / 96Go de RAM
    - 40 cœurs / 128Go de RAM
    - 48 cœurs / 128Go de RAM

Prévision d'intégration sur 2021 :

- 4 nœuds GPU (Projets R. Fablet hors CPER AIDA), pour l'instant utilisé hors infra : 32 coeurs/ 192Go RAM / 3x NVIDIA Quadro RTX 8000 48 GB
- 8 nœuds "récupérés" suite mise à jour ferme Vsphère
- X nœuds GPU dan le cadre du CPER AIDA


