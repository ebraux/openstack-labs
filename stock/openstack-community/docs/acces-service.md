
## Compte d'accès

La gestion des comptes utilisateurs est indépendante de la gestion des comptes d'IMT Atlantique.

Pour avoir un compte d'accès à Openstack, il faut donc :

- si vous êtes un membre du personnel, faire une demande au niveau du support de la DISI
- si vous êtes élèves ou invité externe, demander à un membre du personnel de faire cette démarche

Le login à utiliser pour vous connecter est le même que le login IMT Atlantique, par contre, le mot de passe est indépendant de votre mot de passe centralisé.

Un mot de passe commun est utilisé par défaut, vous devez le changer rapidement.

> Le mot de passe Openstack est dissocié de votre mot de passe centralisé, car si vous utilisez Openstack en ligne de commande, votre mot de passe apparait en clair dans votre environnement, ce qui constitue un risque de sécurité.

## Accès aux Ressources

La gestion des ressources dans Openstack n'est pas gérée au niveau utilisateur, mais au niveau "projet".

Pour pouvoir utiliser des ressources, il faut donc :

- qu'un projet soit créé,
- que des ressources lui soient allouées,
- que vous soyez autorisé à accéder au projet


## Connexion à Openstack et aux instances créées

Openstack n'est accessible que depuis le réseau interne d'IMT atlantique. Il faut donc soit être dans les locaux d'IMT Atlantique, ou être connecté sur le réseau (VPN, VDI, campus distant; paserelle SSH ...)
