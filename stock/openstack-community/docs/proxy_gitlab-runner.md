# Configuration d'un gitlab Runner docker pour utiliser le proxy IMT Atlantique pour les accées sortants

Un runner gitlab en mode Docker utilise Docker, il faut donc tout d'abord configurer le proxy pour Docker sur la machine : [proxy_docker.md](proxy_docker.md)


Ensuite il faut configurer le proxy au niveau gitlabrunner, via le fichier "/etc/gitlab-runner/config.toml"


- Définir les variables dans l'environnement, pour les accès sortant depuis les containers :
```
environment = ["DOCKER_TLS_CERTDIR=", "http_proxy=http://proxy.enst-bretagne.fr:8080", "HTTP_PROXY=http://proxy.enst-bretagne.fr:8080", "https_proxy=http://proxy.enst-bretagne.fr:8080", "HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080", "no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr", "NO_PROXY=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"]
```

Définir un 'pre_build_script' pour configurer l'environnement pour les containers de type service :
```
pre_build_script = "mkdir -p $HOME/.docker/ && echo \"{ \\\"proxies\\\": { \\\"default\\\": { \\\"httpProxy\\\": \\\"$HTTP_PROXY\\\", \\\"httpsProxy\\\": \\\"$HTTPS_PROXY\\\", \\\"noProxy\\\": \\\"$NO_PROXY\\\" } } }\" > $HOME/.docker/config.json"
```


Exemple de de configuration complète :

```ini
concurrent = 2
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner derriere proxy IMT Atlantique"
  limit = 0
  output_limit = 4096
  url = "https://gitlab.imt-atlantique.fr/"
  token = "xxxxxxxxxxxxxxxxxx"
  executor = "docker"
  environment = ["http_proxy=http://proxy.enst-bretagne.fr:8080", "HTTP_PROXY=http://proxy.enst-bretagne.fr:8080", "https_proxy=http://proxy.enst-bretagne.fr:8080", "HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080", "no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr", "NO_PROXY=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"]
  pre_build_script = "mkdir -p $HOME/.docker/ && echo \"{ \\\"proxies\\\": { \\\"default\\\": { \\\"httpProxy\\\": \\\"$HTTP_PROXY\\\", \\\"httpsProxy\\\": \\\"$HTTPS_PROXY\\\", \\\"noProxy\\\": \\\"$NO_PROXY\\\" } } }\" > $HOME/.docker/config.json"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.azure]
  [runners.docker]
    "privileged" = true
    "tls_verify" = false
    image = "ubuntu"
    memory = "512m"
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```