# Infrastructures dédiées - projets TAF-DS-sens_critique

## Description

Environnement multi VM pour projets dans le cadre d'une TAF.

Mise à disposition d'un environnement pour chaque binam, dans le cadre d'un projet pour la TAF DS-sens Critique.

Les élèves déploient des VMs, et installent leurs outils de traitement distribué.


## Détails techniques 

Création d'un "projet" Openstack dédié, pré-configuré avec un réseau privé.

Les élèves doivent connaitre es bases de l'utilisation d'Openstack.


## Mots clés

Infrastructure, Openstack



