# Configuration de docker pour utiliser le proxy IMT Atlantique pour les accées sortants


"Docker" est composé de plusieurs briques. Pour configurer docker pour fonctionner derrière un proxy, il faut configurer la brique server (docker.d), et la brique client en ligne de commande.

## Configuration de docker.d

Pour configurer le proxy pour le daemon Docker avec systemD :

```bash
# creation du dossier de configuration du service docker
sudo mkdir -p /etc/systemd/system/docker.service.d

# creation du fichier de configuration du proxy
sudo tee /etc/systemd/system/docker.service.d/proxy.conf << 'EOF'
[Service]
Environment='HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080' 'HTTP_PROXY=http://proxy.enst-bretagne.fr:8080' 'NO_PROXY=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr'
EOF

cat /etc/systemd/system/docker.service.d/proxy.conf

# relancer Docker
sudo systemctl daemon-reload
sudo systemctl restart docker
```

## Configuration du client docker

Depuis Docker 17.07, le proxy pour client Docker peut-être configuré de façon globale, en indiquant les informations dans un fichier au format json, dans le homedir de l'utilisateur (~/.docker/config.json). Attention, ce fichier peut contenir d'autre éléments de configuration, comme l'authentification auprès d'une registry.

fichier : ~/.docker/config.json
``` bash
# creation du dossier de configuration du service docker
mkdir ~/.docker

# creation du fichier de configuration du client Docker
tee ~/.docker/config.json << 'EOF'
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://proxy.enst-bretagne.fr:8080",
     "httpsProxy": "http://proxy.enst-bretagne.fr:8080",
     "noProxy": "27.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"
   }
 }
}
EOF

cat ~/.docker/config.json
```

La prise en compte est immédiate.

Avec cette configuration, le proxy sera utilisé pour les phase de build de image, et de run des container.

>Si besoin d'une configuration plus précise, il est possible d'utiliser une configuration plus spécifique :
>
>- pour "docker build" : otion "--build-arg" 
>- pour "docker run" : otion "--env" pour "docker run"
>- dans un Dockerfile : avec ARG pour le build uniquement, et ENV pour le Build + execution du container




