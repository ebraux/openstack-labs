# Configuration du shell pour utiliser le proxy IMT atlantique



Il faut configurer les variables `http_proxy` et `https_proxy` (attention ce sont les seules variables système en minuscule)

Il est parfois également necessaire de définir des urls à contacter sans passer par le proxy avec la variable `no_proxy`.


## Au niveau du shell courant 

```bash
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy="127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"

env | grep proxy

```

## De façon globale pour le système

```bash
sudo tee -a /etc/environment << 'EOF'
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
EOF
```


## Exemple de scripts permettant d'activer/désactiver la configuration à la demande

Donne l'accès au commandes :

- `proxy-on` : pour activer la configuration du proxy
- `proxy-off` : pour déactiver la configuration du proxy
- `proxy-status` : pour afficher l'état de la configuration du proxy

```bash
sudo tee -a /etc/bash.bashrc <<EOF
proxy_on() {
    export http_proxy=http://proxy.enst-bretagne.fr:8080
    export https_proxy=http://proxy.enst-bretagne.fr:8080
    export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
}

proxy_off() {
    unset http_proxy
    unset https_proxy
    unset no_proxy
}

proxy_status() {
    env | grep http_proxy
    env | grep https_proxy
    env | grep no_proxy
}
EOF

```
