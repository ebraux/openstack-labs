
# Accès aux VM en mode Bureau distant (VDI)

## Principe général

L’exécution de la VM n’a plus lieu sur un PC, mais  sur un serveur hébergeé dans le datacenter d'IMT Atlantique.

Vous avez accès des groupes de VM, (pool) qui on été définis pour vous par le responsable du module.

Vous devez lancer (executer) une VM appartenant à ce pool. Cette VM vous est alors attribuée.

## Accès 

L’accès à ce portail se fait à l’adresse suivante : [https://vdi.imt-atlantique.fr](https://vdi.imt-atlantique.fr)

L’ergonomie peut évoluer en cours d’année mais il se présente actuellement ainsi : ![Portail_VDI-e1535613985579](img/Portail_VDI-e1535613985579.png)

> Vous devez vous connecter avec votre identifiant (8 caractères) et mot de passe IMT Atlantique.

## Démarrage des VM

Un fois connecté, vous pouvez voir les pools de VM auxquels vous avez accès (indiqué par le  suffixe « -?? »). Ainsi que vos VM si vous en avez déjà lancé.

La figure ci dessous montre :

* le pool "TP-OutilElec"
* constitué de VM Windows 10 en 64 bits.
* le bouton d’état indique que la VM est Fermée (autrement dit : éteinte)
* la seule action possible c’est `Exécuter`, qui permet de démarrer une VM.

![Portail_VM1-2](img/Portail_VM1-2.png)

Une fois la VM lancée, l'image indiquant ne pool n'est plus disponible, elle est replacée par votre VM.
 
 > !! Attention, le temps de réaction au démarrage de la VM est un peu long et laisse apparaître momentanément icône de VM et icône de Pool. Il faut êtere un peut patient, et ne pas re-cliquer sur le pool.
 
 La figure ci dessous montre que :
 
 * la La VM n° 64 du pool de VM TP-OutilElec vous a été attribué.
 * le bouton d’état indique "En cours" (ce qui est symbolisé par un Cercle vert)
 * les actions disponibles apparaissent dans un menu déroulant avec comme action par défaut Fermer
 * d’autres actions sont possibles dont SPICE Console (afin d’accéder au déport d’affichage)
 
 ![Portail_VM2-2](img/Portail_VM2-2.png)

> RDP Console est également disponible quand il s’agit d’une VM Windows), mais dans tous les cas il faut choisir "SPICE Console" pour accéder à la VM car nous supportons exclusivement le protocole SPICE (qui convient aussi bien à des VM Windows que Linux).

## Connexion à la VM

L'accès à la VM se fait en cliquant sur le bouton "Console SPICE", qui va déclencher le lancement du client SPICE sur votre poste. Il est donc nécesaire d'installer un client "SPICE" sur son poste.

Il est également possible de se connecter en ssh sur les machines Linux.

Sur les VM, les comptes ci dessous sont créés par défaut : 

* le compte « super utilisateur » 
  * login : root 
  * mot de passe : adm
* un compte « simple utilisateur »:
  * login : user
  * mot de passe : usr

## Arrêt d'une VM 

Pour arrêter proprement une VM, il faut simplement arrêter le système d’exploitation virtualisé par les moyens habituels (menu démarrer, bouton d’arrêt…).


## Persistance des VM

Les pool de VM peuvent être configurés pour fournir des VM persistantes ou non.

* Une VM non persistante est détruite lorsqu'elle est fermé, et une nouvelle VM est attribuée lors de la prochaine execution.
* une VM persistante n'est pas détruite lorsquelle est fermé. On la retrouve donc lors de la prochaine execution.

C'est le responsable du pool qui fait ce choix lors de sa création, en général en fonction du type d'enseignement (persistante pour un TP se déroulant sur plusieurs séances, non persistante pour un TP mono séance).

> Cette information n’apparait malheureusement pas distinctement dans l’interface, il faut donc avoir à l’esprit le type de VM utillisée (persistante ou non) pour ne pas risquer de pertes de données. Un test en début de séance peut s'avérer utile.


## Echanges avec la VM 

### Accès à internet depuis la VM

L’accès à internet est direct sans ré-authentification (vous vous authentifiez en accédant pour portail)

### Echanger des fichiers avec la VM

La VM n'a pas d'accès à votre poste local. Mais comme elle a accès à Internet, vous  pouvez utiliser :

* un espace de stockage partagé de fichier, comme [https://cloud.imt-atlantique.fr](https://cloud.imt-atlantique.fr)
* un dépot git, comme [https://gitlab.imt-atlantique.fr](https://gitlab.imt-atlantique.fr)
* ou toute autre ressource sur Internet (un site web, ...)

## Sources / informtions complémentaires :

* [https://vdi.imt-atlantique.fr/ovirt-engine/docs/Introduction_to_the_VM_Portal/](https://vdi.imt-atlantique.fr/ovirt-engine/docs/Introduction_to_the_VM_Portal/)
* [https://intranet.imt-atlantique.fr/assistance-support/informatique/didacticiels/presentation-des-solutions-de-virtualisation-utilisees-en-tp/](https://intranet.imt-atlantique.fr/assistance-support/informatique/didacticiels/presentation-des-solutions-de-virtualisation-utilisees-en-tp/)
