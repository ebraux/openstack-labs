# Accès externes depuis vos instances


## Principes et limitations

Pour des raisons réglementaires, il n'est pas possible de donner un accès sortant vers Internet sans authentification.

Les accès sortant sont par défaut limités au réseau d'IMT Atlantique, ainsi qu'à quelques sites estimés indispensables, avec risques limités.

Certains sites sont accessibles au travers d'un proxy. Il est donc necessaire de configurer les différents outils pour qu'ils puissent l'utiliser.

Certains sites ne sont tout simplement pas accessibles.


**C'est donc un point important à prendre en compte lors de vos déploiment !**

Si vous devez accèder à un site externe, et qu'il ne figure pas dans la liste des sites accessibles, vous devez faire une demande d'accès auprès du support DISI.


## Rappel sur ce qu'est un proxy

Un "proxy" est un serveur intermédiare, entre les machines à l'intérieur de l'entreprise et Internet.

Un proxy permet de:

- filtrer les accès, pour limiter l'accès à certaine ressources
- définir qui fait ou a le droit de faire quoi, sur la base des adresse IP, ou authentification des utilisateurs
- limiter la bande passante, le proxy pouvant faire office de cache.

Pour utiliser un proxy, il faut configurer les outils utilisés,  pour rediriger leur requètes à destination d'internet vers le proxy.

Il est parfois nécessaire de différencier les requetes externes vers internet, des requetes internes, comme des API. Le proxy n'ayant as forcément accès aux ressources internes.



## Informations de configuration pour configurer l'accès sortant

- Pour télécharger des paquets avec Ubuntu : [apt](proxy_apt.md)
- Dans un environnement shell (http/https, git, wget, curl, ...) : [shell](proxy_shell.md)
- Avec Docker : [docker](proxy_docker.md)
- Avec Maven : [maven](proxy_maven.md)
- ...

Nous proposons des images préconfigurées pour l'environnement IMT Atlantique (configuration du proxy, outils préinstallés, ...): [images_imta-xxx](images_imta-xxx.md)

## Liste non exhaustive des dommaines accessibles avec le proxy IMT Atlantique

- .github.com,
- .githubusercontent.com
- .git.io
- .gitlab.com, .gitlab.io
- .docker.com, .docker.io
- .openstack.org, .cirros-cloud.net
- .ubuntu.com
- .launchpad.net
- .core-cloud.net
- .python.org, .pypi.org
- .oracle.com
- .mongodb.org
- .postgresql.org
- .mariadb.org
- .enst-bretagne.fr
- .telecom-bretagne.eu
- .imt-atlantique.fr
- .imta.fr
- .mines-telecom.fr
- .snapcraft.io
- .jenkins.io,
- .jenkins-ci.org
- .pythonhosted.org
- .pypa.io
- .wordpress.org
- .gravityhelp.com
- .gcr.io
- .storage.googleapis.com
- .quay.io
- .cloudfront.net
- .amazonaws.com
- .ibiblio.org
- .continuum.io
- .apache.org
- .wikimedia.org
- .nodejs.org
- .npmjs.org
- .nodesource.com
- .alpinelinux.org
- .mesosphere.io
- .mesosphere.com
- .anaconda.org
- .anaconda.com
- .osuosl.org
- .linkerd.io
- .opendev.org
- .zabbix.com
- .datadoghq.com
- .datadoghq.eu
- .dynatrace.com
- .unyonsys.com
- .grafana.com
- .debian.org
- .hashicorp.com
- .maven.org
- .portainer.io
- .rethinkdb.com
- .cnrs.fr
- .bintray.com
- .jfrog.org
- .gradle.org
- .gradle-dn.com
- .getcomposer.org
- .appscode.com
- .packagist.org
- .bigbluebutton.org
- .rubygems.org
- .serverion.com
- .mattermost.com
- .jboss.org
- .hex.pm
- .duniter.org
- .curl.se
  

