

- [Machine Bastion](./os-admin.yaml)
- [Devstack](./devstack.yaml)
- [Réseau dédié Jupyter + jupyter base notebook](./jupyter-base-env.yaml)
- [Jupyter Lab Spark additionnel](./jupyter-spark-node.yaml)