# Connecter une instance au réseau


## le réseau "Common"

Le réseau "common" : réseau commun a tous les projets, vous pouvez l'utiliser si vous n'avez pas créé de réseau privé.
* avantage : pas besoin de créer un réseau privé dans votre projet
* inconvénient : tout les utilisateurs d'Opensatck peuvent accéder au réseau "common".

## Créer un érseau privée pour le projet

Vous pouvez créer un réseau privé pour votre projet
Seul vous pouvez créer des instaces sur ce réseau, c'est donc plus sécurisé que "common".

# Gestion des accès

## Ip Flottante
ajouter une IP flottante (en 10.29.244.xxx)

## Groupe de sécurité
ajouter une régle de sécurité qui permet la connexion SSH


# Connection SSH à une instance Ubuntu

Pour la connexion SSH.
Par défaut les images Ubuntu sont configurées :
 - pas de possibilité de se connecter en tant que root (en local, ou vie ssh) :  il faut se connecter avec un aute utilisateur et faire un sudo.
 - un utilisateur banalisé, login ubuntu, qui  peut se connecter uniquement avec une KEY_PAIR, et donc uniquement en ssh.

Vous pouvez modifier ce fonctionnement sur une instance Ubuntu en ajoutant des scripts au lancement, par exemple : http://formations.telecom-bretagne.eu/syst/cloud/ressources/cloud-init/ubuntu.cloud-init.
