
# Connexion directe en ssh aux VMs  VDI

Dans certains cas avec Spice, le copier/coller ne fonctionne pas, ou le clavier est mal reconnu.

Il est possible de se connecter directement sur les machines VDI en SSH.

Les machines VDI n'ont pas d'IP publique, il faut donc passer par la passerelle SSH d'IMT Atlanttique.

La passerelle SSH :

- autorise les environnemets graphiques, mais le temps de réaction est très important.
- n'autorise pas le forwarding de port.

Cette solution est donc valable pour se connecter en mode "terminal".



## Activer le serveur SSH sur la machine VDI si besoin 

- se connecter à votre VM VDI, et ouvrir un terminal
- installer opensssh-server
```bash
sudo apt install openssh-server
```
- configurer l'accès par mot de passe
  - fichier de configuration /etc/ssh/sshd_config
  - activer `PasswordAuthentication yes` (dé-commenter la ligne)
- redémarrer openssh-server
```bash
sudo systemctl restart sshd
```
  
## Se connecter à la VM

Récupérer l'Ip de la VM (en 10.35....) :

- se connecter à votre VM VDI, et ouvrir un terminal
```bash
ip a
```

Sur votre poste
- se connecter à la passerelle ssh d'IMT Atlantique, avec votre login/mot de passe IMT Atlantique
```bash
ssh -X -l <VOTRE_LOGIN> ssh.telecom-bretagne.eu
```
- une fois sur la passerelle, vous connecter à votre VM, avec le compte "user"
```bash
ssh -X -l user <IP_VM_VDI>
```

## Précautions d'usage

Cette configuration met en place un accès SSH non sécurisé, puisqu'elle utilise un utilisateur banalisé, et un mot de passe faible.

Il est donc recommandé d eprendre des précautions :

Activer l'accès ssh uniquement au besoin
```bash
sudo systemctl stop sshd
sudo systemctl start sshd
```

Modifier le mot de passe du compte user
```bash
passwd
```

**Si cette configuration doit être conservée, il est fortement recommandé de mettre en place une authentification par clé+agent, et de désactiver l'authentifiation par mot de passe au niveau du serveur SSH de la VM VDI
**