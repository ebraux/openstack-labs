# Demander des ressources

## Comment ?

La demande de ressources doit passer par le support DISI.

Il faut préciser un nom de projet (court de préférence), des dates de début et de fin du projet, et les information necessaire en fonction des besoin, comme le type de projet (voir ci dessou), les ressources nécessaires, ...

--- 

## Pour quel usage ?

- Pour un projet élève
    - un serveur pour installer des outils,
    - ...  
- Pour des enseignements
    - un environnement de TP global
    - des environnement identiques pour chaque binôme
    - ...
- Pour un projet de recherche
    - Un environnement permentant de déployer des ressources de calcul
    - ...

Voir la liste des cas d'usages pour plus de détails.

---

## Type de ressources

Les VM déployées sont uniquement des VM Linux, principalement Ubuntu.

Plusieurs modes sont possibles :

- Sans connaissances d'Openstack 
    - des ressources préconfigurées. Dans ce cas, seul la ou les IPs d'accès aux services sont fournies. L'utilisation se fait comme avec n'importe quelle VM

- avec connaissance d'openstack
    - un projet avec une préconfiguration minimum, comme la configuration réseau de base.. 
    - un projet sans aucune ressources.

Il est également possible de créer des ressource sen mode "labs" : création de projets (initialtisés ou pas) individuels pour chaque élève ou binome.



