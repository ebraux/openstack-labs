# Installation du client VDI sou MACOS

## Mise en garde

La procédure qui suit a été testée et validée en mai 2019 sur un MacBook Air de 2013 (processeur Intel Core i5, 4Go de RAM, SSD) avec macOS installé en version 10.14.4 (Mojave).

> cette procédure s’applique pour une version de macOS 10.12 (Sierra) ou supérieure car c’est la version la plus ancienne supportée par Homebrew.

## Étape 1 : installation de Homebrew

Homebrew est un logiciel de gestion de paquets pour macOS gratuit et open-source. S’il est déjà installé sur votre système, vous pouvez passer directement à l’étape 2.

L’installation nécessite d’ouvrir un terminal et d’y saisir la commande suivante :
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
puis de valider le lancement de l’installation et saisir le mot de passe de son compte macOS lorsque celui-ci est demandé.

Le reste de l’installation est automatisé et dure moins de 5 minutes. Pour vérifier que l’installation s’est correctement effectuée, il suffit de tester la commande `brew`. Des exemples d’usage de la commande doivent alors s’afficher dans le terminal.

Pour plus d’informations sur Homebrew se référer à brew.sh.

## Étape 2 : installation du paquet virt-viewer

Ajout d’un dépot pour Homebrew
```bash
brew tap jeffreywildman/homebrew-virt-manager
```

Installation du paquet
```bash
brew install virt-viewer
```
> L’installation télécharge et installe de nombreux paquets fournis par Homebrew et dont dépend le paquet virt-viewer. Elle dure donc plusieurs minutes (environ 10 minutes sur le MacBook Air ayant permis de valider l’installation).

Lancement du programme 
```bash
/usr/local/bin/remote-viewer
```
La fenêtre suivante doit alors s’afficher : ![remote-viewer](img/remote-viewer.png)

## Étape 3 : intégration avec l’interface graphique de macOS

Pour pouvoir exécuter la commande `remote-viewer` sans passer par la ligne de commande, il faut créer une application spécifique avec `Automator`. 

Le fichier remote-viewer.zip contient cette application, il faut le télécharger et le copier dans les Applications avec le Finder.

* fichier remote-viewer.zip : [https://intranet.imt-atlantique.fr/wp-content/uploads/2019/05/remote-viewer.zip](https://intranet.imt-atlantique.fr/wp-content/uploads/2019/05/remote-viewer.zip)
* procédure de création de "remote-viewer.zip" : [https://rizvir.com/articles/ovirt-mac-console/](https://rizvir.com/articles/ovirt-mac-console/)

L'application n’est donc pas vue comme signée par un développeur connu d’Apple. Il est donc probable que la fenêtre suivante s’affiche lors du lancement de l’application remote-viewer. ![developpeur-non-identifie](img/developpeur-non-identifie.png)

Pour éviter ce message, il faut lancer une première fois l’application remote-viewer dans le Finder en utilisant la touche Ctrl et en choisissant « Ouvrir » dans le menu contextuel puis le bouton « Ouvrir » dans la fenêtre qui s’ouvre (Se référer à la page du support d’Apple pour plus d’informations).

Pour simplifier encore plus l’usage, il faut associer les fichiers ".vv" fournis par le portail VDI avec l’application remote-viewer. Pour obtenir un fichier ".vv", il faut se connecter à [https://vdi.imt-atlantique.fr](https://vdi.atlantique.fr) et cliquer le bouton Spice Console d’une VM démarrée (n’importe laquelle). Dans le Finder, il suffit ensuite de :

* faire un clic droit sur le fichier "console.vv" téléchargé
* choisir l’entrée « Lire les informations » dans le menu contextuel
* aller dans la partie « Ouvrir avec » de la fenêtre d’infos qui vient de s’ouvrir
* cliquer sur « Autre… » dans le menu et choisir l’application remote-viewer
* valider le choix en cliquant sur le bouton « Tout modifier »
* fermer la fenêtre d’infos

L’application remote-viewer est maintenant installée, utilisable entièrement graphiquement et associée avec les fichiers .vv.

## Configuration du clavier

Les VM Windows ou Linux sont configurées pour être utilisées avec un clavier AZERTY « PC » (et non pas « MAC »).

 Les touches (ou combinaisons de touches) pour accéder à certains caractères (comme @ par exemple) sont différentes et doivent donc être utilisées dans leur version « PC ».
 
 Se référer à la page [https://fr.wikipedia.org/wiki/AZERTY#En_France](https://fr.wikipedia.org/wiki/AZERTY#En_France) pour plus d’informations concernant les différences.


