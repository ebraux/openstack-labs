# Evolutions envisagées

## Offre GPU as a Service

Plusieurs serveurs GPU sont mis à la disposition des chercheurs (équipe R. Fablet [https://rfablet.github.io/projects/2019-oceanix](https://rfablet.github.io/projects/2019-oceanix)). Pour l'instant, l'accès est imité en SSH et géré par des autorisations locales sur les machines.

Une intégration à Openstack est prévue sur 2021 pour fournir du GPU as a service, soit en dédiant des GPUs à des instances, soit en utilisant les fonctionnalités de vGpu proposées par Nvidia.

Cette offre de GPU as a service pourrait être proposée via Openstack, ou via un cluster k8s, car les framework utilisés sont diffusés sous forme de containers par Nvidia.

Un critère de choix important sera l'ergonomie de la solution pour les chercheurs


## Extension des Services existants 

Si nous disposons d'une infrastructure Openstack fiable et robuste, nous pourrons étendre les services proposés aux utilisateurs. Les pistes envisagées sont : 

- Extension de la volumétrie proposée pour les volumes (extension de capacité de CEPH)
- Mise à disposition de stockage Objet
- Extension de l'utilisation du cluster CEPH pour un usage par d'autre plateforme
    - Cluster Kubernetes
    - Ferme de bureau distant VDI Red Hat Virtualisation (actuellement stockage RHV sur CEPH pas supporté par Red Hat ...)
- Fourniture d'instance de type "service WEB", pour mise à disposition de la communauté par des chercheurs de contenu ou d'API; en remplacement des serveurs dédiés utilisés actuellement.
- extension des environnement de TP virtualisés : Le principal frein pour cet usage est l'aspect ergonomie, que ce soit pour l’accès aux ressources pour les apprenants, ou pour la gestion pour les enseignants la solution pour les chercheurs

