# Environnement de TP Postgresql - UE-BD-IHM

## Decription

Chaque éléves a accès à un serveur postgresql dédié, et à un ensemble de notebook Jupyter qui lui permettent de réaliser les TP associés au cour.

L'authentification est gérée par le mécanisme d'authentification de compte d'IMT-Atlantique.

Près de 300 élèves en simultané. TP se déroulant sur 4 semaines.

## Détails techniques 

Utilisation de Jupyterhub, et le module d'authentification SSO CAS.

Les notebook s'executent dans des containers Docker individuels, basés sur une l'image "python", personnalisée pour intégrer un serveur postgresql, et les librairies python/sql


## Mots clés

jupyterhub, CAS, SSO, Docker, Python, postgresql, Massif