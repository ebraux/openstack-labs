# Financement de la plateforme Openstack Actuelle

- La formation par apprentissage via le CFAI : 3 serveurs de control-plane, et 3 serveurs de stockage, et 2 serveurs compute
- La DISI : de nombreux serveur reformés, l'infrastructure réseau et datacentre, participation au financement du support.
- La DFVS: un serveur compute, une prestation pour ajout de nouvelles fonctionnalités
- Equipe de recherche DECIDE : 2 serveur compute, de la RAM pour compléter des serveurs existants
- IMT par l'intermédiaire du MOOC "MOOC-Understanding-Queues" : un serveur compute, de la RAM pour compléter des sserveur compute, du disque pour compléter le stockage
- IMT par l'intermédiaire de Telecom Evolution : participation au financement du support, et de prestation de déploiement