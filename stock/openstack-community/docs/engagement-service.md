# Engagement de service et condition d'utilisation


Les ressources déployée sur l'infrastructure Openstack ne sont pas garanties.

Les instances déployées ne sont pas garanties, et ne sont pas sauvegardées. Elles s'executent sur des serveurs physiques non redondés. Si un serveurs physique tombe en panne, elles sont perdues, et ne peuvent pas être restaurées.

Vous devez prendre les mesures necessaires pour assurer la sécurité de vos infrastructures et données :

- assurer la sécurité de votre code, par exemple via un dépot GIT,
- le backup de vos données, par exemple via des exports ports), 
- la possibilité de redéployer vos instances, soit par une documentation complète, ou mieux, par un mécanisme de script, ou encore mieux d'infrastructure as code tel que les tempate Heat, Ansible ou Terraform.

Les ressources déployées dans Openstack ont une durée de vie qui doit être définie. Elles ne peuvent pas être hébergées indéfiniment.Dans le cadre d'un projet, les ressources sont supprimées en fin de projet.

il faut donc assurer  la sécurité du contenu des instances : sauvegarder son code (via un dépot GIT) et de ses données (via des exports mysql, ....)


