# Ensemble d'Infrastructures dédiées par Binomes - TAF Devops

## Description

Mise à disposition pour chaque binome, de resources dédiées dans le cadre de la TAF Devops.

Chaque binome déploie ensuite son infrastructure (réseau, VM, ...).

L'environement est mis à disposition des éléves peandnt la durée de la TAF (3 mois).


## Détails techniques 

Création de plusieurs "projet" Openstack dédiés et vierges.

Chaque éléves crée ensuite les resources dont il a besoin, afin de mettre en place un cluster Kubernetes.

La TAF comprend l'apprentissage de l'utilisation d'Openstack.

## Mots clés

infrastructure, Kubernetes, Heat






