# Mode opératoire utilisé pour l'installation de Docker et docker-compose


# Installation de Docker

### Préparation

Ajouter la clé GPG du dépôt officiel de Docker au système :
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Ajouter le dépot Docker aux sources APT :
```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt-get update
```

Vérifier que le dépot est bien celui de Docker, et non celui d'Ubuntu par défaut :
```bash
apt-cache policy docker-ce
```
> Dans "table de version", les "source" des candidats à l'installation doivent provenir du dépôt Docker pour Ubuntu 20.04 (focal).

### Installation

Installer Docker :
```bash
sudo apt install -y docker-ce
```

Vérifier que Docker a bien démarré en mode daemon, le service doit avoir le status "active (running)" :
```bash
sudo systemctl status docker
```
Verifier la version
```bash
docker --version
  Docker version xx.xx.xx, build xxxxxxxxxx
```

Autoriser l'utilisation de Docker sans "sudo" pour l'utilisateur courant :
```bash
sudo usermod -aG docker ${USER}
```

Lancer un nouveau shell, pour prendre en compte cette modification
```bash
sudo usermod -aG docker ${USER}
su - ${USER}
```
> Vous serez invité à saisir le mot de passe utilisateur pour continuer.

Tester le bon fonctionnement :
```bash
docker stats
```

### Validation

Lancer le container Hello World
```bash
docker run --rm hello-world
  ...
 Hello from Docker!
 This message shows that your installation appears to be working correctly.
  ...
```

Ménage
```bash
docker image rm hello-world
```


## Installation de docker-compose

### Installation

Vérifier la dernière version disponible dans la page des versions : [https://github.com/docker/compose/releases](https://github.com/docker/compose/releases)

Nous utilisons la version 1.27.4.

Téléchager docker-compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Rendre le fichier executable
```language
sudo chmod +x /usr/local/bin/docker-compose
```

Verifier le bon fonctionnement :
```bash
docker-compose --version
  docker-compose version 1.27.4, build 40524192
```

### Validation 

Créer un fichier docker-compose.yml de test avec un service "hello" :
```yaml
version: '3'
services:
  hello:
    image: hello-world
```

Lancer le service "hello"
```bash
docker-compose up
  ...
 Hello from Docker!
 This message shows that your installation appears to be working correctly.
  ...
```

Vérifier le service
```bash
docker-compose ps
      Name       Command   State    Ports
  ---------------------------------------
  user_hello_1   /hello    Exit 0        
```

Supprimer le service
```bash
docker-compose down
  Removing user_hello_1 ... done
  Removing network user_default
```

Ménage
```bash
docker image rm hello-world
```

## Références

* [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-fr)
* [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-fr](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-fr)



