# Utilisation

---
## Accès à la plateforme et utilisation

L'infrastructure Openstack d'IMT Atlantique est utilisée uniquement pour des activités d'enseignement et de recherche.
Elle n'est pas utilisée pour les services administratifs.

Elle est uniquement accessible en interne, sur le réseau d'IMT Atlantique, ou via le VPN.

Il est donc nécessaire de disposer d'un compte informatique actif pour pouvoir y accéder.
Certains partenaires extérieurs (entreprises, autres établissements, ...) ont des comptes informatiques.

En moyenne, nous avons environ 200 instances actives, avec la répartition suivante :

- Environ 60% en enseignement, sur des gabarits type : 1vcpu/1goRam gabarits 2vcpu/2goRam, 4Vcpu/4GoRam
- Environ 40% sur des projets de recherche, sur des gabarits type : 4vcpu/4GoRam, 20vcpu/30goRam, 16vcpu/64GoRam

En pic nous sommes monté à plus de 500 instance actives, et à un peu plus de 1000 coeurs.

L'usage varie également en fonction du calendrier scolaire. Les chercheurs savent qu'ils ont plus de ressources à leur disposition pendant les vacances, et s'organisent en conséquence.

---
## Utilisation en enseignement 

- environnement de type "Iaas" : projets vide + création des instances par les utilisateurs
- UE "Devops" : création des instances et déploiement de Kubernetes par les élèves. 
- Inter-semestre "Inside Openstack" : déploiement d'une architecture Openstack dans dans Openstack
- Formation Continue "Administration d'un Cloud Privé": déploiement d'une architecture Openstack dans dans Openstack
- Mastère Spécialisé "Infrastructure Cloud et Devops" : déploiement d'infrastructure de TP (Ansible, Terraform, Kubernetes, ...)
- Session "Introduction à l'utilisation d'Openstack" : donné assez régulièrement, intégré à plusieurs modules d'enseignement
- UE Architecture BigData : déploiement de cluster pour calcul distribué, spark, ...
- UE Système distribués  : déploiement de cluster pour calcul distribué,  MPI, ...
- déploiement d'instances Jupyterhub : utilisé dans de nombreux enseignements, montée en puissance avec le COVID
- mise à disposition de VM, ou d'infra complètes dans le cadre d'enseignement spécifiques : environ 10 groupes par an.
- mise à disposition de VM, ou d'infra complètes dans le cadre de projet d'élèves : environ 20 par an.


---
## Utilisation en Recherche

Le plus gros utilisateur/financeur en recherche est pour l'instant le projet DECIDE: [https://www.labsticc.fr/en/teams-members/m-570-decide.htm](https://www.labsticc.fr/en/teams-members/m-570-decide.htm). Principalement pour des simulations dans le cadre de thèses, et par quelques chercheurs.

D'autres thésards utilisent des ressources, mais plutôt dans le cadre d’une démarche pour leur travaux personnels, que d'une stratégie d'équipe de recherche

Un recensement des projets en 2020 avait fait apparaître des besoins d'infrastructure dans de nombreuses équipes. Mais ça n'a pas été formalisé.

Les seules demandes ayant abouties sont l'utilisation de GPU par les équipes de R. Fablet, une demande concernant des FPGA par Mathieu Arzel, et des besoin de type Infrastructure as a Service pour SRCD.


## Autre plateformes Openstack

Des plateformes Openstack sont déployée par les enseignants ou les élèves, dans le cadre de formations. En général pour des durées très courtes :

- un ensemble de mini PC (NUQ) est utilisé pour des TPs d'infra, entre autre déploiement d'un Openstack sur 2 noeuds - Déploiement par les élèves - DAPI Nantes
- des "bancs de TP Datacenter" sont utilisé pour des déploiement réseau et serveurs, dont un Openstack sur 2 noeuds - Déploiement par les élèves - SRCD Rennes 
- une infrastructure Openstack est déployée temporairement (3 -5 jours) pour des usages sur la grille de calcul Grid 5000 - Déploiement par les administrateurs Grid5000 - DAPI Nantes
- une infrastructure Openstack est déployée temporairement (3 -5 jours) dans l'infrastructure Openstack DISI (Openstack dans Openstack) - Déploiement par les élèves -  LUSSI Brest
