# Instalation du client Openstack

## Méthode "python pip".

La version à installer correspond à "Rocky".

Installation :
```bash
sudo apt install python3-pip
sudo pip3 install python-openstackclient==3.16.*
```

Vérification :
```bash
openstack --help
```

Références :

* [https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html)
