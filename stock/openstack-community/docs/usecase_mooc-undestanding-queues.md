# MOOC Understanding Queues

![MOOC-Understanding-Queues](img/MOOC-Understanding-Queues.jpg)


## Decription

Intégration de TP dans des notebook Jupyter, dans un MOOC. 

MOOC La théorie des files d’attente, hébergé sur la plateforme EDX.

Les participants ont accés à un notebook Jupyter, qui leur permet de réaliser les TP associés aux cours.
L'accès est transparent, sans ré-authentification.

Plus de 800 inscrits.

## Détails techniques 

Utilisation de Jupyterhub, et le mécanisme d'authentification LTI.

Les notebook s'executent dans des containers Docker individuels, basés sur une l'image "sciPy" personnalisée

## Mots clés

jupyterhub, LTI, Docker, Python, Massif
