# Détail des Projets et environnements d etype Labs 

## Informations

Les données ci dessous commencent fin 2018.

---

## Projets sans date de fin

| Date    | Type Projets                  | Nb users | Quota    | Entité |
| ------- | --------------------------- | ----- | ------------- | ------ |
| -       | Equipe de recherche DECIDE                          | 26 | instances: '50', cores: '500', ram: '1024000'  | RECH |
| 2019    | Equipe de recherche DECIDE - ressources dédiées     |  4 | instances: '5',  cores: '64',  ram: '64000'    | RECH |
| 2019    | Equipe de recherche DECIDE - stage                  |  3 | instances: '10', cores: '30',  ram: '51200'    | RECH |
| -       | Enseignement IDLP                                   |  4 | huge                                           | DFVS |
| -       | Infra Jupyterhub (Charge déplacée sur cloud Orange) |  1 | instances: '10', cores: '120',  ram: '1024000' | DISI |
| 2019    | Infra Gitlab (gitlab.imt-atlantique.fr)             |  1 | huge                                           | DISI |
| 2020/03 | Infra generique projets beta.imt-atlantique.fr      |  1 | medium                                         | DISI |
| 2021/04 | LUSSI application EQI - P.Tanguy                    |  1 | medium                                         | RECH |
| 2021/05 | netlogo_echo, Stage plus recherche - R.Waldeck      |  1 | medium                                         | RECH |

---

## Projets personnels, pour tests/évaluation

| Utilisateur | Quota  |
| ----------- | ------ |
| ablanc      | huge   |
| alebre08    | medium |
| bouya       | small  |
| c18pango    | large  |
| ccouturi    | large  |
| ebraux      | large  | 
| j16allem    | small  |
| janin       | small  |
| jpasto09    | medium |
| lenarzul    | huge   |
| pmaille     | medium |
| r17ngora    | medium |
| rcherr12    | medium |
| sfourrie    | medium |

---

## Projets limités dans le temps

| Date    | Durée   | Type Projets  | Détails          | Nb users | Responsable(s) | Quota | Entité |
| ------  | ------- | ------ | ----------------------- | ------ |------------- | ------ | ------ |
| 2019/01 |  6 mois | Projet élèves | MS-CS Docker security CI         |  5 | E. Braux                 | Medium | DFVS   |
| 2019/01 |  6 mois | Projet élèves | MS-CS container security         |  5 | E. Braux                 | medium | DFVS   |
| 2019/01 |  4 mois | Projet élèves | S5 A3 K8s                        |  4 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/01 |  4 mois | Projet élèves | S5 A3 K8s-ansible                |  4 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/01 |  4 mois | Projet élèves | MS-TWCS  microservices-python    |  2 | P. Tanguy / E. Braux     | medium | DFVS   |
| 2019/01 |  4 mois | Projet élèves | MS-TWCS  portaillabs             |  2 | P. Tanguy / E. Braux     | medium | DFVS   |
| 2019/01 |  4 mois | Projet élèves | MS-TWCS  base-connaissances      |  2 | P. Tanguy / E. Braux     | medium | DFVS   |
| 2019/01 |  4 mois | Projet élèves | MS-TWCS  docker-wp               |  2 | P. Tanguy / E. Braux     | medium | DFVS   |
| 2019/01 |  6 mois | Projet élèves | SLR Base connaissance            |  2 | E. Braux / E. Braux      | medium | DFVS   |
| 2019/01 |  4 mois | Projet élèves | S5 A3 easyobs                    |  4 | M. Segarra / A. Beugnard | medium | DFVS   |
| 2019/03 |  4 mois | Stage         | Portail Labs                     |  2 | E. Braux                 | medium | DISI   |
| 2019/03 |  6 mois | Projet élèves | S4  A3 foodalgues                |  5 | L. Lecornu               | small  | DFVS   |
| 2019/03 |  -      | Projet        | App TAF analytics (S5+stage+...) |  3 | P. Picouet               | medium | DFVS   |
| 2019/03 |  -      | Projet        | POC JupyterHub - Spark           |  2 | E. Braux + externe       | huge   | DISI   |
| 2019/05 |  2 mois | Stage         | Php - APIs                       |  2 | E. Braux                 | medium | DISI   |
| 2019/06 |  6 mois | Projet élèves | ST2 Dev                          |  3 | A. Blanc / JP. Lenarzul  | small  | DFVS   |
| 2019/06 |  3 mois | Stage         | Env Recherche - Big data         |  2 | Y. Kermarrec / E. Braux  | huge   | LUSSI  |
| 2019/06 | 18 mois | Projet        | POC infra Tomcat - Contrats      |  2 | E. Braux / externe       | medium | DISI   |
| 2019/06 |  4 mois | Stage         | Monitoring Prometheus/Grafana    |  2 | E. Braux                 | medium | DISI   |
| 2019/06 |  4 mois | Projet élèves | DNN - ADS                        |  3 | Y. Kermarrec             | Huge   | LUSSI  |
| 2019/09 | 12 mois | Projet        | POC Dockerisation site www       |  2 | E. Braux                 | medium | DISI   |
| 2019/09 |  4 mois | Projet élèves | S5 A3 K8s The Hard Way           |  4 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/09 |  4 mois | Projet élèves | S5 A3 K8s KubeSpray              |  4 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/09 |  4 mois | Projet élèves | S5 A3 K8s Networking             |  2 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/10 |  2 mois | Projet élèves | S5 A3 K8s service-meshes         |  2 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/10 |  2 mois | Projet élèves | S5 A3 K8s service-meshes         |  2 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2019/10 |  2 mois | Projet        | POC veille - RKE                 |  2 | E. Braux                 | medium | DISI   |
| 2019/03 |  6 mois | Projet élèves | archi big data bd #1             |  4 | L. Lecornu               | large  | DFVS   |
| 2019/03 |  6 mois | Projet élèves | archi big data bd #2             |  4 | L. Lecornu               | large  | DFVS   |
| 2019/03 |  6 mois | Projet élèves | archi big data bd #3             |  4 | L. Lecornu               | large  | DFVS   |
| 2019/11 |  6 mois | Projet        | POC overleaf                     |  2 | E. Braux                 | medium | DISI   |
| 2019/11 |  6 mois | Projet élèves | UE ProjetDaSci Nosql             |  4 | L. Brisson / S. Bigaret  | huge   | DFVS   |
| 2019/01 |  4 mois | Projet élèves | S5 A3 dslabs                     |  4 | M. Segarra / A. Beugnard | medium | DFVS   |
| 2019/11 |  6 mois | Projet élèves | TAF DS sens critique             |  4 | R. Billot                | large  | DFVS   |
| 2020/01 |  6 mois | Projet élèves | MS-CS Web Security WAF           |  5 | E. Braux                 | medium | DFVS   |
| 2020/02 |  3 mois | Stage         | ux design                        |  3 | E. Braux                 | medium | DISI   |
| 2020/02 |  3 mois | Stage         | docker webenv                    |  3 | E. Braux                 | medium | DISI   |
| 2020/02 |  6 mois | Projet élèves | MS-CS K8S                        |  4 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2020/02 |  4 mois | Projet élèves | S5 A3 epsbox                     |  3 | M. Segarra               | medium | DFVS   |
| 2020/03 |    -    | Projet        | Collaboration  esatic            |  3 | Y. Kermarrec             | large  | DFVS   |
| 2020/05 |  3 mois | Stage         | app MS                           |  2 | E. Braux                 | medium | DISI   |
| 2020/05 |  4 mois | Projet élèves | S5 A3 epsbox                     |  3 | M. Segarra               | medium | DFVS   |
| 2020/10 |  -      | Enseignement  | S5 TAF ILSD                      |  2 | S. Ruanor                | large  | DFVS   |
| 2020/11 |  4 mois | Projet élèves | S5 serverless #1                 |  5 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2020/11 |  4 mois | Projet élèves | S5 serverless #2                 |  5 | A. Blanc / JP. Lenarzul  | large  | DFVS   |
| 2021/02 |    -    | Projet        | POC infra Contrats Swarm + CI    |  3 | E. Braux                 | large  | DISI   |
| 2021/04 |  6 mois | Projet élèves | MSICD y20raoum_perso             |  1 | Y. Raoumbe               | medium | DFVS   |
| 2021/04 |  6 mois | Projet élèves | MSICD i17coul2_perso             |  1 | I. Coulibaly             | medium | DFVS   |

---

## Utilisation de type "labs" 

En mode labs, un ensemble de projets idnetique sont créés et affectés à des utilsiateurs ou binomes.


| Date    | Durée   | NB Projets | Type Projets      | Activité         | Responsable(s)     | Entité  |
| ------- | ------- | ---- | ----------------------- | ---------------- | ------------------ | ------- |
| 2018/09 | 2 jours | 25 | projets avec réseau type medium     | TP Intro Openstack        | Y. Kermarrec LUSSI + E. Braux (f2b101)          | DFVS    |
| 2018/11 | 2 jours | 25 | Openstack dans Openstack - devstack | TP keystone               | Y. Kermarrec LUSSI (f2b101)                     | DFVS    |
| 2019/01 | 3 mois  | 13 | projets bruts, type "small"         | -                         | JM. Bonnin SRCD (f2r113-19)                     | DFVS    |
| 2019/01 | 2 jours |  8 | projets avec réseau type medium     | TP Intro Openstack        | E. Braux  (intersemestre)                       | DFVS    |
| 2019/01 | 3 jours |  8 | projets avec réseau type huge       | TP Installation Openstack | E. Braux  (intersemestre 272)                   | DFVS    |
| 2019/05 | 2 jours | 12 | projets avec réseau type medium     | TP Intro Openstack        | Y. Kermarrec + E. Braux  TEV (cescyber)         |  TEV    |
| 2019/05 | 2 jours | 12 | Openstack dans Openstack - devstack | TP keystone               | Y. Kermarrec + E. Braux  TEV (cescyber)         |  TEV    |
| 2019/05 | 1 jour  | 12 | projets bruts, type "medium"        | TP RabbitMq               | Y. Kermarrec + E. Braux  TEV (cescyber)         |  TEV    |
| 2019/09 | 3 mois  | 16 | projets bruts, type "medium"        | TP DevOps                 | A. Blanc + JP. Lenarzul SRCD                    | DFVS    |
| 2019/09 | 2 jours |  6 | projets avec réseau type medium     | TP Intro Openstack        | Yvon Kermarrec + Emmanuel Braux  TEV (cescyber) |  TEV    |
| 2019/09 | 2 jours |  6 | Openstack dans Openstack - devstack | TP keystone               | Yvon Kermarrec + Emmanuel Braux  TEV (cescyber) |  TEV    |
| 2019/05 | 2 jours | 17 | projets avec réseau type medium     | TP Intro Openstack        | Yvon Kermarrec + Emmanuel Braux  TEV (Ericsson) |  TEV    |
| 2019/11 | 2 jours | 15 | projets avec réseau type medium     | TP Intro Openstack        | Yvon Kermarrec + Emmanuel Braux  (UE Sys Dist)  | DFVS    |
| 2019/11 | 2 jours |  8 | projets avec réseau type medium     | TP Intro Openstack        | Emmanuel Braux (transfert  comp DISI)           | DISI    |
| 2019/12 | 2 jours | 16 | projets avec réseau type medium     | TP Intro Openstack        | Yvon Kermarrec + Emmanuel Braux (UE ass)        | DFVS    |
| 2019/12 | 2 jours | 16 | Openstack dans Openstack - devstack | TP keystone               | Yvon Kermarrec + Emmanuel Braux (UE ass)        | DFVS    |
| 2020/01 | 2 jours | 16 | projets avec réseau type medium     | TP Intro Openstack        | Emmanuel Braux  (intersemestre)                 | DFVS    |
| 2020/01 | 3 jours | 16 | projets avec réseau type huge       | TP Installation Openstack | Emmanuel Braux  (intersemestre 272)             | DFVS    |
| 2020/02 | 3 mois  |  9 | projets bruts, type "extra-large"   | TP DevOps                 | A. Blanc + JP. Lenarzul SRCD                    | DFVS    |
| 2020/07 | 1 mois  |  4 | projets bruts, type "extra-large"   | TP DevOps - rattrapage    | A. Blanc + JP. Lenarzul SRCD                    | DFVS    |
| 2020/09 | 3 mois  | 15 | projets bruts, type "medium"        | TP DevOps                 | A. Blanc + JP. Lenarzul SRCD                    | DFVS    |
| 2020/12 | 2 jours |  8 | projets avec réseau type medium     | TP Intro Openstack        | Yvon Kermarrec + Emmanuel Braux  TEV (cescyber) |  TEV    |
| 2020/12 | 2 jours |  8 | Openstack dans Openstack - devstack | TP keystone               | Yvon Kermarrec + Emmanuel Braux  TEV (cescyber) |  TEV    |
| 2021/01 | 2 jours | 12 | projets avec réseau type medium     | TP Intro Openstack        | Emmanuel Braux  (intersemestre)                 | DFVS    |
| 2021/01 | 3 jours | 12 | projets avec réseau type huge       | TP Installation Openstack | Emmanuel Braux  (intersemestre 272)             | DFVS    |
| 2021/02 | 3 mois  |  8 | projets bruts, type "extra-large"   | TP DevOps                 | A. Blanc + JP. Lenarzul SRCD                    | DFVS    |
| 2021/03 | 1 mois  |  7 | projets bruts type medium           | TP Intro Terraform        | Emmanuel Braux + externe (MS-ICD)               | DFVS    |
| 2021/03 | 1 mois  |  7 | projets avec réseau type huge       | Echanges/REX Openstack    | Emmanuel Braux - LOPS                           | DISI    |
| 2021/03 | 1 jour  | 17 | projets avec réseau type medium     | TP Intro Openstack        | Emmanuel Braux  (UE archi BigData)              | DFVS    |
| 2021/03 | 1 mois  |  7 | projets bruts type medium           | TP ELK                    | Emmanuel Braux + externe (MS-ICD)               | DFVS    |
| 2021/03 | 1 mois  |  7 | projets bruts type medium           | TP Monitoring             | Emmanuel Braux + externe (MS-ICD)               | DFVS    |

