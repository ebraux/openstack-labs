# Capacité des VM 

## Gabarits accessibles à tous, sans conditions

| Nom              |  RAM  | Disk | VCPUs |
| ---------------- | ----- | ---- | ----- |
| cirros           | 256Mo |   -  |     1 |
| m1.tiny          | 512Mo |  1Go |     1 |
| m1.small         |   1Go |  3Go |     1 |
| m1.medium        |   2Go |  5Go |     2 |
| m1.large         |   4Go |  5Go |     4 |
| m1.xlarge        |   8Go |  5Go |     8 |
| m2.large         |   4Go | 10Go |     2 |
| s10.small        |   2Go | 10Go |     2 |
| s10.medium       |   4Go | 10Go |     4 |
| s10.large        |   8Go | 10Go |     8 |
| s20.medium       |   4Go | 20Go |     4 |
| s20.large        |   8Go | 20Go |     8 |
| s20.xlarge       |  16Go | 20Go |     8 |


## Gabarits spécifiques "recherche" sous conditions d'accès et de disponibilité

| Nom              |  RAM | Disk | VCPUs |
| ---------------- | ---- | ---- | ----- |
| recherche.tiny   |  1Go |  5Go |     5 |
| recherche.tiny2  |  8Go | 20Go |     5 |
| recherche.small  | 10Go | 10Go |    10 |
| recherche.small2 | 16Go | 20Go |    10 |
| recherche.large  | 24GO | 10Go |    10 |
| recherche.huge   | 32Go | 10Go |    20 |
