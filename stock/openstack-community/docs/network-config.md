# Création de réseau / sous réseau


## Configurer la résolution DNS

Si vous souhaitez créer un sous réseau, et si vous voulez que les machines connectées à ce sous réseau puissent accèder à des ressources externes, vous devez configurer la résolution de nom, pour utiliser les serveurs DNS d'IMT Atlantique.

Dans la configuration de votre sous réseau, onglet "Subnet Details", valeur "DNS Name Servers" ajouter les serveurs :

```
192.44.75.10
192.108.115.2
```
