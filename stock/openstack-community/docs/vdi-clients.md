# Installation de sclients pour les accès VDI

## Installation native du client virt-viewer

Installer une procédure spécifique à chaque système d’exploitation. C'est la solution préférentielle.

### Windows 

Téléchargement et installation des fichiers msi disponibles sur la page "Ressources de clients de console" du portail VDI.

* [https://vdi.imt-atlantique.fr/ovirt-engine/rhv/client-resources](https://vdi.imt-atlantique.fr/ovirt-engine/rhv/client-resources)

### Linux

Installation du paquet virt-viewer fourni par la distribution installée

Par exemple pour Ubuntu:
```bash
$ sudo apt install virt-viewer
```

### MacOS :

Il n’y a malheureusement pas de paquet dmg tout prêt pour l'installation du client virt-viewer sous macOS. 

La mise en place de virt-viewer doit êtr faite en ligne de commande. Mais elle reste toutefois accessible à tout utilisateur de macOS.

Voir la procédure détaillée dans la page [Client virt-viewer pour MAOS](vdi-client-macos.md).


## Alternative : utilisation d'une VM dédiée

Il est toujours possible de créer une VM avec une configuration minimum : un navigateur et virtviewer.

Dans ce cas, on établi une connexion à un système distant, au travers d'une machine virtuelle. Ce qui augment fortement la probabilité de disfonctionnements (de clavier, de comportement des interfaces, ... )

C'est donc une solution possible, mais avec des llimitations. Qu'il faut tester avec précaution.

## Pour plus d’informations :

* site web Spice : [https://www.spice-space.org/](https://www.spice-space.org/)
* site de téléchargement de virt-viewer : [https://virt-manager.org/download](https://virt-manager.org/download)