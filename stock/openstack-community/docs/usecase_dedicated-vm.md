# Environnement WEB dédié - Projet S5 EpsBox


## Description

Mise à disposition d'une machine virtuelle dans le cadre d'un projet S5, pour la mise en place d'uun site web.

Les élèves installent leur environnement sur la VM.

Le site créé est accessible sur le réseau IMT-Atlantique.


## Détails techniques 

Création d'un "projet" openstack dédié, pré-confuguré avec un réseau privé, et une instance Ubuntu.

Paramétrage à la demande de règle de sécurité d'accès reseau.


## Mots clés

VM dédiée, web

