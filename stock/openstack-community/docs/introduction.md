# Cloud Privé Openstack

IMT Atlantique propose un Cloud privé de type Infrastructure as a service (Iaas).

En quelques mots, cette solution vous permet de lancer des ressources sur les serveurs d'IMT Atlantique, plutôt que sur votre machine.

Vous partez d'un espace vide : le projet. Puis vous créez des ressources pour construire une infrastructure.

La ressource, la plus courante est une Machine Virtuelle (qu'on appelle une instance, ou server).

Vous pouvez créer des infrastructures avec plusieurs instances, les relier en réseau, ...

En quelques clics, vous pouvez déployer une VM, ou un cluster de VM, lancer un traitement, récupérer le résultat, et supprimer les ressources dont vous n'avez plus besoin.

Et comme vous pouvez faire la même chose avec des scripts (ligne de commande Bash, python, ...), ou des outils d'automatisation (Heat, Ansible, Terraform, ...), vous pouvez créer des environnements reproductibles, déployables en quelques minutes.


Le Cloud privé IMT Atlantique a quelques limites: 

- en termes d'accès au service : [plus de détails](acces-service.md)
- en termes d'environnement graphique. Dans ce cas, il est préférable d'utiliser la solution de bureau distant VDI.
- en termes d'accès sortant vers internet, qui sont limités : [plus de détails](acces-sortants.md)
- en termes de garanties du service : [plus de détails](engagement-service.md) 

Mais il apporte une grande souplesse, et peut vous permettre de mettre en place des environnements d'enseignement, de projet, ou de lancer des traitements de façon simple et efficace (voir les cas d'usages).

Vous pouvez de façon très simple contribuer à ce cloud privé, et intégrer vos propres serveurs achetés sur des budgets de département ou de projet de recherche au cloud privé IMT Atlantique. Vous en restez propriétaire, et pouvez les récupérer si vous le souhaitez : [plus de détails](financement.md)

Ce cloud privé est basé sur la solution Openstack [https://www.openstack.org/](https://www.openstack.org/). Openstack est déployé dans un grand nombre d'établissements d'enseignement recherche, de fournisseurs de services Cloud, ... 

C'est notamment la solution déployée par le GIS France Grilles pour ses infrastructure Cloud : [http://www.france-grilles.fr/catalogue-de-services/fg-cloud](http://www.france-grilles.fr/catalogue-de-services/fg-cloud/).






