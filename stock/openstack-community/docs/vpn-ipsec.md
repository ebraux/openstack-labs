# VPN IPSEC

> Le VPN IPSEc n'est pas prévu pour une utilisation "massive". Il peut donc être saturé ou indisponible, et ne doit pas être utilisé comme solution principale de connexion au réseau IMT Atlantique..

## Introduction

Un VPN (Virtual Private Network) permet à un réseau distant d'être inclu au réseau local.

Concrètement, cela signifie que nous allons vous indiquer comment configurer votre PC de telle sorte qu'il soit vu comme étant dans les locaux de Telecom Bretagne (mais uniquement pour les flux à destination de l'école).

Vous trouverez ci dessous les informations pour Windows 7 et linux Ubuntu

## Prérequis

* Une machine sous windows XP,7, 10 ou linux
* Un compte informatique de permanent, élève, docorant ou stagiaire.
* Une connexion à Internet laissant passer les flux liés au protocole IPsec

---

## Récapitulatif pour les impatients

| Linux | Windows (shrew) | Valeur |
| ------| ----------------| -------------|
| version IKE | - | IKEv1 |
| Groupe DH | - | DH 2 |
| IP passerelle | IP | Brest : 192.108.116.161, Rennes  192.44.77.97 |
| Nom du groupe | key id string dans "local identity" | vpnclient
| mot de passe du groupe | pre shared key dans "credentials" | testwin
| Authentification | PSK + XAUTH | faible |

---

## Pour Windows

Téléchargez et installez  le client vpn shrew version 2.2.2-release [https://www.shrew.net/download/vpn](https://www.shrew.net/download/vpn) et installez le en "édition standard".

Configuration de shrew:

* Onglet général :
    * IP de la passerelle (Brest : 192.108.116.161)
* Onglet authentification :
  * Méthode : " Mutual PSK + XAuth"
  * Local identity
    * Identification type : "Key identifier"
    * Key ID string : "vpnclient"
  * Onglet Credentials
    * Pre Shared Key : "testwin"

---

## Pour Linux Ubuntu

Installez les paquetages vpnc complémentaires au Network Manager.

```bash
sudo apt install vpnc
sudo apt install network-manager-vpnc
# Et suivant votre environement :
sudo apt install network-manager-vpnc-gnome
```

Configurez un VPN via le network manager :

* Onglet VPN d'un profile VPN
    * Passerelle : 192.108.116.161
    * nom d'utilisateur : votre login
    * nom du groupe: vpnclient
    * mot de passe du groupe : testwin

* Options avancées
    * Distributeur : Cisco
    * Méthode de chiffrement : Faible
    * Traversée du NAT : NAT-T
    * Groupe DH IKE : Groupe DH 2 
    * perfect forward secrecy : serveur
* Onglet IPv4
    * Method : Automatic (VPN)
