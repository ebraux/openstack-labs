# Utilisation des images IMTA-xxx

Nous proposons des images préconfigurées pour l'environnement IMT Atlantique (configuration du proxy, outils préinstallés, ...)


Elles elle sont préfixées par "imta" :

- imta-ubuntu-base : image de base
- imta-ubuntu-docker : image de base + Docker et docker-compose
- imta-ubuntu-osmanager : image de base + client Openstack


Détail des pré-installations effectuées:

- Configuration communes à toutes les images "imta-xxx" :
    - la gestion du proxy pour installer des paquets : détails dans la documentation [apt](proxy_apt.md)
    - la gestion du proxy dans les sessions shell : utilisation  des commandes "proxy_on", "proxy_off" et "proxy_status".  détails dans la documentation [shell](proxy_shell.md)
- Configuration spécifique image "Imta-ubuntu-docker"
    - installation de Docker et docke-compose : détails dans la documentation  [docker](images_imta-xxx_docker.md)
    - configuration du proxy pour Docker : détails dans la documentation  [proxy docker](proxy_docker.md)
- Configuration spécifique image "Imta-osmanager" 
    - Installation du client Openstack : [Openstack CLI](install-openstack-cli.md)

