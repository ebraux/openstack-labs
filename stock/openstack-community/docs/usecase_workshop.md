#  WORKShop TMA PhD school


## Decription

WORKSHOP autour de l'analyse de données avec Spark.

Chaque participant a accès à un notebook Jupyter, en utilisant un identifiant de son choix.

Infrastructure ephémère (quelques heures), pour 60 étudiants + 10 organisateurs.

## Détails techniques 

Utilisation de Jupyterhub, et le mécanisme Dummy:un mot de passe unique, auto-enregistrement des utilsateurs.

Les notebook s'executent dans des containers Docker individuels, basés sur une l'image "pySpark" personnalisée.

## Mots clés

jupyterhub, Docker, Spark