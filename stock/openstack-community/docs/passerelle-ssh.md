# Utilisation de la passerelle SSH


Vous pouvez utiliser notre passerelle SSH 'ssh.telecom-bretagne.eu'.

Sous Linux, un client ssh est disonible en natif.

Sous windows, vous devez installer un client spécifique. Nous recommandons MobaXterm [https://mobaxterm.mobatek.net/](https://mobaxterm.mobatek.net/), mais il en existe d'autres comme putty [https://www.putty.org/](https://www.putty.org/), ...


## 1ère étape : se connecter à la passerelle 

```bash
ssh -X ssh.telecom-bretagne.eu
```

- si plus de  5 échecs de login/mot de passe, la connexion est bloquée.
- vous ne pouvez pas lancer plus de 3 sessions simultanément
- l'option "-X" permet d'utiliser un environnement graphique. elle est optionelle.
- La redirection de port (tunnel ssh) est désactivée par mesure de sécurité

> Depuis le serveur de paserelle vous pouvez acceder à vos fichier présents dans votre homedir ou votre espace sanssauvegarde en utilisant, sur le client, la commande la commande 'scp' ou un logiciel dédié (par exemple Filezilla sous Windows).


##  2ème étape vous connecter à vos instances

Depuis le serveur de paserelle vous pouvez alors vous connecter en SSH à une instance que vous aurez déployée dans Openstack.


## Optionel : rebondir sur un des serveurs de bureau partagé 

Depuis le serveur de paserelle vous pouvez également vous connecter en SSH à un serveur en interne. Ce qui vous permet par exemple :
- d'utiliser un environnement graphique
- de conserver vos données,
- d'installer vos propres outils.

Vous pouvez vous connecter :

- aux serveurs CAMPUX  :  vnc.svc.enst-bretagne.fr
```bash
ssh -X vnc.svc.enst-bretagne.fr
```
- à des pc CAMPUX de salles de TP (exemples : pc-df-302.priv.enst-bretagne.fr ou pc-df-195.priv.enst-bretagne.fr). Attention, ce sont des PC, pas des serveurs: ils peuvent être éteints ou être rebootés à tout instant.
- eventuellement toute machine dans le réseau IMT Atlantique qui accepte les connexions SSH (serveurs linux dans les départements, 
pc dans SALSA, ...


## Exemple de session complète (avec lancement d'un outil graphique) :

```bash
> ssh -X -l MonLogin ssh.telecom-bretagne.eu

######################################################################
            Serveur SSH de Telecom Bretagne
######################################################################
. Forward X-Window autorise (ssh -X ...)
. Forward de port interdit
. 5 echecs successifs de connexion pour un login = connexion interdite
  Veuillez alors nous contacter via :
              http://support.telecom-bretagne.eu/
####################################################################
MonLogin@ssh.telecom-bretagne.eu's password: ZZZZZZZ
-bash-3.2$ 

# ---

-bash-3.2$ ssh -X vnc.svc.enst-bretagne.fr
MonLogin@vnc.svc.enst-bretagne.fr's password: ZZZZZZZ

Welcome to Ubuntu 11.04 (GNU/Linux 2.6.38-11-generic-pae i686)

 * Documentation:  https://help.ubuntu.com/
MonLogin@srv-disi-vnc-05:~$ 
MonLogin@srv-disi-vnc-05:~$ SETUP MATLAB2011a
MonLogin@srv-disi-vnc-05:~$ matlab
```