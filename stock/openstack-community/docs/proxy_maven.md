# Configuration de Maven pour utiliser les proxy IMT atlantique


fichier : ~/.m2/settings.xml
``` bash
# creation du dossier de configuration du service docker
mkdir ~/.docker

# creation du fichier de configuration du client Docker
tee ~/.m2/settings.xml << 'EOF'
<proxy>
        <id>IMT-atlantique proxy</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>proxy.enst-bretagne.fr</host>
        <port>8080</port>
        <nonProxyHosts>27.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr</nonProxyHosts>
</proxy>
EOF
```

---

Reférences:

- [https://nanxiao.me/en/set-proxy-for-maven/](https://nanxiao.me/en/set-proxy-for-maven/)



