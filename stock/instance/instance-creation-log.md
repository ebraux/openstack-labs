
``` bash
==> nova/nova-compute.log <==
2024-06-19 14:35:09.976 14361 INFO nova.compute.claims [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Claim successful on node worker1
2024-06-19 14:35:12.956 14361 INFO nova.virt.libvirt.driver [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Ignoring supplied device name: /dev/vda. Libvirt can't honour user-supplied dev names

2024-06-19 14:35:13.066 14361 INFO nova.virt.libvirt.driver [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Creating image(s)

2024-06-19 14:35:33.841 14361 INFO oslo.privsep.daemon [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] Running privsep helper: ['sudo', 'nova-rootwrap', '/etc/nova/rootwrap.conf', 'privsep-helper', '--config-file', '/etc/nova/nova.conf', '--config-file', '/etc/nova/nova-compute.conf', '--privsep_context', 'vif_plug_ovs.privsep.vif_plug', '--privsep_sock_path', '/tmp/tmpu_utb1tc/privsep.sock']
2024-06-19 14:35:34.417 14361 INFO oslo.privsep.daemon [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] Spawned new privsep daemon via rootwrap
2024-06-19 14:35:34.314 17830 INFO oslo.privsep.daemon [-] privsep daemon starting
2024-06-19 14:35:34.318 17830 INFO oslo.privsep.daemon [-] privsep process running with uid/gid: 0/0
2024-06-19 14:35:34.320 17830 INFO oslo.privsep.daemon [-] privsep process running with capabilities (eff/prm/inh): CAP_DAC_OVERRIDE|CAP_NET_ADMIN/CAP_DAC_OVERRIDE|CAP_NET_ADMIN/none
2024-06-19 14:35:34.320 17830 INFO oslo.privsep.daemon [-] privsep daemon running as pid 17830

==> openvswitch/ovs-vswitchd.log <==
2024-06-19T12:35:34.626Z|00038|bridge|WARN|could not open network device tap984ca93f-90 (No such device)

==> nova/nova-compute.log <==
2024-06-19 14:35:34.628 14361 INFO os_vif [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] Successfully plugged vif VIFOpenVSwitch(active=False,address=fa:16:3e:08:c5:35,bridge_name='br-int',has_traffic_filtering=True,id=984ca93f-90f4-40b0-92a3-9a83e952aec4,network=Network(2bdfd0bf-4d7f-4842-be6a-388ec15b9f96),plugin='ovs',port_profile=VIFPortProfileOpenVSwitch,preserve_on_delete=False,vif_name='tap984ca93f-90')

==> ovn/ovn-controller.log <==
2024-06-19T12:35:35.222Z|00021|binding|INFO|Claiming lport 984ca93f-90f4-40b0-92a3-9a83e952aec4 for this chassis.
2024-06-19T12:35:35.222Z|00022|binding|INFO|984ca93f-90f4-40b0-92a3-9a83e952aec4: Claiming fa:16:3e:08:c5:35 172.16.1.98

==> openvswitch/ovs-vswitchd.log <==
2024-06-19T12:35:35.218Z|00039|bridge|INFO|bridge br-int: added interface tap984ca93f-90 on port 2

==> neutron/neutron-ovn-metadata-agent.log <==
2024-06-19 14:35:35.230 17787 INFO neutron.agent.ovn.metadata.agent [-] Port 984ca93f-90f4-40b0-92a3-9a83e952aec4 in datapath 2bdfd0bf-4d7f-4842-be6a-388ec15b9f96 bound to our chassis
2024-06-19 14:35:35.257 17787 INFO neutron.agent.ovn.metadata.agent [-] Provisioning metadata for network 2bdfd0bf-4d7f-4842-be6a-388ec15b9f96
2024-06-19 14:35:35.263 17787 INFO oslo.privsep.daemon [-] Running privsep helper: ['sudo', '/usr/bin/neutron-rootwrap', '/etc/neutron/rootwrap.conf', 'privsep-helper', '--config-file', '/etc/neutron/neutron.conf', '--config-file', '/etc/neutron/neutron_ovn_metadata_agent.ini', '--privsep_context', 'neutron.privileged.default', '--privsep_sock_path', '/tmp/tmp8webriev/privsep.sock']

==> ovn/ovn-controller.log <==
2024-06-19T12:35:35.312Z|00023|binding|INFO|Setting lport 984ca93f-90f4-40b0-92a3-9a83e952aec4 ovn-installed in OVS
2024-06-19T12:35:35.312Z|00024|binding|INFO|Setting lport 984ca93f-90f4-40b0-92a3-9a83e952aec4 up in Southbound

==> neutron/neutron-ovn-metadata-agent.log <==
2024-06-19 14:35:36.181 17787 INFO oslo.privsep.daemon [-] Spawned new privsep daemon via rootwrap
2024-06-19 14:35:36.043 17882 INFO oslo.privsep.daemon [-] privsep daemon starting
2024-06-19 14:35:36.045 17882 INFO oslo.privsep.daemon [-] privsep process running with uid/gid: 0/0
2024-06-19 14:35:36.047 17882 INFO oslo.privsep.daemon [-] privsep process running with capabilities (eff/prm/inh): CAP_DAC_OVERRIDE|CAP_DAC_READ_SEARCH|CAP_NET_ADMIN|CAP_SYS_ADMIN|CAP_SYS_PTRACE/CAP_DAC_OVERRIDE|CAP_DAC_READ_SEARCH|CAP_NET_ADMIN|CAP_SYS_ADMIN|CAP_SYS_PTRACE/none
2024-06-19 14:35:36.047 17882 INFO oslo.privsep.daemon [-] privsep daemon running as pid 17882

==> nova/nova-compute.log <==
2024-06-19 14:35:36.741 14361 INFO nova.compute.manager [None req-5c9d4ac9-5bd5-411c-b2b4-00a8b757a8b2 - - - - - -] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] VM Started (Lifecycle Event)
2024-06-19 14:35:36.791 14361 INFO nova.virt.libvirt.driver [-] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Instance spawned successfully.
2024-06-19 14:35:36.818 14361 INFO nova.compute.manager [None req-5c9d4ac9-5bd5-411c-b2b4-00a8b757a8b2 - - - - - -] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] During sync_power_state the instance has a pending task (spawning). Skip.
2024-06-19 14:35:36.819 14361 INFO nova.compute.manager [None req-5c9d4ac9-5bd5-411c-b2b4-00a8b757a8b2 - - - - - -] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] VM Paused (Lifecycle Event)
2024-06-19 14:35:36.876 14361 INFO nova.compute.manager [None req-5c9d4ac9-5bd5-411c-b2b4-00a8b757a8b2 - - - - - -] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] VM Resumed (Lifecycle Event)
2024-06-19 14:35:36.919 14361 INFO nova.compute.manager [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Took 23.85 seconds to spawn the instance on the hypervisor.
2024-06-19 14:35:36.955 14361 INFO nova.compute.manager [None req-5c9d4ac9-5bd5-411c-b2b4-00a8b757a8b2 - - - - - -] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] During sync_power_state the instance has a pending task (spawning). Skip.
2024-06-19 14:35:37.073 14361 INFO nova.compute.manager [None req-4bb2206d-8e3d-4c18-b50b-780a4738db55 683758c039c9446e8c665c2416af4eec a80b8178e4564f10bd6504b30ecb2783 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Took 28.59 seconds to build instance.

==> neutron/neutron-ovn-metadata-agent.log <==
2024-06-19 14:35:37.711 17882 INFO oslo_utils.netutils [-] Could not determine default network interface, using ::1 for IPv6 address
2024-06-19 14:35:37.799 17787 INFO oslo.privsep.daemon [-] Running privsep helper: ['sudo', '/usr/bin/neutron-rootwrap', '/etc/neutron/rootwrap.conf', 'privsep-helper', '--config-file', '/etc/neutron/neutron.conf', '--config-file', '/etc/neutron/neutron_ovn_metadata_agent.ini', '--privsep_context', 'neutron.privileged.link_cmd', '--privsep_sock_path', '/tmp/tmpq0ddicyo/privsep.sock']

==> nova/nova-compute.log <==
2024-06-19 14:35:37.959 14361 WARNING nova.compute.manager [req-3a6b157f-b830-453c-97f3-b32f92eea454 req-b1b86156-2a0b-4ca8-8795-c7b82ba8e6fb 43839f3658de4c9593645ccec9b5c4d6 f2483a3b77624d979fc6d934db5adb35 - - default default] [instance: 65cd8686-999a-442a-9fdf-fef08bd9795c] Received unexpected event network-vif-plugged-984ca93f-90f4-40b0-92a3-9a83e952aec4 for instance with vm_state active and task_state None.

==> neutron/neutron-ovn-metadata-agent.log <==
2024-06-19 14:35:38.956 17787 INFO oslo.privsep.daemon [-] Spawned new privsep daemon via rootwrap
2024-06-19 14:35:38.776 17894 INFO oslo.privsep.daemon [-] privsep daemon starting
2024-06-19 14:35:38.779 17894 INFO oslo.privsep.daemon [-] privsep process running with uid/gid: 0/0
2024-06-19 14:35:38.781 17894 INFO oslo.privsep.daemon [-] privsep process running with capabilities (eff/prm/inh): CAP_NET_ADMIN|CAP_SYS_ADMIN/CAP_NET_ADMIN|CAP_SYS_ADMIN/none
2024-06-19 14:35:38.781 17894 INFO oslo.privsep.daemon [-] privsep daemon running as pid 17894
2024-06-19 14:35:40.714 17894 INFO oslo_utils.netutils [-] Could not determine default network interface, using ::1 for IPv6 address

==> openvswitch/ovs-vswitchd.log <==
2024-06-19T12:35:41.126Z|00040|bridge|INFO|bridge br-int: added interface tap2bdfd0bf-40 on port 3

==> ovn/ovn-controller.log <==
2024-06-19T12:35:41.148Z|00025|binding|INFO|Releasing lport fca8b77d-39d8-49f3-b5fb-ae6f63b60f46 from this chassis (sb_readonly=0)

==> openvswitch/ovs-vswitchd.log <==
2024-06-19T12:35:45.301Z|00041|connmgr|INFO|br-int<->unix#1: 442 flow_mods in the 5 s starting 10 s ago (423 adds, 19 deletes)

==> ovn/ovn-controller.log <==
2024-06-19T12:36:04.623Z|00004|pinctrl(ovn_pinctrl3)|INFO|DHCPOFFER fa:16:3e:08:c5:35 172.16.1.98
2024-06-19T12:36:04.632Z|00005|pinctrl(ovn_pinctrl3)|INFO|DHCPACK fa:16:3e:08:c5:35 172.16.1.98
```
