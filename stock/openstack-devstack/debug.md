
export OVS_PHYSICAL_BRIDGE=br-ex
export PUBLIC_INTERFACE=
export FLOATING_RANGE=

sudo ovs-vsctl --no-wait -- --may-exist add-port $OVS_PHYSICAL_BRIDGE $PUBLIC_INTERFACE

sudo ip link set $OVS_PHYSICAL_BRIDGE up
sudo ip link set br-int up
sudo ip link set $PUBLIC_INTERFACE up


PUBLIC_INTERFACE=eth0

        ## Neutron options
        Q_USE_SECGROUP=True
        FLOATING_RANGE="172.18.161.0/24"
        IPV4_ADDRS_SAFE_TO_USE="10.0.0.0/22"
        Q_FLOATING_ALLOCATION_POOL=start=172.18.161.250,end=172.18.161.254
        PUBLIC_NETWORK_GATEWAY="172.18.161.1"
        PUBLIC_INTERFACE=eth0

        # Open vSwitch provider networking configuration
        Q_USE_PROVIDERNET_FOR_PUBLIC=True
        OVS_PHYSICAL_BRIDGE=br-ex
        PUBLIC_BRIDGE=br-ex
        OVS_BRIDGE_MAPPINGS=public:br-ex

eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether fa:16:3e:af:a6:ee brd ff:ff:ff:ff:ff:ff
    altname enp4s5
    inet 10.20.11.83/24 metric 100 brd 10.20.11.255 scope global dynamic eth2


sudo iptables -t nat -A POSTROUTING -o $d -s $FLOATING_RANGE -j MASQUERADE


``` bash
6: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 92:83:b0:7e:b1:46 brd ff:ff:ff:ff:ff:ff
    inet 172.24.4.1/24 scope global br-ex
       valid_lft forever preferred_lft forever
    inet6 2001:db8::2/64 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::9083:b0ff:fe7e:b146/64 scope link 
       valid_lft forever preferred_lft forever
```

``` bash
openstack subnet show public-subnet -f json
{
  "allocation_pools": [
    {
      "start": "172.24.4.2",
      "end": "172.24.4.254"
    }
  ],
  "cidr": "172.24.4.0/24",
  "created_at": "2024-02-13T07:00:37Z",
  "description": "",
  "dns_nameservers": [],
  "dns_publish_fixed_ip": null,
  "enable_dhcp": false,
  "gateway_ip": "172.24.4.1",
  "host_routes": [],
  "id": "521dacaf-18d0-446d-8bcd-979ac3d5cb0f",
  "ip_version": 4,
  "ipv6_address_mode": null,
  "ipv6_ra_mode": null,
  "name": "public-subnet",
  "network_id": "a5f0e89e-1253-4288-bc52-6ca950668cbe",
  "project_id": "1ab773e14eca466ca18cdfa7bb2223e9",
  "revision_number": 0,
  "segment_id": null,
  "service_types": [],
  "subnetpool_id": null,
  "tags": [],
  "updated_at": "2024-02-13T07:00:37Z"
}
```


``` bash
ip route
default via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
10.20.1.0/24 dev eth0 proto kernel scope link src 10.20.1.35 metric 100 
10.20.1.1 dev eth0 proto dhcp scope link src 10.20.1.35 metric 100 
10.20.1.254 dev eth0 proto dhcp scope link src 10.20.1.35 metric 100 
100.125.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
100.126.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
169.254.169.254 via 10.20.1.254 dev eth0 proto dhcp src 10.20.1.35 metric 100 
172.24.4.0/24 dev br-ex proto kernel scope link src 172.24.4.1 
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown 
```

cloud@dos-aio:~$ ip route
default via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
10.20.1.0/24 dev eth0 proto kernel scope link src 10.20.1.35 metric 100 
10.20.1.1 dev eth0 proto dhcp scope link src 10.20.1.35 metric 100 
10.20.1.254 dev eth0 proto dhcp scope link src 10.20.1.35 metric 100 
10.20.10.0/24 dev eth1 proto kernel scope link src 10.20.10.141 metric 100 
100.125.0.41 dev eth1 proto dhcp scope link src 10.20.10.141 metric 100 
100.125.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
100.126.0.41 dev eth1 proto dhcp scope link src 10.20.10.141 metric 100 
100.126.0.41 via 10.20.1.1 dev eth0 proto dhcp src 10.20.1.35 metric 100 
169.254.169.254 via 10.20.1.254 dev eth0 proto dhcp src 10.20.1.35 metric 100 
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown 


```
sudo ovs-vsctl show
1b93f307-def5-4459-b0ea-eb75900a2718
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-ex
        Port patch-provnet-242d1f39-ac07-4168-8557-994a444a5ef6-to-br-int
            Interface patch-provnet-242d1f39-ac07-4168-8557-994a444a5ef6-to-br-int
                type: patch
                options: {peer=patch-br-int-to-provnet-242d1f39-ac07-4168-8557-994a444a5ef6}
        Port br-ex
            Interface br-ex
                type: internal
    Bridge br-int
        fail_mode: secure
        datapath_type: system
        Port tapd63625a0-08
            Interface tapd63625a0-08
        Port br-int
            Interface br-int
                type: internal
        Port patch-br-int-to-provnet-242d1f39-ac07-4168-8557-994a444a5ef6
            Interface patch-br-int-to-provnet-242d1f39-ac07-4168-8557-994a444a5ef6
                type: patch
                options: {peer=patch-provnet-242d1f39-ac07-4168-8557-994a444a5ef6-to-br-int}
        Port tape746aef4-00
            Interface tape746aef4-00
    ovs_version: "2.17.8"
```



iptables -t nat -I POSTROUTING -o eth0 -s 172.24.0.0/24 -j MASQUERADE
iptables -I FORWARD -s 172.24.0.0/24 -j ACCEPT

https://rahulait.wordpress.com/2016/06/27/manually-routing-traffic-from-br-ex-to-internet-devstack/


- Chapter 6. Troubleshooting provider networks : [https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/15/html/networking_guide/sec-neutron-troubleshooting](https://access.redhat.com/documentation/fr-fr/red_hat_openstack_platform/15/html/networking_guide/sec-neutron-troubleshooting)