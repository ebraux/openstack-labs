


dos-aio
ssh cloud@90.84.194.34

http://90.84.194.34/dashboard



---
## test

ssh -p 2200 stack@localhost


https://docs.openstack.org/neutron/latest/contributor/testing/ml2_ovs_devstack.html



# Prepare system

``` bash
sudo apt update && sudo apt upgrade -y
[ -f /var/run/reboot-required ] && sudo systemctl reboot
```

``` bash
sudo apt install -y git vim
```

---
## Prepare devstack


Devstack user

Vérifier l'utilisateur stck :
``` bash
cat /etc/passwd | grep stack
# stack:x:1001:1001::/opt/stack:/bin/bash
```

Si l'utilisateur n'exite pas :
``` bash
sudo adduser --shell /bin/bash --home /opt/stack  stack
# Adding user `stack' ...
# Adding new group `stack' (1000) ...
# Adding new user `stack' (1000) with group `stack' ...
# Creating home directory `/opt/stack' ...
# Copying files from `/etc/skel' ...

sudo chmod +x /opt/stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
```

Si il existe déjà, mais pas avec le bon homedir
``` bash
sudo usermod -d /opt/stack -m stack 
```

---
##  Devstack repos

Se connecter en tant que stack

- en su
``` bash
su - stack
```
- en ssh
```
ssh -l stack -h xxxx
```

``` bash
git clone https://git.openstack.org/openstack-dev/devstack
cd devstack
```

fix BUG :
``` bash
chmod 755 /opt/stack
```

Config File
``` bash
tee local.conf <<EOF
[[local|localrc]]
# Password for KeyStone, Database, RabbitMQ and Service
ADMIN_PASSWORD=stack
DATABASE_PASSWORD=\$ADMIN_PASSWORD
RABBIT_PASSWORD=\$ADMIN_PASSWORD
SERVICE_PASSWORD=\$ADMIN_PASSWORD
# Host IP - get your Server/VM IP address from ip addr command
HOST_IP=$(hostname -i)

## Neutron options
# Q_USE_SECGROUP=True
FLOATING_RANGE="10.20.11.0/24"
# IPV4_ADDRS_SAFE_TO_USE="10.0.0.0/22"
Q_FLOATING_ALLOCATION_POOL=start=10.20.11.200,end=10.20.11.250
PUBLIC_NETWORK_GATEWAY="10.20.11.1"
PUBLIC_INTERFACE=eth2

# Open vSwitch provider networking configuration
# Q_USE_PROVIDERNET_FOR_PUBLIC=True
# OVS_PHYSICAL_BRIDGE=br-ex
# PUBLIC_BRIDGE=br-ex
# OVS_BRIDGE_MAPPINGS=public:br-ex
EOF


## Lancement

``` bash
./stack.sh
```