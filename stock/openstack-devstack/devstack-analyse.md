

sudo ovs-vsctl show
--> br-ex : pas d'interface associée (de type eth0)



devstack :

``` bash
4: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 62:f6:a4:2c:3c:f7 brd ff:ff:ff:ff:ff:ff

5: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether b6:9d:15:1c:22:44 brd ff:ff:ff:ff:ff:ff

6: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 96:b5:60:4d:c6:41 brd ff:ff:ff:ff:ff:ff
    inet 172.24.4.1/24 scope global br-ex
       valid_lft forever preferred_lft forever
    inet6 2001:db8::2/64 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::94b5:60ff:fe4d:c641/64 scope link 
       valid_lft forever preferred_lft forever

7: br-tun: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 66:a5:eb:d8:c1:4d brd ff:ff:ff:ff:ff:ff
```
``` bash
 ping 172.24.4.1
PING 172.24.4.1 (172.24.4.1) 56(84) bytes of data.
64 bytes from 172.24.4.1: icmp_seq=1 ttl=64 time=0.083 ms
64 bytes from 172.24.4.1: icmp_seq=2 ttl=64 time=0.017 ms
```

public-subnet       | d48fd8e3-885b-4f5b-8f64-cd63eaeb3470 | 172.24.4.0/24  
private-subnet      | 90bf1816-1027-4d79-8ff7-8b4c71bbc57e | 10.0.0.0/26


on a donc une interface pour public-subnet dans br-ex

``` bash
openstack router show  router1 -f json
```
on a une gateway interface 172.24.4.243, qui ping
``` bash
ping 172.24.4.243
PING 172.24.4.243 (172.24.4.243) 56(84) bytes of data.
64 bytes from 172.24.4.243: icmp_seq=1 ttl=64 time=0.861 ms
64 bytes from 172.24.4.243: icmp_seq=2 ttl=64 time=0.098 ms

```



``` bash
ovs-vsctl show
ad0ff99b-5965-4b44-a882-31dc132836c6
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port qr-53e74237-30
            tag: 1
            Interface qr-53e74237-30
                type: internal
        Port qr-c80e11df-d3
            tag: 1
            Interface qr-c80e11df-d3
                type: internal
        Port tapf42f92da-7d
            tag: 1
            Interface tapf42f92da-7d
                type: internal
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
        Port qg-2022428a-22
            tag: 2
            Interface qg-2022428a-22
                type: internal
        Port br-int
            Interface br-int
                type: internal
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
    Bridge br-ex
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port phy-br-ex
            Interface phy-br-ex
                type: patch
                options: {peer=int-br-ex}
        Port br-ex
            Interface br-ex
                type: internal
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port patch-int
            Interface patch-int
                type: patch
                options: {peer=patch-tun}
        Port br-tun
            Interface br-tun
                type: internal
    ovs_version: "2.17.9"
```