 #!/bin/bash

# enable password Authentification, and set ubuntu password
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "ubuntu:stack" | chpasswd

# avoid  the sudo unable to resolve
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts


# set environement for installation and non interactive update
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"

# set the system up to date
apt-get update && apt -y upgrade && apt-get clean

# Gestion de Repositories complementaires
apt install -y  software-properties-common

# Outils standards
apt install -y sudo vim  lynx zip binutils wget openssl ssl-cert ssh nano

# Prerequis pour devstack
apt install -y bridge-utils git python3-pip

# declaration du nom DNS controller pour la machine
echo `hostname -I`'       controller'  >> /etc/hosts

# creation de l'utilisateur stack
groupadd stack
useradd -g stack       -s /bin/bash -d /home/stack  -m  stack
cd /etc/sudoers.d
echo "stack ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_stack_sh
chmod 440 /etc/sudoers.d/50_stack_sh

# Installation de devstack
cd /home/stack
git clone https://opendev.org/openstack/devstack
cd devstack
git checkout stable/victoria
echo '[[local|localrc]]' > local.conf
echo 'ADMIN_PASSWORD=stack' >> local.conf
echo 'DATABASE_PASSWORD=stack' >> local.conf
echo 'RABBIT_PASSWORD=stack' >> local.conf
echo 'SERVICE_PASSWORD=stack' >> local.conf
echo 'GIT_BASE=https://opendev.org'  >> local.conf
echo 'USE_SCREEN=FALSE'  >> local.conf
echo 'PIP_UPGRADE=True'  >> local.conf
echo 'HOST_IP=172.16.50.51'  >> local.conf
echo 'enable_service h-eng h-api h-api-cfn h-api-cw'  >> local.conf
echo 'enable_plugin heat https://opendev.org/openstack/heat stable/victoria'  >> local.conf 
echo 'enable_plugin heat-dashboard https://opendev.org/openstack/heat-dashboard stable/victoria'  >> local.conf
chown -R stack.stack /home/stack/devstack
su -l stack -c "cd devstack; ./stack.sh"

echo "--- POST Install Devstack : port pour Horizon = 8000 (non priviligied port) ---"
echo "  - Apache Listen Port 8000"
#until [ -f /etc/apache2/ports.conf ]
#do
#     sleep 5
#done
cp -p /etc/apache2/ports.conf /etc/apache2/ports.conf.DIST
sed -i 's/Listen 80/Listen 80\nListen 8000/' /etc/apache2/ports.conf
systemctl restart apache2.service

echo "  - Horizon Port 8000"
#until [ -f /etc/apache2/sites-available/horizon.conf ]
#do
#     sleep 5
#done
cp -p /etc/apache2/sites-available/horizon.conf /etc/apache2/sites-available/horizon.conf.DIST
sed -i 's/<VirtualHost \*:80>/<VirtualHost \*:8000>/'  /etc/apache2/sites-available/horizon.conf
systemctl restart apache2.service

echo " --- POST Install Devstack : activer les services en cas de redémarrage ---"
echo "  - prerequis"
systemctl enable mysql.service
systemctl restart mysql.service

systemctl enable rabbitmq-server.service
systemctl restart rabbitmq-server.service

systemctl enable memcached.service 
systemctl restart memcached.service 

systemctl enable apache2.service
systemctl restart apache2.service

systemctl enable devstack@etcd.service
systemctl restart devstack@etcd.service

echo "  - Keystone"
systemctl enable devstack@keystone.service
systemctl restart devstack@keystone.service

echo "  - placement "
systemctl enable devstack@placement-api.service
systemctl restart devstack@placement-api.service

echo "  - glance"
systemctl enable devstack@g-api.service
systemctl restart "devstack@g-*"

echo "  - nova"
systemctl enable \
  devstack@n-cpu.service \
  devstack@n-api.service \
  devstack@n-sch.service \
  devstack@n-super-cond.service \
  devstack@n-cond-cell1.service \
  devstack@n-api-meta.service \
  devstack@n-novnc-cell1.service
systemctl restart "devstack@n-*"

echo "  - neutron"
systemctl enable \
  devstack@g-api.service \
  devstack@q-svc.service  \
  devstack@q-l3.service   \
  devstack@q-meta.service \
  devstack@q-dhcp.service \
  devstack@q-agt.service 
systemctl restart "devstack@q-*"

echo "  - cinder"
systemctl enable \
  devstack@c-api.service \
  devstack@c-sch.service \
  devstack@c-vol.service
systemctl restart "devstack@c-*"

