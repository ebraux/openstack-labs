- config Kolla
``` bash
changed: [localhost] => (item={'name': 'ovn-encap-ip', 'value': '192.168.60.10'})
changed: [localhost] => (item={'name': 'ovn-encap-type', 'value': 'geneve'})
changed: [localhost] => (item={'name': 'ovn-remote', 'value': 'tcp:192.168.60.10:6642'})
changed: [localhost] => (item={'name': 'ovn-remote-probe-interval', 'value': '60000'})
changed: [localhost] => (item={'name': 'ovn-openflow-probe-interval', 'value': '60'})
changed: [localhost] => (item={'name': 'ovn-monitor-all', 'value': False})
changed: [localhost] => (item={'name': 'ovn-bridge-mappings', 'value': 'physnet1:br-ex', 'state': 'present'})
changed: [localhost] => (item={'name': 'ovn-chassis-mac-mappings', 'value': 'physnet1:52:54:00:2f:c6:fd', 'state': 'present'})
changed: [localhost] => (item={'name': 'ovn-cms-options', 'value': 'enable-chassis-as-gw', 'state': 'present'})
---
iface_types         : [afxdp, afxdp-nonpmd, bareudp, erspan, geneve, gre, gtpu, internal, ip6erspan, ip6gre, lisp, patch, srv6, stt, system, tap, vxlan]
datapath_types      : [netdev, system]
other_config        : {ovn-chassis-idx-kolla-aio="", vlan-limit="0"}

```

---


PUBLIC_BRIDGE=br-ex
sudo ovs-vsctl --may-exist add-br $PUBLIC_BRIDGE -- set bridge $PUBLIC_BRIDGE protocols=OpenFlow13,OpenFlow15
sudo ovs-vsctl set open . external-ids:ovn-bridge-mappings=${OVN_BRIDGE_MAPPINGS}

sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-cms-options="enable-chassis-as-gw"

sudo ovs-vsctl --no-wait set-manager ptcp:6640:$OVSDB_SERVER_LOCAL_HOST
sudo ovs-vsctl --no-wait set open_vswitch . system-type="devstack"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:system-id="$OVN_UUID"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-remote="$OVN_SB_REMOTE"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-bridge="br-int"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-type="geneve"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-ip="$TUNNEL_IP"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:hostname=$(hostname)

Pacaked installé pour OVN : devstack/files/debs/ovn
ovn-central
ovn-controller-vtep
ovn-host

Brideg création :
function ovn_base_setup_bridge 
  local addbr_cmd="sudo ovs-vsctl --no-wait -- --may-exist add-br $bridge -- set bridge $bridge protocols=OpenFlow13,OpenFlow15"
  sudo ovs-vsctl --no-wait br-set-external-id $bridge bridge-id $bridge


function create_public_bridge {
    # Create the public bridge that OVN will use
    sudo ovs-vsctl --may-exist add-br $PUBLIC_BRIDGE -- set bridge $PUBLIC_BRIDGE protocols=OpenFlow13,OpenFlow15
    sudo ovs-vsctl set open . external-ids:ovn-bridge-mappings=${OVN_BRIDGE_MAPPINGS}
        
}

        sudo ovs-vsctl --no-wait set-manager ptcp:6640:$OVSDB_SERVER_LOCAL_HOST
        sudo ovs-vsctl --no-wait set open_vswitch . system-type="devstack"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:system-id="$OVN_UUID"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-remote="$OVN_SB_REMOTE"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-bridge="br-int"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-type="geneve"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-ip="$TUNNEL_IP"
        sudo ovs-vsctl --no-wait set open_vswitch . external-ids:hostname=$(hostname)

        # Select this chassis to host gateway routers
        if [[ "$ENABLE_CHASSIS_AS_GW" == "True" ]]; then
            sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-cms-options="enable-chassis-as-gw"
        fi


sudo apt list | grep -i ovn

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

neutron-ovn-metadata-agent/jammy-updates 2:20.5.0-0ubuntu1 all
nova-novncproxy/jammy-updates 3:25.2.1-0ubuntu2 all
novnc/jammy 1:1.0.0-5 all
ovn-central/jammy-updates,now 22.03.3-0ubuntu0.22.04.3 amd64 [installed]
ovn-common/jammy-updates,now 22.03.3-0ubuntu0.22.04.3 amd64 [installed,automatic]
ovn-controller-vtep/jammy-updates,now 22.03.3-0ubuntu0.22.04.3 amd64 [installed]
ovn-doc/jammy-updates 22.03.3-0ubuntu0.22.04.3 all
ovn-docker/jammy-updates 22.03.3-0ubuntu0.22.04.3 amd64
ovn-host/jammy-updates,now 22.03.3-0ubuntu0.22.04.3 amd64 [installed]
ovn-ic-db/jammy-updates 22.03.3-0ubuntu0.22.04.3 amd64
ovn-ic/jammy-updates 22.03.3-0ubuntu0.22.04.3 amd64
puppet-module-ovn/jammy 19.4.0-2 all
python3-novnc/jammy 1:1.0.0-5 all
python3-ovn-octavia-provider/jammy 2.0.0-0ubuntu1 all


neutron-ovn-metadata-agent/jammy-updates 2:20.5.0-0ubuntu1 all


---



ASK [openvswitch : include_tasks] *************************************************************************************************************************************************************************
included: /home/vagrant/kolla-ansible/.venv/share/kolla-ansible/ansible/roles/openvswitch/tasks/deploy.yml for localhost


TASK [module-load : Load modules] **************************************************************************************************************************************************************************
changed: [localhost] => (item=openvswitch)

TASK [module-load : Persist modules via modules-load.d] ****************************************************************************************************************************************************
changed: [localhost] => (item=openvswitch)

TASK [module-load : Drop module persistence] ***************************************************************************************************************************************************************
skipping: [localhost] => (item=openvswitch) 
skipping: [localhost]

TASK [openvswitch : Create /run/openvswitch directory on host] *********************************************************************************************************************************************
skipping: [localhost]

TASK [openvswitch : Ensuring config directories exist] ******

TASK [openvswitch : Copying over config.json files for services] *

TASK [openvswitch : Copying over start-ovs file for openvswitch-vswitchd] 

TASK [openvswitch : Copying over start-ovsdb-server files for openvswitch-db-server] 

TASK [openvswitch : Copying over ovs-vsctl wrapper]


TASK [openvswitch : Check openvswitch containers] 

RUNNING HANDLER [openvswitch : Restart openvswitch-db-server container]


TASK [openvswitch : Set system-id and hw-offload

TASK [openvswitch : Ensuring OVS bridge is properly setup] *************************************************************************************************************************************************
changed: [localhost] => (item=br-ex)

TASK [openvswitch : Ensuring OVS ports are properly setup] *************************************************************************************************************************************************
changed: [localhost] => (item=['br-ex', 'enp0s9'])


PLAY [Apply role ovs-dpdk] *********************************************************************************************************************************************************************************
skipping: no hosts matched

PLAY [Apply role ovn-controller] ***************************************************************************************************************************************************************************

TASK [ovn-controller : include_tasks] **********************************************************************************************************************************************************************
included: /home/vagrant/kolla-ansible/.venv/share/kolla-ansible/ansible/roles/ovn-controller/tasks/deploy.yml for localhost

TASK [ovn-controller : Ensuring config directories exist] **************************************************************************************************************************************************
changed: [localhost] => (item={'key': 'ovn-controller', 'value': {'container_name': 'ovn_controller', 'group': 'ovn-controller', 'enabled': True, 'image': 'quay.io/openstack.kolla/ovn-controller:2024.1-ubuntu-jammy', 'volumes': ['/etc/kolla/ovn-controller/:/var/lib/kolla/config_files/:ro', '/run/openvswitch:/run/openvswitch:shared', '/etc/localtime:/etc/localtime:ro', 'kolla_logs:/var/log/kolla/'], 'dimensions': {}}})

TASK [ovn-controller : Copying over config.json files for services] ****************************************************************************************************************************************
changed: [localhost] => (item={'key': 'ovn-controller', 'value': {'container_name': 'ovn_controller', 'group': 'ovn-controller', 'enabled': True, 'image': 'quay.io/openstack.kolla/ovn-controller:2024.1-ubuntu-jammy', 'volumes': ['/etc/kolla/ovn-controller/:/var/lib/kolla/config_files/:ro', '/run/openvswitch:/run/openvswitch:shared', '/etc/localtime:/etc/localtime:ro', 'kolla_logs:/var/log/kolla/'], 'dimensions': {}}})

TASK [ovn-controller : Check ovn-controller containers] ****************************************************************************************************************************************************
changed: [localhost] => (item={'key': 'ovn-controller', 'value': {'container_name': 'ovn_controller', 'group': 'ovn-controller', 'enabled': True, 'image': 'quay.io/openstack.kolla/ovn-controller:2024.1-ubuntu-jammy', 'volumes': ['/etc/kolla/ovn-controller/:/var/lib/kolla/config_files/:ro', '/run/openvswitch:/run/openvswitch:shared', '/etc/localtime:/etc/localtime:ro', 'kolla_logs:/var/log/kolla/'], 'dimensions': {}}})

TASK [ovn-controller : Create br-int bridge on OpenvSwitch] ************************************************************************************************************************************************
changed: [localhost]

TASK [ovn-controller : Configure OVN in OVSDB] *************************************************************************************************************************************************************
changed: [localhost] => (item={'name': 'ovn-encap-ip', 'value': '192.168.60.10'})
changed: [localhost] => (item={'name': 'ovn-encap-type', 'value': 'geneve'})
changed: [localhost] => (item={'name': 'ovn-remote', 'value': 'tcp:192.168.60.10:6642'})
changed: [localhost] => (item={'name': 'ovn-remote-probe-interval', 'value': '60000'})
changed: [localhost] => (item={'name': 'ovn-openflow-probe-interval', 'value': '60'})
changed: [localhost] => (item={'name': 'ovn-monitor-all', 'value': False})
changed: [localhost] => (item={'name': 'ovn-bridge-mappings', 'value': 'physnet1:br-ex', 'state': 'present'})
changed: [localhost] => (item={'name': 'ovn-chassis-mac-mappings', 'value': 'physnet1:52:54:00:2f:c6:fd', 'state': 'present'})
changed: [localhost] => (item={'name': 'ovn-cms-options', 'value': 'enable-chassis-as-gw', 'state': 'present'})


