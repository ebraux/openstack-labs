``` bash
cat /etc/openvswitch/system-id.conf 
# 24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3

sudo  ovn-sbctl list chassis
# _uuid               : d31f265e-3dea-4170-8e85-e3209da428d3
# encaps              : [0f198029-9d07-4eb4-a7fd-ab789be5d2ee]
# external_ids        : {}
# hostname            : controller
# name                : "24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3"
# nb_cfg              : 0
# other_config        : {ct-no-masked-label="true", datapath-type=system, fdb-timestamp="true", iface-types="", is-interconn="false", mac-binding-timestamp="true", ovn-bridge-mappings="", ovn-chassis-mac-mappings="", ovn-cms-options=enable-chassis-as-gw, ovn-ct-lb-related="true", ovn-enable-lflow-cache="true", ovn-limit-lflow-cache="", ovn-memlimit-lflow-cache-kb="", ovn-monitor-all="false", ovn-trim-limit-lflow-cache="", ovn-trim-timeout-ms="", ovn-trim-wmark-perc-lflow-cache="", port-up-notif="true"}
# transport_zones     : []
# vtep_logical_switches: []

sudo  ovn-sbctl list chassis_private
# _uuid               : b0cbf54d-3c39-495e-938b-12401fc979ba
# chassis             : d31f265e-3dea-4170-8e85-e3209da428d3
# external_ids        : {"neutron:ovn-metadata-id"="e0f87368-1c9b-5e2f-8a5d-7ba44d02929b"}
# name                : "24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3"
# nb_cfg              : 0
# nb_cfg_timestamp    : 0

sudo ovs-vsctl list open_vswitch
# _uuid               : 9fbe102b-9868-447e-940f-65d6fb663e8f
# bridges             : [0ae0d757-d5f0-4965-97c9-2c3ae6720fca, 11f73143-a2bd-4400-996b-6bddfe4b5f6c]
# cur_cfg             : 0
# datapath_types      : []
# datapaths           : {system=8ce17ae5-0306-4173-a9b4-86bf82f66613}
# db_version          : "8.4.0"
# dpdk_initialized    : false
# dpdk_version        : []
# external_ids        : {hostname=controller, ovn-bridge=br-int, ovn-cms-options=enable-chassis-as-gw, ovn-encap-ip="192.168.50.12", ovn-encap-type=geneve, ovn-remote="tcp:192.168.50.12:6642", rundir="/var/run/openvswitch", system-id="24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3"}
# iface_types         : []
# manager_options     : []
# next_cfg            : 4
# other_config        : {ovn-chassis-idx-24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3="", vlan-limit="0"}
# ovs_version         : "3.2.1"
# ssl                 : []
# statistics          : {}
# system_type         : ubuntu
# system_version      : "22.04"

```
