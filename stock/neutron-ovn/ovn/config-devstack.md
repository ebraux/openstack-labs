- config devstack
``` bash
export OVN_UUID=$(sudo cat /etc/openvswitch/system-id.conf)
export OVN_SB_REMOTE='tcp:192.168.50.12:6642'
export TUNNEL_IP='192.168.50.12'

#sudo ovs-vsctl --no-wait set-manager ptcp:6640:$OVSDB_SERVER_LOCAL_HOST
#sudo ovs-vsctl --no-wait set open_vswitch . system-type="devstack"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:system-id="$OVN_UUID"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-remote="$OVN_SB_REMOTE"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-bridge="br-int"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-type="geneve"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:ovn-encap-ip="$TUNNEL_IP"
sudo ovs-vsctl --no-wait set open_vswitch . external-ids:hostname=$(hostname)
sudo ovs-vsctl  --no-wait set open . external-ids:ovn-cms-options=enable-chassis-as-gw
#  ovn-bridge-mappings="public:br-ex"
# sudo ovs-vsctl --no-wait set-manager ptcp:6640:$OVSDB_SERVER_LOCAL_HOST
# sudo ovs-vsctl --no-wait set open_vswitch . system-type="devstack"
---
iface_types         : [bareudp, erspan, geneve, gre, gtpu, internal, ip6erspan, ip6gre, lisp, patch, stt, system, tap, vxlan]
datapath_types      : [netdev, system]
other_config        : {vlan-limit="0"}

```
---
# Example: ``OVS_PHYSICAL_BRIDGE=br-eth1``
OVS_PHYSICAL_BRIDGE=${OVS_PHYSICAL_BRIDGE:-br-ex}


# Use DHCP agent for providing metadata service in the case of
# without L3 agent (No Route Agent), set to True in localrc.
ENABLE_ISOLATED_METADATA=${ENABLE_ISOLATED_METADATA:-False}

# Add a static route as dhcp option, so the request to 169.254.169.254
# will be able to reach through a route(DHCP agent)
# This option require ENABLE_ISOLATED_METADATA = True
ENABLE_METADATA_NETWORK=${ENABLE_METADATA_NETWORK:-False}


# OVN_BRIDGE_MAPPINGS - ovn-bridge-mappings
# NOTE(hjensas): Initialize after sourcing neutron_plugins/services/l3
# which initialize PUBLIC_BRIDGE.
OVN_BRIDGE_MAPPINGS=${OVN_BRIDGE_MAPPINGS:-$PHYSICAL_NETWORK:$PUBLIC_BRIDGE}


create_public_bridge
cleanup_ovn

# _configure_public_network_connectivity() - Configures connectivity to the
# external network using $PUBLIC_INTERFACE or NAT on the single interface
# machines
function _configure_public_network_connectivity {
    # If we've given a PUBLIC_INTERFACE to take over, then we assume
    # that we can own the whole thing, and privot it into the OVS
    # bridge. If we are not, we're probably on a single interface
    # machine, and we just setup NAT so that fixed guests can get out.
    if [[ -n "$PUBLIC_INTERFACE" ]]; then
        _move_neutron_addresses_route "$PUBLIC_INTERFACE" "$OVS_PHYSICAL_BRIDGE" True False "inet"

        if [[ $(ip -f inet6 a s dev "$PUBLIC_INTERFACE" | grep -c 'global') != 0 ]]; then
            _move_neutron_addresses_route "$PUBLIC_INTERFACE" "$OVS_PHYSICAL_BRIDGE" False False "inet6"
        fi
    else
        for d in $default_v4_route_devs; do
            sudo iptables -t nat -A POSTROUTING -o $d -s $FLOATING_RANGE -j MASQUERADE
        done
    fi
}


---
## devstack/lib/neutron_plugins/services/l3

    _neutron_setup_interface_driver $Q_L3_CONF_FILE

    neutron_plugin_configure_l3_agent $Q_L3_CONF_FILE

    _configure_public_network_connectivity




# Get ext_gw_interface depending on value of Q_USE_PUBLIC_VETH
function _neutron_get_ext_gw_interface {
    if [[ "$Q_USE_PUBLIC_VETH" == "True" ]]; then
        echo $Q_PUBLIC_VETH_EX
    else
        # Disable in-band as we are going to use local port
        # to communicate with VMs
        sudo ovs-vsctl set Bridge $PUBLIC_BRIDGE \
            other_config:disable-in-band=true
        echo $PUBLIC_BRIDGE
    fi
}



        if [[ $Q_AGENT == "openvswitch" ]]; then
            sudo ip link set $OVS_PHYSICAL_BRIDGE up
            sudo ip link set br-int up
            sudo ip link set $PUBLIC_INTERFACE up
        fi




                sudo ip addr add $ext_gw_ip/$cidr_len dev $ext_gw_interface
                sudo ip link set $ext_gw_interface up

