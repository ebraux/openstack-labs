
Erreur dans les logs du neutron_o
cause : manque le packge ovn_host sur la machine qui fait controller OVN.
et donc aucun chassis ne lui correspond.


ovsdbapp.backend.ovs_idl.idlutils.RowNotFound: Cannot find Chassis_Private with name=24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3


``` bash
ovs-vsctl list open_vswitch
_uuid               : 9fbe102b-9868-447e-940f-65d6fb663e8f
bridges             : [0ae0d757-d5f0-4965-97c9-2c3ae6720fca, 11f73143-a2bd-4400-996b-6bddfe4b5f6c]
cur_cfg             : 0
datapath_types      : []
datapaths           : {}
db_version          : "8.4.0"
dpdk_initialized    : false
dpdk_version        : []
external_ids        : {rundir="/var/run/openvswitch", system-id="24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3"}
iface_types         : []
manager_options     : []
next_cfg            : 3
other_config        : {}
ovs_version         : "3.2.1"
ssl                 : []
statistics          : {}
system_type         : ubuntu
system_version      : "22.04"
```

avec Devstack, on a : devrait être
``` bash
cur_cfg             : 2
datapath_types      : [netdev, system]
datapaths           : {system=a3a326b4-b0ff-4740-b552-df8ab501aecf}
external_ids        : {hostname=aio, ovn-bridge=br-int, ovn-bridge-mappings="public:br-ex", ovn-cms-options=enable-chassis-as-gw, ovn-encap-ip="192.168.50.10", ovn-encap-type=geneve, ovn-remote="tcp:192.168.50.10:6642", rundir="/var/run/openvswitch", system-id="51b27d72-e5ef-45b4-a374-331d6ab0a986"}
iface_types         : [bareudp, erspan, geneve, gre, gtpu, internal, ip6erspan, ip6gre, lisp, patch, stt, system, tap, vxlan]
manager_options     : [9a7bb502-1262-4580-a4b3-6d1107923280]
next_cfg            : 2
other_config        : {vlan-limit="0"}
```


Display chassis and chasssis private list
``` bash
```

``` bash
sudo ovs-vsctl get open . external_ids:system-id
"24e7065b-0e4f-4dbe-bed8-0cc1c4d54cf3"
```

``` bash
sudo ovs-vsctl get open . external_ids:ovn-bridge-mappings
#ovs-vsctl: no key "ovn-bridge-mappings" in Open_vSwitch record "." column external_ids
# devrait être public:br-ex


```
