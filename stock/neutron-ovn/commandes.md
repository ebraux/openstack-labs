
``` bash
export SB=$(sudo ovs-vsctl get open_vswitch . external_ids:ovn-remote | sed -e 's/\"//g')
sudo ovn-sbctl --db=$SB show
```


https://docs.openstack.org/openstack-ansible-os_neutron/latest/app-ovn.html