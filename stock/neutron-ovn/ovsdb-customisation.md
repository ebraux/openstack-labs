# Gestion de la configuration de ovsdb-server


Pour rendre la base accessible via le réseau, et pas par la socket UNIX,on utilise la commande.
``` bash
sudo ovs-appctl -t ovsdb-server ovsdb-server/add-remote ptcp:6640:0.0.0.0
```

Mais avec cette méthode, ma modification n'est pas persistante.

Pour la rendre persistante, il faut agir au niveau du lancement de ovsdb-server par le système.

ovsdb-server est géré via le script systemd : `/lib/systemd/system/ovsdb-server.service`

La personnalisation se fait via la variable `OVS_CTL_OPTS`.


Scénario 1 : Modification du fichier d'environnement `/etc/default/openvswitch-switch` appelé par le service
``` bash
sudo tee -a /etc/default/openvswitch-switch<< EOF
OVS_CTL_OPTS="--ovsdb-server-options='--remote=ptcp:6640:127.0.0.1'"
EOF

sudo systemctl restart openvswitch-switch.service
```

```
Scénario 2 : Déclaration de la variable au niveau de systemd
```  bash
sudo tee /etc/systemd/system/ovsdb-server.service.d/remote.conf<< EOF
[Service]
Environment=OVS_CTL_OPTS='--ovsdb-server-options="--remote=ptcp:6640:127.0.0.1"'
EOF

sudo systemctl daemon-reload

systemctl restart ovsdb-server.service 
```

Dans tous les cas, pour vérifier que le service est bien exposé :
``` bash
sudo ss -ltupn | grep 6640
# tcp   LISTEN 0      10              0.0.0.0:6640       0.0.0.0:*    users:(("ovsdb-server",pid=88479,fd=23))       
```
