


afficher la config crée par runonce, publiée par neutron :
- un réseau public1 : réseau provider connecté à physnet1 = ens4
- un réseau demo-net : réseau privé
- un router : demo-router

Sur Switch public1
- port localport ??
- port router : associé au routerur demo-router
- port localnet : associé au réseau physnet1  (provnat ...)

Sur switch  demo-net


Sur router demo-router
- un port pour réseau public1
- un port pour réseau demo-net
- une association sNAT entrs les 2 pour le traffic sortant

``` bash
sudo docker exec  ovn_nb_db  ovn-nbctl show

switch 06dfa4d9-0ba7-482b-8c16-ce51460b61f3 (neutron-85bef907-8a60-4665-b218-e96726cd8bce) (aka public1)
    port 7d2c9660-01e8-47b0-a610-46107a85bedf
        type: localport
        addresses: ["fa:16:3e:23:9d:e8"]
    ---> ???

    port 4db58bae-c637-474a-ba53-08bd9f654c57
        type: router
        router-port: lrp-4db58bae-c637-474a-ba53-08bd9f654c57
    ---> lien vers le router demo-router (router-port correspond)

    port provnet-e979f8d8-7fc8-48df-8edb-33c1b51bfae3
        type: localnet
        addresses: ["unknown"]
    ---> ???

switch 723129d6-bfaa-4e30-b04b-8f18d826b777 (neutron-dd788efb-7841-4526-98b8-aba6f1aa38d6) (aka demo-net)
    port 5ccfedd7-3be6-4a93-822a-355c6fa68b7e
        addresses: ["fa:16:3e:07:3f:ab 10.0.0.153"]
    ---> Interface réseau instance demo1

    port c57e7732-5cb2-4d9c-9049-691ffa113470
        type: router
        router-port: lrp-c57e7732-5cb2-4d9c-9049-691ffa113470
    ---> lien vers le router demo-router (router-port correspond)

    port ec58d58e-3686-4229-be5d-c394c0270200
        type: localport
        addresses: ["fa:16:3e:53:ee:bb 10.0.0.2"]
    ---> ???

router 5906d879-258d-4049-b8e6-6574feb58ad0 (neutron-54eca82c-1373-44d3-9f2b-f58126b904f1) (aka demo-router)
    port lrp-c57e7732-5cb2-4d9c-9049-691ffa113470
        mac: "fa:16:3e:8f:18:e0"
        networks: ["10.0.0.1/24"]
    --->  interface de router-demo vers  demo-net        

    port lrp-4db58bae-c637-474a-ba53-08bd9f654c57
        mac: "fa:16:3e:be:b4:e6"
        networks: ["203.0.113.158/24"]
        gateway chassis: [controller-ebraux]
    --->  Gateway du router, connectée au chassis de la machine hôte

    nat 33278368-3a00-4694-9a83-b2b489a8d6bd
        external ip: "203.0.113.158"
        logical ip: "10.0.0.0/24"
        type: "snat"
```
La valeur de "port" correspond au nom de l'objet OVN, et à l'ID dans openstack. Chaque objet OVN a un ID OVN indépendant.

Quand on fait un arping depuis la machine edge, on voit bien que c'est l'adresse mac de l'interface du routeur :
``` bash
sudo arping 203.0.113.158
ARPING 203.0.113.158
58 bytes from fa:16:3e:be:b4:e6 (203.0.113.158): index=0 time=3.168 msec
```

``` bash
openstack port list -c "MAC Address" -c "Fixed IP Addresses" -c "Status"
# +-------------------+------------------------------------------------------------------------------+--------+
# | MAC Address       | Fixed IP Addresses                                                           | Status |
# +-------------------+------------------------------------------------------------------------------+--------+
# | fa:16:3e:be:b4:e6 | ip_address='203.0.113.158', subnet_id='7925f3c0-6201-4d22-8ff1-13bf03af6f1f' | ACTIVE |
# | fa:16:3e:07:3f:ab | ip_address='10.0.0.153', subnet_id='c79ab16f-8125-464a-b75f-e44c8dc391fe'    | ACTIVE   |
# | fa:16:3e:23:9d:e8 |                                                                              | DOWN   |
# | fa:16:3e:8f:18:e0 | ip_address='10.0.0.1', subnet_id='c79ab16f-8125-464a-b75f-e44c8dc391fe'      | ACTIVE |
# | fa:16:3e:53:ee:bb | ip_address='10.0.0.2', subnet_id='c79ab16f-8125-464a-b75f-e44c8dc391fe'      | DOWN   |
# +-------------------+------------------------------------------------------------------------------+--------+
```


sudo docker exec  ovn_nb_db  ovn-nbctl  list logical_switch_port 723129d6-bfaa-4e30-b04b-8f18d826b777

Interraction avec OVS
``` bash
sudo docker exec  openvswitch_vswitchd  ovs-appctl tnl/arp/show
# IP                                            MAC                 Bridge
# ==========================================================================
```

``` bash
sudo docker exec openvswitch_vswitchd ovs-vsctl list-br
# br-ex
# br-int
```

``` bash
sudo docker exec openvswitch_vswitchd ovs-vsctl list-ports br-ex
# enp4s0
# patch-provnet-e979f8d8-7fc8-48df-8edb-33c1b51bfae3-to-br-int
```

``` bash
sudo docker exec openvswitch_vswitchd  ovs-vsctl add-port br-ex int-local -- set Interface int-local type=internal

sudo docker exec openvswitch_vswitchd ovs-vsctl list-ports br-ex
# enp4s0
# int-local
# patch-provnet-e979f8d8-7fc8-48df-8edb-33c1b51bfae3-to-br-int
```
``` bash
ip a
# 10: int-local: <BROADCAST,MULTICAST> mtu 1400 qdisc noop state DOWN group default qlen 1000
#     link/ether 46:53:50:8b:ac:67 brd ff:ff:ff:ff:ff:ff

```
``` bash
sudo ip addr add 203.0.113.11/24 dev int-local
sudo ip link set int-local up
```
``` bash
ip a
# 10: int-local: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1400 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/ether 46:53:50:8b:ac:67 brd ff:ff:ff:ff:ff:ff
#     inet 203.0.113.11/24 scope global int-local
#        valid_lft forever preferred_lft forever
#     inet6 fe80::4453:50ff:fe8b:ac67/64 scope link 
#        valid_lft forever preferred_lft forever
```


https://docs.openvswitch.org/en/latest/faq/issues/
Q: I created a tap device tap0, configured an IP address on it, and added it to a bridge, like this:
