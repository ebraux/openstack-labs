# Installation d'OVN

https://www.server-world.info/en/note?os=Ubuntu_22.04&p=openstack_bobcat2&f=9
https://docs.openstack.org/neutron/2023.2/install/ovn/manual_install.html


Installation de Install the openvswitch-ovn and networking-ovn packages
``` bash
sudo apt install -y ovn-central openvswitch-switch 
```

Start the OVS service. The central OVS service starts the ovsdb-server service that manages OVN databases.
``` bash
sudo systemctl restart  openvswitch-switch
sudo systemctl restart  ovn-central ovn-northd
```


Configure the ovsdb-server component. By default, the ovsdb-server service only permits local access to databases via Unix socket. However, OVN services on compute nodes require access to these databases.

Par défaut, OVN fonctionne uniquement en local, en utilisant les socket UNIX
Ouvre l'accès à la North-bound Database, pour les compute
``` bash
sudo ovn-nbctl set-connection ptcp:6641:0.0.0.0 -- \
          set connection . inactivity_probe=60000
```
Ouvre l'accès à la South-bound Database, pour les compute
``` bash
sudo ovn-sbctl set-connection ptcp:6642:0.0.0.0 -- \
            set connection . inactivity_probe=60000
```

sudo systemctl start ovn-northd


``` bash
ovs-vsctl set open . external-ids:ovn-cms-options=enable-chassis-as-gw
```

---
## Config neutron 

/etc/neutron/neutron.conf 
/etc/neutron/plugins/ml2/ml2_conf.ini 


``` bash
ovs-vsctl set open . external-ids:ovn-cms-options=enable-chassis-as-gw
```

---
## commandes

/usr/share/openvswitch/scripts/ovn-ctl



---
## Pour la config des Listen 
``` bash
if using the VTEP functionality:
ovs-appctl -t ovsdb-server ovsdb-server/add-remote ptcp:6640:0.0.0.
```


voir : /etc/default/openvswitch-switch
OVS_CTL_OPTS="--ovsdb-server-options='--remote=ptcp:6640:127.0.0.1'"

root@node01:~# ovs-vsctl set open . external-ids:ovn-remote=tcp:10.0.0.50:6642
root@node01:~# ovs-vsctl set open . external-ids:ovn-encap-type=geneve
root@node01:~# ovs-vsctl set open . external-ids:ovn-encap-ip=10.0.0.51



---
## Configuration du Neutron server

- Gestion ML2 entre les noeuds :
    - Mécanisme : openwsitch
    - Nom du Bridge pour le tunnel d'interconnexion : br-tun
- Fonctionnalités :
    - router
    - groupes de sécurité
    - firwall
- Réseau de tenant :
    - type : self service
    - ségrégation de réseau : vxlan
- Configuration de la machine controller
    - Interface admin et ML2 :
        - nom : enp0s3
        - IP 10.0.2.15
        - MTU 1500
    - Interface réseau pour "Floating IP provider"  (bridge OpenVswitch)
        - nom : br-ex
        - IP : 172.24.4.1
        - MTU 1500