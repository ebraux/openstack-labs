

Mettre en place le tcpdump sur l'interface qui sert pour br-ex


- arping depuis le edge node vers l'IP du routeur : ca arrive sur l'interface, mais personne ne répond
- arping depuis le serveur KOLLA vers l'IP du routeur : rien
- arping depuis le router vers le edge node : rien
- arping depuis le router vers une autre interface du serveur KOLLA :
- 
- Lancer un arping vers l'IP du routeur, depuis le serveur KOLLA: rien n'arrive au tcpdump

Lancer un arping depuis le routeur vers le edge node 
``` bash
sudo ip netns exec qrouter-04f77b28-94bd-40f9-b628-69cf00bffe27 arping 10.20.11.12
```
Rien n'arrive au tcpdump



suivi dans les tables OVS
``` bash
watch -n1 docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex
```
- arping depuis le edge node vers l'IP du routeur : rien
- arping depuis le serveur KOLLA vers l'IP du routeur : rien
- arping depuis le router vers le edge node : ça passe par br-ex
- arping depuis le router vers une autre interface du serveur KOLLA : ça passe par br-ex
- 
sudo ip netns exec qrouter-04f77b28-94bd-40f9-b628-69cf00bffe27 arping 10.20.1.36


- arping depuis le edge node vers l'IP du routeur
``` bash
sudo arping 10.20.11.183
```
- arping depuis le serveur KOLLA vers l'IP du routeur
``` bash
sudo arping 10.20.11.183
```
- arping depuis le router vers le edge node:
``` bash
sudo ip netns exec qrouter-04f77b28-94bd-40f9-b628-69cf00bffe27 arping 10.20.11.12
```
- arping depuis le router vers le edge node vers une autre interface du serveur KOLLA :
``` bash
sudo ip netns exec qrouter-04f77b28-94bd-40f9-b628-69cf00bffe27 arping 10.20.1.36
```

Voir les paquet arriver sur l'interface avec TCPDump
``` bash
sudo tcpdump -i enp5s0 --immediate-mode -e -n arp
```
Ca arrive sur l'interface physique de br-ex, mais personne ne répond.
``` bash
18:05:15.183890 fa:16:3e:82:0a:5c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 58: Request who-has 10.20.11.183 tell 10.20.11.12, length 44
18:05:16.183854 fa:16:3e:82:0a:5c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 58: Request who-has 10.20.11.183 tell 10.20.11.12, length 44
18:05:17.185483 fa:16:3e:82:0a:5c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 58: Request who-has 10.20.11.183 tell 10.20.11.12, length 44
```

Voir les paquet arrivent sur br-ex : faire un `dump-flows`:
``` bash
watch -n1 docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex
```
``` bash
NXST_FLOW reply (xid=0x4):
 cookie=0x970b1a763b054eb7, duration=2538.944s, table=0, n_packets=213, n_bytes=12170, idle_age=0, priority=4,in_port=2,dl_vlan=1 actions=strip_vlan,NORMAL
```
si `n_packets` augmente, les paquets arrivent sur br-ex, 



arping depuis le routeur vers le node, ou l'autre interface de serveur

Br-int :

arrivent sur table 0  du vlan ID 1 `priority=3,in_port=1,vlan_tci=0x0000/0x1fff actions=mod_vlan_vid:1,resubmit(,59)`  -> 59
  59 -> 60
  60 -> priority=3 actions=NORMAL

On les voit passer sur br-tun, là ils sont DROP, c'est plutôt normal car ils n'y a qu'un noeud

br-ex : on les voit passer :
  priority=4,in_port=2,dl_vlan=1 actions=strip_vlan,NORMAL

docker exec openvswitch_vswitchd  ovs-ofctl show br-int
 ovs-dpctl show br-eth0 flows

 the flow is to drop by default. if we have Vms on the network with Vlan network_type, the port will be opened.
 Is there any reason why these ports would be set to drop by default?




       Port enp8s0
            Interface enp8s0
                error: "could not open network device enp8s0 (No such device)"


256  docker exec openvswitch_vswitchd ovs-vsctl del-port br-ex enp8s0
  257  docker exec openvswitch_vswitchd ovs-vsctl show
  258  docker exec openvswitch_vswitchd ovs-vsctl add-port br-ex enp5s0
  259  docker exec openvswitch_vswitchd ovs-vsctl show

https://randomsecurity.dev/posts/openvswitch-cheat-sheet/

   The Interface table in the Open vSwitch database also maps OpenFlow
   port names to numbers.  To print the OpenFlow port number
   associated with interface eth0, run:

       ovs-vsctl get Interface eth0 ofport

   You can print the entire mapping with:

       ovs-vsctl -- --columns=name,ofport list Interface