
---
## Annexes : Après déploiement de Kolla

``` bash
ip a
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master ovs-system state UP group default qlen 1000
    link/ether 08:00:27:98:b8:17 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fe98:b817/64 scope link 
       valid_lft forever preferred_lft forever
5: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether fe:25:1c:e7:4f:3b brd ff:ff:ff:ff:ff:ff
6: br-ex: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 08:00:27:98:b8:17 brd ff:ff:ff:ff:ff:ff
7: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 1e:19:1a:4c:61:4a brd ff:ff:ff:ff:ff:ff
8: br-tun: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 2e:d8:b3:c1:3a:4f brd ff:ff:ff:ff:ff:ff
```

``` bash
docker exec openvswitch_vswitchd ovs-vsctl show
dd2d0f83-fe7f-4d7f-b48c-a251ee25aae9
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-int
            Interface br-int
                type: internal
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
    Bridge br-ex
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-ex
            Interface br-ex
                type: internal
        Port phy-br-ex
            Interface phy-br-ex
                type: patch
                options: {peer=int-br-ex}
        Port enp0s9
            Interface enp0s9
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        datapath_type: system
        Port br-tun
            Interface br-tun
                type: internal
        Port patch-int
            Interface patch-int
                type: patch
                options: {peer=patch-tun}
```

### br-ex
``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-ex | wc -l
12

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-ex
OFPST_TABLE reply (xid=0x2):
  table 0:
    active=2, lookup=214, matched=214
    max_entries=1000000
    matching:
      exact match or wildcard: in_port eth_{src,dst,type} vlan_{vid,pcp} ip_{src,dst} nw_{proto,tos} tcp_{src,dst}

  table 1:
    active=0, lookup=0, matched=0
    (same features)

  tables 2...253: ditto
```

``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex | wc -l
3

docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex
NXST_FLOW reply (xid=0x4):
 cookie=0x6c08acb10a4e3406, duration=4970.062s, table=0, n_packets=0, n_bytes=0, idle_age=4974, priority=2,in_port=2 actions=drop
 cookie=0x6c08acb10a4e3406, duration=4970.066s, table=0, n_packets=202, n_bytes=40223, idle_age=42, priority=0 actions=NORMAL

```
### br-int
``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-int | wc -l
56

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-int
OFPST_TABLE reply (xid=0x2):
  table 0:
    active=3, lookup=210, matched=210
    max_entries=1000000
    matching:
      exact match or wildcard: in_port eth_{src,dst,type} vlan_{vid,pcp} ip_{src,dst} nw_{proto,tos} tcp_{src,dst}

  table 1:
    active=0, lookup=0, matched=0
    (same features)

  tables 2...22: ditto

  table 23:
    active=1, lookup=0, matched=0
    (same features)

  table 24: ditto

  table 25:
    active=0, lookup=0, matched=0
    (same features)

  tables 26...29: ditto

  table 30:
    active=1, lookup=0, matched=0
    (same features)

  table 31: ditto

  table 32:
    active=0, lookup=0, matched=0
    (same features)

  tables 33...58: ditto

  table 59:
    active=1, lookup=0, matched=0
    (same features)

  table 60: ditto

  table 61:
    active=0, lookup=0, matched=0
    (same features)

  table 62:
    active=1, lookup=0, matched=0
    (same features)

  table 63:
    active=0, lookup=0, matched=0
    (same features)

  
   64...253: ditto
```

``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-int | wc -l
11

docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-int
NXST_FLOW reply (xid=0x4):
 cookie=0x494f86b8ffb0f8d2, duration=5010.415s, table=0, n_packets=0, n_bytes=0, idle_age=5010, priority=65535,dl_vlan=4095 actions=drop
 cookie=0x494f86b8ffb0f8d2, duration=5004.722s, table=0, n_packets=202, n_bytes=40223, idle_age=76, priority=2,in_port=1 actions=drop
 cookie=0x494f86b8ffb0f8d2, duration=5010.419s, table=0, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=resubmit(,59)
 cookie=0x494f86b8ffb0f8d2, duration=5010.421s, table=23, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=drop
 cookie=0x494f86b8ffb0f8d2, duration=5010.416s, table=24, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=drop
 cookie=0x494f86b8ffb0f8d2, duration=5010.413s, table=30, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=resubmit(,59)
 cookie=0x494f86b8ffb0f8d2, duration=5010.412s, table=31, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=resubmit(,59)
 cookie=0x494f86b8ffb0f8d2, duration=5010.418s, table=59, n_packets=0, n_bytes=0, idle_age=5010, priority=0 actions=resubmit(,60)
 cookie=0x494f86b8ffb0f8d2, duration=5010.417s, table=60, n_packets=0, n_bytes=0, idle_age=5010, priority=3 actions=NORMAL
 cookie=0x494f86b8ffb0f8d2, duration=5010.414s, table=62, n_packets=0, n_bytes=0, idle_age=5010, priority=3 actions=NORMAL

```
### br-tun
``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-tun | wc -l
56

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-tun 
OFPST_TABLE reply (xid=0x2):
  table 0:
    active=2, lookup=0, matched=0
    max_entries=1000000
    matching:
      exact match or wildcard: in_port eth_{src,dst,type} vlan_{vid,pcp} ip_{src,dst} nw_{proto,tos} tcp_{src,dst}

  table 1:
    active=0, lookup=0, matched=0
    (same features)

  table 2:
    active=3, lookup=0, matched=0
    (same features)

  table 3:
    active=1, lookup=0, matched=0
    (same features)

  table 4: ditto

  table 5:
    active=0, lookup=0, matched=0
    (same features)

  table 6:
    active=1, lookup=0, matched=0
    (same features)

  table 7:
    active=0, lookup=0, matched=0
    (same features)

  tables 8...9: ditto

  table 10:
    active=1, lookup=0, matched=0
    (same features)

  table 11:
    active=0, lookup=0, matched=0
    (same features)

  tables 12...19: ditto

  table 20:
    active=1, lookup=0, matched=0
    (same features)

  tables 21...22: ditto

  table 23:
    active=0, lookup=0, matched=0
    (same features)

  tables 24...253: ditto

```
``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-tun | wc -l
13


docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-tun
NXST_FLOW reply (xid=0x4):
 cookie=0x6e5d10e8fed25e09, duration=5069.773s, table=0, n_packets=0, n_bytes=0, idle_age=5069, priority=1,in_port=1 actions=resubmit(,2)
 cookie=0x6e5d10e8fed25e09, duration=5069.772s, table=0, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=drop
 cookie=0x6e5d10e8fed25e09, duration=5069.771s, table=2, n_packets=0, n_bytes=0, idle_age=5069, priority=1,arp,dl_dst=ff:ff:ff:ff:ff:ff actions=resubmit(,21)
 cookie=0x6e5d10e8fed25e09, duration=5069.771s, table=2, n_packets=0, n_bytes=0, idle_age=5069, priority=0,dl_dst=00:00:00:00:00:00/01:00:00:00:00:00 actions=resubmit(,20)
 cookie=0x6e5d10e8fed25e09, duration=5069.770s, table=2, n_packets=0, n_bytes=0, idle_age=5069, priority=0,dl_dst=01:00:00:00:00:00/01:00:00:00:00:00 actions=resubmit(,22)
 cookie=0x6e5d10e8fed25e09, duration=5069.769s, table=3, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=drop
 cookie=0x6e5d10e8fed25e09, duration=5069.769s, table=4, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=drop
 cookie=0x6e5d10e8fed25e09, duration=5069.768s, table=6, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=drop
 cookie=0x6e5d10e8fed25e09, duration=5069.768s, table=10, n_packets=0, n_bytes=0, idle_age=5069, priority=1 actions=learn(table=20,hard_timeout=300,priority=1,cookie=0x6e5d10e8fed25e09,NXM_OF_VLAN_TCI[0..11],NXM_OF_ETH_DST[]=NXM_OF_ETH_SRC[],load:0->NXM_OF_VLAN_TCI[],load:NXM_NX_TUN_ID[]->NXM_NX_TUN_ID[],output:OXM_OF_IN_PORT[]),output:1
 cookie=0x6e5d10e8fed25e09, duration=5069.767s, table=20, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=resubmit(,22)
 cookie=0x6e5d10e8fed25e09, duration=5069.767s, table=21, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=resubmit(,22)
 cookie=0x6e5d10e8fed25e09, duration=5069.766s, table=22, n_packets=0, n_bytes=0, idle_age=5069, priority=0 actions=drop

```


