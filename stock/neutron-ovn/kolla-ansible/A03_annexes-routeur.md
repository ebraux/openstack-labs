``` bash
ip a  | wc -l
28

ip route
4

sudo brctl show  | wc -l
0

sudo ip netns  | wc -l
1

sudo iptables -L -n -v  | wc -l
7 -> 39


sudo ebtables -L | wc -l
7

sudo nft list ruleset | wc -l
25 -> 122

sudo docker exec openvswitch_vswitchd ovs-vsctl show
45 -> 49

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-ex | wc -l
12

docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex | wc -l
3 -> 4

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-int | wc -l
56

docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-int | wc -l
11 -> 12
```

docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-tun | wc -l
56 -> 62

docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-tun | wc -l
12 -> 13

``` bash
sudo ip netns
qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd (id: 0)
```
``` bash
sudo iptables -L -n -v 
Chain INPUT (policy ACCEPT 1517K packets, 1709M bytes)
 pkts bytes target     prot opt in     out     source               destination         
 669K  126M neutron-openvswi-INPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 neutron-filter-top  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
    0     0 neutron-openvswi-FORWARD  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain OUTPUT (policy ACCEPT 1441K packets, 262M bytes)
 pkts bytes target     prot opt in     out     source               destination         
 667K  126M neutron-filter-top  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
 667K  126M neutron-openvswi-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-filter-top (2 references)
 pkts bytes target     prot opt in     out     source               destination         
 667K  126M neutron-openvswi-local  all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-openvswi-FORWARD (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-out tapf4483144-07 --physdev-is-bridged /* Accept all packets when port is trusted. */
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            PHYSDEV match --physdev-in tapf4483144-07 /* --physdev-is-bridged Accept all packets when port is trusted. */

Chain neutron-openvswi-INPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-OUTPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-local (1 references)
 pkts bytes target     prot opt in     out     source               destination         

Chain neutron-openvswi-sg-chain (0 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain neutron-openvswi-sg-fallback (0 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      *       0.0.0.0/0            0.0.0.0/0            /* Default drop rule for unmatched traffic. */
```



``` bash
sudo nft list ruleset 
table ip filter {
	chain INPUT {
		...
        counter packets 662059 bytes 124540783 jump neutron-openvswi-INPUT
	}

	chain OUTPUT {
		...
        counter packets 660682 bytes 124644877 jump neutron-filter-top
		counter packets 660682 bytes 124644877 jump neutron-openvswi-OUTPUT
	}

	chain FORWARD {
		...
        counter packets 0 bytes 0 jump neutron-filter-top
		counter packets 0 bytes 0 jump neutron-openvswi-FORWARD
	}

	chain neutron-filter-top {
		counter packets 660682 bytes 124644877 jump neutron-openvswi-local
	}

	chain neutron-openvswi-FORWARD {
		# PHYSDEV match --physdev-out tapf4483144-07 --physdev-is-bridged  counter packets 0 bytes 0 accept
		# PHYSDEV match --physdev-in tapf4483144-07 --physdev-is-bridged  counter packets 0 bytes 0 accept
	}

	chain neutron-openvswi-INPUT {
	}

	chain neutron-openvswi-OUTPUT {
	}

	chain neutron-openvswi-local {
	}

	chain neutron-openvswi-sg-chain {
		counter packets 0 bytes 0 accept
	}

	chain neutron-openvswi-sg-fallback {
		 counter packets 0 bytes 0 drop
	}
}
table ip6 filter {
	chain INPUT {
		...
		counter packets 0 bytes 0 jump neutron-openvswi-INPUT
	}

	chain OUTPUT {
		...
		counter packets 29 bytes 1704 jump neutron-filter-top
		counter packets 29 bytes 1704 jump neutron-openvswi-OUTPUT
	}

	chain FORWARD {
		...
		counter packets 0 bytes 0 jump neutron-filter-top
		counter packets 0 bytes 0 jump neutron-openvswi-FORWARD
	}

	chain neutron-filter-top {
		counter packets 29 bytes 1704 jump neutron-openvswi-local
	}

	chain neutron-openvswi-FORWARD {
		# PHYSDEV match --physdev-out tapf4483144-07 --physdev-is-bridged  counter packets 0 bytes 0 accept
		# PHYSDEV match --physdev-in tapf4483144-07 --physdev-is-bridged  counter packets 0 bytes 0 accept
	}

	chain neutron-openvswi-INPUT {
	}

	chain neutron-openvswi-OUTPUT {
	}

	chain neutron-openvswi-local {
	}

	chain neutron-openvswi-sg-chain {
		counter packets 0 bytes 0 accept
	}

	chain neutron-openvswi-sg-fallback {
		 counter packets 0 bytes 0 drop
	}
}
table ip raw {
	chain OUTPUT {
		type filter hook output priority raw; policy accept;
		counter packets 660682 bytes 124644877 jump neutron-openvswi-OUTPUT
	}

	chain PREROUTING {
		type filter hook prerouting priority raw; policy accept;
		counter packets 662061 bytes 124541935 jump neutron-openvswi-PREROUTING
	}

	chain neutron-openvswi-OUTPUT {
	}

	chain neutron-openvswi-PREROUTING {
	}
}
table ip6 raw {
	chain OUTPUT {
		type filter hook output priority raw; policy accept;
		counter packets 29 bytes 1704 jump neutron-openvswi-OUTPUT
	}

	chain PREROUTING {
		type filter hook prerouting priority raw; policy accept;
		counter packets 0 bytes 0 jump neutron-openvswi-PREROUTING
	}

	chain neutron-openvswi-OUTPUT {
	}

	chain neutron-openvswi-PREROUTING {
	}
}
```

``` bash
docker exec openvswitch_vswitchd ovs-vsctl show
dd2d0f83-fe7f-4d7f-b48c-a251ee25aae9

    Bridge br-int
       ...
        Port qg-f4483144-07
            tag: 1
            Interface qg-f4483144-07
                type: internal
        ...

```
``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-ex 
NXST_FLOW reply (xid=0x4):
 cookie=0x6c08acb10a4e3406, duration=12342.970s, table=0, n_packets=252, n_bytes=19112, idle_age=736, priority=4,in_port=2,dl_vlan=1 actions=strip_vlan,NORMAL
 ...
```
```bash
docker exec openvswitch_vswitchd ovs-ofctl dump-flows br-int 
NXST_FLOW reply (xid=0x4):
...
 cookie=0x494f86b8ffb0f8d2, duration=12486.116s, table=0, n_packets=565, n_bytes=103980, idle_age=1, priority=3,in_port=1,vlan_tci=0x0000/0x1fff actions=mod_vlan_vid:1,resubmit(,59)
...
```

``` bash
docker exec openvswitch_vswitchd ovs-ofctl dump-tables br-tun 
OFPST_TABLE reply (xid=0x2):

  ...
  table 22:
    active=1, lookup=520, matched=520
    (same features)
  ...

```

``` bash
 docker exec openvswitch_vswitchd   ovs-vsctl -- --columns=name,ofport list Interface
name                : br-tun
ofport              : 65534

name                : enp0s9
ofport              : 1

name                : int-br-ex
ofport              : 1

name                : br-ex
ofport              : 65534

name                : patch-int
ofport              : 1

name                : patch-tun
ofport              : 2

name                : br-int
ofport              : 65534

name                : phy-br-ex
ofport              : 2

name                : qg-f4483144-07
ofport              : 3

```

Et ajout de flow OVS permettant de rediriger les flux marqué du `dl_vlan=1` :

- dans br-ex, sur le port `phy-br-ex`
- dans br-int sur le port  `patch-tun`


---
## Tests de connectivité

``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip a
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#     inet 127.0.0.1/8 scope host lo
#        valid_lft forever preferred_lft forever
#     inet6 ::1/128 scope host 
#        valid_lft forever preferred_lft forever
# 9: qg-f4483144-07: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/ether fa:16:3e:57:4c:9c brd ff:ff:ff:ff:ff:ff
#     inet 192.168.50.188/24 brd 192.168.50.255 scope global qg-f4483144-07
#        valid_lft forever preferred_lft forever
#     inet6 fe80::f816:3eff:fe57:4c9c/64 scope link 
#        valid_lft forever preferred_lft forever
```
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip route
# default via 192.168.50.1 dev qg-f4483144-07 proto static 
# 192.168.50.0/24 dev qg-f4483144-07 proto kernel scope link src 192.168.50.188 
```

``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.1
# PING 192.168.50.1 (192.168.50.1) 56(84) bytes of data.
# 64 bytes from 192.168.50.1: icmp_seq=1 ttl=64 time=0.847 ms
# 64 bytes from 192.168.50.1: icmp_seq=2 ttl=64 time=0.485 ms
# 64 bytes from 192.168.50.1: icmp_seq=3 ttl=64 time=0.515 ms

# --- 192.168.50.1 ping statistics ---
# 3 packets transmitted, 3 received, 0% packet loss, time 2047ms
# rtt min/avg/max/mdev = 0.485/0.615/0.847/0.164 ms
```

``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.13
# PING 192.168.50.13 (192.168.50.13) 56(84) bytes of data.
# 64 bytes from 192.168.50.13: icmp_seq=1 ttl=64 time=1.24 ms
# 64 bytes from 192.168.50.13: icmp_seq=2 ttl=64 time=0.489 ms
# 64 bytes from 192.168.50.13: icmp_seq=3 ttl=64 time=0.540 ms

# --- 192.168.50.13 ping statistics ---
# 3 packets transmitted, 3 received, 0% packet loss, time 2015ms
# rtt min/avg/max/mdev = 0.489/0.754/1.235/0.340 ms
```

``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd iptables -L -n -v
# ...
# Chain neutron-l3-agent-INPUT (1 references)
#  pkts bytes target     prot opt in     out     source               destination         
#     0     0 ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            mark match 0x1/0xffff
#     0     0 DROP       tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp dpt:9697
# ...    
```
metadata_port = 9697	(Port number) TCP Port used by Neutron metadata namespace proxy.

> Une régle iptable est mis en place kdans ce namespace, pour bloquer les requetes vers le namespace de metadatas.

