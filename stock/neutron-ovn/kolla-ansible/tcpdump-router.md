# Tester la connectivité avec tcpdump

Utilisation de 3 terminaux :

- TERM-TCPDUMP : sur la machine, pour lancer des TCPDump
- TERM-LOCAL : sur la machine locale
- TERM-EDGE : si disponible, depuis une autre machine

Si besoin dans le terminal TERM-LOCAL, réafficher la configuration ip du routeur
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip a
#   name : qg-f4483144-07
#   link/ether fa:16:3e:57:4c:9c
#   inet 192.168.50.188/24
```

Dans le terminal TERM-TCPDUMP, lancer un tcpdump sur l'interface du routeur (qg-f4483144-07)
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd tcpdump -i qg-f4483144-07 --immediate-mode -e -n
```

Depuis le namespace du routeur lancer un ping vers son adresse IP (192.168.50.188)
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.188
# PING 192.168.50.188 (192.168.50.188) 56(84) bytes of data.
# 64 bytes from 192.168.50.188: icmp_seq=1 ttl=64 time=0.247 ms
# 64 bytes from 192.168.50.188: icmp_seq=2 ttl=64 time=0.089 ms
# 64 bytes from 192.168.50.188: icmp_seq=3 ttl=64 time=0.046 ms

# --- 192.168.50.188 ping statistics ---
# 3 packets transmitted, 3 received, 0% packet loss, time 2049ms
# rtt min/avg/max/mdev = 0.046/0.127/0.247/0.086 ms
```
Dans TERM-TCPDUMP, aucun paquet n’apparaît, pourtant le ping fonctionne bien



Interrompre le tcpdump dans TERM-TCPDUMP, et le lancer sur l'interface de boucle locale :
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd tcpdump -i lo --immediate-mode -e -n
# listening on lo, link-type EN10MB (Ethernet), snapshot length 262144 bytes
# 14:02:01.143485 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 192.168.50.188 > 192.168.50.188: ICMP echo request, id 36063, seq 1, length 64
# 14:02:01.143514 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 192.168.50.188 > 192.168.50.188: ICMP echo reply, id 36063, seq 1, length 64
# ...
```
Cette fois, les échanges apparaissent bien.

### Test au niveau d'OVS

Dans le terminal TERM-TCPDUMP, lancer un tcpdump sur l'interface `br-int` :
``` bash
sudo  tcpdump -i br-int -immediate-mode -e -n
# tcpdump: br-int: That device is not up
```
L'interface `br-int` est gérée par OVS, et non par le système. Idem pour `br-ex`.

`br-ex` est reliée à l'interface physique `enp0s9`. Dans le terminal TERM-TCPDUMP, lancer un tcpdump sur l'interface `enp0s9` :
``` bash
sudo  tcpdump -i enp0s9 --immediate-mode -e -n
# tcpdump: mmediate-mode: No such device exists
# (SIOCGIFHWADDR: No such device)
```

Depuis le terminal TERM-LOCAL, lancer un ping vers l'adresse IP du routeur
``` bash
ping -c 3 192.168.50.188
```
Le ping fonctionne, et les requêtes `ICMP echo request` et `ICMP echo reply` apparaissnt dans le terminal TERM-TCPDUMP

Depuis le terminal TERM-LOCAL, lancer un ping dans l'autre sens depuis le namespce du routeur, vers  la passerelle, ou la machine Edge.
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.1
```

La commande traceroute montre que le lien est direct, pas de routage :
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd traceroute  192.168.50.1
traceroute to 192.168.50.1 (192.168.50.1), 30 hops max, 60 byte packets
 1  192.168.50.1 (192.168.50.1)  1.416 ms  1.116 ms  1.042 ms

sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd traceroute  192.168.50.13
traceroute to 192.168.50.13 (192.168.50.13), 30 hops max, 60 byte packets
 1  192.168.50.13 (192.168.50.13)  2.146 ms  2.113 ms  2.098 ms
```

Si on tente d'accéder à une adresse IP externe depuis le namespace du routeur, les requetes sont bien dirigées vers la passerelle, qui ensuite route (ou non) les paquets
``` bash
udo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd traceroute  8.8.4.4
traceroute to 8.8.4.4 (8.8.4.4), 30 hops max, 60 byte packets
 1  192.168.50.1 (192.168.50.1)  0.587 ms  0.576 ms  0.559 ms
 2  * * *
```
Ici, la paserrelle n'est pas connectée à un réseau externe, et donc le DNS configuré 8.8.4.4 n'ets pas accessible.


``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip neighbour
192.168.50.1 dev qg-f4483144-07 lladdr 0a:00:27:00:00:03 STALE
192.168.50.13 dev qg-f4483144-07 lladdr 08:00:27:13:13:30 STALE
fe80::800:27ff:fe00:3 dev qg-f4483144-07 lladdr 0a:00:27:00:00:03 STALE
fe80::a00:27ff:fe13:1330 dev qg-f4483144-07 lladdr 08:00:27:13:13:30 STALE
fe80::a00:27ff:fea3:1153 dev qg-f4483144-07 lladdr 08:00:27:a3:11:53 STALE
```
L'interconnexion se fait avec :

- 0a:00:27:00:00:03 
- 08:00:27:13:13:30 : adresse MAC de la machine edge
- 08:00:27:a3:11:53

---
## tests de ping depuis un routeur vers la passerelle

lancer un ping depuis le namesapce du routeur, vers la passerelle
``` bash
sudo ip netns exec qrouter-de643622-4501-418b-99fc-de518b290555 ping 10.20.11.1
```
et dans une autre fenêtre  un tcpdump sur l'interface
``` bash
sudo tcpdump -i eth2 --immediate-mode -e -n 
```

---
# Tests avec arpping 


Dans le terminal  TERM-TCPDUMP, lancer tcpdump
``` bash
sudo  tcpdump -i enp0s9 --immediate-mode -e -n
```

Dans le terminal TERM-LOCAL, depuis le namespace du router lancer la commande arping vers l'IP de la passerelle, ou de la machine edge :
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd arping  192.168.50.1
```
Dans TERM-TCPDUMP, on voit les requêtes de demande ARP `Request who-has` et les réponses `Reply` : 
``` bash
06:06:21.337900 fa:16:3e:57:4c:9c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 58: Request who-has 192.168.50.1 tell 192.168.50.188, length 44
06:06:21.338236 0a:00:27:00:00:03 > fa:16:3e:57:4c:9c, ethertype ARP (0x0806), length 60: Reply 192.168.50.1 is-at 0a:00:27:00:00:03, length 46
```

Le même test avec les autres adresses IP de la machine doit également fonctionner
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd arping  10.0.2.15
```
``` bash
06:10:29.284940 fa:16:3e:57:4c:9c > ff:ff:ff:ff:ff:ff, ethertype ARP (0x0806), length 58: Request who-has 10.0.2.15 tell 192.168.50.188, length 44
06:10:29.285447 08:00:27:13:13:30 > fa:16:3e:57:4c:9c, ethertype ARP (0x0806), length 60: Reply 10.0.2.15 is-at 08:00:27:13:13:30, length 46
```
Si à cette étape, les requêtes ARP échouent, c'est qu-il y a un problème de connexion réseau. Dans ce cas plusieurs pistes :

- Les machines portant l'adresse IP utilisée ne sont pas actives, ou leur configuration est mauvaise
- Un sécurité est activée sur l'interface `enp0s9`, ou le mode promiscous n'est pas activé. Ce qui correspond à la configuration par défaut des machines virtuelles, ou des instances cloud.

Pour rappel, le ping lui ne fonctionne pas. Dans la requêtes ping, on voit que des requêtes ARP sont lancées, et aboutissent, mais que pour les requêtes ICMP, les `ICMP echo request` ne recoivent pas de réponse.

Il manquerait donc un routage au niveau de la couche ICMP/TCP/UDP
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping  10.0.2.15
```
``` bash
06:12:15.547504 fa:16:3e:57:4c:9c > 0a:00:27:00:00:03, ethertype IPv4 (0x0800), length 98: 192.168.50.188 > 10.0.2.15: ICMP echo request, id 15746, seq 5, length 64
06:12:16.571402 fa:16:3e:57:4c:9c > 0a:00:27:00:00:03, ethertype IPv4 (0x0800), length 98: 192.168.50.188 > 10.0.2.15: ICMP echo request, id 15746, seq 6, length 64
06:12:16.571816 fa:16:3e:57:4c:9c > 0a:00:27:00:00:03, ethertype ARP (0x0806), length 42: Request who-has 192.168.50.1 tell 192.168.50.188, length 28
06:12:16.572141 0a:00:27:00:00:03 > fa:16:3e:57:4c:9c, ethertype ARP (0x0806), length 60: Reply 192.168.50.1 is-at 0a:00:27:00:00:03, length 46
06:12:17.595393 fa:16:3e:57:4c:9c > 0a:00:27:00:00:03, ethertype IPv4 (0x0800), length 98: 192.168.50.188 > 10.0.2.15: ICMP echo request, id 15746, seq 7, length 64
```