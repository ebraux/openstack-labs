# Déploiement d'Openstack avec OpenVSwitch

---
##

2.11.3. Open vSwitch agent
The Open vSwitch (OVS) neutron plug-in uses its own agent, which runs on each node and manages the OVS bridges. The ML2 plugin integrates with a dedicated agent to manage L2 networks. By default, Red Hat OpenStack Platform uses ovs-agent, which builds overlay networks using OVS bridges.

Sur chaque compute : Layer2 switch (neutron Openvswitch agent)

Sur chaque network node (ou controleur) : Layer 3 router (neutre L3 agent)

Avec le driver Openvswitch, Neutron utilise les composants suivants .

- Linux Bridge
- OpenVSwitch
- Iptables
- Namespace réseau
---
## 

Par défaut, aucune configuration réseau n'est déployée.
On peut tout de même observer le déploiement de neutron

Il faut ensuite déployer un réseau externe, puis un réseau interne, avec un router.

Neutron n'implémente les objets au niveau du système, que lorsqu'ils sont utilisés.

Par exemple, un réseau ...




---
## Commandes utilisées

``` bash
sudo apt install bridge-utils -y
```
Pour interagir avec ces composants, les commandes utilisées sont
- Interfaces réseau
    - Lister les interfaces : `ip a`
    - Lister les routes : `ip route`
- IPtables
    - Lister les Chain et règles : `sudo iptables -L -n -v`
- EBtables
    - Lister les Chain et règles : `sudo ebtables -L`
- NFtable   
    - Lister les tables : `sudo nft list ruleset`
- Linux Bridge
    - lister les bridge : `brctl show`
- OpenVSwitch
  - Lister les bridges : `docker exec openvswitch_vswitchd ovs-vsctl show``
- Namespace réseau
    - Lister les namespace `ip netns`
    - exécuter une commande dans un namespace `ip exec <NETNS_ID> <COMMAND>`
- Openstack :
    - `openstack network agent list -c 'Agent Type' -c 'Alive' -c 'State'`
    - `openstack network agent list --agent-type  'l3'`
    - `openstack network agent show <ID>`

---
## Configuration de la machine

``` bash
sysctl net.ipv4.ip_forward
# net.ipv4.ip_forward = 0
```
modifier
``` bash
#sudo sysctl -w net.ipv4.ip_forward=1
```

Rendre permanent :
``` bash
sudo cp -p /etc/sysctl.conf /etc/sysctl.conf.DIST
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.d/10-network-forward.conf
sudo sysctl --system
```


---
## Configuration initiale

- Interfaces
    - enp0s3 : interface principale, connectée à l'exterieur :10.0.2.15/24
    - enp0s8 : interface
    -  prévue pour un réseau "internal", non utilisé, non connecté
    - enp0s9 : interface pour le réseau External, non connectée
- Linux Bridges : aucun
- OVS Bridges : sera géré par le conteneur neutron openvswitch_vswitchd
- namespaces : aucun
- iptables 
- 
- : par défaut

 


---
## Après installation d'Openstack

### Au niveau système

Création des 3 OVS Bridges br-ex, br-in et br-tun, et des patches les reliant :

- br-ex :
    - Interconnexion avec réseau publics
    - Interfaces :
        - interface physique `enp0s9`
        - interface virtuelle phy-br-ex
- br-in :
    - "internal bridge" Permet de connecter les instances
    - Interfaces :
        - Patche `int-br-ex`, which pairs with phy-br-ex to connect to the physical network shown in br-ex
        - Patch `patch-tun`, which is used to connect to the tunnel bridge and the TAP devices that connect to the instances currently running on the system:
- br-tun : 
    - tunnel d'interconnexion entre les hosts (dans le cas d'un cluster), permet d'interconnecter les instances dans un même réseau privé sur plusieurs noeuds
    - Interfaces :
        - Patch `patch-int` 
        - Dans le cas d'un cluster, `tunnel interfaces` (gre-<N>, ..) four chacun des neuds compute ou network


Les Interfaces correspondantes sont visibles au niveau du système. Elles sont vues comme DOWN, car elles ne sont pas gérée par le système, mais par OVS : 

- ovs-system : fe:25:1c:e7:4f:3b
- br-ex : 08:00:27:98:b8:17  --> la même que l'interface `enp0s9`
- br-int : 1e:19:1a:4c:61:4a
- br-tun : 2e:d8:b3:c1:3a:4f


Pas de nouvelle route créée, pas de Linux Bridge, rien dans les iptables, ebtable ou nftable ...


### Au niveau Openstack

Vérifier les agents neutron : 
``` bash
openstack network agent list -c 'Agent Type' -c 'Alive' -c 'State'
+--------------------+-------+-------+
| Agent Type         | Alive | State |
+--------------------+-------+-------+
| DHCP agent         | :-)   | UP    |
| L3 agent           | :-)   | UP    |
| Metadata agent     | :-)   | UP    |
| Open vSwitch agent | :-)   | UP    |
+--------------------+-------+-------+
```

Récupérer l'ID de l'agent
``` bash
openstack network  agent list --agent-type open-vswitch -f value -c ID 
# f9658bf3-143e-4616-9278-5b035a74eb47
```
et afficher les informations
``` bash
openstack network  agent  show -f json $(openstack network  agent list --agent-type open-vswitch -f value -c ID)
```
``` json
  "configuration": {
    "arp_responder_enabled": true,
    ...
    "bridge_mappings": {
      "physnet1": "br-ex"
    },
    ...
    "resource_provider_hypervisors": {
      "br-ex": "kos-aio"
    },
    ...
    "tunnel_types": [
      "vxlan"
    ],
    ...
    "tunneling_ip": "10.0.2.15",

```

Aucun port, ou réseau créé dans Openstack.

---
## Initialisation de variable

- éviter les erreur de saisie
- mieux faire la differnec entre les mots clé Openstack, et les nom des ressourecs créées

``` bash
export EXTERNAL_NETWORK_NAME=external
export EXTERNAL_SUBNET_NAME=external
export ROUTER_NAME=rt-demo
export ROUTER_2_NAME=rt-demo2
export NETWORK_NAME=net-1
export SUBNET_NAME=subnet-1
export EXTERNAL_SUBNET_RANGE=192.168.50.0/24
export EXTERNAL_SUBNET_GATEWAY=192.168.50.1
export EXTERNAL_SUBNET_POOL_START=192.168.50.100
export EXTERNAL_SUBNET_POOL_END=192.168.50.199
export EXTERNAL_SUBNET_DNS=8.8.4.4

# export EXTERNAL_SUBNET_RANGE=10.20.11.0/24
# export EXTERNAL_SUBNET_GATEWAY=10.20.11.1
# export EXTERNAL_SUBNET_POOL_START=10.20.11.100
# export EXTERNAL_SUBNET_POOL_END=10.20.11.199
```

---
## Création d'un réseau public

Affichage de la configuration du `provider-network`, faisant le lien entre neutron et le réseau physique (physnet)
``` bash
docker exec neutron_server cat  /etc/neutron/plugins/ml2/ml2_conf.ini | grep flat_networks
# flat_networks = physnet1
```

Création du réseau public
``` bash
openstack network create --external --share \
  --provider-physical-network physnet1 \
  --provider-network-type flat \
  ${EXTERNAL_NETWORK_NAME}

```

### Au niveau d'openstack

``` bash
openstack network show ${EXTERNAL_NETWORK_NAME} | grep provider
| provider:network_type     | flat                                 |
| provider:physical_network | physnet1                             |
| provider:segmentation_id  | None                                 |
```

``` bash
openstack network list
# +--------------------------------------+----------+---------+
# | ID                                   | Name     | Subnets |
# +--------------------------------------+----------+---------+
# | e73f3efe-a364-48b1-8c50-d48dffa50470 | external |         |
# +--------------------------------------+----------+---------+
```
### Au niveau Openstack 

Création d'un network


### Au niveau du système

Pas d'interface, pas d'iptable, ebtable, nft
Pas de Linux bridge, pas de port ajouté dans la config d'OVS ..

Pour l'instant, le réseau n'est qu'un objet interne à Openstack, sans implémentation.

Normalement, on devrait avoir : une interface dans le bridge d'intégration

---
## Création d'un sous réseau public

``` bash  
openstack subnet create --no-dhcp \
    --network ${EXTERNAL_NETWORK_NAME} \
    --allocation-pool start=${EXTERNAL_SUBNET_POOL_START},end=${EXTERNAL_SUBNET_POOL_END} \
    --subnet-range ${EXTERNAL_SUBNET_RANGE} \
    --gateway ${EXTERNAL_SUBNET_GATEWAY} \
    --dns-nameserver ${EXTERNAL_SUBNET_DNS} \
    ${EXTERNAL_SUBNET_NAME}
```

### Au niveau Openstack 

Création du subnet. Pas de port créé.

### Au niveau système

Pas d'interface, pas d'iptable, ebtable, nft
Pas de Linux bridge, pas de port ajouté dans la config d'OVS ..

Pour l'instant, le sous-réseau est également uniquement un objet interne à Openstack, sans implémentation.

Normalement, on devrait avoir :

- Un namespace DCHP, correspondant au sous réseau
- Un metadata agent pour permettre aux VM d'obtenir leur metadonnées



---
## Création d'un routeur

``` bash
openstack router create ${ROUTER_NAME}
```

### Au niveau système

Pas d'interface, pas d'iptable, ebtable, nft, ...
Pas de Linux bridge, pas de port ajouté dans la config d'OVS ..

Pour l'instant, le sous-réseau est également uniquement un objet interne à Openstack, sans implémenta


On retrouve le netns, et la so interface lo uniquement.

Déclenche la réation de toutes les iptables pour neutron : neutron-filter-top, 
 neutron-openvswi-FORWARD, neutron-openvswi-INPUT, neutron-openvswi-OUTPUT, neutron-openvswi-local, neutron-openvswi-sg-chain et neutron-openvswi-sg-fallback.
tion.


---
## Ajout d'une interface entre le routeur et le réseau externe

``` bash
openstack router set --external-gateway ${EXTERNAL_NETWORK_NAME}  ${ROUTER_NAME} 
```

### Au niveau Openstack 

Relever l'ID et l'adresse IP externe du router :
``` bash
openstack router  show -f json ${ROUTER_NAME} 
```
``` json
  ...
  "external_gateway_info": {
    "network_id": "e73f3efe-a364-48b1-8c50-d48dffa50470",
    "external_fixed_ips": [
      {
        "subnet_id": "87a8ca77-8a42-43b6-bbb4-38129e7e6f72",
        "ip_address": "192.168.50.188"
      }
    ],
    "enable_snat": true
    ...
    "id": "b936ff62-612c-4345-b8fd-3f20b8a3cacd",
    ...
```
### Au niveau d'Openstack

``` json
 openstack port list -f json
[
  {
    "ID": "f4483144-0717-4d3d-8781-01064f2bf5e6",
    "Name": "",
    "MAC Address": "fa:16:3e:57:4c:9c",
    "Fixed IP Addresses": [
      {
        "subnet_id": "87a8ca77-8a42-43b6-bbb4-38129e7e6f72",
        "ip_address": "192.168.50.188"
      }
    ],
    "Status": "ACTIVE"
  }
]
```
### Au niveau Système

Création des Iptables pour neutron : neutron-filter-top, 
neutron-openvswi-INPUT,
neutron-openvswi-FORWARD, neutron-openvswi-OUTPUT,
neutron-openvswi-local,
neutron-openvswi-sg-chain,
neutron-openvswi-sg-fallback

Création des Iptables spécifiques au routeur dans la Chain  `neutron-openvswi-FORWARD`, pour accepter les paquets entrant et sortant, sur la tap `tapf4483144-07`

> Rem on retrouve ces iptables dans la configuration nftable.

Création d'un namespace : 

- nom : `qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd`
- id: 0.
- contenant une interface, correspondant à la gateway vers external :
    - nom : qg-f4483144-07
    - link/ether fa:16:3e:57:4c:9c
    - inet 192.168.50.188/24
- routes :
    - `default via 192.168.50.1 dev qg-f4483144-07 proto static`
    - `192.168.50.0/24 dev qg-f4483144-07 proto kernel scope link src 192.168.50.188`
Depuis ce namespace, un ping vers la passerelle 192.168.50.1, ou la machine edge 192.168.50.13 doit fonctionner.

Commande à utiliser pour les tests :
``` bash
sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip a

sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ip route

sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.1

sudo ip netns exec qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd ping -c 3 192.168.50.13
```


Création d'un port dans br-int
``` bash
        Port qg-f4483144-07
            tag: 1
            Interface qg-f4483144-07
                type: internal
```
Et ajout de flow OVS permettant de rediriger les flux avec le tag 1.


Pas de Linux Bridge


On a donc :
- un port Openstack créé :
    - "ID": "f4483144-0717-4d3d-8781-01064f2bf5e6"
    - "MAC Address": "fa:16:3e:57:4c:9c",
    - "ip_address": "192.168.50.188"
- un namespace `qrouter-b936ff62-612c-4345-b8fd-3f20b8a3cacd` créé, qui contient une interface :
    - name qg-f4483144-07
    - link/ether fa:16:3e:57:4c:9c
    - inet 192.168.50.188/24
- un port crée dans br-int, et des flow associés
    - Port qg-f4483144-07
    - tag: 1
    - Interface qg-f4483144-07
- des règles IPtables, 
    - interface  `--physdev-in tapf4483144-07 --physdev-is-bridged`     
    - reprend bien le `f4483144-07`, mais où est la tap ???


---




---
## Ressources

- [https://docs.openstack.org/neutron/latest/admin/deploy-ovs-provider.html](https://docs.openstack.org/neutron/latest/admin/deploy-ovs-provider.html )
- [https://docs.openstack.org/neutron/latest/admin/deploy-ovs-selfservice.html](https://docs.openstack.org/neutron/latest/admin/deploy-ovs-selfservice.html)
- [https://access.redhat.com/documentation/de-de/red_hat_openstack_platform/13/html/networking_guide/networking-concepts_rhosp-network](https://access.redhat.com/documentation/de-de/red_hat_openstack_platform/13/html/networking_guide/networking-concepts_rhosp-network)