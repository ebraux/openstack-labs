# Utilisation de placement

---
## Description 

Utilisation du service "Placement".

- Affichage des information d'inventory
- utilisation dans le cadre de l'ajout d'une instance


Référence :
- [https://docs.openstack.org/osc-placement/latest/cli/index.html](https://docs.openstack.org/osc-placement/latest/cli/index.html)

---
## Installation du client "osc-placement"
 
``` bash
sudo dnf install -y   python3-osc-placement
```


---
## Observation de la configuration


Configuration de l'environnement
``` bash
source ~/admin-openrc
env | grep OS_
# OS_USERNAME=admin
# ..
```


Affichage des "Ressources Class" disponibles
``` bash
openstack resource class list
# +----------------------------------------+
# | name                                   |
# +----------------------------------------+
# | VCPU                                   |
# | MEMORY_MB                              |
# | DISK_GB                                |
# | PCI_DEVICE                             |
# | SRIOV_NET_VF                           |
# | NUMA_SOCKET                            |
# | NUMA_CORE                              |
# | NUMA_THREAD                            |
# | NUMA_MEMORY_MB                         |
# | IPV4_ADDRESS                           |
# | VGPU                                   |
# | VGPU_DISPLAY_HEAD                      |
# | NET_BW_EGR_KILOBIT_PER_SEC             |
# | NET_BW_IGR_KILOBIT_PER_SEC             |
# | PCPU                                   |
# | MEM_ENCRYPTION_CONTEXT                 |
# | FPGA                                   |
# | PGPU                                   |
# | NET_PACKET_RATE_KILOPACKET_PER_SEC     |
# | NET_PACKET_RATE_EGR_KILOPACKET_PER_SEC |
# | NET_PACKET_RATE_IGR_KILOPACKET_PER_SEC |
# +----------------------------------------+
```

Affichage des "Ressources Providers" disponibles
``` bash
openstack resource provider list
# +---------+-----------+------------+--------------------------------------+----------------------+
# | uuid    | name      | generation | root_provider_uuid                   | parent_provider_uuid |
# +---------+-----------+------------+--------------------------------------+----------------------+
# | 0d...6a | compute   |          2 | 0d249616-64f7-4c07-8d46-8ca05fc6c76a | None                 |
# +---------+-----------+------------+--------------------------------------+----------------------+
```


Mémorisation de l'uuid du provider
``` bash
UUID_PROV=$(openstack resource provider list --name compute -f value -c uuid)
echo $UUID_PROV
```

> Adapter l'option --name, en fonction du champ nae renvoyé par la commande 'resource provider list'


Détail des ressources gérées par le ressource provider 'compute'
``` bash
openstack resource provider show $UUID_PROV
# +----------------------+--------------------------------------+
# | Field                | Value                                |
# +----------------------+--------------------------------------+
# | uuid                 | 0d249616-64f7-4c07-8d46-8ca05fc6c76a |
# | name                 | compute                              |
# | generation           | 2                                    |
# | root_provider_uuid   | 0d249616-64f7-4c07-8d46-8ca05fc6c76a |
# | parent_provider_uuid | None                                 |
# +----------------------+--------------------------------------+
```

Liste des instances lancées
``` bash
openstack server list  --project=demo  --long -c Name -c Status -c Host -c 'Image Name' -c 'Flavor Name'
# +-----------+--------+------------+-------------+-----------+
# | Name      | Status | Image Name | Flavor Name | Host      |
# +-----------+--------+------------+-------------+-----------+
# | demoTest  | ACTIVE | cirros     | cirros      | compute |
# +-----------+--------+------------+-------------+-----------+
```

Affichage des ressources utilisée sur le provider 'compute'
``` bash
openstack resource provider usage show $UUID_PROV
# +----------------+-------+
# | resource_class | usage |
# +----------------+-------+
# | VCPU           |     1 |
# | MEMORY_MB      |   256 |
# | DISK_GB        |     1 |
# +----------------+-------+
```
Ce qui correspond au déploiment de l'instance cirros

Affichage détaillé :
``` bash
openstack resource provider inventory list $UUID_PROV
# +----------------+------------------+----------+----------+----------+-----------+-------+------+
# | resource_class | allocation_ratio | min_unit | max_unit | reserved | step_size | total | used |
# +----------------+------------------+----------+----------+----------+-----------+-------+------+
# | VCPU           |             16.0 |        1 |        2 |        0 |         1 |     2 |    1 |
# | MEMORY_MB      |              1.5 |        1 |    11702 |      256 |         1 | 11702 |  256 |
# | DISK_GB        |              1.0 |        1 |      124 |        0 |         1 |   124 |    1 |
# +----------------+------------------+----------+----------+----------+-----------+-------+------+
```

Affichage des caractéristques avancées de filtrage disponibles pour le provider 'compute'

``` bash
openstack resource provider trait list $UUID_PROV
# +---------------------------------------+
# | name                                  |
# +---------------------------------------+
# | COMPUTE_ACCELERATORS                  |
# | COMPUTE_DEVICE_TAGGING                |
# | COMPUTE_GRAPHICS_MODEL_NONE           |
# | COMPUTE_GRAPHICS_MODEL_VGA            |
# | COMPUTE_GRAPHICS_MODEL_CIRRUS         |
# | COMPUTE_GRAPHICS_MODEL_VIRTIO         |
# | COMPUTE_GRAPHICS_MODEL_BOCHS          |
# | COMPUTE_IMAGE_TYPE_AKI                |
# | COMPUTE_IMAGE_TYPE_AMI                |
# | COMPUTE_IMAGE_TYPE_ARI                |
# | COMPUTE_IMAGE_TYPE_ISO                |
# | COMPUTE_IMAGE_TYPE_QCOW2              |
# | COMPUTE_IMAGE_TYPE_RAW                |
# | COMPUTE_NET_ATTACH_INTERFACE_WITH_TAG |
# | COMPUTE_NET_VIF_MODEL_E1000E          |
# | COMPUTE_NET_VIF_MODEL_RTL8139         |
# | COMPUTE_NET_VIF_MODEL_E1000           |
# | COMPUTE_NET_VIF_MODEL_SPAPR_VLAN      |
# | COMPUTE_NET_VIF_MODEL_VMXNET3         |
# | COMPUTE_NET_VIF_MODEL_PCNET           |
# | COMPUTE_NET_VIF_MODEL_NE2K_PCI        |
# | COMPUTE_NET_VIF_MODEL_LAN9118         |
# | COMPUTE_NET_VIF_MODEL_VIRTIO          |
# | COMPUTE_NET_ATTACH_INTERFACE          |
# | COMPUTE_NODE                          |
# | COMPUTE_STORAGE_BUS_FDC               |
# | COMPUTE_STORAGE_BUS_SATA              |
# | COMPUTE_STORAGE_BUS_VIRTIO            |
# | COMPUTE_TRUSTED_CERTS                 |
# | COMPUTE_VOLUME_ATTACH_WITH_TAG        |
# | COMPUTE_VOLUME_EXTEND                 |
# | COMPUTE_VOLUME_MULTI_ATTACH           |
# | COMPUTE_RESCUE_BFV                    |
# | COMPUTE_SECURITY_UEFI_SECURE_BOOT     |
# | COMPUTE_SOCKET_PCI_NUMA_AFFINITY      |
# | COMPUTE_STORAGE_BUS_USB               |
# | COMPUTE_STORAGE_BUS_SCSI              |
# | COMPUTE_STORAGE_BUS_IDE               |
# | HW_CPU_X86_AESNI                      |
# | HW_CPU_X86_SSE42                      |
# | HW_CPU_X86_CLMUL                      |
# | HW_CPU_X86_ABM                        |
# | HW_CPU_X86_SSE2                       |
# | HW_CPU_X86_SSE41                      |
# | HW_CPU_X86_MMX                        |
# | HW_CPU_X86_SSE                        |
# | HW_CPU_X86_SSSE3                      |
# | HW_CPU_X86_AVX                        |
# | HW_CPU_X86_AVX2                       |
# +---------------------------------------+
```

---
## Suivi du déploiement d'une nouvelle instance


### Recherche des "Resource Providers" pouvant héberger l'instance

Informations sur le gaarit Cirros
``` bash
openstack flavor show cirros  -c vcpus -c ram
# +-------+--------+
# | Field | Value  |
# +-------+--------+
# | ram   | 256    |
# | vcpus | 1      |
# +-------+--------+
```

Affichage des "Resource Providers" pouvant héberger une instance avec un gabarit de type cirros
``` bash
openstack allocation candidate list --resource VCPU=1 --resource MEMORY_MB=256
# ...

openstack allocation candidate list --resource VCPU=1 --resource MEMORY_MB=256  -f value -c 'resource provider'
# 0d249616-64f7-4c07-8d46-8ca05fc6c76a

openstack resource provider show 0d249616-64f7-4c07-8d46-8ca05fc6c76a -f value -c name
# compute
```

### Déploiment de l'instance

On déploie une instance avec le compte admin, dans le projet "demo"
``` bash
OS_PROJECT_NAME=demo
```

On reprend la configuartion du TP "Demo"
``` bash
PROJECT_NAME=demo
NETWORK_NAME=net-${PROJECT_NAME}
SUBNET_NAME=subnet-${PROJECT_NAME}
ROUTER_NAME=router-${PROJECT_NAME}
EXTERNAL_NETWORK_NAME=external
FLAVOR_CIRROS_ID=$(openstack flavor list -f value -c Name | grep  cirros)
IMAGE_CIRROS_ID=$(openstack image list -f value -c Name | grep 'cirros')

# lancement d'une instance
TEST_INSTANCE_NAME=${PROJECT_NAME}Test3
openstack server create \
   --flavor ${FLAVOR_CIRROS_ID} \
   --image ${IMAGE_CIRROS_ID} \
   --nic net-id=${NETWORK_NAME} \
   --security-group ${SECGROUP_ADMIN} \
   ${TEST_INSTANCE_NAME}
openstack server show   ${TEST_INSTANCE_NAME} 
```

Retour sur le sur le projet "admin"
``` bash
OS_PROJECT_NAME=admin
```

Liste des instances lancées
``` bash
openstack server list  --project=demo  --long -c Name -c Status -c Host -c 'Image Name' -c 'Flavor Name'
# +-----------+--------+------------+-------------+---------+
# | Name      | Status | Image Name | Flavor Name | Host    |
# +-----------+--------+------------+-------------+---------+
# | demoTest2 | ACTIVE | cirros     | cirros      | compute |
# | demoTest  | ACTIVE | cirros     | cirros      | compute |
# +-----------+--------+------------+-------------+---------+
```

###  Observation de l'évolution des ressources utilisée sur le provider 'compute' 

``` bash
openstack resource provider usage show $UUID_PROV
# +----------------+-------+
# | resource_class | usage |
# +----------------+-------+
# | VCPU           |     2 |
# | MEMORY_MB      |   512 |
# | DISK_GB        |     2 |
# +----------------+-------+
```
On retouve l'augmentation des ressources utilisées.


Affichage détaillé :
``` bash
openstack resource provider inventory list $UUID_PROV
# +----------------+------------------+----------+----------+----------+-----------+-------+------+
# | resource_class | allocation_ratio | min_unit | max_unit | reserved | step_size | total | used |
# +----------------+------------------+----------+----------+----------+-----------+-------+------+
# | VCPU           |             16.0 |        1 |        2 |        0 |         1 |     2 |    2 |
# | MEMORY_MB      |              1.5 |        1 |    11702 |      512 |         1 | 11702 |  512 |
# | DISK_GB        |              1.0 |        1 |      124 |        0 |         1 |   124 |    2 |
# +----------------+------------------+----------+----------+----------+-----------+-------+------+


---
## Pour aller plus loin :

- [https://object-storage-ca-ymq-1.vexxhost.net/swift/v1/6e4619c416ff4bd19e1c087f27a43eea/www-assets-prod/presentation-media/Placement-Berlin-Final.pdf](https://object-storage-ca-ymq-1.vexxhost.net/swift/v1/6e4619c416ff4bd19e1c087f27a43eea/www-assets-prod/presentation-media/Placement-Berlin-Final.pdf)

