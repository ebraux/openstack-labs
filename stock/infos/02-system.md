
---
## Résolution DNS

La plupart des fichiers de configuration sont prévus pour contacter les API du control-plane via le nom 'controller'. Il est donc nécessaire de configurer la resolution de nom sur les instances, via le fichier /etc/hosts.

Exemple configuration du fichier host dans le cas de la configuration adresse IP interne du controller = 172.16.50.21, adresse IP interne du compute = 172.16.50.31

```bash
# Gestion de la resolution de nom
sudo cp -p /etc/hosts /etc/hosts.DIST
sudo sed -i 1i$'172.16.50.31    compute' /etc/hosts
sudo sed -i 1i$'172.16.50.21    controller' /etc/hosts
```

Vérification du contenu des fichiers /etc/host avec la commande : `cat /etc/hosts`
```bash
172.16.50.31    compute
172.16.50.21    controller
```

Vérification de la connexion entre les 2 instances déployées :
```bash
ping -c 4 controller
ping -c 4 compute
#  -> les IP des machines doivent être de type 172.16.0.xx et non 127.0.0.1
```

---

## Connexion en tant que root

Un grand nombre d'opérations doivent être réalisée avec un utilisateur avec privilèges (root). Ces commandes doivent être exécutées avec la commande `sudo`, ou `sudo -E`si il est nécessaire d'utiliser des variables d'environnement.

---

