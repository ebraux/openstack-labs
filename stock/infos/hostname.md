

## Gestion des noms des instances

Le nom d'hôte des machines doit correspondre à "controller" et "compute", pour que les services d'Openstack fonctionnent correctement.

Exemple pour le noeud "controller"
``` bash
sudo hostnamectl set-hostname controller
sudo reboot
```

Et vérifier après re-démarrage de la machine
``` bash
hostname
# controller
```

Pas necessaire. il faut que la résolution DNS fonctionne.
mais c'ets tout.