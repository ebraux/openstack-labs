## Mise en place de la partie "storage Node LVM"


### Installation du service cinder-volume, et des des composants LVM et iscsi.

```bash
sudo dnf install -y  openstack-cinder \
  lvm2 device-mapper-persistent-data \
  targetcli
```

> "openstack-cinder" a déjà été installé dans la partie "controller node". Dans le cas de l'instalation d'un Storage Node indépendant, il est necesaire pour la partie "cinder-volume"


---
## Configuration 

La configuration a été anticipée dans la partie controller.

Dans le cas d'un  Storage Node indépendant, les configurations nécéssaires sont identiques à celle d'un control node, plus les informations de configuration LVM.



### Préparation du volume de stockage

Vérifier l'espace disponible
``` bash
sudo df -h
# Filesystem                 Size  Used Avail Use% Mounted on
# devtmpfs                   4.0M     0  4.0M   0% /dev
# tmpfs                      5.8G     0  5.8G   0% /dev/shm
# tmpfs                      2.3G   25M  2.3G   2% /run
# /dev/mapper/centos9s-root  125G  4.6G  121G   4% /
# /dev/sda1                 1014M  322M  693M  32% /boot
# tmpfs                      1.2G     0  1.2G   0% /run/user/1001
# tmpfs                      1.2G  4.0K  1.2G   1% /run/user/0

```
Ici on voit que le volume "/dev/mapper/centos9s-root" monté sur la racine "/" fait 125Go, dont 120G disponibles. On va utiliser 2Go pour notre espace de stockage local LVM.


Création du fichier qui va  accueillir le disque virtuel
``` bash
sudo mkdir -p /var/lib/cinder/
sudo fallocate -l 2G /var/lib/cinder/cinder-volumes

sudo ls -lh /var/lib/cinder/cinder-volumes
# -rw-r--r--. 1 root root 2G Sep 21 12:12 /var/lib/cinder/cinder-volumes
```

Configuration du "loopback device"
``` bash
# Association
sudo losetup /dev/loop2 /var/lib/cinder/cinder-volumes

# verification
sudo losetup -a
# dev/loop2: [64768]:201584090 (/var/lib/cinder/cinder-volumes)
```

Rendre le montage permanent : fichier de configuration [cinder-lvm-losetup.service]  (conf/cinder_cinder-lvm-losetup.service) à mettre en place dans le fichier : /etc/systemd/system/cinder-lvm-losetup.service
``` bash
# copie du fichier de service
sudo cp ~/openstack_config/cinder_cinder-lvm-losetup.service /etc/systemd/system/cinder-lvm-losetup.service
sudo chown root.root /etc/systemd/system/cinder-lvm-losetup.service

# activation du service
sudo systemctl enable cinder-lvm-losetup.service
sudo systemctl start cinder-lvm-losetup.service
```

Vérification
``` bash
sudo fdisk -l /dev/loop2
# Disk /dev/loop2: 2 GiB, 2147483648 bytes, 4194304 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
```


