## Configuration OVS

Installation des packages OVS
``` bash
sudo apt install -y openvswitch-switch openvswitch-common 
```

``` bash
sudo modprobe bridge
sudo modprobe br_netfilter 

cp  ~/openstack_config/sysctl_99-openstack.conf  /etc/sysctl.d/99-openstack.conf
sudo chown root.root /etc/sysctl.d/99-openstack.conf
sudo sysctl --system
```



Création du bridge
``` bash
export OVS_PHYSICAL_BRIDGE=br-ex
export FLOATING_RANGE=172.24.4.0/24
export FLOATING_GATEWAY_IP=172.24.4.1
export FLOATING_GATEWAY_CIDR=${FLOATING_GATEWAY_IP}/24
```

``` bash
sudo ovs-vsctl add-br  $OVS_PHYSICAL_BRIDGE

sudo ovs-vsctl show
# d0c54868-b61c-4656-8ae1-8a0e2b544479
#    Bridge br-ex
#        Port br-ex
#            Interface br-ex
#                type: internal
#    ovs_version: "3.2.1"
```

```  bash
sudo ip addr add $FLOATING_GATEWAY_CIDR dev $OVS_PHYSICAL_BRIDGE
sudo ip link set $OVS_PHYSICAL_BRIDGE up
```

``` bash
ip a
# ...
# 3: ovs-system: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
#     link/ether b6:2e:4d:19:90:61 brd ff:ff:ff:ff:ff:ff
# 4: br-ex: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/ether f2:9c:01:f5:79:41 brd ff:ff:ff:ff:ff:ff
#     inet 172.24.4.1/24 scope global br-ex
#        valid_lft forever preferred_lft forever
#     inet6 fe80::f09c:1ff:fef5:7941/64 scope link 
#        valid_lft forever preferred_lft forever
```

``` bash
ping -c 4 $FLOATING_GATEWAY_IP
```

> rem si i on veut ajouter une interface physique existante : 
``` bash
export PROVIDER_INTERFACE=eth1
sudo ip addr flush dev $PROVIDER_INTERFACE
ovs-vsctl add-port $OVS_PHYSICAL_BRIDGE $PROVIDER_INTERFACE
```
  

- [https://docs.openstack.org/neutron/latest/admin/deploy-ovs-selfservice.html](https://docs.openstack.org/neutron/latest/admin/deploy-ovs-selfservice.html)
- [https://docs.openvswitch.org/en/latest/faq/issues/](https://docs.openvswitch.org/en/latest/faq/issues/)
