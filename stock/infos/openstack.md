---

## Connexion aux instances

Seul le contrôleur dispose d'une IP publique.

```bash
ssh -l ubuntu -i <VOTRE_CLE>.pem <IP_FLOTTANTE>
```
